namespace GeneradorDBF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnIniciar = new System.Windows.Forms.Button();
            this.btnDetener = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.picLight = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpFin = new System.Windows.Forms.DateTimePicker();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgxProcesar = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id_Entidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id_Cuestionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cuestionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Archivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaSolicitud = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnLog = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.dgTerminadas = new System.Windows.Forms.DataGridView();
            this.Id_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id_Entidad_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id_Cuestionario_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cuestionario_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estatus_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Archivo_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Usuario_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaSolicitud_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnOcultar = new System.Windows.Forms.Button();
            this.bgwMake = new System.ComponentModel.BackgroundWorker();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnDetalle = new System.Windows.Forms.Button();
            this.lblConectado = new System.Windows.Forms.Label();
            this.picConexion = new System.Windows.Forms.PictureBox();
            this.bgwConnect = new System.ComponentModel.BackgroundWorker();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mostrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbProgress = new System.Windows.Forms.ProgressBar();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLight)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgxProcesar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTerminadas)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picConexion)).BeginInit();
            this.contextMenuStripTray.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(71, 22);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(75, 23);
            this.btnIniciar.TabIndex = 0;
            this.btnIniciar.Text = "Iniciar";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // btnDetener
            // 
            this.btnDetener.Location = new System.Drawing.Point(71, 52);
            this.btnDetener.Name = "btnDetener";
            this.btnDetener.Size = new System.Drawing.Size(75, 23);
            this.btnDetener.TabIndex = 1;
            this.btnDetener.Text = "Detener";
            this.btnDetener.UseVisualStyleBackColor = true;
            this.btnDetener.Click += new System.EventHandler(this.btnDetener_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.picLight);
            this.groupBox1.Controls.Add(this.btnDetener);
            this.groupBox1.Controls.Add(this.btnIniciar);
            this.groupBox1.Location = new System.Drawing.Point(217, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(168, 92);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Control";
            // 
            // picLight
            // 
            this.picLight.Image = global::GeneradorDBF.Properties.Resources.red;
            this.picLight.Location = new System.Drawing.Point(12, 19);
            this.picLight.Name = "picLight";
            this.picLight.Size = new System.Drawing.Size(48, 56);
            this.picLight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLight.TabIndex = 6;
            this.picLight.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtpFin);
            this.groupBox2.Controls.Add(this.dtpInicio);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(401, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(189, 92);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Temporizador";
            // 
            // dtpFin
            // 
            this.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpFin.Location = new System.Drawing.Point(86, 50);
            this.dtpFin.Name = "dtpFin";
            this.dtpFin.ShowUpDown = true;
            this.dtpFin.Size = new System.Drawing.Size(63, 20);
            this.dtpFin.TabIndex = 18;
            this.dtpFin.ValueChanged += new System.EventHandler(this.dtpFin_ValueChanged);
            // 
            // dtpInicio
            // 
            this.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpInicio.Location = new System.Drawing.Point(86, 22);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.ShowUpDown = true;
            this.dtpInicio.Size = new System.Drawing.Size(63, 20);
            this.dtpInicio.TabIndex = 17;
            this.dtpInicio.ValueChanged += new System.EventHandler(this.dtpInicio_ValueChanged);
            this.dtpInicio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpInicio_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(155, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "hrs.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(155, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "hrs.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Hora limite";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Inicio \r\nprogramado";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tareas por procesar";
            // 
            // dgxProcesar
            // 
            this.dgxProcesar.AllowUserToAddRows = false;
            this.dgxProcesar.AllowUserToDeleteRows = false;
            this.dgxProcesar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgxProcesar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Id_Entidad,
            this.Id_Cuestionario,
            this.Cuestionario,
            this.Tipo,
            this.Estatus,
            this.Archivo,
            this.Usuario,
            this.FechaSolicitud});
            this.dgxProcesar.Location = new System.Drawing.Point(28, 135);
            this.dgxProcesar.Name = "dgxProcesar";
            this.dgxProcesar.ReadOnly = true;
            this.dgxProcesar.Size = new System.Drawing.Size(745, 125);
            this.dgxProcesar.TabIndex = 7;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            // 
            // Id_Entidad
            // 
            this.Id_Entidad.DataPropertyName = "Id_Entidad";
            this.Id_Entidad.HeaderText = "Id_Entidad";
            this.Id_Entidad.Name = "Id_Entidad";
            this.Id_Entidad.ReadOnly = true;
            // 
            // Id_Cuestionario
            // 
            this.Id_Cuestionario.DataPropertyName = "Id_Cuestionario";
            this.Id_Cuestionario.HeaderText = "Id_Cuestionario";
            this.Id_Cuestionario.Name = "Id_Cuestionario";
            this.Id_Cuestionario.ReadOnly = true;
            this.Id_Cuestionario.Visible = false;
            // 
            // Cuestionario
            // 
            this.Cuestionario.DataPropertyName = "Cuestionario";
            this.Cuestionario.HeaderText = "Cuestionario";
            this.Cuestionario.Name = "Cuestionario";
            this.Cuestionario.ReadOnly = true;
            // 
            // Tipo
            // 
            this.Tipo.DataPropertyName = "Tipo";
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.Name = "Tipo";
            this.Tipo.ReadOnly = true;
            // 
            // Estatus
            // 
            this.Estatus.DataPropertyName = "Estatus";
            this.Estatus.HeaderText = "Estatus";
            this.Estatus.Name = "Estatus";
            this.Estatus.ReadOnly = true;
            // 
            // Archivo
            // 
            this.Archivo.DataPropertyName = "Archivo";
            this.Archivo.HeaderText = "Archivo";
            this.Archivo.Name = "Archivo";
            this.Archivo.ReadOnly = true;
            this.Archivo.Visible = false;
            // 
            // Usuario
            // 
            this.Usuario.DataPropertyName = "Usuario";
            this.Usuario.HeaderText = "Usuario";
            this.Usuario.Name = "Usuario";
            this.Usuario.ReadOnly = true;
            // 
            // FechaSolicitud
            // 
            this.FechaSolicitud.DataPropertyName = "FechaSolicitud";
            this.FechaSolicitud.HeaderText = "FechaSolicitud";
            this.FechaSolicitud.Name = "FechaSolicitud";
            this.FechaSolicitud.ReadOnly = true;
            // 
            // btnLog
            // 
            this.btnLog.Location = new System.Drawing.Point(458, 422);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(83, 25);
            this.btnLog.TabIndex = 8;
            this.btnLog.Text = "Log de errores";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 263);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Tareas terminadas";
            // 
            // dgTerminadas
            // 
            this.dgTerminadas.AllowUserToAddRows = false;
            this.dgTerminadas.AllowUserToDeleteRows = false;
            this.dgTerminadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTerminadas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id_,
            this.Id_Entidad_,
            this.Id_Cuestionario_,
            this.Cuestionario_,
            this.Tipo_,
            this.Estatus_,
            this.Archivo_,
            this.Usuario_,
            this.FechaSolicitud_});
            this.dgTerminadas.Location = new System.Drawing.Point(28, 280);
            this.dgTerminadas.Name = "dgTerminadas";
            this.dgTerminadas.ReadOnly = true;
            this.dgTerminadas.Size = new System.Drawing.Size(745, 125);
            this.dgTerminadas.TabIndex = 10;
            // 
            // Id_
            // 
            this.Id_.DataPropertyName = "Id";
            this.Id_.HeaderText = "Id";
            this.Id_.Name = "Id_";
            this.Id_.ReadOnly = true;
            // 
            // Id_Entidad_
            // 
            this.Id_Entidad_.DataPropertyName = "Id_Entidad";
            this.Id_Entidad_.HeaderText = "Id_Entidad";
            this.Id_Entidad_.Name = "Id_Entidad_";
            this.Id_Entidad_.ReadOnly = true;
            // 
            // Id_Cuestionario_
            // 
            this.Id_Cuestionario_.DataPropertyName = "Id_Cuestionario";
            this.Id_Cuestionario_.HeaderText = "Id_Cuestionario";
            this.Id_Cuestionario_.Name = "Id_Cuestionario_";
            this.Id_Cuestionario_.ReadOnly = true;
            this.Id_Cuestionario_.Visible = false;
            // 
            // Cuestionario_
            // 
            this.Cuestionario_.DataPropertyName = "Cuestionario";
            this.Cuestionario_.HeaderText = "Cuestionario";
            this.Cuestionario_.Name = "Cuestionario_";
            this.Cuestionario_.ReadOnly = true;
            // 
            // Tipo_
            // 
            this.Tipo_.DataPropertyName = "Tipo";
            this.Tipo_.HeaderText = "Tipo";
            this.Tipo_.Name = "Tipo_";
            this.Tipo_.ReadOnly = true;
            // 
            // Estatus_
            // 
            this.Estatus_.DataPropertyName = "Estatus";
            this.Estatus_.HeaderText = "Estatus";
            this.Estatus_.Name = "Estatus_";
            this.Estatus_.ReadOnly = true;
            // 
            // Archivo_
            // 
            this.Archivo_.DataPropertyName = "Archivo";
            this.Archivo_.HeaderText = "Archivo";
            this.Archivo_.Name = "Archivo_";
            this.Archivo_.ReadOnly = true;
            // 
            // Usuario_
            // 
            this.Usuario_.DataPropertyName = "Usuario";
            this.Usuario_.HeaderText = "Usuario";
            this.Usuario_.Name = "Usuario_";
            this.Usuario_.ReadOnly = true;
            // 
            // FechaSolicitud_
            // 
            this.FechaSolicitud_.DataPropertyName = "FechaSolicitud";
            this.FechaSolicitud_.HeaderText = "FechaSolicitud";
            this.FechaSolicitud_.Name = "FechaSolicitud_";
            this.FechaSolicitud_.ReadOnly = true;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(252, 422);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(83, 25);
            this.btnCerrar.TabIndex = 11;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnOcultar
            // 
            this.btnOcultar.Location = new System.Drawing.Point(356, 422);
            this.btnOcultar.Name = "btnOcultar";
            this.btnOcultar.Size = new System.Drawing.Size(83, 25);
            this.btnOcultar.TabIndex = 12;
            this.btnOcultar.Text = "Ocultar";
            this.btnOcultar.UseVisualStyleBackColor = true;
            this.btnOcultar.Click += new System.EventHandler(this.btnOcultar_Click);
            // 
            // bgwMake
            // 
            this.bgwMake.WorkerReportsProgress = true;
            this.bgwMake.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwMake_DoWork);
            this.bgwMake.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwMake_ProgressChanged);
            this.bgwMake.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwMake_RunWorkerCompleted);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(6, 19);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(128, 20);
            this.txtPath.TabIndex = 14;
            this.txtPath.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtPath_MouseClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnExaminar);
            this.groupBox3.Controls.Add(this.txtPath);
            this.groupBox3.Location = new System.Drawing.Point(609, 36);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(168, 47);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Guardar en:";
            // 
            // btnExaminar
            // 
            this.btnExaminar.Location = new System.Drawing.Point(140, 17);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(24, 23);
            this.btnExaminar.TabIndex = 16;
            this.btnExaminar.Text = "...";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnDetalle);
            this.groupBox4.Controls.Add(this.lblConectado);
            this.groupBox4.Controls.Add(this.picConexion);
            this.groupBox4.Location = new System.Drawing.Point(10, 27);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(189, 67);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Conexi�n a base de datos";
            // 
            // btnDetalle
            // 
            this.btnDetalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetalle.Location = new System.Drawing.Point(69, 41);
            this.btnDetalle.Name = "btnDetalle";
            this.btnDetalle.Size = new System.Drawing.Size(54, 22);
            this.btnDetalle.TabIndex = 2;
            this.btnDetalle.Text = "Detalles";
            this.btnDetalle.UseVisualStyleBackColor = true;
            this.btnDetalle.Click += new System.EventHandler(this.btnDetalle_Click);
            // 
            // lblConectado
            // 
            this.lblConectado.AutoSize = true;
            this.lblConectado.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConectado.Location = new System.Drawing.Point(62, 12);
            this.lblConectado.Name = "lblConectado";
            this.lblConectado.Size = new System.Drawing.Size(68, 13);
            this.lblConectado.TabIndex = 1;
            this.lblConectado.Text = "lblConectado";
            this.lblConectado.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picConexion
            // 
            this.picConexion.Image = global::GeneradorDBF.Properties.Resources.Error;
            this.picConexion.Location = new System.Drawing.Point(18, 22);
            this.picConexion.Name = "picConexion";
            this.picConexion.Size = new System.Drawing.Size(29, 28);
            this.picConexion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picConexion.TabIndex = 0;
            this.picConexion.TabStop = false;
            // 
            // bgwConnect
            // 
            this.bgwConnect.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwConnect_DoWork);
            this.bgwConnect.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwConnect_RunWorkerCompleted);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.ContextMenuStrip = this.contextMenuStripTray;
            this.notifyIcon.Text = "Generador DBF";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_DoubleClick);
            // 
            // contextMenuStripTray
            // 
            this.contextMenuStripTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mostrarToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.contextMenuStripTray.Name = "contextMenuStripTray";
            this.contextMenuStripTray.Size = new System.Drawing.Size(116, 48);
            // 
            // mostrarToolStripMenuItem
            // 
            this.mostrarToolStripMenuItem.Name = "mostrarToolStripMenuItem";
            this.mostrarToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.mostrarToolStripMenuItem.Text = "Mostrar";
            this.mostrarToolStripMenuItem.Click += new System.EventHandler(this.mostrarToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // pbProgress
            // 
            this.pbProgress.Location = new System.Drawing.Point(133, 113);
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(100, 16);
            this.pbProgress.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(802, 459);
            this.Controls.Add(this.pbProgress);
            this.Controls.Add(this.btnOcultar);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.dgTerminadas);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgxProcesar);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Generador DBF";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLight)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgxProcesar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTerminadas)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picConexion)).EndInit();
            this.contextMenuStripTray.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Button btnDetener;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picLight;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgxProcesar;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgTerminadas;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnOcultar;
        private System.ComponentModel.BackgroundWorker bgwMake;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnExaminar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox picConexion;
        private System.ComponentModel.BackgroundWorker bgwConnect;
        private System.Windows.Forms.Label lblConectado;
        private System.Windows.Forms.Button btnDetalle;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTray;
        private System.Windows.Forms.ToolStripMenuItem mostrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id_Entidad_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id_Cuestionario_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cuestionario_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estatus_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Archivo_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Usuario_;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaSolicitud_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id_Entidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id_Cuestionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cuestionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Archivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaSolicitud;
        private System.Windows.Forms.DateTimePicker dtpInicio;
        private System.Windows.Forms.DateTimePicker dtpFin;
        private System.Windows.Forms.ProgressBar pbProgress;
    }
}

