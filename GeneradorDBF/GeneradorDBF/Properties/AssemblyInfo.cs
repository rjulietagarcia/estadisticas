﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GeneradorDBF")]
[assembly: AssemblyDescription("Generador de DBFs - Cuestionarios 911")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SEP")]
[assembly: AssemblyProduct("GeneradorDBF para SQL Server")]
[assembly: AssemblyCopyright("Nugasys S.A de C.V. Copyright ©  2010 Monterrey Mexico")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3a4e5612-a2c5-41a3-a32a-56e164d52ef0")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.666")]
[assembly: AssemblyFileVersion("1.0.0.666")]
