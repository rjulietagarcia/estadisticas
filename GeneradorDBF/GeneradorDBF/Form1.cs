using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;
using CoreGeneradorDBF.Consultas;

namespace GeneradorDBF
{

    [System.Diagnostics.DebuggerDisplay("Form1")]
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int progressPercent;
        string contra = "";
        string resultado;
        string message;
        string savePath;
        bool start = true;
        bool closeRequest;
        DBFprocess dbf = new DBFprocess();
        System.Configuration.AppSettingsReader rd = new System.Configuration.AppSettingsReader();
        VW_SolicituddbfDP[] solDP;
        SolicituddbfDP soldbfDP = new SolicituddbfDP();
        SolicituddbfMD solMD;
        string appPath = AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug\\", "").Replace("bin\\", "");
        StreamWriter logW;
        StreamReader logR;
        TimeSpan tsi;
        TimeSpan tsf;
        TimeSpan day = TimeSpan.Parse("23:59:59.999");

        private void Form1_Load(object sender, EventArgs e)
        {

            //DateTimePicker
            dtpInicio.Format = DateTimePickerFormat.Custom;
            dtpInicio.CustomFormat = "HH : mm";
            dtpFin.Format = DateTimePickerFormat.Custom;
            dtpFin.CustomFormat = "HH : mm";
            //Checa log
            writeLog("Aplicaci�n iniciada ");
            btnIniciar.Enabled = false;
            btnDetener.Enabled = false;
            lblConectado.Text = "";
            txtPath.Text = @"C:\";
            savePath = txtPath.Text;
            btnDetalle.Enabled = false;
            Connect();

        }

        #region Control

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            start = true;
            picLight.Image = Properties.Resources.green;
            btnIniciar.Enabled = false;
            dtpInicio.Enabled = false;
            dtpFin.Enabled = false;
            SqlConnection con911 = new SqlConnection(rd.GetValue("DB911ConnectionString", typeof(string)).ToString());

            if (dgxProcesar.Rows.Count <= 0) MessageBox.Show("No hay tareas pendientes.", "GeneradorDBF", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (bgwMake.IsBusy)
            {
                MessageBox.Show("El proceso ya est� en ejecuci�n.\nEspere a que termine o use el bot�n Detener", "GeneradorDBF", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                bgwMake.WorkerReportsProgress = true;
                bgwMake.RunWorkerAsync(con911);
            }
        }

        private void btnDetener_Click(object sender, EventArgs e)
        {
            start = false;
            picLight.Image = Properties.Resources.red;


        }
        #endregion

        private void btnLog_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = "notepad.exe";
            proc.StartInfo.Arguments = appPath + @"log\log.txt";
            proc.Start();
        }

        void updateGridViews()
        {
            SqlConnection conMaster = new SqlConnection(rd.GetValue("SEPMasterConnectionString", typeof(string)).ToString());
            CheckForIllegalCrossThreadCalls = false;
            //Por procesar
            SqlTransaction tran = null;
            solDP = VW_SolicituddbfMD.Load_VW_SolicituddbfDP(conMaster, tran, 0, 1);
            dgxProcesar.DataSource = solDP;

            //Terminadas
            solDP = VW_SolicituddbfMD.Load_VW_SolicituddbfDP(conMaster, tran, 0, 3);
            dgTerminadas.DataSource = solDP;
            conMaster.Dispose();
            CheckForIllegalCrossThreadCalls = true;

        }

        #region Connect
        void Connect()
        {
            SqlConnection conMaster = new SqlConnection(rd.GetValue("SEPMasterConnectionString", typeof(string)).ToString());
            picConexion.Image = Properties.Resources.loading;
            bgwConnect.RunWorkerAsync(conMaster);

        }

        private void bgwConnect_DoWork(object sender, DoWorkEventArgs e)
        {

            CheckForIllegalCrossThreadCalls = false;
            SqlConnection conMaster = (SqlConnection)e.Argument;
            lblConectado.Text = "Conectando a: \n" + conMaster.Database;
            try
            {
                conMaster.Open();
                picConexion.Image = Properties.Resources.accept;
                lblConectado.Text = "Conectado a: \n" + conMaster.Database;
                message = "Conectado a: " + conMaster.Database + " en " + conMaster.DataSource;
                writeLog(message);
                message += "\n" + "Si desea reconectar presione Aceptar.";
                btnDetener.Enabled = true;
                btnIniciar.Enabled = true;
                conMaster.Close();



            }
            catch (Exception ex)
            {
                lblConectado.Text = "No se pudo conectar a: \n" + conMaster.Database;
                btnDetalle.Show();
                message = ex.Message;
                message += "\n" + "Verifique la configuraci�n de conexi�n y presione Aceptar para reconectar.";
                picConexion.Image = Properties.Resources.Error;


            }
            conMaster.Dispose();
            CheckForIllegalCrossThreadCalls = true;
        }

        private void bgwConnect_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (btnIniciar.Enabled)
            {
                updateGridViews();
            }
            bgwConnect.Dispose();
            btnDetalle.Enabled = true;
        }

        private void btnDetalle_Click(object sender, EventArgs e)
        {

            DialogResult diag = MessageBox.Show(message, "Detalles", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (diag == DialogResult.OK && bgwConnect.IsBusy != true)
            {

                picConexion.Image = Properties.Resources.loading;
                btnDetalle.Enabled = false;
                Connect();
            }
        }
        #endregion

        #region Make DBF

        private void bgwMake_DoWork(object sender, DoWorkEventArgs e)
        {
            SqlConnection con911 = (SqlConnection)e.Argument;
            SqlConnection conMaster = new SqlConnection(rd.GetValue("SEPMasterConnectionString", typeof(string)).ToString());
            VW_SolicituddbfDP[] tranDP;

        WakeUp:
            while (start)
            {
                if (tsi.TotalMilliseconds > tsf.TotalMilliseconds)
                {
                    tsf = tsf + day;
                }

                if ((DateTime.Now.TimeOfDay.TotalMilliseconds >= tsi.TotalMilliseconds) && (DateTime.Now.TimeOfDay.TotalMilliseconds <= tsf.TotalMilliseconds))
                {
                    #region DO
                    tranDP = VW_SolicituddbfMD.Load_VW_SolicituddbfDP(conMaster, null, 0, 1);

                    if (tranDP.Length > 0)
                    {

                        //Marcar estatus en proceso
                        MakeResult(tranDP[0].Id, "En proceso");
                        //Traer resultado
                        resultado = dbf.Make(con911, tranDP[0].Id_cuestionario, tranDP[0].Id_Entidad, tranDP[0].Id, tranDP[0].Id_cicloEscolar, savePath);
                        bgwMake.ReportProgress(40);
                        //Marcar estatus segun resultado
                        MakeResult(tranDP[0].Id, resultado);

                    }
                    else
                    {
                        //Duerme 5 minutos
                        writeLog("Durmiendo - no tengo tareas pendientes.Vuelvo en 5 minutos");
                        System.Threading.Thread.Sleep(30000);
                        writeLog("Despertando");
                        goto WakeUp;
                    }
                    if (!start)
                    {
                        break;
                    }
                    if (closeRequest)
                    {
                        Close();
                    }
                    #endregion
                }
                else
                {
                    #region Si el tiempo actual es menor al tiempo inicial:
                    if (DateTime.Now.TimeOfDay.TotalMilliseconds < tsi.TotalMilliseconds)
                    {
                        writeLog("Durmiendo - No estoy en horario de trabajo.");
                        double m = tsi.TotalMilliseconds - DateTime.Now.TimeOfDay.TotalMilliseconds;
                        double i = 0;
                        while (i <= m && start)
                        {
                            System.Threading.Thread.Sleep(30000);
                            i = i + 30000;
                        }
                        if (!start)
                        {
                            break;
                        }
                        if (closeRequest)
                        {
                            Close();
                        }
                        writeLog("Despertando");
                        goto WakeUp;
                    }
                    #endregion
                    #region Si el tiempo actual es mayor al tiempo final:
                    if (DateTime.Now.TimeOfDay.TotalMilliseconds > tsf.TotalMilliseconds)
                    {
                        writeLog("Durmiendo - No estoy en horario de trabajo.");
                        double m = (tsi.TotalMilliseconds + day.TotalMilliseconds) - DateTime.Now.TimeOfDay.TotalMilliseconds;
                        double i = 0;
                        while (i <= m && start)
                        {
                            System.Threading.Thread.Sleep(30000);
                            i = i + 30000;
                        }
                        if (!start)
                        {
                            break;
                        }
                        if (closeRequest)
                        {
                            Close();
                        }
                        writeLog("Despertando");
                        goto WakeUp;
                    }
                    #endregion
                }
            }

        }

        void MakeResult(int idsolicitud, string result)
        {
            SqlConnection conMaster = new SqlConnection(rd.GetValue("SEPMasterConnectionString", typeof(string)).ToString());
           
            if (result.Contains("Error"))
            {
                soldbfDP = SolicituddbfMD.Load(conMaster, null, idsolicitud);
                soldbfDP.Id_estadosolicitud = 5;
                solMD = new SolicituddbfMD(soldbfDP);
                solMD.Update(conMaster, null);
                //updateGridViews();
                writeLog(result + " en la solicitud " + idsolicitud);
            }
            if (result.Contains("En proceso"))
            {
                soldbfDP = SolicituddbfMD.Load(conMaster, null, idsolicitud);
                soldbfDP.Id_estadosolicitud = 2;
                solMD = new SolicituddbfMD(soldbfDP);
                solMD.Update(conMaster, null);
                //updateGridViews();
            }
            if (result.Contains("zip"))
            {
                soldbfDP = SolicituddbfMD.Load(conMaster, null, idsolicitud);
                soldbfDP.Id_estadosolicitud = 3;
                soldbfDP.Archivo = result;
                solMD = new SolicituddbfMD(soldbfDP);
                solMD.Update(conMaster, null);
                //updateGridViews();
                writeLog("Solicitud " + idsolicitud + " terminada." + result);
            }
            bgwMake.ReportProgress(30);

        }

        private void bgwMake_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (progressPercent >= 100)
            {
                progressPercent = 0;
                pbProgress.Value = 0;
            }
            progressPercent += e.ProgressPercentage;
            pbProgress.Increment(progressPercent);
            pbProgress.Update();
            updateGridViews();
        }

        private void bgwMake_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Reset el progress
            pbProgress.Value = 0;
            progressPercent = 0;
            //
            picLight.Image = Properties.Resources.red;
            btnIniciar.Enabled = true;
            dtpInicio.Enabled = true;
            dtpFin.Enabled = true;
        }
        #endregion

        #region Set Save Path
        private void txtPath_MouseClick(object sender, MouseEventArgs e)
        {

            folderBrowserDialog1.ShowDialog();
            txtPath.Text = folderBrowserDialog1.SelectedPath;
            savePath = txtPath.Text;
        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            txtPath.Text = folderBrowserDialog1.SelectedPath;
            savePath = txtPath.Text;

        }
        #endregion

        #region Ocultar/Mostrar Tray Icon

        private void btnOcultar_Click(object sender, EventArgs e)
        {
            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(500, "Generador DBF", "Generador DBF", ToolTipIcon.Info);
            notifyIcon.Icon = Properties.Resources.database_down;
            WindowState = FormWindowState.Minimized;
            Hide();

        }


        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            notifyIcon.Visible = false;
            Show();
            WindowState = FormWindowState.Normal;


        }

        private void mostrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notifyIcon.Visible = false;
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            notifyIcon.Visible = false;
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (bgwMake.IsBusy)
            {
                DialogResult diag = MessageBox.Show("Actualmente hay tareas en ejecuci�n,si desea salir presione Aceptar. \nGeneradorDBF terminar� la tarea en proceso y cerrar� autom�ticamente.  ", "Cerrar GeneradorDBF", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (diag == DialogResult.OK)
                {
                    closeRequest = true;
                }
            }
            else
            {
                Close();
            }
        }

        void writeLog(string messageLog)
        {
            if (!Directory.Exists(appPath + "log"))
            {
                Directory.CreateDirectory(appPath + "log");
            }
            if (!File.Exists(appPath + @"log\log.txt"))
            {
                logW = new StreamWriter(appPath + @"log\log.txt");
                logW.Write("");
                logW.Close();
            }

            logR = new StreamReader(appPath + @"log\log.txt");
            string msg = logR.ReadToEnd();
            logR.Close();
            logW = new StreamWriter(appPath + @"log\log.txt");
            logW.Write(msg);
            logW.WriteLine(DateTime.Now + " " + messageLog);
            logW.Close();
        }

        private void dtpInicio_ValueChanged(object sender, EventArgs e)
        {
            tsi = TimeSpan.Parse(dtpInicio.Text.Replace(" ", ""));
        }

        private void dtpFin_ValueChanged(object sender, EventArgs e)
        {
            tsf = TimeSpan.Parse(dtpFin.Text.Replace(" ", ""));
        }


        private void dtpInicio_KeyDown(object sender, KeyEventArgs e)
        {
            #region Keys
            switch (e.KeyCode)
            {
                case Keys.Up:
                    contra += "u";
                    break;
                case Keys.Down:
                    contra += "d";
                    break;
                case Keys.Left:
                    contra += "l";
                    break;
                case Keys.Right:
                    contra += "r";
                    break;
                case Keys.A:
                    contra += "A";
                    break;
                case Keys.B:
                    contra += "B";
                    break;
                case Keys.Enter:
                    contra += "S";
                    break;
                default:
                    contra = "";
                    break;
            }
            #endregion

            if (contra.Length == 11)
            {
                if (contra == "uuddlrlrABS")
                {
                    Form cmsg = new Form();
                    Label msg = new Label();
                    PictureBox pic = new PictureBox();

                    msg.AutoSize = true;
                    msg.Text = "Desarrollado por:\nSantiago Garc�a\ny\nGelen Manzanarez";
                    msg.Font = new Font("Microsoft Sans Serif",8,FontStyle.Bold);
                    
                    pic.Location = new Point(150,0);
                    pic.Image = Properties.Resources.oh;
                    pic.SizeMode = PictureBoxSizeMode.Normal;
                    pic.AutoSize = true;
                    
                    cmsg.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                    cmsg.AutoSize = true;
                    cmsg.Size = new Size(160, 90);
                    cmsg.BackColor = Color.White;
                    cmsg.Icon = Properties.Resources.database_down;
                    cmsg.Controls.Add(pic);
                    cmsg.Controls.Add(msg);
                    cmsg.ShowDialog();
                    
                    contra = "";
                }
                else
                {
                    contra = "";
                }
                
                
            }
        }
    }
}