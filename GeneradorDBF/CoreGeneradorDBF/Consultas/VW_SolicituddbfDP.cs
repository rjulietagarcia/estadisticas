using System.Xml.Serialization;

namespace CoreGeneradorDBF.Consultas
{
    [XmlRoot("VW_SolicituddbfDP")]
    public class VW_SolicituddbfDP
    {
        private int id;
        private int id_Entidad;
        private string cuestionario;
        private string tipo;
        private string estatus;
        private string archivo;
        private string usuario;
        private string fechaSolicitud;
        private int id_cuestionario;
        private int id_cicloEscolar;

        [XmlElement("Id_cicloEscolar")]
        public int Id_cicloEscolar
        {
            get { return id_cicloEscolar; }
            set { id_cicloEscolar = value; }
        }

        [XmlElement("Id_cuestionario")]
        public int Id_cuestionario
        {
            get { return id_cuestionario; }
            set { id_cuestionario = value; }
        }

        [XmlElement("Archivo")]
        public string Archivo
        {
            get { return archivo; }
            set { archivo = value; }
        }

        [XmlElement("FechaSolicitud")]
        public string FechaSolicitud
        {
            get { return fechaSolicitud; }
            set { fechaSolicitud = value; }
        }

        [XmlElement("Usuario")]
        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }


        [XmlElement("Estatus")]
        public string Estatus
        {
            get { return estatus; }
            set { estatus = value; }
        }
        [XmlElement("Tipo")]
        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        [XmlElement("Cuestionario")]
        public string Cuestionario
        {
            get { return cuestionario; }
            set { cuestionario = value; }
        }

        [XmlElement("Id_Entidad")]
        public int Id_Entidad
        {
            get { return id_Entidad; }
            set { id_Entidad = value; }
        }

        [XmlElement("Id")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}
