using System;
using System.Text;
using System.Xml.Serialization;

namespace CoreGeneradorDBF.Consultas
{
	[ XmlRoot( "EstadoSolicitud" ) ]
	public class EstadoSolicitudDP
	{
		[ XmlElement( "Id" ) ]
		public int Id
		{
			get { return id; }
			set { id = value; }
		}
		[ XmlElement( "Nombre" ) ]
		public string Nombre
		{
			get { return nombre; }
			set { nombre = value; }
		}
		[ XmlElement( "BitActivo" ) ]
		public bool BitActivo
		{
			get { return bit_activo; }
			set { bit_activo = value; }
		}
		[ XmlElement( "IdUsuario" ) ]
		public int IdUsuario
		{
			get { return id_usuario; }
			set { id_usuario = value; }
		}
		[ XmlElement( "FechaActualizacion" ) ]
		public string FechaActualizacion
		{
			get { return fecha_actualizacion; }
			set { fecha_actualizacion = value; }
		}

		private int 	id;
		private string 	nombre;
		private bool 	bit_activo;
		private int 	id_usuario;
		private string 	fecha_actualizacion;
	}
}
