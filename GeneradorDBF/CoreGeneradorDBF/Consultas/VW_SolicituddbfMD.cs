using System;
using System.Data.Common;
using System.Text;
using System.Data.SqlClient;
using System.Collections;

namespace CoreGeneradorDBF.Consultas
{
    public class VW_SolicituddbfMD
    {
        private VW_SolicituddbfDP SolicituddbfDP = null;

        public VW_SolicituddbfMD(VW_SolicituddbfDP solicituddbfDP)
        {
            this.SolicituddbfDP = solicituddbfDP;
        }

        public static VW_SolicituddbfDP[] Load_VW_SolicituddbfDP(SqlConnection con, SqlTransaction tran, int id_cuestionario,int id_estadoSolicitud)
        {
            ArrayList list = new ArrayList();
            SqlCommand com;
            StringBuilder str = new StringBuilder();
            str.AppendLine("SELECT    s.Id,");
            str.AppendLine("s.Id_Entidad,");
            str.AppendLine("t.Descripcion AS Cuestionario,");
            str.AppendLine("CASE s.Id_TipoCuestionario");
            str.AppendLine("WHEN 1 THEN 'Inicio'");
            str.AppendLine("WHEN 2 THEN 'Fin'");
            str.AppendLine("END AS Tipo,");
            str.AppendLine("s.Archivo,");
            str.AppendLine("e.Nombre AS Estatus,");
            str.AppendLine("u.[Login] AS Usuario,");
            str.AppendLine("FechaAlta AS FechaSolicitud,");
            str.AppendLine("t.ID_Cuestionario,");
            str.AppendLine("s.ID_CicloEscolar");
            str.AppendLine("FROM    SolicitudDBF s,");
            str.AppendLine("EstadoSolicitud e,");
            str.AppendLine("usuario u,");
            str.AppendLine("SEP_911.dbo.Tb_cuestionarios T");
            str.AppendLine("WHERE   s.Id_Cuestionario = t.Id_Cuestionario");
            str.AppendLine("AND s.Id_Usuario            = u.Id_Usuario");
            str.AppendLine("AND s.Id_EstadoSolicitud    = e.Id");
            str.AppendLine("AND s.Bit_Activo            = 1");
            //str.AppendLine("AND s.Id_CicloEscolar       = " + cicloEscolar);
            str.AppendLine("AND s.Id_EstadoSolicitud    = " + id_estadoSolicitud);

            if (id_cuestionario > 0)
            {
                str.AppendLine("AND s.Id_cuestionario = " + id_cuestionario);
            }
            //if (id_estadoSolicitud == 1)
            //{
            //    str.AppendLine("OR s.Id_EstadoSolicitud = 3");
            //}

            str.AppendLine("ORDER BY s.Id_EstadoSolicitud, s.Id");

            VW_SolicituddbfDP tmp = new VW_SolicituddbfDP();
            SqlDataReader dr;
            
            com = new SqlCommand(str.ToString(), con);

            if ( tran != null )
			{
				com.Transaction = tran;
			}
            try
            {
                con.Open();
                dr = com.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(read(dr));
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
            
            return (VW_SolicituddbfDP[])list.ToArray(typeof(VW_SolicituddbfDP));
        }

        public static VW_SolicituddbfDP read(DbDataReader dr)
        {
            VW_SolicituddbfDP tmp = new VW_SolicituddbfDP();
            tmp.Id = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            tmp.Id_Entidad = dr.IsDBNull(1) ? 0 : int.Parse(dr.GetInt16(1).ToString());
            tmp.Cuestionario = dr.IsDBNull(2) ? "" : dr.GetString(2);
            tmp.Tipo = dr.IsDBNull(3) ? "" : dr.GetString(3);
            tmp.Archivo = dr.IsDBNull(4) ? "" : dr.GetString(4);
            tmp.Estatus = dr.IsDBNull(5) ? "" : dr.GetString(5);
            tmp.Usuario = dr.IsDBNull(6) ? "" : dr.GetString(6);
            tmp.FechaSolicitud = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToString();
            tmp.Id_cuestionario = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            tmp.Id_cicloEscolar = dr.IsDBNull(9) ? 0 : int.Parse(dr.GetInt16(9).ToString());

            return tmp;
        }
    }
}
