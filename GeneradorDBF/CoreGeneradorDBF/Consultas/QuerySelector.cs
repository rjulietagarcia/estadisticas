using System.Text;

namespace CoreGeneradorDBF.Consultas
{
    public class QuerySelector
    {
        public static string SeleccionaCuestionario(int idCuestionario)
        {
            //NombreTabla|Query

            StringBuilder sqlQuery = new StringBuilder();
            switch (idCuestionario)
            {
                #region Fin de cursos
                case 1:
                    #region Preescolar General
                    sqlQuery.Append("PREEGF12|");
                    sqlQuery.Append(" select  v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, ");
                    sqlQuery.Append("   v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR,v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, ");
                    sqlQuery.Append("   v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, ");
                    sqlQuery.Append("   c.v1, c.v2, c.v3,");
                    sqlQuery.Append("   c.v4, c.v5, c.v6, c.v7, c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, ");
                    sqlQuery.Append("   c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, c.v31, c.v32, ");
                    sqlQuery.Append("   c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, ");
                    sqlQuery.Append("   c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, ");
                    sqlQuery.Append("   c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("   c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, ");
                    sqlQuery.Append("   c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, ");
                    sqlQuery.Append("   c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, ");
                    sqlQuery.Append("   c.v115, c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, ");
                    sqlQuery.Append("   c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, c.v135, c.v136, c.v137, c.v138, ");
                    sqlQuery.Append("   c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, ");
                    sqlQuery.Append("   c.v151, c.v152, c.v153, c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, ");
                    sqlQuery.Append("   c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, c.v173, c.v174, ");
                    sqlQuery.Append("   c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, ");
                    sqlQuery.Append("   c.v187, c.v188, c.v189, c.v190, c.v191, c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, ");
                    sqlQuery.Append("   c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("   c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, ");
                    sqlQuery.Append("   c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, c.v230, c.v231, c.v232, c.v233, c.v234, ");
                    sqlQuery.Append("   c.v235, c.v236, c.v237, c.v238, c.v239  ");
                    sqlQuery.Append(" from  ");
                    sqlQuery.Append("dbo.Tb_CONTROL, ");
                    sqlQuery.Append("Vista_DatosDBF v, ");
                    sqlQuery.Append("VW_Preescolar_G_F c ");
                    sqlQuery.Append("where  ");
                    sqlQuery.AppendLine("Tb_CONTROL.Id_CCTNT = v.Id_CCTNT and  ");
                    sqlQuery.AppendLine("Tb_CONTROL.id_control= c.id_control and  ");
                    sqlQuery.AppendLine("Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                case 2:
                    #region Primaria General
                    sqlQuery.Append("PRIMGF12|");
                    sqlQuery.Append(" Select v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, ");
                    sqlQuery.Append(" v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, ");
                    sqlQuery.Append(" v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, ");

                    sqlQuery.Append("        c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, ");
                    sqlQuery.Append("		 c.v28, c.v29, c.v30, c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, ");
                    sqlQuery.Append("		 c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, ");
                    sqlQuery.Append("		 c.v92, c.v93, c.v94, c.v95, c.v96, c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("		 c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, c.v135, c.v136, c.v137, c.v138, c.v139, ");
                    sqlQuery.Append("		 c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, ");
                    sqlQuery.Append("		 c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187,  ");
                    sqlQuery.Append("		 c.v188, c.v189, c.v190, c.v191, c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219,  ");
                    sqlQuery.Append("		 c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, c.v249, c.v250, c.v251,  ");
                    sqlQuery.Append("		 c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283,  ");
                    sqlQuery.Append("		 c.v284, c.v285, c.v286, c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315,  ");
                    sqlQuery.Append("		 c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339,  ");
                    sqlQuery.Append("		 c.v340, c.v341, c.v342, c.v343, c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371,  ");
                    sqlQuery.Append("		 c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, c.v401, c.v402, c.v403,  ");
                    sqlQuery.Append("		 c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435,  ");
                    sqlQuery.Append("		 c.v436, c.v437, c.v438, c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467,  ");
                    sqlQuery.Append("		 c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, c.v496, c.v497, c.v498, c.v499,  ");
                    sqlQuery.Append("		 c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, c.v534, c.v535, c.v536, c.v537, c.v538, c.v539,  ");
                    sqlQuery.Append("		 c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571,  ");
                    sqlQuery.Append("		 c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603,  ");
                    sqlQuery.Append("		 c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, c.v610, c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626, c.v627, c.v628, c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635,  ");
                    sqlQuery.Append("		 c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, c.v643, c.v644, c.v645, c.v646, c.v647, c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, c.v665, c.v666, c.v667, c.v668, c.v669, ");
                    sqlQuery.Append("        c.v670, c.v671, c.v672, c.v673, c.v674, c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, c.v681, c.v682, c.v683  ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF v, ");
                    sqlQuery.Append("  VW_Primaria_G_F c ");
                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");
                    #endregion
                    break;
                case 3:
                    #region Secundaria
                    sqlQuery.Append("SECUNF12|");
                    sqlQuery.Append("select  v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, ");
                    sqlQuery.Append("v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, ");
                    sqlQuery.Append("v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, ");
                    sqlQuery.Append("		 c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26,  ");
                    sqlQuery.Append("		 c.v27, c.v28, c.v29, c.v30, c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, c.v53, c.v54, c.v55, c.v56, c.v57, c.v58,  ");
                    sqlQuery.Append("		 c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90,  ");
                    sqlQuery.Append("		 c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114,  ");
                    sqlQuery.Append("		 c.v115, c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134,  ");
                    sqlQuery.Append("		 c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, c.v154,  ");
                    sqlQuery.Append("		 c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, c.v173, c.v174,  ");
                    sqlQuery.Append("		 c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, c.v192, c.v193, c.v194,  ");
                    sqlQuery.Append("		 c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, c.v211, c.v212, c.v213, c.v214,  ");
                    sqlQuery.Append("		 c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, c.v230, c.v231, c.v232, c.v233, c.v234,  ");
                    sqlQuery.Append("		 c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, c.v249, c.v250, c.v251, c.v252, c.v253, c.v254,  ");
                    sqlQuery.Append("		 c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274,  ");
                    sqlQuery.Append("		 c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294,  ");
                    sqlQuery.Append("		 c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314,  ");
                    sqlQuery.Append("		 c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334,  ");
                    sqlQuery.Append("		 c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354,  ");
                    sqlQuery.Append("		 c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374,  ");
                    sqlQuery.Append("		 c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394,  ");
                    sqlQuery.Append("		 c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414,  ");
                    sqlQuery.Append("		 c.v415, c.v416, c.v417, c.v418, c.v419, c.v420, c.v421, c.v422, c.v423, c.v424, c.v426, c.v427, c.v428, c.v429, c.v430, c.v425  ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_Secundaria_F c ");
                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");
                    #endregion
                    break;
                case 4:
                    #region Profesional T�cnico
                    sqlQuery.Append("PROF1F12|");
                    sqlQuery.Append("SELECT  v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("         v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("         v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, (SELECT     COUNT(id_Carrera) FROM Tb_Carreras WHERE (id_Control = Tb_CONTROL.ID_Control)) AS TOTALCARR, ");
                    sqlQuery.Append("         c.v1,c.v2,c.v3,c.v4,c.v5,c.v6,c.v7,c.v8,c.v9,c.v10,c.v11,c.v12,c.v13,c.v14,c.v15,c.v16,c.v17,c.v18,c.v19,c.v20,c.v21,c.v22,c.v23,c.v24,c.v25,c.v26,c.v27,c.v28,c.v29,c.v30,c.v31,c.v32,c.v33,c.v34,c.v35,c.v36,c.v37,  ");
                    sqlQuery.Append("         c.v38,c.v39,c.v40,c.v41,c.v42,c.v43,c.v44,c.v45,c.v46,c.v47,c.v48,c.v49,c.v50,c.v51,c.v52,c.v53,c.v54,c.v55,c.v56,c.v57,c.v58,c.v59,c.v60,c.v61,c.v62,c.v63,c.v64,c.v65,c.v66,c.v67,c.v68,c.v69,c.v70,c.v71,c.v72,  ");
                    sqlQuery.Append("         c.v73,c.v74,c.v75,c.v494,c.v495,c.v496,c.v497,c.v498,c.v499,c.v76,c.v77,c.v78,c.v79,c.v80,c.v81,c.v82,c.v83,c.v84,c.v85,c.v86,c.v87,c.v88,c.v89,c.v90,c.v91,c.v92,c.v93,c.v94,c.v95,c.v96,c.v97,c.v98,c.v99,c.v100,c.v101,c.v102,c.v103,c.v104,c.v105,c.v106,  ");
                    sqlQuery.Append("         c.v107,c.v108,c.v109,c.v110,c.v111,c.v112,c.v113,c.v114,c.v115,c.v116,c.v117,c.v118,c.v119,c.v120,c.v121,c.v122,c.v123,c.v124,c.v125,c.v126,c.v127,c.v128,c.v129,c.v130,c.v131,c.v132,c.v133,c.v134,c.v135,  ");
                    sqlQuery.Append("         c.v136,c.v137,c.v138,c.v139,c.v140,c.v141,c.v142,c.v143,c.v144,c.v145,c.v146,c.v147,c.v148,c.v149,c.v150,c.v151,c.v152,c.v153,c.v154,c.v155,c.v156,c.v157,c.v158,c.v159,c.v160,c.v161,c.v162,c.v163,c.v164,  ");
                    sqlQuery.Append("         c.v165,c.v166,c.v167,c.v168,c.v169,c.v170,c.v171,c.v172,c.v173,c.v174,c.v175,c.v176,c.v177,c.v178,c.v179,c.v180,c.v181,c.v182,c.v183,c.v184,c.v185,c.v186,c.v187,c.v188,c.v189,c.v190,c.v191,c.v192,c.v193,  ");
                    sqlQuery.Append("         c.v194,c.v195,c.v196,c.v197,c.v198,c.v199,c.v200,c.v201,c.v202,c.v203,c.v204,c.v205,c.v206,c.v207,c.v208,c.v209,c.v210,c.v211,c.v212,c.v213,c.v214,c.v215,c.v216,c.v217,c.v218,c.v219,c.v220,c.v221,c.v222,  ");
                    sqlQuery.Append("         c.v223,c.v224,c.v225,c.v226,c.v227,c.v228,c.v229,c.v230,c.v231,c.v232,c.v233,c.v234,c.v235,c.v236,c.v237,c.v238,c.v239,c.v240,c.v241,c.v242,c.v243,c.v244,c.v245,c.v246,c.v247,c.v248,c.v249,c.v250,c.v251,  ");
                    sqlQuery.Append("         c.v252,c.v253,c.v254,c.v255,c.v256,c.v257,c.v258,c.v259,c.v260,c.v261,c.v262,c.v263,c.v264,c.v265,c.v266,c.v267,c.v268,c.v269,c.v270,c.v271,c.v272,c.v273,c.v274,c.v275,c.v276,c.v277,c.v278,c.v279,c.v280,  ");
                    sqlQuery.Append("         c.v281,c.v282,c.v283,c.v284,c.v285,c.v286,c.v287,c.v288,c.v289,c.v290,c.v291,c.v292,c.v293,c.v294,c.v295,c.v296,c.v297,c.v298,c.v299,c.v300,c.v301,c.v302,c.v303,c.v304,c.v305,c.v306,c.v307,c.v308,c.v309,  ");
                    sqlQuery.Append("         c.v310,c.v311,c.v312,c.v313,c.v314,c.v315,c.v316,c.v317,c.v318,c.v319,c.v320,c.v321,c.v322,c.v323,c.v324,c.v325,c.v326,c.v327,c.v328,c.v329,c.v330,c.v331,c.v332,c.v333,c.v334,c.v335,c.v336,c.v337,c.v338,  ");
                    sqlQuery.Append("         c.v339,c.v340,c.v341,c.v342,c.v343,c.v344,c.v345,c.v346,c.v347,c.v348,c.v349,c.v350,c.v351,c.v352,c.v353,c.v354,c.v355,c.v356,c.v357,c.v358,c.v359,c.v360,c.v361,c.v362,c.v363,c.v364,c.v365,c.v366,c.v367,  ");
                    sqlQuery.Append("         c.v368,c.v369,c.v370,c.v371,c.v372,c.v373,c.v374,c.v375,c.v376,c.v377,c.v378,c.v379,c.v380,c.v381,c.v382,c.v383,c.v384,c.v385,c.v386,c.v387,c.v388,c.v389,c.v390,c.v391,c.v392,c.v393,c.v394,c.v395,c.v396,  ");
                    sqlQuery.Append("         c.v397,c.v398,c.v399,c.v400,c.v401,c.v402,c.v403,c.v404,c.v405,c.v406,c.v407,c.v408,c.v409,c.v410,c.v411,c.v412,c.v413,c.v414,c.v415,c.v416,c.v417,c.v418,c.v419,c.v420,c.v421,c.v422,c.v423,c.v424,c.v425,  ");
                    sqlQuery.Append("         c.v426,c.v427,c.v428,c.v429,c.v430,c.v431,c.v432,c.v433,c.v434,c.v435,c.v436,c.v437,c.v438,c.v439,c.v440,c.v441,c.v442,c.v443,c.v444,c.v445,c.v446,c.v447,c.v448,c.v449,c.v450,c.v451,c.v452,c.v453,c.v454,  ");
                    sqlQuery.Append("         c.v455,c.v456,c.v457,c.v458,c.v459,c.v460,c.v461,c.v462,c.v463,c.v464,c.v465,c.v466,c.v467,c.v468,c.v469,c.v470,c.v471,c.v472,c.v473,c.v474,c.v475,c.v476,c.v477,c.v478,c.v479,c.v480,c.v481,c.v482,c.v483,  ");
                    sqlQuery.Append("         c.v505,c.v506,c.v507,c.v508,c.v509,c.v510,c.v511,c.v512,  ");
                    sqlQuery.Append("         c.v513,c.v514,c.v515,c.v516,c.v517,c.v518,c.v519,c.v520,c.v521,c.v522,c.v523,c.v524,c.v525,c.v526,c.v527,c.v528,c.v529,c.v530,c.v531,c.v532,c.v533,c.v534,c.v535,c.v536,c.v537,c.v538,c.v539,c.v540,c.v541,  ");
                    sqlQuery.Append("         c.v542,c.v543,c.v484,c.v485,c.v486,c.v487,c.v488,c.v489,c.v490,c.v491,c.v492,c.v500,c.v501,c.v502,c.v503,c.v504,c.v493  ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_8P_Fin c ");
                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");
                    #endregion
                    break;
                case 5:
                    #region Bachillerato General
                    sqlQuery.Append("BACHGF12|");
                    sqlQuery.Append("SELECT  v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, (SELECT     COUNT(id_Carrera) FROM Tb_Carreras WHERE (id_Control = Tb_CONTROL.ID_Control)) AS TOTALCARR, ");
                    sqlQuery.Append("		 c.v1,c.v2,c.v3,c.v4,c.v5,c.v6,c.v7,c.v8,c.v9,c.v10,c.v11,c.v12,c.v13,c.v14,c.v15,c.v16,c.v17,c.v18,c.v19,c.v20,c.v21,c.v22,c.v23,c.v24,c.v25,c.v26,c.v27,c.v28,c.v29,   ");
                    sqlQuery.Append("		 c.v30,c.v31,c.v32,c.v33,c.v34,c.v35,c.v36,c.v37,c.v38,c.v39,c.v40,c.v41,c.v42,c.v43,c.v44,c.v45,c.v46,c.v47,c.v48,c.v49,c.v50,c.v51,c.v52,c.v53,c.v54,c.v55,c.v56,   ");
                    sqlQuery.Append("		 c.v57,c.v58,c.v59,c.v60,c.v61,c.v62,c.v63,c.v64,c.v65,c.v66,c.v67,c.v68,c.v69,c.v70,c.v71,c.v72,c.v73,c.v74,c.v75,c.v76,c.v77,c.v78,c.v79,c.v80,c.v81,c.v82,c.v83,   ");
                    sqlQuery.Append("		 c.v84,c.v85,c.v86,c.v87,c.v88,c.v89,c.v90,c.v91,c.v92,c.v93,c.v94,c.v95,c.v96,c.v97,c.v98,c.v99,c.v100,c.v101,c.v102,c.v103,c.v104,c.v105,c.v106,c.v107,c.v108,   ");
                    sqlQuery.Append("		 c.v109,c.v110,c.v111,c.v112,c.v113,c.v114,c.v115,c.v116,c.v117,c.v118,c.v119,c.v120,c.v121,c.v122,c.v123,c.v124,c.v125,c.v126,c.v127,c.v128,c.v129,c.v130,   ");
                    sqlQuery.Append("		 c.v131,c.v132,c.v133,c.v134,c.v135,c.v136,c.v137,c.v138,c.v139,c.v140,c.v141,c.v142,c.v143,c.v144,c.v145,c.v146,c.v147,c.v148,c.v149,c.v150,c.v151,c.v152,   ");
                    sqlQuery.Append("		 c.v153,c.v154,c.v155,c.v156,c.v157,c.v158,c.v159,c.v160,c.v161,c.v162,c.v163,c.v164,c.v165,c.v166,c.v167,c.v168,c.v169,c.v170,c.v171,c.v172,c.v173,c.v174,   ");
                    sqlQuery.Append("		 c.v175,c.v176,c.v177,c.v178,c.v179,c.v180,c.v181,c.v182,c.v183,c.v184,c.v185,c.v186,c.v187,c.v188,c.v189,c.v190,c.v191,c.v192,c.v193,c.v194,c.v195,c.v196,   ");
                    sqlQuery.Append("		 c.v197,c.v198,c.v199,c.v200,c.v201,c.v202,c.v203,c.v204,c.v205,c.v206,c.v207,c.v208,c.v209,c.v210,c.v211,c.v212,c.v213,c.v214,c.v215,c.v216,c.v217,c.v218,   ");
                    sqlQuery.Append("		 c.v219,c.v220,c.v221,c.v222,c.v223,c.v224,c.v225,c.v226,c.v227,c.v228,c.v229,c.v230,c.v231,c.v232,c.v233,c.v234,c.v235,c.v236,c.v237,c.v238,c.v239,c.v240,   ");
                    sqlQuery.Append("		 c.v241,c.v242,c.v243,c.v244,c.v245,c.v246,c.v247,c.v248,c.v249,c.v250,c.v251,c.v252,c.v253,c.v254,c.v255,c.v256,c.v257,c.v258,c.v259,c.v260,c.v261,c.v262,   ");
                    sqlQuery.Append("		 c.v263,c.v264,c.v265,c.v266,c.v267,c.v268,c.v269,c.v270,c.v271,c.v272,c.v273,c.v274,c.v275,c.v276,c.v277,c.v278,c.v279,c.v280,c.v281,c.v282,c.v283,c.v284,   ");
                    sqlQuery.Append("		 c.v285,c.v286,c.v287,c.v288,c.v289,c.v290,c.v291,c.v292,c.v293,c.v294,c.v295,c.v296,c.v297,c.v298,c.v299,c.v300,c.v301,c.v302,c.v303,c.v304,c.v305,c.v306,   ");
                    sqlQuery.Append("		 c.v307,c.v308,c.v309,c.v310,c.v311,c.v312,c.v313,c.v314,c.v315,c.v316,c.v317,c.v318,c.v319,c.v320,c.v321,c.v322,c.v323,c.v324,c.v325,c.v326,c.v327,c.v328,   ");
                    sqlQuery.Append("		 c.v329,c.v330,c.v331,c.v332,c.v333,c.v334,c.v335,c.v336,c.v337,c.v338,c.v339,c.v340,c.v341,c.v342,c.v343,c.v344,c.v345,c.v346,c.v347,c.v348,c.v349,c.v350,   ");
                    sqlQuery.Append("		 c.v351,c.v352,c.v353,c.v354,c.v355,c.v356,c.v357,c.v358,c.v359,c.v360,c.v361,c.v362,c.v363,c.v364,c.v365,c.v366,c.v367,c.v368,c.v369,c.v370,c.v371,c.v372,   ");
                    sqlQuery.Append("		 c.v373,c.v374,c.v375,c.v376,c.v377,c.v378,c.v379,c.v380,c.v381,c.v382,c.v383,c.v384,c.v385,c.v386,c.v387,c.v388,c.v389,c.v390,c.v391,c.v392,c.v393,c.v394,   ");
                    sqlQuery.Append("		 c.v395,c.v396,c.v397,c.v398,c.v399,c.v400,c.v401,c.v402,c.v403,c.v404,c.v405,c.v406,c.v407,c.v408,c.v409,c.v410,c.v411,c.v412,c.v413,c.v414,c.v415,c.v416,   ");
                    sqlQuery.Append("		 c.v417,c.v418,c.v419,c.v420,c.v421,c.v422,c.v423,c.v424,c.v425,c.v426,c.v427,c.v428,c.v429,c.v430,c.v431,c.v432,c.v433,c.v434,c.v435,c.v436,c.v437,c.v438,   ");
                    sqlQuery.Append("		 c.v439,c.v440,c.v441,c.v442,c.v443,c.v444,c.v445,c.v446,c.v447,c.v448,c.v449,c.v450,c.v451,c.v452,c.v453,c.v454,c.v500,c.v501,c.v502,c.v503,c.v504, ");
                    sqlQuery.Append("		 c.v455,c.v456,c.v457,c.v458,c.v459,c.v460,c.v461,c.v462,c.v463,c.v464,c.v465,c.v466,c.v467,c.v468,c.v469,c.v470,c.v471,c.v472,c.v473,c.v474,c.v475,c.v476, ");
                    sqlQuery.Append("		 c.v477,c.v478,c.v479,c.v480,c.v481,c.v482,c.v483,c.v484,c.v485,c.v486,c.v487,c.v488,c.v489,c.v490,c.v491,c.v492,c.v493,c.v494,c.v495,c.v496,c.v497,c.v498,c.v499  ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_8G_Fin c ");
                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");
                    #endregion
                    break;
                case 6:
                    #region Bachillerato Tecnol�gico
                    sqlQuery.Append("BACH1F12|");
                    sqlQuery.Append("SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO,MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, (SELECT COUNT(ID_CARRERA) FROM TB_CARRERAS WHERE TB_CARRERAS.ID_CONTROL = TB_CONTROL.ID_CONTROL) AS TOTALCARR, ");
                    sqlQuery.Append("			c.v1,c.v2,c.v3,c.v4,c.v5,c.v6,c.v7,c.v8,c.v9,c.v10,c.v11,c.v12,c.v13,c.v14,c.v15,c.v16,c.v17,c.v18,c.v19,c.v20,c.v21,c.v22,c.v23,c.v24,c.v25,c.v26,c.v27,c.v28,c.v29,c.v30,  ");
                    sqlQuery.Append("			c.v31,c.v32,c.v33,c.v34,c.v35,c.v36,c.v37,c.v38,c.v39,c.v40,c.v41,c.v42,c.v43,c.v44,c.v45,c.v46,c.v47,c.v48,c.v49,c.v50,c.v51,c.v52,c.v53,c.v54,c.v55,c.v56,c.v57,c.v58,  ");
                    sqlQuery.Append("			c.v59,c.v60,c.v61,c.v62,c.v63,c.v64,c.v65,c.v66,c.v67,c.v68,c.v69,c.v70,c.v71,c.v72,c.v73,c.v74,c.v75,c.v76,c.v77,c.v78,c.v79,c.v80,c.v81,c.v82,c.v83,c.v84,c.v85,c.v86,  ");
                    sqlQuery.Append("			c.v87,c.v88,c.v89,c.v90,c.v91,c.v92,c.v93,c.v94,c.v95,c.v96,c.v97,c.v98,c.v99,c.v100,c.v101,c.v102,c.v103,c.v104,c.v105,c.v106,c.v107,c.v108,c.v109,c.v110,c.v111,  ");
                    sqlQuery.Append("			c.v112,c.v113,c.v114,c.v115,c.v116,c.v117,c.v118,c.v119,c.v120,c.v121,c.v122,c.v123,c.v124,c.v125,c.v126,c.v127,c.v128,c.v129,c.v130,c.v131,c.v132,c.v133,  ");
                    sqlQuery.Append("			c.v134,c.v135,c.v136,c.v137,c.v138,c.v139,c.v140,c.v141,c.v142,c.v143,c.v144,c.v145,c.v146,c.v147,c.v148,c.v149,c.v150,c.v151,c.v152,c.v153,c.v154,c.v155,  ");
                    sqlQuery.Append("			c.v156,c.v157,c.v158,c.v159,c.v160,c.v161,c.v162,c.v163,c.v164,c.v165,c.v166,c.v167,c.v168,c.v169,c.v170,c.v171,c.v172,c.v173,c.v174,c.v175,c.v176,c.v177,  ");
                    sqlQuery.Append("			c.v178,c.v179,c.v180,c.v181,c.v182,c.v183,c.v184,c.v185,c.v186,c.v187,c.v188,c.v189,c.v190,c.v191,c.v192,c.v193,c.v194,c.v195,c.v196,c.v197,c.v198,c.v199,  ");
                    sqlQuery.Append("			c.v200,c.v201,c.v202,c.v203,c.v204,c.v205,c.v206,c.v207,c.v208,c.v209,c.v210,c.v211,c.v212,c.v213,c.v214,c.v215,c.v216,c.v217,c.v218,c.v219,c.v220,c.v221,  ");
                    sqlQuery.Append("			c.v222,c.v223,c.v224,c.v225,c.v226,c.v227,c.v228,c.v229,c.v230,c.v231,c.v232,c.v233,c.v234,c.v235,c.v236,c.v237,c.v238,c.v239,c.v240,c.v241,c.v242,c.v243,  ");
                    sqlQuery.Append("			c.v244,c.v245,c.v246,c.v247,c.v248,c.v249,c.v250,c.v251,c.v252,c.v253,c.v254,c.v255,c.v256,c.v257,c.v258,c.v259,c.v260,c.v261,c.v262,c.v263,c.v264,c.v265,  ");
                    sqlQuery.Append("			c.v266,c.v267,c.v268,c.v269,c.v270,c.v271,c.v272,c.v273,c.v274,c.v275,c.v276,c.v277,c.v278,c.v279,c.v280,c.v281,c.v282,c.v283,c.v284,c.v285,c.v286,c.v287,  ");
                    sqlQuery.Append("			c.v288,c.v289,c.v290,c.v291,c.v292,c.v293,c.v294,c.v295,c.v296,c.v297,c.v298,c.v299,c.v300,c.v301,c.v302,c.v303,c.v304,c.v305,c.v306,c.v307,c.v308,c.v309,  ");
                    sqlQuery.Append("			c.v310,c.v311,c.v312,c.v313,c.v314,c.v315,c.v316,c.v317,c.v318,c.v319,c.v320,c.v321,c.v322,c.v323,c.v324,c.v325,c.v326,c.v327,c.v328,c.v329,c.v330,c.v331,  ");
                    sqlQuery.Append("			c.v332,c.v333,c.v334,c.v335,c.v336,c.v337,c.v338,c.v339,c.v340,c.v341,c.v342,c.v343,c.v344,c.v345,c.v346,c.v347,c.v348,c.v349,c.v350,c.v351,c.v352,c.v353,  ");
                    sqlQuery.Append("			c.v354,c.v355,c.v356,c.v357,c.v358,c.v359,c.v360,c.v361,c.v362,c.v363,c.v364,c.v365,c.v366,c.v367,c.v368,c.v369,c.v370,c.v371,c.v372,c.v373,c.v374,c.v375,  ");
                    sqlQuery.Append("			c.v376,c.v377,c.v378,c.v379,c.v380,c.v381,c.v382,c.v383,c.v384,c.v385,c.v386,c.v387,c.v388,c.v389,c.v390,c.v391,c.v392,c.v393,c.v394,c.v395,c.v396,c.v397,  ");
                    sqlQuery.Append("			c.v398,c.v399,c.v400,c.v401,c.v402,c.v403,c.v404,c.v405,c.v406,c.v407,c.v408,c.v409,c.v410,c.v411,c.v412,c.v413,c.v414,c.v415,c.v416,c.v417,c.v418,c.v419,  ");
                    sqlQuery.Append("			c.v420,c.v421,c.v422,c.v423,c.v424,c.v425,c.v426,c.v427,c.v428,c.v429,c.v430,c.v431,c.v432,c.v433,c.v434,c.v435,c.v436,c.v437,c.v438,c.v439,c.v440,c.v441,  ");
                    sqlQuery.Append("			c.v442,c.v443,c.v444,c.v445,c.v446,c.v447,c.v448,c.v449,c.v450,c.v451,c.v452,c.v453,c.v454,");
                    sqlQuery.Append("			c.v500, c.v501, c.v502, c.v503, c.v504, ");

                    sqlQuery.Append("			c.v455,c.v456,c.v457,c.v458,c.v459,c.v460,c.v461,c.v462,c.v463,  ");
                    sqlQuery.Append("			c.v464,c.v465,c.v466,c.v467,c.v468,c.v469,c.v470,c.v471,c.v472,c.v473,c.v474,c.v475,c.v476,c.v477,c.v478,c.v479,c.v480,c.v481,c.v482,c.v483,c.v484,c.v485,  ");
                    sqlQuery.Append("			c.v486,c.v487,c.v488,c.v489,c.v490,c.v491,c.v492,c.v493,c.v494,c.v495,c.v496,c.v497,c.v498,c.v499   ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_8T_Fin c ");
                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                case 7:
                    #region Preescolar Conafe
                    sqlQuery.Append("PREECF12|");
                    sqlQuery.Append("SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, '' v0, ");
                    sqlQuery.Append("		 c.v1, c.v2, c.v3, c.v40, c.v41, c.v4, c.v5, c.v6, c.v7, c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16,    ");
                    sqlQuery.Append("		 c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, c.v31, c.v32,    ");
                    sqlQuery.Append("        c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39  ");
                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_Preescolar_Conafe_Fin c ");
                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");


                    #endregion
                    break;
                case 8:
                    #region Primaria Conafe

                    sqlQuery.Append("PRIMCF12|");
                    sqlQuery.Append("SELECT  v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("   v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("   v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, '' NIVEL1, '' NIVEL2, '' NIVEL3, '' NIVEL4, '' NIVEL5, '' NIVEL6, ");
                    sqlQuery.Append("	c.v1,c.v1014,c.v1015,c.v2,c.v3,c.v4,c.v5,c.v6,c.v7,c.v8,c.v9,c.v10,c.v11,c.v12,c.v13,c.v14,c.v15,c.v16,c.v17,c.v18,c.v19,c.v20,c.v21,c.v22,c.v23,c.v24,c.v25,c.v26,c.v27,c.v28,c.v29,c.v30,c.v31,c.v32,c.v33,c.v34,c.v35,c.v36,  ");
                    sqlQuery.Append("	c.v37,c.v38,c.v39,c.v40,c.v41,c.v42,c.v43,c.v44,c.v45,c.v46,c.v47,c.v48,c.v49,c.v50,c.v51,c.v52,c.v53,c.v54,c.v55,c.v56,c.v57,c.v58,c.v59,c.v60,c.v61,c.v62,c.v63,c.v64,c.v65,c.v66,c.v67,c.v68,c.v69,c.v70,c.v71,c.v72,c.v73,  ");
                    sqlQuery.Append("	c.v74,c.v75,c.v76,c.v77,c.v78,c.v79,c.v80,c.v81,c.v82,c.v83,c.v84,c.v85,c.v86,c.v87,c.v88,c.v89,c.v90,c.v91,c.v92,c.v93,c.v94,c.v95,c.v96,c.v97,c.v98,c.v99,c.v100,c.v101,c.v102,c.v103,c.v104,c.v105,c.v106,c.v107,c.v108,  ");
                    sqlQuery.Append("   c.v109,c.v110,c.v111,c.v112,c.v113,c.v114,c.v115,c.v116,c.v117,c.v118,c.v119,c.v120,c.v121,c.v122,c.v123,c.v124,c.v125,c.v126,c.v127,c.v128,c.v129,c.v130,c.v131,c.v132,c.v133,c.v134,c.v135,c.v136,c.v137,  ");
                    sqlQuery.Append("	c.v138,c.v139,c.v140,c.v141,c.v142,c.v143,c.v144,c.v145,c.v146,c.v147,c.v148,c.v149,c.v150,c.v151,c.v152,c.v153,c.v154,c.v155,c.v156,c.v157,c.v158,c.v159,c.v160,c.v161,c.v162,c.v163,c.v164,c.v165,c.v166,c.v167,  ");
                    sqlQuery.Append("	c.v168,c.v169,c.v170,c.v171,c.v172,c.v173,c.v174,c.v175,c.v176,c.v177,c.v178,c.v179,c.v180,c.v181,c.v182,c.v183,c.v184,c.v185,c.v186,c.v187,c.v188,c.v189,c.v190,c.v191,c.v192,c.v193,c.v194,c.v195,c.v196,c.v197,  ");
                    sqlQuery.Append("	c.v198,c.v199,c.v200,c.v201,c.v202,c.v203,c.v204,c.v205,c.v206,c.v207,c.v208,c.v209,c.v210,c.v211,c.v212,c.v213,c.v214,c.v215,c.v216,c.v217,c.v218,c.v219,c.v220,c.v221,c.v222,c.v223,c.v224,c.v225,c.v226,c.v227,  ");
                    sqlQuery.Append("	c.v228,c.v229,c.v230,c.v231,c.v232,c.v233,c.v234,c.v235,c.v236,c.v237,c.v238,c.v239,c.v240,c.v241,c.v242,c.v243,c.v244,c.v245,c.v246,c.v247,c.v248,c.v249,c.v250,c.v251,c.v252,c.v253,c.v254,c.v255,c.v256,c.v257,  ");
                    sqlQuery.Append("	c.v258,c.v259,c.v260,c.v261,c.v262,c.v263,c.v264,c.v265,c.v266,c.v267,c.v268,c.v269,c.v270,c.v271,c.v272,c.v273,c.v274,c.v275,c.v276,c.v277,c.v278,c.v279,c.v280,c.v281,c.v282,c.v283,c.v284,c.v285,c.v286,c.v287,  ");
                    sqlQuery.Append("	c.v288,c.v289,c.v290,c.v291,c.v292,c.v293,c.v294,c.v295,c.v296,c.v297,c.v298,c.v299,c.v300,c.v301,c.v302,c.v303,c.v304,c.v305,c.v306,c.v307,c.v308,c.v309,c.v310,c.v311,c.v312,c.v313,c.v314,c.v315,c.v316,c.v317,  ");
                    sqlQuery.Append("	c.v318,c.v319,c.v320,c.v321,c.v322,c.v323,c.v324,c.v325,c.v326,c.v327,c.v328,c.v329,c.v330,c.v331,c.v332,c.v333,c.v334,c.v335,c.v336,c.v337,c.v338,c.v339,c.v340,c.v341,c.v342,c.v343,c.v344,c.v345,c.v346,c.v347,  ");
                    sqlQuery.Append("	c.v348,c.v349,c.v350,c.v351,c.v352,c.v353,c.v354,c.v355,c.v356,c.v357,c.v358,c.v359,c.v360,c.v361,c.v362,c.v363,c.v364,c.v365,c.v366,c.v367,c.v368,c.v369,c.v370,c.v371,c.v372,c.v373,c.v374,c.v375,c.v376,c.v377,  ");
                    sqlQuery.Append("	c.v378,c.v379,c.v380,c.v381,c.v382,c.v383,c.v384,c.v385,c.v386,c.v387,c.v388,c.v389,c.v390,c.v391,c.v392,c.v393,c.v394,c.v395,c.v396,c.v397,c.v398,c.v399,c.v400,c.v401,c.v402,c.v403,c.v404,c.v405,c.v406,c.v407,  ");
                    sqlQuery.Append("	c.v408,c.v409,c.v410,c.v411,c.v412,c.v413,c.v414,c.v415,c.v416,c.v417,c.v418,c.v419,c.v420,c.v421,c.v422,c.v423,c.v424,c.v425,c.v426,c.v427,c.v428,c.v429,c.v430,c.v431,c.v432,c.v433,c.v434,c.v435,c.v436,c.v437,  ");
                    sqlQuery.Append("	c.v438,c.v439,c.v440,c.v441,c.v442,c.v443,c.v444,c.v445,c.v446,c.v447,c.v448,c.v449,c.v450,c.v451,c.v452,c.v453,c.v454,c.v455,c.v456,c.v457,c.v458,c.v459,c.v460,c.v461,c.v462,c.v463,c.v464,c.v465,c.v466,c.v467,  ");
                    sqlQuery.Append("	c.v468,c.v469,c.v470,c.v471,c.v472,c.v473,c.v474,c.v475,c.v476,c.v477,c.v478,c.v479,c.v480,c.v481,c.v482,c.v483,c.v484,c.v485,c.v486,c.v487,c.v488,c.v489,c.v490,c.v491,c.v492,c.v493,c.v494,c.v495,c.v496,c.v497,  ");
                    sqlQuery.Append("	c.v498,c.v499,c.v500,c.v501,c.v502,c.v503,c.v504,c.v505,c.v506,c.v507,c.v508,c.v509,c.v510,c.v511,c.v512,c.v513,c.v514,c.v515,c.v516,c.v517,c.v518,c.v519,c.v520,c.v521,c.v522,c.v523,c.v524,c.v525,c.v526,c.v527,  ");
                    sqlQuery.Append("	c.v528,c.v529,c.v530,c.v531,c.v532,c.v533,c.v534,c.v535,c.v536,c.v537,c.v538,c.v539,c.v540,c.v541,c.v542,c.v543,c.v544,c.v545,c.v546,c.v547,c.v548,c.v549,c.v550,c.v551,c.v552,c.v553,c.v554,c.v555,c.v556,c.v557,  ");
                    sqlQuery.Append("	c.v558,c.v559,c.v560,c.v561,c.v562,c.v563,c.v564,c.v565,c.v566,c.v567,c.v568,c.v569,c.v570,c.v571,c.v572,c.v573,c.v574,c.v575,c.v576,c.v577,c.v578,c.v579,c.v580,c.v581,c.v582,c.v583,c.v584,c.v585,c.v586,c.v587,  ");
                    sqlQuery.Append("	c.v588,c.v589,c.v590,c.v591,c.v592,c.v593,c.v594,c.v595,c.v596,c.v597,c.v598,c.v599,c.v600,c.v601,c.v602,c.v603,c.v604,c.v605,c.v606,c.v607,c.v608,c.v609,c.v610,c.v611,c.v612,c.v613,c.v614,c.v615,c.v616,c.v617,  ");
                    sqlQuery.Append("   c.v618,c.v619,c.v620,c.v621,c.v622,c.v623,c.v624,c.v625,c.v626,c.v627,c.v628,c.v629,c.v630,c.v631,c.v632,c.v633,c.v634,c.v635,c.v636,c.v637,c.v638,c.v639,c.v640,c.v641,c.v642,c.v643,c.v644,c.v645,c.v646,c.v647,  ");
                    sqlQuery.Append("	c.v648,c.v649,c.v650,c.v651,c.v652,c.v653,c.v654,c.v655,c.v656,c.v657,c.v658,c.v659,c.v660,c.v661,c.v662,c.v663,c.v664,c.v665,c.v666,c.v667,c.v668,c.v669,c.v670,c.v671,c.v672,c.v673,c.v674,c.v675,c.v676,c.v677,  ");
                    sqlQuery.Append("	c.v678,c.v679,c.v680,c.v681,c.v682,c.v683,c.v684,c.v685,c.v686,c.v687,c.v688,c.v689,c.v690,c.v691,c.v692,c.v693,c.v694,c.v695,c.v696,c.v697,c.v698,c.v699,c.v700,c.v701,c.v702,c.v703,c.v704,c.v705,c.v706,c.v707,  ");
                    sqlQuery.Append("	c.v708,c.v709,c.v710,c.v711,c.v712,c.v713,c.v714,c.v715,c.v716,c.v717,c.v718,c.v719,c.v720,c.v721,c.v722,c.v723,c.v724,c.v725,c.v726,c.v727,c.v728,c.v729,c.v730,c.v731,c.v732,c.v733,c.v734,c.v735,c.v736,c.v737,  ");
                    sqlQuery.Append("	c.v738,c.v739,c.v740,c.v741,c.v742,c.v743,c.v744,c.v745,c.v746,c.v747,c.v748,c.v749,c.v750,c.v751,c.v752,c.v753,c.v754,c.v755,c.v756,c.v757,c.v758,c.v759,c.v760,c.v761,c.v762,c.v763,c.v764,c.v765,c.v766,c.v767,  ");
                    sqlQuery.Append("	c.v768,c.v769,c.v770,c.v771,c.v772,c.v773,c.v774,c.v775,c.v776,c.v777,c.v778,c.v779,c.v780,c.v781,c.v782,c.v783,c.v784,c.v785,c.v786,c.v787,c.v788,c.v789,c.v790,c.v791,c.v792,c.v793,c.v794,c.v795,c.v796,c.v797,  ");
                    sqlQuery.Append("	c.v798,c.v799,c.v800,c.v801,c.v802,c.v803,c.v804,c.v805,c.v806,c.v807,c.v808,c.v809,c.v810,c.v811,c.v812,c.v813,c.v814,c.v815,c.v816,c.v817,c.v818,c.v819,c.v820,c.v821,c.v822,c.v823,c.v824,c.v825,c.v826,c.v827,  ");
                    sqlQuery.Append("	c.v828,c.v829,c.v830,c.v831,c.v832,c.v833,c.v834,c.v835,c.v836,c.v837,c.v838,c.v839,c.v840,c.v841,c.v842,c.v843,c.v844,c.v845,c.v846,c.v847,c.v848,c.v849,c.v850,c.v851,c.v852,c.v853,c.v854,c.v855,c.v856,c.v857,  ");
                    sqlQuery.Append("	c.v858,c.v859,c.v860,c.v861,c.v862,c.v863,c.v864,c.v865,c.v866,c.v867,c.v868,c.v869,c.v870,c.v871,c.v872,c.v873,c.v874,c.v875,c.v876,c.v877,c.v878,c.v879,c.v880,c.v881,c.v882,c.v883,c.v884,c.v885,c.v886,c.v887,  ");
                    sqlQuery.Append("	c.v888,c.v889,c.v890,c.v891,c.v892,c.v893,c.v894,c.v895,c.v896,c.v897,c.v898,c.v899,c.v900,c.v901,c.v902,c.v903,c.v904,c.v905,c.v906,c.v907,c.v908,c.v909,c.v910,c.v911,c.v912,c.v913,c.v914,c.v915,c.v916,c.v917,  ");
                    sqlQuery.Append("	c.v918,c.v919,c.v920,c.v921,c.v922,c.v923,c.v924,c.v925,c.v926,c.v927,c.v928,c.v929,c.v930,c.v931,c.v932,c.v933,c.v934,c.v935,c.v936,c.v937,c.v938,c.v939,c.v940,c.v941,c.v942,c.v943,c.v944,c.v945,c.v946,c.v947,  ");
                    sqlQuery.Append("	c.v948,c.v949,c.v950,c.v951,c.v952,c.v953,c.v954,c.v955,c.v956,c.v957,c.v958,c.v959,c.v960,c.v961,c.v962,c.v963,c.v964,c.v965,c.v966,c.v967,c.v968,c.v969,c.v970,c.v971,c.v972,c.v973,c.v974,c.v975,c.v976,c.v977,  ");
                    sqlQuery.Append("	c.v978,c.v979,c.v980,c.v981,c.v982,c.v983,c.v984,c.v985,c.v986,c.v987,c.v988,c.v989,c.v990,c.v991,c.v992,c.v993,c.v994,c.v995,c.v996,c.v997,c.v998,c.v999,  ");
                    sqlQuery.Append("	c.v1000,c.v1001,c.v1002,c.v1003,c.v1004,c.v1005,c.v1006,c.v1007,c.v1008,c.v1009,c.v1010,c.v1011,c.v1012, c.v1013   ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_Primaria_Conafe_Fin c ");
                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                case 9:
                    #region CAM
                    sqlQuery.Append("CAMF12|");
                    sqlQuery.Append("SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, ");
                    sqlQuery.Append("c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266,  c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.Append("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.Append("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.Append("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.Append("c.v496, c.v497, c.v498, c.v499, c.v500, c2.v501, c2.v502, c2.v503, c2.v504, c2.v505, c2.v506, c2.v507, c2.v508, c2.v509, c2.v510, c2.v511, c2.v512, c2.v513, c2.v514, ");
                    sqlQuery.Append("c2.v515, c2.v516, c2.v517, c2.v518, c2.v519, c2.v520, c2.v521, c2.v522, c2.v523, c2.v524, c2.v525, c2.v526, c2.v527, c2.v528, c2.v529, c2.v530, c2.v531, c2.v532, c2.v533, ");
                    sqlQuery.Append("c2.v534, c2.v535, c2.v536, c2.v537, c2.v538, c2.v539, c2.v540, c2.v541, c2.v542, c2.v543, c2.v544, c2.v545, c2.v546, c2.v547, c2.v548, c2.v549, c2.v550, c2.v551, c2.v552, ");
                    sqlQuery.Append("c2.v553, c2.v554, c2.v555, c2.v556, c2.v557, c2.v558, c2.v559, c2.v560, c2.v561, c2.v562, c2.v563, c2.v564, c2.v565, c2.v566, c2.v567, c2.v568, c2.v569, c2.v570, c2.v571, ");
                    sqlQuery.Append("c2.v572, c2.v573, c2.v574, c2.v575, c2.v576, c2.v577, c2.v578, c2.v579, c2.v580, c2.v581, c2.v582, c2.v583, c2.v584, c2.v585, c2.v586, c2.v587, c2.v588, c2.v589, c2.v590, ");
                    sqlQuery.Append("c2.v591, c2.v592, c2.v593, c2.v594, c2.v595, c2.v596, c2.v597, c2.v598, c2.v599, c2.v600, c2.v601, c2.v602, c2.v603, c2.v604, c2.v605, c2.v606, c2.v607, c2.v608, c2.v609, ");
                    sqlQuery.Append("c2.v610, c2.v611, c2.v612, c2.v613, c2.v614, c2.v615, c2.v616, c2.v617, c2.v618, c2.v619, c2.v620, c2.v621, c2.v622, c2.v623, c2.v624, c2.v625, c2.v626, c2.v627, c2.v628, ");
                    sqlQuery.Append("c2.v629, c2.v630, c2.v631, c2.v632, c2.v633, c2.v634, c2.v635, c2.v636, c2.v637, c2.v638, c2.v639, c2.v640, c2.v641, c2.v642, c2.v643, c2.v644, c2.v645, c2.v646, c2.v647, ");
                    sqlQuery.Append("c2.v648, c2.v649, c2.v650, ");
                    sqlQuery.Append("c3.v1507, c3.v1508, c3.v1509, c3.v1510, c3.v1511, c3.v1512,c3.v1513,c3.v1514,c3.v1515,c3.v1516,c3.v1517,c3.v1518,c3.v1519,c3.v1520,c3.v1521,c3.v1522,c3.v1523,c3.v1524,");
                    sqlQuery.Append("c3.v1525, c3.v1526, c3.v1527, c3.v1528, c3.v1529, c3.v1530,c3.v1531,c3.v1532,c3.v1533,c3.v1534,c3.v1535,c3.v1536,c3.v1537,c3.v1538,c3.v1539,c3.v1540,c3.v1541,c3.v1542,");
                    sqlQuery.Append("c3.v1543, c3.v1544, c3.v1545, c3.v1546, c3.v1547, c3.v1548,c3.v1549,c3.v1550,c3.v1551,c3.v1552,c3.v1553,c3.v1554,c3.v1555,c3.v1556,c3.v1557,c3.v1558,c3.v1559,c3.v1560,");
                    sqlQuery.Append("c3.v1561, c3.v1562, c3.v1563, c3.v1564, c3.v1565, c3.v1566,c3.v1567,c3.v1568,c3.v1569,c3.v1570,c3.v1571,c3.v1572,c3.v1573,c3.v1574,c3.v1575,c3.v1576,c3.v1577,c3.v1578,");
                    sqlQuery.Append("c3.v1579, c3.v1580, c3.v1581, c3.v1582, c3.v1583, c3.v1584,c3.v1585,c3.v1586,c3.v1587,c3.v1588,c3.v1589,c3.v1590,c3.v1591,c3.v1592,c3.v1593,c3.v1594,c3.v1595,c3.v1596,");

                    sqlQuery.Append("c2.v651, c2.v652, c2.v653, c2.v654, c2.v655, c2.v656, c2.v657, c2.v658, c2.v659, c2.v660, c2.v661, c2.v662, c2.v663, c2.v664, c2.v665, c2.v666, ");
                    sqlQuery.Append("c2.v667, c2.v668, c2.v669, c2.v670, c2.v671, c2.v672, c2.v673, c2.v674, c2.v675, c2.v676, c2.v677, c2.v678, c2.v679, c2.v680, c2.v681, c2.v682, c2.v683, c2.v684, c2.v685, ");
                    sqlQuery.Append("c2.v686, c2.v687, c2.v688, c2.v689, c2.v690, c2.v691, c2.v692, c2.v693, c2.v694, c2.v695, c2.v696, c2.v697, c2.v698, c2.v699, c2.v700, c2.v701, c2.v702, c2.v703, c2.v704, ");
                    sqlQuery.Append("c2.v705, c2.v706, c2.v707, c2.v708, c2.v709, c2.v710, c2.v711, c2.v712, c2.v713, c2.v714, c2.v715, c2.v716, c2.v717, c2.v718, c2.v719, c2.v720, c2.v721, c2.v722, c2.v723, ");
                    sqlQuery.Append("c2.v724, c2.v725, c2.v726, c2.v727, c2.v728, c2.v729, c2.v730, c2.v731, c2.v732, c2.v733, c2.v734, c2.v735, c2.v736, c2.v737, c2.v738, c2.v739, c2.v740, c2.v741, c2.v742, ");
                    sqlQuery.Append("c2.v743, c2.v744, c2.v745, c2.v746, c2.v747, c2.v748, c2.v749, c2.v750, c2.v751, c2.v752, c2.v753, c2.v754, c2.v755, c2.v756, c2.v757, c2.v758, c2.v759, c2.v760, c2.v761, ");
                    sqlQuery.Append("c2.v762, c2.v763, c2.v764, c2.v765, c2.v766, c2.v767, c2.v768, c2.v769, c2.v770, c2.v771, c2.v772, c2.v773, c2.v774, c2.v775, c2.v776, c2.v777, c2.v778, c2.v779, c2.v780, ");
                    sqlQuery.Append("c2.v781, c2.v782, c2.v783, c2.v784, c2.v785, c2.v786, c2.v787, c2.v788, c2.v789, c2.v790, c2.v791, c2.v792, c2.v793, c2.v794, c2.v795, c2.v796, c2.v797, c2.v798, c2.v799, ");
                    sqlQuery.Append("c2.v800, c2.v801, c2.v802, c2.v803, c2.v804, c2.v805, c2.v806, c2.v807, c2.v808, c2.v809, c2.v810, c2.v811, c2.v812, c2.v813, c2.v814, c2.v815, c2.v816, c2.v817, c2.v818, ");
                    sqlQuery.Append("c2.v819, c2.v820, c2.v821, c2.v822, c2.v823, c2.v824, c2.v825, c2.v826, c2.v827, c2.v828, c2.v829, c2.v830, c2.v831, c2.v832, c2.v833, c2.v834, c2.v835, c2.v836, c2.v837, ");
                    sqlQuery.Append("c2.v838, c2.v839, c2.v840, c2.v841, c2.v842, c2.v843, c2.v844, c2.v845, c2.v846, c2.v847, c2.v848, c2.v849, c2.v850, c2.v851, c2.v852, c2.v853, c2.v854, c2.v855, c2.v856, ");
                    sqlQuery.Append("c2.v857, c2.v858, c2.v859, c2.v860, c2.v861, c2.v862, c2.v863, c2.v864, c2.v865, c2.v866, c2.v867, c2.v868, c2.v869, c2.v870, c2.v871, c2.v872, c2.v873, c2.v874, c2.v875, ");
                    sqlQuery.Append("c2.v876, c2.v877, c2.v878, c2.v879, c2.v880, c2.v881, c2.v882, c2.v883, c2.v884, c2.v885, c2.v886, c2.v887, c2.v888, c2.v889, c2.v890, c2.v891, c2.v892, c2.v893, c2.v894, ");
                    sqlQuery.Append("c2.v895, c2.v896, c2.v897, c2.v898, c2.v899, c2.v900, c2.v901, c2.v902, c2.v903, c2.v904, c2.v905, c2.v906, c2.v907, c2.v908, c2.v909, c2.v910, c2.v911, c2.v912, c2.v913, ");
                    sqlQuery.Append("c2.v914, c2.v915, c2.v916, c2.v917, c2.v918, c2.v919, c2.v920, c2.v921, c2.v922, c2.v923, c2.v924, c2.v925, c2.v926, c2.v927, c2.v928, c2.v929, c2.v930, c2.v931, c2.v932, ");
                    sqlQuery.Append("c2.v933, c2.v934, c2.v935, c2.v936, c2.v937, c2.v938, c2.v939, c2.v940, c2.v941, c2.v942, c2.v943, c2.v944, c2.v945, c2.v946, c2.v947, c2.v948, c2.v949, c2.v950, c2.v951, ");
                    sqlQuery.Append("c2.v952, c2.v953, c2.v954, c2.v955, c2.v956, c2.v957, c2.v958, c2.v959, c2.v960, c2.v961, c2.v962, c2.v963, c2.v964, c2.v965, c2.v966, c2.v967, c2.v968, c2.v969, c2.v970, ");
                    sqlQuery.Append("c2.v971, c2.v972, c2.v973, c2.v974, c2.v975, c2.v976, c2.v977, c2.v978, c2.v979, c2.v980, c2.v981, c2.v982, c2.v983, c2.v984, c2.v985, c2.v986, c2.v987, c2.v988, c2.v989,  ");
                    sqlQuery.Append("c2.v990, c2.v991, c2.v992, c2.v993, c2.v994, c2.v995, c2.v996, c2.v997, c2.v998, c2.v999, c2.v1000, c3.v1001, c3.v1002, c3.v1003, c3.v1004, c3.v1005, c3.v1006, c3.v1007, c3.v1008, c3.v1009, c3.v1010, ");
                    sqlQuery.Append("c3.v1011, c3.v1012, c3.v1013, c3.v1014, c3.v1015, c3.v1016, c3.v1017, c3.v1018, c3.v1019, c3.v1020, c3.v1021, c3.v1022, c3.v1023, c3.v1024, c3.v1025, c3.v1026, c3.v1027, c3.v1028, c3.v1029, c3.v1030, ");
                    sqlQuery.Append("c3.v1031, c3.v1032, c3.v1033, c3.v1034, c3.v1035, c3.v1036, c3.v1037, c3.v1038, c3.v1039, c3.v1040, ");
                    sqlQuery.Append("c3.v1041, c3.v1042, c3.v1043, c3.v1044, c3.v1045, c3.v1046, c3.v1047, c3.v1048, c3.v1049, c3.v1050, c3.v1051, c3.v1052, c3.v1053, c3.v1054, c3.v1055, c3.v1056, c3.v1057, c3.v1058, c3.v1059, c3.v1060, ");
                    sqlQuery.Append("c3.v1061, c3.v1062, c3.v1063, c3.v1064, c3.v1065, c3.v1066, c3.v1067, c3.v1068, c3.v1069, c3.v1070, c3.v1071, c3.v1072, c3.v1073, c3.v1074, c3.v1075, c3.v1076, c3.v1077, c3.v1078, c3.v1079, c3.v1080, ");
                    sqlQuery.Append("c3.v1081, c3.v1082, c3.v1083, c3.v1084, c3.v1085, c3.v1086, c3.v1087, c3.v1088, c3.v1089, c3.v1090, c3.v1091, c3.v1092, c3.v1093, c3.v1094, c3.v1095, c3.v1096, c3.v1097, c3.v1098, c3.v1099, c3.v1100, ");
                    sqlQuery.Append("c3.v1101, c3.v1102, c3.v1103, c3.v1104, c3.v1105, c3.v1106, c3.v1107, c3.v1108, c3.v1109, c3.v1110, c3.v1111, c3.v1112, c3.v1113, c3.v1114, c3.v1115, c3.v1116, c3.v1117, c3.v1118, c3.v1119, c3.v1120, ");
                    sqlQuery.Append("c3.v1121, c3.v1122, c3.v1123, c3.v1124, c3.v1125, c3.v1126, c3.v1127, c3.v1128, c3.v1129, c3.v1130, c3.v1131,");
                    sqlQuery.Append("c3.v1616,c3.v1617,c3.v1618,c3.v1619,c3.v1620,c3.v1621,c3.v1622,c3.v1623,c3.v1624,c3.v1625,c3.v1626,c3.v1627,c3.v1628,");

                    sqlQuery.Append("c3.v1132, c3.v1133, c3.v1134, c3.v1135, c3.v1136, c3.v1137, c3.v1138, c3.v1139, c3.v1140, ");
                    sqlQuery.Append("c3.v1141, c3.v1142, c3.v1143, c3.v1144, c3.v1145, c3.v1146, c3.v1147, c3.v1148, c3.v1149, c3.v1150, c3.v1151, c3.v1152, c3.v1153, c3.v1154, c3.v1155, c3.v1156, c3.v1157, c3.v1158, c3.v1159, c3.v1160, ");
                    sqlQuery.Append("c3.v1161, c3.v1162, ");

                    sqlQuery.Append("c3.v1597,c3.v1163, c3.v1164, c3.v1165, c3.v1166, c3.v1167, c3.v1168, c3.v1169, c3.v1170, c3.v1171, c3.v1172, c3.v1173, c3.v1174, c3.v1175, c3.v1176, c3.v1177, c3.v1178, c3.v1179, c3.v1180, ");
                    sqlQuery.Append("c3.v1181, c3.v1182, c3.v1183, c3.v1184, c3.v1185, c3.v1186, c3.v1187, c3.v1188, c3.v1189, c3.v1190, c3.v1191, c3.v1192, c3.v1193, ");
                    sqlQuery.Append("c3.v1194, c3.v1195, c3.v1196, c3.v1197, c3.v1198, c3.v1199, c3.v1200, c3.v1201, c3.v1202, c3.v1203, c3.v1204, c3.v1205, c3.v1206, ");
                    sqlQuery.Append("c3.v1207, c3.v1208, c3.v1209, c3.v1210, c3.v1211, c3.v1212, c3.v1213, c3.v1214, c3.v1215, c3.v1216, c3.v1217, c3.v1218, c3.v1219, ");
                    sqlQuery.Append("c3.v1220, c3.v1221, c3.v1222, c3.v1223, c3.v1224, c3.v1225, c3.v1226, c3.v1227, c3.v1228, c3.v1229, c3.v1230, c3.v1231, c3.v1232, ");
                    sqlQuery.Append("c3.v1233, c3.v1234, c3.v1235, c3.v1236, c3.v1237, c3.v1238, c3.v1239, c3.v1240, c3.v1241, c3.v1242, c3.v1243, c3.v1244, c3.v1245, ");
                    sqlQuery.Append("c3.v1246, c3.v1247, c3.v1248, c3.v1249, c3.v1250, c3.v1251, c3.v1252, c3.v1253, c3.v1254, c3.v1255, c3.v1256, c3.v1257, c3.v1258, ");
                    sqlQuery.Append("c3.v1259, c3.v1260, c3.v1261, c3.v1262, c3.v1263, c3.v1264, c3.v1265, c3.v1266, c3.v1267, c3.v1268, c3.v1269, c3.v1270, c3.v1271, ");
                    sqlQuery.Append("c3.v1272, c3.v1273, c3.v1274, c3.v1275, c3.v1276, c3.v1277, c3.v1278, c3.v1279, c3.v1280, c3.v1281, c3.v1282, c3.v1283, c3.v1284, ");
                    sqlQuery.Append("c3.v1285, c3.v1286, c3.v1287, c3.v1288, c3.v1289, c3.v1290, c3.v1291, c3.v1292, c3.v1293, c3.v1294, c3.v1295, c3.v1296, c3.v1297, c3.v1298, ");
                    sqlQuery.Append("c3.v1299, c3.v1300, c3.v1301, c3.v1302, c3.v1303, c3.v1304, c3.v1305, c3.v1306, c3.v1307, c3.v1308, c3.v1309, c3.v1310, c3.v1311, ");
                    sqlQuery.Append("c3.v1312, c3.v1313, c3.v1314, c3.v1315, c3.v1316, c3.v1317, c3.v1318, c3.v1319, c3.v1320, c3.v1321, c3.v1322, c3.v1323, c3.v1324, ");
                    sqlQuery.Append("c3.v1325, c3.v1326, c3.v1598, c3.v1599, c3.v1600, c3.v1601, c3.v1602, c3.v1603, c3.v1604, c3.v1605, c3.v1606, c3.v1607, c3.v1608, ");
                    sqlQuery.Append("c3.v1609, c3.v1610, c3.v1611, c3.v1612, c3.v1613, c3.v1614, c3.v1615, c3.v1327, c3.v1328, c3.v1329, c3.v1330, c3.v1331, c3.v1332, ");
                    sqlQuery.Append("c3.v1333, c3.v1334, c3.v1335, c3.v1336, c3.v1337, c3.v1338, c3.v1339, c3.v1340, c3.v1341, c3.v1342, c3.v1343, c3.v1344, c3.v1345, ");
                    sqlQuery.Append("c3.v1346, c3.v1347, c3.v1348, c3.v1349, c3.v1350, c3.v1351, c3.v1352, c3.v1353, c3.v1354, c3.v1355, ");
                    sqlQuery.Append("c3.v1356, c3.v1357, c3.v1358, c3.v1359, c3.v1360, c3.v1361, c3.v1362, c3.v1363, c3.v1364, c3.v1365, ");
                    sqlQuery.Append("c3.v1366, c3.v1367, c3.v1368, c3.v1369, c3.v1370, c3.v1371, c3.v1372, c3.v1373, c3.v1374, c3.v1375, ");
                    sqlQuery.Append("c3.v1376, c3.v1377, c3.v1378, c3.v1379, c3.v1380, c3.v1381, c3.v1382, c3.v1383, c3.v1384, c3.v1385, ");
                    sqlQuery.Append("c3.v1386, c3.v1387, c3.v1388, c3.v1389, c3.v1390, c3.v1391, c3.v1392, c3.v1393, c3.v1394, c3.v1395, ");
                    sqlQuery.Append("c3.v1396, c3.v1397, c3.v1398, c3.v1399, c3.v1400, c3.v1401, c3.v1402, c3.v1403, c3.v1404, c3.v1405, ");
                    sqlQuery.Append("c3.v1406, c3.v1407, c3.v1408, c3.v1409, c3.v1410, c3.v1411, c3.v1412, c3.v1413, c3.v1414, c3.v1415, ");
                    sqlQuery.Append("c3.v1416, c3.v1417, c3.v1418, c3.v1419, c3.v1420, c3.v1421, c3.v1422, c3.v1423, c3.v1424, c3.v1425, ");
                    sqlQuery.Append("c3.v1426, c3.v1427, c3.v1428, c3.v1429, c3.v1430, c3.v1431, c3.v1432, c3.v1433, c3.v1434, c3.v1435, ");
                    sqlQuery.Append("c3.v1436, c3.v1437, c3.v1438, c3.v1439, c3.v1440, c3.v1441, c3.v1442, c3.v1443, c3.v1444, c3.v1445, ");
                    sqlQuery.Append("c3.v1446, c3.v1447, c3.v1448, c3.v1449, c3.v1450, c3.v1451, c3.v1452, c3.v1453, c3.v1454, c3.v1455, ");
                    sqlQuery.Append("c3.v1456, c3.v1457, c3.v1458, c3.v1459, c3.v1460, c3.v1461, c3.v1462, c3.v1463, c3.v1464, c3.v1465, ");
                    sqlQuery.Append("c3.v1466, c3.v1467, c3.v1468, c3.v1469, c3.v1470, c3.v1471, c3.v1472, c3.v1473, c3.v1474, c3.v1475, ");
                    sqlQuery.Append("c3.v1476, c3.v1477, c3.v1478, c3.v1479, c3.v1480, c3.v1481, c3.v1482, c3.v1483, c3.v1484, c3.v1485, ");
                    sqlQuery.Append("c3.v1486, c3.v1487, c3.v1488, c3.v1489, c3.v1490, c3.v1491, c3.v1492, c3.v1493, c3.v1494, c3.v1495, ");
                    sqlQuery.Append("c3.v1496, c3.v1497, c3.v1498, c3.v1499, c3.v1500, c3.v1501, c3.v1502, c3.v1503, c3.v1504, c3.v1505, c3.v1506 ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_CAM_Fin_1 c, ");
                    sqlQuery.Append("  VW_CAM_Fin_2 c2, ");
                    sqlQuery.Append("  VW_CAM_Fin_3 c3 ");
                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control           = c2.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control           = c3.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");



                    #endregion
                    break;
                case 11:
                    #region USAER
                    sqlQuery.Append("USAERF12|");
                    sqlQuery.Append("SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON,  ");
                    sqlQuery.Append("      c.v1, c.v2, c.v3, UPPER(c.v4), c.v5, c.v6, c.v7, UPPER(c.v8), c.v9, c.v10, c.v11, UPPER(c.v12), c.v13, c.v14, c.v15, UPPER(c.v16), c.v17, c.v18, c.v19,   ");
                    sqlQuery.Append("      UPPER(c.v20), c.v21, c.v22, c.v23, UPPER(c.v24), c.v25, c.v26, c.v27, UPPER(c.v28), c.v29, c.v30, c.v31, UPPER(c.v32), c.v33, c.v34, c.v35, UPPER(c.v36), c.v37,     ");
                    sqlQuery.Append("      c.v38, c.v39, UPPER(c.v40), c.v41, c.v42, ");
                    sqlQuery.Append("      c2.v1071, c2.v1072, ");
                    sqlQuery.Append("      c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62,   ");
                    sqlQuery.Append("      c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, c.v75, c.v76, c.v77, c.v78, c.v79, c.v80,   ");
                    sqlQuery.Append("      c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, c.v97, c.v98,   ");
                    sqlQuery.Append("      c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114,   ");
                    sqlQuery.Append("      c.v115, c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130,   ");
                    sqlQuery.Append("      c.v131, c.v132, c.v133, c.v134, c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146,   ");
                    sqlQuery.Append("      c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162,   ");
                    sqlQuery.Append("      c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, c.v173, c.v174, c.v175, c.v176, c.v177, c.v178,   ");
                    sqlQuery.Append("      c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, c.v192, c.v193, c.v194,   ");
                    sqlQuery.Append("      c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210,   ");
                    sqlQuery.Append("      c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226,   ");
                    sqlQuery.Append("      c.v227, c.v228, c.v229, c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242,   ");
                    sqlQuery.Append("      c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258,   ");
                    sqlQuery.Append("      c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274,   ");
                    sqlQuery.Append("      c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, c.v287, c.v288, c.v289, c.v290,   ");
                    sqlQuery.Append("      c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, c.v306,   ");
                    sqlQuery.Append("      c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322,   ");
                    sqlQuery.Append("      c.v323, c.v324, c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338,   ");
                    sqlQuery.Append("      c.v339, c.v340, c.v341, c.v342, c.v343, c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354,   ");
                    sqlQuery.Append("      c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370,   ");
                    sqlQuery.Append("      c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, c.v382, c.v383, c.v384, c.v385, c.v386,   ");
                    sqlQuery.Append("      c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, c.v401, c.v402,   ");
                    sqlQuery.Append("      c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418,   ");
                    sqlQuery.Append("      c.v419, c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434,   ");
                    sqlQuery.Append("      c.v435, c.v436, c.v437, c.v438, c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450,   ");
                    sqlQuery.Append("      c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466,   ");
                    sqlQuery.Append("      c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, c.v477, c.v478, c.v479, c.v480, c.v481, c.v482,   ");
                    sqlQuery.Append("      c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, c.v496, c.v497, c.v498,   ");
                    sqlQuery.Append("      c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514,   ");
                    sqlQuery.Append("      c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530,   ");
                    sqlQuery.Append("      c.v531, c.v532, c.v533, c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546,   ");
                    sqlQuery.Append("      c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562,   ");
                    sqlQuery.Append("      c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578,   ");
                    sqlQuery.Append("      c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, c.v591, c.v592, c.v593, c.v594,   ");
                    sqlQuery.Append("      c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, c.v610,   ");
                    sqlQuery.Append("      c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626,   ");
                    sqlQuery.Append("      c.v627, c.v628, c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642,   ");
                    sqlQuery.Append("      c.v643, c.v644, c.v645, c.v646, c.v647, c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658,   ");
                    sqlQuery.Append("      c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, c.v665, c.v666, c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674,   ");
                    sqlQuery.Append("      c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, c.v681, c.v682, c.v683, c.v684, c.v685, c.v686, c.v687, c.v688, c.v689, c.v690,   ");
                    sqlQuery.Append("      c.v691, c.v692, c.v693, c.v694, c.v695, c.v696, c.v697, c.v698, c.v699, c.v700, c.v701, c.v702, c.v703, c.v704, c.v705, c.v706,   ");
                    sqlQuery.Append("      c.v707, c.v708, c.v709, c.v710, c.v711, c.v712, c.v713, c.v714, c.v715, c.v716, c.v717, c.v718, c.v719, c.v720, c.v721, c.v722,   ");
                    sqlQuery.Append("      c.v723, c.v724, c.v725, c.v726, c.v727, c.v728, c.v729, c.v730, c.v731, c.v732, c.v733, c.v734, c.v735, c.v736, c.v737, c.v738,   ");
                    sqlQuery.Append("      c.v739, c.v740, c.v741, c.v742, c.v743, c.v744, c.v745, c.v746, c.v747, c.v748, c.v749, c.v750, c.v751, c.v752, c.v753, c.v754,   ");
                    sqlQuery.Append("      c.v755, c.v756, c.v757, c.v758, c.v759, c.v760, c.v761, c.v762, c.v763, c.v764, c.v765, c.v766, c.v767, c.v768, c.v769, c.v770,   ");
                    sqlQuery.Append("      c.v771, c.v772, c.v773, c.v774, c.v775, c.v776, c.v777, c.v778, c.v779, c.v780, c.v781, c.v782, c.v783, c.v784, c.v785, c.v786,   ");
                    sqlQuery.Append("      c.v787, c.v788, c.v789, c.v790, c.v791, c.v792, c.v793, c.v794, c.v795, c.v796, c.v797, c.v798, c.v799, c.v800, c.v801, c.v802,   ");
                    sqlQuery.Append("      c.v803, c.v804, c.v805, c.v806, c.v807, c.v808, c.v809, c.v810, c.v811, c.v812, c.v813, c.v814, c.v815, c.v816, c.v817, c.v818,   ");
                    sqlQuery.Append("      c.v819, c.v820, c.v821, c.v822, c.v823, c.v824, c.v825, c.v826, c.v827, c.v828, c.v829, c.v830, c.v831, c.v832, c.v833, c.v834,   ");
                    sqlQuery.Append("      c.v835, c.v836, c.v837, c.v838, c.v839, c.v840, c.v841, c.v842, c.v843, c.v844, c.v845, c.v846, c.v847, c.v848, c.v849, c.v850,   ");
                    sqlQuery.Append("      c.v851, c.v852, c.v853, c.v854, c.v855, c.v856, c.v857, c.v858, c.v859, c2.v1073, c.v860, c.v861, c.v862, c.v863, c.v864, c.v865,   ");
                    sqlQuery.Append("      c.v866, c.v867, c.v868, c.v869, c.v870, c.v871, c.v872, c.v873, c.v874, c.v875, c.v876, c.v877, c.v878, c.v879, c.v880, c.v881,    ");
                    sqlQuery.Append("      c.v882, c.v883, c.v884, c.v885, c.v886, c.v887, c.v888, c.v889, c.v890, c.v891, c.v892, c.v893, c.v894, c.v895, c.v896, c.v897, c.v898, c.v899, c.v900,   ");
                    sqlQuery.Append("      c2.v901, c2.v902, c2.v903, c2.v904, c2.v905, c2.v906, c2.v907, c2.v908, c2.v909, c2.v910, c2.v911, c2.v912, c2.v913, c2.v914,  ");
                    sqlQuery.Append("      c2.v915, c2.v916, c2.v917, c2.v918, c2.v919, c2.v920, c2.v921, c2.v922, c2.v923, c2.v924, c2.v925, c2.v926, c2.v927, c2.v928, c2.v929, c2.v930,   ");
                    sqlQuery.Append("      c2.v931, c2.v932, c2.v933, c2.v934, c2.v935, c2.v936, c2.v937, c2.v938, c2.v939, c2.v940, c2.v941, c2.v942, c2.v943, c2.v944, c2.v945, c2.v946,   ");
                    sqlQuery.Append("      c2.v947, c2.v948, c2.v949, c2.v950, c2.v951, c2.v952, c2.v953, c2.v954, c2.v955, c2.v956, c2.v957, c2.v958, c2.v959, c2.v960, c2.v961, c2.v962, c2.v963,   ");
                    sqlQuery.Append("      c2.v964, c2.v965, c2.v966, c2.v967, c2.v968, c2.v969, c2.v970, c2.v971, c2.v972, c2.v973, c2.v974, c2.v975, c2.v976, c2.v977, c2.v978, c2.v979, c2.v980, c2.v981,   ");
                    sqlQuery.Append("      c2.v982, c2.v983, c2.v984, c2.v985, c2.v986, c2.v987, c2.v988, c2.v989, c2.v990, c2.v991, c2.v992, c2.v993, c2.v994, c2.v995, c2.v996, c2.v997,   ");
                    sqlQuery.Append("      c2.v998, c2.v999, c2.v1000, c2.v1001, c2.v1002, c2.v1003, c2.v1004, c2.v1005, c2.v1006, c2.v1007, c2.v1008, c2.v1009, c2.v1010, c2.v1011,   ");
                    sqlQuery.Append("      c2.v1012, c2.v1013, c2.v1014, c2.v1015, c2.v1016, c2.v1017, c2.v1018, c2.v1019, c2.v1020, c2.v1021, c2.v1022, c2.v1023, c2.v1024, c2.v1025, c2.v1026, c2.v1027,   ");
                    sqlQuery.Append("      c2.v1028, c2.v1029, c2.v1030, c2.v1031, c2.v1032, c2.v1033, c2.v1034, c2.v1035, c2.v1036, c2.v1037, c2.v1038, c2.v1039, c2.v1040, c2.v1041, c2.v1042, c2.v1043,   ");
                    sqlQuery.Append("      c2.v1044, c2.v1045, c2.v1046, c2.v1047, c2.v1048, c2.v1049, c2.v1050, c2.v1051, c2.v1052, c2.v1053, c2.v1054, c2.v1055, c2.v1056, c2.v1074, c2.v1075, c2.v1076,   ");
                    sqlQuery.Append("      c2.v1077, c2.v1078, c2.v1079, c2.v1060, c2.v1080, c2.v1061, c2.v1062, c2.v1063, c2.v1064, c2.v1065, c2.v1066, c2.v1067, c2.v1068, c2.v1069, c2.v1070    ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_Usaer_Fin_1 c, ");
                    sqlQuery.Append("  VW_Usaer_Fin_2 c2 ");

                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control           = c2.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                case 12:
                    #region Inicial No Escolarizado
                    sqlQuery.Append("NOESCF12|");
                    sqlQuery.Append("SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, '' v0, ");
                    sqlQuery.Append("		c.v1, c.v2,c.v3, c.v4,c.v5,c.v6,c.v7,c.v8,c.v9,c.v10,c.v11,c.v12,c.v13,c.v14,c.v15,c.v16,c.v17,c.v18,c.v19,c.v20,c.v21,c.v22,c.v23,c.v24,c.v25,c.v26,c.v27,c.v28,c.v29,c.v30,c.v31,c.v32,c.v33,c.v34,c.v35,  ");
                    sqlQuery.Append("		c.v36, c.v37, c.v38,  c.v39,c.v40,c.v41,c.v42,c.v43,c.v44,c.v45,c.v46,c.v47,c.v48, ");
                    sqlQuery.Append("		c.v562,c.v563,c.v564, c.v565, c.v566,c.v567, ");
                    sqlQuery.Append("		c.v49, c.v50, c.v51,c.v52,c.v53,c.v54,c.v55,c.v56,c.v57,c.v58,c.v59,c.v60,c.v61, c.v62,c.v63,c.v64,c.v65, c.v66, c.v67, c.v68, c.v69,c.v70,c.v71,");
                    sqlQuery.Append("		c.v72,c.v73,c.v74,c.v75,c.v76,c.v77,c.v78,c.v79,c.v80,c.v81,c.v82,c.v83,c.v84,c.v85,c.v86,c.v87,c.v88,c.v89,c.v90,c.v91,c.v92,c.v93,c.v94,c.v95, ");
                    sqlQuery.Append("		c.v96, c.v97, c.v98,c.v99,c.v100,c.v101, c.v102,c.v103,c.v104,c.v105,c.v106,c.v107,c.v108,c.v109,c.v110,c.v111,c.v112,c.v113,c.v114,c.v115,c.v116, ");
                    sqlQuery.Append("		c.v117,c.v118,c.v119,c.v120,c.v121,c.v122,c.v123,c.v124,c.v125, c.v126,c.v127,c.v128, c.v129,c.v130,c.v131,c.v132,c.v133,c.v134,c.v135,c.v136,c.v137,");
                    sqlQuery.Append("		c.v138,c.v139,c.v140,c.v141,c.v142,c.v143,c.v144,c.v145,c.v146,c.v147,c.v148,c.v149,c.v150,c.v151,c.v152,c.v153,c.v154,c.v155, c.v156,c.v157,c.v158, ");
                    sqlQuery.Append("		c.v159,c.v160,c.v161,c.v162,c.v163,c.v164,c.v165,c.v166,c.v167,c.v168,c.v169,c.v170,c.v171,c.v172,c.v173,c.v174,c.v175,c.v176,c.v177,c.v178,c.v179,");
                    sqlQuery.Append("		c.v180,c.v181,c.v182, c.v183,c.v184,c.v185,c.v186,c.v187,c.v188,c.v189,c.v190,c.v191,c.v192,c.v193,c.v194,c.v195,c.v196,c.v197,c.v198,c.v199,c.v200,");
                    sqlQuery.Append("		c.v201,c.v202,c.v203,c.v204,c.v205,c.v206,c.v207,c.v208,c.v209, c.v210,c.v211,c.v212,c.v213,c.v214,c.v215,c.v216,c.v217,c.v218,c.v219,c.v220,c.v221,");
                    sqlQuery.Append("		c.v222,c.v223,c.v224,c.v225,c.v226,c.v227,c.v228,c.v229,c.v230,c.v231,c.v232,c.v233,c.v234,c.v235,c.v236,c.v237,c.v238,c.v239,c.v240,c.v241,  ");
                    sqlQuery.Append("		c.v568, c.v569, c.v570, c.v571, c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, ");
                    sqlQuery.Append("		c.v588, c.v589, c.v590, c.v591, ");
                    sqlQuery.Append("		c.v242, c.v243, c.v244,c.v245,c.v246,c.v247,c.v248,c.v249,c.v250,c.v251,c.v252,c.v253,c.v254,c.v255,c.v256,c.v257,c.v258,c.v259,c.v260,c.v261,c.v262,c.v263,  ");
                    sqlQuery.Append("		c.v264,c.v265,c.v266,c.v267,c.v268,c.v269, c.v592, c.v593,");
                    sqlQuery.Append("		c.v270,c.v271,c.v272,c.v273,c.v274,c.v275,c.v276,c.v277,c.v278,c.v279,c.v280,c.v281,c.v282,c.v283,c.v284,c.v285,c.v286, ");
                    sqlQuery.Append("		c.v594,c.v595, c.v287, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, c.v610, ");
                    sqlQuery.Append("		c.v611,c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626, c.v627, c.v628, ");
                    sqlQuery.Append("		c.v629,c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, c.v643, c.v644, c.v645, c.v646, ");
                    sqlQuery.Append("		c.v647,c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, ");
                    sqlQuery.Append("		c.v665,c.v666, c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674, ");
                    sqlQuery.Append("		c.v303, c.v304, c.v305, c.v306, c.v307, c.v308, c.v309, c.v310, c.v311,c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318,  c.v319, c.v320,");
                    sqlQuery.Append("		c.v321, c.v322, c.v323, c.v324, c.v325, c.v326, c.v327, c.v328, ");
                    sqlQuery.Append("		c.v329,c.v330, c.v331, c.v312, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, c.v344, c.v345, c.v346, ");
                    sqlQuery.Append("		c.v347,c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, c.v363, c.v364, ");
                    sqlQuery.Append("		c.v365,c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, c.v382,");
                    sqlQuery.Append("		c.v383,c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v389, c.v399, c.v400,");
                    sqlQuery.Append("		c.v401,c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, ");

                    sqlQuery.Append("		c.v705,c.v706, c.v707, c.v708, c.v709, c.v710, c.v711, c.v712, c.v713, c.v714, c.v715, c.v716, c.v717, c.v718, ");

                    sqlQuery.Append("		c.v415,c.v416, c.v417, c.v418, c.v419, c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432,");
                    sqlQuery.Append("		c.v433,c.v434, c.v435, c.v436, c.v437, c.v438, c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450,");
                    sqlQuery.Append("		c.v451,c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468,");
                    sqlQuery.Append("		c.v469,c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486,");
                    sqlQuery.Append("		c.v487,c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504,");
                    sqlQuery.Append("		c.v505,c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522,");
                    sqlQuery.Append("		c.v523,c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540,");
                    sqlQuery.Append("		c.v541,c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.Append("		c.v719,c.v720, c.v721, c.v722, c.v723, c.v724, c.v725, c.v726, c.v727, c.v728, c.v729, c.v730, c.v731, c.v732, c.v733, c.v734, ");
                    sqlQuery.Append("		c.v553,c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561 ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_EI_NE2_Fin c ");

                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");


                    #endregion
                    break;
                case 13:
                    #region Inicial Escolarizado
                    sqlQuery.Append("INICIF12|");

                    sqlQuery.Append("SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, '' v0_1, '' v0_2, '' v0_3,");
                    sqlQuery.Append("		v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,  ");
                    sqlQuery.Append("		v34,v35,v36,v37,v38,v39,v40,v41,v42,v43,v44,v45,v46,v47,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,  ");
                    sqlQuery.Append("		v65,v66,v67,v68,v69,v70,v71,v72,v73,v74,v75,v76,v77,v78,v79,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98,v99,  ");
                    sqlQuery.Append("		v100,v101,v102,v103,v104,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122,v123,v124,v125,v126,v127,  ");
                    sqlQuery.Append("		v128,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146,v147,v148,v149,v150,v151,v152,v153,v154,v155,  ");
                    sqlQuery.Append("		v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170,v171,v172,v173,v174,v175,v176,v177,v178,v179,v180,v181,v182,v183,  ");
                    sqlQuery.Append("		v184,v185,v186,v187,v188,v189,v190,v191,v192,v193,v194,v195,v196,v197,v198,v199,v200,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,  ");
                    sqlQuery.Append("		v212,v213,v214,v215,v216,v217,v218,v219,v220,v221,v222,v223,v224,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239,  ");
                    sqlQuery.Append("		v240,v241,v242,v243,v244,v245,v246,v247,v248,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262,v263,v264,v265,v266,v267,  ");
                    sqlQuery.Append("		v268,v269,v270,v271,v272,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285,v286,v287,v288,v289,v290,v291,v292,v293,v294,v295,  ");
                    sqlQuery.Append("		v296,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306   ");

                    sqlQuery.Append(" from ");
                    sqlQuery.Append("  dbo.Tb_CONTROL, ");
                    sqlQuery.Append("  Vista_DatosDBF  v, ");
                    sqlQuery.Append("  VW_EI_2_Fin c ");

                    sqlQuery.Append(" where ");
                    sqlQuery.AppendLine("       Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("       Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");


                    #endregion
                    break;
                case 14:
                    #region Capacitacion para el Trabajo
                    sqlQuery.Append("CAPA1F12|");
                    sqlQuery.Append("SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("       v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("       v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, v216 as TOTCURS, ");
                    sqlQuery.Append("		v150,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,  ");
                    sqlQuery.Append("		v31,v32,v33,v34,v35,v36,v37,v38,v39,v40,v41,v42,v43,v44,v45,v46,v47,v48,v49,v50,v51,v52,v53,v54,v55,v56,v156,v157,v57,v58,  ");
                    sqlQuery.Append("		v158,v159,v59,v60,v61,v62,v160,v161,v63,v64,v162,v163,v65,v66,v67,v68,v164,v165,v69,v70,v166,v167,v71,v72,v73,v74, ");
                    sqlQuery.Append("		v168,v169,v75,v76,v170,v171,v77,v78,v79,v80,v172,v173,v81,v82,v174,v175,v83,v84,v85,v86,v176,v177,  ");
                    sqlQuery.Append("		v87,v88,v178,v179,v89,v90,v91,v92,v180,v181,v93,v94,v182,v183,v95,v96,v97,v98,v184,v185,v99,v100,v186,v187,v101,v102,v103, ");
                    sqlQuery.Append("		v104,v188,v189,v105,v106,v190,v191,v107,v108,v109,v110,v192,v193,v111,  ");
                    sqlQuery.Append("		v112,v194,v195,v113,v114,v115,v116,v196,v197,v117,v118,v198,v199,v119,v120,v121,v122,v123,v200,v201,v124,v125,v202, ");
                    sqlQuery.Append("		v203,v126,v127,v128,v129,v130,v204,v205,v131,v132,v206,v207,v133,  ");
                    sqlQuery.Append("		v134,v135,v136,v137,v208,v209,v138,v139,v210,v211,v140,v141,v142,v143,v212,v213,v144,v145,v214,v215,v146,v147,v148,v151,v152,v153,v154,v155,v149 ");

                    sqlQuery.Append(" FROM  ");
                    sqlQuery.Append("   dbo.Tb_CONTROL, ");
                    sqlQuery.Append("   Vista_DatosDBF  v, ");
                    sqlQuery.Append("   VW_6C_Fin c ");
                    sqlQuery.Append(" WHERE  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                case 30:
                    #region PREESCOLAR INDIGENA
                    sqlQuery.Append("PREEIF12|");
                    sqlQuery.Append(" SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI,  ");
                    sqlQuery.Append(" v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append(" v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, ");
                    sqlQuery.Append(" v1,	   v2,	   v3,	   v4,	   v5,	   v6,	   v7,	   v8,	   v9,	   v10,	   v11,	   v12,	   v13, ");
                    sqlQuery.Append(" v14,	   v15,	   v16,	   v17,	   v18,	   v19,	   v20,	   v21,	   v22,	   v23,	   v24,	   v25,	   v26, ");
                    sqlQuery.Append(" v27,	   v28,	   v29,	   v30,	   v31,	   v32,	   v33,	   v34,	   v35,	   v36,	   v37,	   v38,	   v39, ");
                    sqlQuery.Append(" v40,	   v41,	   v42,	   v43,	   v44,	   v45,	   v46,	   v47,	   v48,	   v49,	   v50,	   v51,	   v52, ");
                    sqlQuery.Append(" v53,	   v54,	   v55,	   v56,	   v57,	   v58,	   v59,	   v60,	   v61,	   v62,	   v63,	   v64,	   v65, ");
                    sqlQuery.Append(" v66,	   v67,	   v68,	   v69,	   v70,	   v71,	   v72,	   v73,	   v74,	   v75,	   v76,	   v77,	   v78, ");
                    sqlQuery.Append(" v79,	   v80,	   v81,	   v82,	   v83,	   v84,	   v85,	   v86,	   v87,	   v88,	   v89,	   v90,	   v91, ");
                    sqlQuery.Append(" v92,	   v93,	   v94,	   v95,	   v96,	   v97,	   v98,	   v99,	   v100,   v101,   v102,   v103,   v104, ");
                    sqlQuery.Append(" v105,   v106,   v107,   v108,   v109,   v110,   v111,	   v112,   v113,   v114,   v115,   v116,   v117, ");
                    sqlQuery.Append(" v118,   v119,   v120,   v121,   v122,   v123,   v124,   v125,   v126,   v127,   v128,   v129,   v130, ");
                    sqlQuery.Append(" v131,	   v132,	   v133,	   v134,	   v135,	   v136,	   v137,	   v138,	   v139, ");
                    sqlQuery.Append(" v140,	   v141,	   v142,	   v143,	   v144,	   v145,	   v146,	   v147,	   v148, ");
                    sqlQuery.Append(" v149,	   v150,	   v151,	   v152,	   v153,	   v154,	   v155,	   v156,	   v157, ");
                    sqlQuery.Append(" v158,	   v159,	   v160,	   v161,	   v162,	   v163,	   v164,	   v165,	   v166, ");
                    sqlQuery.Append(" v167,	   v168,	   v169,	   v170,	   v171,	   v172,	   v173,	   v174,	   v175, ");
                    sqlQuery.Append(" v176,	   v177,	   v178,	   v179,	   v180,	   v181,	   v182,	   v183,	   v184, ");
                    sqlQuery.Append(" v185,	   v186,	   v187,	   v188,	   v189,	   v190,	   v191,	   v192,	   v193, ");
                    sqlQuery.Append(" v194,	    v195  ");

                    sqlQuery.Append(" FROM  ");
                    sqlQuery.Append("   dbo.Tb_CONTROL, ");
                    sqlQuery.Append("   Vista_DatosDBF  v, ");
                    sqlQuery.Append("   VW_PreescolarIndigena_Fin c ");
                    sqlQuery.Append(" WHERE  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                case 31:
                    #region PRIMARIA INDIGENA
                    sqlQuery.Append("PRIMIF12|");
                    sqlQuery.AppendLine("   SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.AppendLine("   v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.AppendLine("   v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, ");
                    sqlQuery.AppendLine("   c.v1,		c.v2,		/*c.v3,*/		c.v4,		c.v5,		c.v6,		c.v7,		c.v8,		c.v9,		c.v10,		c.v11,		c.v12,		c.v13,		c.v14,		c.v15,		c.v16,		c.v17,		c.v18,		c.v19,		c.v20, ");
                    sqlQuery.AppendLine("   c.v21,		c.v22,		c.v23,		c.v24,		c.v25,		c.v26,		c.v27,		c.v28,		c.v29,		c.v30,		c.v31,		c.v32,		c.v33,		c.v34,		c.v35,		c.v36,		c.v37,		c.v38,		c.v39,		c.v40, ");
                    sqlQuery.AppendLine("   c.v41,		c.v42,		c.v43,		c.v44,		c.v45,		c.v46,		c.v47,		c.v48,		c.v49,		c.v50,		c.v51,		c.v52,		c.v53,		c.v54,		c.v55,		c.v56,		c.v57,		c.v58,		c.v59,		c.v60, ");
                    sqlQuery.AppendLine("   c.v61,		c.v62,		c.v63,		c.v64,		c.v65,		c.v66,		c.v67,		c.v68,		c.v69,		c.v70,		c.v71,		c.v72,		c.v73,		c.v74,		c.v75,		c.v76,		c.v77,		c.v78,		c.v79,		c.v80, ");
                    sqlQuery.AppendLine("   c.v81,		c.v82,		c.v83,		c.v84,		c.v85,		c.v86,		c.v87,		c.v88,		c.v89,		c.v90,		c.v91,		c.v92,		c.v93,		c.v94,		c.v95,		c.v96,		c.v97,		c.v98,      c.v99,		c.v100, ");
                    sqlQuery.AppendLine("   c.v101,		c.v102,		c.v103,		c.v104,		c.v105,		c.v106,		c.v107,		c.v108,		c.v109,		c.v110,		c.v111,		c.v112,		c.v113,		c.v114,     c.v115,		c.v116,		c.v117,		c.v118,		c.v119,		c.v120,	");
                    sqlQuery.AppendLine("   c.v121,		c.v122,		c.v123,		c.v124,		c.v125,		c.v126,		c.v127,		c.v128,		c.v129,		c.v130,   	");
                    sqlQuery.AppendLine("   c.v131,		c.v132,		c.v133,		c.v134,		c.v135,		c.v136,		c.v137,		c.v138,		c.v139,		c.v140,		c.v141,		c.v142,		c.v143,		c.v144,		c.v145,		c.v146,		c.v147,		c.v148,		c.v149,		c.v150, ");
                    sqlQuery.AppendLine("   c.v151,		c.v152,		c.v153,		c.v154,		c.v155,		c.v156,		c.v157,		c.v158,		c.v159,		c.v160,		c.v161,		c.v162,		c.v163,		c.v164,		c.v165,		c.v166,		c.v167,		c.v168,		c.v169,		c.v170, ");
                    sqlQuery.AppendLine("   c.v171,		c.v172,		c.v173,		c.v174,		c.v175,		c.v176,		c.v177,		c.v178,		c.v179,		c.v180,		c.v181,		c.v182,		c.v183,		c.v184,		c.v185,		c.v186,		c.v187,		c.v188,		c.v189,		c.v190,	");
                    sqlQuery.AppendLine("   c.v191,		c.v192,		c.v193,		c.v194,		c.v195,		c.v196,		c.v197,		c.v198,		c.v199,		c.v200,		c.v201,		c.v202,		c.v203,		c.v204,		c.v205,		c.v206,		c.v207,		c.v208,		c.v209,		c.v210, ");
                    sqlQuery.AppendLine("   c.v211,		c.v212,		c.v213,		c.v214,		c.v215,		c.v216,		c.v217,		c.v218,		c.v219,		c.v220,		c.v221,		c.v222,		c.v223,		c.v224,		c.v225,		c.v226,		c.v227,		c.v228,		c.v229,		c.v230, ");
                    sqlQuery.AppendLine("   c.v231,		c.v232,		c.v233,		c.v234,		c.v235,		c.v236,		c.v237,		c.v238,		c.v239,		c.v240,		c.v241,		c.v242,		c.v243,		c.v244,		c.v245,		c.v246,		c.v247,		c.v248,		c.v249,		c.v250, ");
                    sqlQuery.AppendLine("   c.v251,		c.v252,		c.v253,		c.v254,		c.v255,		c.v256,		c.v257,		c.v258,		c.v259,		c.v260,		c.v261,		c.v262,		c.v263,		c.v264,		c.v265,		c.v266,		c.v267,		c.v268,		c.v269,		c.v270, ");
                    sqlQuery.AppendLine("   c.v271,		c.v272,		c.v273,		c.v274,		c.v275,		c.v276,		c.v277,		c.v278,		c.v279,		c.v280,		c.v281,		c.v282,		c.v283,		c.v284,		c.v285,		c.v286,		c.v287,		c.v288,		c.v289,		c.v290, ");
                    sqlQuery.AppendLine("   c.v291,		c.v292,		c.v293,		c.v294,		c.v295,		c.v296,		c.v297,		c.v298,		c.v299,		c.v300,		c.v301,		c.v302,		c.v303,		c.v304,		c.v305,		c.v306,		c.v307,		c.v308,		c.v309,		c.v310, ");
                    sqlQuery.AppendLine("   c.v311,		c.v312,		c.v313,		c.v314,		c.v315,		c.v316,		c.v317,		c.v318,		c.v319,		c.v320,		c.v321,		c.v322,		c.v323,		c.v324,		c.v325,		c.v326,		c.v327,		c.v328,		c.v329,		c.v330, ");
                    sqlQuery.AppendLine("   c.v331,		c.v332,		c.v333,		c.v334,		c.v335,		c.v336,		c.v337,		c.v338,		c.v339,		c.v340,		c.v341,		c.v342,		c.v343,		c.v344,		c.v345,		c.v346,		c.v347,		c.v348,		c.v349,		c.v350, ");
                    sqlQuery.AppendLine("   c.v351,		c.v352,		c.v353,		c.v354,		c.v355,		c.v356,		c.v357,		c.v358,		c.v359,		c.v360,		c.v361,		c.v362,		c.v363,		c.v364,		c.v365,		c.v366,		c.v367,		c.v368,		c.v369,		c.v370, ");
                    sqlQuery.AppendLine("   c.v371,		c.v372,		c.v373,		c.v374,		c.v375,		c.v376,		c.v377,		c.v378,		c.v379,		c.v380,		c.v381,		c.v382,		c.v383,		c.v384,		c.v385,		c.v386,		c.v387,		c.v388,		c.v389,		c.v390, ");
                    sqlQuery.AppendLine("   c.v391,		c.v392,		c.v393,		c.v394,		c.v395,		c.v396,		c.v397,		c.v398,		c.v399,		c.v400,		c.v401,		c.v402,		c.v403,		c.v404,		c.v405,		c.v406,		c.v407,		c.v408,		c.v409,		c.v410, ");
                    sqlQuery.AppendLine("   c.v411,		c.v412,		c.v413,		c.v414,		c.v415,		c.v416,		c.v417,		c.v418,		c.v419,		c.v420,		c.v421,		c.v422,		c.v423,		c.v424,		c.v425,		c.v426,		c.v427,		c.v428,		c.v429,		c.v430, ");
                    sqlQuery.AppendLine("   c.v431,		c.v432,		c.v433,		c.v434,		c.v435,		c.v436,		c.v437,		c.v438,		c.v439,		c.v440,		c.v441,		c.v442,		c.v443,		c.v444,		c.v445,		c.v446,		c.v447,		c.v448,		c.v449,		c.v450, ");
                    sqlQuery.AppendLine("   c.v451,		c.v452,		c.v453,		c.v454,		c.v455,		c.v456,		c.v457,		c.v458,		c.v459,		c.v460,		c.v461,		c.v462,		c.v463,		c.v464,		c.v465,		c.v466,		c.v467,		c.v468,		c.v469,		c.v470, ");
                    sqlQuery.AppendLine("   c.v471,		c.v472,		c.v473,		c.v474,		c.v475,		c.v476,		c.v477,		c.v478,		c.v479,		c.v480,		c.v481,		c.v482,		c.v483,		c.v484,		c.v485,		c.v486,		c.v487,		c.v488,		c.v489,		c.v490, ");
                    sqlQuery.AppendLine("   c.v491,		c.v492,		c.v493,		c.v494,		c.v495,		c.v496,		c.v497,		c.v498,		c.v499,		c.v500,		c.v501,		c.v502,		c.v503,		c.v504,		c.v505,		c.v506,		c.v507,		c.v508,		c.v509,		c.v510, ");
                    sqlQuery.AppendLine("   c.v511,		c.v512,		c.v513,		c.v514,		c.v515,		c.v516,		c.v517,		c.v518,		c.v519,		c.v520,		c.v521,		c.v522,		c.v523,		c.v524,		c.v525,		c.v526,		c.v527,		c.v528,		c.v529,		c.v530, ");
                    sqlQuery.AppendLine("   c.v531,		c.v532,		c.v533,		c.v534,		c.v535,		c.v536,		c.v537,		c.v538,		c.v539,		c.v540,		c.v541,		c.v542,		c.v543,		c.v544,		c.v545,		c.v546,		c.v547,		c.v548,		c.v549,		c.v550, ");
                    sqlQuery.AppendLine("   c.v551,		c.v552,		c.v553,		c.v554,		c.v555,		c.v556,		c.v557,		c.v558,		c.v559,		c.v560,		c.v561,		c.v562,		c.v563,		c.v564,		c.v565,		c.v566,		c.v567,		c.v568,		c.v569,		c.v570, ");
                    sqlQuery.AppendLine("   c.v571,		c.v572,		c.v573,		c.v574,		c.v575,		c.v576,		c.v577,		c.v578,		c.v579,		c.v580,		c.v581,		c.v582,		c.v583,		c.v584,		c.v585,		c.v586,		c.v587,		c.v588,		c.v589,		c.v590, ");
                    sqlQuery.AppendLine("   c.v591,		c.v592,		c.v593,		c.v594,		c.v595,		c.v596,		c.v597,		c.v598,		c.v599,		c.v600,		c.v601,		c.v602,		c.v603,		c.v604,		c.v605,		c.v606,		c.v607,		c.v608,		c.v609,		c.v610, ");
                    sqlQuery.AppendLine("   c.v611,		c.v612,		c.v613,		c.v614,		c.v615,		c.v616,		c.v617,		c.v618,		c.v619,		c.v620,		c.v621,		c.v622,		c.v623,		c.v624,		c.v625,		c.v626,		c.v627,		c.v628,		c.v629,		c.v630, ");
                    sqlQuery.AppendLine("   c.v631,		c.v632,		c.v633,		c.v634,		c.v635,		c.v636,		c.v637,		c.v638,		c.v639,		c.v640,		c.v641,		c.v642,		c.v643,		c.v644,		c.v645,		c.v646,		c.v647,		c.v648,		c.v649,		c.v650 ");
                    sqlQuery.AppendLine("   FROM  ");
                    sqlQuery.AppendLine("   dbo.Tb_CONTROL,  ");
                    sqlQuery.AppendLine("   Vista_DatosDBF  v,  ");
                    sqlQuery.AppendLine("   dbo.VW_PrimariaIndigena_Fin c  ");

                    sqlQuery.AppendLine(" WHERE  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");
                    #endregion
                    break;
                case 1000:
                    #region COMPLEMENTO FIN ( anexo )
                    sqlQuery.AppendLine("COMPLF12|");
                    sqlQuery.AppendLine(" SELECT  v.CLAVECCT, v.N_CLAVECCT, v.N_ENTIDAD, v.TURNO, v.MUNICIPIO, v.N_MUNICIPI, v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, ");
                    sqlQuery.AppendLine("   v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO,  ");
                    sqlQuery.AppendLine("   MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON,  ");
                    sqlQuery.AppendLine("   Anx660, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx26,   ");
                    sqlQuery.AppendLine("   Anx27, Anx28, Anx29, Anx30, Anx31, Anx32, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx42, Anx43, Anx44, Anx45, Anx46, Anx47,   ");
                    sqlQuery.AppendLine("   Anx48, Anx49, Anx50, Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx57, Anx58, Anx59, Anx60, Anx61, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68,   ");
                    sqlQuery.AppendLine("   Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx84, Anx85, Anx86, Anx87, Anx88, Anx89,   ");
                    sqlQuery.AppendLine("   Anx90, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, Anx104, Anx105, Anx106, Anx107, Anx108, Anx109,   ");
                    sqlQuery.AppendLine("   Anx110, Anx111, Anx112, Anx113, Anx114, Anx115, Anx116, Anx117, Anx118, Anx119, Anx120, Anx121, Anx122, Anx123, Anx124, Anx125, Anx126, Anx127, Anx128,   ");
                    sqlQuery.AppendLine("   Anx129, Anx130, Anx131, Anx132, Anx133, Anx134, Anx135, Anx136, Anx137, Anx138, Anx139, Anx140, Anx141, Anx142, Anx143, Anx144, Anx145, Anx146, Anx147,   ");
                    sqlQuery.AppendLine("   Anx148, Anx149, Anx150, Anx151, Anx152, Anx153, Anx154, Anx155, Anx156, Anx157, Anx158, Anx159, Anx160, Anx161, Anx162, Anx163, Anx164, Anx165, Anx166,   ");
                    sqlQuery.AppendLine("   Anx167, Anx168, Anx169, Anx170, Anx171, Anx172, Anx173, Anx174, Anx175, Anx176, Anx177, Anx178, Anx179, Anx180, Anx181, Anx182, Anx183, Anx184, Anx185,   ");
                    sqlQuery.AppendLine("   Anx186, Anx187, Anx188, Anx189, Anx190, Anx191, Anx192, Anx193, Anx194, Anx195, Anx196, Anx197, Anx198, Anx199, Anx200, Anx201, Anx202, Anx203, Anx204,   ");
                    sqlQuery.AppendLine("   Anx205, Anx206, Anx207, Anx208, Anx209, Anx210, Anx211, Anx212, Anx213, Anx214, Anx215, Anx216, Anx217, Anx218, Anx219, Anx220, Anx221, Anx222, Anx223,   ");
                    sqlQuery.AppendLine("   Anx224, Anx225, Anx226, Anx227, Anx228, Anx229, Anx230, Anx231, Anx232, Anx233, Anx234, Anx235, Anx236, Anx237, Anx238, Anx239, Anx240, Anx241, Anx242,   ");
                    sqlQuery.AppendLine("   Anx243, Anx244, Anx245, Anx246, Anx247, Anx248, Anx249, Anx250, Anx251, Anx252, Anx253, Anx254, Anx255, Anx256, Anx257, Anx258, Anx259, Anx260, Anx261,   ");
                    sqlQuery.AppendLine("   Anx262, Anx263, Anx264, Anx265, Anx266, Anx267, Anx268, Anx269, Anx270, Anx271, Anx272, Anx273, Anx274, Anx275, Anx276, Anx277, Anx278, Anx279, Anx280,   ");
                    sqlQuery.AppendLine("   Anx281, Anx282, Anx283, Anx284, Anx285, Anx286, Anx287, Anx288, Anx289, Anx290, Anx291, Anx292, Anx293, Anx294, Anx295, Anx296, Anx297, Anx298, Anx299,   ");
                    sqlQuery.AppendLine("   Anx300, Anx301, Anx302, Anx303, Anx304, Anx305, Anx306, Anx307, Anx308, Anx309, Anx310, Anx311, Anx312, Anx313, Anx314, Anx315, Anx316, Anx317, Anx318, ");
                    sqlQuery.AppendLine("   Anx319, Anx320, Anx321, Anx322, Anx323, Anx324, Anx325, Anx326,  ");

                    sqlQuery.AppendLine("   REPLACE(REPLACE(Anx327,'_',''),'0','') AS Anx327,  ");
		            sqlQuery.AppendLine("   REPLACE(REPLACE(Anx328,'_',''),'0','') AS Anx328, ");
		            sqlQuery.AppendLine("   CASE WHEN REPLACE(REPLACE(Anx327,'_',''),'0','') = '' AND REPLACE(REPLACE(Anx328,'_',''),'0','')='' AND REPLACE(REPLACE(Anx330 ,'_',''),'0','')='' THEN 'X' WHEN Anx329 = 'X' THEN 'X' ELSE ''END  AS Anx329,   ");
		            sqlQuery.AppendLine("   REPLACE(REPLACE(Anx330,'_',''),'0','') AS Anx330,  ");
		            sqlQuery.AppendLine("   REPLACE(REPLACE(Anx659,'_',''),'0','') AS Anx659,  ");
		            sqlQuery.AppendLine("   REPLACE(REPLACE(Anx331,'_',''),'0','') AS Anx331,  ");
		            sqlQuery.AppendLine("   REPLACE(REPLACE(Anx332,'_',''),'0','') AS Anx332, Anx645, Anx646, Anx647, Anx648, Anx649, Anx650, Anx651, Anx652, Anx653, Anx654, Anx655, Anx656, Anx657, Anx661, Anx662, Anx663, Anx658  ");

                    sqlQuery.AppendLine(" FROM   dbo.Tb_CONTROL,  ");
                    sqlQuery.AppendLine("   Vista_DatosDBF  v,  ");
                    sqlQuery.AppendLine("   VW_Anexo c  ");
                    sqlQuery.AppendLine(" WHERE  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_control			    = c.id_control ");
                    sqlQuery.AppendLine("   and  Tb_CONTROL.Id_CCTNT	        =* v.Id_CCTNT ");
                    sqlQuery.AppendLine("   and  Tb_CONTROL.id_tipoCuestionario = 2    ");

                    sqlQuery.AppendLine("   AND Tb_CONTROL.ID_CicloEscolar = @Id_cicloEscolar ");
                    sqlQuery.AppendLine("   AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine("   AND dbo.Tb_CONTROL.Estatus     > 0  ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT ");

                    #endregion
                    break;
                case 104:
                    #region Carreras Profesional Tecnico
                    sqlQuery.Append("PROF2F12|");

                    sqlQuery.Append("SELECT  ");
                    sqlQuery.Append("clave AS clavecct, c.ID_Turno AS Turno, numcarr,  ");
                    sqlQuery.Append("C1,CASE C2 WHEN '' THEN '_' ELSE C2 END AS C2,C3,CASE C4 WHEN '' THEN '_' ELSE C4 END AS C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20,  ");
                    sqlQuery.Append("C21,C22,C23,C24,C25,C26,C27,C28,C29,C30,C31,C32,C33,C34,C35,C36,C37,C38,C39,C40,  ");
                    sqlQuery.Append("C41,C42,C43,C44,C45,C46,C47,C48  ");

                    sqlQuery.Append(" FROM  ");
                    sqlQuery.Append("   dbo.Tb_CONTROL, ");
                    sqlQuery.Append("   Vista_DatosDBF  v, ");
                    sqlQuery.Append("   VW_8P_Carreras_Fin c ");
                    sqlQuery.Append(" WHERE  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                case 106:
                    #region Carreras Bachillerato Tecnologico
                    sqlQuery.Append("BACH2F12|");
                    sqlQuery.Append("SELECT  ");
                    sqlQuery.Append("   clave as clavecct,c.ID_Turno as Turno,numcarr,  ");
                    sqlQuery.Append("   C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20,C21,C22,  ");
                    sqlQuery.Append("   C23,C24,C25,C26,C27,C28,C29,C30,C31,C32,C33,C34,C35,C36,C37,C38,C39,C40,C41  ");

                    sqlQuery.Append(" FROM  ");
                    sqlQuery.Append("   dbo.Tb_CONTROL, ");
                    sqlQuery.Append("   Vista_DatosDBF  v, ");
                    sqlQuery.Append("   VW_8T_Carreras_Fin c ");
                    sqlQuery.Append(" WHERE  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                case 114:
                    #region Carreras Capacitacion para el Trabajo
                    sqlQuery.Append("CAPA2F12|");

                    sqlQuery.Append("SELECT  ");
                    sqlQuery.Append("clave as clavecct, c.ID_Turno as Turno, numcarr as NumProg, C59 as CVECURS,  ");
                    sqlQuery.Append("C1,C56,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20,C21,C22,  ");
                    sqlQuery.Append("C23,C24,C25,C26,C27,C28,C29,C30,C31,C32,C33,C34,C35,C36,C37,C38,C39,C40,C41,C42,  ");
                    sqlQuery.Append("C43,C44,C45,C46,C47,C48,C49,C50,C51,C52,C53,C54,C55  ");
                    sqlQuery.Append(" FROM  ");
                    sqlQuery.Append("   dbo.Tb_CONTROL, ");
                    sqlQuery.Append("   Vista_DatosDBF  v, ");
                    sqlQuery.Append("   VW_6C_Carreras_Fin c ");
                    sqlQuery.Append(" WHERE  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.Id_CCTNT			    = v.Id_CCTNT			and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_control			= c.id_control          and  ");
                    sqlQuery.AppendLine("   Tb_CONTROL.id_tipoCuestionario  = 2   ");

                    sqlQuery.AppendLine(" AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine(" AND v.entidad                  = @ID_Entidad ");
                    sqlQuery.AppendLine(" order by v.id_CCTNT  ");

                    #endregion
                    break;
                #endregion

                #region Inicio de cursos
                case 15:	
                    #region Inicial Escolarizado
                    sqlQuery.Append("INICII11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, '' v0_1, '' v0_2, '' v0_3,c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_EI_1 AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    

                    #endregion
                    break;
                case 16:	
                    #region Inicial no Escolarizado
                    sqlQuery.Append("NOESCI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, '' v0, c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.Append("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.Append("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.Append("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.Append("c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, ");
                    sqlQuery.Append("c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, ");
                    sqlQuery.Append("c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.Append("c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, ");
                    sqlQuery.Append("c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, ");
                    sqlQuery.Append("c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, ");
                    sqlQuery.Append("c.v610, c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626, c.v627, c.v628, ");
                    sqlQuery.Append("c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, c.v643, c.v644, c.v645, c.v646, c.v647, ");
                    sqlQuery.Append("c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, c.v665, c.v666, ");
                    sqlQuery.Append("c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674, c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, c.v681, c.v682, c.v683, c.v684, c.v685, ");
                    sqlQuery.Append("c.v686, c.v687, c.v688, c.v689, c.v690, c.v691, c.v692, c.v693, c.v694, c.v695, c.v696, c.v697, c.v698, c.v699, c.v700, c.v701, c.v702, c.v703, c.v704, ");
                    sqlQuery.Append("c.v705, c.v706, c.v707, c.v708 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_EI_NE1 AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 17:	
                    #region USAER
                    sqlQuery.Append("USAERI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.Append("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.Append("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.Append("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.Append("c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, ");
                    sqlQuery.Append("c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, ");
                    sqlQuery.Append("c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.Append("c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, ");
                    sqlQuery.Append("c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, ");
                    sqlQuery.Append("c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, ");
                    sqlQuery.Append("c.v610, c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626, c.v627, c.v628, ");
                    sqlQuery.Append("c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, c.v643, c.v644, c.v645, c.v646, c.v647, ");
                    sqlQuery.Append("c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, c.v665, c.v666, ");
                    sqlQuery.Append("c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674, c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, c.v681, c.v682, c.v683, c.v684, c.v685, ");
                    sqlQuery.Append("c.v686, c.v687, c.v688, c.v689, c.v690, c.v691, c.v692, c.v693, c.v694, c.v695, c.v696, c.v697, c.v698, c.v699, c.v700, c.v701, c.v702, c.v703, c.v704, ");
                    sqlQuery.Append("c.v705, c.v706, c.v707, c.v708, c.v709, c.v710, c.v711, c.v712, c.v713, c.v714, c.v715, c.v716, c.v717, c.v718, c.v719, c.v720, c.v721, c.v722, c.v723, ");
                    sqlQuery.Append("c.v724, c.v725, c.v726, c.v727, c.v728, c.v729, c.v730, c.v731, c.v732, c.v733, c.v734, c.v735, c.v736, c.v737, c.v738, c.v739, c.v740, c.v741, c.v742, ");
                    sqlQuery.Append("c.v743, c.v744, c.v745, c.v746, c.v747, c.v748, c.v749, c.v750, c.v751, c.v752, c.v753, c.v754, c.v755, c.v756, c.v757, c.v758, c.v759, c.v760, c.v761, ");
                    sqlQuery.Append("c.v762, c.v763, c.v764, c.v765, c.v766, c.v767, c.v768, c.v769, c.v770, c.v771, c.v772, c.v773, c.v774, c.v775, c.v776, c.v777, c.v778, c.v779, c.v780, ");
                    sqlQuery.Append("c.v781, c.v782, c.v783, c.v784, c.v785, c.v786, c.v787, c.v788, c.v789, c.v790, c.v791, c.v792, c.v793, c.v794, c.v795, c.v796, c.v797, c.v798, c.v799, ");
                    sqlQuery.Append("c.v800, c2.v801, c2.v802, c2.v803, c2.v804, c2.v805, c2.v806, c2.v807, c2.v808, c2.v809, c2.v810, c2.v811, c2.v812, c2.v813, c2.v814, c2.v815, c2.v816, c2.v817, c2.v818, ");
                    sqlQuery.Append("c2.v819, c2.v820, c2.v821, c2.v822, c2.v823, c2.v824, c2.v825, c2.v826, c2.v827, c2.v828, c2.v829, c2.v830, c2.v831, c2.v832, c2.v833, c2.v834, c2.v835, c2.v836, c2.v837, ");
                    sqlQuery.Append("c2.v838, c2.v839, c2.v840, c2.v841, c2.v842, c2.v843, c2.v844, c2.v845, c2.v846, c2.v847, c2.v848, c2.v849, c2.v850, c2.v851, c2.v852, c2.v853, c2.v854, c2.v855, c2.v856, ");
                    sqlQuery.Append("c2.v857, c2.v858, c2.v859, c2.v860, c2.v861, c2.v862, c2.v863, c2.v864, c2.v865, c2.v866, c2.v867, c2.v868, c2.v869, c2.v870, c2.v871, c2.v872, c2.v873, c2.v874, c2.v875, ");
                    sqlQuery.Append("c2.v876, c2.v877, c2.v878, c2.v879, c2.v880, c2.v881, c2.v882, c2.v883, c2.v884, c2.v885, c2.v886, c2.v887, c2.v888, c2.v889, c2.v890, c2.v891, c2.v892, c2.v893, c2.v894, ");
                    sqlQuery.Append("c2.v895, c2.v896, c2.v897, c2.v898, c2.v899, c2.v900, c2.v901, c2.v902, c2.v903, c2.v904, c2.v905, c2.v906, c2.v907, c2.v908, c2.v909, c2.v910, c2.v911, c2.v912, c2.v913, ");
                    sqlQuery.Append("c2.v914, c2.v915, c2.v916, c2.v917, c2.v918, c2.v919, c2.v920, c2.v921, c2.v922, c2.v923, c2.v924, c2.v925, c2.v926, c2.v927, c2.v928, c2.v929, c2.v930, c2.v931, c2.v932, ");
                    sqlQuery.Append("c2.v933, c2.v934, c2.v935, c2.v936, c2.v937, c2.v938, c2.v939, c2.v940, c2.v941, c2.v942, c2.v943, c2.v944, c2.v945, c2.v946, c2.v947, c2.v948, c2.v949, c2.v950, c2.v951, ");
                    sqlQuery.Append("c2.v952, c2.v953, c2.v954, c2.v955, c2.v956, c2.v957, c2.v958, c2.v959, c2.v960, c2.v961, c2.v962, c2.v963, c2.v964, c2.v965, c2.v966, c2.v967, c2.v968, c2.v969, c2.v970, ");
                    sqlQuery.Append("c2.v971, c2.v972, c2.v973, c2.v974, c2.v975, c2.v976, c2.v977, c2.v978, c2.v979, c2.v980, c2.v981, c2.v982, c2.v983, c2.v984, c2.v985, c2.v986, c2.v987, c2.v988, c2.v989,  ");
                    sqlQuery.Append("c2.v990, c2.v991, c2.v992, c2.v993, c2.v994, c2.v995, c2.v996, c2.v997, c2.v998, c2.v999, c2.v1000, c2.v1001, c2.v1002, c2.v1003, c2.v1004, c2.v1005, c2.v1006, c2.v1007, c2.v1008, c2.v1009, c2.v1010, ");
                    sqlQuery.Append("c2.v1011, c2.v1012, c2.v1013, c2.v1014, c2.v1015, c2.v1016, c2.v1017, c2.v1018, c2.v1019, c2.v1020, c2.v1021, c2.v1022, c2.v1023, c2.v1024, c2.v1025, c2.v1026, c2.v1027, c2.v1028, c2.v1029, c2.v1030, ");
                    sqlQuery.Append("c2.v1031, c2.v1032, c2.v1033, c2.v1034, c2.v1035, c2.v1036, c2.v1037, c2.v1038, c2.v1039, c2.v1040, ");
                    sqlQuery.Append("c2.v1041, c2.v1042, c2.v1043, c2.v1044, c2.v1045, c2.v1046, c2.v1047, c2.v1048, c2.v1049, c2.v1050, c2.v1051, c2.v1052, c2.v1053, c2.v1054, c2.v1055, c2.v1056, c2.v1057, c2.v1058, c2.v1059, c2.v1060, ");
                    sqlQuery.Append("c2.v1061, c2.v1062, c2.v1063, c2.v1064, c2.v1065, c2.v1066, c2.v1067, c2.v1068, c2.v1069, c2.v1070, c2.v1071, c2.v1072, c2.v1073, c2.v1074, c2.v1075, c2.v1076, c2.v1077, c2.v1078, c2.v1079, c2.v1080, ");
                    sqlQuery.Append("c2.v1081, c2.v1082, c2.v1083, c2.v1084, c2.v1085, c2.v1086, c2.v1087, c2.v1088, c2.v1089, c2.v1090, c2.v1091, c2.v1092, c2.v1093, c2.v1094, c2.v1095, c2.v1096, c2.v1097, c2.v1098, c2.v1099, c2.v1100, ");
                    sqlQuery.Append("c2.v1101, c2.v1102, c2.v1103, c2.v1104, c2.v1105, c2.v1106, c2.v1107, c2.v1108, c2.v1109, c2.v1110, c2.v1111, c2.v1112, c2.v1113, c2.v1114, c2.v1115, c2.v1116, c2.v1117, c2.v1118, c2.v1119, c2.v1120, ");
                    sqlQuery.Append("c2.v1121, c2.v1122, c2.v1123, c2.v1124, c2.v1125, c2.v1126, c2.v1127, c2.v1128, c2.v1129, c2.v1130, c2.v1131, c2.v1132, c2.v1133, c2.v1134, c2.v1135, c2.v1136, c2.v1137, c2.v1138, c2.v1139, c2.v1140, ");
                    sqlQuery.Append("c2.v1141, c2.v1142, c2.v1143, c2.v1144, c2.v1145, c2.v1146, c2.v1147, c2.v1148, c2.v1149, c2.v1150, c2.v1151, c2.v1152, c2.v1153, c2.v1154, c2.v1155, c2.v1156, c2.v1157, c2.v1158, c2.v1159, c2.v1160, ");
                    sqlQuery.Append("c2.v1161, c2.v1162, c2.v1163, c2.v1164, c2.v1165, c2.v1166, c2.v1167, c2.v1168, c2.v1169, c2.v1170, c2.v1171, c2.v1172, c2.v1173, c2.v1174, c2.v1175, c2.v1176, c2.v1177, c2.v1178, c2.v1179, c2.v1180, ");
                    sqlQuery.Append("c2.v1181, c2.v1182, c2.v1183, c2.v1184, c2.v1185, c2.v1186, c2.v1187, c2.v1188, c2.v1189, c2.v1190, c2.v1191, c2.v1192, c2.v1193, c2.v1194, c2.v1195, c2.v1196, c2.v1197, c2.v1198, c2.v1199, c2.v1200, ");
                    sqlQuery.Append("c2.v1201, c2.v1202, c2.v1203, c2.v1204, c2.v1205, c2.v1206, c2.v1207, c2.v1208, c2.v1209, c2.v1210, c2.v1211, c2.v1212, c2.v1213, c2.v1214, c2.v1215, c2.v1216, c2.v1217, c2.v1218, c2.v1219, c2.v1220, ");
                    sqlQuery.Append("c2.v1221, c2.v1222, c2.v1223, c2.v1224, c2.v1225, c2.v1226, c2.v1227, c2.v1228, c2.v1229, c2.v1230, c2.v1231, c2.v1232, c2.v1233, c2.v1234, c2.v1235, c2.v1236, c2.v1237, c2.v1238, c2.v1239, c2.v1240, ");
                    sqlQuery.Append("c2.v1241, c2.v1242, c2.v1243, c2.v1244, c2.v1245, c2.v1246, c2.v1247, c2.v1248, c2.v1249, c2.v1250, c2.v1251, c2.v1252, c2.v1253, c2.v1254, c2.v1255, c2.v1256, c2.v1257, c2.v1258, c2.v1259, c2.v1260, ");
                    sqlQuery.Append("c2.v1261, c2.v1262, c2.v1263, c2.v1264, c2.v1265, c2.v1266, c2.v1267, c2.v1268, c2.v1269, c2.v1270, c2.v1271, c2.v1272, c2.v1273, c2.v1274, c2.v1275, c2.v1276, c2.v1277, c2.v1278, c2.v1279, c2.v1280, ");
                    sqlQuery.Append("c2.v1281, c2.v1282, c2.v1283, c2.v1284, c2.v1285, c2.v1286, c2.v1287, c2.v1288, c2.v1289, c2.v1290, c2.v1291, c2.v1292, c2.v1293, c2.v1294, c2.v1295, c2.v1296, c2.v1297, c2.v1298, c2.v1299, c2.v1300, ");
                    sqlQuery.Append("c2.v1301, c2.v1302, c2.v1303, c2.v1304, c2.v1305, c2.v1306, c2.v1307, c2.v1308, c2.v1309, c2.v1310, c2.v1311, c2.v1312, c2.v1313, c2.v1314, c2.v1315, c2.v1316, c2.v1317, c2.v1318, c2.v1319, c2.v1320, ");
                    sqlQuery.Append("c2.v1321, c2.v1322, c2.v1323, c2.v1324, c2.v1325, c2.v1326, c2.v1327, c2.v1328, c2.v1329, c2.v1330, c2.v1331, c2.v1332, c2.v1333, c2.v1334, c2.v1335, c2.v1336, c2.v1337, c2.v1338, c2.v1339, c2.v1340, ");
                    sqlQuery.Append("c2.v1341, c2.v1342, c2.v1343, c2.v1344, c2.v1345, c2.v1346, c2.v1347, c2.v1348, c2.v1349, c2.v1350, c2.v1351, c2.v1352, c2.v1353, c2.v1354, c2.v1355, c2.v1356, c2.v1357, c2.v1358, c2.v1359, c2.v1360, ");
                    sqlQuery.Append("c2.v1361, c2.v1362, c2.v1363, c2.v1364, c2.v1365, c2.v1366, c2.v1367, c2.v1368, c2.v1369, c2.v1370, c2.v1371, c2.v1372, c2.v1373, c2.v1374, c2.v1375, c2.v1376, c2.v1377, c2.v1378, c2.v1379, c2.v1380, ");
                    sqlQuery.Append("c2.v1381, c2.v1382, c2.v1383, c2.v1384, c2.v1385, c2.v1386, c2.v1387, c2.v1388, c2.v1389, c2.v1390, c2.v1391, c2.v1392, c2.v1393, c2.v1394, c2.v1395, c2.v1396, c2.v1397, c2.v1398, c2.v1399, c2.v1400, ");
                    sqlQuery.Append("c2.v1401, c2.v1402, c2.v1403, c2.v1404, c2.v1405, c2.v1406, c2.v1407, c2.v1408, c2.v1409, c2.v1410, c2.v1411, c2.v1412, c2.v1413, c2.v1414, c2.v1415, c2.v1416, c2.v1417, c2.v1418, c2.v1419, c2.v1420, ");
                    sqlQuery.Append("c2.v1421, c2.v1422, c2.v1423, c2.v1448, c2.v1424, c2.v1425, c2.v1426, c2.v1427, c2.v1428, c2.v1429, c2.v1430, c2.v1431, c2.v1432, c2.v1433, c2.v1434, c2.v1435, c2.v1436, c2.v1437, c2.v1438, c2.v1439, c2.v1440, ");
                    sqlQuery.Append("c2.v1441, c2.v1442, c2.v1443, c2.v1444, c2.v1445, c2.v1446, c2.v1447 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_Usaer_Inicio_1 AS c ON Tb_CONTROL.ID_Control = c.ID_Control LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_Usaer_Inicio_2 AS c2 ON Tb_CONTROL.ID_Control = c2.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 18:	    
                    #region CAM
                    sqlQuery.Append("CAMI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, '' as id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c2.v1194, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c2.v1195, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c2.v1196, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c2.v1197, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c2.v1198, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c2.v1199, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c2.v1200, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c2.v1201, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c2.v1202, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.Append("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.Append("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.Append("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.Append("c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, ");
                    sqlQuery.Append("c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, ");
                    sqlQuery.Append("c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.Append("c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, ");
                    sqlQuery.Append("c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, ");
                    sqlQuery.Append("c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c2.v601, c2.v602, c2.v603, c2.v604, c2.v605, c2.v606, c2.v607, c2.v608, c2.v609, ");
                    sqlQuery.Append("c2.v610, c2.v611, c2.v612, c2.v613, c2.v614, c2.v615, c2.v616, c2.v617, c2.v618, c2.v619, c2.v620, c2.v621, c2.v622, c2.v623, c2.v624, c2.v625, c2.v626, c2.v627, c2.v628, ");
                    sqlQuery.Append("c2.v629, c2.v630, c2.v631, c2.v632, c2.v633, c2.v634, c2.v635, c2.v636, c2.v637, c2.v638, c2.v639, c2.v640, c2.v641, c2.v642, c2.v643, c2.v644, c2.v645, c2.v646, c2.v647, ");
                    sqlQuery.Append("c2.v648, c2.v649, c2.v650, c2.v651, c2.v652, c2.v653, c2.v654, c2.v655, c2.v656, c2.v657, c2.v658, c2.v659, c2.v660, c2.v661, c2.v662, c2.v663, c2.v664, c2.v665, c2.v666, ");
                    sqlQuery.Append("c2.v667, c2.v668, c2.v669, c2.v670, c2.v671, c2.v672, c2.v673, c2.v674, c2.v675, c2.v676, c2.v677, c2.v678, c2.v679, c2.v680, c2.v681, c2.v682, c2.v683, c2.v684, c2.v685, ");
                    sqlQuery.Append("c2.v686, c2.v687, c2.v688, c2.v689, c2.v690, c2.v691, c2.v692, c2.v693, c2.v694, c2.v695, c2.v696, c2.v697, c2.v698, c2.v699, c2.v700, c2.v701, c2.v702, c2.v703, c2.v704, ");
                    sqlQuery.Append("c2.v705, c2.v706, c2.v707, c2.v708, c2.v709, c2.v710, c2.v711, c2.v712, c2.v713, c2.v714, c2.v715, c2.v716, c2.v717, c2.v718, c2.v719, c2.v720, c2.v721, c2.v722, c2.v723, ");
                    sqlQuery.Append("c2.v724, c2.v725, c2.v726, c2.v727, c2.v728, c2.v729, c2.v730, c2.v731, c2.v732, c2.v733, c2.v734, c2.v735, c2.v736, c2.v737, c2.v738, c2.v739, c2.v740, c2.v741, c2.v742, ");
                    sqlQuery.Append("c2.v743, c2.v744, c2.v745, c2.v746, c2.v747, c2.v748, c2.v749, c2.v750, c2.v751, c2.v752, c2.v753, c2.v754, c2.v755, c2.v756, c2.v757, c2.v758, c2.v759, c2.v760, c2.v761, ");
                    sqlQuery.Append("c2.v762, c2.v763, c2.v764, c2.v765, c2.v766, c2.v767, c2.v768, c2.v769, c2.v770, c2.v771, c2.v772, c2.v773, c2.v774, c2.v775, c2.v776, c2.v777, c2.v778, c2.v779, c2.v780, ");
                    sqlQuery.Append("c2.v781, c2.v782, c2.v783, c2.v784, c2.v785, c2.v786, c2.v787, c2.v788, c2.v789, c2.v790, c2.v791, c2.v792, c2.v793, c2.v794, c2.v795, c2.v796, c2.v797, c2.v798, c2.v799, ");
                    sqlQuery.Append("c2.v800, c2.v801, c2.v802, c2.v803, c2.v804, c2.v805, c2.v806, c2.v807, c2.v808, c2.v809, c2.v810, c2.v811, c2.v812, c2.v813, c2.v814, c2.v815, c2.v816, c2.v817, c2.v818, ");
                    sqlQuery.Append("c2.v819, c2.v820, c2.v821, c2.v822, c2.v823, c2.v824, c2.v825, c2.v826, c2.v827, c2.v828, c2.v829, c2.v830, c2.v831, c2.v832, c2.v833, c2.v834, c2.v835, c2.v836, c2.v837, ");
                    sqlQuery.Append("c2.v838, c2.v839, c2.v840, c2.v841, c2.v842, c2.v843, c2.v844, c2.v845, c2.v846, c2.v847, c2.v848, c2.v849, c2.v850, c2.v851, c2.v852, c2.v853, c2.v854, c2.v855, c2.v856, ");
                    sqlQuery.Append("c2.v857, c2.v858, c2.v859, c2.v860, c2.v861, c2.v862, c2.v863, c2.v864, c2.v865, c2.v866, c2.v867, c2.v868, c2.v869, c2.v870, c2.v871, c2.v872, c2.v873, c2.v874, c2.v875, ");
                    sqlQuery.Append("c2.v876, c2.v877, c2.v878, c2.v879, c2.v880, c2.v881, c2.v882, c2.v883, c2.v884, c2.v885, c2.v886, c2.v887, c2.v888, c2.v889, c2.v890, c2.v891, c2.v892, c2.v893, c2.v894, ");
                    sqlQuery.Append("c2.v895, c2.v896, c2.v897, c2.v898, c2.v899, c2.v900, c2.v901, c2.v902, c2.v903, c2.v904, c2.v905, c2.v906, c2.v907, c2.v908, c2.v909, c2.v910, c2.v911, c2.v912, c2.v913, ");
                    sqlQuery.Append("c2.v914, c2.v915, c2.v916, c2.v917, c2.v918, c2.v919, c2.v920, c2.v921, c2.v922, c2.v923, c2.v924, c2.v925, c2.v926, c2.v927, c2.v928, c2.v929, c2.v930, c2.v931, c2.v932, ");
                    sqlQuery.Append("c2.v933, c2.v934, c2.v935, c2.v936, c2.v937, c2.v938, c2.v939, c2.v940, c2.v941, c2.v942, c2.v943, c2.v944, c2.v945, c2.v946, c2.v947, c2.v948, c2.v949, c2.v950, c2.v951, ");
                    sqlQuery.Append("c2.v952, c2.v953, c2.v954, c2.v955, c2.v956, c2.v957, c2.v958, c2.v959, c2.v960, c2.v961, c2.v962, c2.v963, c2.v964, c2.v965, c2.v966, c2.v967, c2.v968, c2.v969, c2.v970, ");
                    sqlQuery.Append("c2.v971, c2.v972, c2.v973, c2.v974, c2.v975, c2.v976, c2.v977, c2.v978, c2.v979, c2.v980, c2.v981, c2.v982, c2.v983, c2.v984, c2.v985, c2.v986, c2.v987, c2.v988, c2.v989,  ");
                    sqlQuery.Append("c2.v990, c2.v991, c2.v992, c2.v993, c2.v994, c2.v995, c2.v996, c2.v997, c2.v998, c2.v999, c2.v1000, c2.v1001, c2.v1002, c2.v1003, c2.v1004, c2.v1005, c2.v1006, c2.v1007, c2.v1008, c2.v1009, c2.v1010, ");
                    sqlQuery.Append("c2.v1011, c2.v1012, c2.v1013, c2.v1014, c2.v1015, c2.v1016, c2.v1017, c2.v1018, c2.v1019, c2.v1020, c2.v1021, c2.v1022, c2.v1023, c2.v1024, c2.v1025, c2.v1026, c2.v1027, c2.v1028, c2.v1029, c2.v1030, ");
                    sqlQuery.Append("c2.v1031, c2.v1032, c2.v1033, c2.v1034, c2.v1035, c2.v1036, c2.v1037, c2.v1038, c2.v1039, c2.v1040, ");
                    sqlQuery.Append("c2.v1041, c2.v1042, c2.v1043, c2.v1044, c2.v1045, c2.v1046, c2.v1047, c2.v1048, c2.v1049, c2.v1050, c2.v1051, c2.v1052, c2.v1053, c2.v1054, c2.v1055, c2.v1056, c2.v1057, c2.v1058, c2.v1059, c2.v1060, ");
                    sqlQuery.Append("c2.v1061, c2.v1062, c2.v1063, c2.v1064, c2.v1065, c2.v1066, c2.v1067, c2.v1068, c2.v1069, c2.v1070, c2.v1071, c2.v1072, c2.v1073, c2.v1074, c2.v1075, c2.v1076, c2.v1077, c2.v1078, c2.v1079, c2.v1080, ");
                    sqlQuery.Append("c2.v1081, c2.v1082, c2.v1083, c2.v1084, c2.v1085, c2.v1086, c2.v1087, c2.v1088, c2.v1089, c2.v1090, c2.v1091, c2.v1092, c2.v1093, c2.v1094, c2.v1095, c2.v1096, c2.v1097, c2.v1098, c2.v1099, c2.v1100, ");
                    sqlQuery.Append("c2.v1101, c2.v1102, c2.v1103, c2.v1104, c2.v1105, c2.v1106, c2.v1107, c2.v1108, c2.v1109, c2.v1110, c2.v1111, c2.v1112, c2.v1113, c2.v1114, c2.v1115, c2.v1116, c2.v1117, c2.v1118, c2.v1119, c2.v1120, ");
                    sqlQuery.Append("c2.v1121, c2.v1122, c2.v1123, c2.v1124, c2.v1125, c2.v1126, c2.v1127, c2.v1128, c2.v1129, c2.v1130, c2.v1131, c2.v1132, c2.v1133, c2.v1134, c2.v1135, c2.v1136, c2.v1137, c2.v1138, c2.v1139, c2.v1140, ");
                    sqlQuery.Append("c2.v1141, c2.v1142, c2.v1143, c2.v1144, c2.v1145, c2.v1146, c2.v1147, c2.v1148, c2.v1149, c2.v1150, c2.v1151, c2.v1152, c2.v1153, c2.v1154, c2.v1155, c2.v1156, c2.v1157, c2.v1158, c2.v1159, c2.v1160, ");
                    sqlQuery.Append("c2.v1161, c2.v1162, c2.v1163, c2.v1164, c2.v1165, c2.v1166, c2.v1167, c2.v1168, c2.v1169, c2.v1170, c2.v1171, c2.v1172, c2.v1173, c2.v1174, c2.v1175, c2.v1176, c2.v1177, c2.v1178, c2.v1179, c2.v1180, ");
                    sqlQuery.Append("c2.v1181, c2.v1182, c2.v1183, c2.v1184, c2.v1185, c2.v1186, c2.v1187, c2.v1188, c2.v1189, c2.v1190, c2.v1191, c2.v1192, c2.v1193 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("(SELECT   distinct  Id_CentroTrabajo, CLAVECCT, N_CLAVECCT, TURNO, N_ENTIDAD, MUNICIPIO, N_MUNICIPI, LOCALIDAD, N_LOCALIDA, DOMICILIO, DEPADMVA, DEPNORMTVA, ZONAESCOLA, SECTOR, DIRSERVREG, SOSTENIMIE, SERVICIO, UNIDADRESP, PROGRAMA, SUBPROG, RENGLON, N_RENGLON, PERIODO, MOTIVO, DISPON FROM Vista_DatosDBF) AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_CAM_Inicio1 AS c ON Tb_CONTROL.ID_Control = c.ID_Control LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_CAM_Inicio2 AS c2 ON Tb_CONTROL.ID_Control = c2.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    #endregion
                    break;
                case 19:	
                    #region Preescolar General
                    sqlQuery.Append("PREEGI11|");
                    sqlQuery.Append("select Tb_CONTROL.id_control, ");
                    sqlQuery.Append("v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, ");
                    sqlQuery.Append("v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, ");
                    sqlQuery.Append("v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, ");
                    sqlQuery.Append("c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, ");
                    sqlQuery.Append("c.v28, c.v29, c.v30, c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, c.v53, c.v54, c.v55, c.v56, c.v57, ");
                    sqlQuery.Append("c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, ");
                    sqlQuery.Append("c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, ");
                    sqlQuery.Append("c.v115, c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, c.v135, c.v136, c.v137, c.v138, ");
                    sqlQuery.Append("c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, ");
                    sqlQuery.Append("c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, ");
                    sqlQuery.Append("c.v187, c.v188, c.v189, c.v190, c.v191, c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, c.v230, c.v231, c.v232, c.v233, c.v234, ");
                    sqlQuery.Append("c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, ");
                    sqlQuery.Append("c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, ");
                    sqlQuery.Append("c.v283, c.v284, c.v285, c.v286, c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, c.v306, ");
                    sqlQuery.Append("c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, ");
                    sqlQuery.Append("c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, ");
                    sqlQuery.Append("c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, ");
                    sqlQuery.Append("c.v379, c.v380, c.v381, c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, c.v401, c.v402, ");
                    sqlQuery.Append("c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, ");
                    sqlQuery.Append("c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, ");
                    sqlQuery.Append("c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, ");
                    sqlQuery.Append("c.v475, c.v476, c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, c.v496, c.v497, c.v498, ");
                    sqlQuery.Append("c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, ");
                    sqlQuery.Append("c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, ");
                    sqlQuery.Append("c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, c.v553, c.v554, c.v555, c.v556 ");
                    sqlQuery.Append("from ");
                    sqlQuery.Append("dbo.Tb_CONTROL, ");
                    sqlQuery.Append("Vista_DatosDBF v, ");
                    sqlQuery.Append("vw_Preesc_G_Inicio c ");
                    sqlQuery.Append("where ");
                    sqlQuery.Append("Tb_CONTROL.Id_CentroTrabajo=v.id_CentroTrabajo and ");
                    sqlQuery.Append("Tb_CONTROL.Id_Turno=v.Turno and ");
                    sqlQuery.Append("Tb_CONTROL.id_control=c.id_control and ");
                    sqlQuery.Append("Tb_CONTROL.id_tipoCuestionario=1 and ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.Append("AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.Append(" order by v.id_CCTNT ");
                    #endregion
                    break;
                case 20:		
                    #region Preescolar Comunitaria
                    sqlQuery.Append("PREECI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, '' v0, c.v1, c.v2, c.v3, c.v4, c.v54, c.v55, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_Preescolar_Conafe_Inicio AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 21:		
                    #region Preescolar Indigena
                    sqlQuery.AppendLine("PREEII11|");
                    sqlQuery.AppendLine("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI,");
                    sqlQuery.AppendLine("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO,");
                    sqlQuery.AppendLine("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON,");
                    sqlQuery.AppendLine("c.v1,c.v2,c.v3, c.v4, c.v5, c.v6, c.v7, c.v8,c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15,");
                    sqlQuery.AppendLine("c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22,c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29,");
                    sqlQuery.AppendLine("c.v30, c.v31, c.v32, c.v33, c.v34, c.v35, c.v36,c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43,");
                    sqlQuery.AppendLine("c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50,c.v51, c.v52, c.v53, c.v54, c.v55, c.v56, c.v57,");
                    sqlQuery.AppendLine("c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64,c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71,");
                    sqlQuery.AppendLine("c.v72, c.v73, c.v74, c.v75, c.v76, c.v77, c.v78,c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85,");
                    sqlQuery.AppendLine("c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92,c.v93, c.v94, c.v95, c.v96, c.v97, c.v98, c.v99,");
                    sqlQuery.AppendLine("c.v100, c.v101, c.v102, c.v103, c.v104, c.v105,c.v106, c.v107, c.v108, c.v109, c.v110, c.v111,");
                    sqlQuery.AppendLine("c.v112, c.v113, c.v114, c.v115, c.v116, c.v117,c.v118, c.v119, c.v120, c.v121, c.v122, c.v123,");
                    sqlQuery.AppendLine("c.v124, c.v125, c.v126, c.v127, c.v128, c.v129,c.v130, c.v131, c.v132, c.v133, c.v134, c.v135,");
                    sqlQuery.AppendLine("c.v136, c.v137, c.v138, c.v139, c.v140, c.v141,c.v142, c.v143, c.v144, c.v145, c.v146, c.v147,");
                    sqlQuery.AppendLine("c.v148, c.v149, c.v150, c.v151, c.v152, c.v153,c.v154, c.v155, c.v156, c.v157, c.v158, c.v159,");
                    sqlQuery.AppendLine("c.v160, c.v161, c.v162, c.v163, c.v164, c.v165,c.v166, c.v167, c.v168, c.v169, c.v170, c.v171,");
                    sqlQuery.AppendLine("c.v172, c.v173, c.v174, c.v175, c.v176, c.v177,c.v178, c.v179, c.v180, c.v181, c.v182, c.v183,");
                    sqlQuery.AppendLine("c.v184, c.v185, c.v186, c.v187, c.v188, c.v189,c.v190, c.v191, c.v192, c.v193, c.v194, c.v195,");
                    sqlQuery.AppendLine("c.v196, c.v197, c.v198, c.v199, c.v200, c.v201,c.v202, c.v203, c.v204, c.v205, c.v206, c.v207,");
                    sqlQuery.AppendLine("c.v208, c.v209, c.v210, c.v211, c.v212, c.v213,c.v214, c.v215, c.v216, c.v217, c.v218, c.v219,");
                    sqlQuery.AppendLine("c.v220, c.v221, c.v222, c.v223, c.v224, c.v225,c.v226, c.v227, c.v228, c.v229, c.v230, c.v231,");
                    sqlQuery.AppendLine("c.v232, c.v233, c.v234, c.v235, c.v236, c.v237,c.v238, c.v239, c.v240, c.v241, c.v242, c.v243,");
                    sqlQuery.AppendLine("c.v244, c.v245, c.v246, c.v247, c.v248, c.v249,c.v250, c.v251, c.v252, c.v253, c.v254, c.v255,");
                    sqlQuery.AppendLine("c.v256, c.v257, c.v258, c.v259, c.v260, c.v261,c.v262, c.v263, c.v264, c.v265, c.v266, c.v267,");
                    sqlQuery.AppendLine("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273,c.v274, c.v275, c.v276, c.v277, c.v278, c.v279,");
                    sqlQuery.AppendLine("c.v280, c.v281, c.v282, c.v283, c.v284, c.v285,c.v286, c.v287, c.v288, c.v289, c.v290, c.v291,");
                    sqlQuery.AppendLine("c.v292, c.v293, c.v294, c.v295, c.v296, c.v297,c.v298, c.v299, c.v300, c.v301, c.v302, c.v303,");
                    sqlQuery.AppendLine("c.v304, c.v305, c.v306, c.v307, c.v308, c.v309,c.v310, c.v311, c.v312, c.v313, c.v314, c.v315,");
                    sqlQuery.AppendLine("c.v316, c.v317, c.v318, c.v319, c.v320, c.v321,c.v322, c.v323, c.v324, c.v325, c.v326, c.v327,");
                    sqlQuery.AppendLine("c.v328, c.v329, c.v330, c.v331, c.v332, c.v333,c.v334, c.v335, c.v336, c.v337, c.v338, c.v339,");
                    sqlQuery.AppendLine("c.v340, c.v341, c.v342, c.v343, c.v344, c.v345,c.v346, c.v347, c.v348, c.v349, c.v350, c.v351,");
                    sqlQuery.AppendLine("c.v352, c.v353, c.v354, c.v355, c.v356, c.v357,c.v358, c.v359, c.v360, c.v361, c.v362, c.v363,");
                    sqlQuery.AppendLine("c.v364, c.v365, c.v366, c.v367, c.v368, c.v369,c.v370, c.v371, c.v372, c.v373, c.v374, c.v375,");
                    sqlQuery.AppendLine("c.v376, c.v377, c.v378, c.v379, c.v380, c.v381,c.v382, c.v383, c.v384, c.v385, c.v386, c.v387,");
                    sqlQuery.AppendLine("c.v388, c.v389, c.v390, c.v391, c.v392, c.v393,c.v394, c.v395, c.v396, c.v397, c.v398, c.v399,");
                    sqlQuery.AppendLine("c.v400, c.v401, c.v402, c.v403, c.v404, c.v405,c.v406, c.v407, c.v408, c.v409, c.v411, c.v412,");
                    sqlQuery.AppendLine("c.v413,c.v414");
                    sqlQuery.AppendLine("FROM Tb_CONTROL INNER JOIN");
                    sqlQuery.AppendLine("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN");
                    sqlQuery.AppendLine("VW_PreescolarIndigena_Ini AS c ON Tb_CONTROL.ID_Control = c.ID_Control");
                    sqlQuery.AppendLine("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND (Tb_CONTROL.Estatus > 0)");
                    sqlQuery.AppendLine("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario)");
                    sqlQuery.AppendLine("AND (Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar)");
                    sqlQuery.AppendLine("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 22:		
                    #region Primaria General
                    sqlQuery.Append("PRIMGI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.Append("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.Append("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.Append("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.Append("c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, ");
                    sqlQuery.Append("c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, ");
                    sqlQuery.Append("c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.Append("c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, ");
                    sqlQuery.Append("c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, ");
                    sqlQuery.Append("c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, ");
                    sqlQuery.Append("c.v610, c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626, c.v627, c.v628, ");
                    sqlQuery.Append("c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, c.v643, c.v644, c.v645, c.v646, c.v647, ");
                    sqlQuery.Append("c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, c.v665, c.v666, ");
                    sqlQuery.Append("c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674, c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, c.v681, c.v682, c.v683, c.v684, c.v685, ");
                    sqlQuery.Append("c.v686, c.v687, c.v688, c.v689, c.v690, c.v691, c.v692, c.v693, c.v694, c.v695, c.v696, c.v697, c.v698, c.v699, c.v700, c.v701, c.v702, c.v703, c.v704, ");
                    sqlQuery.Append("c.v705, c.v706, c.v707, c.v708, c.v709, c.v710, c.v711, c.v712, c.v713, c.v714, c.v715, c.v716, c.v717, c.v718, c.v719, c.v720, c.v721, c.v722, c.v723, ");
                    sqlQuery.Append("c.v724, c.v725, c.v726, c.v727, c.v728, c.v729, c.v730, c.v731, c.v732, c.v733, c.v734, c.v735, c.v736, c.v737, c.v738, c.v739, c.v740, c.v741, c.v742, ");
                    sqlQuery.Append("c.v743, c.v744, c.v745, c.v746, c.v747, c.v748, c.v749, c.v750, c.v751, c.v752, c.v753, c.v754, c.v755, c.v756, c.v757, c.v758, c.v759, c.v760, c.v761, ");
                    sqlQuery.Append("c.v762, c.v763, c.v764, c.v765, c.v766, c.v767, c.v768, c.v769, c.v770, c.v771, c.v772, c.v773, c.v774, c.v775, c.v776, c.v777, c.v778, c.v779, c.v780, ");
                    sqlQuery.Append("c.v781, c.v782, c.v783, c.v784, c.v785, c.v786, c.v787, c.v788, c.v789, c.v790, c.v791, c.v792, c.v793, c.v794, c.v795, c.v796, c.v797, c.v798, c.v799, ");
                    sqlQuery.Append("c.v800, c.v801, c.v802, c.v803, c.v804, c.v805, c.v806, c.v807, c.v808, c.v809, c.v810, c.v811, c.v812, c.v813, c.v814, c.v815, c.v816, c.v817, c.v818, ");
                    sqlQuery.Append("c.v819, c.v820, c.v821, c.v822, c.v823, c.v824, c.v825, c.v826, c.v827, c.v828, c.v829, c.v830, c.v831, c.v832, c.v833, c.v834, c.v835, c.v836, c.v837, ");
                    sqlQuery.Append("c.v838, c.v839, c.v840, c.v841, c.v842, c.v843, c.v844, c.v845, c.v846, c.v847, c.v848, c.v849, c.v850, c.v851, c.v852, c.v853, c.v854, c.v855, c.v856, ");
                    sqlQuery.Append("c.v857, c.v858, c.v859, c.v860, c.v861, c.v862, c.v863, c.v864, c.v865, c.v866, c.v867, c.v868, c.v869, c.v870, c.v871, c.v872, c.v873, c.v874, c.v875, ");
                    sqlQuery.Append("c.v876, c.v877, c.v878, c.v879, c.v880, c.v881, c.v882, c.v883, c.v884, c.v885, c.v886, c.v887, c.v888, c.v889, c.v890, c.v891, c.v892, c.v893, c.v894, ");
                    sqlQuery.Append("c.v895, c.v896, c.v897, c.v898, c.v899, c.v900, c.v901, c.v902, c.v903, c.v904, c.v905, c.v906, c.v907, c.v908, c.v909, c.v910, c.v911, c.v912, c.v913, ");
                    sqlQuery.Append("c.v914, c.v915, c.v916, c.v917, c.v918, c.v919, c.v920, c.v921, c.v922, c.v923 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_Primaria_G_Inicio AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 23:		
                    #region Primaria Comunitaria
                    sqlQuery.Append("PRIMCI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, '' NIVEL1, '' NIVEL2, '' NIVEL3, '' NIVEL4, '' NIVEL5, '' NIVEL6, c.v1, c.v2, c.v3, c.v410, c.v411, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_Primaria_Conafe_Inicio AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 24:	
                    #region Primaria Indigena
                    sqlQuery.AppendLine("PRIMII11|");
                    sqlQuery.AppendLine("SELECT v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.AppendLine("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.AppendLine("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, ");
                    sqlQuery.AppendLine("c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.AppendLine("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.AppendLine("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.AppendLine("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.AppendLine("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.AppendLine("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.AppendLine("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.AppendLine("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.AppendLine("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.AppendLine("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.AppendLine("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.AppendLine("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.AppendLine("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.AppendLine("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266,c.v267, ");
                    sqlQuery.AppendLine("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.AppendLine("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.AppendLine("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.AppendLine("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.AppendLine("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.AppendLine("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.AppendLine("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.AppendLine("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.AppendLine("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.AppendLine("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.AppendLine("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.AppendLine("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.AppendLine("c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, ");
                    sqlQuery.AppendLine("c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, ");
                    sqlQuery.AppendLine("c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.AppendLine("c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, ");
                    sqlQuery.AppendLine("c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, ");
                    sqlQuery.AppendLine("c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c2.v601, c2.v602, c2.v603, c2.v604, c2.v605, c2.v606, c2.v607, c2.v608, c2.v609, ");
                    sqlQuery.AppendLine("c2.v610, c2.v611, c2.v612, c2.v613, c2.v614, c2.v615, c2.v616, c2.v617, c2.v618, c2.v619, c2.v620, c2.v621, c2.v622, c2.v623, c2.v624, c2.v625, c2.v626, c2.v627, c2.v628, ");
                    sqlQuery.AppendLine("c2.v629, c2.v630, c2.v631, c2.v632, c2.v633, c2.v634, c2.v635, c2.v636, c2.v637, c2.v638, c2.v639, c2.v640, c2.v641, c2.v642, c2.v643, c2.v644, c2.v645, c2.v646, c2.v647, ");
                    sqlQuery.AppendLine("c2.v648, c2.v649, c2.v650,c2.v651, c2.v652, c2.v653, c2.v654, c2.v655, c2.v656, c2.v657, c2.v658, c2.v659, c2.v660, c2.v661, c2.v662, c2.v663, c2.v664, c2.v665, c2.v666,");
                    sqlQuery.AppendLine("c2.v667, c2.v668, c2.v669, c2.v670, c2.v671, c2.v672, c2.v673, c2.v674, c2.v675, c2.v676, c2.v677, c2.v678, c2.v679, c2.v680, c2.v681, c2.v682, c2.v683, c2.v684, c2.v685,");
                    sqlQuery.AppendLine("c2.v686, c2.v687, c2.v688, c2.v689, c2.v690, c2.v691, c2.v692, c2.v693, c2.v694, c2.v695, c2.v696, c2.v697, c2.v698, c2.v699, c2.v700, c2.v701, c2.v702, c2.v703, c2.v704,");
                    sqlQuery.AppendLine("c2.v705, c2.v706, c2.v707, c2.v708, c2.v709, c2.v710, c2.v711, c2.v712, c2.v713, c2.v714, c2.v715, c2.v716, c2.v717, c2.v718, c2.v719, c2.v720, c2.v721, c2.v722, c2.v723,");
                    sqlQuery.AppendLine("c2.v724, c2.v725, c2.v726, c2.v727, c2.v728, c2.v729, c2.v730, c2.v731, c2.v732, c2.v733, c2.v734");
                    sqlQuery.AppendLine("FROM ");
                    sqlQuery.AppendLine("dbo.Tb_CONTROL,Vista_DatosDBFv,VW_PrimariaIndigena_Ini1 c,VW_PrimariaIndigena_Ini2 c2 ");
                    sqlQuery.AppendLine("WHERE ");
                    sqlQuery.AppendLine("Tb_CONTROL.Id_CCTNT = v.Id_CCTNT and");
                    sqlQuery.AppendLine("Tb_CONTROL.id_control = c.id_control and");
                    sqlQuery.AppendLine("Tb_CONTROL.id_control = c2.id_control and");
                    sqlQuery.AppendLine("Tb_CONTROL.id_tipoCuestionario= 1 ");
                    sqlQuery.AppendLine("AND Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario ");
                    sqlQuery.AppendLine("AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.AppendLine("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 25:		
                    #region Secundaria
                    sqlQuery.Append("SECUNI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.Append("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.Append("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.Append("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.Append("c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, ");
                    sqlQuery.Append("c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, ");
                    sqlQuery.Append("c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.Append("c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, ");
                    sqlQuery.Append("c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, ");
                    sqlQuery.Append("c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, ");
                    sqlQuery.Append("c.v610, c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626, c.v627, c.v628, ");
                    sqlQuery.Append("c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, c.v643, c.v644, c.v645, c.v646, c.v647, ");
                    sqlQuery.Append("c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, c.v665, c.v666, ");
                    sqlQuery.Append("c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674, c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, c.v681, c.v682, c.v683, c.v684, c.v685, ");
                    sqlQuery.Append("c.v686, c.v687, c.v688, c.v689, c.v690, c.v691, c.v692, c.v693, c.v694, c.v695, c.v696, c.v697, c.v698, c.v699, c.v700, c.v701, c.v702, c.v703, c.v704, ");
                    sqlQuery.Append("c.v705, c.v706, c.v707, c.v708, c.v709, c.v710, c.v711, c.v712, c.v713, c.v714, c.v715, c.v716, c.v717, c.v718, c.v719, c.v720, c.v721, c.v722, c.v723, ");
                    sqlQuery.Append("c.v724, c.v725, c.v726, c.v727, c.v728, c.v729, c.v730, c.v731, c.v732, c.v733, c.v734, c.v735, c.v736, c.v737, c.v738, c.v739 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_Secundaria_G_Inicio AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 26:		
                    #region Bachillerato General
                    sqlQuery.Append("BACHGI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.Append("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.Append("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.Append("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.Append("c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, ");
                    sqlQuery.Append("c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, ");
                    sqlQuery.Append("c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.Append("c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, ");
                    sqlQuery.Append("c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, ");
                    sqlQuery.Append("c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, ");
                    sqlQuery.Append("c.v610, c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626, c.v627, c.v628, ");
                    sqlQuery.Append("c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, c.v643, c.v644, c.v645, c.v646, c.v647, ");
                    sqlQuery.Append("c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, c.v665, c.v666, ");
                    sqlQuery.Append("c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674, c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, c.v681, c.v682, c.v683, c.v684, c.v685, ");
                    sqlQuery.Append("c.v686, c.v687, c.v688, c.v689, c.v690, c.v691, c.v692, c.v693, c.v694, c.v695, c.v696, c.v697, c.v698, c.v699, c.v700, c.v701, c.v702, c.v703, c.v704, ");
                    sqlQuery.Append("c.v705, c.v706, c.v707, c.v708, c.v709, c.v710, c.v711, c.v712, c.v713, c.v714, c.v715, c.v716, c.v717, c.v718, c.v719, c.v720, c.v721, c.v722, c.v723, ");
                    sqlQuery.Append("c.v724, c.v725, c.v726, c.v727, c.v728, c.v729, c.v730, c.v731, c.v732, c.v733, c.v734, c.v735, c.v736, c.v737, c.v738, c.v739, c.v740, c.v741, c.v742, ");
                    sqlQuery.Append("c.v743, c.v744, c.v745, c.v746, c.v747, c.v748, c.v749, c.v750, c.v751, c.v752, c.v753, c.v754, c.v755, c.v756, c.v757, c.v758, c.v759, c.v760, c.v761, ");
                    sqlQuery.Append("c.v762, c.v763, c.v764, c.v765, c.v766, c.v767, c.v768, c.v769, c.v770, c.v771, c.v772, c.v773, c.v774, c.v775, c.v776, c.v777, c.v778, c.v779, c.v780, ");
                    sqlQuery.Append("c.v781, c.v782, c.v783, c.v784, c.v785, c.v786, c.v787, c.v788, c.v789, c.v790, c.v791, c.v792, c.v793, c.v794, c.v795, c.v796, c.v797, c.v798, c.v799, ");
                    sqlQuery.Append("c.v800, c.v801, c.v802, c.v803, c.v804, c.v805, c.v806, c.v807, c.v808, c.v809, c.v810, c.v811, c.v812, c.v813, c.v814, c.v815, c.v816, c.v817, c.v818, ");
                    sqlQuery.Append("c.v819, c.v820, c.v821, c.v822, c.v823, c.v824, c.v825, c.v826, c.v827, c.v828, c.v829, c.v830, c.v831, c.v832, c.v833, c.v834, c.v835, c.v836, c.v837, ");
                    sqlQuery.Append("c.v838, c.v839, c.v840, c.v841, c.v842, c.v843, c.v844, c.v845, c.v846, c.v847, c.v848, c.v849, c.v850, c.v851, c.v852, c.v853, c.v854, c.v855, c.v856, ");
                    sqlQuery.Append("c.v857, c.v858, c.v859, c.v860, c.v861, c.v862, c.v863, c.v864, c.v865, c.v866, c.v867, c.v868, c.v869, c.v870, c.v871, c.v872 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_7G_Inicio AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 27:		
                    #region Bachillerato Tecnologico
                    sqlQuery.Append("BACH1I11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO,MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, (SELECT COUNT(ID_CARRERA) FROM TB_CARRERAS WHERE TB_CARRERAS.ID_CONTROL = TB_CONTROL.ID_CONTROL) AS TOTALCARR, c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, ");
                    sqlQuery.Append("c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, ");
                    sqlQuery.Append("c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, ");
                    sqlQuery.Append("c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, ");
                    sqlQuery.Append("c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, ");
                    sqlQuery.Append("c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, c.v115, ");
                    sqlQuery.Append("c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, ");
                    sqlQuery.Append("c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, ");
                    sqlQuery.Append("c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, ");
                    sqlQuery.Append("c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, c.v191, ");
                    sqlQuery.Append("c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ");
                    sqlQuery.Append("c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, ");
                    sqlQuery.Append("c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, c.v248, ");
                    sqlQuery.Append("c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, c.v267, ");
                    sqlQuery.Append("c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, c.v286, ");
                    sqlQuery.Append("c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, c.v305, ");
                    sqlQuery.Append("c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, c.v324, ");
                    sqlQuery.Append("c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, c.v343, ");
                    sqlQuery.Append("c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, c.v362, ");
                    sqlQuery.Append("c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, c.v381, ");
                    sqlQuery.Append("c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c.v391, c.v392, c.v393, c.v394, c.v395, c.v396, c.v397, c.v398, c.v399, c.v400, ");
                    sqlQuery.Append("c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, c.v415, c.v416, c.v417, c.v418, c.v419, ");
                    sqlQuery.Append("c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, c.v434, c.v435, c.v436, c.v437, c.v438, ");
                    sqlQuery.Append("c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, c.v453, c.v454, c.v455, c.v456, c.v457, ");
                    sqlQuery.Append("c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, c.v472, c.v473, c.v474, c.v475, c.v476, ");
                    sqlQuery.Append("c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, c.v491, c.v492, c.v493, c.v494, c.v495, ");
                    sqlQuery.Append("c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, c.v510, c.v511, c.v512, c.v513, c.v514, ");
                    sqlQuery.Append("c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, c.v529, c.v530, c.v531, c.v532, c.v533, ");
                    sqlQuery.Append("c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, c.v548, c.v549, c.v550, c.v551, c.v552, ");
                    sqlQuery.Append("c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, c.v567, c.v568, c.v569, c.v570, c.v571, ");
                    sqlQuery.Append("c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, c.v586, c.v587, c.v588, c.v589, c.v590, ");
                    sqlQuery.Append("c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, c.v605, c.v606, c.v607, c.v608, c.v609, ");
                    sqlQuery.Append("c.v610, c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, c.v624, c.v625, c.v626, c.v627, c.v628, ");
                    sqlQuery.Append("c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, c.v643, c.v644, c.v645, c.v646, c.v647, ");
                    sqlQuery.Append("c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, c.v662, c.v663, c.v664, c.v665, c.v666, ");
                    sqlQuery.Append("c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674, c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, c.v681, c.v682, c.v683, c.v684, c.v685, ");
                    sqlQuery.Append("c.v686, c.v687, c.v688, c.v689, c.v690, c.v691, c.v692, c.v693, c.v694, c.v695, c.v696, c.v697, c.v698, c.v699, c.v700, c.v701, c.v702, c.v703, c.v704, ");
                    sqlQuery.Append("c.v705, c.v706, c.v707, c.v708, c.v709, c.v710, c.v711, c.v712, c.v713, c.v714, c.v715, c.v716, c.v717, c.v718, c.v719, c.v720, c.v721, c.v722, c.v723, ");
                    sqlQuery.Append("c.v724, c.v725, c.v726, c.v727, c.v728, c.v729, c.v730, c.v731, c.v732, c.v733, c.v734, c.v735, c.v736, c.v737, c.v738, c.v739, c.v740, c.v741, c.v742, ");
                    sqlQuery.Append("c.v743, c.v744, c.v745, c.v746, c.v747, c.v748, c.v749, c.v750, c.v751, c.v752, c.v753, c.v754, c.v755, c.v756, c.v757, c.v758, c.v759, c.v760, c.v761, ");
                    sqlQuery.Append("c.v762, c.v763, c.v764, c.v765, c.v766, c.v767, c.v768, c.v769, c.v770, c.v771, c.v772, c.v773, c.v774, c.v775, c.v776, c.v777, c.v778, c.v779, c.v780, ");
                    sqlQuery.Append("c.v781, c.v782, c.v783, c.v784, c.v785, c.v786, c.v787, c.v788, c.v789, c.v790, c.v791, c.v792, c.v793, c.v794, c.v795, c.v796, c.v797, c.v798, c.v799, ");
                    sqlQuery.Append("c.v800, c.v801, c.v802, c.v803, c.v804, c.v805, c.v806, c.v807, c.v808, c.v809, c.v810, c.v811, c.v812, c.v813, c.v814, c.v815, c.v816, c.v817, c.v818, ");
                    sqlQuery.Append("c.v819, c.v820, c.v821, c.v822, c.v823, c.v824, c.v825, c.v826, c.v827, c.v828, c.v829, c.v830, c.v831, c.v832, c.v833, c.v834, c.v835, c.v836, c.v837, ");
                    sqlQuery.Append("c.v838, c.v839, c.v840, c.v841, c.v842, c.v843, c.v844, c.v845 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_7T_Inicio AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 28:		
                    #region Profesional T�cnico
                    sqlQuery.Append("PROF1I11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, ");
                    sqlQuery.Append("v.LOCALIDAD, v.N_LOCALIDA, v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, ");
                    sqlQuery.Append("v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, (SELECT     COUNT(id_Carrera) FROM Tb_Carreras WHERE (id_Control = Tb_CONTROL.ID_Control)) AS TOTALCARR, c.v1, c.v2, c.v3, c.v4, c.v5, c.v6, c.v7, c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, ");
                    sqlQuery.Append("c.v15, c.v16, c.v17, c.v18, c2.v886, c2.v887, c2.v888, c2.v889, c2.v890, c2.v891, c.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, ");
                    sqlQuery.Append("c.v29, c.v30, c.v31, c.v32, c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50,");
                    sqlQuery.Append("c.v51, c.v52, c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, ");
                    sqlQuery.Append("c.v73, c.v74, c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, ");
                    sqlQuery.Append("c.v95, c.v96, c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, ");
                    sqlQuery.Append("c.v115, c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, ");
                    sqlQuery.Append("c.v134, c.v135, c.v136, c.v137, c.v138, c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, ");
                    sqlQuery.Append("c.v153, c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, ");
                    sqlQuery.Append("c.v172, c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, c.v187, c.v188, c.v189, c.v190, ");
                    sqlQuery.Append("c.v191, c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, ");
                    sqlQuery.Append("c.v210, c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, ");
                    sqlQuery.Append("c.v229, c.v230, c.v231, c.v232, c.v233, c.v234, c.v235, c.v236, c.v237, c.v238, c.v239, c.v240, c.v241, c.v242, c.v243, c.v244, c.v245, c.v246, c.v247, ");
                    sqlQuery.Append("c.v248, c.v249, c.v250, c.v251, c.v252, c.v253, c.v254, c.v255, c.v256, c.v257, c.v258, c.v259, c.v260, c.v261, c.v262, c.v263, c.v264, c.v265, c.v266, ");
                    sqlQuery.Append("c.v267, c.v268, c.v269, c.v270, c.v271, c.v272, c.v273, c.v274, c.v275, c.v276, c.v277, c.v278, c.v279, c.v280, c.v281, c.v282, c.v283, c.v284, c.v285, ");
                    sqlQuery.Append("c.v286, c.v287, c.v288, c.v289, c.v290, c.v291, c.v292, c.v293, c.v294, c.v295, c.v296, c.v297, c.v298, c.v299, c.v300, c.v301, c.v302, c.v303, c.v304, ");
                    sqlQuery.Append("c.v305, c.v306, c.v307, c.v308, c.v309, c.v310, c.v311, c.v312, c.v313, c.v314, c.v315, c.v316, c.v317, c.v318, c.v319, c.v320, c.v321, c.v322, c.v323, ");
                    sqlQuery.Append("c.v324, c.v325, c.v326, c.v327, c.v328, c.v329, c.v330, c.v331, c.v332, c.v333, c.v334, c.v335, c.v336, c.v337, c.v338, c.v339, c.v340, c.v341, c.v342, ");
                    sqlQuery.Append("c.v343, c.v344, c.v345, c.v346, c.v347, c.v348, c.v349, c.v350, c.v351, c.v352, c.v353, c.v354, c.v355, c.v356, c.v357, c.v358, c.v359, c.v360, c.v361, ");
                    sqlQuery.Append("c.v362, c.v363, c.v364, c.v365, c.v366, c.v367, c.v368, c.v369, c.v370, c.v371, c.v372, c.v373, c.v374, c.v375, c.v376, c.v377, c.v378, c.v379, c.v380, ");
                    sqlQuery.Append("c.v381, c.v382, c.v383, c.v384, c.v385, c.v386, c.v387, c.v388, c.v389, c.v390, c2.v883, c2.v884, c2.v885, c.v391, c.v392, c.v393, c.v394, c.v395, ");
                    sqlQuery.Append("c.v396, c.v397, c.v398, c.v399, c.v400, c.v401, c.v402, c.v403, c.v404, c.v405, c.v406, c.v407, c.v408, c.v409, c.v410, c.v411, c.v412, c.v413, c.v414, ");
                    sqlQuery.Append("c.v415, c.v416, c.v417, c.v418, c.v419, c.v420, c.v421, c.v422, c.v423, c.v424, c.v425, c.v426, c.v427, c.v428, c.v429, c.v430, c.v431, c.v432, c.v433, ");
                    sqlQuery.Append("c.v434, c.v435, c.v436, c.v437, c.v438, c.v439, c.v440, c.v441, c.v442, c.v443, c.v444, c.v445, c.v446, c.v447, c.v448, c.v449, c.v450, c.v451, c.v452, ");
                    sqlQuery.Append("c.v453, c.v454, c.v455, c.v456, c.v457, c.v458, c.v459, c.v460, c.v461, c.v462, c.v463, c.v464, c.v465, c.v466, c.v467, c.v468, c.v469, c.v470, c.v471, ");
                    sqlQuery.Append("c.v472, c.v473, c.v474, c.v475, c.v476, c.v477, c.v478, c.v479, c.v480, c.v481, c.v482, c.v483, c.v484, c.v485, c.v486, c.v487, c.v488, c.v489, c.v490, ");
                    sqlQuery.Append("c.v491, c.v492, c.v493, c.v494, c.v495, c.v496, c.v497, c.v498, c.v499, c.v500, c.v501, c.v502, c.v503, c.v504, c.v505, c.v506, c.v507, c.v508, c.v509, ");
                    sqlQuery.Append("c.v510, c.v511, c.v512, c.v513, c.v514, c.v515, c.v516, c.v517, c.v518, c.v519, c.v520, c.v521, c.v522, c.v523, c.v524, c.v525, c.v526, c.v527, c.v528, ");
                    sqlQuery.Append("c.v529, c.v530, c.v531, c.v532, c.v533, c.v534, c.v535, c.v536, c.v537, c.v538, c.v539, c.v540, c.v541, c.v542, c.v543, c.v544, c.v545, c.v546, c.v547, ");
                    sqlQuery.Append("c.v548, c.v549, c.v550, c.v551, c.v552, c.v553, c.v554, c.v555, c.v556, c.v557, c.v558, c.v559, c.v560, c.v561, c.v562, c.v563, c.v564, c.v565, c.v566, ");
                    sqlQuery.Append("c.v567, c.v568, c.v569, c.v570, c.v571, c.v572, c.v573, c.v574, c.v575, c.v576, c.v577, c.v578, c.v579, c.v580, c.v581, c.v582, c.v583, c.v584, c.v585, ");
                    sqlQuery.Append("c.v586, c.v587, c.v588, c.v589, c.v590, c.v591, c.v592, c.v593, c.v594, c.v595, c.v596, c.v597, c.v598, c.v599, c.v600, c.v601, c.v602, c.v603, c.v604, ");
                    sqlQuery.Append("c.v605, c.v606, c.v607, c.v608, c.v609, c.v610, c.v611, c.v612, c.v613, c.v614, c.v615, c.v616, c.v617, c.v618, c.v619, c.v620, c.v621, c.v622, c.v623, ");
                    sqlQuery.Append("c.v624, c.v625, c.v626, c.v627, c.v628, c.v629, c.v630, c.v631, c.v632, c.v633, c.v634, c.v635, c.v636, c.v637, c.v638, c.v639, c.v640, c.v641, c.v642, ");
                    sqlQuery.Append("c.v643, c.v644, c.v645, c.v646, c.v647, c.v648, c.v649, c.v650, c.v651, c.v652, c.v653, c.v654, c.v655, c.v656, c.v657, c.v658, c.v659, c.v660, c.v661, ");
                    sqlQuery.Append("c.v662, c.v663, c.v664, c.v665, c.v666, c.v667, c.v668, c.v669, c.v670, c.v671, c.v672, c.v673, c.v674, c.v675, c.v676, c.v677, c.v678, c.v679, c.v680, ");
                    sqlQuery.Append("c.v681, c.v682, c.v683, c.v684, c.v685, c.v686, c.v687, c.v688, c.v689, c.v690, c.v691, c.v692, c.v693, c.v694, c.v695, c.v696, c.v697, c.v698, c.v699, ");
                    sqlQuery.Append("c.v700, c2.v701, c2.v702, c2.v703, c2.v704, c2.v705, c2.v706, c2.v707, c2.v708, c2.v709, c2.v710, c2.v711, c2.v712, c2.v713, c2.v714, c2.v715, ");
                    sqlQuery.Append("c2.v716, c2.v717, c2.v718, c2.v719, c2.v720, c2.v721, c2.v722, c2.v723, c2.v724, c2.v725, c2.v726, c2.v727, c2.v728, c2.v729, c2.v730, c2.v731, ");
                    sqlQuery.Append("c2.v732, c2.v733, c2.v734, c2.v735, c2.v736, c2.v737, c2.v738, c2.v739, c2.v740, c2.v741, c2.v742, c2.v743, c2.v744, c2.v745, c2.v746, c2.v747, ");
                    sqlQuery.Append("c2.v748, c2.v749, c2.v750, c2.v751, c2.v752, c2.v753, c2.v754, c2.v755, c2.v756, c2.v757, c2.v758, c2.v759, c2.v760, c2.v761, c2.v762, c2.v763, ");
                    sqlQuery.Append("c2.v764, c2.v765, c2.v766, c2.v767, c2.v768, c2.v769, c2.v770, c2.v771, c2.v772, c2.v773, c2.v774, c2.v775, c2.v776, c2.v777, c2.v778, c2.v779, ");
                    sqlQuery.Append("c2.v780, c2.v781, c2.v782, c2.v783, c2.v784, c2.v785, c2.v786, c2.v787, c2.v788, c2.v789, c2.v790, c2.v791, c2.v792, c2.v793, c2.v794, c2.v795, ");
                    sqlQuery.Append("c2.v796, c2.v797, c2.v798, c2.v799, c2.v800, c2.v801, c2.v802, c2.v803, c2.v804, c2.v805, c2.v806, c2.v807, c2.v808, c2.v809, c2.v810, c2.v811, ");
                    sqlQuery.Append("c2.v812, c2.v813, c2.v814, c2.v815, c2.v816, c2.v817, c2.v818, c2.v819, c2.v820, c2.v821, c2.v822, c2.v823, c2.v824, c2.v825, c2.v826, c2.v827, ");
                    sqlQuery.Append("c2.v828, c2.v829, c2.v830, c2.v831, c2.v832, c2.v833, c2.v834, c2.v835, c2.v836, c2.v837, c2.v838, c2.v839, c2.v840, c2.v841, c2.v842, c2.v843, ");
                    sqlQuery.Append("c2.v844, c2.v845, c2.v846, c2.v847, c2.v848, c2.v849, c2.v850, c2.v851, c2.v852, c2.v853, c2.v854, c2.v855, c2.v856, c2.v857, c2.v858, c2.v859, ");
                    sqlQuery.Append("c2.v860, c2.v861, c2.v862, c2.v863, c2.v864, c2.v865, c2.v866, c2.v867, c2.v868, c2.v869, c2.v870, c2.v871, c2.v872, c2.v873, c2.v874, c2.v875, ");
                    sqlQuery.Append("c2.v876, c2.v877, c2.v878, c2.v879, c2.v880, c2.v881, c2.v892, c2.v893, c2.v894, c2.v895, c2.v896, c2.v897, c2.v898, c2.v899, c2.v900, c2.v901, ");
                    sqlQuery.Append("c2.v902, c2.v903, c2.v904, c2.v905, c2.v906, c2.v907, c2.v908, c2.v909, c2.v910, c2.v911, c2.v912, c2.v913, c2.v914, c2.v915, c2.v916, c2.v917, ");
                    sqlQuery.Append("c2.v918, c2.v919, c2.v920, c2.v921, c2.v922, c2.v923, c2.v924, c2.v925, c2.v926, c2.v927, c2.v928, c2.v929, c2.v930, c2.v931, c2.v932, c2.v933, ");
                    sqlQuery.Append("c2.v934, c2.v935, c2.v936, c2.v937, c2.v938, c2.v939, c2.v940, c2.v941, c2.v942, c2.v943, c2.v944, c2.v945, c2.v946, c2.v947, c2.v948, c2.v949, ");
                    sqlQuery.Append("c2.v950, c2.v951, c2.v952, c2.v953, c2.v954, c2.v955, c2.v956, c2.v957, c2.v958, c2.v959, c2.v960, c2.v961, c2.v962, c2.v963, c2.v964, c2.v965, ");
                    sqlQuery.Append("c2.v966, c2.v967, c2.v968, c2.v969, c2.v970, c2.v971, c2.v972, c2.v973, c2.v974, c2.v975, c2.v976, c2.v977, c2.v978, c2.v979, c2.v980, c2.v981, ");
                    sqlQuery.Append("c2.v982, c2.v983, c2.v984, c2.v985, c2.v986, c2.v987, c2.v988, c2.v989, c2.v990, c2.v991, c2.v992, c2.v993, c2.v994, c2.v995, c2.v996, c2.v997, ");
                    sqlQuery.Append("c2.v998, c2.v999, c2.v1000, c2.v1001, c2.v1002, c2.v1003, c2.v1004, c2.v1005, c2.v1006, c2.v1007, c2.v1008, c2.v1009, c2.v1010, c2.v1011, ");
                    sqlQuery.Append("c2.v1012, c2.v1013, c2.v1014, c2.v1015, c2.v1016, c2.v1017, c2.v1018, c2.v1019, c2.v1020, c2.v1021, c2.v1022, c2.v1023, c2.v1024, c2.v1025, ");
                    sqlQuery.Append("c2.v1026, c2.v1027, c2.v1028, c2.v1029, c2.v1030, c2.v1031, c2.v1032, c2.v1033, c2.v1034, c2.v1035, c2.v1036, c2.v1037, c2.v1038, c2.v1039, ");
                    sqlQuery.Append("c2.v1040, c2.v1041, c2.v1042, c2.v1043, c2.v1044, c2.v1045, c2.v1046, c2.v1047, c2.v1048, c2.v1049, c2.v1050, c2.v1051, c2.v1052, c2.v1053, ");
                    sqlQuery.Append("c2.v1054, c2.v1055, c2.v1056, c2.v1057, c2.v1058, c2.v1059, c2.v1060, c2.v1061, c2.v1062, c2.v1063, c2.v1064, c2.v1065, c2.v1066, c2.v1067, ");
                    sqlQuery.Append("c2.v1068, c2.v1069, c2.v1070, c2.v1071, c2.v1072, c2.v1073, c2.v1074, c2.v1075, c2.v1076, c2.v1077, c2.v1078, c2.v1079, c2.v1080, c2.v1081, ");
                    sqlQuery.Append("c2.v1082, c2.v1083, c2.v1084, c2.v1085, c2.v1086, c2.v1087, c2.v1088, c2.v1089, c2.v1090, c2.v1091, c2.v1092, c2.v1093, c2.v1094, c2.v1095, ");
                    sqlQuery.Append("c2.v1096, c2.v1097, c2.v1098, c2.v1099, c2.v882 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_7P_Inicio_1 AS c ON Tb_CONTROL.ID_Control = c.ID_Control LEFT OUTER JOIN ");
                    sqlQuery.Append("VW_7P_Inicio_2 AS c2 ON Tb_CONTROL.ID_Control = c2.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_Cuestionario = @ID_Cuestionario) AND ");
                    sqlQuery.Append("(Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                case 201: 	
                    #region Carreras Bachillerato Tecnologico
                    sqlQuery.Append("BACH2I11|");
                    sqlQuery.Append("select Tb_CONTROL.id_control, ");
                    sqlQuery.Append("v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.TURNO, c.id_Carrera as numcarr, v.DISPON, ");
                    sqlQuery.Append("c.C1 AVAR1, c.C2 AVAR2, c.C3 AVAR3, c.C4 AVAR4, c.C5 AVAR5, c.C6 AVAR6, c.C7 AVAR7, c.C8 AVAR8, c.C9 AVAR9, c.C10 AVAR10, c.C11 AVAR11, c.C12 AVAR12, c.C13 AVAR13, c.C14 AVAR14, c.C15 AVAR15, c.C16 AVAR16, c.C17 AVAR17, c.C18 AVAR18, c.C19 AVAR19, c.C20 AVAR20 ");
                    sqlQuery.Append("from ");
                    sqlQuery.Append("dbo.Tb_CONTROL, ");
                    sqlQuery.Append("Vista_DatosDBF v, ");
                    sqlQuery.Append("VW_Carreras_7T_Inicio c ");
                    sqlQuery.Append("where ");
                    sqlQuery.Append("Tb_CONTROL.Id_CentroTrabajo=v.id_CentroTrabajo and ");
                    sqlQuery.Append("Tb_CONTROL.Id_Turno=v.Turno and ");
                    sqlQuery.Append("Tb_CONTROL.id_control=c.id_control and ");
                    sqlQuery.Append("Tb_CONTROL.id_tipoCuestionario=1 and ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.Append(" order by v.id_CCTNT, C.ID_CARRERA ");
                    #endregion
                    break;
                case 202: 	
                    #region Carreras Profesional T�cnico
                    sqlQuery.Append("Prof2I11|");
                    sqlQuery.Append("select Tb_CONTROL.id_control, ");
                    sqlQuery.Append("v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.TURNO, c.id_Carrera as numcarr, v.DISPON, ");
                    sqlQuery.Append("c.C1 AVAR1, c.C2 AVAR2, c.C3 AVAR3, c.C4 AVAR4, c.C5 AVAR5, c.C6 AVAR6, c.C7 AVAR7, c.C8 AVAR8, c.C9 AVAR9, c.C10 AVAR10, c.C11 AVAR11, c.C12 AVAR12, c.C13 AVAR13, c.C14 AVAR14, c.C15 AVAR15, c.C16 AVAR16, c.C17 AVAR17, c.C18 AVAR18, c.C19 AVAR19, c.C20 AVAR20, ");
                    sqlQuery.Append("c.C21 AVAR21, c.C22 AVAR22, c.C23 AVAR23, c.C24 AVAR24, c.C25 AVAR25, c.C26 AVAR26, c.C27 AVAR27, c.C28 AVAR28, c.C29 AVAR29 ");
                    sqlQuery.Append("from ");
                    sqlQuery.Append("dbo.Tb_CONTROL, ");
                    sqlQuery.Append("Vista_DatosDBF v, ");
                    sqlQuery.Append("VW_Carreras_7P_Inicio c ");
                    sqlQuery.Append("where ");
                    sqlQuery.Append("Tb_CONTROL.Id_CentroTrabajo=v.id_CentroTrabajo and ");
                    sqlQuery.Append("Tb_CONTROL.Id_Turno=v.Turno and ");
                    sqlQuery.Append("Tb_CONTROL.id_control=c.id_control and ");
                    sqlQuery.Append("Tb_CONTROL.id_tipoCuestionario=1 and ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ");
                    sqlQuery.Append(" order by v.id_CCTNT, C.ID_CARRERA ");
                    #endregion
                    break;
                case 2000: 	
                    #region Anexo Recursos Computacionales
                    sqlQuery.Append("COMPLI11|");
                    sqlQuery.Append("SELECT Tb_CONTROL.ID_Control, v.Id_CentroTrabajo, v.id_CCTNT, v.CLAVECCT, v.N_CLAVECCT, v.TURNO, ");
                    sqlQuery.Append("CASE WHEN v.TURNO = 1 THEN 'MATUTINO' WHEN v.TURNO = 2 THEN 'VESPERTINO' WHEN v.TURNO = 3 THEN 'NOCTURNO' WHEN v.TURNO = 4 THEN ");
                    sqlQuery.Append("'DISCONTINUO' WHEN v.TURNO = 5 THEN 'MIXTO' END AS N_TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, v.LOCALIDAD, v.N_LOCALIDA, ");
                    sqlQuery.Append("v.DOMICILIO, v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG, v.SOSTENIMIE, v.SERVICIO, v.UNIDADRESP, ");
                    sqlQuery.Append("v.PROGRAMA, v.SUBPROG, v.RENGLON, v.N_RENGLON, v.PERIODO, MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON, c.Anx1 AS V1, c.Anx2 AS V2, c.Anx3 AS V3, c.Anx4 AS V4, ");
                    sqlQuery.Append("c.Anx6 AS V6, c.Anx7 AS V7, c.Anx8 AS V8, c.Anx9 AS V9, c.Anx10 AS V10, c.Anx11 AS V11, c.Anx12 AS V12, c.Anx13 AS V13, c.Anx14 AS V14, ");
                    sqlQuery.Append("c.Anx15 AS V15, c.Anx16 AS V16, c.Anx17 AS V17, c.Anx18 AS V18, c.Anx19 AS V19, c.Anx20 AS V20, c.Anx21 AS V21, c.Anx22 AS V22, c.Anx23 AS V23, ");
                    sqlQuery.Append("c.Anx24 AS V24, c.Anx25 AS V25, c.Anx107 AS V107, c.Anx108 AS V108, c.Anx109 AS V109, c.Anx110 AS V110, c.Anx111 AS V111, c.Anx112 AS V112, ");
                    sqlQuery.Append("c.Anx26 AS V26, c.Anx91 AS V91, c.Anx92 AS V92, c.Anx93 AS V93, c.Anx94 AS V94, c.Anx95 AS V95, c.Anx96 AS V96, c.Anx97 AS V97, c.Anx98 AS V98, ");
                    sqlQuery.Append("c.Anx99 AS V99, c.Anx100 AS V100, c.Anx101 AS V101, c.Anx102 AS V102, c.Anx103 AS V103, c.Anx104 AS V104, c.Anx113 AS V113, c.Anx114 AS V114, ");
                    sqlQuery.Append("c.Anx33 AS V33, c.Anx34 AS V34, c.Anx35 AS V35, c.Anx36 AS V36, c.Anx37 AS V37, c.Anx38 AS V38, c.Anx39 AS V39, c.Anx40 AS V40, c.Anx41 AS V41, ");
                    sqlQuery.Append("c.Anx43 AS V43, c.Anx44 AS V44, c.Anx45 AS V45, c.Anx46 AS V46, c.Anx47 AS V47, c.Anx48 AS V48, c.Anx49 AS V49, c.Anx50 AS V50, c.Anx51 AS V51, ");
                    sqlQuery.Append("c.Anx52 AS V52, c.Anx53 AS V53, c.Anx54 AS V54, c.Anx55 AS V55, c.Anx56 AS V56, c.Anx62 AS V62, c.Anx63 AS V63, c.Anx64 AS V64, c.Anx65 AS V65, ");
                    sqlQuery.Append("c.Anx66 AS V66, c.Anx67 AS V67, c.Anx68 AS V68, c.Anx69 AS V69, c.Anx70 AS V70, c.Anx71 AS V71, c.Anx72 AS V72, c.Anx73 AS V73, c.Anx74 AS V74, ");
                    sqlQuery.Append("c.Anx75 AS V75, c.Anx76 AS V76, c.Anx77 AS V77, c.Anx78 AS V78, c.Anx79 AS V79, c.Anx80 AS V80, c.Anx81 AS V81, c.Anx82 AS V82, c.Anx83 AS V83, ");
                    sqlQuery.Append("c.Anx86 AS V86, c.Anx87 AS V87, c.Anx88 AS V88, c.Anx89 AS V89, Tb_CONTROL.Fecha_Actualizacion AS V90 ");
                    sqlQuery.Append("FROM Tb_CONTROL INNER JOIN ");
                    sqlQuery.Append("Vista_DatosDBF AS v ON Tb_CONTROL.ID_CentroTrabajo = v.Id_CentroTrabajo AND Tb_CONTROL.ID_Turno = v.TURNO LEFT OUTER JOIN ");
                    sqlQuery.Append("AnexoInicio AS c ON Tb_CONTROL.ID_Control = c.ID_Control ");
                    sqlQuery.Append("WHERE (Tb_CONTROL.ID_TipoCuestionario = 1) AND ");
                    sqlQuery.Append("(Tb_CONTROL.Estatus > 0) ");
                    sqlQuery.Append("AND (Tb_CONTROL.ID_CicloEscolar = @ID_CicloEscolar ) ");
                    sqlQuery.Append("ORDER BY v.id_CCTNT");
                    #endregion
                    break;
                #endregion
            }
            return sqlQuery.ToString();
        }

        
    }
}
