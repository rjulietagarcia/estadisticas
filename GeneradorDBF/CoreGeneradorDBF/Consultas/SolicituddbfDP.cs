using System;
using System.Text;
using System.Xml.Serialization;

namespace CoreGeneradorDBF.Consultas
{
	[ XmlRoot( "Solicituddbf" ) ]
	public class SolicituddbfDP
	{


        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int id_entidad;

        public int Id_entidad
        {
            get { return id_entidad; }
            set { id_entidad = value; }
        }
        private int id_cuestionario;

        public int Id_cuestionario
        {
            get { return id_cuestionario; }
            set { id_cuestionario = value; }
        }
        private int id_tipoCuestionario;

        public int Id_tipoCuestionario
        {
            get { return id_tipoCuestionario; }
            set { id_tipoCuestionario = value; }
        }
        private int id_cicloEscolar;

        public int Id_cicloEscolar
        {
            get { return id_cicloEscolar; }
            set { id_cicloEscolar = value; }
        }
        private string fechainicio;

        public string Fechainicio
        {
            get { return fechainicio; }
            set { fechainicio = value; }
        }
        private string fechafin;

        public string Fechafin
        {
            get { return fechafin; }
            set { fechafin = value; }
        }
        private int id_estadosolicitud;

        public int Id_estadosolicitud
        {
            get { return id_estadosolicitud; }
            set { id_estadosolicitud = value; }
        }
        private string archivo;

        public string Archivo
        {
            get { return archivo; }
            set { archivo = value; }
        }
        private bool bit_activo;

        public bool Bit_activo
        {
            get { return bit_activo; }
            set { bit_activo = value; }
        }
        private string fechaAlta;

        public string FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }
        private string fechaactualizacion;

        public string Fechaactualizacion
        {
            get { return fechaactualizacion; }
            set { fechaactualizacion = value; }
        }

        private int id_usuario;

        public int Id_usuario
        {
            get { return id_usuario; }
            set { id_usuario = value; }
        }
	}
}
