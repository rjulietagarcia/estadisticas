using System;
using System.Text;
using System.Data.SqlClient;

namespace CoreGeneradorDBF.Consultas
{
	public class EstadoSolicitudMD
	{
		private EstadoSolicitudDP EstadoSolicitud = null;

		//*** Constructor de la clase  ***
		public EstadoSolicitudMD( EstadoSolicitudDP estadosolicitud )
		{
			this.EstadoSolicitud = estadosolicitud;
		}

		//*** Método de inserción de registro ***
		public int Insert( SqlConnection con, SqlTransaction tran )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "INSERT INTO ESTADOSOLICITUD (" );
			str.Append( "id , " );
			str.Append( "nombre , " );
			str.Append( "bit_activo , " );
			str.Append( "id_usuario , " );
			str.Append( "fecha_actualizacion ) " );
			str.Append( "VALUES (" );
			str.Append( EstadoSolicitud.Id.ToString() +", " );
			str.Append( "'"+EstadoSolicitud.Nombre+"'"+", " );
			str.Append( (EstadoSolicitud.BitActivo== true ? 1 : 0).ToString() +", " );
			str.Append( EstadoSolicitud.IdUsuario.ToString() +", " );
			str.Append( "'"+EstadoSolicitud.FechaActualizacion+"'"+ ")" );
			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
			return com.ExecuteNonQuery();
		}

		//*** Método de búsqueda de registro ***
		public bool Find( SqlConnection con, SqlTransaction tran )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "SELECT COUNT(*) FROM ESTADOSOLICITUD " );
			str.Append( " WHERE " );
			str.Append( "id = "+EstadoSolicitud.Id.ToString() );
 			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
			int encontrados = Int32.Parse(com.ExecuteScalar().ToString());
			return encontrados > 0;
		}

		//*** Método de borrado de registro ***
		public int Delete( SqlConnection con, SqlTransaction tran )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "DELETE ESTADOSOLICITUD " );
			str.Append( " WHERE " );
			str.Append( "id = "+EstadoSolicitud.Id.ToString() );
 			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
			return com.ExecuteNonQuery();
		}

		//*** Método de actualización de registro ***
		public int Update( SqlConnection con, SqlTransaction tran )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "UPDATE ESTADOSOLICITUD SET " );
			str.Append( "nombre = " + "'"+EstadoSolicitud.Nombre+"'" +", " );
			str.Append( (EstadoSolicitud.BitActivo== true ? 1 : 0).ToString() +", " );
			str.Append( "id_usuario = " + EstadoSolicitud.IdUsuario +", " );
			str.Append( "fecha_actualizacion = " + "'"+EstadoSolicitud.FechaActualizacion+"'"  );
			str.Append( " WHERE " );
			str.Append( "id = "+EstadoSolicitud.Id.ToString() );
 			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
			return com.ExecuteNonQuery();
		}

		//*** Método de carga de registro ***
		public static EstadoSolicitudDP Load( SqlConnection con, SqlTransaction tran, 
			int id )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "SELECT " );
			str.Append( "id, " );
			str.Append( "nombre, " );
			str.Append( "id_usuario, " );
			str.Append( "fecha_actualizacion " );
			str.Append( " FROM ESTADOSOLICITUD " );
			str.Append( " WHERE " );
			str.Append( "id = "+id.ToString() );
 			EstadoSolicitudDP tmp = new EstadoSolicitudDP();
			SqlDataReader dr;
			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
			try
			{
				dr = com.ExecuteReader();
				if ( dr.Read() )
				{
					tmp.Id = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
					tmp.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
					tmp.IdUsuario = dr.IsDBNull(2) ? 0 : dr.GetInt32(2);
					tmp.FechaActualizacion = dr.IsDBNull(3) ? "" : dr.GetString(3);
				} else tmp = null;
			}
			catch ( SqlException )
			{
				tmp = null;
			}
			return tmp;
		}
	}
}
