using System;
using System.Text;
using System.Data.SqlClient;

namespace CoreGeneradorDBF.Consultas
{
	public class SolicituddbfMD
	{
		private SolicituddbfDP Solicituddbf = null;

		//*** Constructor de la clase  ***
		public SolicituddbfMD( SolicituddbfDP solicituddbf )
		{
			this.Solicituddbf = solicituddbf;
		}

		//*** Método de inserción de registro ***
		public int Insert( SqlConnection con, SqlTransaction tran )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "INSERT INTO SOLICITUDDBF (" );
			//str.Append( "id , " );
			str.Append( "id_entidad, " );
            str.Append("id_cuestionario , ");
            str.Append("id_tipoCuestionario , ");
            str.Append("id_cicloEscolar , ");
            str.Append("fechaInicio , ");
            str.Append("fechafin , ");
            str.Append("id_EstadoSolicitud , ");
            str.Append("Archivo , ");
            str.Append("Id_usuario, ");
            str.Append("bit_activo, ");
            str.Append("fechaAlta, ");
            str.Append("fechaActualizacion ) ");
			str.Append( "VALUES (" );
			//str.Append( Solicituddbf.Id.ToString() +", " );
			str.Append( Solicituddbf.Id_entidad.ToString() +", " );
			str.Append( Solicituddbf.Id_cuestionario.ToString() +", " );
			str.Append( Solicituddbf.Id_tipoCuestionario.ToString() +", " );
			str.Append( Solicituddbf.Id_cicloEscolar+", " );
			str.Append( "'"+Solicituddbf.Fechainicio+"'"+", " );
            str.Append("'" + Solicituddbf.Fechafin + "'" + ", ");
			str.Append( Solicituddbf.Id_estadosolicitud.ToString() +", " );
            str.Append("'" + Solicituddbf.Archivo + "'" + ", ");
            str.Append(Solicituddbf.Id_usuario + ", ");
			str.Append( (Solicituddbf.Bit_activo== true ? 1 : 0).ToString() +", " );
			str.Append( "'"+Solicituddbf.FechaAlta+ "," );
            str.Append("getdate()" + ")");
			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
			return com.ExecuteNonQuery();
		}

		//*** Método de búsqueda de registro ***
		public bool Find( SqlConnection con, SqlTransaction tran )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "SELECT COUNT(*) FROM SOLICITUDDBF " );
			str.Append( " WHERE " );
			str.Append( "id = "+Solicituddbf.Id.ToString() );
 			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
			int encontrados = Int32.Parse(com.ExecuteScalar().ToString());
			return encontrados > 0;
		}

		//*** Método de borrado de registro ***
		public int Delete( SqlConnection con, SqlTransaction tran )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "DELETE SOLICITUDDBF " );
			str.Append( " WHERE " );
			str.Append( "id = "+Solicituddbf.Id.ToString() );
 			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
			return com.ExecuteNonQuery();
		}

		//*** Método de actualización de registro ***
		public int Update( SqlConnection con, SqlTransaction tran )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "UPDATE SOLICITUDDBF SET " );
			str.Append( "id_entidad = " + Solicituddbf.Id_entidad +", " );
            str.Append("id_cuestionario = " + Solicituddbf.Id_cuestionario + ", ");
            str.Append("id_tipoCuestionario = " + Solicituddbf.Id_tipoCuestionario + ", ");
            str.Append("id_cicloEscolar = " + Solicituddbf.Id_cicloEscolar + ", ");
            if (Solicituddbf.Fechainicio == "")
            {
                str.Append("fechainicio = NULL,");
            }
            else
            {
                str.Append("fechainicio = " + "'" + DateTime.Parse( Solicituddbf.Fechainicio).ToString("yyyyMMdd hh:mm") + "'" + ", ");
            }
            if (Solicituddbf.Fechafin == "")
            {
                str.Append("fechafin = NULL,");
            }
            else
            {
                str.Append("fechafin = " + "'" + DateTime.Parse(Solicituddbf.Fechafin).ToString("yyyyMMdd hh:mm") + "'" + ", ");
            }
			str.Append( "id_estadosolicitud = " + Solicituddbf.Id_estadosolicitud +", " );
            str.Append("archivo = " + "'" + Solicituddbf.Archivo + "'" + ", ");
			str.Append( "id_usuario = " + Solicituddbf.Id_usuario +", " );
			str.Append("bit_activo=" + (Solicituddbf.Bit_activo== true ? 1 : 0).ToString() +", " );
			str.Append( "fechaactualizacion = getdate()"  );
			str.Append( " WHERE " );
			str.Append( "id = "+Solicituddbf.Id.ToString() );
 			com = new SqlCommand( str.ToString(), con);
            con.Open();
			if ( tran != null )
			{
				com.Transaction = tran;
			}
            
            try
            {
                return com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                con.Close();
            }
			
            
		}

		//*** Método de carga de registro ***
		public static SolicituddbfDP Load( SqlConnection con, SqlTransaction tran, 
			int id )
		{
			SqlCommand com;
			StringBuilder str = new StringBuilder();
			str.Append( "SELECT " );
			str.Append( "id, " );
			str.Append( "id_entidad, " );
			str.Append( "id_cuestionario, " );
			str.Append( "id_tipoCuestionario, " );
			str.Append( "id_cicloEscolar, " );
			str.Append( "fechaInicio, " );
			str.Append( "fechafin, " );
            str.Append( "id_EstadoSolicitud, ");
			str.Append( "Archivo, " );
			str.Append( "Id_usuario, " );
            str.Append( "bit_activo, ");
            str.Append( "fechaAlta, ");
            str.Append( "fechaActualizacion ");
			str.Append( " FROM SOLICITUDDBF " );
			str.Append( " WHERE " );
			str.Append( "id = "+id.ToString() );
 			SolicituddbfDP tmp = new SolicituddbfDP();
			SqlDataReader dr;
			com = new SqlCommand( str.ToString(), con);
			if ( tran != null )
			{
				com.Transaction = tran;
			}
            try
            {
                con.Open();
                dr = com.ExecuteReader();
                if (dr.Read())
                {
                    tmp.Id = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
                    tmp.Id_entidad = dr.IsDBNull(1) ? 0 : int.Parse(dr.GetInt16(1).ToString());
                    tmp.Id_cuestionario = dr.IsDBNull(2) ? 0 : int.Parse(dr.GetInt16(2).ToString());
                    tmp.Id_tipoCuestionario = dr.IsDBNull(3) ? 0 : int.Parse(dr.GetInt16(3).ToString());
                    tmp.Id_cicloEscolar = dr.IsDBNull(4) ? 0 : int.Parse(dr.GetInt16(4).ToString());
                    tmp.Fechainicio = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToString();
                    tmp.Fechafin = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToString();
                    tmp.Id_estadosolicitud = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
                    tmp.Archivo = dr.IsDBNull(8) ? "" : dr.GetString(8);
                    tmp.Id_usuario = dr.IsDBNull(9) ? 0 : dr.GetInt32(9);
                    tmp.Bit_activo = dr.IsDBNull(10) ? false : dr.GetBoolean(10);
                    tmp.FechaAlta = dr.IsDBNull(11) ? "" : dr.GetDateTime(11).ToString();
                    tmp.Fechaactualizacion = dr.IsDBNull(12) ? "" : dr.GetDateTime(12).ToString();
                }
                else tmp = null;
            }
            catch (SqlException ex)
            {
                tmp = null;
            }
            finally
            {
                con.Close();
            }
			return tmp;
		}
	}
}
