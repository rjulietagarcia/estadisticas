using System;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text;
using Ionic.Zip;

namespace CoreGeneradorDBF.Consultas
{
    public class DBFprocess
    {
        public string Make(SqlConnection con, int id_cuestionario,int id_Entidad,int id_Solicitud,int id_cicloEscolar,string ruta)
        {
            
           
            
            //string header = "";
            string line;
            string path;
            StringBuilder csvFile = new StringBuilder();
            string rutadbf;
            string rutacsv = ruta;
            StringCollection tozip = new StringCollection();
            
            //Trae query y nombre del DBF
            string getquery = QuerySelector.SeleccionaCuestionario(id_cuestionario);
            string[] q = getquery.Split(char.Parse("|"));

            if(q.Length > 1)
            {
                string fileDBF = q[0];
                string filename = id_Entidad + "_" + q[0] + "_" + id_Solicitud;
                string query = q[1];

                #region Main
                try
                {
                    con.Open();
                    SqlCommand com = con.CreateCommand();
                    com.CommandTimeout = 0;
                    com.CommandText = query;
                    com.Parameters.AddWithValue("@ID_Cuestionario", id_cuestionario);
                    com.Parameters.AddWithValue("@ID_CicloEscolar", id_cicloEscolar);
                    com.Parameters.AddWithValue("@ID_Entidad", id_Entidad);
                    SqlDataReader dr = com.ExecuteReader();

                    ////Header
                    //for (int j = 0; j < dr.FieldCount; j++)
                    //{
                    //    header += dr.GetName(j);
                    //    if (dr.FieldCount > 1 && j < dr.FieldCount - 1)
                    //    {
                    //        header += ",";
                    //    }
                    //}
                    //csvFile.AppendLine(header);

                    while (dr.Read())
                    {
                        line = "";
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            //registros
                            line += dr[i].ToString().Replace("_", "");
                            if (dr.FieldCount > 1 && i < dr.FieldCount - 1)
                            {
                                line += ";";
                            }
                        }
                        csvFile.AppendLine(line);
                    }
                   
                    dr.Dispose();
                    com.Dispose();
                    con.Close();

                    //Escribe el csv en hdd
                    if (rutacsv.EndsWith(@"\"))
                    {
                        rutacsv += filename + ".csv";
                        
                    }
                    else
                    {
                        rutacsv += @"\" + filename + ".csv";
                    }

                    StreamWriter sw = new StreamWriter(rutacsv);
                    sw.Write(csvFile.ToString());
                                
                    sw.Close();
                    
                }
              
                catch (Exception ex)
                {
                    con.Close();
                   

                    path = "Error al crear csv." + ex.Message;
                    return path;
                }
                
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                appPath = appPath.Replace("bin\\Debug\\", "").Replace("bin\\", "");
                
                if(ruta.EndsWith(@"\"))
                {
                    rutadbf = ruta + filename + ".dbf";
                }
                else
                {
                    rutadbf = ruta + @"\" + filename + ".dbf";
                }

                string rutadbfvacio = appPath + @"baseDBF\" + fileDBF + ".dbf";
                try
                {
                    File.Copy(rutadbfvacio, rutadbf, true);
                    File.SetAttributes(rutadbf, FileAttributes.Normal);
                }
                catch (Exception ex)
                {
                    return "Error al copiar dbf original" + ex.Message;
                }
                
                //Llena el dbf con la info del csv
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = appPath + @"dbfview\dbview.exe";
                proc.StartInfo.Arguments = "/APPEND:" + rutadbf +  ", " + rutacsv + " /SEP; /ZAP";
                proc.StartInfo.UseShellExecute = true;
                try
                {
                    proc.Start();
                    tozip.Add(rutadbf);
                    //Esperar a que termine de crearse el dbf
                    while (!proc.HasExited)
                    {
                        System.Threading.Thread.Sleep(5000);
                    }
                    
                }
                catch (Exception ex)
                {
                    path = "Error al crear dbf." + ex.Message;
                    return path;
                }
                #endregion

                //Comprimir dbf y obtener ruta de archivo
                path = Zip(tozip, filename, ruta);

            }
            else
            {
                path = "Error. Cuestionario no disponible.";
            }

            return path;
        }

        public string Zip(StringCollection files, string zipfilename, string outputPath)
        {
            ZipFile zip = new ZipFile();
            zipfilename = zipfilename + ".zip";
            string saveFile;
            if(outputPath.EndsWith(@"\"))
            {
                saveFile = outputPath + zipfilename;
            }
            else
            {
                saveFile = outputPath + @"\" + zipfilename;
            }
            
            foreach (string file in files)
            {
                zip.AddFile(file,"");
            }
            if (File.Exists(saveFile))
            {
                File.Delete(saveFile);
            }
            try
            {
                zip.Save(saveFile);
            }
            catch (Exception ex)
            {
                return "Error al crear archivo compreso." + ex.Message;
            }
            zip.Dispose();
            return zipfilename;
        }




    }
}
