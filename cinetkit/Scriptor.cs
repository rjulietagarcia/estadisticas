using System;
using System.Web;

namespace cinetkit
{
	/// <summary>
	/// Descripci�n breve de scriptor.
	/// </summary>
	public class Scriptor
	{
		public Scriptor()
		{
			//
			// TODO: agregar aqu� la l�gica del constructor
			//
		}
		/// <summary>
		/// Genera un formato basico de HTML
		/// </summary>
		/// <param name="titulo">Texto que aparecera como Titulo</param>
		/// <param name="cuerpo">Texto general del cuerpo</param>
		/// <param name="centrado">Genera el contenido centrado?</param>
		/// <returns>HTML format</returns>
		public static string ConvertToHtml(string titulo,string cuerpo,bool centrado)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			if (centrado) sb.Append("<center>");
			sb.Append("<h2>");
			sb.Append(HttpContext.Current.Server.HtmlEncode(titulo));
			sb.Append("</h2><br>");
			sb.Append(cuerpo);
			if (centrado) 		sb.Append("</center>");
			return sb.ToString();
		}
		/// <summary>
		/// Establece el foco en un elemento o control de la pagina al terminar de cargarse
		/// </summary>
		/// <param name="ControlId">ID del control</param>
		/// <returns>control.focus()</returns>
		public static string  JSFocusin(string ControlId)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'>");
			sb.Append("function setFocus() {");
			sb.Append("document.getElementById('");
			sb.Append(ControlId);
			sb.Append("').focus();}");
			sb.Append(" window.onload=setFocus();");
			sb.Append("</script>");
			return sb.ToString();
		}
		/// <summary>
		/// Genera los tags para ejecutar el contenido llamando al compilador JavaScript 
		/// </summary>
		/// <param name="Contenido">Contenido de la funcion que desea ejecutar</param>
		/// <returns>js directo, no funcion()</returns>
		public static string  JSGeneral(string Contenido)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'>");
			sb.Append(HttpContext.Current.Server.HtmlEncode(Contenido));
			sb.Append(";");
			sb.Append("</script>");
			return sb.ToString();
		}
		/// <summary>
		/// Abre una nueva ventana
		/// </summary>
		/// <param name="MPage">Nombre de la pagina</param>
		/// <param name="Ancho">Pixeles de anchura</param>
		/// <param name="Alto">Pixeles de altura</param>
		/// <returns>window.open()</returns>
		public static string JSNuevaVentana(string MPage,int Ancho,int Alto)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'> window.open('");
			sb.Append(MPage);
			sb.Append("','','menubar=no,scrollbars=yes,resizable=yes,width=");
			sb.Append(Ancho);
			sb.Append(",height=");
			sb.Append(Alto);
			sb.Append(",left=100,top=100');");
			sb.Append("</script>");
			return sb.ToString();
		}
		/// <summary>
		/// Abre una nueva ventana con opcion de tama�o
		/// </summary>
		/// <param name="MPage">Nombre de la pagina</param>
		/// <param name="Ancho">Pixeles de anchura</param>
		/// <param name="Alto">Pixeles de altura</param>
		/// <param name="TamanoFijo">Tama�o fijo (True) o modificable (False)</param>
		/// <returns>window.open()</returns>
		public static string JSNuevaVentana(string MPage,int Ancho,int Alto,bool TamanoFijo)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'> window.open('");
			sb.Append(MPage);
			sb.Append("','','menubar=no,scrollbars=yes,resizable=");
			string yes = (TamanoFijo)? "no":"yes";
			sb.Append(yes);
			sb.Append(",width=");
			sb.Append(Ancho);
			sb.Append(",height=");
			sb.Append(Alto);
			sb.Append(",left=100,top=100');");
			sb.Append("</script>");
			return sb.ToString();
		}
		/// <summary>
		/// Abre una nueva ventana con opciones avanzadas
		/// </summary>
		/// <param name="MPage">Nombre de la pagina</param>
		/// <param name="Ancho">Pixeles de anchura</param>
		/// <param name="Alto">Pixeles de altura</param>
		/// <param name="TamanoFijo">Tama�o fijo? (para hacerlo modificable indique:False)</param>
		/// <param name="EstatusBar">Mostrar barra de estado?</param>
		/// <returns>window.open()</returns>
		public static string JSNuevaVentana(string MPage,int Ancho,int Alto,bool TamanoFijo,bool EstatusBar)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'> window.open('");
			sb.Append(MPage);
			sb.Append("','','menubar=no,statusbar=");
			string yes = (EstatusBar)? "1":"0";
			sb.Append(yes);
			sb.Append(",scrollbars=yes,resizable=");
			yes = (TamanoFijo)? "no":"yes";
			sb.Append(yes);
			sb.Append(",width=");
			sb.Append(Ancho);
			sb.Append(",height=");
			sb.Append(Alto);
			sb.Append(",left=100,top=100');");
			sb.Append("</script>");
			return sb.ToString();
		}
		/// <summary>
		/// Muestra una ventana de confirmacion para generar una accion de javascript
		/// </summary>
		/// <param name="mensaje">Mensaje o pregunta de confirmacion</param>
		/// <param name="accion">evento javascript (directo o funcion() validos)</param>
		/// <returns>js confirm()</returns>
		public static string JSVentanaDeConfirmacion(string mensaje,string accion)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'>");
			sb.Append("if (confirm(\"");
			sb.Append(HttpContext.Current.Server.HtmlEncode(mensaje));
			sb.Append("\")) { ");
			sb.Append(accion);
			sb.Append("\t}");
			sb.Append("</script>");
			return sb.ToString();
		}
		/// <summary>
		/// Muestra una ventana de confirmacion para generar una accion de servidor por medio de un srv.LinkButton
		/// </summary>
		/// <param name="mensaje">Mensaje o pregunta de confirmacion</param>
		/// <param name="linkButtonName">Nombre del Server.LinkButton</param>
		/// <returns>js confirm()</returns>
		public static string JSVentanaDeConfirmacion(string mensaje,object linkButtonName)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type='text/javascript'>");
			sb.Append("if (confirm(\"");
			sb.Append(HttpContext.Current.Server.HtmlEncode(mensaje));
			sb.Append("\")) { ");
			string linkbuton = Convert.ToString(linkButtonName);
			sb.Append("__doPostBack('"+linkbuton+"','');");
			sb.Append("\t}");
			sb.Append("</script>");
			return sb.ToString();
		}
		/// <summary>
		/// Muestra un mensaje
		/// </summary>
		/// <param name="Mensa">Mensaje a mostrar</param>
		/// <returns>alert()</returns>
		public static string JSVentanaDeMensaje(string Mensa)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append("<script type='text/javascript'>");
			sb.Append("alert(\"");
			sb.Append(Mensa);
			sb.Append("\");");
			sb.Append("</script>");
			return sb.ToString();
		}



	}
}