using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
namespace cinetkit
{
    public class GenerarExcel
    {
        #region Generar un Archivo Excel apartir de un DataSet
        public static string ExportarExcel(DataSet ds, string[] campo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table border='1'>");
            bool si = true;
            bool no = false;
            if (ds != null)
            {
                int totalColumn = ds.Tables[0].Columns.Count;
                sb.Append("<tr>");
                for (int i = 0; i < campo.Length; i++)
                {
                    sb.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;'>" + campo[i] + "</td>");
                }
                sb.Append("</tr>");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    sb.Append("<tr>");
                    for (int s = 0; s < totalColumn; s++)
                    {
                        for (int i = 0; i < campo.Length; i++)
                        {
                            if (ds.Tables[0].Columns[s].ToString() == campo[i])
                            {
                                if (dr[s].ToString() == si.ToString()) sb.Append("<td> Si </td>");
                                else
                                    if (dr[s].ToString() == no.ToString()) sb.Append("<td> No </td>");
                                    else
                                        sb.Append("<td>" + dr[s].ToString() + "</td>");
                            }

                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }

            return sb.ToString();


        }


        #endregion

        #region Generar un Archivo Excel apartir de un Datagrid
        public static string ExportarExcel(DataGrid dg, string[] campo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table border='1'>");
            int totalColumn = dg.Columns.Count;
            sb.Append("<tr>");
            for (int i = 0; i < campo.Length; i++)
            {
                sb.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;'>" + campo[i] + "</td>");
            }
            sb.Append("</tr>");
            foreach (DataGridItem dr in dg.Items)
            {
                sb.Append("<tr>");
                for (int s = 0; s < totalColumn; s++)
                {
                    for (int i = 0; i < campo.Length; i++)
                    {
                        if (dg.Columns[s].HeaderText == campo[i] && dg.Columns[s].Visible == false)
                        {
                            sb.Append("<td>" + dr.Cells[s].Text + "</td>");
                        }
                    }
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString(); ;

        }
        #endregion
        
        #region Generar un Archivo Excel apartir de un GridView
        public static string ExportarExcel(GridView gvExcel, string[] campo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table border='1'>");
            int totalColumn = gvExcel.Columns.Count;
            sb.Append("<tr>");
            for (int i = 0; i < campo.Length; i++)
            {
                sb.Append("<td style='font-size: 12px;align: right; color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;'>" + campo[i] + "</td>");
            }
            sb.Append("</tr>");
            foreach (GridViewRow dr in gvExcel.Rows)
            {
                sb.Append("<tr>");
                for (int s = 0; s < totalColumn; s++)
                {
                    for (int i = 0; i < campo.Length; i++)
                    {
                        if (gvExcel.Columns[s].HeaderText == campo[i] && gvExcel.Columns[s].Visible == true)
                        {
                            sb.Append("<td>" + dr.Cells[s].Text + "</td>");
                        }
                    }
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString(); ;

        }
        #endregion

        #region Genera un Archivo Excel Apartir de un GridView sin necesidad de indicarle el nombre de las columnas
        public static string ExportarExcel(GridView gvExcel)//, string[] campo
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table border='1'>");
            int totalColumn = gvExcel.Columns.Count;
            sb.Append("<tr>");
            for (int i = 0; i < totalColumn; i++)
            {
                string ColumnId = gvExcel.Columns[i].HeaderText.ToUpper();
                //if (gvExcel.Columns[i].HeaderText.StartsWith("Id") || gvExcel.Columns[i].HeaderText.StartsWith("id") || gvExcel.Columns[i].HeaderText.EndsWith("id") || gvExcel.Columns[i].HeaderText.EndsWith("Id")) { }
                if (ColumnId.StartsWith("ID") || ColumnId.EndsWith("ID")) { }
                else
                    if (gvExcel.Columns[i].Visible == true)// && gvExcel.Columns[i].HeaderText != "")
                        sb.Append("<td style='font-size: 12px;align: right; color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;'>" + gvExcel.Columns[i].HeaderText + "</td>");
            }
            sb.Append("</tr>");
            foreach (GridViewRow dr in gvExcel.Rows)
            {
                sb.Append("<tr>");
                for (int s = 0; s < totalColumn; s++)
                {
                    if (gvExcel.Columns[s].HeaderText.StartsWith("Id") || gvExcel.Columns[s].HeaderText.StartsWith("id") || gvExcel.Columns[s].HeaderText.EndsWith("id") || gvExcel.Columns[s].HeaderText.EndsWith("Id")) { }
                    else
                    {
                        if (gvExcel.Columns[s].Visible == true)// && gvExcel.Columns[s].HeaderText != "")
                        {
                            string ColumnId = gvExcel.Columns[s].HeaderText.ToUpper();
                            if (dr.Cells[s].HasControls())
                            {
                                for (int c = 0; c < dr.Cells[s].Controls.Count; c++)
                                {
                                    if (dr.Cells[s].Controls[c].Visible == true)
                                    {
                                        if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is CheckBox)
                                        {
                                            if ((((CheckBox)dr.Cells[s].Controls[c]).Checked) == false)
                                                sb.Append("<td> NO </td>");
                                            else
                                                sb.Append("<td> SI </td>");
                                        }
                                        else
                                            if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is LinkButton)
                                                sb.Append("<td>" + ((LinkButton)dr.Cells[s].Controls[c]).Text + "</td>");
                                            else
                                                if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is TextBox)
                                                    sb.Append("<td>" + ((TextBox)dr.FindControl(dr.Cells[s].Controls[c].ID)).Text + "</td>");
                                                else
                                                    if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is DataBoundLiteralControl)
                                                        sb.Append("<td>" + (((DataBoundLiteralControl)(dr.Cells[s].Controls[c])).Text).ToString() + "</td>");
                                                    else
                                                        if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is GridView)
                                                        {
                                                            string tabla = GenerarExcel.ExportarExcel((GridView)dr.Cells[s].Controls[c]);
                                                            sb.Append("<td>" + tabla + "</td>");
                                                        }
                                                        else
                                                            if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is ImageButton)
                                                                sb.Append("<td>" + /*(((ImageButton)(dr.Cells[s].Controls[c]))).ToString()*/ "</td>");
                                                            else
                                                                if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is Button)
                                                                    sb.Append("<td>" + (((Button)(dr.Cells[s].Controls[c])).Text).ToString() + "</td>");
                                    }
                                }
                            }
                            else
                            {
                                if (gvExcel.Columns[s].HeaderText.Contains("Grado") || gvExcel.Columns[s].HeaderText.Contains("grado"))
                                    sb.Append("<td>" + dr.Cells[s].Text.ToString().Replace(" ", "") + "</td>");
                                else
                                    //if (gvExcel.Columns[i].HeaderText.StartsWith("Id") || gvExcel.Columns[i].HeaderText.StartsWith("id") || gvExcel.Columns[i].HeaderText.EndsWith("id") || gvExcel.Columns[i].HeaderText.EndsWith("Id")) { }
                                    if (ColumnId.StartsWith("ID") || ColumnId.EndsWith("ID")) { }
                                    else
                                        sb.Append("<td>" + dr.Cells[s].Text + "</td>");
                            }
                        }
                    }
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString(); ;

        }
        #endregion

        #region Genera un Archivo Excel Apartir de un DataSet sin necesidad de indicarle el nombre de las columnas
        public static string ExportarExcel(DataSet ds)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table border='1'>");
            bool si = true;
            bool no = false;
            if (ds != null)
            {
                int totalColumn = ds.Tables[0].Columns.Count;
                sb.Append("<tr>");
                for (int i = 0; i < totalColumn; i++)
                {
                    string ColumnId = ds.Tables[0].Columns[i].ColumnName.ToUpper();
                    if (ColumnId.StartsWith("ID") || ColumnId.EndsWith("ID")) { }
                    else
                    {
                        sb.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;'>" + ds.Tables[0].Columns[i].ColumnName + "</td>");
                    }

                }
                sb.Append("</tr>");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    for (int s = 0; s < totalColumn; s++)
                    {
                        string ColumnId = ds.Tables[0].Columns[s].ColumnName.ToUpper();
                        if (ColumnId.StartsWith("ID") || ColumnId.EndsWith("ID")) { }
                        else
                        {
                            if (dr[s].ToString() == si.ToString()) sb.Append("<td> Si </td>");
                            else
                                if (dr[s].ToString() == no.ToString()) sb.Append("<td> No </td>");
                                else
                                    ///* Esta condicion valida que cuando la columna sea Grado y sea de la secciona A borre el espacio entre el # y A por que por ejemplo "3 A" en excel se escribe 03:00 a.m*/
                                    if (ds.Tables[0].Columns[s].ColumnName.Contains("Grupo") || ds.Tables[0].Columns[s].ColumnName.Contains("grupo"))
                                        sb.Append("<td>" + dr[s].ToString().Replace(" ", "") + "</td>");
                                    else
                                        sb.Append("<td>" + dr[s].ToString() + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            return sb.ToString();
        }
        #endregion

        #region Genera un Archivo Excel apartir de un Datagrid sin necesidad de indicarle el nombre de las columnas
        public static string ExportarExcel(DataGrid dg)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table border='1'>");
            int totalColumn = dg.Columns.Count;
            sb.Append("<tr>");
            for (int i = 0; i < totalColumn; i++)
            {
                string ColumnId = dg.Columns[i].HeaderText.ToUpper();
                if (ColumnId.StartsWith("ID") || ColumnId.EndsWith("ID")) { /*nada*/ }
                else
                {
                    if (dg.Columns[i].Visible == true)// && dg.Columns[i].HeaderText != "")
                        sb.Append("<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;'>" + dg.Columns[i].HeaderText + "</td>");
                }
            }
            sb.Append("</tr>");
            foreach (DataGridItem dr in dg.Items)
            {
                sb.Append("<tr>");
                for (int s = 0; s < totalColumn; s++)
                {
                    string ColumnId = dg.Columns[s].HeaderText.ToUpper();
                    if (ColumnId.StartsWith("ID") || ColumnId.EndsWith("ID")) { /*nada*/ }
                    {
                        if (dg.Columns[s].Visible == true)// && dg.Columns[s].HeaderText != "")
                        {
                            if (dr.Cells[s].HasControls())
                            {
                                sb.Append("<td>");
                                for (int c = 0; c < dr.Cells[s].Controls.Count; c++)
                                {
                                    if (dr.Cells[s].Controls[c].Visible == true)
                                    {
                                        if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is CheckBox)
                                        {
                                            if ((((CheckBox)dr.Cells[s].Controls[c]).Checked) == false)
                                                sb.Append(/*"<td>*/ " NO" /*</td>"*/);
                                            else
                                                sb.Append(/*"<td>*/ " SI" /*</td>"*/);
                                        }
                                        else
                                            if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is LinkButton)
                                                sb.Append(/*"<td>" +*/" " + ((LinkButton)dr.Cells[s].Controls[c]).Text/* + "</td>"*/);
                                            else
                                                if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is TextBox)
                                                    sb.Append(/*"<td>" + */" " + ((TextBox)dr.Cells[s].Controls[c]).Text/* + "</td>"*/);
                                                else
                                                    if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is DataBoundLiteralControl)
                                                        sb.Append(/*"<td>" + */" " + (((DataBoundLiteralControl)(dr.Cells[s].Controls[c])).Text).ToString()/* + "</td>"*/);
                                                    else
                                                        if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is Label)
                                                            sb.Append(/*"<td>" + */" " + (((Label)(dr.Cells[s].Controls[c])).Text).ToString()/* + "</td>"*/);
                                    }
                                }
                                sb.Append("</td>");
                            }
                            else
                            {
                                if (dg.Columns[s].HeaderText.Contains("Grado") || dg.Columns[s].HeaderText.Contains("grado"))
                                    sb.Append("<td>" + dr.Cells[s].Text.ToString().Replace(" ", "") + "</td>");
                                else
                                    if (dr.Cells[s].Text == "True")
                                        sb.Append("<td>SI</td>");
                                    else
                                        if (dr.Cells[s].Text == "False")
                                            sb.Append("<td>NO</td>");
                                        else
                                            sb.Append("<td>" + dr.Cells[s].Text + "</td>");
                            }
                        }
                    }
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString(); ;

        }
        #endregion

        #region Genera un Archivo Excel apartir de un Repeater sin necesidad de indicarle el nombre de las columnas
        public static string ExportarExcel(Repeater rpt)
        {
            StringBuilder sb = new StringBuilder();
            rpt.DataBind();
            for (int i = 0; i < rpt.Controls.Count; i++)
            {
                for (int j = 0; j < rpt.Controls[i].Controls.Count; j++)
                {
                    if (rpt.Controls[i].Controls[j] is LiteralControl)
                    {
                        sb.Append(((LiteralControl)rpt.Controls[i].Controls[j]).Text.Replace("height=\"1\"", " ").Replace("<td", "<td style='font-size: 12px;color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;' "));
                    }
                    else
                        if (rpt.Controls[i].Controls[j].HasControls())
                        {
                            for (int k = 0; k < rpt.Controls[i].Controls[j].Controls.Count; k++)
                            {
                                if (rpt.Controls[i].Controls[j].Controls[k].HasControls())
                                {
                                    for (int l = 0; l < rpt.Controls[i].Controls[j].Controls[k].Controls.Count; l++)
                                    {
                                        if (rpt.Controls[i].Controls[j].Controls[k].Controls[l] is DataBoundLiteralControl)
                                        {
                                            sb.Append(((DataBoundLiteralControl)rpt.Controls[i].Controls[j].Controls[k].Controls[l]).Text.ToString().Replace("height=\"1\"", " "));
                                        }
                                    }
                                }
                                else
                                {
                                    if (rpt.Controls[i].Controls[j].Controls[k] is DataBoundLiteralControl)
                                    {
                                        sb.Append(((DataBoundLiteralControl)rpt.Controls[i].Controls[j].Controls[k]).Text.Replace("height=\"1\"", " "));
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (rpt.Controls[i].Controls[j] is DataBoundLiteralControl)
                            {
                                sb.Append(((DataBoundLiteralControl)rpt.Controls[i].Controls[j]).Text.Replace("height=\"1\"", " "));
                            }
                        }

                }
            }
            sb.Append("</table>");
            return sb.ToString(); ;
        }
        #endregion

        #region Genera un Archivo Excel Apartir de un GridViewMatriz sin necesidad de indicarle el nombre de las columnas
        public static string ExportarMatrizExcel(GridView gvExcel)//, string[] campo
        {
            StringBuilder sb = new StringBuilder();
            string valor = "";
            sb.Append("<table border='1'>");
            int totalColumn = gvExcel.Columns.Count;
            sb.Append("<tr>");
            for (int i = 0; i < totalColumn; i++)
            {
                string ColumnID = gvExcel.Columns[i].HeaderText.ToUpper();
                if (ColumnID.StartsWith("ID") || ColumnID.EndsWith("ID")) { }
                else
                {
                    if (gvExcel.Columns[i].HeaderText != (i - 1).ToString() && gvExcel.Columns[i].HeaderText != "" && gvExcel.Columns[i].Visible == true && gvExcel.Columns[i].HeaderText != i.ToString())
                    {
                        sb.Append("<td style='font-size: 12px;align: right; color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;'>" + gvExcel.Columns[i].HeaderText + "</td>");
                    }

                    else
                    {
                        if (gvExcel.Columns[i].Visible == true && gvExcel.Columns[i].HeaderText != "")
                        {
                            if (gvExcel.Columns[i].Visible == true)
                            {
                                valor = ((Label)(gvExcel.HeaderRow.Controls[i].FindControl("lblH" + (i - 1).ToString()))).Text;
                                sb.Append("<td style='font-size: 12px;align: right; color: #ffffff;font-family: Verdana, Tahoma, Arial;background-color: #992000;font-weight: bold;vertical-align: middle;'>" + valor + "</td>");
                            }
                        }
                    }

                }
            }
            sb.Append("</tr>");
            foreach (GridViewRow dr in gvExcel.Rows)
            {
                sb.Append("<tr>");
                for (int s = 0; s < totalColumn; s++)
                {
                    string ColumnID = gvExcel.Columns[s].HeaderText.ToUpper();
                    if (ColumnID.StartsWith("ID") || ColumnID.EndsWith("ID")) { }
                    else
                    {
                        if (gvExcel.Columns[s].Visible == true && gvExcel.Columns[s].HeaderText != "")
                        {
                            if (dr.Cells[s].HasControls())
                            {
                                for (int c = 0; c < dr.Cells[s].Controls.Count; c++)
                                {
                                    //if (dr.Cells[s].Controls[c].Visible == true)
                                    //{
                                    if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is CheckBox)
                                    {
                                        if ((((CheckBox)dr.FindControl(dr.Cells[s].Controls[c].ID)).Checked) == false)
                                            sb.Append("<td> NO </td>");
                                        else
                                            sb.Append("<td> SI </td>");
                                    }
                                    else
                                        if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is LinkButton)
                                            sb.Append("<td>" + ((LinkButton)dr.FindControl(dr.Cells[s].Controls[c].ID)).Text + "</td>");
                                        else
                                            if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is TextBox)
                                                sb.Append("<td>" + ((TextBox)dr.FindControl(dr.Cells[s].Controls[c].ID)).Text + "</td>");
                                            else
                                                if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is DataBoundLiteralControl)
                                                    sb.Append("<td>" + (((DataBoundLiteralControl)(dr.Cells[s].Controls[c])).Text).ToString() + "</td>");
                                                else
                                                    if (dr.Cells[s].Controls[c] != null && dr.Cells[s].Controls[c] is GridView)
                                                    {
                                                        string tabla = GenerarExcel.ExportarExcel((GridView)dr.Cells[s].Controls[c]);
                                                        sb.Append("<td>" + tabla + "</td>");
                                                    }

                                    //}
                                }
                            }
                            else
                            {
                                if (gvExcel.Columns[s].HeaderText.Contains("Grado") || gvExcel.Columns[s].HeaderText.Contains("grado"))
                                    sb.Append("<td>" + dr.Cells[s].Text.ToString().Replace(" ", "") + "</td>");
                                else
                                    if (gvExcel.Columns[s].HeaderText.StartsWith("Id") || gvExcel.Columns[s].HeaderText.StartsWith("id") || gvExcel.Columns[s].HeaderText.EndsWith("id") || gvExcel.Columns[s].HeaderText.EndsWith("Id")) { }
                                    else
                                        sb.Append("<td>" + dr.Cells[s].Text + "</td>");
                            }
                        }
                    }
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString(); ;

        }
        #endregion

    }
}
