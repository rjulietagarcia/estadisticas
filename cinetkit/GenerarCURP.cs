using System;
using System.Collections.Generic;
using System.Text;

namespace cinetkit
{
    public class GenerarCURP
    {
        /// <summary>
        /// Genera los Primeros 16 caracteres de la CURP
        /// </summary>
        /// <param name="nombre">Nombre</param>
        /// <param name="paterno">apellido paterno</param>
        /// <param name="materno">apellido materno</param>
        /// <param name="nacimiento">fecha nacimiento (formato dd/mm/aaaa)</param>
        /// <param name="sexo">sexo (H o M)</param>
        /// <param name="entidad">entidad federativa</param>
        /// <param name="pais">nacionalidad</param>
        public static string generar_CURP(string paterno,string materno, string nombre, string nacimiento, string sexo, string entidad, string pais)
        {
            if (materno == null)
                materno = "";
            if (nombre != "" && nombre != null && paterno != "" && paterno != null && nacimiento != "" && nacimiento != null && sexo != "" && sexo != null && entidad != "" && entidad != null && pais != "" && pais != null)
            {
                string CURP = "";
                nombre = quita_nombres_comunes(nombre);
                paterno = quita_preposiciones_conjunciones_contracciones(paterno);
                materno = quita_preposiciones_conjunciones_contracciones(materno);
                nombre = quita_caracteres_especiales(nombre);
                paterno = quita_caracteres_especiales(paterno);
                materno = quita_caracteres_especiales(materno);
                nombre = quita_dieresis(nombre);
                paterno = quita_dieresis(paterno);
                materno = quita_dieresis(materno);

                CURP += dame_primera_letra_paterno(paterno.Trim());
                CURP += dame_primera_vocal_paterno(paterno.Trim());
                CURP += dame_primera_letra_materno(materno.Trim());
                CURP += dame_primera_letra_nombre(nombre.Trim());
                CURP = es_mala_palabra(CURP);
                CURP += dame_fecha(nacimiento);
                CURP += sexo;
                CURP += dame_letras_entidad(entidad, pais);
                CURP += dame_primera_constante_interna(paterno);
                CURP += dame_primera_constante_interna(materno);
                CURP += dame_primera_constante_interna(nombre);

                return CURP;
            }
            return "";
        }
        public static string generar_CURP(string paterno, string materno, string nombre, string nacimiento, string sexo, string entidad)
        {
            string CURP = "";
            nombre = quita_nombres_comunes(nombre);
            paterno = quita_preposiciones_conjunciones_contracciones(paterno);
            materno = quita_preposiciones_conjunciones_contracciones(materno);
            nombre = quita_caracteres_especiales(nombre);
            paterno = quita_caracteres_especiales(paterno);
            materno = quita_caracteres_especiales(materno);
            nombre = quita_dieresis(nombre);
            paterno = quita_dieresis(paterno);
            materno = quita_dieresis(materno);

            CURP += dame_primera_letra_paterno(paterno.Trim());
            CURP += dame_primera_vocal_paterno(paterno.Trim());
            CURP += dame_primera_letra_materno(materno.Trim());
            CURP += dame_primera_letra_nombre(nombre.Trim());
            CURP = es_mala_palabra(CURP);
            CURP += dame_fecha(nacimiento);
            CURP += sexo;
            CURP += dame_letras_entidad(entidad);
            CURP += dame_primera_constante_interna(paterno);
            CURP += dame_primera_constante_interna(materno);
            CURP += dame_primera_constante_interna(nombre);

            return CURP;
        }
        protected static string dame_primera_constante_interna(string cadena)
        {
            char[] letrasCadena = cadena.ToCharArray();
            string consonantes = "BCDFGHJKLMNPQRSTVWXYZ";
            char caracter = 'X';
            for (int i = 1; i < letrasCadena.Length; i++)
            {
                if (consonantes.IndexOf(letrasCadena[i]) != -1)
                {
                    caracter = letrasCadena[i];
                    break;
                }
            }
            return caracter.ToString();
        }
        protected static string dame_letras_entidad(string entidad, string pais)
        {
            if (pais == "MEXICO" || pais == "ESTADOS UNIDOS MEXICANOS" && entidad != "SERVICIO EXTERIOR MEXICANO")
            {
                int numPalabras = entidad.Split(' ').Length;
                if (entidad == "CAMPECHE")
                {
                    return "CC";
                }
                if (entidad == "CHIAPAS")
                {
                    return "CS";
                }
                if (entidad == "CHIHUAHUA")
                {
                    return "CH";
                }
                if (numPalabras == 1)
                {
                    string consonantes = "BCDFGHJKLMNPQRSTVWXYZ";
                    entidad = invertir(entidad);
                    char[] letrasEntidad = entidad.ToCharArray();
                    string regresa = "";
                    regresa += letrasEntidad[letrasEntidad.Length - 1];

                    for (int i = 1; i < letrasEntidad.Length; i++)
                    {
                        if (consonantes.IndexOf(letrasEntidad[i]) != -1)
                        {
                            return regresa += letrasEntidad[i];
                        }
                    }
                }
                if (numPalabras > 1)
                {
                    string[] palabrasEntidad = entidad.Split(' ');
                    string primera = palabrasEntidad[0];
                    string ultima = palabrasEntidad[palabrasEntidad.Length - 1];
                    return (primera.Substring(0, 1) + ultima.Substring(0, 1)).ToUpper();
                }
                return "NL";
            }
            else
            {
                return "NE";
            }
        }
        protected static string dame_letras_entidad(string entidad)
        {
            string[] estados = { "AGUASCALIENTES", "BAJA CALIFORNIA", "BAJA CALIFORNIA SUR", "CAMPECHE", "COAHUILA", 
                                 "COLIMA", "CHIAPAS", "CHIHUAHUA", "DISTRITO FEDERAL", "DURANGO",
                                 "GUANAJUATO", "GUERRERO", "HIDALGO", "JALISCO", "MEXICO",
                                 "MICHOACAN", "MORELOS", "NAYARIT", "NUEVO LEON", "OAXACA",
                                 "PUEBLA", "QUERETARO", "QUINTANA ROO", "SAN LUIS POTOSI", "SINALOA",
                                 "SONORA","TABASCO","TAMAULIPAS","TLAXCALA","VERACRUZ","YUCATAN","ZACATECAS"};
            bool band = false;
            for (int index = 0; index < estados.Length; index++)
            {
                band = false;
                if (entidad == estados[index])
                {
                    string[] claveEdo = { "AS", "BC", "BS", "CC", "CL", "CM", "CS", "CH", "DF", "DG", "GT", "GR", "HG", "JC", "MC", "MN",
                                    "MS","NT","NL","OC","PL","QT","QR","SP","SL","SR","TC","TS","TL","VZ","YN","ZS"};
                    int numPalabras = entidad.Split(' ').Length;
                    if (entidad == "CAMPECHE")
                    {
                        return "CC";
                    }
                    if (entidad == "CHIAPAS")
                    {
                        return "CS";
                    }
                    if (entidad == "CHIHUAHUA")
                    {
                        return "CH";
                    }
                    if (entidad == "QUERETARO")
                    {
                        return "QT";
                    }
                    if (numPalabras == 1)
                    {
                        string consonantes = "BCDFGHJKLMNPQRSTVWXYZ";
                        entidad = invertir(entidad);
                        char[] letrasEntidad = entidad.ToCharArray();
                        string regresa = "";
                        regresa += letrasEntidad[letrasEntidad.Length - 1];
                        for (int i = 0; i < letrasEntidad.Length; i++)
                        {
                            if (consonantes.IndexOf(letrasEntidad[i]) != -1)
                            {
                                regresa += letrasEntidad[i];
                                for (int k = 0; k < claveEdo.Length; k++)
                                {
                                    if (regresa.Trim() == claveEdo[k])
                                    {
                                        return regresa;
                                    }
                                }
                            }
                        }
                    }
                    if (numPalabras > 1)
                    {
                        string regresa = "";
                        string[] palabrasEntidad = entidad.Split(' ');
                        string primera = palabrasEntidad[0];
                        string ultima = palabrasEntidad[palabrasEntidad.Length - 1];
                        regresa = (primera.Substring(0, 1) + ultima.Substring(0, 1)).ToUpper();
                        for (int l = 0; l < claveEdo.Length; l++)
                        {
                            if (regresa.Trim() == claveEdo[l])
                            {
                                return regresa;
                            }
                        }
                    }
                }
                else
                {
                    band = true;
                }
            }
            if (band == true)
            {
                return "NE";
            }
            return "NL";
        }
        protected static string invertir(string cadena)
        {
            string invertido = "";
            for (int i = cadena.Length - 1; i >= 0; i--)
            {
                invertido = invertido + cadena.Substring(i, 1);
            }
            return invertido;
        }

        protected static string dame_fecha(string nacimiento)
        {
            
            string[] fecha = nacimiento.Split('/');
            string dia = fecha[0];
            string mes = fecha[1];
            string anio = fecha[2].Substring(2, 2);
            return (anio + mes + dia).ToUpper();
        }
        protected static string es_mala_palabra(string curp)
        {
            string[] palabrotas = {"BACA","BAKA","BUEI","BUEY","CACA","CACO","CAGA","CAGO","CAKA","CAKO","COGE","COGI","COJA","COJE","COJI",
                                 "COJO","COLA","CULO","FALO","FETO","GETA","GUEI","GUEY","JETA","JOTO","KACA","KACO","KAGA","KAGO","KAKA",
                                 "KAKO","KOGE","KOGI","KOJA","KOJE","KOJI","KOJO","KOLA","KULO","LILO","LOCA","LOCO","LOKA","LOKO","MAME",
                                 "MAMO","MEAR","MEAS","MEON","MIAR","MION","MOCO","MOKO","MULA","MULO","NACA","NACO","PEDA","PEDO","PENE",
                                 "PIPI","PITO","POPO","PUTA","PUTO","QULO","RATA","ROBA","ROBE","ROBO","RUIN","SENO","TETA","VACA","VAGA",
                                 "VAGO","VAKA","VUEI","VUEY","WUEI","WUEY"};
            bool band = false;
            char[] letrasCurp = { };
            string regresa = "";
            for (int i = 0; i < palabrotas.Length; i++)
            {
                if (curp == palabrotas[i])
                {
                    band = true;
                    break;
                }
            }
            if (band == true)
            {
                letrasCurp = curp.ToCharArray();
                letrasCurp[1] = 'X';
                for (int j = 0; j < letrasCurp.Length; j++)
                {
                    regresa += letrasCurp[j].ToString();
                }
            }
            else
            {
                return curp.ToUpper();
            }

            return regresa.ToUpper();
        }

        protected static string dame_primera_letra_nombre(string nombre)
        {
            char[] letrasName = nombre.ToCharArray();
            return letrasName[0].ToString().ToUpper();
        }
        protected static string dame_primera_letra_materno(string apMaterno)
        {
            if (apMaterno != "")
            {
                char[] letrasMaterno = apMaterno.ToCharArray();
                return letrasMaterno[0].ToString().ToUpper();
            }
            else
                return "X";
        }
        protected static string quita_dieresis(string cadena)
        {
            char[] caracteres = cadena.ToLower().ToCharArray();
            string dieresis = "������".ToLower();
            string regresa = "";
            for (int i = 0; i < caracteres.Length; i++)
            {
                if (dieresis.IndexOf(caracteres[i]) != -1)
                {
                    switch (caracteres[i])
                    {
                        case '�': caracteres[i] = 'a'; break;
                        case '�': caracteres[i] = 'e'; break;
                        case '�': caracteres[i] = 'i'; break;
                        case '�': caracteres[i] = 'o'; break;
                        case '�': caracteres[i] = 'u'; break;
                        case '�': caracteres[i] = 'y'; break;
                    }
                }
            }
            for (int j = 0; j < caracteres.Length; j++)
            {
                regresa += caracteres[j].ToString();
            }
            return regresa.ToString().ToUpper();
        }
        protected static string quita_caracteres_especiales(string cadena)
        {
            string especiales = "\"/*-+_.,;:><!��\'+��^�$%&()=?��{}[]��";
            char[] caracteres = cadena.ToCharArray();
            string regresa = "";
            for (int i = 0; i < caracteres.Length; i++)
            {
                if (especiales.IndexOf(caracteres[i]) != -1)
                {
                    caracteres[i] = 'X';
                }
            }
            for (int j = 0; j < caracteres.Length; j++)
            {
                regresa += caracteres[j].ToString();
            }

            return regresa.ToUpper();
        }
        protected static string quita_preposiciones_conjunciones_contracciones(string apellido)
        {
            string regresa = "";
            bool band = false;
            string[] preposiciones = { "DA", "DAS", "DE", "DEL", "DER", "DI", "DIE", "DD", "EL", "LA", "LOS", "LAS", "LE", "LES", "MAC", "MC", "VAN", "VON", "Y" };
            string[] apellidos = apellido.TrimStart().TrimEnd().Split(' ');
            for (int i = 0; i < apellidos.Length; i++)
            {
                band = false;
                for (int j = 0; j < preposiciones.Length; j++)
                {
                    if (apellidos[i] == preposiciones[j])
                    {
                        band = true;
                    }
                }
                if (band == false)
                {
                    regresa += apellidos[i].ToString() + " ";
                }
            }
            return regresa.ToString().ToUpper();
        }
        protected static string quita_nombres_comunes(string nombre)
        {
            if (nombre.TrimStart().TrimEnd().Split(' ').Length > 1)
            {
                string[] comunes = { "DA", "DAS", "DE", "DEL", "DER", "DI", "DIE", "DD", "EL", "LA", "LOS", "LAS", "LE", "LES", "MAC", "MC", "VAN", "VON", "Y", "MARIA", "MA.", "MA", "JOSE", "J", "J." };
                string[] nombres = nombre.Split(' ');
                bool band = false;
                string regresa = "";
                int index = 0;
                for (int i = 0; i < nombres.Length; i++)
                {
                    index = i;
                    band = false;
                    for (int j = 0; j < comunes.Length; j++)
                    {
                        if (nombres[i] == comunes[j])
                        {
                            band = true;
                        }
                    }
                    if (band == false)
                    {
                        regresa += nombres[i].ToString() + " ";
                    }
                }
                if (regresa == "")
                {
                    return nombres[nombres.Length - 1].ToString().ToUpper();
                }
                else
                    return regresa;
            }
            else
            {
                return nombre.ToUpper();
            }
        }
        protected static string dame_primera_letra_paterno(string apPaterno)
        {
            char[] padre = apPaterno.ToCharArray();
            return padre[0].ToString();
        }
        protected static string dame_primera_vocal_paterno(string apPaterno)
        {
            string vocales = "AEIOU";
            char[] padre = apPaterno.ToCharArray();
            for (int i = 1; i < padre.Length; i++)
            {
                if (vocales.IndexOf(padre[i]) != -1)
                {
                    return padre[i].ToString();
                }
            }
            return 'X'.ToString();
        }

    }
}
