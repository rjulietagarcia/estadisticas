using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace cinetkit
{
    public class FnGenericas
    {

        public static string StringPascalCasing(string cadena)
        {
            string[] words = cadena.Split(' ');
            string result = "";
            string[] pronouns = { "de", "del", "el", "la", "tu", "con", "las", "los" };
            int change = 1;
            for (int wo = 0; wo < words.Length; wo++)
            {
                change = 1;
                for (int p = 0; p < pronouns.Length; p++)
                {
                    if (words[wo] == pronouns[p])
                        change = 0;
                }
                if (change == 1)
                    words[wo] = words[wo].Substring(0, 1).ToUpper() + words[wo].Substring(1, words[wo].Length - 1).ToLower();

                result = result + words[wo] + " ";
            }
            return result;
        }

    }
}
