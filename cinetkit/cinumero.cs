using System;

namespace cinetkit
{
	/// <summary>
	/// Descripción breve de numero.
	/// </summary>
	public class cinumero
	{
		protected int unidades;
		protected int decenas;
		protected int centenas;
		protected int millares;

		public int Unidades
		{
			get{return unidades;}
		}
		public int Decenas
		{
			get{return decenas;}
		}
		public int Centenas
		{
			get{return centenas;}
		}
		public int Millares
		{
			get{return millares;}
		}
		public cinumero(int numero)
		{
			millares = numero / 1000;
			int NumMils = numero % 1000;//Num5=1000s
			centenas = NumMils / 100;
			int NumCien = NumMils%100;//Num0=100s
			decenas = NumCien / 10;//Num2=10s
			unidades = NumCien%10;//Num3=1s
		}
	}
}
