using System;

namespace cinetkit
{
	/// <summary>
	/// Descripci�n breve de Calculador.
	/// </summary>
	public sealed class Calculador
	{
		public Calculador()
		{
			//
			// TODO: agregar aqu� la l�gica del constructor
			//
		}
		/// <summary>
		/// Calcula la edad entre dos cadenas de fechas
		/// </summary>
		/// <param name="fechaNaci">Fecha de nacimiento</param>
		/// <param name="fechaFin">Fecha a la cual se quiere calcular la edad</param>
		/// <returns>La Edad o numero de a�os transcurridos</returns>
		public  static int CalculaEdad(string fechaNaci,string fechaFin)
		{
			try
			{
				DateTime fechaN = Convert.ToDateTime(fechaNaci);
				DateTime fechaF = Convert.ToDateTime(fechaFin);
				int edad=0;
				if (DateTime.Compare(fechaF,fechaN)==1)
				{
					edad = fechaF.Year - fechaN.Year;
					if (fechaN.Month > fechaF.Month) edad--;
					else if ((fechaN.Month == fechaF.Month) && (fechaN.Day > fechaF.Day)) edad--;
				}
				else 
				{
					throw new cnkException("La fecha Nacimiento no puede ser futura a la fecha Final");
				}
		
				return edad;
			}
			finally
			{
			}
		}
		/// <summary>
		/// Calcula la edad al dia de hoy segun la fecha del server
		/// </summary>
		/// <param name="fechaNaci">Fecha de nacimiento</param>
		/// <returns>La Edad o numero de a�os transcurridos</returns>
		public  static int CalculaEdad(string fechaNaci)
		{
			try
			{
				DateTime fechaN = Convert.ToDateTime(fechaNaci);
				DateTime fechaF = DateTime.Today;
				int edad=0;
				if (DateTime.Compare(fechaF,fechaN)==1)
				{
					edad = fechaF.Year - fechaN.Year;
					if (fechaN.Month > fechaF.Month) edad--;
					else if ((fechaN.Month == fechaF.Month) && (fechaN.Day > fechaF.Day)) edad--;
				}
				else 
				{
					throw new cnkException("La fecha Nacimiento no puede ser futura a la fecha Final");
				}
		
				return edad;
			}
			finally
			{
			}
		}
		/// <summary>
		/// Indica si un valor dado representa una fecha o no
		/// </summary>
		/// <param name="canFecha">Valor candidato a ser fecha</param>
		/// <returns>true=si es fecha. false=No es fecha</returns>
		public static bool EsFecha(object canFecha)
		{
			try
			{
				DateTime f = Convert.ToDateTime(canFecha.ToString());
				return true;
			}
			catch
			{
				return false;
			}
		}
		/// <summary>
		/// Indica si un valor dado representa una fecha valida o menor al dia de hoy en el server o no
		/// </summary>
		/// <param name="canFecha">Valor candidato a ser fecha valida</param>
		/// <returns>true=si es fecha valida. false=No es fecha valida o no es siquiera fecha</returns>
		public static bool EsFechaValida(string canFecha)
		{
			if (EsFecha(canFecha))
			{
				if (DateTime.Compare(DateTime.Today,Convert.ToDateTime(canFecha))==1)
				{
					return true;
				}
				else 
				{
					return false;
				}
			}
			else return false;
		}
		/// <summary>
		/// Indica si un valor dado representa un numero entero
		/// </summary>
		/// <param name="canNumero">Valor candidato a ser numero entero</param>
		/// <returns>true=si es numero entero. false=No es numero entero</returns>
		public static bool EsNumero(object canNumero)
		{
			try
			{
				int f = Int32.Parse(canNumero.ToString());
				return true;
			}
			catch
			{
				return false;
			}
		}
		/// <summary>
		/// Regresa SIEMPRE el numero entero dado un valor. Por default regresa 0 (Cero)
		/// </summary>
		/// <param name="canNumero">Valor candidato a ser numero</param>
		/// <returns>Numero entero o 0(cero) si el valor de entrada no era numero entero</returns>
		public static int ForzaNumero(object canNumero)
		{
			if (EsNumero(canNumero)) return Int32.Parse(canNumero.ToString());
			else return 0;
		}
		/// <summary>
		/// Regresa SIEMPRE una cadena dado un valor.
		/// </summary>
		/// <param name="canTexto">Valor candidato a ser Texto</param>
		/// <returns>Cadena String</returns>
		public static string ForzaTexto(object canTexto)
		{
			return Convert.ToString(canTexto);
		}
		private static string GetNumberMainName(int numero, int ceros,bool special)
		{
			string res="";
			//if (numero>9 || numero<0) throw new Exception("numero incompatible, numero{0,1,2,3,4,5,6,7,8,9}");
			if (ceros==0)
			{
				switch(numero)
				{
					case 1 : res = special?"un":"uno";
						break;
					case 2 : res = "dos";
						break;
					case 3 : res = "tres";
						break;
					case 4 : res = "cuatro";
						break;
					case 5 : res = "cinco";
						break;
					case 6 : res = "seis";
						break;
					case 7 : res = "siete";
						break;
					case 8 : res = "ocho";
						break;	 
					case 9 : res = "nueve";
						break;
					case 11 : res = "once";
						break;
					case 12 : res = "doce";
						break;
					case 13 : res = "trese";
						break;
					case 14 : res = "catorce";
						break;
					case 15 : res = "quince";
						break;
					default:  res = " ";
						break;
				}
			}
			else if (ceros == 1)
			{
				switch(numero)
				{
					case 1 : res = special?"dieci":"diez";
						break;
					case 2 : res = special?"veinti":"veinte";
						break;
					case 3 : res = special?"treinta y":"treinta";
						break;
					case 4 : res = special?"cuarenta y":"cuarenta";
						break;
					case 5 : res = special?"cincuenta y":"cincuenta";
						break;
					case 6 : res = special?"sesenta y":"sesenta";
						break;
					case 7 : res = special?"setenta y":"setenta";
						break;
					case 8 : res = special?"ochenta y":"ochenta";
						break; 
					case 9 : res = special?"noventa y":"noventa";
						break;
					default: res = " ";
						break;
				}
			}
			else if (ceros == 2)
			{
				switch(numero)
				{
					case 1 : res = special?"ciento":"cien";
						break;
					case 2 : res = "docientos";
						break;
					case 3 : res = "trecientos";
						break;
					case 4 : res = "cuatrocientos";
						break;
					case 5 : res = "quinientos";
						break;
					case 6 : res = "seicientos";
						break;
					case 7 : res = "setecientos";
						break;
					case 8 : res = "ochocientos";
						break;
					case 9 : res = "novecientos";
						break;
					default: res = " ";
						break;
				}
			}
			else
			{
				throw new Exception("ceros incompatible, ceros:{0,1,2}");
			}
			return res;
		}
		public static string GetNumberName(string numero)
		{
			string result = "";
			string[] triadas = SetNumberComma(numero).Split(',');
			int i=triadas.Length;
			int num=0;
			for (int c=0;c<triadas.Length;c++)
			{
				if (!EsNumero(triadas[c])) throw new Exception("Uno o mas caracteres no son enteros, numero:{Numero entero real}");
				num = Int32.Parse(triadas[c]);
				result += (NamerByTriada(Int32.Parse(triadas[c].ToString())) + GetSeparator(i,num>1));
				i--;
			}

//			cinumero ci = new cinumero(numero);
//			string r2, r3, r4;
//			//r1 = GetNumberMainName(ci.Millares,"000",(ci.Centenas>0));//mil
//			r2 = GetNumberMainName(ci.Centenas,2,(ci.Decenas>0));//cien
//			r3 = GetNumberMainName(ci.Decenas,1,(ci.Unidades>0));//diez
//			r4 = GetNumberMainName(ci.Unidades,0,false);//uno
//			return r2 + " " + r3 + " " + r4;
			return result;
		}
		private static string GetSeparator(int i,bool special)
		{
			
			string separador="";
				if (i>1)
				{
						if (InSerieDeMarcelochi(i,3))
							separador = special? " millones ":" millon ";
						else if (InSerieDeMarcelochi(i,5))
							separador = special? " billones ":" billon ";
						else if (InSerieDeMarcelochi(i,7))
							separador = special? " trillones ":" trillon ";
						else separador = special? " mil ":" ";
				}
			return separador;
		}
		private static bool InSerieDeMarcelochi(int n,int tag)
		{
			if (tag>6) tag=tag-6;
			return (n%6==tag);
		}
		private static string NamerByTriada(int numero)
		{
			cinumero ci = new cinumero(numero);
			string r2, r3, r4;
			//r1 = GetNumberMainName(ci.Millares,"000",(ci.Centenas>0));//mil
			r2 = GetNumberMainName(ci.Centenas,2,(ci.Decenas>0 || ci.Unidades>0));//cien
			bool once = (ci.Decenas==1 && ci.Unidades<6 && ci.Unidades>0);
			r3 = once? "":GetNumberMainName(ci.Decenas,1,(ci.Unidades>0));//diez
			r4 = once? GetNumberMainName(10+ci.Unidades,0,false):GetNumberMainName(ci.Unidades,0,false);//uno

			return r2 + " " + r3 + " " + r4;
		}
		private static string SetNumberComma(string numero)
		{
			string result=numero;
			int i=0;
			for(int c=numero.Length;c>0;c--)
			{
				i++;
				if (i%3==0)
				{
					result = result.Insert(c-1,",");
				}
			}
			if (result.IndexOf(",")==0) result = result.Remove(0,1);
			return result;
		}

        public static string optToUpperFirstLetter(string texto)
        {
            string result = "";
            try
            {
                string[] articulos = { "de", "del", "la", "los", "y" };
                string[] palabras = texto.Split(' ');
                bool salir = false;
                for (int pal = 0; pal < palabras.Length; pal++)
                {
                    foreach (string x in articulos)
                    {
                        if (x == palabras[pal])
                            salir = true;
                    }
                    if (!salir)
                    {
                        result = result + palabras[pal].Substring(0, 1).ToUpper() + palabras[pal].Substring(1, palabras[pal].Length - 1) + " ";
                    }
                    else
                    {
                        result = result + palabras[pal] + " ";
                        salir = false;
                    }
                }
            }
            catch (Exception)
            {
                result = texto;
            }
            return result.Trim();
        }

	}
}


