using System;
using System.IO;


namespace cinetkit
{
	/// <summary>
	/// Graba un LOG en un archivo de texto llamado :LogText.txt
	/// Si es instanciado se graba en el archivo que se le indique Path/Default_nameYYYYMMDD.txt [Recomendado]
	/// </summary>
	public sealed class Logger
	{
		//private string DateLogString;
		private string DateLogFile;
		private string fullPath="";
        private string path;

		public Logger(string Path)
		{
            this.fullPath = "";
            this.path = Path;
            nuevoPath();
		}
		public string FullPath
		{
			get{return fullPath;}
		}

		/// <summary>
		/// Graba texto de la forma mas rapida [Recomendado].
		/// </summary>
		/// <param name="Path">Path donde se tenga permisos de escritura</param>
		/// <param name="Texto">Texto a guardar en el Log</param>
        /// <param name="deleteIfNotInDate">Borra el archivo anterior</param>
		public void Write(string Texto,bool deleteIfNotInDate)
		{
			try
			{
                if (deleteIfNotInDate)
                    DeleteFileIfNotInDate(fullPath, DateTime.Today);
				StreamWriter sw = new StreamWriter(fullPath,true);
				lock(sw)
				{
                    sw.WriteLine(DateTime.Now.ToString("dd-MM-yy HH:mm:ss:fff") + "| " + Texto);
				}
				sw.Flush();
				sw.Close();
			}
			catch
			{

			}
		}

        /// <summary>
        /// Graba texto de la forma mas rapida [Recomendado].
        /// </summary>
        /// <param name="Path">Path donde se tenga permisos de escritura</param>
        /// <param name="Texto">Texto a guardar en el Log</param>      
        public void Write(string Texto)
        {
            try
            {
                if (cambiarFecha())
                    nuevoPath();

                StreamWriter writer = new StreamWriter(this.fullPath, true);
                lock (writer)
                {
                    writer.WriteLine(DateTime.Now.ToString("dd-MM-yy HH:mm:ss:fff") + "| " + Texto);
                }
                writer.Flush();
                writer.Close();
            }
            catch
            {
            }
        }

        private bool cambiarFecha()
        {
            bool respuesta = false;
            if (!File.Exists(this.fullPath))
                return true;
            DateTime creacion = File.GetCreationTime(this.fullPath);
            if (creacion.Day != DateTime.Now.Day)
                return true;
            if (creacion.Month != DateTime.Now.Month)
                return true;
            if (creacion.Year != DateTime.Now.Year)
                return true;

            return respuesta;
        }

        private void nuevoPath()
        {
            string str = DateTime.Now.Year.ToString();
            string str2 = DateTime.Now.Month.ToString("00");
            string str3 = DateTime.Now.Day.ToString("00");
            this.DateLogFile = str + str2 + str3 + ".log";
            this.fullPath = path + this.DateLogFile;
        }


		private void DeleteFileIfNotInDate(string FileName,DateTime Date)
		{
			if (File.Exists(FileName))
			{
				if (File.GetCreationTime(FileName)<Date)
					File.Delete(FileName);
			}
		}
		/// <summary>
		/// Graba texto de forma basica.
		/// </summary>
		/// <param name="path">Path donde se tenga permisos de escritura</param>
		/// <param name="logText">Texto a guardar en el Log</param>
		public static void GrabarTextoEnLogFile(string path,string logText)
		{
			try
			{
				StreamWriter SW;
				string Direccion = path + "\\LogText.txt";
				if (!File.Exists(Direccion))
				{
					SW=File.CreateText(Direccion);
				}
				else SW=File.AppendText(Direccion);
				SW.WriteLine(DateTime.Now.ToString()+" |"+logText);
				SW.Flush();
				SW.Close();
                
			}
			catch {}
		}
		/// <summary>
		/// Graba texto de forma avanzada.
		/// </summary>
		/// <param name="path">Path donde se tenga permisos de escritura</param>
		/// <param name="logText">Texto a guardar en el Log</param>
		/// <param name="anidado">En forma anidada o En una sola linea tabulada </param>
		/// <param name="nivel">Nivel de anidamiento</param>
		/// <param name="MuestraFechaHora">Si desea mostrar la fecha y la hora (solo aplica para anidamiento = true)</param>
		public static void GrabarTextoEnLogFile(string path,string logText,bool anidado,int nivel,bool MuestraFechaHora)
		{
			try
			{
				StreamWriter SW;
				string Direccion = path + "\\LogText.txt";
				if (!File.Exists(Direccion))
				{
					SW=File.CreateText(Direccion);
				}
				else SW=File.AppendText(Direccion);
				if (!anidado)
				{
					SW.WriteLine(DateTime.Now.ToString()+"\t|"+logText);
				}
				else
				{
					string nvl = "";
					for(int i = 1;i<nivel;i++){nvl+="\t|";}
					if (MuestraFechaHora) SW.WriteLine(DateTime.Now.ToString());
					SW.WriteLine(nvl+logText);
				}
				SW.Flush();
				SW.Close();
			}
			catch{}
		}

	}

}
