using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace WsEstadisticasEducativas.Datos
{
    public class ReportesRowset
    {
        private string idFiltro;
        private string filtro;
        
        private int totales;
        private int capturados;
        private int conMotivo;
        private int oficializados;
        private int pendientes;
        private decimal porcentaje;
        private int enCaptura;
        private int pendientesCaptura;

        public int PendientesCaptura
        {
            get { return pendientesCaptura; }
            set { pendientesCaptura = value; }
        }

        public int EnCaptura
        {
            get { return enCaptura; }
            set { enCaptura = value; }
        }


        public int Oficializados
        {
            get { return oficializados; }
            set { oficializados = value; }
        }

    
        public int ConMotivo
        {
            get { return conMotivo; }
            set { conMotivo = value; }
        }

        public decimal Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }

        public int PendientesTotales
        {
            get { return pendientes; }
            set { pendientes = value; }
        }

        public int Capturados
        {
            get { return capturados; }
            set { capturados = value; }
        }

        public int Totales
        {
            get { return totales; }
            set { totales = value; }
        }

        public string Filtro
        {
            get { return filtro; }
            set { filtro = value; }
        }

        public string IdFiltro
        {
            get { return idFiltro; }
            set { idFiltro = value; }
        }
    }
}
