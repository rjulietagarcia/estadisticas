using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using SEroot.WsRefSeguridad;


namespace EstadisticasEducativas.Seguridad
{
    public partial class usuariosEnMasa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtConn.Text = @"Data Source=;Initial Catalog=;User ID=;Password=";
                btnCarga.Attributes.Add("onclick","javascript:return confirm('Confirma agregar los registros a la base de datos de seguridad');");
            }
        }

        protected void btnCarga_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(txtConn.Text);
            bool validConn = false;
            try
            {
                conn.Open();
                validConn = true;
            }
            catch (Exception ex)
            {
                Response.Write(@"<script type='text/javascript'>alert('" + ex.Message.Replace("'", " ") + "');</script>");

            }
            if (validConn)
            {
                StringBuilder sb = new StringBuilder();
                string query = txtQuery.Text;
                if (query.IndexOf("insert") > 0 || query.IndexOf("delete") > 0 || query.IndexOf("update") > 0)
                {
                    lblResTest.Text = "La consulta no debe contener las palabras insert, delete o update";
                }
                else
                {
                    sb.Append(query);
                    SqlCommand comm = new SqlCommand(sb.ToString(), conn);
                    SqlDataAdapter adap = new SqlDataAdapter(comm);
                    DataSet ds = new DataSet();
                    try
                    {
                        adap.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        string errorComm = ex.Message.Replace("'", " ");
                        ds = null;
                        Response.Write(@"<script type='text/javascript'>alert('" + errorComm + "');</script>");
                    }
                    if (ds != null)
                    {
                        int numRegs = ds.Tables[0].Rows.Count;
                        DataColumn username = ds.Tables[0].Columns[0];
                        DataColumn password = ds.Tables[0].Columns[1];
                        lblResTest.Text = "";
                        int conta = 0;
                        foreach (DataRow r in ds.Tables[0].Select())
                        {
                            try
                            {
                                conta++;
                                Membership.CreateUser(r[username.ColumnName].ToString().ToUpper(), r[password.ColumnName].ToString().ToUpper());
                            }
                            catch { }
                        }
                        Response.Write(@"<script type='text/javascript'>alert('Se terminaron de agregar los registros "+conta.ToString()+"');</script>");
                    }
                    else 
                    {
                        Response.Write(@"<script type='text/javascript'>alert('No se encontraron registros');</script>");
                    }
                }
            }

        }

        protected void btnTest_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(txtConn.Text);
            bool validConn = false;
            try
            {
                conn.Open();
                validConn = true;
            }
            catch (Exception ex)
            {
                Response.Write(@"<script type='text/javascript'>alert('"+ex.Message.Replace("'", " ")+"');</script>");
                
            }
            if (validConn)
            {
                StringBuilder sb = new StringBuilder();
                string query = txtQuery.Text;
                if (query.IndexOf("insert")>0 || query.IndexOf("delete")>0 || query.IndexOf("update")>0)
                {
                    lblResTest.Text = "La consulta no debe contener las palabras insert, delete o update";
                }
                else
                {
                    sb.Append(query);
                    SqlCommand comm = new SqlCommand(sb.ToString(), conn);
                    SqlDataAdapter adap = new SqlDataAdapter(comm);
                    DataSet ds = new DataSet();
                    try
                    {
                        adap.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        string errorComm = ex.Message.Replace("'", " ");
                        ds = null;
                        Response.Write(@"<script type='text/javascript'>alert('" + errorComm + "');</script>");
                    }
                    if (ds!=null)
                    { 
                        int numRegs = ds.Tables[0].Rows.Count;
                        DataColumn username = ds.Tables[0].Columns[0];
                        DataColumn password = ds.Tables[0].Columns[1];
                        lblResTest.Text = "";
                        lblResTest.Text = "N�mero de registros encontrados: <b>" + numRegs + "</b><br/>columna que se usara como nombre de usuario: <b>" + username.ColumnName + "</b><br/>columna que se usara como contrase�a:<b> " + password.ColumnName + "</b>";
                        btnCarga.Visible = true;
                    }
                }
            }
        }

        protected void txtQuery_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
