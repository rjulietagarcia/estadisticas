using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Security.Permissions;
using System.Collections.Specialized;
//using Mx.Com.Cimar.Supports.Web.MemberShip;
//using System.Reflection;
using System.Xml;
using System.Web.UI;
using System.IO;

namespace SEP.SiteMapProvider
{

    public enum SiteMapNivelTrabajo
    {
        Federal, Estatal, Region, Zona, CT
    }

    /// <summary>
    /// Mapa de Sitio Personalizado.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class SEPSiteMapProvider : XmlSiteMapProvider

    {
        private SiteMapNode rootNode = null;
        //
        // Constructor
        public SEPSiteMapProvider()
        {
            
        }

        // Maneja el estado para conocer si el proveedor ya  fue inicializado
        private bool initialized = false;

        public virtual bool IsInitialized
        {
            get { return initialized; }
        }

        // Return the root node of the current site map.
        public override SiteMapNode RootNode
        {
            get
            {
                //SiteMapNode temp = null;
                SiteMapNode temp = BuildSiteMap();
                return temp;
            }
        }

        protected override SiteMapNode GetRootNodeCore()
        {
            return RootNode;
        }

        // Initialize es usado para inicializar las propiedades
        // y el estado que mantenga un proveedor de WsSE
        // pero no es usado para construir el mapa de sitio
        // el cual es generado cuando se llame el m�todo BuildSiteMap.
        public override void Initialize(string name, NameValueCollection attributes)
        {
            if (IsInitialized)
                return;

            base.Initialize(name, attributes);

            //// Create y prueba la conexi�n con la clase hija
            //if (cim == null)
            //    cim = CimarSecurityHelper.GetCurrentSecurityClass();

            initialized = true;
        }

        ///
        /// SiteMapProvider and StaticSiteMapProvider methods that this derived class must override.
        ///

        // Limpia cualquier conexi�n u otro estado
        // que una instancia de este pueda tener.
        protected override void Clear()
        {
            lock (this)
            {
                rootNode = null;
                base.Clear();
            }
        }

        // Construye en memoria la representacion del SiteMap
        // desde la BD, y regresa el nodo raiz del sitemap.
        public override SiteMapNode BuildSiteMap()
        {
            // Desde que SiteMap class es estatica, asegurate que esta no
            // ha sido modificada mientras el sitemap se construye.            
            //lock (this)
            //{
                // Si no hay inicializacion, este m�todo ha sido
                // llamado en otro orden.
                if (!IsInitialized)
                {
                    throw new Exception("BuildSiteMap called incorrectly.");
                }
                // Comenzamos, desde un estado limpio.
                //Clear();
                rootNode = GetSEPSiteMapProvider(ref rootNode);
            //}
            return rootNode;
        }

        public override bool IsAccessibleToUser(HttpContext context, SiteMapNode node)
        {
            //try
            //{
            //    //if (cim == null)
            //    //    CimarSecurityHelper.GetCurrentSecurityClass();
            //    //StringDictionary sd = cim.GetUserPermision(null);
            //    try
            //    {
            //        if (sd.ContainsKey(node.Key))
            //            return true;
            //    }
            //    catch (Exception exception) {
            //        HttpContext.Current.Trace.Warn("CimarSiteMapProvider.IsAccessibleToUser",exception.Message,exception);
            //        //System.Diagnostics.EventLog.WriteEntry("Error :" + exception.Message);
            //    }
            //}
            //catch (Exception exception)
            //{
            //    HttpContext.Current.Trace.Warn("CimarSiteMapProvider.IsAccessibleToUser", exception.Message, exception);
            //    //System.Diagnostics.EventLog.WriteEntry("Error :" + exception.Message);
            //    // System.Diagnostics.EventLog.WriteEntry("SE", exception.Message, System.Diagnostics.EventLogEntryType.Information);                
            //}
            return false;
        }

        public void AddNodeNow(SiteMapNode node, SiteMapNode parentNode) {
            try
            {
                base.AddNode(node, parentNode);
            }
            catch
            { }

        }
        static SiteMapNode GetSEPSiteMapProvider(ref SiteMapNode actual)
        {
            Mx.Gob.Nl.Educacion.UsuarioSeDP  usr = Mx.Gob.Nl.Educacion.SeguridadSE.GetUsuario(HttpContext.Current);
            //string providerActual = SiteMap.Provider.Name;
            string providerNuevo = ((SiteMapNivelTrabajo)usr.NivelTrabajo.NiveltrabajoId).ToString();
            //if (providerActual != providerNuevo)
            //{   
            actual = SiteMap.Providers[providerNuevo].RootNode;
            //    actual.Provider.ParentProvider = 
            //}
            return actual;
        }
        //ICimarSecurity cim;

    }
}
