<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="ALT_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.ALT_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

<!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title=""><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->

    <h2> II. LOCALES DE LA CONSTRUCCI&Oacute;N Y ESTADO F&Iacute;SICO</h2>
    <br />

            <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

    <%--<form id="locales" runat="server">--%>
    <div>
    1. Escriba el n�mero de aulas, laboratorios y talleres separ&aacute;ndolos en: construidos para uso educativo, provisionales, adaptados y fuera del inmueble principal;
    clasif&iacute;quelos por su estado f&iacute;sico: bueno, regular, malo y no apto para su uso;
    posteriormente escriba el material predominante de muros, techos y pisos en cada uno de ellos.
    <br /> <br />
    
    <!-- Tabla de las AULAS -->
    
        <table>
            <tr>
                <td rowspan="3">
                </td>
                <td colspan="5" style="text-align: center">
                    AULAS
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center">
                    Estado F&iacute;sico
                </td>
                <td rowspan="2" style="text-align: center">Total
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            <tr>
                <td>Construidos para <br />
                    uso educativo
                </td>
                <td> <asp:TextBox runat="server" id="txtv41" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv42" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv43" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv44" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv45" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Provisionales
                </td>
                <td> <asp:TextBox runat="server" id="txtv46" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv47" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv48" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv49" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv50" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Adaptados
                </td>
                <td> <asp:TextBox runat="server" id="txtv51" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv52" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv53" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv54" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv55" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Locales fuera del <br />
                    inmueble principal
                </td>
                <td> <asp:TextBox runat="server" id="txtv56" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv57" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv58" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv59" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv60" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Total
                </td>
                <td> <asp:TextBox runat="server" id="txtv61" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv62" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv63" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv64" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv65" Width="60" />
                </td>
            </tr>
        </table>
        <br />
        
        Material predominante de la construcci&oacute;n (AULAS):
        <table>
            <tr>
                <td style="text-align: center">Muros
                </td>
                <td style="text-align: center">Techos
                </td>
                <td style="text-align: center">Pisos
                </td>
            </tr>
            <tr>
                <td>
        <asp:DropDownList ID="txtv66" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
                <td>
        <asp:DropDownList ID="txtv67" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
                <td>
        <asp:DropDownList ID="txtv68" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
            </tr>
        </table>
    <br /> <br />
    
    
    <!-- Tabla de los LABORATORIOS -->
    
        <table>
            <tr>
                <td rowspan="3">
                </td>
                <td colspan="5" style="text-align: center">
                    LABORATORIOS
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center">
                    Estado F&iacute;sico
                </td>
                <td rowspan="2" style="text-align: center">Total
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            <tr>
                <td>Construidos para <br />
                    uso educativo
                </td>
                <td> <asp:TextBox runat="server" id="txtv69" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv70" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv71" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv72" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv73" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Provisionales
                </td>
                <td> <asp:TextBox runat="server" id="txtv74" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv75" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv76" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv77" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv78" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Adaptados
                </td>
                <td> <asp:TextBox runat="server" id="txtv79" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv80" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv81" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv82" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv83" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Locales fuera del <br />
                    inmueble principal
                </td>
                <td> <asp:TextBox runat="server" id="txtv84" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv85" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv86" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv87" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv88" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Total
                </td>
                <td> <asp:TextBox runat="server" id="txtv89" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv90" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv91" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv92" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv93" Width="60" />
                </td>
            </tr>
        </table>
        <br />
    
     Material predominante de la construcci&oacute;n (LABORATORIOS):
        <table>
            <tr>
                <td style="text-align: center">Muros
                </td>
                <td style="text-align: center">Techos
                </td>
                <td style="text-align: center">Pisos
                </td>
            </tr>
            <tr>
                <td>
        <asp:DropDownList ID="txtv94" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
                <td>
        <asp:DropDownList ID="txtv95" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
                <td>
        <asp:DropDownList ID="txtv96" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
            </tr>
        </table>
    <br /> <br />
    
    
    
    <!-- Tabla de los TALLERES -->
    
            <table>
            <tr>
                <td rowspan="3">
                </td>
                <td colspan="5" style="text-align: center">
                    TALLERES
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center">
                    Estado F&iacute;sico
                </td>
                <td rowspan="2" style="text-align: center">Total
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            <tr>
                <td>Construidos para <br />
                    uso educativo
                </td>
                <td> <asp:TextBox runat="server" id="txtv97" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv98" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv99" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv100" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv101" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Provisionales
                </td>
                <td> <asp:TextBox runat="server" id="txtv102" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv103" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv104" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv105" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv106" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Adaptados
                </td>
                <td> <asp:TextBox runat="server" id="txtv107" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv108" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv109" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv110" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv111" Width="60" />
                </td>
            </tr>
            <tr>
                <td>Locales fuera del <br />
                    inmueble principal
                </td>
                <td> <asp:TextBox runat="server" id="txtv112" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv113" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv114" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv115" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv116" Width="60" />
                </td>
            </tr>
            <tr>
                <td> Total
                </td>
                <td> <asp:TextBox runat="server" id="txtv117" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv118" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv119" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv120" Width="60" />
                </td>
                <td> <asp:TextBox runat="server" id="txtv" Width="60" />
                </td>
            </tr>
        </table>
        <br />
        
         Material predominante de la construcci&oacute;n (TALLERES):
        <table>
            <tr>
                <td style="text-align: center">Muros
                </td>
                <td style="text-align: center">Techos
                </td>
                <td style="text-align: center">Pisos
                </td>
            </tr>
            <tr>
                <td>
        <asp:DropDownList ID="txtv122" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
                <td>
        <asp:DropDownList ID="txtv123" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
                <td>
        <asp:DropDownList ID="txtv124" runat="server">
            <asp:ListItem Value="01"> Adobe </asp:ListItem>
                        <asp:ListItem Value="02"> Asbesto</asp:ListItem>
                        <asp:ListItem Value="03"> Asfalto</asp:ListItem>
                        <asp:ListItem Value="04"> Bloque hueco</asp:ListItem>
                        <asp:ListItem Value="05"> Concreto</asp:ListItem>
                        <asp:ListItem Value="06"> Cart&oacute;n</asp:ListItem>
                        <asp:ListItem Value="07"> Carrizo</asp:ListItem>
                        <asp:ListItem Value="08"> Ladrillo</asp:ListItem>
                        <asp:ListItem Value="09"> L&aacute;mina met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="10"> Madera</asp:ListItem>
                        <asp:ListItem Value="11"> Mosaico</asp:ListItem>
                        <asp:ListItem Value="12"> Piedra</asp:ListItem>
                        <asp:ListItem Value="13"> Palma</asp:ListItem>
                        <asp:ListItem Value="14"> Tabicon</asp:ListItem>
                        <asp:ListItem Value="15"> Teja de barro</asp:ListItem>
                        <asp:ListItem Value="16"> Tierra</asp:ListItem>
                        <asp:ListItem Value="17"> Tejamanil</asp:ListItem>
                        <asp:ListItem Value="18"> Fibra de vidrio</asp:ListItem>
                        <asp:ListItem Value="19"> Multip&aacute;nel</asp:ListItem>
                        <asp:ListItem Value="20"> Material regional</asp:ListItem>
                        <asp:ListItem Value="21"> Alambre de p&uacute;as</asp:ListItem>
                        <asp:ListItem Value="22"> Estruct&uacute;ra met&aacute;lica</asp:ListItem>
                        <asp:ListItem Value="23"> Loseta</asp:ListItem>
                        <asp:ListItem Value="24"> Malla cicl�nica</asp:ListItem>
                        <asp:ListItem Value="25"> Tabique</asp:ListItem>
                        <asp:ListItem Value="26"> Otro</asp:ListItem>
                        <asp:ListItem Value="27"> Ninguno</asp:ListItem>
        </asp:DropDownList>
                </td>
            </tr>
        </table>
    <br /> <br />
    
    <%--<input id="Submit2" type="submit" value="Continuar" />--%>
    
    </div>
    <%--</form>--%>
    
                <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


    
 </asp:Content>
