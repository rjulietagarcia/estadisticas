<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="Problemas_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.Problemas_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

<!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title=""><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->


    <h2> VII. PROBLEM&Aacute;TICA QUE PRESENTA EL INMUEBLE </h2> 
    <br />
    
    <%--<form id="form1" runat="server">--%>
    <div>
    
    
        <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
    
    Marque los problema que muestra el inmueble:
    <br /> <br />
    
    <asp:CheckBox runat="server" id="txtv689" /> Fugas en tuber�as <br />
    <asp:CheckBox runat="server" id="txtv690" /> Cables expuestos <br />
    <asp:CheckBox runat="server" id="txtv691" /> Corrosi&oacute;n del acero estructural <br />
    <asp:CheckBox runat="server" id="txtv692" /> Encharcamiento de las losas <br />
    <asp:CheckBox runat="server" id="txtv693" /> Humedad o salitre en muros <br />
    <asp:CheckBox runat="server" id="txtv694" /> Vibraci&oacute;n excesiva en circulaciones o escaleras <br />
    <asp:CheckBox runat="server" id="txtv695" /> Puertas, canceles o vidrieria sueltos o con fijaci&oacute;n deficiente <br />
    <asp:CheckBox runat="server" id="txtv696" /> Fisuras o grietas <br />
    <asp:CheckBox runat="server" id="txtv697" /> Aplanado o recubrimientos sueltos <br />
    <asp:CheckBox runat="server" id="txtv698" /> Otro: <asp:TextBox runat="server" id="txtv699" /> <br />
    
    
    <br /> <br /> <br />
    
    <h2> VIII. MOBILIARIO </h2>
    <br />
    
    1. Escriba la cantidad de mobiliario con el que cuenta el inmueble seg&uacute;n su estado f&iacute;sico: Bueno, Regular y Malo.
    <br />
    
        <table>
            <tr>
                <td rowspan="2"> Mobiliario
                </td>
                <td colspan="3" style="text-align:center"> Estado f&iacute;sico
                </td>
                <td  rowspan="2" style="text-align:center"> Total
                </td>
            </tr>
            <tr>
                <td  style="text-align:center"> Bueno
                </td> 
                <td  style="text-align:center"> Regular
                </td>
                <td  style="text-align:center"> Malo
                </td>
            </tr>
            <tr>
                <td> Escritorios
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv700" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv701" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv702" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv703" />
                </td>
            </tr>
            <tr>
                <td> Sillas
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv704" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv705" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv706" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv707" />
                </td>
            </tr>
            <tr>
                <td> Pizarrones
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv708" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv709" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv710" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv711" />
                </td>
            </tr>
            <tr>
                <td> Mesitas
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv712" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv713" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv714" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv715" />
                </td>
            </tr>
            <tr> 
                <td> Mesabanco Individual
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv716" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv717" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv718" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv719" />
                </td>
            </tr>
            <tr>
                <td> Mesabanco binario
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv720" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv721" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv722" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv723" />
                </td>
            </tr>
            <tr>
                <td> M&aacute;quinas de escribir
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv724" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv725" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv726" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv727" />
                </td>
            </tr>
            <tr>
                <td> Archivero
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv728" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv729" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv730" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv731" />
                </td>
            </tr>
            <tr>
                <td> Instrumentos de banda de guerra
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv732" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv733" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv734" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv735" />
                </td>
            </tr>
            <tr>
                <td> Bandera
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv736" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv737" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv738" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv739" />
                </td>
            </tr>
            <tr>
                <td> Restiradores
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv740" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv741" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv742" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv743" />
                </td>
            </tr>
            <tr>
                <td> M&aacute;quinas de coser
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv744" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv745" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv746" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv747" />
                </td>
            </tr>
            <tr>
                <td> Estanter�a
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv748" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv749" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv750" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv751" />
                </td>
            </tr>
            <tr>
                <td> Armarios
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv752" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv753" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv754" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv755" />
                </td>
            </tr>
            <tr>
                <td> Camas
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv756" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv757" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv758" />
                </td>
                <td> <asp:TextBox runat="server" Width="60" id="txtv759" />
                </td>
            </tr>
        </table>
    
    <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
    
    </div>
    <%--</form>--%>

</asp:Content>
