using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Mx.Gob.Nl.Educacion;
using System.Text;

namespace EstadisticasEducativas._911
{
    public partial class SolicitudDBF : System.Web.UI.Page
    {
        SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

        SEroot.ServiceSolicitudDBF.ServiceSolicitudDBF wsSolicitudDBF = new SEroot.ServiceSolicitudDBF.ServiceSolicitudDBF();

        SEroot.WSEscolar.WsEscolar wsEscolar = new SEroot.WSEscolar.WsEscolar();    

        protected void Page_Load(object sender, EventArgs e)
        {

            rblTipo.Items[0].Attributes.Add("onclick", "muestraCuestionarios('ctl00_cphMainMaster_rblTipo_0','ctl00_cphMainMaster_rblTipo_1')");
            rblTipo.Items[1].Attributes.Add("onclick", "muestraCuestionarios('ctl00_cphMainMaster_rblTipo_0','ctl00_cphMainMaster_rblTipo_1')");

            if (!Page.IsPostBack)
            {
                ComboCiclos();
                Entidad();
                llenaGridSolicitudes();
                Cuestionario();
                llenaGridDescargas();
            }
        }
        private void ComboCiclos()
        {
            SEroot.WSEscolar.DsCiclosEscolares dsCiclos = null;
            dsCiclos = wsEscolar.ListaCicloEscolarCombo(1);
            ddlCiclo.DataSource = dsCiclos;
            ddlCiclo.DataTextField = "nombre";
            ddlCiclo.DataValueField = "id_cicloescolar";
            ddlCiclo.DataBind();

            SEroot.WSEscolar.CicloEscolarActualDP actualDP = null;
            actualDP = wsEscolar.LoadCiclo(1);
            int intCicloActual = actualDP.cicloActual.cicloEscolarId;
            ddlCiclo.SelectedValue = intCicloActual.ToString();
        }
        private void Entidad()
        {
            SEroot.WsEstadisticasEducativas.CatListaEntidad911DP[] listaEntidades = wsEstadisticas.Lista_CatListaEntidad911(223);//EL ID PAIS = 223 es M�XICO
            for (int i = 0; i < listaEntidades.Length; i++)
            {
                string valor = listaEntidades[i].ID_Entidad.ToString();
                string texto = listaEntidades[i].Nombre;
                ddlEntidad.Items.Add(new ListItem(texto, valor));

            }

        }
        private void llenaGridSolicitudes()
        {
            UsuarioSeDP nuevoUser = SeguridadSE.GetUsuario(HttpContext.Current);
            //nuevoUser.Selecciones.CentroTrabajoSeleccionado.

            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            int region = int.Parse(usr[2]);
            int ID_Entidad = nuevoUser.EntidadDP.EntidadId;
            int zona = int.Parse(usr[3]);
            string[] centroTrabajo = usr[5].Split(',');
            
            string ID_NivelEducacion = usr[7];

            switch (ID_NivelTrabajo)
            {
                case 0://FEDERAL
                    llenaGridSolicitudesExtra(0, 0, int.Parse(ddlCiclo.SelectedValue.ToString()),0);
                    break;
                case 1://SE ESTATAL
                    //Bloquear el estado y mostrar solamente el estado al cual pertenece
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    this.ddlEntidad.Enabled = false;
                    llenaGridSolicitudesExtra(0, 0, int.Parse(ddlCiclo.SelectedValue.ToString()), int.Parse(ID_Entidad.ToString()));
                    break;
                case 2://Region
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "add", "alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);", true);
                    break;
                case 3://ZONA
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "add", "alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);", true);
                    break;
                case 4://CCT
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "add", "alert('Permisos insuficientes para acceder a la p" + '\u00e1' + "gina solicitada');history.back(-1);", true);
                    break;
            }
        }
        private void Cuestionario()
        {
            
            SEroot.WsEstadisticasEducativas.CuestionarioDP[] cuestionarioDP = null;

            cuestionarioDP = wsEstadisticas.GetComboCuestionario(1);
            ddlCuestionarioIni.DataSource = cuestionarioDP;
            ddlCuestionarioIni.DataTextField = "Descrpcion";
            ddlCuestionarioIni.DataValueField = "ID_Cuestionario";
            ddlCuestionarioIni.DataBind();

            cuestionarioDP = wsEstadisticas.GetComboCuestionario(2);
            ddlCuestionarioFin.DataSource = cuestionarioDP;
            ddlCuestionarioFin.DataTextField = "Descrpcion";
            ddlCuestionarioFin.DataValueField = "ID_Cuestionario";
            ddlCuestionarioFin.DataBind();
            
        }

        protected void btnAddSolicitud_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (ddlEntidad.SelectedValue == "0")
            {
                lblError.Text = "Debe seleccionar una Entidad";
                return;
            }

            SEroot.ServiceSolicitudDBF.SolicituddbfDP solicitudDbfDP = new SEroot.ServiceSolicitudDBF.SolicituddbfDP();

            
            string[] usr = User.Identity.Name.Split('|');
            int ID_Usuario = int.Parse(usr[0]);

            int IDCuestionario=0;
            int IDTipoCuestionario=0;
            if(rblTipo.SelectedValue=="1")//si el tipo de cuestionario es uno  que es Inicio
            {
                //si es tipo uno toma el valos del ddl de Inicio
                IDTipoCuestionario=1;
                IDCuestionario=int.Parse(ddlCuestionarioIni.SelectedValue.ToString());
            }
            else if (rblTipo.SelectedValue == "2")
            {
                //si es tipo dos toma el valos del ddl de FIN
                IDTipoCuestionario=2;
                IDCuestionario=int.Parse(ddlCuestionarioFin.SelectedValue.ToString());
            }

            #region llena el objeto solicitud dbf
            //solicitudDbfDP.Archivo=null; sin archivo porque es el insert
            solicitudDbfDP.BitActivo=true;
            //fecha actualizacion y fecha alta no se usan porque en el insert usa getdate()
            //solicitudDbfDP.Fechaactualizacion =;
            //solicitudDbfDP.Fechaalta = ;
            solicitudDbfDP.Fechafin = obtieneFechaISO(txtFecFin.Text);
            solicitudDbfDP.Fechainicio = obtieneFechaISO(txtFecInicio.Text); 
            solicitudDbfDP.IdCicloescolar=int.Parse(ddlCiclo.SelectedValue.ToString());
            solicitudDbfDP.IdCuestionario=IDCuestionario;//tomado de uno de los ddl
            solicitudDbfDP.IdEntidad=int.Parse(ddlEntidad.SelectedValue.ToString());
            solicitudDbfDP.IdEstadosolicitud=1;
            solicitudDbfDP.IdTipocuestionario=IDTipoCuestionario;
            solicitudDbfDP.IdUsuario=ID_Usuario;
            #endregion
            int resul = 0;
            resul=wsSolicitudDBF.InserSolicitudDBF(solicitudDbfDP);
            if (resul > 0)
            {
                StringBuilder cstext2 = new StringBuilder();
                cstext2.Append("alert('Solicitud agregada')");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "add", cstext2.ToString(), true);
            }
            else
            {
                StringBuilder cstext2 = new StringBuilder();
                cstext2.Append("alert('Solicitud no agregada')");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "noadd", cstext2.ToString(), true);
            }

            llenaGridSolicitudes();
        }

        private void llenaGridSolicitudesExtra(int id_cuestionario, int id_estadoSolicitud, int cicloEscolar, int id_Entidad)
        {
            SEroot.ServiceSolicitudDBF.VW_SolicituddbfDP[] VWsolicitudDbfDP = null;
            VWsolicitudDbfDP=wsSolicitudDBF.Load_VW_SolicituddbfDP( id_cuestionario,  id_estadoSolicitud,  cicloEscolar,id_Entidad);
            gvSolPend.DataSource = VWsolicitudDbfDP;
            gvSolPend.DataBind();
        }
        private void llenaGridDescargas()
        {
            
            SEroot.ServiceSolicitudDBF.VW_SolicituddbfDP[] VWsolicitudDbfDP = null;

            UsuarioSeDP nuevoUser = SeguridadSE.GetUsuario(HttpContext.Current);
            int ID_Entidad = nuevoUser.EntidadDP.EntidadId;
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);

            int cicloEscolar = int.Parse(ddlCiclo.SelectedValue.ToString());

            switch (ID_NivelTrabajo)
            {
                case 0://FEDERAL
                    VWsolicitudDbfDP = wsSolicitudDBF.Load_VW_SolicituddbfDP(0, 3, cicloEscolar, 0);
                    break;
                case 1://SE ESTATAL
                    //Bloquear el estado y mostrar solamente el estado al cual pertenece
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    this.ddlEntidad.Enabled = false;
                    VWsolicitudDbfDP = wsSolicitudDBF.Load_VW_SolicituddbfDP(0, 3, cicloEscolar, ID_Entidad);
                    break;
            }

            
           
            gvDescDisp.DataSource = VWsolicitudDbfDP;
            gvDescDisp.DataBind();
        }

        protected void gvSolPend_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Cancelar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvSolPend.Rows[index];
                int id_SolicitudDBF = int.Parse(row.Cells[0].Text);
                SEroot.ServiceSolicitudDBF.SolicituddbfDP sDbfDP = wsSolicitudDBF.LoadSolicituddbfDP(id_SolicitudDBF);
                sDbfDP.IdEstadosolicitud = 4;//EL ESTADO DE CANCELADO ES 4
                int res = wsSolicitudDBF.UpdateSolicituddbfDP(sDbfDP);
                if (res > 0)
                {
                    StringBuilder cstext2 = new StringBuilder();
                    cstext2.Append("alert('Solicitud cancelada')");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "can", cstext2.ToString(), true);
                    
                    llenaGridSolicitudes();
                }
            }
        }

        //para fechas con formato dd/MM/yyyy
        protected string obtieneFechaISO(string fecha)
        {
            if (fecha != "")
            {
                string[] arrFecha = fecha.Split('/');
                return arrFecha[2] + arrFecha[1] + arrFecha[0];
            }
            else
                return "";
        }

        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            llenaGridSolicitudes();
            llenaGridDescargas();
        }

        protected void gvSolPend_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSolPend.PageIndex = e.NewPageIndex;
            llenaGridSolicitudes(); 
        }

        protected void gvDescDisp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDescDisp.PageIndex = e.NewPageIndex;
            llenaGridDescargas(); 
        }

       

       
    }
}
