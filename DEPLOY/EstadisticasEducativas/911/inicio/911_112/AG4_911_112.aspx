<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AG4_911_112.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_112.AG4_911_112" Title="911.112(4�)" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1250px; height:65px;">
    <div id="header" style="text-align:center;">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PRIMARIA IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1250px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_112',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_911_112',true)"><a href="#" title=""><span>1�</span></a></li><li onclick="openPage('AG2_911_112',true)"><a href="#" title=""><span>2�</span></a></li><li onclick="openPage('AG3_911_112',true)"><a href="#" title=""><span>3�</span></a></li><li onclick="openPage('AG4_911_112',true)"><a href="#" title="" class="activo"><span>4�</span></a></li><li onclick="openPage('AG5_911_112',false)"><a href="#" title=""><span>5�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>6�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>      
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

           
         <table style="text-align:center; width: 700px;">
            <tr>
                    <td>
                    <table>
                    <tr>
                                    <td colspan="11" style="padding-bottom: 20px; text-align:center">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblRojo" 
                                        Text="Estad�stica de alumnos por grado, sexo, nuevo ingreso, repetidores y edad"
                                            ></asp:Label>
                                    </td>
                                </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbl1" runat="server" Text="4�" CssClass="lblRojo" Font-Size="25px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl8" runat="server" Text="8 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td style="width: 81px">
                                <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="2" style="width: 75px">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV167" runat="server" Columns="3" ReadOnly="True" TabIndex="10101" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV168" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10102" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV169" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10103" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV170" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10104" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV171" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10105" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV172" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10106" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV173" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10107" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV174" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10108" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV175" runat="server" Columns="4" ReadOnly="True" TabIndex="10109" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV176" runat="server" Columns="3" ReadOnly="True" TabIndex="10201" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV177" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10202" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV178" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10203" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV179" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10204" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV180" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10205" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV181" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10206" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV182" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10207" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV183" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10208" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV184" runat="server" Columns="4" ReadOnly="True" TabIndex="10209" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="11" rowspan="1">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" style="width: 75px">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionM" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV185" runat="server" Columns="3" ReadOnly="True" TabIndex="10301" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV186" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10302" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV187" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10303" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV188" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10304" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV189" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10305" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV190" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10306" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV191" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10307" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV192" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10308" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV193" runat="server" Columns="4" ReadOnly="True" TabIndex="10309" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaM" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV194" runat="server" Columns="3" ReadOnly="True" TabIndex="10401" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV195" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10402" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV196" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10403" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV197" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10404" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV198" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10405" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV199" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10406" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV200" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10407" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV201" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10408" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV202" runat="server" Columns="4" ReadOnly="True" TabIndex="10409" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="11" rowspan="1">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                            </td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="SUBTOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV203" runat="server" Columns="3" ReadOnly="True" TabIndex="10501" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV204" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10502" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV205" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10503" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV206" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10504" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV207" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10505" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV208" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10506" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV209" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10507" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV210" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10508" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV211" runat="server" Columns="4" ReadOnly="True" TabIndex="10509" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr></tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG3_911_112',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG3_911_112',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                 <td style="width: 330px; ">&nbsp;
                </td>
                <td ><span  onclick="openPage('AG5_911_112',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG5_911_112',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>

          <div class="divResultado" id="divResultado"></div> 


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
    <script type="text/javascript" language="javascript">
        MaxCol = 10;
        MaxRow = 11;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
        
        var CambiarPagina = "";
        var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
        GetTabIndexes();
    </script>
</asp:Content>
