using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

//using SEroot.WsSESeguridad;
using Mx.Gob.Nl.Educacion;
using SEroot.WsEstadisticasEducativas;


namespace EstadisticasEducativas._911.inicio
{
    public partial class BuscaCT_Inicio : System.Web.UI.Page
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadiscticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //txtRegion.Attributes["onkeypress"] = "return Num(event)";
                txtZona.Attributes["onkeypress"] = "return Num(event)";
                btnBuscar.Attributes["onblur"] = "javascript:_enter=true;";
                btnBuscar.Attributes["onfocus"] = "javascript:_enter=false;";
                Entidad();
                Nivel();
                GridVacio();
                Filtros();
                Ciclos();
            }
        }
        
        private void Filtros()
        {
            UsuarioSeDP nuevoUser = SeguridadSE.GetUsuario(HttpContext.Current);
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            int region = int.Parse(usr[2]);
            int zona = int.Parse(usr[3]);
            string[] centroTrabajo = usr[5].Split(',');
            SEroot.WsCentrosDeTrabajo.ZonaDP zonaDP = ws.Load_Zona(zona);
            string ID_NivelEducacion = usr[7];
            int ID_Entidad = nuevoUser.EntidadDP.EntidadId;

            //ddlNivel.Enabled = false;

            switch (ID_NivelTrabajo)
            {
                case 0://FEDERAL
                    break;
                case 1://SE
                    //Bloquear el estado y mostrar solamente el estado al cual pertenece
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);

                    this.ddlEntidad.Enabled = false;
                    break;
                case 2://Region
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;
                    break;
                case 3:
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;

                    this.txtZona.Text = zona.ToString();
                    this.txtZona.Enabled = false;
                    break;
                case 5:
                case 4:

                    //Entidad
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    try
                    {
                        //Si truena por que estan mal establecidos los identificadores
                        setRegion(ID_Entidad);
                        //Region
                        this.ddlRegion.SelectedValue = region.ToString();
                        //Zona
                        this.txtZona.Text = zona.ToString();
                    }
                    catch (Exception ex1)
                    {
                        Response.Write("<script language='javascript'> alert('Verifique con el Administrador la Regi�n de su Escuela.Error #R01');</script>");
                    }
                    //Desabilitando los control para la edicion
                    this.ddlEntidad.Enabled = false;
                    this.ddlRegion.Enabled = false;
                    this.txtZona.Enabled = false;
                    this.txtClaveCT.Enabled = false;


                    //Es director de mas de una escuela
                    //try Catch enorme para que si truena, no lo haga de forma inapropiada :P
                    try
                    {
                        if (centroTrabajo.Length > 1)
                        {
                            // txtRegion.Text = "";
                            txtZona.Text = "";
                            //txtRegion.Enabled = false;
                            txtZona.Enabled = false;

                            txtClaveCT.Visible = false; txtClaveCT.Text = "";
                            ddlCentroTrabajo.Visible = true;
                            for (int i = 0; i < centroTrabajo.Length; i++)
                            {
                                SEroot.WsCentrosDeTrabajo.CentroTrabajoDP ctDP = ws.Load_CT(int.Parse(centroTrabajo[i]));

                                ddlCentroTrabajo.Items.Add(new ListItem(ctDP.clave, ctDP.clave));
                            }
                            ddlCentroTrabajo.SelectedIndex = 0;
                        }
                        //Es director de SOLO una escuela                        
                        else
                        {
                            #region codigo viejo
                            //  ddlNivel.Enabled = false;

                            ////  txtRegion.Text = region.ToString();
                            ////  txtRegion.Enabled = false;
                            //  txtZona.Text = zonaDP.numeroZona.ToString();
                            //  txtZona.Enabled = false;

                            //  SEroot.WsCentrosDeTrabajo.CentroTrabajoDP ctDP = ws.Load_CT(int.Parse(centroTrabajo[0]));
                            //  txtClaveCT.Visible = true; 
                            //  txtClaveCT.Text = ctDP.clave;

                            //  ddlCentroTrabajo.Visible = false;
                            #endregion
                            if (centroTrabajo.Length == 1)
                            {
                                int ID_Ciclo = int.Parse(nuevoUser.Selecciones.CicloEscolarSeleccionado.CicloescolarId.ToString()); //5; //int.Parse(ddlCiclo.SelectedValue);
                                //string[] usr = User.Identity.Name.Split('|');
                                int ID_Usuario = int.Parse(usr[0]);


                                string lbl = nuevoUser.Selecciones.CentroTrabajoSeleccionado.CctntId.ToString();

                                //SEroot.WsCentrosDeTrabajo.CentroTrabajoDP ctDP = ws.Load_CT(int.Parse(centroTrabajo[0]));

                                //this.txtClaveCT.Text= nuevoUser.CctNTs[0].Clavecct;
                                this.txtClaveCT.Text = nuevoUser.CentrosTrabajo[0].Clave;


                                SeguridadSE.SetCctNt(this.Page, int.Parse(lbl));
                                int id_cctnt = int.Parse(lbl);
                                ControlDP controlDP = wsEstadiscticas.ObtenerDatosEncuestaID_CCTNT(id_cctnt, Class911.ID_Inicio, ID_Ciclo, ID_Usuario);
                                Class911.SetControlSeleccionado(HttpContext.Current, controlDP);
                                string path = Ruta(controlDP.ID_Cuestionario);
                                Response.Write("<script language='javascript'> window.open('" + path + "', 'window','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668');</script>");
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                        Response.Write("<script language='javascript'> alert('Consulte a su Administrador. Error #R02.');</script>");
                    }
                    break;
            }
        }

        private void Entidad()
        {
            SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
            SEroot.WsEstadisticasEducativas.CatListaEntidad911DP[] listaEntidades = wsEstadisticas.Lista_CatListaEntidad911(223);//EL ID PAIS = 223 es M�XICO
            for (int i = 0; i < listaEntidades.Length; i++)
            {
                string valor = listaEntidades[i].ID_Entidad.ToString();
                string texto =listaEntidades[i].Nombre; 
                ddlEntidad.Items.Add(new ListItem(texto, valor));
                
            }
        }

        private void Nivel()
        {
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            //Response.Write(usr[0]);
            if (usr[0] == "352599")
            {
                ddlNivel.Items.Add(new ListItem("CONALEP", "21_20"));
            }
            else if (ID_NivelTrabajo == 0 || ID_NivelTrabajo == 1 || ID_NivelTrabajo == 2 || ID_NivelTrabajo == 3)
            {
                SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

                SEroot.WsEstadisticasEducativas.CatListaNiveles911DP[] listaNiveles = wsEstadisticas.Lista_CatListaNiveles911(Class911.ID_Inicio);
                int secundaria = 0;
                for (int i = 0; i < listaNiveles.Length; i++)
                {
                    string valor;
                    if (listaNiveles[i].ID_Nivel != 13)
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_" + listaNiveles[i].ID_SubNivel.ToString();
                        secundaria = 0;
                    }
                    else
                    {
                        valor = listaNiveles[i].ID_Nivel.ToString() + "_0";
                        secundaria++;
                    }
                    if (secundaria < 2)
                    {
                        string texto = listaNiveles[i].Descrip;
                        ddlNivel.Items.Add(new ListItem(texto, valor));
                    }
                }
            }
            else
            {
                ddlNivel.Enabled = false;
                ddlNivel.Items.Add(new ListItem("", "0_0"));
            }
        }

        private void Ciclos()
        {
            SEroot.WSEscolar.WsEscolar wsEscolar = new SEroot.WSEscolar.WsEscolar();
            SEroot.WSEscolar.DsCiclosEscolares dsCiclos = wsEscolar.ListaCicloEscolarCombo(1);//Anual
           
            ddlCiclo.DataSource = dsCiclos.CicloEscolar;
            ddlCiclo.DataValueField = "Id_CicloEscolar";
            ddlCiclo.DataTextField = "Nombre";
            ddlCiclo.DataBind();

            getUsr();

            ListItem itemActual = ddlCiclo.Items.FindByValue(usr.Selecciones.CicloEscolarSeleccionado.CicloescolarId.ToString());
            if (itemActual != null)
            {
                itemActual.Selected = true;
            }
           
        }

        //protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        DataRowView rowView = (DataRowView)e.Row.DataItem;
        //        ImageButton ib = new ImageButton();
        //        ib.ID = "est9111";
        //        ib.ImageUrl = "../../tema/images/iconEsc.gif";
        //        ib.CommandName = "ImageButton";
        //        ib.OnClientClick = "window.open('911_1/Identificacion_911_1.aspx?cnt=" + rowView["ID_CCTNT"].ToString() + "&cic=" + ddlCiclo.SelectedValue + "', 'ventana911', 'width=924,height=800,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668');return false;";
        //        ib.Visible = true;
        //        e.Row.Cells[0].Controls.Add(ib);
        //        e.Row.Cells[0].Visible = true;
        //    }
        //}

        protected void GridVacio()
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            Session["dsGrid"] = null;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            LlenaGridView_2();
        }

        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }

        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.DataSource = SortDataTable(true);
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected DataView SortDataTable(bool isPageIndexChanging)
        {
            //DataSet ds = (DataSet)Session["dsGrid"];
            //if (ds != null)
            if (Session["dsGrid"] != null)
            {
                //DataTable dataTable = new DataTable();
                //dataTable = ds.Tables[0];

                DataTable dataTable = new DataTable();
                dataTable = (DataTable)Session["dsGrid"];

                if (dataTable != null)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
            else
            {
                return new DataView();
            }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridViewSortExpression = e.SortExpression;
            int pageIndex = GridView1.PageIndex;
            GridView1.DataSource = SortDataTable(false);
            GridView1.DataBind();
            GridView1.PageIndex = pageIndex;
        }

      
        protected void RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            if (row != null)
            {
                int ID_Ciclo = int.Parse(ddlCiclo.SelectedValue);
                string[] usr = User.Identity.Name.Split('|');
                int ID_Usuario = int.Parse(usr[0]);

                Label lbl = (Label)row.FindControl("lblID_CCTNT");

                //EstablecerBarrita(0, 0, 0, 0, int.Parse(lbl.Text));

                SeguridadSE.SetCctNt(this.Page, int.Parse(lbl.Text));
                int id_cctnt = int.Parse(lbl.Text);
                ControlDP controlDP = wsEstadiscticas.ObtenerDatosEncuestaID_CCTNT(id_cctnt, Class911.ID_Inicio, ID_Ciclo, ID_Usuario);
                Class911.SetControlSeleccionado(HttpContext.Current, controlDP);
                string path = Ruta(controlDP.ID_Cuestionario);
                string codigoJS = "window.open('" + path + "', 'window','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668');";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SC123", codigoJS, true);
                //Response.Write("<script language='javascript'> window.open('" + path + "', 'window','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668');</script>");

            }
        }

        protected void LlenaGridView_2()
        {
            SEroot.WsCentrosDeTrabajo.DsBuscaCCT911 Ds = new SEroot.WsCentrosDeTrabajo.DsBuscaCCT911();
            int reg = 0;
            int zon = 0;
            string ct = "";
            //if (txtRegion.Text.Trim().Length > 0)
            //{
            //    reg = int.Parse(txtRegion.Text.Trim());
            //}
            //Ahora la region la obtiene del dropdownlist

            reg = int.Parse(ddlRegion.SelectedValue);

            if (txtZona.Text.Trim().Length > 0)
            {
                zon = int.Parse(txtZona.Text.Trim());
            }
            if (txtClaveCT.Visible)
            {
                if (txtClaveCT.Text.Trim().Length > 0)
                {
                    ct = txtClaveCT.Text.Trim();
                }
            }
            else
            {
                ct = ddlCentroTrabajo.SelectedValue;
            }

            string valor = ddlNivel.SelectedValue;



            int ID_Entidad = int.Parse(ddlEntidad.SelectedValue);
            int ID_Nivel = int.Parse(valor.Split('_')[0]);

            int ID_SubNivel = int.Parse(valor.Split('_')[1]);

            Ds = ws.ListaEscuelas911(ID_Entidad,reg, zon, ct, ID_Nivel, ID_SubNivel);

            if (Ds != null && Ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = Ds.CCT911;
                GridView1.DataBind();
                Session["dsGrid"] = Ds.CCT911;
                lblMsg.Text = "Registros encontrados: " + Ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                GridVacio();
                lblMsg.Text = "No se encontraron registros";
            }
        }

        protected void ddlNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtRegion.Text = "";
            //txtZona.Text = "";
            //txtClaveCT.Text = "";
            lblMsg.Text = "";
            GridVacio();
        }
   
        UsuarioSeDP usr = null;
        protected void getUsr()
        {
            if (usr == null)
                usr = SeguridadSE.GetUsuario(HttpContext.Current);
        }
       
        protected void EstablecerBarrita(int ID_Nviel, int ID_Region, int ID_Sostenimiento, int ID_Zona, int ID_CCTNT)
        {
            getUsr();
            //SERaiz.Controles.FiltraCCTporZona FiltraCCTporZona1 = (SERaiz.Controles.FiltraCCTporZona)Page.Master.FindControl("FiltraCCTporZona1");

         
            if ((ID_CCTNT == -1 || ID_CCTNT == 0) && (ID_Zona == -1 || ID_Zona == 0) && (ID_Region == -1 || ID_Region == 0))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('���Seleccione un Centro de Trabajo!!!');", true);
            }
            else
            {
                if (ID_CCTNT != 0 && ID_CCTNT != -1)
                    SeguridadSE.SetCctNt(this.Page, ID_CCTNT);//Set Centro Trabajo
                else
                    if (ID_Zona != 0 && ID_Zona != -1)
                        SeguridadSE.SetCctNt(this.Page, ID_Zona);//Set Zona
                    else
                        if (ID_Region != 0 && ID_Region != -1)
                            SeguridadSE.SetCctNt(this.Page, ID_Region);//Set Region
                        else
                            if (usr.NivelTrabajo.NiveltrabajoId == 1)
                                SeguridadSE.SetCctNt(this.Page, 2);//Set Secretaria de Educacion
                            else
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('���Seleccione un Centro de Trabajo!!!');", true);

                ((Label)Page.Master.FindControl("lblCCTSeleccionado")).Text = SeguridadSE.cctSelected.Clavecct + " - " + SeguridadSE.cctSelected.Nombrecct + " - " + SeguridadSE.cctSelected.Truno;
            }
            if (SeguridadSE.cicloSelected != null)
            {
                if (SeguridadSE.cicloSelected.Nombre != "")
                    ((Label)Page.Master.FindControl("lblCicloEscolar")).Text = SeguridadSE.cicloSelected.Nombre;
            }
            //FiltraCCTporZona1.filtrosPermisos(usr);
        }

       private string Ruta(int id_cuestionario)
        {
            string abrirVentana = "";
            switch (id_cuestionario)
            {
                case 15:
                    abrirVentana = "EI_1/Identificacion_EI_1.aspx";
                    break; 
                case 16:
                    abrirVentana = "EI_NE1/Identificacion_EI_NE1.aspx";
                    break;
                case 17:
                    abrirVentana = "911_USAER_1/Identificacion_911_USAER_1.aspx";
                    break;
                case 18:
                    abrirVentana = "CAM_1/Identificacion_CAM_1.aspx";
                    break;
                case 19:
                    abrirVentana = "911_1/Identificacion_911_1.aspx";
                    break;
                case 20:
                    abrirVentana = "ECC_11/Identificacion_ECC_11.aspx";
                    break;
                case 21:
                    abrirVentana = "911_111/Identificacion_911_111.aspx";
                    break;
                case 22:
                    abrirVentana = "911_3/Identificacion_911_3.aspx";
                    break;
                case 23:
                    abrirVentana = "ECC_12/Identificacion_ECC_12.aspx";
                    break;
                case 24:
                    abrirVentana = "911_112/Identificacion_911_112.aspx";
                    break;
                case 25:
                    abrirVentana = "911_5/Identificacion_911_5.aspx";
                    break;
                case 26:
                    abrirVentana = "911_7G/Identificacion_911_7G.aspx";
                    break;
                case 27:
                    abrirVentana = "911_7T/Identificacion_911_7T.aspx";                    
                    break;
                case 28:
                    abrirVentana = "911_7P/Identificacion_911_7P.aspx";
                    break;
                default:
                    abrirVentana = "Reportes/Reporte.aspx";
                    break;

            }
            return abrirVentana;
        }
        public void setRegion(int ID_Entidad)
        {
            SEroot.WsCentrosDeTrabajo.DsCctRegion Ds = new SEroot.WsCentrosDeTrabajo.DsCctRegion();

            if (ID_Entidad > 0)//Solo cuando es un estado consulta
            {
                Ds = ws.ListaRegiones(223, ID_Entidad);
                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddlRegion.Items.Clear();
                    this.ddlRegion.Items.Add(new ListItem("Todas las regiones", "0"));
                    this.ddlRegion.AppendDataBoundItems = true;
                    this.ddlRegion.DataSource = Ds;
                    this.ddlRegion.DataMember = "Region";
                    this.ddlRegion.DataTextField = "Nombre";
                    this.ddlRegion.DataValueField = "ID_Region";
                    //this.ddlRegion.SelectedIndex = 1;
                    this.ddlRegion.DataBind();

                    this.ddlRegion.Enabled = true;//Una vez que tiene contenido, desplegar
                }
            }
            else// de lo Contrario limpia el Dropdownlist
            {
                this.ddlRegion.Items.Clear();
                this.ddlRegion.Items.Add(new ListItem("Seleccione una Regi�n", "0"));
                this.ddlRegion.Enabled = false;
            }



        }
        
        //Usando el WebService WSCentrosdeTrabajo para obtenes las regiones de un Estado
        protected void ddlEntidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            SEroot.WsCentrosDeTrabajo.DsCctRegion Ds = new SEroot.WsCentrosDeTrabajo.DsCctRegion();
            int ID_Entidad= int.Parse(ddlEntidad.SelectedValue);
            if (ID_Entidad > 0)//Solo cuando es un estado consulta
            {
                Ds = ws.ListaRegiones(223, ID_Entidad);
                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddlRegion.Items.Clear();
                    this.ddlRegion.Items.Add(new ListItem("Todas las regiones", "0"));
                    this.ddlRegion.AppendDataBoundItems = true;
                    this.ddlRegion.DataSource = Ds;
                    this.ddlRegion.DataMember = "Region";
                    this.ddlRegion.DataTextField = "Nombre";
                    this.ddlRegion.DataValueField = "ID_Region";
                    //this.ddlRegion.SelectedIndex = 1;
                    this.ddlRegion.DataBind();
                    this.ddlRegion.Enabled = true;
                }
            }
            else// de lo Contrario limpia el Dropdownlist
            {
                this.ddlRegion.Items.Clear();
                this.ddlRegion.Items.Add(new ListItem("Seleccione una Regi�n", "0"));
                this.ddlRegion.Enabled = false;
            }


        }





        //END FILE
    }
}
