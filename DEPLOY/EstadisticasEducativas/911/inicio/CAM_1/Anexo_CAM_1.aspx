<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="Anexo_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.Anexo_CAM_1" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <div id="logo"></div>
    <div style="min-width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N ESPECIAL"></asp:Label></td>
        </tr>
         <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div></div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span>INICIAL Y PREE</span></a></li>
        <li onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><span>PRIM Y SEC</span></a></li>
        <li onclick="openPage('AG3_CAM_1',true)"><a href="#" title=""><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li>
        <li onclick="openPage('Desgolse_CAM_1',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal1_CAM_1',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('Personal2_CAM_1',true)"><a href="#" title=""><span>PERSONAL(CONT)</span></a></li>
        <li onclick="openPage('CMYA_CAM_1',true)"><a href="#" title=""><span>CARRERA MAG Y AULAS</span></a></li>
        <li onclick="openPage('Gasto_CAM_1',true)"><a href="#" title=""><span>GASTO</span></a></li>
        <li onclick="openPage('Anexo_CAM_1',true)"><a href="#" title="" class="activo"><span>ANEXO</span></a></li>
        <li onclick="openPage('Oficializacion_CAM_1',false)"><a href="#" title="" ><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
 
      <!--Tabla Fondo-->
      <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"></td>
				    <td>   
 					 <!--Contenido dentro del area de trabajo-->

                            <table >
                                <tr>
                                    <td valign="top" style="text-align: left">
                                        <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="IMPORTANTE: La escuela debe informar los recursos que tiene a su disposici�n, independientemente de que los comparta o no con otra escuela."
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td valign="top" style="text-align: left">
                                        <asp:Label ID="Label35" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                            Text="RECURSOS COMPUTACIONALES" Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 800px">
                                            <tr>
                                                <td style="text-align: left;" colspan="">
                                        <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                                            Text="1. �La escuela cuenta con equipo de c�mputo?"
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                
                                                <td style="text-align: center;" colspan="">
                                                    <asp:RadioButton TabIndex="10101" onkeydown="return Arrows(event,this.tabIndex)"  onclick = "OPTs2Txt('show1');" ID="optVANX1" runat="server" GroupName="Preg1" Text="SI"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX1" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2"  style="visibility:hidden;"></asp:TextBox>
                                                    <asp:RadioButton  TabIndex="10201" onkeydown="return Arrows(event,this.tabIndex)"  onclick = "OPTs2Txt('hide1');" ID="optVANX2" runat="server" GroupName="Preg1" Text="NO"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX2" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2"  style="visibility:hidden;" ></asp:TextBox></td>
                                            </tr>
                                           <%-- empieza seccion compus--%>
                                           <tr><td>
                                           <table id="tcompu">
                                            <tr>
                                                <td colspan="" style="text-align: left">
                                                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="En caso de que la respuesta sea No, pase a la secci�n  de RECURSOS AUDIOVISUALES"
                                                        Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;" colspan="">
                                                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. �Cu�ntas computadoras tiene la escuela?"
                                                        Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center" colspan="">
                                        <table>
                                <tr>
                                    <td style="text-align: center; height: 19px;" colspan="2">
                                        <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="N�mero de computadoras"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="En operaci�n"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtVANX3" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Descompuestas"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtVANX4" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Guardadas o en reserva"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtVANX6" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: left">
                                        <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtVANX7" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
                                </tr>
                            </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="" style="text-align: left">
                                        <asp:Label ID="lblInstruccion2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. De las computadoras en operaci�n, escriba las cantidades seg�n corresponda."
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center; height: 319px;" colspan="">
                            <table width="800">
                                <tr>
                                    <td class="linaBajoAlto">
                                        <asp:Label ID="lbl29" runat="server" CssClass="lblRojo" Font-Bold="True" Text="COMPUTADORAS EN OPERACI�N"
                                            Width="100px"></asp:Label></td>
                                    <td colspan="5" style="text-align: center" class="linaBajoAlto">
                                        <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Font-Bold="True" Text="CANTIDAD SEG�N LAS CARACTER�STICAS INDICADAS"
                                            Width="540px"></asp:Label></td>
                                    <td colspan="1" style="text-align: center" class="linaBajoAlto">
                                        <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                    <td class="Orila" colspan="1" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td rowspan="2" class="linaBajo">
                                        <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEG�N SU USO"
                                            Width="100px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EDUCATIVO"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ADMINISTRATIVO"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                            Text="EDUCATIVO Y ADMINISTRATIVO" Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRO"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO S�"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        &nbsp;</td>
                                    <td class="Orila" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX8" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX9" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20502"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX10" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20503"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX11" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20504"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX12" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20505"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX13" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20506"></asp:TextBox></td>
                                    <td class="Orila" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td rowspan="2" class="linaBajo">
                                        <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TIPO DE PROCESADOR"
                                            Width="100px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="486 Y ANTERIOR"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PENTIUM I Y II"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PENTIUM III Y POSTERIOR"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRO"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO S�"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        &nbsp;</td>
                                    <td class="Orila" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX14" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX15" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20602"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX16" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20603"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX17" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20604"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX18" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20605"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX19" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20606"></asp:TextBox></td>
                                    <td class="Orila" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td rowspan="2" class="linaBajo">
                                        <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TIPO DE MONITOR"
                                            Width="100px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="VGA Y ANTERIOR"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label19" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUPER VGA"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ULTRA, XVGA Y SUPERIOR"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRO"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label22" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO S�"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        &nbsp;</td>
                                    <td class="Orila" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX20" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX21" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20702"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX22" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20703"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX23" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20704"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX24" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20705"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX25" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20706"></asp:TextBox></td>
                                    <td class="Orila" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td rowspan="2" class="linaBajo">
                                        <asp:Label ID="Label23" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CAPACIDAD DE DISCO DURO"
                                            Width="100px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MENOS DE 30 GB"
                                            Width="75px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="DE 30 A 40 GB"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M�S DE 40 GB"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRO"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        <asp:Label ID="Label28" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO S�"
                                            Width="80px"></asp:Label></td>
                                    <td style="text-align: center" class="Orila">
                                        &nbsp;</td>
                                    <td class="Orila" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX107" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX108" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20802"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX109" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20803"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX110" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20804"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX111" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20805"></asp:TextBox></td>
                                    <td style="text-align: center" class="linaBajo">
                                        <asp:TextBox ID="txtVANX112" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="20806"></asp:TextBox></td>
                                    <td class="Orila" style="text-align: center">
                                        &nbsp;</td>
                                </tr>
                            </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    <asp:Label ID="Label29" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. �Cu�ntas computadoras en operaci�n tienen lector de CD-ROM o DVD?"
                                                        Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    <asp:TextBox ID="txtVANX26" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="20901"></asp:TextBox>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    <asp:Label ID="Label30" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5. �Cu�ntas computadoras en operaci�n tienen acceso a internet?"
                                                        Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    <asp:TextBox ID="txtVANX91" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="21001">
                                                    </asp:TextBox>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: center">
                            <table>
                                <tr>
                                    <td colspan="1" style="width: 356px">
                                        <asp:Label ID="lblGRUPOS" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TIPO DE ACCESO A INTERNET"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21101" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX92" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Por l�nea telef�nica conmutada (m�dem 56 Kbps)"
                                            Width="300px" ValidationGroup="Preg5T"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX92" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"  style="visibility:hidden;"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21201" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX93" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Por l�nea telef�nica permanente (ADSL)"
                                            Width="300px" ValidationGroup="Preg5T"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX93" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"   style="visibility:hidden;"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21301" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX94" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Cable de televisi�n"
                                            Width="300px" ValidationGroup="Preg5T"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX94" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"   style="visibility:hidden;"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21401" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX95" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Antena Satelital"
                                            Width="300px" ValidationGroup="Preg5T"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX95" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"   style="visibility:hidden;"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21501" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX96" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Otro"
                                            Width="50px" ValidationGroup="Preg5T"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX96" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"  style="visibility:hidden;"></asp:TextBox>
                                        <asp:TextBox ID="txtVANX97" runat="server" Columns="30" CssClass="lblNegro" MaxLength="30" TabIndex="21502"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21601" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX98" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="No s�"
                                            Width="300px" ValidationGroup="Preg5T"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX98" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"  style="visibility:hidden;" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align: center; width: 356px;">
                                        <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ANCHO DE BANDA"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21701" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX99" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="56 Kbps"
                                            Width="300px" ValidationGroup="Preg5A"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX99" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"  style="visibility:hidden;"
                                            ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21801" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX100" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="64 Kbps a 256 Kbps"
                                            Width="300px" ValidationGroup="Preg5A"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX100" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"   style="visibility:hidden;"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="21901" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX101" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="512 Kbps a 2 Mbps"
                                            Width="300px" ValidationGroup="Preg5A"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX101" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"   style="visibility:hidden;"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="22001" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX102" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Otro"
                                            Width="50px" ValidationGroup="Preg5A"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX102" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"  style="visibility:hidden;"
                                            ></asp:TextBox>
                                        <asp:TextBox ID="txtVANX103" runat="server" Columns="30" CssClass="lblNegro" MaxLength="30" TabIndex="22002"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 356px;">
                                        <asp:CheckBox TabIndex="22101" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX104" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="No s�"
                                            Width="300px" ValidationGroup="Preg5A"></asp:CheckBox>
                                        <asp:TextBox ID="txtVANX104" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"  style="visibility:hidden;"
                                            ></asp:TextBox></td>
                                </tr>
                            </table>
                                                </td>
                                            </tr>
                                           <tr>
                                                <td style="text-align: left;" colspan="">
                                        <asp:Label ID="Label39" runat="server" CssClass="lblRojo" Font-Bold="True"
                                            Text="6. �Su conexi�n a internet es a trav�s del Sistema Nacional e-M�xico?"
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                
                                                <td style="text-align: center;" colspan="">
                                                    <asp:RadioButton TabIndex="22201" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX113" runat="server" GroupName="Preg6" Text="SI"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX113" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2"  style="visibility:hidden;"
                                                        ></asp:TextBox>
                                                    <asp:RadioButton TabIndex="22202" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX114" runat="server" GroupName="Preg6" Text="NO"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX114" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox></td>
                                            </tr>
                                           <tr>
                                                <td style="text-align: left;" colspan="">
                                        <asp:Label ID="Label40" runat="server" CssClass="lblRojo" Font-Bold="True"
                                            Text="7. �La escuela cuenta con su propia direcci�n de correo electr�nico?"
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                
                                                <td style="text-align: center;" colspan="">
                                                    <asp:RadioButton TabIndex="22301" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX33" runat="server" GroupName="Preg7" Text="SI"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX33" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox>
                                                    <asp:RadioButton TabIndex="22302" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX34" runat="server" GroupName="Preg7" Text="NO"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX34" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left; padding-left: 30px;">
                                                    <asp:Label ID="Label41" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="An�tela:"
                                                        Width="50px"></asp:Label><asp:TextBox ID="txtVANX35" runat="server" Columns="30"
                                                            CssClass="lblNegro" MaxLength="30" TabIndex="22401"></asp:TextBox></td>
                                            </tr>
                                           <tr>
                                                <td style="text-align: left;" colspan="">
                                        <asp:Label ID="Label42" runat="server" CssClass="lblRojo" Font-Bold="True"
                                            Text="8. �La escuela cuenta con su propia p�gina web?"
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                
                                                <td style="text-align: center;" colspan="">
                                                    <asp:RadioButton TabIndex="22501" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX36" runat="server" GroupName="Preg8" Text="SI"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX36" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox>
                                                    <asp:RadioButton TabIndex="22502" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX37" runat="server" GroupName="Preg8" Text="NO"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX37" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left; padding-left: 30px;">
                                                    <asp:Label ID="Label43" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="An�tela:"
                                                        Width="50px"></asp:Label>
                                                    <asp:Label ID="Label44" runat="server" CssClass="lblRojo" Font-Bold="True" Text="http://"
                                                        Width="30px"></asp:Label>
                                                    <asp:TextBox ID="txtVANX38" runat="server" Columns="30"
                                                            CssClass="lblNegro" MaxLength="30" TabIndex="22601"></asp:TextBox></td>
                                            </tr>
                                           <tr>
                                                <td style="text-align: left;" colspan="">
                                        <asp:Label ID="Label45" runat="server" CssClass="lblRojo" Font-Bold="True"
                                            Text="9. �La escuela participa en la Red Escolar?"
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                
                                                <td style="text-align: center;" colspan="">
                                                    <asp:RadioButton TabIndex="22701" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX39" runat="server" GroupName="Preg9" Text="SI"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX39" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                       ></asp:TextBox>
                                                    <asp:RadioButton TabIndex="22702" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt('noRed');" ID="optVANX40" runat="server" GroupName="Preg9" Text="NO"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX40" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                      ></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    &nbsp;<asp:Label ID="Label47" runat="server" CssClass="lblRojo" Font-Bold="True" Text="�De qu� forma?"
                                                        Width="100%"></asp:Label>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="padding-left: 30px; text-align: left">
                                                    <table>
                                                        <tr>
                                                            <td style="text-align: left; width: 433px;">
                                                                <asp:RadioButton TabIndex="22801" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt('opcionRed');" ID="optVANX41" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Fue equipada"
                                                                    Width="80%" GroupName="Preg9D"></asp:RadioButton>
                                                                <asp:TextBox ID="txtVANX41" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" style="visibility:hidden;"
                                                                    ></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left; width: 433px;">
                                                                <asp:RadioButton TabIndex="22901" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt('opcionRed');" ID="optVANX43" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Utiliza correo electr�nico e internet"
                                                                    Width="80%" GroupName="Preg9D"></asp:RadioButton>
                                                                <asp:TextBox ID="txtVANX43" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" style="visibility:hidden;"
                                                                 ></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left; width: 433px;">
                                                                <asp:RadioButton TabIndex="23001" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt('opcionRed');" ID="optVANX44" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Participa en proyectos colaborativos y foros de discusi�n"
                                                                    Width="80%" GroupName="Preg9D"></asp:RadioButton>
                                                                <asp:TextBox ID="txtVANX44" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" style="visibility:hidden;"
                                                                   ></asp:TextBox></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            </table>
                                           </td></tr>
                                            <%--termina seccion compus--%>
                                            
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    <asp:Label ID="Label46" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                                        Text="RECURSOS AUDIOVISUALES" Width="100%"></asp:Label></td>
                                            </tr>
                                           <tr>
                                                <td style="text-align: left;" colspan="">
                                        <asp:Label ID="Label48" runat="server" CssClass="lblRojo" Font-Bold="True"
                                            Text="10. �La escuela tiene equipos audiovisuales? (Video, TV, DVD, etc�tera)."
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                
                                                <td style="text-align: center;" colspan="">
                                                    <asp:RadioButton TabIndex="23101" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt('show2');" ID="optVANX45" runat="server" GroupName="Preg10" Text="SI"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX45" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox>
                                                    <asp:RadioButton TabIndex="23102" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt('hide2');" ID="optVANX46" runat="server" GroupName="Preg10" Text="NO"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX46" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox></td>
                                            </tr>
                                            <%--empieza seccion recursos audiovisuales--%>
                                            <tr><td>
                                           <table id="trecursos">
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    <asp:Label ID="Label52" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Registre el n�mero de equipos seg�n corresponda."
                                                        Width="100%"></asp:Label>
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                    <td style="text-align: center" rowspan="2">
                                        <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TV"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center" colspan="2">
                                        <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="VIDEOGRABADORA"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center" rowspan="2">
                                        <asp:Label ID="lblTotal3a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DVD"
                                            Width="100%"></asp:Label></td>
                                    <td rowspan="2" style="text-align: center">
                                        <asp:Label ID="Label53" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:Label ID="Label54" runat="server" CssClass="lblRojo" Font-Bold="True" Text="BETA"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:Label ID="Label55" runat="server" CssClass="lblRojo" Font-Bold="True" Text="VHS"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblEU" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN OPERACI�N"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX47" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23201"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX48" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23202"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX49" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23203"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX50" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23204"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX51" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23205"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblCANADA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="DESCOMPUESTAS"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX52" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23301"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX53" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23302"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX54" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23303"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX55" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23304"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX56" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23305"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblCAYC" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="GUARDADAS O EN RESERVA"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX62" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23401"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX63" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23402"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX64" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23403"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX65" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23404"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX66" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23405"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblSUDAM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX67" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23501"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX68" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23502"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX69" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23503"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX70" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23504"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX71" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="23505" ></asp:TextBox></td>
                                </tr>
                            </table>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left;" colspan="">
                                        <asp:Label ID="Label56" runat="server" CssClass="lblRojo" Font-Bold="True"
                                            Text="11. �La escuela tiene videoteca?"
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                
                                                <td style="text-align: center;" colspan="">
                                                    <asp:RadioButton TabIndex="23601" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX72" runat="server" GroupName="Preg11" Text="SI"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX72" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                       ></asp:TextBox>
                                                    <asp:RadioButton TabIndex="23602" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX73" runat="server" GroupName="Preg11" Text="NO" CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX73" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    <asp:Label ID="Label57" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Escriba la cantidad de videos."
                                                        Width="100%"></asp:Label>
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                    <td style="text-align: center" rowspan="1">
                                        <asp:Label ID="Label58" runat="server" CssClass="lblRojo" Font-Bold="True" Text="BETA"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center" colspan="1">
                                        <asp:Label ID="Label59" runat="server" CssClass="lblRojo" Font-Bold="True" Text="VHS"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center" rowspan="1">
                                        <asp:Label ID="Label60" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DVD"
                                            Width="100%"></asp:Label></td>
                                    <td rowspan="1" style="text-align: center">
                                        <asp:Label ID="Label61" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label64" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="N�MERO DE VOL�MENES"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX74" runat="server" Columns="3" MaxLength="4" CssClass="lblNegro" TabIndex="23701"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX75" runat="server" Columns="3" MaxLength="4" CssClass="lblNegro" TabIndex="23702"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX76" runat="server" Columns="3" MaxLength="4" CssClass="lblNegro" TabIndex="23703"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX77" runat="server" Columns="3" MaxLength="4" CssClass="lblNegro" TabIndex="23704"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label65" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="N�MERO DE T�TULOS"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX78" runat="server" Columns="3" MaxLength="4" CssClass="lblNegro" TabIndex="23801"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX79" runat="server" Columns="3" MaxLength="4" CssClass="lblNegro" TabIndex="23802"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX80" runat="server" Columns="3" MaxLength="4" CssClass="lblNegro" TabIndex="23803"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtVANX81" runat="server" Columns="3" MaxLength="4" CssClass="lblNegro" TabIndex="23804"></asp:TextBox></td>
                                </tr>
                            </table>
                                                    </td>
                                            </tr>
                                             <tr>
                                                <td style="text-align: left;" colspan="">
                                        <asp:Label ID="Label62" runat="server" CssClass="lblRojo" Font-Bold="True"
                                            Text="12. �La escuela recibe la se�al de la Red Edusat?"
                                            Width="100%"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                
                                                <td style="text-align: center;" colspan="">
                                                    <asp:RadioButton TabIndex="23901" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX82" runat="server" GroupName="Preg12" Text="SI"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX82" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                      ></asp:TextBox>
                                                    <asp:RadioButton TabIndex="23902" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX83" runat="server" GroupName="Preg12" Text="NO"  CssClass="lblGrisTit" />
                                                    <asp:TextBox ID="txtVANX83" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2" style="visibility:hidden;"
                                                        ></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="text-align: left">
                                                    <asp:Label ID="Label63" runat="server" CssClass="lblRojo" Font-Bold="True" Text="�De qu� forma?"
                                                        Width="100%"></asp:Label>
                            <table>
                                <tr>
                                    <td><asp:CheckBox TabIndex="24001" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX86" runat="server" ValidationGroup="Preg12D" Text="ANTENA"  CssClass="lblGrisTit" />
                                        <asp:TextBox ID="txtVANX86" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro"  style="visibility:hidden;"></asp:TextBox></td>
                                    <td><asp:CheckBox TabIndex="24101" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX87" runat="server" ValidationGroup="Preg12D" Text="CABLE"  CssClass="lblGrisTit" />
                                        <asp:TextBox ID="txtVANX87" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro"  style="visibility:hidden;"></asp:TextBox></td>
                                    <td><asp:CheckBox TabIndex="24201" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX88" runat="server" ValidationGroup="Preg12D" Text="SE�AL ABIERTA"  CssClass="lblGrisTit" />
                                        <asp:TextBox ID="txtVANX88" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro"  style="visibility:hidden;"></asp:TextBox></td>
                                    <td><asp:CheckBox TabIndex="24301" onkeydown="return Arrows(event,this.tabIndex)" onclick = "OPTs2Txt();" ID="optVANX89" runat="server" ValidationGroup="Preg12D" Text="NO S�"  CssClass="lblGrisTit" />
                                        <asp:TextBox ID="txtVANX89" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro"  style="visibility:hidden;"></asp:TextBox></td>
                                </tr>
                            </table>
                                                    </td>
                                            </tr>
                                            </table>
                                           </td></tr>
                                            <%--termina seccion recursos audiovisuales--%>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <hr />
                            <table align="center">
                                <tr>
                                    <td ><span  onclick="openPage('Gasto_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                                    <td ><span  onclick="openPage('Gasto_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                                    <td style="width: 330px;">
                                        <center>
                                            &nbsp;</center>
                                    </td>
                                    <td ><span  onclick="openPage('Oficializacion_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                                    <td ><span  onclick="openPage('Oficializacion_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                                </tr>
                            </table>
                            
                            <div id="divResultado" class="divResultado" ></div>
                            <!--/Contenido dentro del area de trabajo-->
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
         </table>
         <!--/Tabla Fondo-->

        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
           
        <script type="text/javascript" language="javascript">
                MaxCol = 20;
                MaxRow = 44;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                  function OPTs2Txt(hs){
                     hideshow(hs);
                     marcarTXT('1');
                     marcarTXT('2'); 
                     marcarTXT('33');
                     marcarTXT('34');
                     marcarTXT('36');
                     marcarTXT('37');
                     marcarTXT('39');
                     marcarTXT('40');
                     marcarTXT('41');
                     marcarTXT('43');
                     marcarTXT('44');
                     marcarTXT('45');
                     marcarTXT('46');
                     marcarTXT('72');
                     marcarTXT('73');
                     marcarTXT('82');
                     marcarTXT('83');
                     marcarTXT('86');
                     marcarTXT('87');
                     marcarTXT('88');
                     marcarTXT('89');
                     marcarTXT('92');
                     marcarTXT('93');
                     marcarTXT('94');
                     marcarTXT('95');
                     marcarTXT('96');
                     marcarTXT('98');
                     marcarTXT('99');
                     marcarTXT('100');
                     marcarTXT('101');
                     marcarTXT('102');
                     marcarTXT('104');
                     marcarTXT('113');
                     marcarTXT('114');
                     
                    
                } 
                function PintaOPTs(){
                     marcar('1');
                     marcar('2'); 
                     marcar('33');
                     marcar('34');
                     marcar('36');
                     marcar('37');
                     marcar('39');
                     marcar('40');
                     marcar('41');
                     marcar('43');
                     marcar('44');
                     marcar('45');
                     marcar('46');
                     marcar('72');
                     marcar('73');
                     marcar('82');
                     marcar('83');
                     marcar('86');
                     marcar('87');
                     marcar('88');
                     marcar('89');
                     marcar('92');
                     marcar('93');
                     marcar('94');
                     marcar('95');
                     marcar('96');
                     marcar('98');
                     marcar('99');
                     marcar('100');
                     marcar('101');
                     marcar('102');
                     marcar('104');
                     marcar('113');
                     marcar('114');
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_optVANX'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtVANX'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtVANX'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_optVANX'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                 PintaOPTs();
                Disparador(<%=hidDisparador.Value %>);     
                function OpenPageCharged(Pagina){
                    Guion('97');
                    Guion('103');
                    Guion('35');
                    Guion('38');
                    openPage(Pagina);
                }
                function Guion(variable){
                    var txtv = document.getElementById('ctl00_cphMainMaster_txtVANX'+variable);
                    if (txtv != null){
                       if (txtv.value == "")
                          txtv.value = "_";  
                    }
                }
                
             
        </script> 

        </center> 
</asp:Content>
