<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911_3(Total)" AutoEventWireup="true" CodeBehind="AGT_911_3.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_3.AGT_911_3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
      
   <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">  
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="EDUCACI�N PRIMARIA"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div>
    </div>
    <div id="menu" style="min-width:1400px; width:expression(document.body.clientWidth < 1401? '1400px': 'auto' );">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_3',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_3',true)"><a href="#" title=""><span>1�, 2� y 3�</span></a></li>
        <li onclick="openPage('AG4_911_3',true)"><a href="#" title=""><span>4�, 5� y 6�</span></a></li>
        <li onclick="openPage('AGT_911_3',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_3',false)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>      
     <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center> 
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td style="width: 340px">
                    <table id="TABLE1" style="text-align: center" >
                        <tr>
                            <td colspan="16" style="text-align:center;">
                                                
                                                <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit" Font-Bold="True" 
                                                Text="Estad�stica de alumnos por grado, sexo, tipo de ingreso y edad"></asp:Label>
                                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1">
                            </td>
                            <td  colspan="1" rowspan="1">
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="">
                                </td>
                            <td colspan="" rowspan="" class="linaBajoAlto">
                                <asp:Label ID="lbl6_Menos" runat="server" CssClass="lblRojo" Text="Menos de 6 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="6 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl7" runat="server" CssClass="lblRojo" Text="7 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="8 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl15_Mas" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td class="linaBajoAlto Orila">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td class="Orila">
                                <asp:Label ID="lblGrupos1" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="5" class="linaBajoAlto">
                                <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td rowspan="2" class="linaBajoAlto">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblNvoIngresoHombres1" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV290" runat="server" Columns="4" TabIndex="10101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV291" runat="server" Columns="4" TabIndex="10102" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV292" runat="server" Columns="4" TabIndex="10103" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV293" runat="server" Columns="4" TabIndex="10104" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV294" runat="server" Columns="4" TabIndex="10105" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV295" runat="server" Columns="4" TabIndex="10106" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV296" runat="server" Columns="4" TabIndex="10107" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV297" runat="server" Columns="4" TabIndex="10108" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV298" runat="server" Columns="4" TabIndex="10109" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV299" runat="server" Columns="4" TabIndex="10110" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV300" runat="server" Columns="4" TabIndex="10111" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Alto">
                                <asp:TextBox ID="txtV301" runat="server" Columns="4" TabIndex="10112" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo">
                                <asp:Label ID="lblRepetidoresHombres1" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV2000" runat="server" BackColor="Silver" BorderColor="Silver" onkeydown="return Arrows(event,this.tabIndex)" 
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" TabIndex="10201" CssClass="lblNegro" ReadOnly="true"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV302" runat="server" Columns="4" TabIndex="10202" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV303" runat="server" Columns="4" TabIndex="10203" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                
                                <asp:TextBox ID="txtV304" runat="server" Columns="4" TabIndex="10204" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV305" runat="server" Columns="4" TabIndex="10205" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV306" runat="server" Columns="4" TabIndex="10206" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV307" runat="server" Columns="4" TabIndex="10207" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV308" runat="server" Columns="4" TabIndex="10208" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV309" runat="server" Columns="4" TabIndex="10209" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV310" runat="server" Columns="4" TabIndex="10210" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV311" runat="server" Columns="4" TabIndex="10211" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Alto">
                                <asp:TextBox ID="txtV312" runat="server" Columns="4" TabIndex="10212" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="linaBajo">
                            <asp:Label ID="lblMujeres" runat="server" CssClass="lblRojo" Text="MUJERES" Height="17px"></asp:Label></td>
                            <td class="linaBajo">
                            <asp:Label ID="lblNvoIngresoMujeres1" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV313" runat="server" Columns="4" TabIndex="10301" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV314" runat="server" Columns="4" TabIndex="10302" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV315" runat="server" Columns="4" TabIndex="10303" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV316" runat="server" Columns="4" TabIndex="10304" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV317" runat="server" Columns="4" TabIndex="10305" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV318" runat="server" Columns="4" TabIndex="10306" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV319" runat="server" Columns="4" TabIndex="10307" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV320" runat="server" Columns="4" TabIndex="10308" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV321" runat="server" Columns="4" TabIndex="10309" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV322" runat="server" Columns="4" TabIndex="10310" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV323" runat="server" Columns="4" TabIndex="10311" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Alto">
                                <asp:TextBox ID="txtV324" runat="server" Columns="4" TabIndex="10312" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" class="linaBajo">
                            <asp:Label ID="lblRepetidoresMujeres1" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV1000" runat="server" BackColor="Silver" BorderColor="Silver" onkeydown="return Arrows(event,this.tabIndex)" 
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" TabIndex="10401" CssClass="lblNegro" ReadOnly="true"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV325" runat="server" Columns="4" TabIndex="10402" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV326" runat="server" Columns="4" TabIndex="10403" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV327" runat="server" Columns="4" TabIndex="10404" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV328" runat="server" Columns="4" TabIndex="10405" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV329" runat="server" Columns="4" TabIndex="10406" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV330" runat="server" Columns="4" TabIndex="10407" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV331" runat="server" Columns="4" TabIndex="10408" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV332" runat="server" Columns="4" TabIndex="10409" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV333" runat="server" Columns="4" TabIndex="10410" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV334" runat="server" Columns="4" TabIndex="10411" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Alto">
                                <asp:TextBox ID="txtV335" runat="server" Columns="4" TabIndex="10412" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" class="linaBajo">
                            <asp:Label ID="lblTotal3" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td rowspan="1" class="linaBajo">
                                <asp:TextBox ID="txtV336" runat="server" Columns="4" TabIndex="10501" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV337" runat="server" Columns="4" TabIndex="10502" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV338" runat="server" Columns="4" TabIndex="10503" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV339" runat="server" Columns="4" TabIndex="10504" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV340" runat="server" Columns="4" TabIndex="10505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV341" runat="server" Columns="4" TabIndex="10506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV342" runat="server" Columns="4" TabIndex="10507" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV343" runat="server" Columns="4" TabIndex="10508" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV344" runat="server" Columns="4" TabIndex="10509" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV345" runat="server" Columns="4" TabIndex="10510" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV346" runat="server" Columns="4" TabIndex="10511" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo Alto">
                                <asp:TextBox ID="txtV347" runat="server" Columns="4" TabIndex="10512" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">
                                <asp:TextBox ID="txtV348" runat="server" Columns="2" TabIndex="10513" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG4_911_3',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG4_911_3',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                    &nbsp;
                </td>
                <td ><span  onclick="openPage('AGD_911_3',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_3',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado"></div>


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center>
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 14;
                MaxRow = 15;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
