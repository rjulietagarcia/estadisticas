<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AG1_911_3.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_3.AG1_911_31" Title="911.3(1�, 2� y 3�)" %>
   


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">     
   
    
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">  
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="EDUCACI�N PRIMARIA"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div>
    </div>
    <div id="menu" style="min-width:1400px; width:expression(document.body.clientWidth < 1401? '1400px': 'auto' );">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_3',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_3',true)"><a href="#" title="" class="activo"><span>1�, 2� y 3�</span></a></li>
        <li onclick="openPage('AG4_911_3',false)"><a href="#" title=""><span>4�, 5� y 6�</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>       
    <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
   


            <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>
            <center>
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                        <table>
                            <tr>
                                <td style="width: 340px">
                                    <table id="TABLE1" style="text-align: center" >
                                        
                                        <tr>
                                            <td colspan="16" style="text-align:justify;">
                                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblGrisTit" Font-Bold="True" 
                                                Text="I. ALUMNOS Y GRUPOS" Width="250px" Font-Size="16px"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="lblRojo">
                                            <td colspan="16" style="text-align:justify;">
                                                <asp:Label ID="Label28" runat="server" CssClass="lblRojo" 
                                                Text="
                                                    1.En esta p�gina y la siguiente, se requiere anotar el total de alumnos inscritos a partir de la fecha de inicio de cursos,
                                            sumando las altas y restando las bajas hasta el 30 de septiembre, desglos�ndolos por grado, sexo, nuevo ingreso, repetidores
                                            y edad. Verifique que la suma de los subtotales de alumnos por edad sea igual al total; escriba adem�s el n�mero de grupos 
                                            por grado.    
                                                ">
                                                </asp:Label>
                                            
                                         </td>
                                        </tr>
                                        <tr>
                                            <td colspan="16" style="text-align:center;">
                                                <br />
                                                <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit" Font-Bold="True" 
                                                Text="Estad�stica de alumnos por grado, sexo, tipo de ingreso y edad"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="16" rowspan="1" style="text-align: center">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" rowspan="" style="text-align: center; height: 3px;">
                                                </td>
                                            <td colspan="" rowspan="" style="width: 67px; text-align: center; height: 3px;" class="linaBajoAlto">
                                                <asp:Label ID="lbl6_Menos" runat="server" CssClass="lblRojo" Text="Menos de 6 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px;" class="linaBajoAlto">
                                                <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="6 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl7" runat="server" CssClass="lblRojo" Text="7 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="8 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="lbl15_Mas" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                                            <td style="width: 67px; height: 3px;" class="linaBajoAlto Orila">
                                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                                               
                                            <td style="width: 54px; height: 3px" class="Orila">
                                                <asp:Label ID="lblGrupos1" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="5" style="padding-right: 5px; width: 85px" class="linaBajoAlto">
                                                <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1�"></asp:Label></td>
                                            <td style="width: 85px; padding-right: 5px;" rowspan="2" class="linaBajoAlto">
                                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                                            <td style="width: 120px; height: 26px; text-align: left" class="linaBajoAlto">
                                                <asp:Label ID="lblNvoIngresoHombres1" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                                            <td style="width: 67px; height: 26px;" class="linaBajo">
                                                <asp:TextBox ID="txtV1" runat="server" Columns="4" TabIndex="10101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px;" class="linaBajo">
                                                <asp:TextBox ID="txtV2" runat="server" Columns="4" TabIndex="10102" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV3" runat="server" Columns="4" TabIndex="10103" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV4" runat="server" Columns="4" TabIndex="10104" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV5" runat="server" Columns="4" TabIndex="10105" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV6" runat="server" Columns="4" TabIndex="10106" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV7" runat="server" Columns="4" TabIndex="10107" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV8" runat="server" Columns="4" TabIndex="10108" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV9" runat="server" Columns="4" TabIndex="10109" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV10" runat="server" Columns="4" TabIndex="10110" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV11" runat="server" Columns="4" TabIndex="10111" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px;" class="linaBajo Orila">
                                                <asp:TextBox ID="txtV12" runat="server" Columns="4" TabIndex="10112" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 120px; height: 26px; text-align: left" class="linaBajo">
                                                <asp:Label ID="lblRepetidoresHombres1" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                                            <td style="width: 67px; height: 26px;" class="linaBajo">
                                                <asp:TextBox ID="txtVBlank7" runat="server" BackColor="Silver" BorderColor="Silver" onkeydown="return Arrows(event,this.tabIndex)"
                                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" TabIndex="10201" CssClass="lblNegro"  ReadOnly="true"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px;" class="linaBajo">
                                                <asp:TextBox ID="txtV13" runat="server" Columns="4" TabIndex="10202" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV14" runat="server" Columns="4" TabIndex="10203" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV15" runat="server" Columns="4" TabIndex="10204" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV16" runat="server" Columns="4" TabIndex="10205" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV17" runat="server" Columns="4" TabIndex="10206" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV18" runat="server" Columns="4" TabIndex="10207" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV19" runat="server" Columns="4" TabIndex="10208" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV20" runat="server" Columns="4" TabIndex="10209" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV21" runat="server" Columns="4" TabIndex="10210" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV22" runat="server" Columns="4" TabIndex="10211" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px;" class="linaBajo Orila">
                                                <asp:TextBox ID="txtV23" runat="server" Columns="4" TabIndex="10212" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="width: 85px; margin-right: 5px; text-align: left;" class="linaBajo">
                                            <asp:Label ID="lblMujeres" runat="server" CssClass="lblRojo" Text="MUJERES" Height="17px"></asp:Label></td>
                                            <td style="width: 120px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblNvoIngresoMujeres1" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"></asp:Label></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV24" runat="server" Columns="4" TabIndex="10301" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV25" runat="server" Columns="4" TabIndex="10302" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV26" runat="server" Columns="4" TabIndex="10303" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV27" runat="server" Columns="4" TabIndex="10304" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV28" runat="server" Columns="4" TabIndex="10305" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV29" runat="server" Columns="4" TabIndex="10306" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV30" runat="server" Columns="4" TabIndex="10307" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV31" runat="server" Columns="4" TabIndex="10308" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV32" runat="server" Columns="4" TabIndex="10309" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV33" runat="server" Columns="4" TabIndex="10310" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV34" runat="server" Columns="4" TabIndex="10311" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                                <asp:TextBox ID="txtV35" runat="server" Columns="4" TabIndex="10312" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left;" class="linaBajo">
                                            <asp:Label ID="lblRepetidoresMujeres1" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                                &nbsp;<asp:TextBox ID="txtVBlank6" runat="server" BackColor="Silver" BorderColor="Silver" onkeydown="return Arrows(event,this.tabIndex)"
                                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" TabIndex="10401" CssClass="lblNegro"  ReadOnly="true"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV36" runat="server" Columns="4" TabIndex="10402" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV37" runat="server" Columns="4" TabIndex="10403" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV38" runat="server" Columns="4" TabIndex="10404" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV39" runat="server" Columns="4" TabIndex="10405" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV40" runat="server" Columns="4" TabIndex="10406" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV41" runat="server" Columns="4" TabIndex="10407" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV42" runat="server" Columns="4" TabIndex="10408" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV43" runat="server" Columns="4" TabIndex="10409" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV44" runat="server" Columns="4" TabIndex="10410" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV45" runat="server" Columns="4" TabIndex="10411" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                                <asp:TextBox ID="txtV46" runat="server" Columns="4" TabIndex="10412" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblSubtotal1" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV47" runat="server" Columns="4" TabIndex="10501" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                                <asp:TextBox ID="txtV48" runat="server" Columns="4" TabIndex="10502" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV49" runat="server" Columns="4" TabIndex="10503" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV50" runat="server" Columns="4" TabIndex="10504" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV51" runat="server" Columns="4" TabIndex="10505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV52" runat="server" Columns="4" TabIndex="10506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV53" runat="server" Columns="4" TabIndex="10507" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV54" runat="server" Columns="4" TabIndex="10508" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV55" runat="server" Columns="4" TabIndex="10509" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV56" runat="server" Columns="4" TabIndex="10510" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                                <asp:TextBox ID="txtV57" runat="server" Columns="4" TabIndex="10511" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                                <asp:TextBox ID="txtV58" runat="server" Columns="4" TabIndex="10512" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 54px; height: 26px" class="Orila">
                                                <asp:TextBox ID="txtV59" runat="server" Columns="2" TabIndex="10513" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                                height: 3px; text-align: center">
                                            </td>
                                            <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                            </td>
                                            <td rowspan="1" style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px; text-align: left">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 54px; height: 26px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" rowspan="" style="text-align: center; height: 3px;">
                                                </td>
                                            <td colspan="" rowspan="" style="width: 67px; text-align: center; height: 3px;" class="linaBajoAlto">
                                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="Menos de 6 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px;" class="linaBajoAlto">
                                                <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Text="6 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Text="7 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Text="8 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                                            <td style="width: 67px; height: 3px;" class="linaBajoAlto Orila">
                                                <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                                            <td style="width: 54px; height: 3px" class="Orila">
                                                <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="5" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                                height: 3px; text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2�"></asp:Label></td>
                                            <td rowspan="2" style="width: 40px; height: 26px; text-align: left" class="linaBajoAlto">
                                            <asp:Label ID="lblHombres2" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajoAlto">
                                            <asp:Label ID="lblNvoIngresoHombres2" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                                            <td style="width: 67px; height: 26px;" class="linaBajo">
                                            <asp:TextBox ID="txtBlank1" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV60" runat="server" Columns="4" TabIndex="20101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV61" runat="server" Columns="4" TabIndex="20102" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV62" runat="server" Columns="4" TabIndex="20103" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV63" runat="server" Columns="4" TabIndex="20104" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV64" runat="server" Columns="4" TabIndex="20105" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV65" runat="server" Columns="4" TabIndex="20106" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV66" runat="server" Columns="4" TabIndex="20107" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV67" runat="server" Columns="4" TabIndex="20108" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV68" runat="server" Columns="4" TabIndex="20109" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV69" runat="server" Columns="4" TabIndex="20110" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 54px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV70" runat="server" Columns="4" TabIndex="20111" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="1" style="width: 50px; height: 3px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblRepetidoresHombres2" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px;" class="linaBajo">
                                            <asp:TextBox ID="txtBlank2" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV71" runat="server" Columns="4" TabIndex="20201" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV72" runat="server" Columns="4" TabIndex="20202" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV73" runat="server" Columns="4" TabIndex="20203" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV74" runat="server" Columns="4" TabIndex="20204" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV75" runat="server" Columns="4" TabIndex="20205" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV76" runat="server" Columns="4" TabIndex="20206" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV77" runat="server" Columns="4" TabIndex="20207" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV78" runat="server" Columns="4" TabIndex="20208" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV79" runat="server" Columns="4" TabIndex="20209" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV80" runat="server" Columns="4" TabIndex="20210" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV81" runat="server" Columns="4" TabIndex="20211" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="2" style="width: 50px; height: 3px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblMujeres2" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                                            <td rowspan="1" style="width: 40px; height: 26px;" class="linaBajo">
                                            <asp:Label ID="lblNvoIngresoMujeres2" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"
                                                Width="100px"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtBlank3" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV82" runat="server" Columns="4" TabIndex="20301" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV83" runat="server" Columns="4" TabIndex="20302" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV84" runat="server" Columns="4" TabIndex="20303" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV85" runat="server" Columns="4" TabIndex="20304" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV86" runat="server" Columns="4" TabIndex="20305" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV87" runat="server" Columns="4" TabIndex="20306" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV88" runat="server" Columns="4" TabIndex="20307" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV89" runat="server" Columns="4" TabIndex="20308" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV90" runat="server" Columns="4" TabIndex="20309" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV91" runat="server" Columns="4" TabIndex="20310" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV92" runat="server" Columns="4" TabIndex="20311" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="1" style="width: 50px; height: 3px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblRepetidoresMujeres2" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px; " class="linaBajo">
                                            <asp:TextBox ID="txtBlank4" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV93" runat="server" Columns="4" TabIndex="20401" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV94" runat="server" Columns="4" TabIndex="20402" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV95" runat="server" Columns="4" TabIndex="20403" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV96" runat="server" Columns="4" TabIndex="20404" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV97" runat="server" Columns="4" TabIndex="20405" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV98" runat="server" Columns="4" TabIndex="20406" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV99" runat="server" Columns="4" TabIndex="20407" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV100" runat="server" Columns="4" TabIndex="20408" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV101" runat="server" Columns="4" TabIndex="20409" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV102" runat="server" Columns="4" TabIndex="20410" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV103" runat="server" Columns="4" TabIndex="20411" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="1" style="width: 50px; height: 3px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblSubtotal2" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px; " class="linaBajo">
                                            <asp:TextBox ID="txtBlank5" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV104" runat="server" Columns="4" TabIndex="20501" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV105" runat="server" Columns="4" TabIndex="20502" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV106" runat="server" Columns="4" TabIndex="20503" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV107" runat="server" Columns="4" TabIndex="20504" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV108" runat="server" Columns="4" TabIndex="20505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV109" runat="server" Columns="4" TabIndex="20506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV110" runat="server" Columns="4" TabIndex="20507" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV111" runat="server" Columns="4" TabIndex="20508" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV112" runat="server" Columns="4" TabIndex="20509" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV113" runat="server" Columns="4" TabIndex="20510" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV114" runat="server" Columns="4" TabIndex="20511" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="Orila">
                                            <asp:TextBox ID="txtV115" runat="server" Columns="2" TabIndex="20512" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                                height: 3px; text-align: center">
                                            </td>
                                            <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                            </td>
                                            <td rowspan="1" style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px; text-align: left">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td style="width: 67px; height: 26px">
                                            </td>
                                            <td >&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" rowspan="" style="text-align: center; height: 3px;">
                                                </td>
                                            <td colspan="" rowspan="" style="width: 67px; text-align: center; height: 3px;" class="linaBajoAlto">
                                                <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Text="Menos de 6 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px;" class="linaBajoAlto">
                                                <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Text="6 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Text="7 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Text="8 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label22" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label23" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                                            <td style="width: 67px; height: 3px" class="linaBajoAlto">
                                                <asp:Label ID="Label24" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                                            <td style="width: 67px; height: 3px;" class="linaBajoAlto Orila">
                                                <asp:Label ID="Label25" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                                            <td style="width: 54px; height: 3px" class="Orila">
                                                <asp:Label ID="Label26" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="5" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                                height: 3px; text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3�"></asp:Label></td>
                                            <td rowspan="2" style="width: 67px; height: 26px;" class="linaBajoAlto">
                                            <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Height="17px" Text="HOMBRES"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajoAlto">
                                            <asp:Label ID="lblNvoIngresoHombres3" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"
                                                Width="100px"></asp:Label></td>
                                            <td style="width: 67px; height: 26px; " class="linaBajo">
                                            <asp:TextBox ID="txtBlank8" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtBlank13" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV116" runat="server" Columns="4" TabIndex="30101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV117" runat="server" Columns="4" TabIndex="30102" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV118" runat="server" Columns="4" TabIndex="30103" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV119" runat="server" Columns="4" TabIndex="30104" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV120" runat="server" Columns="4" TabIndex="30105" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV121" runat="server" Columns="4" TabIndex="30106" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV122" runat="server" Columns="4" TabIndex="30107" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV123" runat="server" Columns="4" TabIndex="30108" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV124" runat="server" Columns="4" TabIndex="30109" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 54px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV125" runat="server" Columns="4" TabIndex="30110" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="1" style="width: 50px; height: 3px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblRepetidoresHombres3" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px;" class="linaBajo">
                                            <asp:TextBox ID="txtBlank9" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtBlank14" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV126" runat="server" Columns="4" TabIndex="30201" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV127" runat="server" Columns="4" TabIndex="30202" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV128" runat="server" Columns="4" TabIndex="30203" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV129" runat="server" Columns="4" TabIndex="30204" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV130" runat="server" Columns="4" TabIndex="30205" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV131" runat="server" Columns="4" TabIndex="30206" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV132" runat="server" Columns="4" TabIndex="30207" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV133" runat="server" Columns="4" TabIndex="30208" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV134" runat="server" Columns="4" TabIndex="30209" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV135" runat="server" Columns="4" TabIndex="30210" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="2" style="width: 50px; height: 3px; text-align: center" class="linaBajo">
                                            <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px;" class="linaBajo">
                                            <asp:Label ID="lblNvoIngresoMujeres3" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"
                                                Width="100px"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtBlank10" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px; " class="linaBajo">
                                            <asp:TextBox ID="txtBlank15" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV136" runat="server" Columns="4" TabIndex="30301" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV137" runat="server" Columns="4" TabIndex="30302" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV138" runat="server" Columns="4" TabIndex="30303" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV139" runat="server" Columns="4" TabIndex="30304" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV140" runat="server" Columns="4" TabIndex="30305" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV141" runat="server" Columns="4" TabIndex="30306" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV142" runat="server" Columns="4" TabIndex="30307" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV143" runat="server" Columns="4" TabIndex="30308" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV144" runat="server" Columns="4" TabIndex="30309" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV145" runat="server" Columns="4" TabIndex="30310" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" rowspan="1" style="width: 50px; height: 3px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblRepetidoresMujeres3" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px; " class="linaBajo">
                                            <asp:TextBox ID="txtBlank11" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtBlank16" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV146" runat="server" Columns="4" TabIndex="30401" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV147" runat="server" Columns="4" TabIndex="30402" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV148" runat="server" Columns="4" TabIndex="30403" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV149" runat="server" Columns="4" TabIndex="30404" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV150" runat="server" Columns="4" TabIndex="30405" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV151" runat="server" Columns="4" TabIndex="30406" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV152" runat="server" Columns="4" TabIndex="30407" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV153" runat="server" Columns="4" TabIndex="30408" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV154" runat="server" Columns="4" TabIndex="30409" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV155" runat="server" Columns="4" TabIndex="30410" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblSubtotal3" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                                            <td rowspan="1" style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtBlank12" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px;" class="linaBajo">
                                            <asp:TextBox ID="txtBlank17" runat="server" BackColor="Silver" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV156" runat="server" Columns="4" TabIndex="30501" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV157" runat="server" Columns="4" TabIndex="30502" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV158" runat="server" Columns="4" TabIndex="30503" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV159" runat="server" Columns="4" TabIndex="30504" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV160" runat="server" Columns="4" TabIndex="30505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV161" runat="server" Columns="4" TabIndex="30506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV162" runat="server" Columns="4" TabIndex="30507" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV163" runat="server" Columns="4" TabIndex="30508" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo">
                                            <asp:TextBox ID="txtV164" runat="server" Columns="4" TabIndex="30509" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 67px; height: 26px" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV165" runat="server" Columns="4" TabIndex="30510" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                            <td style="width: 54px; height: 26px" class="Orila">
                                            <asp:TextBox ID="txtV166" runat="server" Columns="2" TabIndex="30511" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        
                        <hr />
                        <table align="center">
                            <tr>
                                <td ><span  onclick="openPage('Identificacion_911_3',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                                <td ><span  onclick="openPage('Identificacion_911_3',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                                <td style="width: 330px;">
                                    &nbsp;
                                </td>
                                <td ><span  onclick="openPage('AG4_911_3',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                                <td ><span  onclick="openPage('AG4_911_3',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                            </tr>
                        </table>
                        
                        <div class="divResultado" id="divResultado"  ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
                &nbsp; &nbsp; &nbsp;
        <input type="Hidden" id="hdnIdCct" runat="server" value=""  /><input type="Hidden" id="hdnInm" runat="server" value=""  /><input type="Hidden" id="hdnCct" runat="server" value=""  /><input type="Hidden" id="hdnTur" runat="server" value=""  /><input id="hidIdCtrl" type="hidden" runat= "server" /><input id="hidListaTxtBoxs" type="hidden" runat = "server" /><input id="hidDisparador" type="hidden" runat="server" value="90" /><br />
          <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;background:#000;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
           </center>
        <script type="text/javascript" language="javascript">
                MaxCol = 14;
                MaxRow = 15;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		        Disparador(<%=hidDisparador.Value %>);
        </script> 
</asp:Content>
