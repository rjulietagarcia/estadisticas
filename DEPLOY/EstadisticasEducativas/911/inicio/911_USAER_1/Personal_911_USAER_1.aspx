<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-1(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_USAER_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_USAER_1.Personal_911_USAER_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1380px; height:65px;">
    <div id="header">
        
    <table style="width:100%">
       <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
        
    </div></div>
    <div id="menu" style="min-width:1380px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_1',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_1',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_1',true)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_1',true)"><a href="#" title=""><span>SECUNDARIA</span></a></li>
        <li onclick="openPage('AGD_911_USAER_1',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_USAER_1',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="openPage('CM_911_USAER_1',false)"><a href="#" title=""><span>CARRERA MAGISTERIAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%> 


        <table>
            <tr>
                <td>
                    <table id="TABLE1" style="text-align: center;">
                        <tr>
                            <td colspan="12" rowspan="1" style="vertical-align: top; text-align: left">
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblRojo" Font-Size="16px" Text="PERSONAL POR FUNCI�N"
                                    Width="480px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="vertical-align: top; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="vertical-align: top; text-align: left">
                                <asp:Label ID="lblDirector" runat="server" CssClass="lblRojo" Font-Size="14px" Text="DIRECTOR"
                                    Width="480px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="vertical-align: top; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left; vertical-align: top;">
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Seleccione la situaci�n del director."
                                    Width="270px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left">
                            </td>
                            <td colspan="4" rowspan="1" style="width: 270px;
                                text-align: left">
                                <asp:Label ID="lblInstrucciones2" runat="server" CssClass="lblRojo" Text="2. Escriba la clave y el nombre del nivel m�ximo de estudios y de su especialidad del director."
                                    Width="350px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="padding-right: 10px; padding-left: 10px; width: 270px;
                                text-align: left">
                            </td>
                            <td colspan="3" rowspan="1" style="width: 270px;
                                height: 26px; text-align: left">
                                <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="4. Seleccione la situaci�n acad�mica del director."
                                    Width="250px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lblDirCGpo" runat="server" CssClass="lblGrisTit" Text="CON GRUPO" Width="90px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lblDirSGpo" runat="server" CssClass="lblGrisTit" Text="SIN GRUPO" Width="90px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 89px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="optV1213" runat="server" GroupName="ConSinGpo"   Text=" " />
                                <asp:TextBox ID="txtV1213" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                            </td>
                            <td colspan="1" rowspan="1" style="width: 90px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="optV1214" runat="server" GroupName="ConSinGpo"  Text=" " />
                                <asp:TextBox ID="txtV1214" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                            </td>
                            <td colspan="1" rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                                <asp:Label ID="lblDirNivel" runat="server" CssClass="lblGrisTit" Text="NIVEL" Width="67px"></asp:Label></td>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1215" runat="server"  Enabled="false" Columns="2" MaxLength="2">
                                </asp:TextBox><asp:TextBox ID="txtV1216" runat="server"  MaxLength="30" style="visibility:hidden; width:1px;">
                                </asp:TextBox></td>
                            <td colspan="2" rowspan="1" style="width: 180px; text-align: left">
                                <asp:DropDownList ID="ddl_Nivel" runat="server" onchange="OnChange_Nivel(this)"  Width="180px">
                                </asp:DropDownList></td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 180px; text-align: left">
                                <asp:Label ID="lblDirPosgrado" runat="server" CssClass="lblGrisTit" Text="POSGRADO" Width="180px"></asp:Label></td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="optV1222" runat="server" GroupName="SituacionAcadDir"  Text=" " />
                                <asp:TextBox ID="txtV1222" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblInstrucciones3" runat="server" CssClass="lblRojo" Text="3. Indique el sexo del director."
                                    Width="270px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="padding-right: 10px; padding-left: 10px; width: 270px;
                                text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblDirEspecialidad" runat="server" CssClass="lblGrisTit" Text="ESPECIALIDAD" Width="90px"></asp:Label></td>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1217" runat="server"  Enabled="false" Columns="2" MaxLength="2">
                                </asp:TextBox><asp:TextBox ID="txtV1218" runat="server"  MaxLength="30" style="visibility:hidden; width:1px;">
                                </asp:TextBox></td>
                            <td colspan="2" rowspan="1" style="width: 180px; text-align: left">
                                <asp:DropDownList ID="ddl_Especialidad" runat="server" onchange="OnChange_Especialidad(this)"  Width="180px">
                                </asp:DropDownList></td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 180px; text-align: left">
                                <asp:Label ID="lblDirLicTitulado" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO" Width="180px"></asp:Label></td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="optV1223" runat="server" GroupName="SituacionAcadDir"  Text=" " />
                                <asp:TextBox ID="txtV1223" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lblDirHombre" runat="server" CssClass="lblGrisTit" Text="HOMBRE" Width="67px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="width: 90px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lblDirMujer" runat="server" CssClass="lblGrisTit" Text="MUJER" Width="67px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                                <asp:Label ID="lblDirOtro" runat="server" CssClass="lblGrisTit" Text="OTRA" Width="67px"></asp:Label></td>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                </td>
                            <td colspan="2" rowspan="1" style="width: 180px; text-align: left">
                                <asp:TextBox ID="txtV1219" runat="server" Columns="25"  CssClass="lblNegro"></asp:TextBox></td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 180px; text-align: left">
                                <asp:Label ID="lblDirLicNoTitulado" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, NO TITULADO"
                                    Width="180px"></asp:Label></td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="optV1224" runat="server" GroupName="SituacionAcadDir"  Text=" " />
                                <asp:TextBox ID="txtV1224" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="optV1220" runat="server" GroupName="SexoDir"  Text=" " />
                                <asp:TextBox ID="txtV1220" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                            </td>
                            <td colspan="1" rowspan="1" style="width: 90px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="optV1221" runat="server" GroupName="SexoDir"  Text=" " />
                                <asp:TextBox ID="txtV1221" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                            </td>
                            <td colspan="1" rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 180px; text-align: left">
                                <asp:Label ID="lblDirLicEstudiante" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, ESTUDIANTE"
                                    Width="180px"></asp:Label></td>
                            <td rowspan="1" style="width: 90px; text-align: left">
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="optV1225" runat="server" GroupName="SituacionAcadDir"  Text=" " />
                                <asp:TextBox ID="txtV1225" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblMtrosApoyo" runat="server" CssClass="lblRojo" Font-Size="14px" Text="MAESTROS DE APOYO"
                                    Width="480px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="lblInstrucciones5" runat="server" CssClass="lblRojo" Text="5. Escriba la cantidad de maestros de apoyo, por formaci�n, seg�n su situaci�n acad�mica y sexo."
                                    Width="600px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center">
                            </td>
                            <td colspan="6" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblLicenciatura5" runat="server" CssClass="lblRojo" Text="LICENCIATURA" Width="480px"></asp:Label></td>
                            <td colspan="4" rowspan="1" style="text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblFormacion5" runat="server" CssClass="lblRojo" Text="FORMACI�N" Width="180px"></asp:Label></td>
                            <td colspan="2" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblTitulados5" runat="server" CssClass="lblRojo" Text="TITULADOS" Width="180px"></asp:Label></td>
                            <td colspan="2" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblNoTitulados5" runat="server" CssClass="lblRojo" Text="NO TITULADOS" Width="180px"></asp:Label></td>
                            <td colspan="2" rowspan="1" style="text-align: center">
                                <asp:Label ID="blEstudiantes" runat="server" CssClass="lblRojo" Text="ESTUDIANTES" Width="180px"></asp:Label></td>
                            <td colspan="2" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblTotal51" runat="server" CssClass="lblRojo" Text="TOTAL" Width="180px"></asp:Label></td>
                            <td colspan="2" rowspan="1" style="text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center; height: 26px; width: 90px;">
                                </td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                </td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblHombresTit5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblMujeresTit5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                &nbsp;<asp:Label ID="lblHombresNTit5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; text-align: center">
                                <asp:Label ID="lblMujeresNTit5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresEst5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td colspan="" style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresEst5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td colspan="" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresTot5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td colspan="" style="padding-right: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresTot5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td colspan="" style="padding-left: 10px; width: 89px; height: 26px; text-align: center">
                                </td>
                            <td colspan="" style="width: 90px; height: 26px; text-align: center">
                                </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 90px; height: 26px;" colspan="2">
                                <asp:Label ID="lblDefMental" runat="server" CssClass="lblGrisTit" Text="EN DEFICIENCIA MENTAL" Width="180px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV1226" runat="server" Columns="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV1227" runat="server" Columns="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV1228" runat="server" Columns="2" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV1229" runat="server" Columns="2" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1230" runat="server" Columns="2" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV1231" runat="server" Columns="2" TabIndex="10106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1232" runat="server" Columns="2" TabIndex="10107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1233" runat="server" Columns="2" TabIndex="10108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblAudicion" runat="server" CssClass="lblGrisTit" Text="EN AUDICI�N Y LENGUAJE"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1234" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1235" runat="server" Columns="2" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1236" runat="server" Columns="2" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1237" runat="server" Columns="2" TabIndex="10204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1238" runat="server" Columns="2" TabIndex="10205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1239" runat="server" Columns="2" TabIndex="10206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1240" runat="server" Columns="2" TabIndex="10207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1241" runat="server" Columns="2" TabIndex="10208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblAprendizaje" runat="server" CssClass="lblGrisTit" Text="EN APRENDIZAJE"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1242" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1243" runat="server" Columns="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1244" runat="server" Columns="2" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1245" runat="server" Columns="2" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1246" runat="server" Columns="2" TabIndex="10305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1247" runat="server" Columns="2" TabIndex="10306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1248" runat="server" Columns="2" TabIndex="10307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1249" runat="server" Columns="2" TabIndex="10308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblDefVisual" runat="server" CssClass="lblGrisTit" Text="EN DEFICIENCIA VISUAL"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1250" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1251" runat="server" Columns="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1252" runat="server" Columns="2" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1253" runat="server" Columns="2" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1254" runat="server" Columns="2" TabIndex="10405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1255" runat="server" Columns="2" TabIndex="10406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1256" runat="server" Columns="2" TabIndex="10407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1257" runat="server" Columns="2" TabIndex="10408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblLocomotor" runat="server" CssClass="lblGrisTit" Text="EN APARATO LOCOMOTOR"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1258" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1259" runat="server" Columns="2" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1260" runat="server" Columns="2" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1261" runat="server" Columns="2" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1262" runat="server" Columns="2" TabIndex="10505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1263" runat="server" Columns="2" TabIndex="10506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1264" runat="server" Columns="2" TabIndex="10507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1265" runat="server" Columns="2" TabIndex="10508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblInadSociales" runat="server" CssClass="lblGrisTit" Text="EN INADAPTADOS SOCIALES"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1266" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1267" runat="server" Columns="2" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1268" runat="server" Columns="2" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1269" runat="server" Columns="2" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1270" runat="server" Columns="2" TabIndex="10605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1271" runat="server" Columns="2" TabIndex="10606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1272" runat="server" Columns="2" TabIndex="10607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1273" runat="server" Columns="2" TabIndex="10608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblPedagogo" runat="server" CssClass="lblGrisTit" Text="PEDAGOGO" Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1274" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1275" runat="server" Columns="2" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1276" runat="server" Columns="2" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1277" runat="server" Columns="2" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1278" runat="server" Columns="2" TabIndex="10705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1279" runat="server" Columns="2" TabIndex="10706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1280" runat="server" Columns="2" TabIndex="10707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1281" runat="server" Columns="2" TabIndex="10708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblPsicologo" runat="server" CssClass="lblGrisTit" Text="PSIC�LOGO" Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1282" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1283" runat="server" Columns="2" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1284" runat="server" Columns="2" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1285" runat="server" Columns="2" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1286" runat="server" Columns="2" TabIndex="10805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1287" runat="server" Columns="2" TabIndex="10806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1288" runat="server" Columns="2" TabIndex="10807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1289" runat="server" Columns="2" TabIndex="10808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblMaestroPre" runat="server" CssClass="lblGrisTit" Text="MAESTRO DE PREESCOLAR"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1290" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1291" runat="server" Columns="2" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1292" runat="server" Columns="2" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1293" runat="server" Columns="2" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1294" runat="server" Columns="2" TabIndex="10905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1295" runat="server" Columns="2" TabIndex="10906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1296" runat="server" Columns="2" TabIndex="10907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1297" runat="server" Columns="2" TabIndex="10908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblMaestroPri" runat="server" CssClass="lblGrisTit" Text="MAESTRO DE PRIMARIA"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1298" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1299" runat="server" Columns="2" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1300" runat="server" Columns="2" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1301" runat="server" Columns="2" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1302" runat="server" Columns="2" TabIndex="11005" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1303" runat="server" Columns="2" TabIndex="11006" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1304" runat="server" Columns="2" TabIndex="11007" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1305" runat="server" Columns="2" TabIndex="11008" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblMaestroSec" runat="server" CssClass="lblGrisTit" Text="MAESTRO DE SECUNDARIA"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1306" runat="server" Columns="2" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1307" runat="server" Columns="2" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1308" runat="server" Columns="2" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1309" runat="server" Columns="2" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1310" runat="server" Columns="2" TabIndex="11105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1311" runat="server" Columns="2" TabIndex="11106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1312" runat="server" Columns="2" TabIndex="11107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1313" runat="server" Columns="2" TabIndex="11108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblIntEducativa" runat="server" CssClass="lblGrisTit" Text="EN INTEGRACI�N EDUCATIVA"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1314" runat="server" Columns="2" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1315" runat="server" Columns="2" TabIndex="11202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1316" runat="server" Columns="2" TabIndex="11203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1317" runat="server" Columns="2" TabIndex="11204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1318" runat="server" Columns="2" TabIndex="11205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1319" runat="server" Columns="2" TabIndex="11206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1320" runat="server" Columns="2" TabIndex="11207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1321" runat="server" Columns="2" TabIndex="11208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblNota5" runat="server" CssClass="lblRojo" Text="Si existen maestros de apoyo con formaci�n diferente de las especificadas, escriba la cantidad de personal de acuerdo con su situaci�n acad�mica y sexo."
                                    Width="900px"></asp:Label></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV1322" runat="server" Columns="25" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1323" runat="server" Columns="2" TabIndex="11302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1324" runat="server" Columns="2" TabIndex="11303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1325" runat="server" Columns="2" TabIndex="11304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1326" runat="server" Columns="2" TabIndex="11305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1327" runat="server" Columns="2" TabIndex="11306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1328" runat="server" Columns="2" TabIndex="11307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1329" runat="server" Columns="2" TabIndex="11308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1330" runat="server" Columns="2" TabIndex="11309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV1331" runat="server" Columns="25" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1332" runat="server" Columns="2" TabIndex="11402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1333" runat="server" Columns="2" TabIndex="11403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1334" runat="server" Columns="2" TabIndex="11404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1335" runat="server" Columns="2" TabIndex="11405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1336" runat="server" Columns="2" TabIndex="11406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1337" runat="server" Columns="2" TabIndex="11407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1338" runat="server" Columns="2" TabIndex="11408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1339" runat="server" Columns="2" TabIndex="11409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV1340" runat="server" Columns="25" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1341" runat="server" Columns="2" TabIndex="11502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1342" runat="server" Columns="2" TabIndex="11503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1343" runat="server" Columns="2" TabIndex="11504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1344" runat="server" Columns="2" TabIndex="11505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1345" runat="server" Columns="2" TabIndex="11506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1346" runat="server" Columns="2" TabIndex="11507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1347" runat="server" Columns="2" TabIndex="11508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1348" runat="server" Columns="2" TabIndex="11509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblTotal52" runat="server" CssClass="lblRojo" Text="TOTAL" Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1349" runat="server" Columns="2" TabIndex="11601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1350" runat="server" Columns="2" TabIndex="11602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1351" runat="server" Columns="2" TabIndex="11603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1352" runat="server" Columns="2" TabIndex="11604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1353" runat="server" Columns="2" TabIndex="11605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1354" runat="server" Columns="2" TabIndex="11606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1355" runat="server" Columns="3" TabIndex="11607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1356" runat="server" Columns="3" TabIndex="11608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblPersParadocente" runat="server" CssClass="lblRojo" Font-Size="14px" Text="PERSONAL PARADOCENTE"
                                    Width="480px"></asp:Label></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" style="padding-bottom: 10px; width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblInstrucciones6" runat="server" CssClass="lblRojo" Text="6. Escriba la cantidad de personal paradocente, por formaci�n, seg�n su situaci�n acad�mica y sexo."
                                    Width="800px"></asp:Label></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td colspan="6" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblLicenciatura6" runat="server" CssClass="lblRojo" Text="LICENCIATURA" Width="480px"></asp:Label></td>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblFormacion6" runat="server" CssClass="lblRojo" Text="FORMACI�N" Width="180px"></asp:Label></td>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblTitulados6" runat="server" CssClass="lblRojo" Text="TITULADOS" Width="180px"></asp:Label></td>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblNoTitulados6" runat="server" CssClass="lblRojo" Text="NO TITULADOS" Width="180px"></asp:Label></td>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEstudiantes6" runat="server" CssClass="lblRojo" Text="ESTUDIANTES" Width="180px"></asp:Label></td>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal6" runat="server" CssClass="lblRojo" Text="TOTAL" Width="180px"></asp:Label></td>
                            <td style="width: 89px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresTit6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresTit6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresNTit6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresNTit6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; text-align: center">
                                <asp:Label ID="lblHombresEst6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresEst6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresTot6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresTot6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 180px; text-align: left; height: 26px;">
                                <asp:Label ID="lblPsiologo6" runat="server" CssClass="lblGrisTit" Text="PSIC�LOGO"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1357" runat="server" Columns="3" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1358" runat="server" Columns="3" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1359" runat="server" Columns="3" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1360" runat="server" Columns="3" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1361" runat="server" Columns="3" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1362" runat="server" Columns="3" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1363" runat="server" Columns="3" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1364" runat="server" Columns="3" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 180px; height: 26px; text-align: left">
                                <asp:Label ID="lblTrabSocial6" runat="server" CssClass="lblGrisTit" Text="TRABAJADOR SOCIAL"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1365" runat="server" Columns="3" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1366" runat="server" Columns="3" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1367" runat="server" Columns="3" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1368" runat="server" Columns="3" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1369" runat="server" Columns="3" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1370" runat="server" Columns="3" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1371" runat="server" Columns="3" TabIndex="20207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1372" runat="server" Columns="3" TabIndex="20208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 180px; height: 26px; text-align: left">
                                <asp:Label ID="lblMaestroEsp6" runat="server" CssClass="lblGrisTit" Text="MAESTRO ESPECIALISTA"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1373" runat="server" Columns="3" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1374" runat="server" Columns="3" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1375" runat="server" Columns="3" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1376" runat="server" Columns="3" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1377" runat="server" Columns="3" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1378" runat="server" Columns="3" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1379" runat="server" Columns="3" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1380" runat="server" Columns="3" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="width: 270px; height: 26px; text-align: left">
                                <asp:Label ID="lblEspecialidad6" runat="server" CssClass="lblGrisTit" Text="ESCRIBA LA ESPECIALIDAD DEL MAESTRO"
                                    Width="270px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: left" colspan="3">
                                <asp:TextBox ID="txtV1381" runat="server" Columns="35" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblNota6" runat="server" CssClass="lblRojo" Text="Si existe personal paradocente con formaci�n diferente a las especificadas, escriba la cantidad de personal de acuerdo a su situaci�n acad�mica y sexo."
                                    Width="900px"></asp:Label></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV1382" runat="server" Columns="25" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1383" runat="server" Columns="3" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1384" runat="server" Columns="3" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1385" runat="server" Columns="3" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1386" runat="server" Columns="3" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1387" runat="server" Columns="3" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1388" runat="server" Columns="3" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1389" runat="server" Columns="3" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1390" runat="server" Columns="3" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV1391" runat="server" Columns="25" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1392" runat="server" Columns="3" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1393" runat="server" Columns="3" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1394" runat="server" Columns="3" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1395" runat="server" Columns="3" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1396" runat="server" Columns="3" TabIndex="20606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1397" runat="server" Columns="3" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1398" runat="server" Columns="3" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1399" runat="server" Columns="3" TabIndex="20608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV1400" runat="server" Columns="25" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1401" runat="server" Columns="3" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1402" runat="server" Columns="3" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1403" runat="server" Columns="3" TabIndex="20704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1404" runat="server" Columns="3" TabIndex="20705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1405" runat="server" Columns="3" TabIndex="20706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1406" runat="server" Columns="3" TabIndex="20707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1407" runat="server" Columns="3" TabIndex="20708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1408" runat="server" Columns="3" TabIndex="20709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblTotal62" runat="server" CssClass="lblRojo" Text="TOTAL" Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1409" runat="server" Columns="3" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1410" runat="server" Columns="3" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1411" runat="server" Columns="3" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1412" runat="server" Columns="3" TabIndex="20804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV1413" runat="server" Columns="3" TabIndex="20805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1414" runat="server" Columns="3" TabIndex="20806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1415" runat="server" Columns="3" TabIndex="20807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1416" runat="server" Columns="3" TabIndex="20808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblPersonalAdmin" runat="server" CssClass="lblRojo" Font-Size="14px" Text="PERSONAL ADMINISTRATIVO"
                                    Width="480px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblInstrucciones7" runat="server" CssClass="lblRojo" Text="7. Escriba el n�mero de secretarias(os) y de otro personal administrativo que labore en la unidad."
                                    Width="800px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center; height: 15px;">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 15px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 15px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="width: 180px; height: 26px; text-align: center">
                                <asp:Label ID="lblSecretarias7" runat="server" CssClass="lblRojo" Text="SECRETARIAS(OS)"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td colspan="2" style="width: 180px; height: 26px; text-align: center">
                                <asp:Label ID="lblOtro7" runat="server" CssClass="lblRojo" Text="OTRO*"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td colspan="2" style="width: 180px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal7" runat="server" CssClass="lblRojo" Text="TOTAL"
                                    Width="180px"></asp:Label></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresSec7" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresSec7" runat="server" CssClass="lblGrisTit" Text="MUJERES"
                                    Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresOtro7" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresOtro7" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresTot7" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresTot7" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1417" runat="server" Columns="2" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1418" runat="server" Columns="2" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1419" runat="server" Columns="2" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1420" runat="server" Columns="2" TabIndex="20904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1421" runat="server" Columns="2" TabIndex="20905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1422" runat="server" Columns="2" TabIndex="20906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="1" style="width: 180px; text-align: left">
                                <asp:Label ID="lblEspecifique6" runat="server" CssClass="lblRojo" Text="*ESPECIFIQUE:" Width="180px"></asp:Label></td>
                            <td colspan="3" style="width: 270px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV1423" runat="server" Columns="35" TabIndex="21001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblTotalPers" runat="server" CssClass="lblRojo" Text="TOTAL DE PERSONAL (Considere al personal reportado en los apartados 1, 5, 6 y 7.)" Width="530px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1448" runat="server" Columns="4" TabIndex="21101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" rowspan="1" style="height: 26px; text-align: left">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" rowspan="1" style="padding-bottom: 20px; text-align: left">
                                <asp:Label ID="lblInstrucciones8" runat="server" CssClass="lblRojo" Text="8. Del n�mero total de personal que labore en la unidad escriba:"
                                    Width="800px"></asp:Label></td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="width: 270px; height: 26px; text-align: left">
                                <asp:Label ID="lblLicLimitada8" runat="server" CssClass="lblGrisTit" Text="PERSONAL CON LICENCIA LIMITADA"
                                    Width="270px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1424" runat="server" Columns="3" TabIndex="21201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="width: 270px; height: 26px; text-align: left">
                                <asp:Label ID="lblComisionado8" runat="server" CssClass="lblGrisTit" Text="PERSONAL COMISIONADO"
                                    Width="270px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="padding-left: 30px; width: 270px; height: 26px;
                                text-align: left">
                                <asp:Label ID="lblComInterno8" runat="server" CssClass="lblGrisTit" Text="INTERNO" Width="240px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1425" runat="server" Columns="3" TabIndex="21301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="padding-left: 30px; width: 270px; height: 26px;
                                text-align: left">
                                <asp:Label ID="lblComExterno8" runat="server" CssClass="lblGrisTit" Text="EXTERNO" Width="240px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1426" runat="server" Columns="3" TabIndex="21401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="width: 270px; height: 26px; text-align: left">
                                <asp:Label ID="lblBecado8" runat="server" CssClass="lblGrisTit" Text="PERSONAL BECADO"
                                    Width="270px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1427" runat="server" Columns="3" TabIndex="21501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 89px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGD_911_USAER_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_USAER_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('CM_911_USAER_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('CM_911_USAER_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                     function OPTs2Txt(){
                     marcarTXT('1213');
                     marcarTXT('1214'); 
                     marcarTXT('1220');
                     marcarTXT('1221');
                     marcarTXT('1222');
                     marcarTXT('1223');
                     marcarTXT('1224');
                     marcarTXT('1225');       
                } 
                function PintaOPTs(){
                     marcar('1213');
                     marcar('1214'); 
                     marcar('1220');
                     marcar('1221');
                     marcar('1222');
                     marcar('1223');
                     marcar('1224');
                     marcar('1225');       
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                 PintaOPTs();

                
              function OnChange_Nivel(dropdown)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    var SelText = dropdown.options[myindex].text;
                    document.getElementById("ctl00_cphMainMaster_txtV1215").value = SelValue;
                    var txt = document.getElementById("ctl00_cphMainMaster_txtV1216");
                    txt.value = SelText;
                  
                    return true;
                }
              function OnChange_Especialidad(dropdown)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    var SelText = dropdown.options[myindex].text;
                    document.getElementById("ctl00_cphMainMaster_txtV1217").value = SelValue;
                    var txt = document.getElementById("ctl00_cphMainMaster_txtV1218");
                    txt.value = SelText;
                  
                    return true;
                }
 
 		    Disparador(<%=hidDisparador.Value %>);
            MaxCol = 17;
            MaxRow = 31;
            TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
            GetTabIndexes();
        </script> 
 
</asp:Content>
