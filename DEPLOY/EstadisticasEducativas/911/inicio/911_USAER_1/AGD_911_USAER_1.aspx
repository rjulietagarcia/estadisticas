<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-1(Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_USAER_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_USAER_1.AGD_911_USAER_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1380px; height:65px;">
    <div id="header">
    
    <table style="width:100%">
       <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    
    </div></div>
    <div id="menu" style="min-width:1380px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_1',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_1',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_1',true)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_1',true)"><a href="#" title=""><span>SECUNDARIA</span></a></li>
        <li onclick="openPage('AGD_911_USAER_1',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_USAER_1',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
  <center>  
           
   
    
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td>
                    <table id="TABLE1" style="text-align: center; ">
                        <tr>
                            <td colspan="6" rowspan="1" style="text-align: left; vertical-align: top;">
                                <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="6. Escriba, por escuela, el total de alumnos beneficiados al inicio del ciclo escolar, seg�n el servicio o nivel educativo."
                                    Width="540px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="padding-right: 10px; padding-left: 10px; text-align: left">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 270px;
                                text-align: left">
                                <asp:Label ID="lbl7" runat="server" CssClass="lblRojo" Text="7. Escriba, por escuela, el n�mero de maestros que recibieron asesor�a al inicio del ciclo escolar."
                                    Width="180px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="padding-right: 10px; padding-left: 10px; width: 270px;
                                height: 26px; text-align: left">
                            </td>
                            <td colspan="3" rowspan="1" style="width: 270px;
                                height: 26px; text-align: left">
                                <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="8. Escriba, por escuela, el n�mero de padres de familia que recibe orientaci�n al inicio del ciclo escolar."
                                    Width="180px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblAlumnos6" runat="server" CssClass="lblRojo" Text="ALUMNOS BENEFICIADOS" Width="480px"></asp:Label>
                                </td>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td colspan="3" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center; height: 26px; width: 90px;">
                                <asp:Label ID="lblEscuela6" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblInicial6" runat="server" CssClass="lblRojo" Text="INICIAL" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblPreescolar6" runat="server" CssClass="lblRojo" Text="PREESCOLAR" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblPrimaria6" runat="server" CssClass="lblRojo" Text="PRIMARIA" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblSecundaria6" runat="server" CssClass="lblRojo" Text="SECUNDARIA" Width="67px"></asp:Label>&nbsp;</td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal61" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td colspan="" style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td colspan="" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEscuela7" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td colspan="" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMaestros7" runat="server" CssClass="lblRojo" Text="MAESTROS ASESORADOS"
                                    Width="80px"></asp:Label></td>
                            <td colspan="" style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td colspan="" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEscuela8" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td colspan="" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblPadres8" runat="server" CssClass="lblRojo" Text="PADRES ORIENTADOS"
                                    Width="80px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblEsc16" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV942" runat="server" Columns="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV943" runat="server" Columns="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV944" runat="server" Columns="2" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV945" runat="server" Columns="2" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV946" runat="server" Columns="2" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc17" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV997" runat="server" Columns="3" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc18" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1008" runat="server" Columns="3" TabIndex="21201" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblEsc26" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV947" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV948" runat="server" Columns="2" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV949" runat="server" Columns="2" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV950" runat="server" Columns="2" TabIndex="10204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV951" runat="server" Columns="2" TabIndex="10205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc27" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV998" runat="server" Columns="3" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc28" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1009" runat="server" Columns="3" TabIndex="21301" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 90px; height: 12px;">
                            <asp:Label ID="lblEsc36" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 12px;">
                                <asp:TextBox ID="txtV952" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 12px;">
                                <asp:TextBox ID="txtV953" runat="server" Columns="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 12px;">
                                <asp:TextBox ID="txtV954" runat="server" Columns="2" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV955" runat="server" Columns="2" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV956" runat="server" Columns="2" TabIndex="10305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 12px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 12px; text-align: center">
                                <asp:Label ID="lblEsc37" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV999" runat="server" Columns="3" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 12px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 12px; text-align: center">
                                <asp:Label ID="lblEsc38" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV1010" runat="server" Columns="3" TabIndex="21401" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px; width: 90px;">
                            <asp:Label ID="lblEsc46" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV957" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV958" runat="server" Columns="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV959" runat="server" Columns="2" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV960" runat="server" Columns="2" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV961" runat="server" Columns="2" TabIndex="10405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc47" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1000" runat="server" Columns="3" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc48" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1011" runat="server" Columns="3" TabIndex="21501" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px; width: 90px;">
                                <asp:Label ID="lblEsc56" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV962" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV963" runat="server" Columns="2" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV964" runat="server" Columns="2" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV965" runat="server" Columns="2" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV966" runat="server" Columns="2" TabIndex="10505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc57" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1001" runat="server" Columns="3" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc58" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1012" runat="server" Columns="3" TabIndex="21601" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px; width: 90px;">
                                <asp:Label ID="lblEsc66" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV967" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV968" runat="server" Columns="2" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV969" runat="server" Columns="2" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV970" runat="server" Columns="2" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV971" runat="server" Columns="2" TabIndex="10605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc67" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1002" runat="server" Columns="3" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc68" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1013" runat="server" Columns="3" TabIndex="21701" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblEsc76" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV972" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV973" runat="server" Columns="2" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV974" runat="server" Columns="2" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV975" runat="server" Columns="2" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV976" runat="server" Columns="2" TabIndex="10705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc77" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1003" runat="server" Columns="3" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc78" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1014" runat="server" Columns="3" TabIndex="21801" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblEsc86" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV977" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV978" runat="server" Columns="2" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV979" runat="server" Columns="2" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV980" runat="server" Columns="2" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV981" runat="server" Columns="2" TabIndex="10805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc87" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1004" runat="server" Columns="3" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc88" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1015" runat="server" Columns="3" TabIndex="21901" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblEsc96" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV982" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV983" runat="server" Columns="2" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV984" runat="server" Columns="2" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV985" runat="server" Columns="2" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV986" runat="server" Columns="2" TabIndex="10905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc97" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1005" runat="server" Columns="3" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc98" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1016" runat="server" Columns="3" TabIndex="22001" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px; width: 90px;">
                                <asp:Label ID="lblEsc106" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV987" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV988" runat="server" Columns="2" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV989" runat="server" Columns="2" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV990" runat="server" Columns="2" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV991" runat="server" Columns="2" TabIndex="11005" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc107" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1006" runat="server" Columns="3" TabIndex="21001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc108" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1017" runat="server" Columns="3" TabIndex="22101" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblTotal62" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV992" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV993" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px; width: 90px;">
                                <asp:TextBox ID="txtV994" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV995" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV996" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal7" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1007" runat="server" Columns="4" TabIndex="21101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="padding-left: 10px; width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal8" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV1018" runat="server" Columns="4" TabIndex="22201" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 37px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1" style="height: 4px; text-align: left">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9. Escriba la poblaci�n total atendida, desglos�ndola por nivel, servicio, edad cumplida a la fecha de llenado del cuestionario y sexo."
                                    Width="800px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                </td>
                            <td colspan="2" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblInicial9" runat="server" CssClass="lblRojo" Text="EDUCACI�N INICIAL" Width="180px"></asp:Label></td>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblPreescolar9" runat="server" CssClass="lblRojo" Text="PREESCOLAR"
                                    Width="180px"></asp:Label></td>
                            <td colspan="2" style="text-align: center; width: 90px; height: 26px;">
                                &nbsp;<asp:Label ID="lblPrimaria9" runat="server" CssClass="lblRojo" Text="PRIMARIA"
                                    Width="180px"></asp:Label></td>
                            <td colspan="2" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblSecundaria9" runat="server" CssClass="lblRojo" Text="SECUNDARIA" Width="180px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal91" runat="server" CssClass="lblRojo" Text="TOTAL" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresIni9" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresIni9" runat="server" CssClass="lblGrisTit" Text="MUJERES"
                                    Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresPre9" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresPre9" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresPri9" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresPri9" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresSec9" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresSec9" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 30px; width: 90px;">
                                <asp:Label ID="lbl45Dias" runat="server" CssClass="lblGrisTit" Text="45 d�as a 1 a�o" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1019" runat="server" Columns="3" TabIndex="30101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1020" runat="server" Columns="3" TabIndex="30102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1021" runat="server" Columns="3" TabIndex="30103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1022" runat="server" Columns="3" TabIndex="30104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1023" runat="server" Columns="3" TabIndex="30105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1024" runat="server" Columns="3" TabIndex="30106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1025" runat="server" Columns="3" TabIndex="30107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1026" runat="server" Columns="3" TabIndex="30108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1027" runat="server" Columns="3" TabIndex="30109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lbl2A�os" runat="server" CssClass="lblGrisTit" Text="2 a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1028" runat="server" Columns="3" TabIndex="30201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1029" runat="server" Columns="3" TabIndex="30202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1030" runat="server" Columns="3" TabIndex="30203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1031" runat="server" Columns="3" TabIndex="30204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1032" runat="server" Columns="3" TabIndex="30205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1033" runat="server" Columns="3" TabIndex="30206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1034" runat="server" Columns="3" TabIndex="30207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1035" runat="server" Columns="3" TabIndex="30208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1036" runat="server" Columns="3" TabIndex="30209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lbl3A�os" runat="server" CssClass="lblGrisTit" Text="3 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1037" runat="server" Columns="3" TabIndex="30301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1038" runat="server" Columns="3" TabIndex="30302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1039" runat="server" Columns="3" TabIndex="30303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1040" runat="server" Columns="3" TabIndex="30304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1041" runat="server" Columns="3" TabIndex="30305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1042" runat="server" Columns="3" TabIndex="30306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1043" runat="server" Columns="3" TabIndex="30307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1044" runat="server" Columns="3" TabIndex="30308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1045" runat="server" Columns="3" TabIndex="30309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lbl3A�os6Meses" runat="server" CssClass="lblGrisTit" Text="3 a�os 6 meses a 3 a�os 11 meses"
                                    Width="100px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1046" runat="server" Columns="3" TabIndex="30401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1047" runat="server" Columns="3" TabIndex="30402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1048" runat="server" Columns="3" TabIndex="30403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1049" runat="server" Columns="3" TabIndex="30404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1050" runat="server" Columns="3" TabIndex="30405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1051" runat="server" Columns="3" TabIndex="30406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1052" runat="server" Columns="3" TabIndex="30407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1053" runat="server" Columns="3" TabIndex="30408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1054" runat="server" Columns="3" TabIndex="30409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lbl4A�os" runat="server" CssClass="lblGrisTit" Text="4 a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1055" runat="server" Columns="3" TabIndex="30501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1056" runat="server" Columns="3" TabIndex="30502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1057" runat="server" Columns="3" TabIndex="30503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1058" runat="server" Columns="3" TabIndex="30504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1059" runat="server" Columns="3" TabIndex="30505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1060" runat="server" Columns="3" TabIndex="30506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1061" runat="server" Columns="3" TabIndex="30507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1062" runat="server" Columns="3" TabIndex="30508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1063" runat="server" Columns="3" TabIndex="30509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lbl5A�os" runat="server" CssClass="lblGrisTit" Text="5 a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1064" runat="server" Columns="3" TabIndex="30601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1065" runat="server" Columns="3" TabIndex="30602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1066" runat="server" Columns="3" TabIndex="30603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1067" runat="server" Columns="3" TabIndex="30604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1068" runat="server" Columns="3" TabIndex="30605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1069" runat="server" Columns="3" TabIndex="30606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1070" runat="server" Columns="3" TabIndex="30607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1071" runat="server" Columns="3" TabIndex="30608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1072" runat="server" Columns="3" TabIndex="30609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lbl6A�os" runat="server" CssClass="lblGrisTit" Text="6 a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1073" runat="server" Columns="3" TabIndex="30701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1074" runat="server" Columns="3" TabIndex="30702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1075" runat="server" Columns="3" TabIndex="30703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1076" runat="server" Columns="3" TabIndex="30704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1077" runat="server" Columns="3" TabIndex="30705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1078" runat="server" Columns="3" TabIndex="30706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1079" runat="server" Columns="3" TabIndex="30707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1080" runat="server" Columns="3" TabIndex="30708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1081" runat="server" Columns="3" TabIndex="30708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px;">
                                <asp:Label ID="lbl7A�os" runat="server" CssClass="lblGrisTit" Text="7 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1082" runat="server" Columns="3" TabIndex="30801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1083" runat="server" Columns="3" TabIndex="30802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1084" runat="server" Columns="3" TabIndex="30803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1085" runat="server" Columns="3" TabIndex="30804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1086" runat="server" Columns="3" TabIndex="30805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1087" runat="server" Columns="3" TabIndex="30806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1088" runat="server" Columns="3" TabIndex="30807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1089" runat="server" Columns="3" TabIndex="30808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1090" runat="server" Columns="3" TabIndex="30809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center; height: 26px;">
                                <asp:Label ID="lbl8A�os" runat="server" CssClass="lblGrisTit" Text="8 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1091" runat="server" Columns="3" TabIndex="30901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1092" runat="server" Columns="3" TabIndex="30902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1093" runat="server" Columns="3" TabIndex="30903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1094" runat="server" Columns="3" TabIndex="30904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1095" runat="server" Columns="3" TabIndex="30905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1096" runat="server" Columns="3" TabIndex="30906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1097" runat="server" Columns="3" TabIndex="30907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1098" runat="server" Columns="3" TabIndex="30908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1099" runat="server" Columns="3" TabIndex="30909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center; height: 26px;">
                            </td>
                            <td style="width: 90px; text-align: center; height: 26px;">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lbl9A�os" runat="server" CssClass="lblGrisTit" Text="9 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1100" runat="server" Columns="3" TabIndex="31001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1101" runat="server" Columns="3" TabIndex="31002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1102" runat="server" Columns="3" TabIndex="31003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1103" runat="server" Columns="3" TabIndex="31004" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1104" runat="server" Columns="3" TabIndex="31005" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1105" runat="server" Columns="3" TabIndex="31006" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1106" runat="server" Columns="3" TabIndex="31007" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1107" runat="server" Columns="3" TabIndex="31008" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1108" runat="server" Columns="3" TabIndex="31009" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lbl10A�os" runat="server" CssClass="lblGrisTit" Text="10 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1109" runat="server" Columns="3" TabIndex="31101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1110" runat="server" Columns="3" TabIndex="31102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1111" runat="server" Columns="3" TabIndex="31103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1112" runat="server" Columns="3" TabIndex="31104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1113" runat="server" Columns="3" TabIndex="31105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1114" runat="server" Columns="3" TabIndex="31106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1115" runat="server" Columns="3" TabIndex="31107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1116" runat="server" Columns="3" TabIndex="31108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1117" runat="server" Columns="3" TabIndex="31109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lbl11A�os" runat="server" CssClass="lblGrisTit" Text="11 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1118" runat="server" Columns="3" TabIndex="31201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1119" runat="server" Columns="3" TabIndex="31202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1120" runat="server" Columns="3" TabIndex="31203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1121" runat="server" Columns="3" TabIndex="31204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1122" runat="server" Columns="3" TabIndex="31205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1123" runat="server" Columns="3" TabIndex="31206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1124" runat="server" Columns="3" TabIndex="31207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1125" runat="server" Columns="3" TabIndex="31208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1126" runat="server" Columns="3" TabIndex="31209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lbl12A�os" runat="server" CssClass="lblGrisTit" Text="12 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1127" runat="server" Columns="3" TabIndex="31301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1128" runat="server" Columns="3" TabIndex="31302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1129" runat="server" Columns="3" TabIndex="31303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1130" runat="server" Columns="3" TabIndex="31304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1131" runat="server" Columns="3" TabIndex="31305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1132" runat="server" Columns="3" TabIndex="31306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1133" runat="server" Columns="3" TabIndex="31307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1134" runat="server" Columns="3" TabIndex="31308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1135" runat="server" Columns="3" TabIndex="31309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lbl13A�os" runat="server" CssClass="lblGrisTit" Text="13 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1136" runat="server" Columns="3" TabIndex="31401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1137" runat="server" Columns="3" TabIndex="31402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1138" runat="server" Columns="3" TabIndex="31403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1139" runat="server" Columns="3" TabIndex="31404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1140" runat="server" Columns="3" TabIndex="31405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1141" runat="server" Columns="3" TabIndex="31406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1142" runat="server" Columns="3" TabIndex="31407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1143" runat="server" Columns="3" TabIndex="31408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1144" runat="server" Columns="3" TabIndex="31409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lbl14A�os" runat="server" CssClass="lblGrisTit" Text="14 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1145" runat="server" Columns="3" TabIndex="31501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1146" runat="server" Columns="3" TabIndex="31502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1147" runat="server" Columns="3" TabIndex="31503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1148" runat="server" Columns="3" TabIndex="31504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1149" runat="server" Columns="3" TabIndex="31505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1150" runat="server" Columns="3" TabIndex="31506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1151" runat="server" Columns="3" TabIndex="31507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1152" runat="server" Columns="3" TabIndex="31508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1153" runat="server" Columns="3" TabIndex="31509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; height: 16px; text-align: center">
                                <asp:Label ID="lbl15A�os" runat="server" CssClass="lblGrisTit" Text="15 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1154" runat="server" Columns="3" TabIndex="31601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1155" runat="server" Columns="3" TabIndex="31602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1156" runat="server" Columns="3" TabIndex="31603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1157" runat="server" Columns="3" TabIndex="31604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1158" runat="server" Columns="3" TabIndex="31605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1159" runat="server" Columns="3" TabIndex="31606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1160" runat="server" Columns="3" TabIndex="31607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1161" runat="server" Columns="3" TabIndex="31608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1162" runat="server" Columns="3" TabIndex="31609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 16px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 16px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lbl16A�os" runat="server" CssClass="lblGrisTit" Text="16 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1163" runat="server" Columns="3" TabIndex="31701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1164" runat="server" Columns="3" TabIndex="31702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1165" runat="server" Columns="3" TabIndex="31703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1166" runat="server" Columns="3" TabIndex="31704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1167" runat="server" Columns="3" TabIndex="31705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1168" runat="server" Columns="3" TabIndex="31706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1169" runat="server" Columns="3" TabIndex="31707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1170" runat="server" Columns="3" TabIndex="31708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1171" runat="server" Columns="3" TabIndex="31709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lbl17A�os" runat="server" CssClass="lblGrisTit" Text="17 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1172" runat="server" Columns="3" TabIndex="31801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1173" runat="server" Columns="3" TabIndex="31802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1174" runat="server" Columns="3" TabIndex="31803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1175" runat="server" Columns="3" TabIndex="31804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1176" runat="server" Columns="3" TabIndex="31805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1177" runat="server" Columns="3" TabIndex="31806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1178" runat="server" Columns="3" TabIndex="31807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1179" runat="server" Columns="3" TabIndex="31808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1180" runat="server" Columns="3" TabIndex="31809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lbl18A�os" runat="server" CssClass="lblGrisTit" Text="18 a�os" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1181" runat="server" Columns="3" TabIndex="31901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1182" runat="server" Columns="3" TabIndex="31902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1183" runat="server" Columns="3" TabIndex="31903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1184" runat="server" Columns="3" TabIndex="31904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1185" runat="server" Columns="3" TabIndex="31905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1186" runat="server" Columns="3" TabIndex="31906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1187" runat="server" Columns="3" TabIndex="31907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1188" runat="server" Columns="3" TabIndex="31908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1189" runat="server" Columns="3" TabIndex="31909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; text-align: center">
                                <asp:Label ID="lbl19A�os" runat="server" CssClass="lblGrisTit" Text="19 o m�s a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1190" runat="server" Columns="3" TabIndex="32001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1191" runat="server" Columns="3" TabIndex="32002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1192" runat="server" Columns="3" TabIndex="32003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1193" runat="server" Columns="3" TabIndex="32004" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1194" runat="server" Columns="3" TabIndex="32005" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1195" runat="server" Columns="3" TabIndex="32006" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1196" runat="server" Columns="3" TabIndex="32007" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1197" runat="server" Columns="3" TabIndex="32008" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1198" runat="server" Columns="3" TabIndex="32009" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                            </td>
                            <td style="width: 90px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 90px; height: 26px;">
                                <asp:Label ID="lblSubtotal9" runat="server" CssClass="lblRojo" Text="SUBTOTALES" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1199" runat="server" Columns="3" TabIndex="32101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1200" runat="server" Columns="3" TabIndex="32102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1201" runat="server" Columns="3" TabIndex="32103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1202" runat="server" Columns="3" TabIndex="32104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1203" runat="server" Columns="3" TabIndex="32105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1204" runat="server" Columns="3" TabIndex="32106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1205" runat="server" Columns="3" TabIndex="32107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1206" runat="server" Columns="3" TabIndex="32108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1207" runat="server" Columns="3" TabIndex="32109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal92" runat="server" CssClass="lblRojo" Text="TOTAL" Width="90px"></asp:Label></td>
                            <td style="text-align: center" colspan="2">
                                <asp:TextBox ID="txtV1208" runat="server" Columns="3" TabIndex="32201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" colspan="2">
                                <asp:TextBox ID="txtV1209" runat="server" Columns="3" TabIndex="32202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" colspan="2">
                                <asp:TextBox ID="txtV1210" runat="server" Columns="3" TabIndex="32203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" colspan="2">
                                <asp:TextBox ID="txtV1211" runat="server" Columns="3" TabIndex="32204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV1212" runat="server" Columns="3" TabIndex="32205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('ASEC_911_USAER_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('ASEC_911_USAER_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Personal_911_USAER_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_USAER_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          MaxCol = 17;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
         GetTabIndexes();
        </script> 
             
</asp:Content>
