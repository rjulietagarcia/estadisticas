<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-1(Alumnos Preescolar)" AutoEventWireup="true" CodeBehind="APRE_911_USAER_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_USAER_1.APRE_911_USAER_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

<div id="logo"></div>
    <div style="min-width:1380px; height:65px;">
    <div id="header">
    
    <table style="width:100%">
       <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    
    </div></div>
    <div id="menu" style="min-width:1380px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AE_911_USAER_1',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li><li onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title=""><span>INICIAL</span></a></li><li onclick="openPage('APRE_911_USAER_1',true)"><a href="#" title="" class="activo"><span>PREESCOLAR</span></a></li><li onclick="openPage('APRIM_911_USAER_1',false)"><a href="#" title=""><span>PRIMARIA</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>SECUNDARIA</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo" ><span>DESGLOSE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
     <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
    
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td >
                    <table id="TABLE1" style="text-align: center;">
                        <tr>
                            <td colspan="16" rowspan="1" style="text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3. Escriba la poblaci�n total con necesidades educativas especiales atendida en educaci�n preescolar, por escuela, grado y sexo."
                                    Width="750px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPreescolar" runat="server" CssClass="lblRojo" Text="PREESCOLAR" Width="500px"></asp:Label>
                                </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td colspan="9" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPoblacion" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL POR GRADO"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPoblacionTotal" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL (SUMA)"
                                    Width="200px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center; height: 3px; width: 67px;">
                                <asp:Label ID="lblEscuela" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="3">
                                <asp:Label ID="lblPrimero" runat="server" CssClass="lblRojo" Text="1�" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="3">
                                <asp:Label ID="lblSegundo" runat="server" CssClass="lblRojo" Text="2�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="3">
                                <asp:Label ID="lblTercero" runat="server" CssClass="lblRojo" Text="3�" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;" colspan="3">
                                &nbsp;</td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center;">
                                &nbsp;<asp:Label ID="lblTotal1" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;">
                                <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;">
                                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal2" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal3" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:Label ID="lblHombresTot" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                                <asp:Label ID="lblMujeresTot" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                                <asp:Label ID="lblTotal31" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV195" runat="server" Columns="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV196" runat="server" Columns="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV197" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV198" runat="server" Columns="2" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV199" runat="server" Columns="2" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV200" runat="server" Columns="3" TabIndex="10106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV201" runat="server" Columns="2" TabIndex="10107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV202" runat="server" Columns="2" TabIndex="10108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV203" runat="server" Columns="3" TabIndex="10109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV204" runat="server" Columns="3" TabIndex="10110" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV205" runat="server" Columns="3" TabIndex="10111" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV206" runat="server" Columns="3" TabIndex="10112" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV207" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV208" runat="server" Columns="2" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV209" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV210" runat="server" Columns="2" TabIndex="10204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV211" runat="server" Columns="2" TabIndex="10205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV212" runat="server" Columns="3" TabIndex="10206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV213" runat="server" Columns="2" TabIndex="10207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV214" runat="server" Columns="2" TabIndex="10208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV215" runat="server" Columns="3" TabIndex="10209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV216" runat="server" Columns="3" TabIndex="10210" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV217" runat="server" Columns="3" TabIndex="10211" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV218" runat="server" Columns="3" TabIndex="10212" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV219" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV220" runat="server" Columns="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV221" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV222" runat="server" Columns="2" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV223" runat="server" Columns="2" TabIndex="10305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV224" runat="server" Columns="3" TabIndex="10306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV225" runat="server" Columns="2" TabIndex="10307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV226" runat="server" Columns="2" TabIndex="10308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV227" runat="server" Columns="3" TabIndex="10309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV228" runat="server" Columns="3" TabIndex="10310" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV229" runat="server" Columns="3" TabIndex="10311" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV230" runat="server" Columns="3" TabIndex="10312" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV231" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV232" runat="server" Columns="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV233" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV234" runat="server" Columns="2" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV235" runat="server" Columns="2" TabIndex="10405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV236" runat="server" Columns="3" TabIndex="10406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV237" runat="server" Columns="2" TabIndex="10407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV238" runat="server" Columns="2" TabIndex="10408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV239" runat="server" Columns="3" TabIndex="10409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV240" runat="server" Columns="3" TabIndex="10410" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV241" runat="server" Columns="3" TabIndex="10411" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV242" runat="server" Columns="3" TabIndex="10412" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV243" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV244" runat="server" Columns="2" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV245" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV246" runat="server" Columns="2" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV247" runat="server" Columns="2" TabIndex="10505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV248" runat="server" Columns="3" TabIndex="10506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV249" runat="server" Columns="2" TabIndex="10507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV250" runat="server" Columns="2" TabIndex="10508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV251" runat="server" Columns="3" TabIndex="10509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV252" runat="server" Columns="3" TabIndex="10510" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV253" runat="server" Columns="3" TabIndex="10511" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV254" runat="server" Columns="3" TabIndex="10512" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV255" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV256" runat="server" Columns="2" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV257" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV258" runat="server" Columns="2" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV259" runat="server" Columns="2" TabIndex="10605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV260" runat="server" Columns="3" TabIndex="10606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV261" runat="server" Columns="2" TabIndex="10607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV262" runat="server" Columns="2" TabIndex="10608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV263" runat="server" Columns="3" TabIndex="10609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV264" runat="server" Columns="3" TabIndex="10610" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV265" runat="server" Columns="3" TabIndex="10611" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV266" runat="server" Columns="3" TabIndex="10612" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV267" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV268" runat="server" Columns="2" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV269" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV270" runat="server" Columns="2" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV271" runat="server" Columns="2" TabIndex="10705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV272" runat="server" Columns="3" TabIndex="10706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV273" runat="server" Columns="2" TabIndex="10707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV274" runat="server" Columns="2" TabIndex="10708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV275" runat="server" Columns="3" TabIndex="10709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV276" runat="server" Columns="3" TabIndex="10710" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV277" runat="server" Columns="3" TabIndex="10711" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV278" runat="server" Columns="3" TabIndex="10712" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV279" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV280" runat="server" Columns="2" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV281" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV282" runat="server" Columns="2" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV283" runat="server" Columns="2" TabIndex="10805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV284" runat="server" Columns="3" TabIndex="10806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV285" runat="server" Columns="2" TabIndex="10807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV286" runat="server" Columns="2" TabIndex="10808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV287" runat="server" Columns="3" TabIndex="10809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV288" runat="server" Columns="3" TabIndex="10810" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV289" runat="server" Columns="3" TabIndex="10811" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV290" runat="server" Columns="3" TabIndex="10812" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV291" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV292" runat="server" Columns="2" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV293" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV294" runat="server" Columns="2" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV295" runat="server" Columns="2" TabIndex="10905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV296" runat="server" Columns="3" TabIndex="10906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV297" runat="server" Columns="2" TabIndex="10907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV298" runat="server" Columns="2" TabIndex="10908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV299" runat="server" Columns="3" TabIndex="10909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV300" runat="server" Columns="3" TabIndex="10910" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV301" runat="server" Columns="3" TabIndex="10911" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV302" runat="server" Columns="3" TabIndex="10912" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV303" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV304" runat="server" Columns="2" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV305" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV306" runat="server" Columns="2" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV307" runat="server" Columns="2" TabIndex="11005" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV308" runat="server" Columns="3" TabIndex="11006" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV309" runat="server" Columns="2" TabIndex="11007" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV310" runat="server" Columns="2" TabIndex="11008" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV311" runat="server" Columns="3" TabIndex="11009" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV312" runat="server" Columns="3" TabIndex="11010" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV313" runat="server" Columns="3" TabIndex="11011" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV314" runat="server" Columns="3" TabIndex="11012" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 28px;">
                                <asp:Label ID="lblTotal32" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center; height: 28px;">
                                <asp:TextBox ID="txtV315" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 28px;">
                                <asp:TextBox ID="txtV316" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 28px;">
                                <asp:TextBox ID="txtV317" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 28px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV318" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 28px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV319" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV320" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV321" runat="server" Columns="3" TabIndex="11107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV322" runat="server" Columns="3" TabIndex="11108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV323" runat="server" Columns="3" TabIndex="11109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 28px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV324" runat="server" Columns="4" TabIndex="11110" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV325" runat="server" Columns="4" TabIndex="11111" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV326" runat="server" Columns="4" TabIndex="11112" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 28px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 28px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" 
                                Text="De los alumnos reportados en el rubro anterior, 
                                escriba la cantidad de alumnos 
                                con discapacidad, aptitudes sobresalientes u otras condiciones, 
                                seg�n las definiciones establecidas en el glosario."
                                    Width="1136px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNOS"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" style="text-align: center">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="200px"></asp:Label></td>
                            <td colspan="10" style="text-align: center">
                                &nbsp;<asp:Label ID="lblAlumnosEsc" runat="server" CssClass="lblRojo" Text="ALUMNOS POR ESCUELA"
                                    Width="200px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresAl" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresAl" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotalAl" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc12" runat="server" CssClass="lblGrisTit" Text="1" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc22" runat="server" CssClass="lblGrisTit" Text="2" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc32" runat="server" CssClass="lblGrisTit" Text="3" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc42" runat="server" CssClass="lblGrisTit" Text="4" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc52" runat="server" CssClass="lblGrisTit" Text="5" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc62" runat="server" CssClass="lblGrisTit" Text="6" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc72" runat="server" CssClass="lblGrisTit" Text="7" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc82" runat="server" CssClass="lblGrisTit" Text="8" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc92" runat="server" CssClass="lblGrisTit" Text="9" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc102" runat="server" CssClass="lblGrisTit" Text="10" Width="67px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left; height: 26px;">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV327" runat="server" Columns="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV328" runat="server" Columns="2" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV329" runat="server" Columns="3" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV330" runat="server" Columns="2" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV331" runat="server" Columns="2" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV332" runat="server" Columns="2" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV333" runat="server" Columns="2" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV334" runat="server" Columns="2" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV335" runat="server" Columns="2" TabIndex="20109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV336" runat="server" Columns="2" TabIndex="20110" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV337" runat="server" Columns="2" TabIndex="20111" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV338" runat="server" Columns="2" TabIndex="20112" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV339" runat="server" Columns="2" TabIndex="20113" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV340" runat="server" Columns="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV341" runat="server" Columns="2" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV342" runat="server" Columns="3" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV343" runat="server" Columns="2" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV344" runat="server" Columns="2" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV345" runat="server" Columns="2" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV346" runat="server" Columns="2" TabIndex="20207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV347" runat="server" Columns="2" TabIndex="20208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV348" runat="server" Columns="2" TabIndex="20209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV349" runat="server" Columns="2" TabIndex="20210" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV350" runat="server" Columns="2" TabIndex="20211" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV351" runat="server" Columns="2" TabIndex="20212" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV352" runat="server" Columns="2" TabIndex="20213" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV353" runat="server" Columns="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV354" runat="server" Columns="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV355" runat="server" Columns="3" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV356" runat="server" Columns="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV357" runat="server" Columns="2" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV358" runat="server" Columns="2" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV359" runat="server" Columns="2" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV360" runat="server" Columns="2" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV361" runat="server" Columns="2" TabIndex="20309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV362" runat="server" Columns="2" TabIndex="20310" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV363" runat="server" Columns="2" TabIndex="20311" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV364" runat="server" Columns="2" TabIndex="20312" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV365" runat="server" Columns="2" TabIndex="20313" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV366" runat="server" Columns="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV367" runat="server" Columns="2" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV368" runat="server" Columns="3" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV369" runat="server" Columns="2" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV370" runat="server" Columns="2" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV371" runat="server" Columns="2" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV372" runat="server" Columns="2" TabIndex="20407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV373" runat="server" Columns="2" TabIndex="20408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV374" runat="server" Columns="2" TabIndex="20409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV375" runat="server" Columns="2" TabIndex="20410" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV376" runat="server" Columns="2" TabIndex="20411" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV377" runat="server" Columns="2" TabIndex="20412" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV378" runat="server" Columns="2" TabIndex="20413" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV379" runat="server" Columns="2" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV380" runat="server" Columns="2" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV381" runat="server" Columns="3" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV382" runat="server" Columns="2" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV383" runat="server" Columns="2" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV384" runat="server" Columns="2" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV385" runat="server" Columns="2" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV386" runat="server" Columns="2" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV387" runat="server" Columns="2" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV388" runat="server" Columns="2" TabIndex="20510" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV389" runat="server" Columns="2" TabIndex="20511" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV390" runat="server" Columns="2" TabIndex="20512" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV391" runat="server" Columns="2" TabIndex="20513" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV392" runat="server" Columns="2" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV393" runat="server" Columns="2" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV394" runat="server" Columns="3" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV395" runat="server" Columns="2" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV396" runat="server" Columns="2" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV397" runat="server" Columns="2" TabIndex="20606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV398" runat="server" Columns="2" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV399" runat="server" Columns="2" TabIndex="20608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV400" runat="server" Columns="2" TabIndex="20609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV401" runat="server" Columns="2" TabIndex="20610" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV402" runat="server" Columns="2" TabIndex="20611" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV403" runat="server" Columns="2" TabIndex="20612" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV404" runat="server" Columns="2" TabIndex="20613" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV405" runat="server" Columns="2" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV406" runat="server" Columns="2" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV407" runat="server" Columns="3" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV408" runat="server" Columns="2" TabIndex="20704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV409" runat="server" Columns="2" TabIndex="20705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV410" runat="server" Columns="2" TabIndex="20706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV411" runat="server" Columns="2" TabIndex="20707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV412" runat="server" Columns="2" TabIndex="20708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV413" runat="server" Columns="2" TabIndex="20709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV414" runat="server" Columns="2" TabIndex="20710" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV415" runat="server" Columns="2" TabIndex="20711" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV416" runat="server" Columns="2" TabIndex="20712" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV417" runat="server" Columns="2" TabIndex="20713" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV418" runat="server" Columns="2" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV419" runat="server" Columns="2" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV420" runat="server" Columns="3" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV421" runat="server" Columns="2" TabIndex="20804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV422" runat="server" Columns="2" TabIndex="20805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV423" runat="server" Columns="2" TabIndex="20806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV424" runat="server" Columns="2" TabIndex="20807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV425" runat="server" Columns="2" TabIndex="20808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV426" runat="server" Columns="2" TabIndex="20809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV427" runat="server" Columns="2" TabIndex="20810" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV428" runat="server" Columns="2" TabIndex="20811" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV429" runat="server" Columns="2" TabIndex="20812" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV430" runat="server" Columns="2" TabIndex="20813" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblTotal33" runat="server" CssClass="lblRojo" Text="TOTAL" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV431" runat="server" Columns="3" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV432" runat="server" Columns="3" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV433" runat="server" Columns="3" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV434" runat="server" Columns="3" TabIndex="20904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV435" runat="server" Columns="3" TabIndex="20905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV436" runat="server" Columns="3" TabIndex="20906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV437" runat="server" Columns="3" TabIndex="20907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV438" runat="server" Columns="3" TabIndex="20908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV439" runat="server" Columns="3" TabIndex="20909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV440" runat="server" Columns="3" TabIndex="20910" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV441" runat="server" Columns="3" TabIndex="20911" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV442" runat="server" Columns="3" TabIndex="20912" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV443" runat="server" Columns="3" TabIndex="20913" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 14px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('APRIM_911_USAER_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('APRIM_911_USAER_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
          MaxCol = 17;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
         GetTabIndexes();
        </script> 

</asp:Content>
