<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-1(Alumnos por CT)" AutoEventWireup="true" CodeBehind="AE_911_USAER_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_USAER_1.AE_911_USAER_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1380px; height:65px;">
    <div id="header">

    <table style="width:100%">
       <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    
    </div></div>
    <div id="menu" style="min-width:1380px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_1',true)"><a href="#" title="" class="activo"><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_1',false)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PREESCOLAR</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PRIMARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>SECUNDARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
<center>  
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot"  cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td>
                    <table style="width: 820px" >
                        <tr>
                            <td colspan="5" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Font-Size="16px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" rowspan="1" style="padding-bottom: 10px; height: 3px; text-align: left">
                                <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1. Para dar respuesta a los requerimientos de la tabla siguiente, tenga en cuenta las especificaciones de los incisos que se presentan a continuaci�n:"
                                    Width="800px"></asp:Label>
                                <asp:Label ID="lblNotaA" runat="server" CssClass="lblRojo" Text="A. Escriba la clave del centro de trabajo de las escuelas regulares (inicial, preescolar, primaria y/o secundaria). Si funciona la sede de la unidad en una de las escuelas, an�tela en primer lugar."
                                    Width="800px"></asp:Label>
                                <asp:Label ID="lblNotaB" runat="server" CssClass="lblRojo" Text="B. Escriba el n�mero de turno: 1 Matutino, 2 Vespertino, 3 Nocturno, 4 Discontinuo, 5 Continuo (tiempo completo), 6 Complementario, 7 Continuo (horario ampliado)."
                                    Width="800px"></asp:Label>
                                <asp:Label ID="lblNotaC" runat="server" CssClass="lblRojo" Text="C. Escriba el n�mero de docentes asignados por escuela."
                                    Width="800px"></asp:Label>
                                <asp:Label ID="lblNotaD" runat="server" CssClass="lblRojo" Text="D. Escriba el n�mero de alumnos con discapacidad o aptitudes sobresalientes que asisten a la escuela regular."
                                    Width="800px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center; height: 3px; width: 25px;">
                                </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblA" runat="server" CssClass="lblRojo" Text="(A)"></asp:Label></td>
                            <td style="text-align: center;">
                                &nbsp;<asp:Label ID="lblB" runat="server" CssClass="lblRojo" Text="(B)"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblC" runat="server" CssClass="lblRojo" Text="(C)"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblD" runat="server" CssClass="lblRojo" Text="(D)"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 3px; text-align: center; width: 25px;">
                            </td>
                            <td style="text-align: center">
                                &nbsp;<asp:Label ID="lblClave" runat="server" CssClass="lblRojo" Text="CLAVE DE LA ESCUELA"
                                    Width="100px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTurno" runat="server" CssClass="lblRojo" Text="TURNO" Width="99px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblDocentes" runat="server" CssClass="lblRojo" Text="N�MERO DE DOCENTES POR ESCUELA"
                                    Width="150px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblAlumnosDisc" runat="server" CssClass="lblRojo" Text="No. DE ALUMNOS CON DISCAPACIDAD O APTITUDES SOBRESALIENTES"
                                    Width="300px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: right; width: 25px;">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV3" runat="server" Columns="10" TabIndex="10101" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV4" runat="server" Columns="1" TabIndex="10102" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV5" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV6" runat="server" Columns="5" TabIndex="10104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: right; width: 25px;">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV7" runat="server" Columns="10" TabIndex="10201" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV8" runat="server" Columns="1" TabIndex="10202" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV9" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV10" runat="server" Columns="5" TabIndex="10204" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: right; width: 25px;">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV11" runat="server" Columns="10" TabIndex="10301" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV12" runat="server" Columns="1" TabIndex="10302" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV13" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV14" runat="server" Columns="5" TabIndex="10304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="height: 26px; text-align: right; width: 25px;">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV15" runat="server" Columns="10" TabIndex="10401" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV16" runat="server" Columns="1" TabIndex="10402" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV17" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV18" runat="server" Columns="5" TabIndex="10404" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 26px; text-align: right; width: 25px;">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV19" runat="server" Columns="10" TabIndex="10501" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV20" runat="server" Columns="1" TabIndex="10502" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV21" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV22" runat="server" Columns="5" TabIndex="10504" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 26px; text-align: right; width: 25px;">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV23" runat="server" Columns="10" TabIndex="10601" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV24" runat="server" Columns="1" TabIndex="10602" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV25" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV26" runat="server" Columns="5" TabIndex="10604" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 26px; text-align: right; width: 25px;">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV27" runat="server" Columns="10" TabIndex="10701" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV28" runat="server" Columns="1" TabIndex="10702" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV29" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV30" runat="server" Columns="5" TabIndex="10704" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 26px; text-align: right; width: 25px;">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV31" runat="server" Columns="10" TabIndex="10801" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV32" runat="server" Columns="1" TabIndex="10802" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV33" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV34" runat="server" Columns="5" TabIndex="10804" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 26px; text-align: right; width: 25px;">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV35" runat="server" Columns="10" TabIndex="10901" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV36" runat="server" Columns="1" TabIndex="10902" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV37" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV38" runat="server" Columns="5" TabIndex="10904" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 26px; text-align: right; width: 25px;">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV39" runat="server" Columns="10" TabIndex="11001" CssClass="lblNegro" MaxLength="10"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV40" runat="server" Columns="1" TabIndex="11002" CssClass="lblNegro" MaxLength="1" onkeypress="return Num(event)"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV41" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV42" runat="server" Columns="5" TabIndex="11004" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 26px; text-align: left; width: 25px;">
                            </td>
                            <td style="height: 26px; text-align: center">
                            </td>
                            <td style="height: 26px; text-align: center">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV43" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center">
                                <asp:TextBox ID="txtV44" runat="server" Columns="5" TabIndex="11102" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
 
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_USAER_1')"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_USAER_1')"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('AINI_911_USAER_1')"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AINI_911_USAER_1')"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div style="width:800px" id="divResultado" class="divResultado"  ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                 
               
          		Disparador(<%=hidDisparador.Value %>);
          
                MaxCol = 17;
                MaxRow = 31;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
        </script> 
         
</asp:Content>
