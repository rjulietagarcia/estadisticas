<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7T(Alumnos por edad)" AutoEventWireup="true" CodeBehind="Alumnos2_911_7T.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7T.Alumnos2_911_7T" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <script type="text/javascript">
        MaxCol = 15;
        MaxRow = 28;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>

    
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
    <table style="width:100%">
        
        <tr><td><span>BACHILLERATO TECNOL�GICO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
      <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title="" ><span>IDENTIFICACI�N</span></a></li>
      <li onclick="openPage('Carreras_911_7T',true)"><a href="#" title="" ><span>ALUMNOS POR CARRERA</span></a></li>
      <li onclick="openPage('Alumnos1_911_7T',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
      <li onclick="openPage('Egresados_911_7T',true)"><a href="#" title="" ><span>EGRESADOS</span></a></li>
      <li onclick="openPage('Alumnos2_911_7T',true)"><a href="#" title=""  class="activo"><span>ALUMNOS POR EDAD</span></a></li>
      <li onclick="openPage('Planteles_911_7T',false)"><a href="#" title="" ><span>PLANTELES</span></a></li>
      <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
      <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li>
      <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
      <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
      <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    
   <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

<center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        &nbsp;
        <table>
            <tr>
                <td colspan="16" style="text-align:justify">
                    <asp:Label ID="lblALUMNOSPOREDAD" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                        Text="III. ALUMNOS DE NUEVO INGRESO Y REPETIDORES, POR EDAD" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="16" style="text-align:justify">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True" 
                    Text="1. Escriba el total de alumnos inscritos a partir de la fecha de inicio 
                    de cursos, sumando las altas y restando las bajas hasta el 30 de septiembre, y desgl�selo por grado, sexo, 
                    nuevo ingreso, repetidores y edad. Verifique que la suma de los subtotales de alumnos por edad sea igual 
                    al total. Incluya a los alumnos de los planteles de extensi�n o m�dulos."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="16" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="lbltabla" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="Nota: No utilice las �reas sombreadas."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td rowspan="1">
                </td>
                <td rowspan="1">
                </td>
                <td>
                </td>
                <td style="text-align: center">
                    <asp:Label ID="lbl14a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="14 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl15a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="15 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl16a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="16 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl17a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl18a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl19a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl20a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl21a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl22a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl23a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl24a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl25a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="25 a�os y mas"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td rowspan="5" style="text-align: center">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o. (sem. 1 y 2)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcion1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="110px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV96" runat="server" Columns="4" MaxLength="4" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV97" runat="server" Columns="4" MaxLength="4" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV98" runat="server" Columns="4" MaxLength="4" TabIndex="10103"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV99" runat="server" Columns="4" MaxLength="4" TabIndex="10104"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV100" runat="server" Columns="4" MaxLength="4" TabIndex="10105"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV101" runat="server" Columns="4" MaxLength="4" TabIndex="10106"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV102" runat="server" Columns="4" MaxLength="4" TabIndex="10107"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV103" runat="server" Columns="4" MaxLength="4" TabIndex="10108"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV104" runat="server" Columns="4" MaxLength="4" TabIndex="10109"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV105" runat="server" Columns="4" MaxLength="4" TabIndex="10110"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV106" runat="server" Columns="4" MaxLength="4" TabIndex="10111"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV107" runat="server" Columns="4" MaxLength="4" TabIndex="10112"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV108" runat="server" Columns="4" MaxLength="4" TabIndex="10113"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblExistencia1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack1" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" TabIndex="10201" MaxLength="4"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV109" runat="server" Columns="4" MaxLength="4" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV110" runat="server" Columns="4" MaxLength="4" TabIndex="10203"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV111" runat="server" Columns="4" MaxLength="4" TabIndex="10204"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV112" runat="server" Columns="4" MaxLength="4" TabIndex="10205"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV113" runat="server" Columns="4" MaxLength="4" TabIndex="10206"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV114" runat="server" Columns="4" MaxLength="4" TabIndex="10207"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV115" runat="server" Columns="4" MaxLength="4" TabIndex="10208"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV116" runat="server" Columns="4" MaxLength="4" TabIndex="10209"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV117" runat="server" Columns="4" MaxLength="4" TabIndex="10210"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV118" runat="server" Columns="4" MaxLength="4" TabIndex="10211"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV119" runat="server" Columns="4" MaxLength="4" TabIndex="10212"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV120" runat="server" Columns="4" MaxLength="4" TabIndex="10213"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcion1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV121" runat="server" Columns="4" MaxLength="4" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV122" runat="server" Columns="4" MaxLength="4" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV123" runat="server" Columns="4" MaxLength="4" TabIndex="10303"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV124" runat="server" Columns="4" MaxLength="4" TabIndex="10304"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV125" runat="server" Columns="4" MaxLength="4" TabIndex="10305"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV126" runat="server" Columns="4" MaxLength="4" TabIndex="10306"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV127" runat="server" Columns="4" MaxLength="4" TabIndex="10307"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV128" runat="server" Columns="4" MaxLength="4" TabIndex="10308"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV129" runat="server" Columns="4" MaxLength="4" TabIndex="10309"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV130" runat="server" Columns="4" MaxLength="4" TabIndex="10310"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV131" runat="server" Columns="4" MaxLength="4" TabIndex="10311"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV132" runat="server" Columns="4" MaxLength="4" TabIndex="10312"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV133" runat="server" Columns="4" MaxLength="4" TabIndex="10313"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblExistencia1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack2" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV134" runat="server" Columns="4" MaxLength="4" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV135" runat="server" Columns="4" MaxLength="4" TabIndex="10403"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV136" runat="server" Columns="4" MaxLength="4" TabIndex="10404"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV137" runat="server" Columns="4" MaxLength="4" TabIndex="10405"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV138" runat="server" Columns="4" MaxLength="4" TabIndex="10406"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV139" runat="server" Columns="4" MaxLength="4" TabIndex="10407"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV140" runat="server" Columns="4" MaxLength="4" TabIndex="10408"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV141" runat="server" Columns="4" MaxLength="4" TabIndex="10409"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV142" runat="server" Columns="4" MaxLength="4" TabIndex="10410"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV143" runat="server" Columns="4" MaxLength="4" TabIndex="10411"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV144" runat="server" Columns="4" MaxLength="4" TabIndex="10412"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV145" runat="server" Columns="4" MaxLength="4" TabIndex="10413"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="padding-bottom: 10px; width: 63px; height: 26px; text-align: center" colspan="2">
                    <asp:Label ID="lblAprobados1oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV146" runat="server" Columns="4" MaxLength="4" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV147" runat="server" Columns="4" MaxLength="4" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV148" runat="server" Columns="4" MaxLength="4" TabIndex="10503"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV149" runat="server" Columns="4" MaxLength="4" TabIndex="10504"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV150" runat="server" Columns="4" MaxLength="4" TabIndex="10505"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV151" runat="server" Columns="4" MaxLength="4" TabIndex="10506"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV152" runat="server" Columns="4" MaxLength="4" TabIndex="10507"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV153" runat="server" Columns="4" MaxLength="4" TabIndex="10508"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV154" runat="server" Columns="4" MaxLength="4" TabIndex="10509"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV155" runat="server" Columns="4" MaxLength="4" TabIndex="10510"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV156" runat="server" Columns="4" MaxLength="4" TabIndex="10511"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV157" runat="server" Columns="4" MaxLength="4" TabIndex="10512"></asp:TextBox></td>
                <td style="text-align: center; padding-bottom: 10px; width: 63px; height: 26px;">
                    <asp:TextBox ID="txtV158" runat="server" Columns="4" MaxLength="4" TabIndex="10513"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="5" style="text-align: center">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o. (sem. 3 y 4)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcion2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack3" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" onkeydown="return Arrows(event,this.tabIndex)" Enabled="true" MaxLength="4" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV159" runat="server" Columns="4" MaxLength="4" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV160" runat="server" Columns="4" MaxLength="4" TabIndex="10603"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV161" runat="server" Columns="4" MaxLength="4" TabIndex="10604"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV162" runat="server" Columns="4" MaxLength="4" TabIndex="10605"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV163" runat="server" Columns="4" MaxLength="4" TabIndex="10606"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV164" runat="server" Columns="4" MaxLength="4" TabIndex="10607"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV165" runat="server" Columns="4" MaxLength="4" TabIndex="10608"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV166" runat="server" Columns="4" MaxLength="4" TabIndex="10609"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV167" runat="server" Columns="4" MaxLength="4" TabIndex="10610"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV168" runat="server" Columns="4" MaxLength="4" TabIndex="10611"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV169" runat="server" Columns="4" MaxLength="4" TabIndex="10612"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV170" runat="server" Columns="4" MaxLength="4" TabIndex="10613"></asp:TextBox></td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="lblExistencia2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack4" runat="server" BackColor="Silver" BorderColor="Silver" TabIndex="10701"
                        BorderStyle="Solid" Columns="4" onkeydown="return Arrows(event,this.tabIndex)" Enabled="true" MaxLength="4"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV171" runat="server" Columns="4" MaxLength="4" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV172" runat="server" Columns="4" MaxLength="4" TabIndex="10703"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV173" runat="server" Columns="4" MaxLength="4" TabIndex="10704"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV174" runat="server" Columns="4" MaxLength="4" TabIndex="10705"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV175" runat="server" Columns="4" MaxLength="4" TabIndex="10706"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV176" runat="server" Columns="4" MaxLength="4" TabIndex="10707"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV177" runat="server" Columns="4" MaxLength="4" TabIndex="10708"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV178" runat="server" Columns="4" MaxLength="4" TabIndex="10709"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV179" runat="server" Columns="4" MaxLength="4" TabIndex="10710"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV180" runat="server" Columns="4" MaxLength="4" TabIndex="10711"></asp:TextBox></td>
                 <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV181" runat="server" Columns="4" MaxLength="4" TabIndex="10712"></asp:TextBox></td>
                 <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV182" runat="server" Columns="4" MaxLength="4" TabIndex="10713"></asp:TextBox></td>
            </tr> 
             <tr>
                <td rowspan="2">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcion2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack" runat="server" BackColor="Silver" BorderColor="Silver" TabIndex="10801"
                        BorderStyle="Solid" Columns="4" onkeydown="return Arrows(event,this.tabIndex)" Enabled="true" MaxLength="4"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV183" runat="server" Columns="4" MaxLength="4" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV184" runat="server" Columns="4" MaxLength="4" TabIndex="10803"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV185" runat="server" Columns="4" MaxLength="4" TabIndex="10804"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV186" runat="server" Columns="4" MaxLength="4" TabIndex="10805"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV187" runat="server" Columns="4" MaxLength="4" TabIndex="10806"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV188" runat="server" Columns="4" MaxLength="4" TabIndex="10807"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV189" runat="server" Columns="4" MaxLength="4" TabIndex="10808"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV190" runat="server" Columns="4" MaxLength="4" TabIndex="10809"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV191" runat="server" Columns="4" MaxLength="4" TabIndex="10810"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV192" runat="server" Columns="4" MaxLength="4" TabIndex="10811"></asp:TextBox></td>
                 <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV193" runat="server" Columns="4" MaxLength="4" TabIndex="10812"></asp:TextBox></td>
                 <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV194" runat="server" Columns="4" MaxLength="4" TabIndex="10813"></asp:TextBox></td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="lblExistencia2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack6" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV195" runat="server" Columns="4" MaxLength="4" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV196" runat="server" Columns="4" MaxLength="4" TabIndex="10903"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV197" runat="server" Columns="4" MaxLength="4" TabIndex="10904"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV198" runat="server" Columns="4" MaxLength="4" TabIndex="10905"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV199" runat="server" Columns="4" MaxLength="4" TabIndex="10906"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV200" runat="server" Columns="4" MaxLength="4" TabIndex="10907"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV201" runat="server" Columns="4" MaxLength="4" TabIndex="10908"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV202" runat="server" Columns="4" MaxLength="4" TabIndex="10909"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV203" runat="server" Columns="4" MaxLength="4" TabIndex="10910"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV204" runat="server" Columns="4" MaxLength="4" TabIndex="10911"></asp:TextBox></td>
                 <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV205" runat="server" Columns="4" MaxLength="4" TabIndex="10912"></asp:TextBox></td>
                 <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV206" runat="server" Columns="4" MaxLength="4" TabIndex="10913"></asp:TextBox></td>
            </tr>
             <tr>
                <td style="padding-bottom: 10px" colspan="2">
                    <asp:Label ID="lblAprobados2oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtVblack7" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="11001"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV207" runat="server" Columns="4" MaxLength="4" TabIndex="11002"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV208" runat="server" Columns="4" MaxLength="4" TabIndex="11003"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV209" runat="server" Columns="4" MaxLength="4" TabIndex="11004"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV210" runat="server" Columns="4" MaxLength="4" TabIndex="11005"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV211" runat="server" Columns="4" MaxLength="4" TabIndex="11006"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV212" runat="server" Columns="4" MaxLength="4" TabIndex="11007"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV213" runat="server" Columns="4" MaxLength="4" TabIndex="11008"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV214" runat="server" Columns="4" MaxLength="4" TabIndex="11009"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV215" runat="server" Columns="4" MaxLength="4" TabIndex="11010"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV216" runat="server" Columns="4" MaxLength="4" TabIndex="11011"></asp:TextBox></td>
                 <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtV217" runat="server" Columns="4" MaxLength="4" TabIndex="11012"></asp:TextBox></td>
                 <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtV218" runat="server" Columns="4" MaxLength="4" TabIndex="11013"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="5" style="text-align: center">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o. (sem. 5 y 6)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcion3oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack8" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="11101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack08" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV219" runat="server" Columns="4" MaxLength="4" TabIndex="11103"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV220" runat="server" Columns="4" MaxLength="4" TabIndex="11104"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV221" runat="server" Columns="4" MaxLength="4" TabIndex="11105"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV222" runat="server" Columns="4" MaxLength="4" TabIndex="11106"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV223" runat="server" Columns="4" MaxLength="4" TabIndex="11107"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV224" runat="server" Columns="4" MaxLength="4" TabIndex="11108"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV225" runat="server" Columns="4" MaxLength="4" TabIndex="11109"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV226" runat="server" Columns="4" MaxLength="4" TabIndex="11110"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV227" runat="server" Columns="4" MaxLength="4" TabIndex="11111"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV228" runat="server" Columns="4" MaxLength="4" TabIndex="11112"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV229" runat="server" Columns="4" MaxLength="4" TabIndex="11113"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblExistencia3oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack9" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="11201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack0" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV230" runat="server" Columns="4" MaxLength="4" TabIndex="11203"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV231" runat="server" Columns="4" MaxLength="4" TabIndex="11204"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV232" runat="server" Columns="4" MaxLength="4" TabIndex="11205"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV233" runat="server" Columns="4" MaxLength="4" TabIndex="11206"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV234" runat="server" Columns="4" MaxLength="4" TabIndex="11207"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV235" runat="server" Columns="4" MaxLength="4" TabIndex="11208"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV236" runat="server" Columns="4" MaxLength="4" TabIndex="11209"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV237" runat="server" Columns="4" MaxLength="4" TabIndex="11210"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV238" runat="server" Columns="4" MaxLength="4" TabIndex="11211"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV239" runat="server" Columns="4" MaxLength="4" TabIndex="11212"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV240" runat="server" Columns="4" MaxLength="4" TabIndex="11213"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcion3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack10" runat="server" BackColor="Silver" BorderColor="Silver" TabIndex="11301"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" onkeydown="return Arrows(event,this.tabIndex)" Enabled="true" MaxLength="4"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVblack11" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" onkeydown="return Arrows(event,this.tabIndex)" Enabled="true" MaxLength="4" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV241" runat="server" Columns="4" MaxLength="4" TabIndex="11303"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV242" runat="server" Columns="4" MaxLength="4" TabIndex="11304"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV243" runat="server" Columns="4" MaxLength="4" TabIndex="11305"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV244" runat="server" Columns="4" MaxLength="4" TabIndex="11306"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV245" runat="server" Columns="4" MaxLength="4" TabIndex="11307"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV246" runat="server" Columns="4" MaxLength="4" TabIndex="11308"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV247" runat="server" Columns="4" MaxLength="4" TabIndex="11309"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV248" runat="server" Columns="4" MaxLength="4" TabIndex="11310"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV249" runat="server" Columns="4" MaxLength="4" TabIndex="11311"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV250" runat="server" Columns="4" MaxLength="4" TabIndex="11312"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV251" runat="server" Columns="4" MaxLength="4" TabIndex="11313"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblExistencia3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVaaa" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" TabIndex="11401" Enabled="true" MaxLength="4"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVaaaa" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" TabIndex="11402" Enabled="true" MaxLength="4"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV252" runat="server" Columns="4" MaxLength="4" TabIndex="11403"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV253" runat="server" Columns="4" MaxLength="4" TabIndex="11404"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV254" runat="server" Columns="4" MaxLength="4" TabIndex="11405"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV255" runat="server" Columns="4" MaxLength="4" TabIndex="11406"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV256" runat="server" Columns="4" MaxLength="4" TabIndex="11407"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV257" runat="server" Columns="4" MaxLength="4" TabIndex="11408"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV258" runat="server" Columns="4" MaxLength="4" TabIndex="11409"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV259" runat="server" Columns="4" MaxLength="4" TabIndex="11410"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV260" runat="server" Columns="4" MaxLength="4" TabIndex="11411"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV261" runat="server" Columns="4" MaxLength="4" TabIndex="11412"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV262" runat="server" Columns="4" MaxLength="4" TabIndex="11413"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="padding-bottom: 10px" colspan="2">
                    <asp:Label ID="lblAprobados3oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtVa" onkeydown="return Arrows(event,this.tabIndex)" TabIndex="11501" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtVas" onkeydown="return Arrows(event,this.tabIndex)" TabIndex="11502" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV263" runat="server" Columns="4" MaxLength="4" TabIndex="11503"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV264" runat="server" Columns="4" MaxLength="4" TabIndex="11504"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV265" runat="server" Columns="4" MaxLength="4" TabIndex="11505"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV266" runat="server" Columns="4" MaxLength="4" TabIndex="11506"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV267" runat="server" Columns="4" MaxLength="4" TabIndex="11507"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV268" runat="server" Columns="4" MaxLength="4" TabIndex="11508"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV269" runat="server" Columns="4" MaxLength="4" TabIndex="11509"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV270" runat="server" Columns="4" MaxLength="4" TabIndex="11510"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV271" runat="server" Columns="4" MaxLength="4" TabIndex="11511"></asp:TextBox></td>
                <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtV272" runat="server" Columns="4" MaxLength="4" TabIndex="11512"></asp:TextBox></td>
                <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtV273" runat="server" Columns="4" MaxLength="4" TabIndex="11513"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="5" style="text-align: center">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o. (sem. 7 y 8)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2">
                    <asp:Label ID="lblHom4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcion4oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVaa1" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11601"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVaa2" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11602"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVaa3" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11603"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV274" runat="server" Columns="4" MaxLength="4" TabIndex="11604"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV275" runat="server" Columns="4" MaxLength="4" TabIndex="11605"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV276" runat="server" Columns="4" MaxLength="4" TabIndex="11606"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV277" runat="server" Columns="4" MaxLength="4" TabIndex="11607"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV278" runat="server" Columns="4" MaxLength="4" TabIndex="11608"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV279" runat="server" Columns="4" MaxLength="4" TabIndex="11609"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV280" runat="server" Columns="4" MaxLength="4" TabIndex="11610"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV281" runat="server" Columns="4" MaxLength="4" TabIndex="11611"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV282" runat="server" Columns="4" MaxLength="4" TabIndex="11612"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV283" runat="server" Columns="4" MaxLength="4" TabIndex="11613"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblExistencia4oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVa1" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="11701"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVa2" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="11702"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVa3" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="true" ReadOnly="true" MaxLength="4" TabIndex="11703"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV284" runat="server" Columns="4" MaxLength="4" TabIndex="11704"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV285" runat="server" Columns="4" MaxLength="4" TabIndex="11705"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV286" runat="server" Columns="4" MaxLength="4" TabIndex="11706"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV287" runat="server" Columns="4" MaxLength="4" TabIndex="11707"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV288" runat="server" Columns="4" MaxLength="4" TabIndex="11708"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV289" runat="server" Columns="4" MaxLength="4" TabIndex="11709"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV290" runat="server" Columns="4" MaxLength="4" TabIndex="11710"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV291" runat="server" Columns="4" MaxLength="4" TabIndex="11711"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV292" runat="server" Columns="4" MaxLength="4" TabIndex="11712"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV293" runat="server" Columns="4" MaxLength="4" TabIndex="11713"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2">
                    <asp:Label ID="lblMuj4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcion4oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtVaz" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11801"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVaz1" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11802"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVaz2" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11803"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV294" runat="server" Columns="4" MaxLength="4" TabIndex="11804"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV295" runat="server" Columns="4" MaxLength="4" TabIndex="11805"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV296" runat="server" Columns="4" MaxLength="4" TabIndex="11806"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV297" runat="server" Columns="4" MaxLength="4" TabIndex="11807"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV298" runat="server" Columns="4" MaxLength="4" TabIndex="11808"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV299" runat="server" Columns="4" MaxLength="4" TabIndex="11809"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV300" runat="server" Columns="4" MaxLength="4" TabIndex="11810"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV301" runat="server" Columns="4" MaxLength="4" TabIndex="11811"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV302" runat="server" Columns="4" MaxLength="4" TabIndex="11812"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV303" runat="server" Columns="4" MaxLength="4" TabIndex="11813"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblExistencia4oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtVab" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11901"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVab1" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11902"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVab2" onkeydown="return Arrows(event,this.tabIndex)" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="11903"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV304" runat="server" Columns="4" MaxLength="4" TabIndex="11904"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV305" runat="server" Columns="4" MaxLength="4" TabIndex="11905"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV306" runat="server" Columns="4" MaxLength="4" TabIndex="11906"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV307" runat="server" Columns="4" MaxLength="4" TabIndex="11907"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV308" runat="server" Columns="4" MaxLength="4" TabIndex="11908"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV309" runat="server" Columns="4" MaxLength="4" TabIndex="11909"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV310" runat="server" Columns="4" MaxLength="4" TabIndex="11910"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV311" runat="server" Columns="4" MaxLength="4" TabIndex="11911"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV312" runat="server" Columns="4" MaxLength="4" TabIndex="11912"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV313" runat="server" Columns="4" MaxLength="4" TabIndex="11913"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="padding-bottom: 10px" colspan="2">
                    <asp:Label ID="lblAprobados4oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtVn" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="12001"></asp:TextBox></td>
                <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtVn1" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="12002"></asp:TextBox></td>
                <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtVn2" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="12003"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV314" runat="server" Columns="4" MaxLength="4" TabIndex="12004"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV315" runat="server" Columns="4" MaxLength="4" TabIndex="12005"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV316" runat="server" Columns="4" MaxLength="4" TabIndex="12006"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV317" runat="server" Columns="4" MaxLength="4" TabIndex="12007"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV318" runat="server" Columns="4" MaxLength="4" TabIndex="12008"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV319" runat="server" Columns="4" MaxLength="4" TabIndex="12009"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV320" runat="server" Columns="4" MaxLength="4" TabIndex="12010"></asp:TextBox></td>
                <td style="padding-bottom: 10px;">
                    <asp:TextBox ID="txtV321" runat="server" Columns="4" MaxLength="4" TabIndex="12011"></asp:TextBox></td>
                <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtV322" runat="server" Columns="4" MaxLength="4" TabIndex="12012"></asp:TextBox></td>
                <td style="padding-bottom: 10px">
                    <asp:TextBox ID="txtV323" runat="server" Columns="4" MaxLength="4" TabIndex="12013"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="5" style="text-align: center">
                    <asp:Label ID="lblTotalT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td rowspan="2">
                    <asp:Label ID="lblHomT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcionTH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV324" runat="server" Columns="4" MaxLength="4" TabIndex="12101"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV325" runat="server" Columns="4" MaxLength="4" TabIndex="12102"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV326" runat="server" Columns="4" MaxLength="4" TabIndex="12103"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV327" runat="server" Columns="4" MaxLength="4" TabIndex="12104"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV328" runat="server" Columns="4" MaxLength="4" TabIndex="12105"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV329" runat="server" Columns="4" MaxLength="4" TabIndex="12106"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV330" runat="server" Columns="4" MaxLength="4" TabIndex="12107"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV331" runat="server" Columns="4" MaxLength="4" TabIndex="12108"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV332" runat="server" Columns="4" MaxLength="4" TabIndex="12109"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV333" runat="server" Columns="4" MaxLength="4" TabIndex="12110"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV334" runat="server" Columns="4" MaxLength="4" TabIndex="12111"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV335" runat="server" Columns="4" MaxLength="4" TabIndex="12112"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV336" runat="server" Columns="4" MaxLength="4" TabIndex="12113"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblExistenciaTH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtVam" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="12201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV337" runat="server" Columns="4" MaxLength="4" TabIndex="12202"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV338" runat="server" Columns="4" MaxLength="4" TabIndex="12203"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV339" runat="server" Columns="4" MaxLength="4" TabIndex="12204"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV340" runat="server" Columns="4" MaxLength="4" TabIndex="12205"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV341" runat="server" Columns="4" MaxLength="4" TabIndex="12206"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV342" runat="server" Columns="4" MaxLength="4" TabIndex="12207"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV343" runat="server" Columns="4" MaxLength="4" TabIndex="12208"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV344" runat="server" Columns="4" MaxLength="4" TabIndex="12209"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV345" runat="server" Columns="4" MaxLength="4" TabIndex="12210"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV346" runat="server" Columns="4" MaxLength="4" TabIndex="12211"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV347" runat="server" Columns="4" MaxLength="4" TabIndex="12212"></asp:TextBox></td>
                <td style="width: 55px; text-align: center">
                    <asp:TextBox ID="txtV348" runat="server" Columns="4" MaxLength="4" TabIndex="12213"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2">
                    <asp:Label ID="lblMujT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblInscripcionTM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV349" runat="server" Columns="4" MaxLength="4" TabIndex="12301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV350" runat="server" Columns="4" MaxLength="4" TabIndex="12302"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV351" runat="server" Columns="4" MaxLength="4" TabIndex="12303"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV352" runat="server" Columns="4" MaxLength="4" TabIndex="12304"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV353" runat="server" Columns="4" MaxLength="4" TabIndex="12305"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV354" runat="server" Columns="4" MaxLength="4" TabIndex="12306"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV355" runat="server" Columns="4" MaxLength="4" TabIndex="12307"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV356" runat="server" Columns="4" MaxLength="4" TabIndex="12308"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV357" runat="server" Columns="4" MaxLength="4" TabIndex="12309"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV358" runat="server" Columns="4" MaxLength="4" TabIndex="12310"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV359" runat="server" Columns="4" MaxLength="4" TabIndex="12311"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV360" runat="server" Columns="4" MaxLength="4" TabIndex="12312"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV361" runat="server" Columns="4" MaxLength="4" TabIndex="12313"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="height: 30px">
                    <asp:Label ID="lblExistenciaTM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtVaj" runat="server" onkeydown="return Arrows(event,this.tabIndex)" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="true" MaxLength="4" TabIndex="12401"></asp:TextBox></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtV362" runat="server" Columns="4" MaxLength="4" TabIndex="12402"></asp:TextBox></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtV363" runat="server" Columns="4" MaxLength="4" TabIndex="12403"></asp:TextBox></td>
                <td style="text-align: center; width: 63px; height: 30px;">
                    <asp:TextBox ID="txtV364" runat="server" Columns="4" MaxLength="4" TabIndex="12404"></asp:TextBox></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtV365" runat="server" Columns="4" MaxLength="4" TabIndex="12405"></asp:TextBox></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtV366" runat="server" Columns="4" MaxLength="4" TabIndex="12406"></asp:TextBox></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtV367" runat="server" Columns="4" MaxLength="4" TabIndex="12407"></asp:TextBox></td>
                <td style="text-align: center; width: 63px; height: 30px;">
                    <asp:TextBox ID="txtV368" runat="server" Columns="4" MaxLength="4" TabIndex="12408"></asp:TextBox></td>
                <td style="text-align: center; width: 63px; height: 30px;">
                    <asp:TextBox ID="txtV369" runat="server" Columns="4" MaxLength="4" TabIndex="12409"></asp:TextBox></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtV370" runat="server" Columns="4" MaxLength="4" TabIndex="12410"></asp:TextBox></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtV371" runat="server" Columns="4" MaxLength="4" TabIndex="12411"></asp:TextBox></td>
                <td style="text-align: center; height: 30px;">
                    <asp:TextBox ID="txtV372" runat="server" Columns="4" MaxLength="4" TabIndex="12412"></asp:TextBox></td>
                <td style="text-align: center; width: 55px; height: 30px;">
                    <asp:TextBox ID="txtV373" runat="server" Columns="4" MaxLength="4" TabIndex="12413"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblAprobadosTM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV374" runat="server" Columns="4" MaxLength="4" TabIndex="12501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV375" runat="server" Columns="4" MaxLength="4" TabIndex="12502"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV376" runat="server" Columns="4" MaxLength="4" TabIndex="12503"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV377" runat="server" Columns="4" MaxLength="4" TabIndex="12504"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV378" runat="server" Columns="4" MaxLength="4" TabIndex="12505"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV379" runat="server" Columns="4" MaxLength="4" TabIndex="12506"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV380" runat="server" Columns="4" MaxLength="4" TabIndex="12507"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV381" runat="server" Columns="4" MaxLength="4" TabIndex="12508"></asp:TextBox></td>
                <td style="text-align: center; width: 63px;">
                    <asp:TextBox ID="txtV382" runat="server" Columns="4" MaxLength="4" TabIndex="12509"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV383" runat="server" Columns="4" MaxLength="4" TabIndex="12510"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV384" runat="server" Columns="4" MaxLength="4" TabIndex="12511"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV385" runat="server" Columns="4" MaxLength="4" TabIndex="12512"></asp:TextBox></td>
                <td style="text-align: center; width: 55px;">
                    <asp:TextBox ID="txtV386" runat="server" Columns="4" MaxLength="4" TabIndex="12513"></asp:TextBox></td>
            </tr>
            
          </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Egresados_911_7T',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Egresados_911_7T',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;</td>
                <td ><span  onclick="openPage('Planteles_911_7T',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Planteles_911_7T',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
