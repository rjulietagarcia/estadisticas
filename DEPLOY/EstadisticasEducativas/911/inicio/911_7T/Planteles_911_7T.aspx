<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7T(Planteles)" AutoEventWireup="true" CodeBehind="Planteles_911_7T.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7T.Planteles_911_7T" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

  
   <script type="text/javascript">
        MaxCol = 18;
        MaxRow = 28;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
    <table style="width:100%">
        
        <tr><td><span>BACHILLERATO TECNOLÓGICO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title="" ><span>IDENTIFICACIÓN</span></a></li><li onclick="openPage('Carreras_911_7T',true)"><a href="#" title="" ><span>ALUMNOS POR CARRERA</span></a></li><li onclick="openPage('Alumnos1_911_7T',true)"><a href="#" title="" ><span>TOTAL</span></a></li><li onclick="openPage('Egresados_911_7T',true)"><a href="#" title="" ><span>EGRESADOS</span></a></li><li onclick="openPage('Alumnos2_911_7T',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li><li onclick="openPage('Planteles_911_7T',true)"><a href="#" title="" class="activo" ><span>PLANTELES</span></a></li><li onclick="openPage('Personal_911_7T',false)"><a href="#" title="" ><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACIÓN</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opción SIGUIENTE 
    para continuar con la generación de las Estadísticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

<center>
           
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        &nbsp;<br />

        <table style="width: 600px">
            <tr>
                <td>
                    <table style="width: 550px;text-align:justify">
                        <tr>
                            <td>
                                <asp:Label ID="lblPLANTELES" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                                    Text="V. PLANTELES DE EXTENSIÓN" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px">
                                <asp:Label ID="lblInstruccionIV1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Si existen anexos (planteles de extensión), escriba el total de alumnos que atienden, desglosándolo por sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: center; width: 45px; height: 19px;">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                        Width="90px"></asp:Label></td>
                <td rowspan="1" style="text-align: center; height: 19px;">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEMESTRE"
                        Width="90px"></asp:Label></td><td style="text-align: center">
                            <asp:Label ID="lblInsTotal" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="MUJERES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 45px;">
                    <asp:Label ID="lblG1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 y 2"
                        Width="100%"></asp:Label></td><td style="text-align: center">
                            <asp:TextBox ID="txtV387" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                                TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV388" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV389" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10103"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 45px;">
                    <asp:Label ID="lblG2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; height: 26px;">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 y 4"
                        Width="100%"></asp:Label></td><td style="text-align: center">
                            <asp:TextBox ID="txtV390" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                                TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV391" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV392" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 45px;">
                    <asp:Label ID="lblG3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 y 6"
                        Width="100%"></asp:Label></td><td style="text-align: center">
                            <asp:TextBox ID="txtV393" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                                TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV394" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV395" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 45px;">
                    <asp:Label ID="lblG4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 y 8"
                        Width="100%"></asp:Label></td><td style="text-align: center">
                            <asp:TextBox ID="txtV396" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                                TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV397" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV398" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right" colspan="2" rowspan="2">
                    <asp:Label ID="lblTotalT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV399" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV400" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV401" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10503"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccionIV2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Escriba, por grado, los grupos existentes."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td style="text-align: center">
                    <asp:Label ID="lblGrupos2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o. (1o. y 2o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV402" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o. (3o. y 4o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV403" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o. (5o. y 6o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV404" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o. (7o. y 8o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV405" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalGrupos" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV406" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblInstruccionIV3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Escriba el personal docente que atiende a los alumnos de extensión, desglosándolo por sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table style="height: 85px">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblHombres" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV407" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMujeres" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV408" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalPersonal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV409" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Alumnos2_911_7T',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir página previa" /></a></span></td> 
                <td ><span  onclick="openPage('Alumnos2_911_7T',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;</td>
                <td ><span  onclick="openPage('Personal_911_7T',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_7T',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir página siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando información por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
                  GetTabIndexes();
        </script> 
</asp:Content>
