<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7G(Planteles y Aulas)" AutoEventWireup="true" CodeBehind="Planteles_911_7G.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7G.Planteles_911_7G" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

<div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">

   <table style="width:100%">
        <tr><td><span>BACHILLERATO GENERAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
   
    </div></div>
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7G',true)"><a href="#" title=""><span>IDENTIFICACIÓN</span></a></li>
        <li onclick="openPage('Alumnos1_911_7G',true)"><a href="#" title=""><span>ALUMNOS</span></a></li>
        <li onclick="openPage('Alumnos2_911_7G',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Egresados_911_7G',true)"><a href="#" title=""><span>EGRESADOS</span></a></li>
        <li onclick="openPage('Planteles_911_7G',true)"><a href="#" title="" class="activo"><span>PLANTELES</span></a></li>
        <li onclick="openPage('Personal_911_7G',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACIÓN</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>



        <table style="width: 600px">
            <tr>
                <td>
                    <table style="width: 500px" align="center">
                        <tr>
                            <td style="text-align:justify">
                                <asp:Label ID="lblPLANTELES" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                                    Text="IV. PLANTELES DE EXTENSIÓN" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px">
                                <asp:Label ID="lblInstruccionIV1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Si existen anexos (planteles de extensión), escriba el total de alumnos que atienden, desglosándolo por sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: center; width: 45px; height: 19px;">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                        Width="90px"></asp:Label></td>
                <td rowspan="1" style="text-align: center; height: 19px;">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEMESTRE"
                        Width="90px"></asp:Label></td><td style="text-align: center">
                            <asp:Label ID="lblInsTotal" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="MUJERES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 45px;">
                    <asp:Label ID="lblG1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 y 2"
                        Width="100%"></asp:Label></td><td style="text-align: center">
                            <asp:TextBox ID="txtV414" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                                TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV415" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV416" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10103"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 45px;">
                    <asp:Label ID="lblG2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; height: 26px;">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 y 4"
                        Width="100%"></asp:Label></td><td style="text-align: center">
                            <asp:TextBox ID="txtV417" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                                TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV418" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV419" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 45px;">
                    <asp:Label ID="lblG3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 y 6"
                        Width="100%"></asp:Label></td><td style="text-align: center">
                            <asp:TextBox ID="txtV420" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                                TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV421" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV422" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 45px;">
                    <asp:Label ID="lblG4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 y 8"
                        Width="100%"></asp:Label></td><td style="text-align: center">
                            <asp:TextBox ID="txtV423" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                                TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV424" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV425" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right" colspan="2" rowspan="2">
                    <asp:Label ID="lblTotalT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV426" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV427" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV428" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="10503"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccionIV2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Escriba, por grado, los grupos existentes."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td style="text-align: center">
                    <asp:Label ID="lblGrupos2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o. (1o. y 2o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV429" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o. (3o. y 4o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV430" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o. (5o. y 6o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV431" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o. (7o. y 8o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV432" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalGrupos" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV433" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblInstruccionIV3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Escriba el personal docente que atiende a los alumnos de extensión, desglosándolo por sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; height: 104px;">
        <table style="height: 85px">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblHombres" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV434" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMujeres" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV435" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalPersonal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV436" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Egresados_911_7G',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir página previa" /></a></span></td> 
                <td ><span  onclick="openPage('Egresados_911_7G',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Personal_911_7G',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_7G',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir página siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando información por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 16;
                MaxRow = 31;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
      
</asp:Content>
