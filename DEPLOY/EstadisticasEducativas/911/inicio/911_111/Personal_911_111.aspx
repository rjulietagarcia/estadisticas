<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.111(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_111.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_111.Personal_911_111" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    
    <div id="logo"></div>
    <div style="min-width:1250px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        
        <tr><td><span>EDUCACI�N PREESCOLAR IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1250px;">
       <ul class="left">
        <li onclick="openPage('Identificacion_911_111',true)">  <a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AYG_911_111',true)">             <a href="#" title="" ><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('LMD_911_111',true)">             <a href="#" title="" ><span>LENGUA MATERNA</span></a></li>
        <li onclick="openPage('Personal_911_111',true)">        <a href="#" title="" class="activo"><span>PERSONAL POR FUNCION</span></a></li>
        <li onclick="openPage('CMYA_911_111',false)">           <a href="#" title="" ><span>CARRERA Y AULAS</span></a></li>
        <li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>OFICIALIZACION</span></a></li>
        
        </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>  
            

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

        <table style="width:800px">
            <tr>
                <td >
                    <table >
                        <tr>
                            <td colspan="12" style="text-align:justify;"  >
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="III. PERSONAL POR FUNCI�N"
                                     Font-Size="16px"></asp:Label><br />
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="12" style="text-align:justify;" >
                                <br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de personal que realiza funciones de directivo (con y sin grupo), docente, promotores, y administrativo, auxiliares y de servicios, independientemente de su nombramiento, tipo y fuente de pago, desglos�ndolos seg�n su funci�n, nivel m�ximo de estudios y sexo."
                                   ></asp:Label><br />
                                <br />
                                <asp:Label ID="lblNota1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Notas:"
                                    ></asp:Label><br />
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="a) Si una persona desempe�a dos o m�s funciones an�tela en aquella a la que dedique m�s tiempo"
                                   ></asp:Label><br />
                                <asp:Label ID="lblNota3" runat="server" CssClass="lblRojo" Text="b) Si en la tabla corresponde al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente o en OTROS"
                                    ></asp:Label><br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="4"    style="text-align:center;"   >
                                <asp:Label  runat="server" CssClass="lblRojo" Width="150px"  Text="PERSONAL DIRECTIVO" ></asp:Label></td>
                            <td colspan="2"   rowspan="2">
                                <asp:Label  runat="server" CssClass="lblRojo" Width="150px" Text="PERSONAL DOCENTE" ></asp:Label></td>
                            
                            <td colspan="2"   rowspan="2" >
                                <asp:Label  runat="server" CssClass="lblRojo" Width="150px" Text="PROMOTORES" ></asp:Label></td>
                            <td colspan="2"   rowspan="2" >
                                <asp:Label  runat="server" CssClass="lblRojo" Width="150px" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS" ></asp:Label>
                            </td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2"   >
                                <asp:Label  runat="server" CssClass="lblRojo" Text="CON GRUPO" ></asp:Label></td>
                            <td colspan="2"    >
                                <asp:Label  runat="server" CssClass="lblRojo" Text="SIN GRUPO" ></asp:Label></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td  >
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="NIVEL EDUCATIVO" ></asp:Label></td>
                            
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="HOMBRES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="MUJERES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="HOMBRES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="MUJERES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="HOMBRES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="MUJERES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="HOMBRES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="MUJERES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="HOMBRES"></asp:Label></td>
                            <td   >
                                <asp:Label  runat="server" CssClass="lblGrisTit"  Text="MUJERES"></asp:Label></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA" Width="200px"></asp:Label></td>
                            <td  >
                                <asp:TextBox ID="txtV130" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10101"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV131" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10102"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV132" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10103"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV133" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10104"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV134" runat="server" Columns="2" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="10105"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV135" runat="server" Columns="2" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="10106"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV136" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10107"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV137" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10108"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV138" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10109"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV139" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10110"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="PRIMARIA TERMINADA" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV140" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10201"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV141" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10202"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV142" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10203"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV143" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10204"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV144" runat="server" Columns="2" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="10205"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV145" runat="server" Columns="2" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="10206"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV146" runat="server" Columns="2" TabIndex="10207" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV147" runat="server" Columns="2" TabIndex="10208" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV148" runat="server" Columns="2" TabIndex="10209" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV149" runat="server" Columns="2" TabIndex="10210" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                            <asp:Label  runat="server" CssClass="lblGrisTit" Text="SECUNDARIA INCOMPLETA" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV150" runat="server" Columns="2" TabIndex="10301" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV151" runat="server" Columns="2" TabIndex="10302" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV152" runat="server" Columns="2" TabIndex="10303" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV153" runat="server" Columns="2" TabIndex="10304" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV154" runat="server" Columns="2" TabIndex="10305" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV155" runat="server" Columns="2" TabIndex="10306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV156" runat="server" Columns="2" TabIndex="10307" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV157" runat="server" Columns="2" TabIndex="10308" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV158" runat="server" Columns="2" TabIndex="10309" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV159" runat="server" Columns="2" TabIndex="10310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="SECUNDARIA TERMINADA" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV160" runat="server" Columns="2" TabIndex="10401" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV161" runat="server" Columns="2" TabIndex="10402" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV162" runat="server" Columns="2" TabIndex="10403" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV163" runat="server" Columns="2" TabIndex="10404" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV164" runat="server" Columns="2" TabIndex="10405" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV165" runat="server" Columns="2" TabIndex="10406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV166" runat="server" Columns="2" TabIndex="10407" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV167" runat="server" Columns="2" TabIndex="10408" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV168" runat="server" Columns="2" TabIndex="10409" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV169" runat="server" Columns="2" TabIndex="10410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="PROFESIONAL T�CNICO" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV170" runat="server" Columns="2" TabIndex="10501" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV171" runat="server" Columns="2" TabIndex="10502" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV172" runat="server" Columns="2" TabIndex="10503" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV173" runat="server" Columns="2" TabIndex="10504" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV174" runat="server" Columns="2" TabIndex="10505" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV175" runat="server" Columns="2" TabIndex="10506" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV176" runat="server" Columns="2" TabIndex="10507" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV177" runat="server" Columns="2" TabIndex="10508" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV178" runat="server" Columns="2" TabIndex="10509" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV179" runat="server" Columns="2" TabIndex="10510" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="BACHILLERATO INCOMPLETO" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV180" runat="server" Columns="2" TabIndex="10601" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV181" runat="server" Columns="2" TabIndex="10602" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV182" runat="server" Columns="2" TabIndex="10603" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV183" runat="server" Columns="2" TabIndex="10604" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV184" runat="server" Columns="2" TabIndex="10605" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV185" runat="server" Columns="2" TabIndex="10606" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV186" runat="server" Columns="2" TabIndex="10607" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV187" runat="server" Columns="2" TabIndex="10608" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV188" runat="server" Columns="2" TabIndex="10609" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV189" runat="server" Columns="2" TabIndex="10610" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="BACHILLERATO TERMINADO" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV190" runat="server" Columns="2" TabIndex="10701" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV191" runat="server" Columns="2" TabIndex="10702" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV192" runat="server" Columns="2" TabIndex="10703" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV193" runat="server" Columns="2" TabIndex="10704" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV194" runat="server" Columns="2" TabIndex="10705" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV195" runat="server" Columns="2" TabIndex="10706" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV196" runat="server" Columns="2" TabIndex="10707" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV197" runat="server" Columns="2" TabIndex="10708" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV198" runat="server" Columns="2" TabIndex="10709" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV199" runat="server" Columns="2" TabIndex="10710" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="NORMAL PREESCOLAR INCOMPLETA" Width="230px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV200" runat="server" Columns="2" TabIndex="10801" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV201" runat="server" Columns="2" TabIndex="10802" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV202" runat="server" Columns="2" TabIndex="10803" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV203" runat="server" Columns="2" TabIndex="10804" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV204" runat="server" Columns="2" TabIndex="10805" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV205" runat="server" Columns="2" TabIndex="10806" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV206" runat="server" Columns="2" TabIndex="10807" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV207" runat="server" Columns="2" TabIndex="10808" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV208" runat="server" Columns="2" TabIndex="10809" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV209" runat="server" Columns="2" TabIndex="10810" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="NORMAL PREESCOLAR TERMINADA" Width="230px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV210" runat="server" Columns="2" TabIndex="10901" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV211" runat="server" Columns="2" TabIndex="10902" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV212" runat="server" Columns="2" TabIndex="10903" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV213" runat="server" Columns="2" TabIndex="10904" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV214" runat="server" Columns="2" TabIndex="10905" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV215" runat="server" Columns="2" TabIndex="10906" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV216" runat="server" Columns="2" TabIndex="10907" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV217" runat="server" Columns="2" TabIndex="10908" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV218" runat="server" Columns="2" TabIndex="10909" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV219" runat="server" Columns="2" TabIndex="10910" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="NORMAL PRIMARIA INCOMPLETA" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV220" runat="server" Columns="2" TabIndex="11001" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV221" runat="server" Columns="2" TabIndex="11002" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV222" runat="server" Columns="2" TabIndex="11003" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV223" runat="server" Columns="2" TabIndex="11004" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV224" runat="server" Columns="2" TabIndex="11005" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV225" runat="server" Columns="2" TabIndex="11006" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV226" runat="server" Columns="2" TabIndex="11007" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV227" runat="server" Columns="2" TabIndex="11008" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV228" runat="server" Columns="2" TabIndex="11009" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV229" runat="server" Columns="2" TabIndex="11010" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="NORMAL PRIMARIA TERMINADA" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV230" runat="server" Columns="2" TabIndex="11101" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV231" runat="server" Columns="2" TabIndex="11102" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV232" runat="server" Columns="2" TabIndex="11103" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV233" runat="server" Columns="2" TabIndex="11104" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV234" runat="server" Columns="2" TabIndex="11105" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV235" runat="server" Columns="2" TabIndex="11106" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV236" runat="server" Columns="2" TabIndex="11107" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV237" runat="server" Columns="2" TabIndex="11108" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV238" runat="server" Columns="2" TabIndex="11109" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV239" runat="server" Columns="2" TabIndex="11110" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR INCOMPLETA" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV240" runat="server" Columns="2" TabIndex="11201" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV241" runat="server" Columns="2" TabIndex="11202" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV242" runat="server" Columns="2" TabIndex="11203" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV243" runat="server" Columns="2" TabIndex="11204" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV244" runat="server" Columns="2" TabIndex="11205" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV245" runat="server" Columns="2" TabIndex="11206" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV246" runat="server" Columns="2" TabIndex="11207" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV247" runat="server" Columns="2" TabIndex="11208" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV248" runat="server" Columns="2" TabIndex="11209" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV249" runat="server" Columns="2" TabIndex="11210" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR, PASANTE" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV250" runat="server" Columns="2" TabIndex="11301" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV251" runat="server" Columns="2" TabIndex="11302" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV252" runat="server" Columns="2" TabIndex="11303" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV253" runat="server" Columns="2" TabIndex="11304" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV254" runat="server" Columns="2" TabIndex="11305" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV255" runat="server" Columns="2" TabIndex="11306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV256" runat="server" Columns="2" TabIndex="11307" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV257" runat="server" Columns="2" TabIndex="11308" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV258" runat="server" Columns="2" TabIndex="11309" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV259" runat="server" Columns="2" TabIndex="11310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR, TITULADO" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV260" runat="server" Columns="2" TabIndex="11401" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV261" runat="server" Columns="2" TabIndex="11402" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV262" runat="server" Columns="2" TabIndex="11403" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV263" runat="server" Columns="2" TabIndex="11404" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV264" runat="server" Columns="2" TabIndex="11405" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV265" runat="server" Columns="2" TabIndex="11406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV266" runat="server" Columns="2" TabIndex="11407" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV267" runat="server" Columns="2" TabIndex="11408" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV268" runat="server" Columns="2" TabIndex="11409" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV269" runat="server" Columns="2" TabIndex="11410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="LICENCIATURA INCOMPLETA" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV270" runat="server" Columns="2" TabIndex="11501" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV271" runat="server" Columns="2" TabIndex="11502" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV272" runat="server" Columns="2" TabIndex="11503" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV273" runat="server" Columns="2" TabIndex="11504" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV274" runat="server" Columns="2" TabIndex="11505" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV275" runat="server" Columns="2" TabIndex="11506" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV276" runat="server" Columns="2" TabIndex="11507" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV277" runat="server" Columns="2" TabIndex="11508" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV278" runat="server" Columns="2" TabIndex="11509" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV279" runat="server" Columns="2" TabIndex="11510" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, PASANTE" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV280" runat="server" Columns="2" TabIndex="11601" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV281" runat="server" Columns="2" TabIndex="11602" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV282" runat="server" Columns="2" TabIndex="11603" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV283" runat="server" Columns="2" TabIndex="11604" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV284" runat="server" Columns="2" TabIndex="11605" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV285" runat="server" Columns="2" TabIndex="11606" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV286" runat="server" Columns="2" TabIndex="11607" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV287" runat="server" Columns="2" TabIndex="11608" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV288" runat="server" Columns="2" TabIndex="11609" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV289" runat="server" Columns="2" TabIndex="11610" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                            <asp:Label  runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV290" runat="server" Columns="2" TabIndex="11701" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV291" runat="server" Columns="2" TabIndex="11702" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV292" runat="server" Columns="2" TabIndex="11703" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV293" runat="server" Columns="2" TabIndex="11704" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV294" runat="server" Columns="2" TabIndex="11705" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV295" runat="server" Columns="2" TabIndex="11706" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV296" runat="server" Columns="2" TabIndex="11707" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV297" runat="server" Columns="2" TabIndex="11708" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV298" runat="server" Columns="2" TabIndex="11709" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV299" runat="server" Columns="2" TabIndex="11710" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="MAESTR�A INCOMPLETA" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV300" runat="server" Columns="2" TabIndex="11801" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV301" runat="server" Columns="2" TabIndex="11802" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV302" runat="server" Columns="2" TabIndex="11803" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV303" runat="server" Columns="2" TabIndex="11804" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV304" runat="server" Columns="2" TabIndex="11805" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV305" runat="server" Columns="2" TabIndex="11806" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV306" runat="server" Columns="2" TabIndex="11807" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV307" runat="server" Columns="2" TabIndex="11808" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV308" runat="server" Columns="2" TabIndex="11809" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV309" runat="server" Columns="2" TabIndex="11810" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="MAESTR�A, GRADUADO" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV310" runat="server" Columns="2" TabIndex="11901" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV311" runat="server" Columns="2" TabIndex="11902" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV312" runat="server" Columns="2" TabIndex="11903" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV313" runat="server" Columns="2" TabIndex="11904" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV314" runat="server" Columns="2" TabIndex="11905" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV315" runat="server" Columns="2" TabIndex="11906" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV316" runat="server" Columns="2" TabIndex="11907" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV317" runat="server" Columns="2" TabIndex="11908" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV318" runat="server" Columns="2" TabIndex="11909" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV319" runat="server" Columns="2" TabIndex="11910" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="DOCTORADO INCOMPLETO" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV320" runat="server" Columns="2" TabIndex="12001" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV321" runat="server" Columns="2" TabIndex="12002" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV322" runat="server" Columns="2" TabIndex="12003" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV323" runat="server" Columns="2" TabIndex="12004" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV324" runat="server" Columns="2" TabIndex="12005" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV325" runat="server" Columns="2" TabIndex="12006" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV326" runat="server" Columns="2" TabIndex="12007" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV327" runat="server" Columns="2" TabIndex="12008" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV328" runat="server" Columns="2" TabIndex="12009" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV329" runat="server" Columns="2" TabIndex="12010" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="DOCTORADO, GRADUADO" Width="200px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV330" runat="server" Columns="2" TabIndex="12101" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV331" runat="server" Columns="2" TabIndex="12102" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV332" runat="server" Columns="2" TabIndex="12103" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV333" runat="server" Columns="2" TabIndex="12104" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV334" runat="server" Columns="2" TabIndex="12105" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV335" runat="server" Columns="2" TabIndex="12106" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV336" runat="server" Columns="2" TabIndex="12107" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV337" runat="server" Columns="2" TabIndex="12108" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV338" runat="server" Columns="2" TabIndex="12109" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV339" runat="server" Columns="2" TabIndex="12110" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   colspan="11" style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="OTROS*"></asp:Label>
                                <br />
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE"></asp:Label></td>
                           
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify; width: 230px;">
                                <asp:TextBox ID="txtV340" runat="server" Columns="30" TabIndex="12201" MaxLength="30" CssClass="lblNegro" Width="200px"></asp:TextBox>
                                
                                </td>
                            <td   >
                                <asp:TextBox ID="txtV341" runat="server" Columns="2" TabIndex="12202" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV342" runat="server" Columns="2" TabIndex="12203" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV343" runat="server" Columns="2" TabIndex="12204" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV344" runat="server" Columns="2" TabIndex="12205" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV345" runat="server" Columns="2" TabIndex="12206" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV346" runat="server" Columns="2" TabIndex="12207" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV347" runat="server" Columns="2" TabIndex="12208" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV348" runat="server" Columns="2" TabIndex="12209" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td    >
                                <asp:TextBox ID="txtV349" runat="server" Columns="2" TabIndex="12210" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV350" runat="server" Columns="2" TabIndex="12211" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify; width: 230px;">
                                <asp:TextBox ID="txtV351" runat="server" Columns="30" TabIndex="12301" MaxLength="30" CssClass="lblNegro" Width="200px"></asp:TextBox>
                                </td>
                            <td   >
                                <asp:TextBox ID="txtV352" runat="server" Columns="2" TabIndex="12302" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV353" runat="server" Columns="2" TabIndex="12303" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV354" runat="server" Columns="2" TabIndex="12304" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV355" runat="server" Columns="2" TabIndex="12305" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV356" runat="server" Columns="2" TabIndex="12306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV357" runat="server" Columns="2" TabIndex="12307" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV358" runat="server" Columns="2" TabIndex="12308" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV359" runat="server" Columns="2" TabIndex="12309" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV360" runat="server" Columns="2" TabIndex="12310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV361" runat="server" Columns="2" TabIndex="12311" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify; width: 230px;">
                                <asp:TextBox ID="txtV362" runat="server" Columns="30" TabIndex="12401" MaxLength="30" CssClass="lblNegro" Width="200px"></asp:TextBox>
                                
                            </td>
                            <td   >
                                <asp:TextBox ID="txtV363" runat="server" Columns="2" TabIndex="12402" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV364" runat="server" Columns="2" TabIndex="12403" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV365" runat="server" Columns="2" TabIndex="12404" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV366" runat="server" Columns="2" TabIndex="12405" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV367" runat="server" Columns="2" TabIndex="12406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV368" runat="server" Columns="2" TabIndex="12407" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV369" runat="server" Columns="2" TabIndex="12408" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV370" runat="server" Columns="2" TabIndex="12409" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV371" runat="server" Columns="2" TabIndex="12410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV372" runat="server" Columns="2" TabIndex="12411" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td   style="text-align:justify;">
                                <asp:Label  runat="server" CssClass="lblGrisTit" Text="SUBTOTALES"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV373" runat="server" Columns="2" TabIndex="12501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV374" runat="server" Columns="2" TabIndex="12502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV375" runat="server" Columns="2" TabIndex="12503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV376" runat="server" Columns="2" TabIndex="12504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV377" runat="server" Columns="2" TabIndex="12505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV378" runat="server" Columns="2" TabIndex="12506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV379" runat="server" Columns="2" TabIndex="12507" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV380" runat="server" Columns="2" TabIndex="12508" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV381" runat="server" Columns="2" TabIndex="12509" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                                <asp:TextBox ID="txtV382" runat="server" Columns="2" TabIndex="12510" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                            &nbsp;
                            </td>
                        </tr>
                        <tr >
                            <td   colspan="10" style="text-align:right;" >
                        
                               <asp:Label ID="Label1000"  runat="server" CssClass="lblRojo" Text="TOTAL PERSONAL (Suma de subtotales)"
                                    Width="300px"></asp:Label></td>
                            <td   >
                                <asp:TextBox ID="txtV383" runat="server" Columns="2" TabIndex="20101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td   >
                            &nbsp;
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>

        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('LMD_911_111',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('LMD_911_111',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                    <center>
                        &nbsp;</center>
                </td>
                <td ><span  onclick="openPage('CMYA_911_111',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('CMYA_911_111',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" />
        <input type="Hidden" id="hdnInm" runat="server" value="" />
        <input type="Hidden" id="hdnCct" runat="server" value=""  />
        <input type="Hidden" id="hdnTur" runat="server" value="" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 18;
                MaxRow = 28;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
        </script> 
</asp:Content>
