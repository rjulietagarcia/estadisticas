using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
using EstadisticasEducativas._911;

namespace EstadisticasEducativasInicio._911.inicio._911_1
{
    public partial class Oficializacion_911_1 : System.Web.UI.Page
    {
        ArrayList listaVariablesDP = new ArrayList();
        ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
        ControlDP controlDP = null;

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                 controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                if (controlDP != null)
                {
                    ObservacionesDP observacionDP = wsEstadisiticas.LeerObservacion(controlDP.ID_Control);
                    txtObservaciones.Text = observacionDP.Observacion;

                    this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";

                    LlenarMotivos();
                    EstadoBotones(controlDP);

                    if (controlDP.Estatus == 0)
                        pnlOficializado.Visible = false;
                    else
                        pnlOficializado.Visible = true;
                }
            }

        }
        private void LlenarMotivos()
        {
            ddlMotivos.Items.Add(new ListItem("Seleccionar ...", "0"));
            ddlMotivos.Items.Add(new ListItem("1.- La escuela est� en proceso de clausura", "1"));
            ddlMotivos.Items.Add(new ListItem("2.- Falta de personal Docente", "2"));
            ddlMotivos.Items.Add(new ListItem("3.- Falta de alumnos", "3"));
            ddlMotivos.Items.Add(new ListItem("4.- Incumplimiento del Director", "4"));
            ddlMotivos.Items.Add(new ListItem("5.- Escuelas de nueva creaci�n", "5"));
            ddlMotivos.Items.Add(new ListItem("6.- Causa administrativa", "6"));
            ddlMotivos.Items.Add(new ListItem("7.- No corresponde la fecha de levantamiento con el inicio de cursos de la escuela", "7"));
            ddlMotivos.Items.Add(new ListItem("8.- Compactaci�n de turno", "8"));
            ddlMotivos.Items.Add(new ListItem("9.- Cambio de turno", "9"));
            //10.- Cerrado
        }
        private void EstadoBotones(ControlDP controlDP)
        {
            if (controlDP.Estatus == 0)
            {
                cmdOficializar.Enabled = true;
                cmdGenerarComprobante.Enabled = false;
                cmdImprimircuestionario.Enabled = true;
                cmdImprimirCuestionario_Lleno.Enabled = false; // ??
                cmdGuardarObs.Enabled = true;
                ddlMotivos.Enabled = true;
            }
            else
            {
                cmdOficializar.Enabled = false;
                ddlMotivos.Enabled = false;
                cmdGenerarComprobante.Enabled = true;
                cmdImprimirCuestionario_Lleno.Enabled = true;
                cmdImprimircuestionario.Enabled = true; // ??
                cmdGuardarObs.Enabled = false;
                if (controlDP.Estatus != 10)
                    ddlMotivos.SelectedValue = controlDP.Estatus.ToString();
            }
        }

        #region Metodos y Propiedades para Oficializar
        protected void cmdOficializar_Click(object sender, EventArgs e)
        {
            //hacer una corrida de todas las validaciones
            //Camibar Bit a Cerrado  

            GuardarObs();
          
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
           
            string[] usr = User.Identity.Name.Split('|');

            string resValidaCuestionario = "";
            string resValidaAnexo = "";
            if (ddlMotivos.SelectedValue != "0")
                controlDP.Estatus = int.Parse(ddlMotivos.SelectedValue);
            else
            {
                CargarTodoYGuardar(controlDP);
                resValidaCuestionario = ValidarCuestionario(controlDP);
                resValidaAnexo = ValidarAnexo(controlDP);
            }
            if ((resValidaCuestionario == "" && resValidaAnexo == "" && ddlMotivos.SelectedValue == "0") || ddlMotivos.SelectedValue != "0")
            {
                if (ddlMotivos.SelectedValue == "0")
                    controlDP.Estatus = 10;

                wsEstadisiticas.Oficializar_Cuestionario(controlDP, int.Parse(usr[0]));

                EstadoBotones(controlDP);
            }
        }

        private void CargarTodoYGuardar(ControlDP controlDP)
        {
            Class911.CargaInicialCuestionario(controlDP, 1);
        }

        private string ValidarCuestionario(ControlDP controlDP)
        {
            string salida = "";

            System.Text.StringBuilder cadena = new System.Text.StringBuilder();
            cadena.Append("NoVarialbe=Valor=ReadOnly|");
            for (int i = 1; i < controlDP.CuestionarioDP.TotalVariables; i++)
            {
                cadena.Append(i.ToString() + "=X=false|");
            }

            cadena.Append("!Ejecutar!" + controlDP.ID_Control.ToString());
            salida = Class911.RaiseCallbackEvent(cadena.ToString(), Class911.Doc_Cuestionario, HttpContext.Current);

            PintarFallas(salida, "VAR");
            return salida;
        }
        private string ValidarAnexo(ControlDP controlDP)
        {
            string res;
            //  |NoVariable = Valor
            //Inicio
            string Datos = "NoVariable=Valor=ReadOnly|1=0=false|2=0=false|3=0=false|4=0=false|6=0=false|7=0=false|8=0=false|9=0=false|10=0=false|11=0=false|12=0=false|13=0=false|14=0=false|15=0=false|16=0=false|17=0=false|18=0=false|19=0=false|20=0=false|21=0=false|22=0=false|23=0=false|24=0=false|25=0=false|26=0=false|33=0=false|34=0=false|35=0=false|36=0=false|37=0=false|38=0=false|39=0=false|40=0=false|41=0=false|43=0=false|44=0=false|45=0=false|46=0=false|47=0=false|48=0=false|49=0=false|50=0=false|51=0=false|52=0=false|53=0=false|54=0=false|55=0=false|56=0=false|62=0=false|63=0=false|64=0=false|65=0=false|66=0=false|67=0=false|68=0=false|69=0=false|70=0=false|71=0=false|72=0=false|73=0=false|74=0=false|75=0=false|76=0=false|77=0=false|78=0=false|79=0=false|80=0=false|81=0=false|82=0=false|83=0=false|86=0=false|87=0=false|88=0=false|89=0=false|91=0=false|92=0=false|93=0=false|94=0=false|95=0=false|96=0=false|97=0=false|98=0=false|99=0=false|100=0=false|101=0=false|102=0=false|103=0=false|104=0=false|107=0=false|108=0=false|109=0=false|110=0=false|111=0=false|112=0=false|113=0=false|114=0=false|!Ejecutar!" + controlDP.ID_Control.ToString();
            res = Class911.RaiseCallbackEvent(Datos, Class911.Doc_AnexoInicio, HttpContext.Current);

            //|266! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |276! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |277! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |312! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |313! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |314! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# #
            PintarFallas(res, "VarAnex");

            return res;
        }
        private void PintarFallas(string datos, string variable)
        {
            //ConfigCapturaDP[] listaConfigCapturaDP = (ConfigCapturaDP[])HttpContext.Current.Session["listaConfigCaptura"];

            Class911 x = new Class911();
            string[] lista = datos.Split('|');
            foreach (string data in lista)
            {
                if (data != "")
                {
                    string[] ele = data.Split('!');
                    TableRow row = new TableRow();
                    TableCell c1 = new TableCell(); c1.CssClass = "BordesX";
                    TableCell c2 = new TableCell(); c2.CssClass = "BordesX";
                    TableCell c3 = new TableCell(); c3.CssClass = "BordesX";
                    ConfigCapturaDP conf = null;
                    if (variable == "VAR")
                    {
                        //conf = x.BuscarConfigCaptura(int.Parse(ele[0]), listaConfigCapturaDP);
                        c2.Text = "";// conf.Descripcion;
                    }
                    else
                    {
                        c2.Text = "";
                    }
                    c1.Text = variable + ele[0];


                    string fallas = "";
                    foreach (string falla in ele[1].Split('#'))
                    {
                        if (falla != "")
                            fallas = fallas + falla;
                    }
                    c3.Text = fallas;

                    row.Cells.Add(c1);
                    row.Cells.Add(c2);
                    row.Cells.Add(c3);
                    tblFallas.Rows.Add(row);
                    tblFallas.Visible = true;
                }
            }
        }
       

        #endregion

        protected void cmdGenerarComprobante_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarComprobante_PDF(int.Parse(hidIdCtrl.Value));
        }

        protected void cmdImprimirCuestionario_Lleno_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarCuestionario_PDF(int.Parse(hidIdCtrl.Value));
        }

        protected void cmdImprimircuestionario_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarCuestionario_Bacio_PDF(int.Parse(hidIdCtrl.Value));
        }

        protected void cmdGuardarObs_Click(object sender, EventArgs e)
        {
            GuardarObs();
        }
        private void GuardarObs()
        {
            ObservacionesDP observacionesDP = new ObservacionesDP();

            observacionesDP.ID_Control = int.Parse(hidIdCtrl.Value);
            observacionesDP.Observacion = txtObservaciones.Text;

            SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
            wsEstadisiticas.GuardarObservacion(Class911.GetControlSeleccionado(HttpContext.Current), observacionesDP);
        }

    }
}
