<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.1(Carreras Magisterial y Aulas)" AutoEventWireup="true" CodeBehind="CMYA_911_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_1.CMYA_911_1" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
   <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td>
                <span>EDUCACI�N PREESCOLAR</span>
            </td>
        </tr>
        <tr>
            <td>
                <span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span>
            </td>
        </tr>
        <tr>
            <td>
                <span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span>
            </td>
        </tr>
    </table>
    </div></div>
    <div id="menu" style="min-width:1000px; width:expression(document.body.clientWidth < 1001? '1000px': 'auto' );">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos_911_1',true)"><a href="#" title=""><span>ALUMNOS</span></a></li>
        <li onclick="openPage('Personal_911_1',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('CMYA_911_1',true)"><a href="#" title="" class="activo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
        <li onclick="openPage('Gasto_911_1',false)"><a href="#" title=""><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
        </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>  
<br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
   <center>
    
       <!--Tabla Fondo-->
      <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"></td>
				    <td>   
 					 <!--Contenido dentro del area de trabajo-->

                        <table >            
                           
                            <tr>
                                <td colspan="2">
                                </td>
                            </tr>
                         <tr>
                                <td>
                        <table style="width: 315px; height: 729px;text-align:justify;" >
                            <tr>
                                <td>
                                                            <asp:Label ID="lblCarrera" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="III. CARRERA MAGISTERIAL"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify">
                                                            <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de profesores que se encuentran en el programa de carrera magisterial"
                                                                Width="299px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align:justify">
                                                            <asp:TextBox ID="txtV515" runat="server" Columns="3" TabIndex="10101" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify; padding-bottom: 20px; padding-top: 10px;">
                                                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Desglose la cantidad anotada en el inciso anterior, seg�n la vertiente y el nivel en que se encuentran los profesores." Width="299px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td rowspan="6" style="width: 147px; text-align: left" valign="top">
                                                            <asp:Label ID="lbl1aVertiente" runat="server" CssClass="lblRojo" Text="1a. VERTIENTE"
                                                                Width="100%" Height="21px" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lbl1aVertiente2" runat="server"
                                                                    CssClass="lblRojo" Height="21px" Text="(Profesores frente a grupo)" Width="100%" Font-Bold="True"></asp:Label></td>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelA1" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV516" runat="server" Columns="3" TabIndex="10201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelB1" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV517" runat="server" Columns="3" TabIndex="10301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelBC1" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV518" runat="server" Columns="3" TabIndex="10401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelC1" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV519" runat="server" Columns="3" TabIndex="10501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelD1" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV520" runat="server" Columns="3" TabIndex="10601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelE1" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV521" runat="server" Columns="3" TabIndex="10701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td rowspan="1" style="width: 147px; height: 5px; text-align: left" valign="top">
                                </td>
                                <td nowrap="nowrap" style="height: 5px; width: 55px;">
                                </td>
                                <td style="height: 5px; width: 57px; text-align: right;">
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="6" style="width: 147px; text-align: left" valign="top">
                                                            <asp:Label ID="lbl2aVertiente" runat="server" CssClass="lblRojo" Height="21px" Text="2a. VERTIENTE"
                                                                Width="100%" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lbl2aVertiente2" runat="server" CssClass="lblRojo" Height="21px" Text="(Docentes en funciones directivas y de supervisi�n)"
                                                                Width="100%" Font-Bold="True"></asp:Label></td>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelA2" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV522" runat="server" Columns="3" TabIndex="10801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelB2" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV523" runat="server" Columns="3" TabIndex="10901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelBC2" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV524" runat="server" Columns="3" TabIndex="11001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelC2" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV525" runat="server" Columns="3" TabIndex="11101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelD2" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV526" runat="server" Columns="3" TabIndex="11201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelE2" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV527" runat="server" Columns="3" TabIndex="11301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td rowspan="1" style="width: 147px; height: 8px; text-align: left" valign="top">
                                </td>
                                <td nowrap="nowrap" style="height: 8px; width: 55px;">
                                </td>
                                <td style="height: 8px; width: 57px; text-align: right;">
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="6" style="width: 147px; text-align: left" valign="top">
                                                            <asp:Label ID="lbl3aVertiente" runat="server" CssClass="lblRojo" Height="21px" Text="3a. VERTIENTE"
                                                                Width="100%" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lbl3aVertiente2" runat="server" CssClass="lblRojo" Height="21px" Text="(Docentes en actividades t�cno-pedag�gicas)"
                                                                Width="100%" Font-Bold="True"></asp:Label></td>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelA3" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV528" runat="server" Columns="3" TabIndex="11401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelB3" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV529" runat="server" Columns="3" TabIndex="11501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelBC3" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV530" runat="server" Columns="3" TabIndex="11601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelC3" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV531" runat="server" Columns="3" TabIndex="11701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelD3" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV532" runat="server" Columns="3" TabIndex="11801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 55px;">
                                                            <asp:Label ID="lblNivelE3" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                                <td style="width: 57px; height: 25px; text-align: right">
                                                            <asp:TextBox ID="txtV533" runat="server" Columns="3" TabIndex="11901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                        </table>
                                </td>
                            </tr>
                        </table>
                                </td>
                                <td valign="top">
                                    <table style="width: 93px">
                                        <tr>
                                            <td style="text-align:justify;">
                                                            <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="IV. AULAS"
                                                                Width="100%"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: justify">
                                                            <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Text="1. Registre el n�mero de aulas por grado, seg�n su tipo."
                                                                Width="100%"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 20px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                            <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Text="Notas:" Width="100%"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: justify">
                                                            <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Text="a) El reporte de aulas debe ser por turno."
                                                                Width="100%"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: justify">
                                                            <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Text="b) Si un aula se utiliza para impartir clases a m�s de un grado, an�tela en el rubro correspondiente"
                                                                Width="100%"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 25px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="height: 36px; text-align: center">
                                                            <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Text="AULAS" Width="100%" Font-Bold="True"></asp:Label></td>
                                                        <td style="height: 36px; text-align: center">
                                                            <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Text="PRIMERO" Width="100%" Font-Bold="True"></asp:Label></td>
                                                        <td style="text-align: center; height: 36px;">
                                                            <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Text="SEGUNDO" Width="100%" Font-Bold="True"></asp:Label></td>
                                                        <td style="text-align: center; height: 36px;">
                                                            <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Text="TERCERO" Width="100%" Font-Bold="True"></asp:Label></td>
                                                        <td style="text-align: center; height: 36px;">
                                                            <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Text="M�S DE UN GRADO"
                                                                Width="60px" Font-Bold="True"></asp:Label></td>
                                                        <td style="height: 36px; text-align: center">
                                                            <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Text="TOTAL" Width="100%" Font-Bold="True"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Text="EXISTENTES" Width="100%"></asp:Label></td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtV534" runat="server" Columns="3" TabIndex="20101" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Text="EN USO" Width="100%"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV535" runat="server" Columns="3" TabIndex="20201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV536" runat="server" Columns="3" TabIndex="20202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV537" runat="server" Columns="3" TabIndex="20203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV538" runat="server" Columns="3" TabIndex="20204" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV539" runat="server" Columns="3" TabIndex="20205" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Text="De las reportadas en uso, indique el n�mero de las adaptadas."
                                                                Width="100%"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Text="ADAPTADAS" Width="100%"></asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV540" runat="server" Columns="3" TabIndex="20301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV541" runat="server" Columns="3" TabIndex="20302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV542" runat="server" Columns="3" TabIndex="20303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV543" runat="server" Columns="3" TabIndex="20304" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="txtV544" runat="server" Columns="3" TabIndex="20305" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                         <hr />
                        <table align="center">
                            <tr>
                                <td ><span  onclick="openPage('Personal_911_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                                <td ><span  onclick="openPage('Personal_911_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                                <td style="width: 330px;">
                                &nbsp;
                                </td>
                                <td ><span  onclick="openPage('Gasto_911_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                                <td ><span  onclick="openPage('Gasto_911_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                            </tr>
                        </table>
                        
                        <div id="divResultado" class="divResultado" ></div>
                       
                        <br />
                        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
                        <input id="hidIdCtrl" type="hidden" runat= "server" />
                        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
                        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
          </div>
                           <asp:Panel ID="pnlFallas" runat="server">
                           </asp:Panel>
                <!--/Contenido dentro del area de trabajo-->
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
         </table>
         <!--/Tabla Fondo-->

        <script type="text/javascript" language="javascript">
                    MaxCol = 16;
                    MaxRow = 31;
                    TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                    GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                Disparador(<%=this.hidDisparador.Value %>);      
        </script> 
         <script type="text/javascript">
        
    </script>

</center> 
</asp:Content> 