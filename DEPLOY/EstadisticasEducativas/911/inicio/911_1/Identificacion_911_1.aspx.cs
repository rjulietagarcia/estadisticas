using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using SEroot.WsInmuebles;
using SEroot.WsEstadisticasEducativas;
using SEroot.WsCentrosDeTrabajo;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
using EstadisticasEducativas._911;

namespace EstadisticasEducativasInicio._911.inicio._911_1
{
    public partial class Identificacion_911_1 : System.Web.UI.Page
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadiscticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ControlDP controlDP = null;
               

                usr = SeguridadSE.GetUsuario(HttpContext.Current);
                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;

                #region Carga Parametros Y Hidden's
                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur =0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;
                #endregion

                //ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                //-------Ingresador para el control del Popup
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                if (controlDP != null)
                {
                    CombosCatalogos cmbcat = new CombosCatalogos();
                    cmbcat.LlenarMotivos(ddlMotivos);
                    EstadoBotones(controlDP);
                }
                //-------Fin de Nuevo Codigo
                if (controlDP.Estatus == 0)
                {
                    
                    pnlOficializado.Visible = false;
                }
                else
                    pnlOficializado.Visible = true;

                CargaDatos((int)cctSeleccionado.CctntId, (int)cctSeleccionado.InmuebleId);
            }
        }

        UsuarioSeDP usrdp = null;
        protected void getUsr()
        {
            if (usrdp == null)
                usrdp = SeguridadSE.GetUsuario(HttpContext.Current);
        }
        
        #region  carga datos old
        //private void CargaDatos(int id_CT, int id_Inm, string cct, string tur)
        //{
        //    string claveCCT = "";
        //    Service_Inmuebles ws = new Service_Inmuebles();
        //    if (id_Inm > 0)
        //    {
        //        DsBusquedaInmuebles ds = new DsBusquedaInmuebles();
        //        ds = ws.BusquedaInmuebles(id_Inm.ToString());

        //        txtClaveInmueble.Text = ds.Tables[0].Rows[0]["Clave"].ToString().ToUpper();
        //        txtMunicipio.Text = ds.Tables[0].Rows[0]["Municipio"].ToString().ToUpper();
        //        txtLocalidad.Text = ds.Tables[0].Rows[0]["Localidad"].ToString().ToUpper();
        //        txtColonia.Text = ds.Tables[0].Rows[0]["Colonia"].ToString().ToUpper();
        //        txtCalle.Text = ds.Tables[0].Rows[0]["Calle"].ToString().ToUpper();
        //        txtEntreCalle.Text = ds.Tables[0].Rows[0]["EntreCalle"].ToString().ToUpper();
        //        txtYCalle.Text = ds.Tables[0].Rows[0]["YCalle"].ToString().ToUpper();
        //        txtCPosterior.Text = ds.Tables[0].Rows[0]["CallePosterior"].ToString().ToUpper();
        //        txtNumeroExterior.Text = ds.Tables[0].Rows[0]["Numero"].ToString();
        //        txtNumeroInterior.Text = ds.Tables[0].Rows[0]["Numero_Interior"].ToString().ToUpper();
        //        txtCodigoPostal.Text = ds.Tables[0].Rows[0]["Codigo_Postal"].ToString();
        //    }

        //    SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo wsCt = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        //    SEroot.WsCentrosDeTrabajo.CentroTrabajoDP CtDp = new SEroot.WsCentrosDeTrabajo.CentroTrabajoDP();

        //    if (id_CT > 0)
        //    {
        //        CtDp = wsCt.Load_CT(id_CT);

        //        txtTipoCT.ReadOnly = true;
        //        txtClasificador.ReadOnly = true;
        //        txtIdentificador.ReadOnly = true;
        //        txtDepNormativa.ReadOnly = true;
        //        txtDepOperativa.ReadOnly = true;
        //        txtServicio.ReadOnly = true;
        //        txtSostenimiento.ReadOnly = true;

        //        string dTipoCT = "";
        //        string dClas = "";
        //        string dIden = "";
        //        string dDepNor = "";
        //        string dDepOpe = "";
        //        string dSer = "";
        //        string dSos = "";

        //        string cTipoCT = "";
        //        string cClas = "";
        //        string cIden = "";
        //        string cDepNor = "";
        //        string cDepOpe = "";
        //        string cSer = "";
        //        string cSos = "";

        //        string idTipoCT = "";
        //        string idClas = "";
        //        int idIden = 0;
        //        int idDepNor = 0;
        //        int idDepOpe = 0;
        //        int idSer = 0;
        //        int idSos = 0;

        //        if (CtDp != null)
        //        {
        //            if (CtDp.estatusId == 2 || CtDp.estatusId == 3)
        //            {
        //                lblMsg.Text = "El centro de trabajo esta clausurado";
        //            }
        //            else
        //            {
        //                idTipoCT = CtDp.tipoCTId;
        //                idClas = CtDp.clave.Substring(2, 1);
        //                cIden = CtDp.clave.Substring(3, 2);
        //                idDepNor = int.Parse(CtDp.dependenciaNormativaId.ToString());
        //                idDepOpe = int.Parse(CtDp.dependenciaOperativaId.ToString());
        //                idSer = int.Parse(CtDp.servicioId.ToString());
        //                idSos = int.Parse(CtDp.sostenimientoId.ToString());

        //                string id_Regla = "";
        //                id_Regla = wsCt.RegresaIdRegla2(idTipoCT, idClas, cIden, idDepNor, idDepOpe, idSer, idSos);
        //                if (id_Regla.Length > 0)
        //                {
        //                    SEroot.WsCentrosDeTrabajo.CctCriteriaDP critDP = new SEroot.WsCentrosDeTrabajo.CctCriteriaDP();
        //                    critDP = wsCt.Load(int.Parse(id_Regla));
        //                    if (critDP != null)
        //                    {
        //                        idIden = critDP.identificadorId;
        //                        SEroot.WsCentrosDeTrabajo.TipoCentroTrabajoDP tipoCtDP = new SEroot.WsCentrosDeTrabajo.TipoCentroTrabajoDP();
        //                        tipoCtDP = wsCt.Load_TipoCentroTrabajo(idTipoCT);
        //                        cTipoCT = tipoCtDP.tipoCTId;
        //                        dTipoCT = tipoCtDP.nombre;

        //                        SEroot.WsCentrosDeTrabajo.ClasificadorDP clasDP = new SEroot.WsCentrosDeTrabajo.ClasificadorDP();
        //                        clasDP = wsCt.Load_Clasificador(idClas);
        //                        cClas = clasDP.clasificadorId;
        //                        dClas = clasDP.nombre;

        //                        SEroot.WsCentrosDeTrabajo.IdentificadorDP idenDP = new SEroot.WsCentrosDeTrabajo.IdentificadorDP();
        //                        idenDP = wsCt.Load_Identificador(idIden);
        //                        cIden = idenDP.clave;
        //                        dIden = idenDP.nombre;

        //                        SEroot.WsCentrosDeTrabajo.DependenciaNormativaDP depNorDP = new SEroot.WsCentrosDeTrabajo.DependenciaNormativaDP();
        //                        depNorDP = wsCt.Load_DependenciaNormativa(idDepNor);
        //                        cDepNor = depNorDP.clave;
        //                        dDepNor = depNorDP.nombre;

        //                        SEroot.WsCentrosDeTrabajo.DependenciaOperativaDP depOpeDP = new SEroot.WsCentrosDeTrabajo.DependenciaOperativaDP();
        //                        depOpeDP = wsCt.Load_DependenciaOperativa(idDepOpe);
        //                        cDepOpe = depOpeDP.clave;
        //                        dDepOpe = depOpeDP.nombre;

        //                        SEroot.WsCentrosDeTrabajo.ServicioDP serDP = new SEroot.WsCentrosDeTrabajo.ServicioDP();
        //                        serDP = wsCt.Load_Servicio(idSer);
        //                        cSer = serDP.clave;
        //                        dSer = serDP.nombre;

        //                        SEroot.WsCentrosDeTrabajo.SostenimientoDP sosDP = new SEroot.WsCentrosDeTrabajo.SostenimientoDP();
        //                        sosDP = wsCt.Load_Sostenimiento(idSos);
        //                        cSos = sosDP.sostenimientoId.ToString();
        //                        dSos = sosDP.nombre;

        //                        int tipoEdu = critDP.tipo;
        //                        int niv = critDP.nivel;
        //                        int sos = critDP.sostenimiento;
        //                        string tipoZona = critDP.zonaTipo;
        //                        string eduFisTipo = critDP.eduFisTipo;
        //                        string almaTipo = critDP.almaTipo;
        //                        string secTipo = critDP.secTipo;
        //                    }
        //                }

        //                txtCCTEnt.Text = CtDp.clave.Substring(0, 2);
        //                txtCCTIden.Text = CtDp.clave.Substring(2, 3);
        //                txtCCTCons.Text = CtDp.clave.Substring(5, 4);
        //                txtCCTDV.Text = CtDp.clave.Substring(9, 1);

        //                claveCCT = txtCCTEnt.Text + txtCCTIden.Text + txtCCTCons.Text + txtCCTDV.Text;

        //                txtFechaFundacion.Text = CtDp.fecha_Fundacion.ToString();
        //                txtFechaAlta.Text = CtDp.fecha_Alta.ToString();

        //                SEroot.WsCentrosDeTrabajo.TurnoCriterioDP turCritDP = new SEroot.WsCentrosDeTrabajo.TurnoCriterioDP();

        //                int turno3d = int.Parse(CtDp.turno3d.ToString());
        //                int turno1d = int.Parse(CtDp.turno3d.ToString().Substring(0, 1));

        //                turCritDP = wsCt.LoadTurnoCriterio(turno3d, turno1d);

        //                txtTurno.Text = CtDp.turno3d.ToString();
        //                txtTurnoD.Text = turCritDP.nombre;

        //                txtNombreCT.Text = CtDp.nombre;

        //                int zonaid = CtDp.zonaId;
        //                txtRegion.Text = CtDp.regionId.ToString();

        //                SEroot.WsCentrosDeTrabajo.ZonaDP zonDP = new SEroot.WsCentrosDeTrabajo.ZonaDP();
        //                zonDP = wsCt.Load_Zona(zonaid);
        //                txtZona.Text = zonDP.numeroZona.ToString();
        //            }
        //        }

        //        txtTipoCT.Text = cTipoCT;
        //        txtClasificador.Text = cClas;
        //        txtIdentificador.Text = cIden;
        //        txtDepNormativa.Text = cDepNor;
        //        txtDepOperativa.Text = cDepOpe;
        //        txtServicio.Text = cSer;
        //        txtSostenimiento.Text = cSos;

        //        string valIden = cIden + "_" + idIden.ToString();
        //        string valDepNor = cDepNor + "_" + idDepNor.ToString();
        //        string valDepOpe = cDepOpe + "_" + idDepOpe.ToString();
        //        string valSer = cSer + "_" + idSer.ToString();

        //        txtTipoCTD.Text = dTipoCT;
        //        txtClasificadorD.Text = dClas;
        //        txtIdentificadorD.Text = dIden;
        //        txtDepNormativaD.Text = dDepNor;
        //        txtDepOperativaD.Text = dDepOpe;
        //        txtServicioD.Text = dSer;
        //        txtSostenimientoD.Text = dSos;

        //        SEroot.WsCentrosDeTrabajo.DsCctTelefonos dsTels = new SEroot.WsCentrosDeTrabajo.DsCctTelefonos();
        //        dsTels = wsCt.ListaTelefonosPorCCT(id_CT);
        //        StringBuilder sb = new StringBuilder();
        //        sb.Append("<table>");
        //        if (dsTels != null && dsTels.Tables[0].Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in dsTels.Tables[0].Rows)
        //            {
        //                sb.Append("<tr class='fondoNaranja'><td class='lblNaranja'><b>Tel�fono:</b></td><td width='130px' class='lblNaranja'>" + dr["Numero"].ToString() + "</td><td class='lblNaranja'><b>Extensi�n:</b></td><td width='100px' class='lblNaranja'>" + dr["Extension"].ToString() + "</td></tr>");
        //            }
        //        }
        //        else
        //        {
        //            sb.Append("<tr><td class='lblNaranja'><b>Tel�fono:</b></td><td width='130px'></td><td class='lblNaranja'><b>Extensi�n:</b></td><td width='100px'></td></tr>");
        //        }
        //        sb.Append("</table>");
        //        tels.InnerHtml = sb.ToString();
        //        txtDirector.Text = wsCt.GetDirectorCT(id_CT);
        //    }
        //}
        #endregion

        private void CargaDatos(int id_CT, int id_Inm)
        {
            string claveCCT = "";


            if (id_Inm > 0)
            {
                #region Inmuebles
                Service_Inmuebles wsin = new Service_Inmuebles();
                SEroot.WsInmuebles.DatosIdentificacionInDP dp = wsin.Load_Inmueble(id_Inm);

                txtClaveInmueble.Text = dp.ClaveInmueble;
                txtMunicipio.Text = dp.Municipio;
                txtLocalidad.Text = dp.Localidad;
                txtColonia.Text = dp.Colonia;
                txtCalle.Text = dp.Calle;
                txtEntreCalle.Text = dp.EntreCalle;
                txtYCalle.Text = dp.YCalle;
                txtCPosterior.Text = dp.CallePosterior;
                txtNumeroExterior.Text = dp.Numero.ToString();
                txtNumeroInterior.Text = dp.NumeroInterior;
                txtCodigoPostal.Text = dp.CodigoPostal;

                #endregion
            }



            if (id_CT > 0)
            {
                #region CT
                DatosIdentificacionCTDP CtDp = new DatosIdentificacionCTDP();
                Service_CentrosDeTrabajo wsct = new Service_CentrosDeTrabajo();
                CtDp = wsct.GetDatosIdentificacion(id_CT);

                txtTipoCT.ReadOnly = true;
                txtClasificador.ReadOnly = true;
                txtIdentificador.ReadOnly = true;
                txtDepNormativa.ReadOnly = true;
                txtDepOperativa.ReadOnly = true;
                txtServicio.ReadOnly = true;
                txtSostenimiento.ReadOnly = true;

                txtCCTEnt.Text = CtDp.Clavect.Substring(0, 2);
                txtCCTIden.Text = CtDp.Clavect.Substring(2, 3);
                txtCCTCons.Text = CtDp.Clavect.Substring(5, 4);
                txtCCTDV.Text = CtDp.Clavect.Substring(9, 1);
                claveCCT = txtCCTEnt.Text + txtCCTIden.Text + txtCCTCons.Text + txtCCTDV.Text;

                txtFechaFundacion.Text = CtDp.FechaFundacion;
                txtFechaAlta.Text = CtDp.FechaAlta;
                txtTurno.Text = CtDp.Turno;
                txtTurnoD.Text = CtDp.DTurno;
                txtNombreCT.Text = CtDp.NombreCT;

                txtRegion.Text = CtDp.Region.ToString();
                txtZona.Text = CtDp.Zona.ToString();
                txtTipoCT.Text = CtDp.TipoCT;
                txtClasificador.Text = CtDp.Clasificador;
                txtIdentificador.Text = CtDp.Identificador;

                txtDepNormativa.Text = CtDp.DepNormativa;
                txtDepOperativa.Text = CtDp.DepOperativa;
                txtServicio.Text = CtDp.Servicio;
                txtSostenimiento.Text = CtDp.Sostenimiento;
                txtTipoCTD.Text = CtDp.DTipoCT;

                txtClasificadorD.Text = CtDp.DClasificador;
                txtIdentificadorD.Text = CtDp.DIdentificador;
                txtDepNormativaD.Text = CtDp.DDepNormativa;
                txtDepOperativaD.Text = CtDp.DDepOperativa;
                txtServicioD.Text = CtDp.DServicio;

                txtSostenimientoD.Text = CtDp.DSostenimiento;
                tels.InnerHtml = CtDp.TelefonosTable;
                txtDirector.Text = CtDp.Director;

                #endregion
            }

        }

        //Agregados para la Funcionalidad de oficializar con motivo
        protected void cmdOficializar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);

            string[] usr = User.Identity.Name.Split('|');

            string resValidaCuestionario = "";
            string resValidaAnexo = "";
            if (ddlMotivos.SelectedValue != "0")
                controlDP.Estatus = int.Parse(ddlMotivos.SelectedValue);
            else
            {
                //CargarTodoYGuardar(controlDP);


                if (true)
                {

                    resValidaCuestionario = "";//ValidarCuestionario(controlDP);
                    resValidaAnexo = "";// ValidarAnexo(controlDP);
                }

            }
            if ((resValidaCuestionario == "" && resValidaAnexo == "" && ddlMotivos.SelectedValue == "0") || ddlMotivos.SelectedValue != "0")
            {
                if (ddlMotivos.SelectedValue == "0")
                    controlDP.Estatus = 10;

                wsEstadiscticas.Oficializar_Cuestionario(controlDP, int.Parse(usr[0]));

                EstadoBotones(controlDP);
                pnlOficializado.Visible = true;
            }

        }
        
        private void EstadoBotones(ControlDP controlDP)
        {
            if (controlDP.Estatus == 0)
            {
                cmdOficializar.Enabled = true;
                cmdGenerarComprobante.Enabled = false;
                cmdImprimircuestionario.Enabled = true;

                ddlMotivos.Enabled = true;
            }
            else
            {
                cmdOficializar.Enabled = false;
                ddlMotivos.Enabled = false;
                cmdGenerarComprobante.Enabled = true;
                cmdImprimircuestionario.Enabled = true; // ??

                if (controlDP.Estatus != 10)
                    ddlMotivos.SelectedValue = controlDP.Estatus.ToString();
            }
        }
        protected void cmdGenerarComprobante_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarComprobante_PDF(int.Parse(hidIdCtrl.Value));
        }
        protected void cmdImprimircuestionario_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarCuestionario_Bacio_PDF(int.Parse(hidIdCtrl.Value));
        }
        



    }
}