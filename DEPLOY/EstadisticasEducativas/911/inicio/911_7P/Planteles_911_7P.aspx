<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7P(Planteles y Aulas)" AutoEventWireup="true" CodeBehind="Planteles_911_7P.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7P.Planteles_911_7P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

 <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
   <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="PROFESIONAL T�CNICO MEDIO"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div></div>
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Carreras_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('Alumnos1_911_7P',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('Alumnos2_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Egresados_911_7P',true)"><a href="#" title=""><span>EGRESADOS</span></a></li>
        <li onclick="openPage('Personal_911_7P',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('Planteles_911_7P',true)"><a href="#" title="" class="activo"><span>AULAS</span></a></li>
        <li onclick="openPage('Gasto_911_7P',false)"><a href="#" title="" ><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROCEIES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
        

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        &nbsp;<br />
        
        <table style="width: 600px">
            <tr>
                <td>
                    <table style="width: 550px">
                        <tr>
                            <td style="padding-bottom: 10px; padding-top: 10px; text-align: left">
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                                    Text="V. AULAS" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Escriba el n�mero de aulas por grado y tipo, y el n�mero de talleres y laboratorios existentes en el plantel."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px; padding-top: 10px; text-align: left">
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Nota:"
                                    Width="100%"></asp:Label>
                                <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="a) El reporte de aulas debe ser por turno."
                                    Width="100%"></asp:Label>
                                <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="b) Si un aula se utiliza para dar clases a m�s de un grado, an�tela en el rubro correspondiente."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><table>
                                <tr>
                                    <td style="text-align: center; padding-bottom: 10px; vertical-align: bottom; height: 19px;">
                                        <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="AULAS"
                                            Width="90px"></asp:Label></td>
                                    <td style="text-align: center; padding-bottom: 10px; vertical-align: bottom; height: 19px;">
                                        <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                                            Width="70px"></asp:Label></td>
                                    <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                                        <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                                            Width="70px"></asp:Label></td>
                                    <td style="text-align: center; padding-bottom: 10px; vertical-align: bottom; height: 19px;">
                                        <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                                            Width="70px"></asp:Label></td>
                                    <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                                            Width="70px"></asp:Label></td>
                                    <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                                        <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="QUINTO"
                                            Width="70px"></asp:Label></td>
                                    <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                                        <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M�S DE UN GRADO"
                                            Width="70px"></asp:Label></td>
                                    <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                                        <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                                            Width="70px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 45px;">
                                        <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EXISTENTES"
                                            Width="90px"></asp:Label></td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center; width: 71px;">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV854" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="10101"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 45px;">
                                        <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EN USO"
                                            Width="90px"></asp:Label></td>
                                    <td style="text-align: center; height: 26px;">
                                        <asp:TextBox ID="txtV855" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10201"></asp:TextBox></td>
                                    <td style="text-align: center; width: 71px; height: 26px;">
                                        <asp:TextBox ID="txtV856" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10202"></asp:TextBox></td>
                                    <td style="text-align: center; height: 26px;">
                                        <asp:TextBox ID="txtV857" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10203"></asp:TextBox></td>
                                    <td style="height: 26px; text-align: center">
                                        <asp:TextBox ID="txtV858" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10204"></asp:TextBox></td>
                                    <td style="height: 26px; text-align: center">
                                        <asp:TextBox ID="txtV859" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10205"></asp:TextBox></td>
                                    <td style="height: 26px; text-align: center">
                                        <asp:TextBox ID="txtV860" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10206"></asp:TextBox></td>
                                    <td style="height: 26px; text-align: center">
                                        <asp:TextBox ID="txtV861" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="10207"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="8" style="padding-bottom: 10px; padding-top: 10px; text-align: left">
                                        <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="De las aulas reportadas en uso, indique el n�mero de las adaptadas."
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 45px;">
                                        <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ADAPTADAS"
                                            Width="90px"></asp:Label></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV862" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10301"></asp:TextBox></td>
                                    <td style="text-align: center; width: 71px;">
                                        <asp:TextBox ID="txtV863" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10302"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV864" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10303"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV865" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10304"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV866" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10305"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV867" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                            TabIndex="10306"></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV868" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="10307"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 45px;">
                                        <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TALLERES"
                                            Width="90px"></asp:Label></td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center; width: 71px;">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV869" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="10401"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 45px; text-align: left" colspan="1" rowspan="2">
                                        <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Text="LABORATORIOS"
                                            Width="90px"></asp:Label></td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center; width: 71px;">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                    </td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV870" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="10501"></asp:TextBox></td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal_911_7P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_7P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                &nbsp;
                </td>
                <td ><span  onclick="openPage('Gasto_911_7P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Gasto_911_7P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        <div id="divResultado" class="divResultado" ></div>
        

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 10;
                MaxRow = 10;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
