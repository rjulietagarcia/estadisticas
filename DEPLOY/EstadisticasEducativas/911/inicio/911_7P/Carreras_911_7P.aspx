<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7P(Alumnos por Carrera)" AutoEventWireup="true" CodeBehind="Carreras_911_7P.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7P.Carreras_911_7P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
           
 <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
   <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="PROFESIONAL T�CNICO MEDIO"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div></div>
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Carreras_911_7P',true)"><a href="#" title="" class="activo"><span>ALUMNOS POR CARRERA</span></a></li><li onclick="openPage('Alumnos1_911_7P',false)"><a href="#" title="" ><span>TOTAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ALUMNOS POR EDAD</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>EGRESADOS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROCEIES</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
           
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 600px">
            <tr>
                <td colspan="2" valign="top" style="text-align: left">
                    <asp:Label ID="Label35" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                        Text="I. ALUMNOS POR CARRERA O ESPECIALIDAD Y GRUPOS" Width="100%"></asp:Label></td>
            </tr>
           
            <tr>
                <td style="text-align: center">
                    <table style="width: 600px">
                        <tr>
                            <td style="text-align: left; height: 70px;">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba por carrera o especialidad el n�mero de alumnos, desglos�ndolo por grado y sexo. Para llenar el nombre y la clave del �rea de estudio, consulte la tabla de �reas del glosario. Si no cuenta con la clave de la carrera, deje los espacios en blanco."
                        Width="100%"></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Nombre de la carrera o especialidad."
                                    Width="220px"></asp:Label>
                                <asp:TextBox ID="txtVA1" runat="server" Columns="30" CssClass="lblNegro" MaxLength="30"
                                    TabIndex="10101"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="�rea de estudio"
                                    Width="110px"></asp:Label>
                                    <asp:DropDownList ID="ddl_Areas" runat="server"
                                        onchange="OnChange_Area(this)">
                                    </asp:DropDownList><br />
                                <asp:TextBox ID="txtVA2" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="10201"></asp:TextBox>
                                <asp:TextBox ID="txtVA3" runat="server" Columns="40" CssClass="lblNegro" MaxLength="40"
                                    TabIndex="10301"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Clave"
                                    Width="60px"></asp:Label>
                                <asp:TextBox ID="txtVA4" runat="server" Columns="10" CssClass="lblNegro" MaxLength="10"
                                    TabIndex="10401"></asp:TextBox>
                                <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Duraci�n"
                                    Width="70px"></asp:Label>
                                <asp:TextBox ID="txtVA5" runat="server" Columns="30" CssClass="lblNegro" MaxLength="30"
                                    TabIndex="10501"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                  <table>
                                    <tr>
                                        <td style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                                                Width="90px"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblSemestre" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEMESTRE"
                                                Width="90px"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="HOMBRES" Width="90px"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblExistencia" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                                                Width="90px"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajoAlto Orila">
                                            <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                                                Width="90px"></asp:Label></td>
                                        <td class="Orila">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 Y 2"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA6" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA7" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtVA8" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
                                        <td class="Orila">
                                            &nbsp;</td>    
                                    </tr>
                                    <tr>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 Y 4"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA9" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA10" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtVA11" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
                                        <td class="Orila">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 Y 6"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA12" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA13" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtVA14" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
                                        <td class="Orila">
                                            &nbsp;</td>    
                                    </tr>
                                    <tr>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lbl4o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o."
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 Y 8"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA15" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA16" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtVA17" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
                                        <td class="Orila">
                                            &nbsp;</td>    
                                    </tr>
                                    <tr>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5o."
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="9 Y 10"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA18" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA19" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtVA20" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20503"></asp:TextBox></td>
                                        <td class="Orila">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center" class="linaBajoS">
                                            <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajoS">
                                            <asp:TextBox ID="txtVA21" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajoS">
                                            <asp:TextBox ID="txtVA22" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajoS Orila">
                                            <asp:TextBox ID="txtVA23" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20603"></asp:TextBox></td>
                                        <td class="Orila">
                                            &nbsp;</td>               
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center" class="linaBajoS">
                                            <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PROCEIES (SOLO CONALEP)"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajoS">
                                            <asp:TextBox ID="txtVA24" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajoS">
                                            <asp:TextBox ID="txtVA25" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20702"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajoS Orila">
                                            <asp:TextBox ID="txtVA26" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20703"></asp:TextBox></td>
                                        <td class="Orila">
                                            &nbsp;</td>    
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: left">
                                            <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Escriba el n�mero de alumnos certificados o titulados, del ciclo anterior (S�LO CONALEP)."
                                                Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center">
                                        </td>
                                        <td style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T�CNICO AUXILIAR"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T�CNICO B�SICO"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajoAlto Orila">
                                            <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TITULADOS"
                                                Width="100%"></asp:Label></td>
                                        <td class="Orila">
                                            &nbsp;</td>        
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center">
                                            </td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA27" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtVA28" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20802"></asp:TextBox></td>
                                        <td style="text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtVA29" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20803"></asp:TextBox></td>
                                        <td class="Orila">
                                            &nbsp;</td>    
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkEliminarCarrera" runat="server" OnClick="lnkEliminarCarrera_Click">Eliminar Carrera Actual</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span style="color: #880000">Carreras: </span>
                                        </td>
                                        <td style="font-weight: bold; color: #880000">
                                            <asp:DropDownList ID="ddlID_Carrera" runat="server" onchange="OnChange(this);">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtVA0" runat="server" BackColor="Bisque" Columns="5" Font-Bold="True"
                                                MaxLength="5" ReadOnly="True" Style="text-align: right" Width="24px">0</asp:TextBox><span
                                                    style="color: #880000"> de </span>
                                            <asp:TextBox ID="lblUnodeN" runat="server" BackColor="Bisque" Columns="5" Font-Bold="True"
                                                MaxLength="5" ReadOnly="True" Width="24px">0</asp:TextBox>
                                        </td>
                                        <td id="td_Nuevo" runat="server">
                                            <span onclick="navegarCarreras_new(-1);">&nbsp;<a href="#">Agregar Otra <strong>*</strong></a></span>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_7P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_7P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                    &nbsp;
                </td>
                <td ><span  onclick="openPage('Alumnos1_911_7P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Alumnos1_911_7P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"  ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
          <script type="text/javascript" language="javascript">
                MaxCol = 20;
                MaxRow = 20;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                var ID_Carrera =0; 
                
              
                function OnChange_Area(dropdown)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    var SelText = dropdown.options[myindex].text;
                    document.getElementById("ctl00_cphMainMaster_txtVA2").value = SelValue;
                    var txt = document.getElementById("ctl00_cphMainMaster_txtVA3");
                    txt.value = SelText;
                  
                    return true;
                }
                function OnChange(dropdown)
                {
                    var myindex  = dropdown.selectedIndex
                    var SelValue = dropdown.options[myindex].value
                    navegarCarreras_new(SelValue);
                    return true;
                }
                
                function navegarCarreras_new(id_carrera){
                     ID_Carrera = (id_carrera);
                     var ID_Carreratmp = ID_Carrera; 
                                        
                    if (id_carrera == -1)
                    {
                       var totalRegs = document.getElementById('<%=lblUnodeN.ClientID%>').value;
                       ID_Carrera = eval(totalRegs) + 1; 
                       alert(ID_Carrera);
                    }
                
                    openPage('Carreras_911_7P');
                }
               
             
                function __ReceiveServerData_Variables(rValue){
                   if (rValue != null){
                        document.getElementById("divWait").style.visibility = "hidden";
                        var rows = rValue.split('|');
                        var vi = 1;
                        if (rValue != "")  LimpiarFallas();
                        var detalles="<ul>";
                        for(vi = 1; vi < rows.length; vi ++){
                            var res = rows[vi];
                            var colums = res.split('!');
                            var caja = document.getElementById("ctl00_cphMainMaster_txtV" + colums[0]);
                            if (caja!=null){
                                if(isNaN(caja.value))
                                {
                                    caja.className='txtVarFallas txtleft';
                                }
                                else
                                {
                                    caja.className='txtVarFallas';
                                }
                                caja.title = colums[1];
                                detalles = detalles + "<li><span style=\"cursor:pointer;\" onclick = \"javascript:document.getElementById('" + caja.id + "').focus();\">" + colums[1]+ "</span></li>";  
                            }else{
                                detalles = detalles + "<li>" + colums[1]+ "</li>";
                            }
                            
                       }
                       if (CambiarPagina != "") {  // para que no se borre la informacion a menos que se mueva de la pagina
                            var obj = document.getElementById('divResultado');
                            obj.innerHTML =  detalles + "</ul>";
                       }
                       var Cambiar = "No";
                       if ( rValue != "" && CambiarPagina != "" ){
                          if(ir1)
                            {
                               
                                Cambiar = "Si";
                            }
                            else
                            {
                                alert("Se encontraron errores de captura\nDebe corregirlos para avanzar a la siguiente p"+'\u00e1'+"gina.");
                                Cambiar = "No";
                            }
                       }
                       else if(rValue == "" && CambiarPagina != ""){
                          Cambiar = "Si";
                       }
                       if (Cambiar == "Si"){
                             window.location.href=CambiarPagina+".aspx?idC=" + ID_Carrera;
                       } 
                   }
                }

            
               
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
