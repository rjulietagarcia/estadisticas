using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;
using System.Text;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
using EstadisticasEducativas._911;

namespace EstadisticasEducativasInicio._911.inicio._911_7P
{
    public partial class Carreras_911_7P : System.Web.UI.Page, ICallbackEventHandler
    {
        //SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                //txtVA6.Attributes.Add("onkeypress", "return Num(event)"); txtVA6.Attributes.Add("onfocus", "VerTab(this);");

                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion


                //Cargar los datos para la carga inicial

                int ID_Carrera;
                int.TryParse(Request.Params["idC"], out ID_Carrera);
                txtVA0.Text = ListaCarreras(ddlID_Carrera, ID_Carrera, controlDP);

                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas_Carreras(this);

                Class911.LlenarDatosDB11(this.Page, controlDP, Class911.Doc_Carreras, int.Parse(txtVA0.Text), hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                {
                    pnlOficializado.Visible = false;
                    td_Nuevo.Visible = true;
                    Cargar_Areas();
                    lnkEliminarCarrera.Enabled = controlDP.NoCarrerasIngresadas > 0;
                }
                else
                {
                    pnlOficializado.Visible = true;
                    td_Nuevo.Visible = false;
                    lnkEliminarCarrera.Visible = false;
                }
            }
        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 4, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        // *************************************** codigo Extra Carreras ***************************************


        string ListaCarreras(DropDownList ddl, int ID_Carrera, ControlDP controlDP)
        {
            ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();

            lblUnodeN.Text = controlDP.NoCarrerasIngresadas.ToString();

            if (controlDP.NoCarrerasIngresadas > 0)
            {
                for (int i = 1; i <= controlDP.NoCarrerasIngresadas; i++)
                {
                    VariablesDP[] listaVarDP = wsEstadisiticas.LeerVariableEncuesta_Lista(controlDP, Class911.Doc_Carreras, i, "1");//Nombre de la Especialidad
                    string NombreEspecialidad = "";
                    if (listaVarDP[0].valor != null)
                        NombreEspecialidad = listaVarDP[0].valor.ToString();
                    ListItem listItem = new ListItem(i.ToString() + " - " + NombreEspecialidad, i.ToString());
                    if (i == ID_Carrera)
                        listItem.Selected = true;
                    ddl.Items.Add(listItem);
                }


            }

            if (controlDP.NoCarrerasIngresadas < ID_Carrera || controlDP.NoCarrerasIngresadas == 0)
            {
                ListItem itmNuevo = new ListItem(ID_Carrera.ToString() + " - " + "Nueva Carrera", ID_Carrera.ToString());
                itmNuevo.Selected = true;
                ddl.Items.Add(itmNuevo);
            }
            if (ID_Carrera < 1)
                ID_Carrera = 1;

            if (ID_Carrera > controlDP.NoCarrerasIngresadas)
            {
                lblUnodeN.Text = ID_Carrera.ToString();
                controlDP.NoCarrerasIngresadas = ID_Carrera;
            }

            return ID_Carrera.ToString();
        }

        void Cargar_Areas()
        {

            CombosCatalogos cc = new CombosCatalogos();
            cc.LlenardropsAreas(ddl_Areas);

            ListItem item = ddl_Areas.Items.FindByValue(txtVA2.Text);
            if (item != null)
                item.Selected = true;
           
        }

        protected void lnkEliminarCarrera_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
            ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
            int id_Carrera;
            int.TryParse(txtVA0.Text, out id_Carrera);
            wsEstadisiticas.BorrarCarrera(controlDP, id_Carrera);
            usr = SeguridadSE.GetUsuario(HttpContext.Current);

            controlDP = wsEstadisiticas.ObtenerDatosEncuestaID_CCTNT(controlDP.ID_CCTNT, Class911.ID_Inicio, controlDP.ID_CicloEscolar, usr.PersonaId);
            Class911.SetControlSeleccionado(HttpContext.Current, controlDP);
            Response.Redirect("Carreras_911_7P.aspx");
        }

    }
}
