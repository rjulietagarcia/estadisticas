<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7G(Proceies)" AutoEventWireup="true" CodeBehind="Proceies_911_7P.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7P.Proceies_911_7P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
   <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="PROFESIONAL T�CNICO MEDIO"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div></div>
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Carreras_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('Alumnos1_911_7P',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('Alumnos2_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Egresados_911_7P',true)"><a href="#" title=""><span>EGRESADOS</span></a></li>
        <li onclick="openPage('Personal_911_7P',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('Planteles_911_7P',true)"><a href="#" title=""><span>AULAS</span></a></li>
        <li onclick="openPage('Gasto_911_7P',true)"><a href="#" title=""><span>GASTO</span></a></li>
        <li onclick="openPage('Proceies_911_7P',true)"><a href="#" title="" class="activo"><span>PROCEIES</span></a></li>
        <li onclick="openPage('Anexo_911_7P',false)"><a href="#" title="" ><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
<br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center> 
         
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        &nbsp;
        <table>
            <tr>
                <td colspan="16" style="text-align:justify">
                    <asp:Label ID="lblALUMNOSPOREDAD" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                        Text="VII. ALUMNOS CERTIFICADOS REINSCRITOS, NO REINSCRITOS Y EN PROCEIES (S�LO CONALEP)" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="16" style="text-align: left">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Escriba el total de alumnos certificados reinscritos y certificados no reinscritos, desglos�ndolo por grado, sexo y edad (en esta pregunta s�lo se reporta informaci�n de 2� y 3er. grado)."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="16" style="text-align: left; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="lbltabla" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="Nota: No utilice las �reas sombreadas."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td rowspan="1">
                </td>
                <td rowspan="1">
                </td>
                <td>
                </td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl14a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="14 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl15a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="15 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl16a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="16 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl17a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl18a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl19a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl20a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl21a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl22a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl23a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl24a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl25a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="25 a�os y mas"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto Orila">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td rowspan="1">
                </td>
                <td colspan="2" rowspan="1" style="text-align: left" >
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T�CNICO AUXILIAR"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                </td>
                <td style="width: 63px; text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="width: 63px; text-align: center">
                </td>
                <td style="width: 63px; text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="width: 55px; text-align: center">
                </td>
                <td style="width: 55px; text-align: center">
                </td>
                <td style="width: 55px; text-align: center">
                </td>
            </tr>
            <tr>
                <td rowspan="5" class="linaBajoAlto">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o. (sem. 3 y 4)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcion2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtVB1" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"   Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV892" runat="server" Columns="4" MaxLength="4" TabIndex="10101" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV893" runat="server" Columns="4" MaxLength="4" TabIndex="10102" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV894" runat="server" Columns="4" MaxLength="4" TabIndex="10103" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV895" runat="server" Columns="4" MaxLength="4" TabIndex="10104" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV896" runat="server" Columns="4" MaxLength="4" TabIndex="10105" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV897" runat="server" Columns="4" MaxLength="4" TabIndex="10106" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV898" runat="server" Columns="4" MaxLength="4" TabIndex="10107" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV899" runat="server" Columns="4" MaxLength="4" TabIndex="10108" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV900" runat="server" Columns="4" MaxLength="4" TabIndex="10109" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV901" runat="server" Columns="4" MaxLength="4" TabIndex="10110" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV902" runat="server" Columns="4" MaxLength="4" TabIndex="10111" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto Orila">
                    <asp:TextBox ID="txtV903" runat="server" Columns="4" MaxLength="4" TabIndex="10112" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 63px; height: 26px; text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistencia2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtVB2" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV904" runat="server" Columns="4" MaxLength="4" TabIndex="10201" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV905" runat="server" Columns="4" MaxLength="4" TabIndex="10202" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV906" runat="server" Columns="4" MaxLength="4" TabIndex="10203" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV907" runat="server" Columns="4" MaxLength="4" TabIndex="10204" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV908" runat="server" Columns="4" MaxLength="4" TabIndex="10205" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV909" runat="server" Columns="4" MaxLength="4" TabIndex="10206" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV910" runat="server" Columns="4" MaxLength="4" TabIndex="10207" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV911" runat="server" Columns="4" MaxLength="4" TabIndex="10208" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV912" runat="server" Columns="4" MaxLength="4" TabIndex="10209" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV913" runat="server" Columns="4" MaxLength="4" TabIndex="10210" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV914" runat="server" Columns="4" MaxLength="4" TabIndex="10211" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV915" runat="server" Columns="4" MaxLength="4" TabIndex="10212" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcion2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB3" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV916" runat="server" Columns="4" MaxLength="4" TabIndex="10301" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV917" runat="server" Columns="4" MaxLength="4" TabIndex="10302" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV918" runat="server" Columns="4" MaxLength="4" TabIndex="10303" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV919" runat="server" Columns="4" MaxLength="4" TabIndex="10304" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV920" runat="server" Columns="4" MaxLength="4" TabIndex="10305" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV921" runat="server" Columns="4" MaxLength="4" TabIndex="10306" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV922" runat="server" Columns="4" MaxLength="4" TabIndex="10307" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV923" runat="server" Columns="4" MaxLength="4" TabIndex="10308" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV924" runat="server" Columns="4" MaxLength="4" TabIndex="10309" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV925" runat="server" Columns="4" MaxLength="4" TabIndex="10310" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV926" runat="server" Columns="4" MaxLength="4" TabIndex="10311" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV927" runat="server" Columns="4" MaxLength="4" TabIndex="10312" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
             <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistencia2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB4" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV928" runat="server" Columns="4" MaxLength="4" TabIndex="10401" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV929" runat="server" Columns="4" MaxLength="4" TabIndex="10402" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV930" runat="server" Columns="4" MaxLength="4" TabIndex="10403" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV931" runat="server" Columns="4" MaxLength="4" TabIndex="10404" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV932" runat="server" Columns="4" MaxLength="4" TabIndex="10405" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV933" runat="server" Columns="4" MaxLength="4" TabIndex="10406" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV934" runat="server" Columns="4" MaxLength="4" TabIndex="10407" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV935" runat="server" Columns="4" MaxLength="4" TabIndex="10408" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV936" runat="server" Columns="4" MaxLength="4" TabIndex="10409" ></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV937" runat="server" Columns="4" MaxLength="4" TabIndex="10410" ></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV938" runat="server" Columns="4" MaxLength="4" TabIndex="10411" ></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV939" runat="server" Columns="4" MaxLength="4" TabIndex="10412" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr> 
             <tr>
                <td colspan="2" style="padding-bottom: 10px; text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobados2oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVB5" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV940" runat="server" Columns="4" MaxLength="4" TabIndex="10501" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV941" runat="server" Columns="4" MaxLength="4" TabIndex="10502" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV942" runat="server" Columns="4" MaxLength="4" TabIndex="10503" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV943" runat="server" Columns="4" MaxLength="4" TabIndex="10504" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV944" runat="server" Columns="4" MaxLength="4" TabIndex="10505" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV945" runat="server" Columns="4" MaxLength="4" TabIndex="10506" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV946" runat="server" Columns="4" MaxLength="4" TabIndex="10507" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV947" runat="server" Columns="4" MaxLength="4" TabIndex="10508" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV948" runat="server" Columns="4" MaxLength="4" TabIndex="10509" ></asp:TextBox></td>
                 <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV949" runat="server" Columns="4" MaxLength="4" TabIndex="10510" ></asp:TextBox></td>
                 <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV950" runat="server" Columns="4" MaxLength="4" TabIndex="10511" ></asp:TextBox></td>
                 <td style="text-align: center;" class="linaBajoS Orila">
                    <asp:TextBox ID="txtV951" runat="server" Columns="4" MaxLength="4" TabIndex="10512" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td rowspan="1">
                </td>
                <td colspan="2" rowspan="1" style="text-align: left">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T�CNICO B�SICO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
            </tr>
             <tr>
                 <td rowspan="5" class="linaBajoAlto">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o. (sem. 5 y 6)"
                        Width="40px"></asp:Label></td>
                 <td rowspan="2" class="linaBajoAlto">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcion3oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtVB6" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtVB7" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV952" runat="server" Columns="4" MaxLength="4" TabIndex="10701" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV953" runat="server" Columns="4" MaxLength="4" TabIndex="10702" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV954" runat="server" Columns="4" MaxLength="4" TabIndex="10703" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV955" runat="server" Columns="4" MaxLength="4" TabIndex="10704" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV956" runat="server" Columns="4" MaxLength="4" TabIndex="10705" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV957" runat="server" Columns="4" MaxLength="4" TabIndex="10706" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV958" runat="server" Columns="4" MaxLength="4" TabIndex="10707" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV959" runat="server" Columns="4" MaxLength="4" TabIndex="10708" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV960" runat="server" Columns="4" MaxLength="4" TabIndex="10709" ></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV961" runat="server" Columns="4" MaxLength="4" TabIndex="10710" ></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajoAlto Orila">
                    <asp:TextBox ID="txtV962" runat="server" Columns="4" MaxLength="4" TabIndex="10711" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
             <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblExistencia3oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO REINSCRITO"
                        Width="100px"></asp:Label></td>
                 <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB8" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB9" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV963" runat="server" Columns="4" MaxLength="4" TabIndex="10801" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV964" runat="server" Columns="4" MaxLength="4" TabIndex="10802" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV965" runat="server" Columns="4" MaxLength="4" TabIndex="10803" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV966" runat="server" Columns="4" MaxLength="4" TabIndex="10804" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV967" runat="server" Columns="4" MaxLength="4" TabIndex="10805" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV968" runat="server" Columns="4" MaxLength="4" TabIndex="10806" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV969" runat="server" Columns="4" MaxLength="4" TabIndex="10807" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV970" runat="server" Columns="4" MaxLength="4" TabIndex="10808" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV971" runat="server" Columns="4" MaxLength="4" TabIndex="10809" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV972" runat="server" Columns="4" MaxLength="4" TabIndex="10810" ></asp:TextBox></td>
                 <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV973" runat="server" Columns="4" MaxLength="4" TabIndex="10811" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcion3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB10" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB11" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV974" runat="server" Columns="4" MaxLength="4" TabIndex="10901" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV975" runat="server" Columns="4" MaxLength="4" TabIndex="10902" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV976" runat="server" Columns="4" MaxLength="4" TabIndex="10903" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV977" runat="server" Columns="4" MaxLength="4" TabIndex="10904" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV978" runat="server" Columns="4" MaxLength="4" TabIndex="10905" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV979" runat="server" Columns="4" MaxLength="4" TabIndex="10906" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV980" runat="server" Columns="4" MaxLength="4" TabIndex="10907" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV981" runat="server" Columns="4" MaxLength="4" TabIndex="10908" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV982" runat="server" Columns="4" MaxLength="4" TabIndex="10909" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV983" runat="server" Columns="4" MaxLength="4" TabIndex="10910" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV984" runat="server" Columns="4" MaxLength="4" TabIndex="10911" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistencia3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB12" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB13" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV985" runat="server" Columns="4" MaxLength="4" TabIndex="11001" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV986" runat="server" Columns="4" MaxLength="4" TabIndex="11002" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV987" runat="server" Columns="4" MaxLength="4" TabIndex="11003" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV988" runat="server" Columns="4" MaxLength="4" TabIndex="11004" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV989" runat="server" Columns="4" MaxLength="4" TabIndex="11005" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV990" runat="server" Columns="4" MaxLength="4" TabIndex="11006" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV991" runat="server" Columns="4" MaxLength="4" TabIndex="11007" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV992" runat="server" Columns="4" MaxLength="4" TabIndex="11008" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV993" runat="server" Columns="4" MaxLength="4" TabIndex="11009" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV994" runat="server" Columns="4" MaxLength="4" TabIndex="11010" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV995" runat="server" Columns="4" MaxLength="4" TabIndex="11011" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="padding-bottom: 10px; text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobados3oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVB14" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVB15" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV996" runat="server" Columns="4" MaxLength="4" TabIndex="11101" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV997" runat="server" Columns="4" MaxLength="4" TabIndex="11102" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV998" runat="server" Columns="4" MaxLength="4" TabIndex="11103" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV999" runat="server" Columns="4" MaxLength="4" TabIndex="11104" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1000" runat="server" Columns="4" MaxLength="4" TabIndex="11105" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1001" runat="server" Columns="4" MaxLength="4" TabIndex="11106" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1002" runat="server" Columns="4" MaxLength="4" TabIndex="11107" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1003" runat="server" Columns="4" MaxLength="4" TabIndex="11108" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1004" runat="server" Columns="4" MaxLength="4" TabIndex="11109" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1005" runat="server" Columns="4" MaxLength="4" TabIndex="11110" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS Orila">
                    <asp:TextBox ID="txtV1006" runat="server" Columns="4" MaxLength="4" TabIndex="11111" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td  rowspan="1">
                </td>
                <td  colspan="2" style="padding-bottom: 10px; text-align: left">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
                <td  style="text-align: center">
                </td>
            </tr>
            <tr>
                <td rowspan="5" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTotalT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td rowspan="2" class="linaBajoAlto">
                    <asp:Label ID="lblHomT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionTH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtVB16" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1007" runat="server" Columns="4" MaxLength="4" TabIndex="11301" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1008" runat="server" Columns="4" MaxLength="4" TabIndex="11302" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1009" runat="server" Columns="4" MaxLength="4" TabIndex="11303" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1010" runat="server" Columns="4" MaxLength="4" TabIndex="11304" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1011" runat="server" Columns="4" MaxLength="4" TabIndex="11305" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1012" runat="server" Columns="4" MaxLength="4" TabIndex="11306" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1013" runat="server" Columns="4" MaxLength="4" TabIndex="11307" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1014" runat="server" Columns="4" MaxLength="4" TabIndex="11308" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1015" runat="server" Columns="4" MaxLength="4" TabIndex="11309" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1016" runat="server" Columns="4" MaxLength="4" TabIndex="11310" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1017" runat="server" Columns="4" MaxLength="4" TabIndex="11311" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto Orila">
                    <asp:TextBox ID="txtV1018" runat="server" Columns="4" MaxLength="4" TabIndex="11312" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaTH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB17" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1019" runat="server" Columns="4" MaxLength="4" TabIndex="11401" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1020" runat="server" Columns="4" MaxLength="4" TabIndex="11402" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1021" runat="server" Columns="4" MaxLength="4" TabIndex="11403" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1022" runat="server" Columns="4" MaxLength="4" TabIndex="11404" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1023" runat="server" Columns="4" MaxLength="4" TabIndex="11405" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1024" runat="server" Columns="4" MaxLength="4" TabIndex="11406" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1025" runat="server" Columns="4" MaxLength="4" TabIndex="11407" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1026" runat="server" Columns="4" MaxLength="4" TabIndex="11408" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1027" runat="server" Columns="4" MaxLength="4" TabIndex="11409" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1028" runat="server" Columns="4" MaxLength="4" TabIndex="11410" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1029" runat="server" Columns="4" MaxLength="4" TabIndex="11411" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV1030" runat="server" Columns="4" MaxLength="4" TabIndex="11412" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lblMujT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcionTM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB18" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1031" runat="server" Columns="4" MaxLength="4" TabIndex="11501" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1032" runat="server" Columns="4" MaxLength="4" TabIndex="11502" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1033" runat="server" Columns="4" MaxLength="4" TabIndex="11503" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1034" runat="server" Columns="4" MaxLength="4" TabIndex="11504" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1035" runat="server" Columns="4" MaxLength="4" TabIndex="11505" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1036" runat="server" Columns="4" MaxLength="4" TabIndex="11506" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1037" runat="server" Columns="4" MaxLength="4" TabIndex="11507" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1038" runat="server" Columns="4" MaxLength="4" TabIndex="11508" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1039" runat="server" Columns="4" MaxLength="4" TabIndex="11509" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1040" runat="server" Columns="4" MaxLength="4" TabIndex="11510" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1041" runat="server" Columns="4" MaxLength="4" TabIndex="11511" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV1042" runat="server" Columns="4" MaxLength="4" TabIndex="11512" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblExistenciaTM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO REINSCRITO"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtVB19" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1043" runat="server" Columns="4" MaxLength="4" TabIndex="11601" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1044" runat="server" Columns="4" MaxLength="4" TabIndex="11602" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1045" runat="server" Columns="4" MaxLength="4" TabIndex="11603" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1046" runat="server" Columns="4" MaxLength="4" TabIndex="11604" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1047" runat="server" Columns="4" MaxLength="4" TabIndex="11605" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1048" runat="server" Columns="4" MaxLength="4" TabIndex="11606" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1049" runat="server" Columns="4" MaxLength="4" TabIndex="11607" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1050" runat="server" Columns="4" MaxLength="4" TabIndex="11608" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1051" runat="server" Columns="4" MaxLength="4" TabIndex="11609" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1052" runat="server" Columns="4" MaxLength="4" TabIndex="11610" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1053" runat="server" Columns="4" MaxLength="4" TabIndex="11611" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV1054" runat="server" Columns="4" MaxLength="4" TabIndex="11612" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosTM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVB20" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1055" runat="server" Columns="4" MaxLength="4" TabIndex="11701" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1056" runat="server" Columns="4" MaxLength="4" TabIndex="11702" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1057" runat="server" Columns="4" MaxLength="4" TabIndex="11703" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1058" runat="server" Columns="4" MaxLength="4" TabIndex="11704" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1059" runat="server" Columns="4" MaxLength="4" TabIndex="11705" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1060" runat="server" Columns="4" MaxLength="4" TabIndex="11706" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1061" runat="server" Columns="4" MaxLength="4" TabIndex="11707" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1062" runat="server" Columns="4" MaxLength="4" TabIndex="11708" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1063" runat="server" Columns="4" MaxLength="4" TabIndex="11709" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1064" runat="server" Columns="4" MaxLength="4" TabIndex="11710" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1065" runat="server" Columns="4" MaxLength="4" TabIndex="11711" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Orila">
                    <asp:TextBox ID="txtV1066" runat="server" Columns="4" MaxLength="4" TabIndex="11712" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
             <tr>
                <td rowspan="1" style="padding-bottom: 10px; padding-top: 10px; text-align: left;" colspan="16">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Escriba el total de alumnos se encuentran en el Programa de Complementaci�n de Estudios para el Ingreso a la Educaci�n Superior (PROCEIES), desglos�ndolos por sexo y edad."
                        Width="100%"></asp:Label></td>
            </tr>
             <tr>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PROCEIES"
                        Width="40px"></asp:Label></td>
                <td rowspan="1" style="text-align: left" colspan="2" class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtVB21" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtVB22" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1067" runat="server" Columns="4" MaxLength="4" TabIndex="11901" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1068" runat="server" Columns="4" MaxLength="4" TabIndex="11902" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1069" runat="server" Columns="4" MaxLength="4" TabIndex="11903" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1070" runat="server" Columns="4" MaxLength="4" TabIndex="11904" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1071" runat="server" Columns="4" MaxLength="4" TabIndex="11905" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1072" runat="server" Columns="4" MaxLength="4" TabIndex="11906" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1073" runat="server" Columns="4" MaxLength="4" TabIndex="11907" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1074" runat="server" Columns="4" MaxLength="4" TabIndex="11908" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1075" runat="server" Columns="4" MaxLength="4" TabIndex="11909" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:TextBox ID="txtV1076" runat="server" Columns="4" MaxLength="4" TabIndex="11910" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoAlto Orila">
                    <asp:TextBox ID="txtV1077" runat="server" Columns="4" MaxLength="4" TabIndex="11911" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td rowspan="1" style="text-align: left" colspan="2" class="linaBajo">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB23" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVB24" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1078" runat="server" Columns="4" MaxLength="4" TabIndex="12001" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1079" runat="server" Columns="4" MaxLength="4" TabIndex="12002" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1080" runat="server" Columns="4" MaxLength="4" TabIndex="12003" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1081" runat="server" Columns="4" MaxLength="4" TabIndex="12004" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1082" runat="server" Columns="4" MaxLength="4" TabIndex="12005" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1083" runat="server" Columns="4" MaxLength="4" TabIndex="12006" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1084" runat="server" Columns="4" MaxLength="4" TabIndex="12007" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV1085" runat="server" Columns="4" MaxLength="4" TabIndex="12008" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1086" runat="server" Columns="4" MaxLength="4" TabIndex="12009" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1087" runat="server" Columns="4" MaxLength="4" TabIndex="12010" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV1088" runat="server" Columns="4" MaxLength="4" TabIndex="12011" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
             <tr>
                <td colspan="2" style="padding-bottom: 10px; text-align: left" class="linaBajoS">
                    <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVB25" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVB26" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1089" runat="server" Columns="4" MaxLength="4" TabIndex="12101" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1090" runat="server" Columns="4" MaxLength="4" TabIndex="12102" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1091" runat="server" Columns="4" MaxLength="4" TabIndex="12103" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1092" runat="server" Columns="4" MaxLength="4" TabIndex="12104" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1093" runat="server" Columns="4" MaxLength="4" TabIndex="12105" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1094" runat="server" Columns="4" MaxLength="4" TabIndex="12106" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1095" runat="server" Columns="4" MaxLength="4" TabIndex="12107" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1096" runat="server" Columns="4" MaxLength="4" TabIndex="12108" ></asp:TextBox></td>
                 <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1097" runat="server" Columns="4" MaxLength="4" TabIndex="12109" ></asp:TextBox></td>
                 <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV1098" runat="server" Columns="4" MaxLength="4" TabIndex="12110" ></asp:TextBox></td>
                 <td style="text-align: center;" class="linaBajoS Orila">
                    <asp:TextBox ID="txtV1099" runat="server" Columns="4" MaxLength="4" TabIndex="12111" ></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
          
          </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Gasto_911_7P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Gasto_911_7P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Anexo_911_7P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_911_7P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 14;
                MaxRow = 22;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
