<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7G(Egresados)" AutoEventWireup="true" CodeBehind="Egresados_911_7P.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7P.Egresados_911_7P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
   <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="PROFESIONAL T�CNICO MEDIO"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
       <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div></div>
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Carreras_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('Alumnos1_911_7P',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('Alumnos2_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Egresados_911_7P',true)"><a href="#" title="" class="activo"><span>EGRESADOS</span></a></li>
        <li onclick="openPage('Personal_911_7P',false)"><a href="#" title="" ><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROCEIES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    
    <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
          
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 500px" align="center">
            <tr>
                <td colspan="2" valign="top" style="text-align:justify">
                    <asp:Label ID="Label35" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                        Text="III. EGRESADOS, REPROBADOS Y REGULARIZADOS DEL CICLO ANTERIOR." Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="12px"
                        Text="(CONSIDERE LA INFORMACI�N REPORTADA EN EL FIN DE CURSOS ANTERIOR)" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba la cantidad de alumnos egresados del ciclo escolar anterior (incluya los regularizados hasta el 30 de Septiembre de este a�o), desglos�ndola por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;" class="linaBajoAlto Orila">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ALUMNOS EGRESADOS"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV388" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV389" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo">
                    <asp:TextBox ID="txtV390" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
                        <tr>
                            <td colspan="5" style="padding-bottom: 10px; padding-top: 10px; text-align: justify">
                                <asp:Label ID="Label33" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="La pregunta n�mero 2 �nicamente deber� ser contestada por las escuelas del CONALEP."
                                    Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label29" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="2. De los alumnos reportados en el punto anterior, escriba cu�ntos de ellos cursaron y aprobaron todas las asignaturas optativas complementarias para obtener la equivalencia al bachillerato."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label30" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label31" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;" class="linaBajoAlto Orila">
                    <asp:Label ID="Label32" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV883" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV884" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV885" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="3. Escriba la cantidad de alumnos, por grado, que reprobaron de 1 a 5 asignaturas el ciclo escolar anterior, desglos�ndola por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;" class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV391" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV392" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV393" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV394" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV395" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV396" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV397" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV398" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV399" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV400" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV401" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV402" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label36" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="QUINTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV403" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV404" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV405" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV406" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV407" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV408" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="4. De los alumnos reportados en el punto anterior registre el n�mero de alumnos que se regularizaron (aprobaron mediante ex�menes extraordinarios todas las asignaturas que adeudaban) al 30 de Septiembre del presente a�o, desglos�ndolo por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;" class="linaBajoAlto">
                    <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV409" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV410" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV411" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV412" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV413" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV414" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV415" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV416" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV417" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV418" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV419" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV420" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label34" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="QUINTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV421" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV422" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV423" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV424" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV425" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV426" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="5. De los alumnos reportados en la pregunta 3, escriba la cantidad de ellos que est�n inscritos en el presente ciclo escolar y contin�an como irregulares (adeudan asignaturas), seg�n sexo y el grado que cursan actualmente."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label22" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;" class="linaBajoAlto">
                    <asp:Label ID="Label23" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoAlto">
        <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
            Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV427" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="11501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV428" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="11502"></asp:TextBox></td>
                <td style="width: 93px; text-align: center" class="linaBajo Orila">
                    <asp:TextBox ID="txtV429" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="11503"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>              
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV430" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV431" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV432" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV433" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV434" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV435" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV436" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV437" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV438" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblQuinto" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="QUINTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV439" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV440" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV441" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label28" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV442" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV443" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;" class="linaBajo Orila">
                    <asp:TextBox ID="txtV444" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>             
            </tr>                        
        </table>
       
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Alumnos2_911_7P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Alumnos2_911_7P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Personal_911_7P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_7P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
         <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 4;
                MaxRow = 21;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
