<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7P(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_7P.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7P.Personal_911_7P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

  <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
   <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="PROFESIONAL T�CNICO MEDIO"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div>
    </div>
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Carreras_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li><li onclick="openPage('Alumnos1_911_7P',true)"><a href="#" title=""><span>TOTAL</span></a></li><li onclick="openPage('Alumnos2_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li><li onclick="openPage('Egresados_911_7P',true)"><a href="#" title=""><span>EGRESADOS</span></a></li><li onclick="openPage('Personal_911_7P',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li><li onclick="openPage('Planteles_911_7P',false)"><a href="#" title="" ><span>AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROCEIES</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center> 
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


    <div>
    <br/>
        <table>
            <tr>
                <td style="width: 340px; text-align: center;">
                    <table id="TABLE1" style="text-align: center" >
                        <tr>
                            <td colspan="17" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="IV. PERSONAL POR FUNCI�N"
                                    Width="250px" Font-Size="16px"></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="17" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="17" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de personal que realiza funciones de directivo (con y sin grupo), docente, docente especial (profesor de educaci�n f�sica, actividades art�sticas, tecnol�gicas y de idiomas), y administrativo, auxiliar y de servicios, independientemente de su nombramiento, tipo y fuente de pago, desgl�selos seg�n su funci�n, nivel m�ximo de estudios y sexo."
                                    Width="1195px"></asp:Label><br />
                                <br />
                                <asp:Label ID="lblNota1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Notas:"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="a) Si una persona desempe�a dos o m�s funciones an�tela en aquella a la que dedique m�s tiempo"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="lblNota3" runat="server" CssClass="lblRojo" Text="b) Si en la tabla corresponde al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente o en OTROS"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="c) Considere exclusivamente al personal que labora en el turno al que se refiere este cuestionario, independientemente de que labore en otro turno."
                                    Width="1195px"></asp:Label><br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="2" style="width: 215px; height: 3px; text-align: center; padding-right: 5px; padding-left: 5px;">
                                <asp:Label ID="lblNivelEd" runat="server" CssClass="lblRojo" Text="NIVEL EDUCATIVO" Width="200px"></asp:Label></td>
                            <td colspan="4" rowspan="" style="text-align: center;" class="linaBajoAlto">
                                <asp:Label ID="lblPersDir" runat="server" CssClass="lblRojo" Text="PERSONAL DIRECTIVO" Width="141px"></asp:Label></td>
                            <td colspan="2" rowspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPersDocente" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE" Width="85px"></asp:Label></td>
                            <td  colspan="8" rowspan="" class="linaBajoAlto">
                                <asp:Label ID="lblPersDocEsp" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE ESPECIAL"
                                    Width="254px"></asp:Label></td>
                            <td colspan="2" rowspan="2" style="text-align: center" class="linaBajoAlto Orila">
                                <asp:Label ID="lblPersAdmin" runat="server" CssClass="lblRojo" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS" Height="54px" Width="95px"></asp:Label></td>
                            <td class="Orila">&nbsp;</td>                   
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPersDirCG" runat="server" CssClass="lblGrisTit" Text="CON GRUPO" Width="70px"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPersDirSG" runat="server" CssClass="lblGrisTit" Text="SIN GRUPO" Width="70px"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblDocEdFis" runat="server" CssClass="lblGrisTit" Text="PROFESORES DE EDUCACI�N F�SICA" Width="110px"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblDocActArt" runat="server" CssClass="lblGrisTit" Text="PROFESORES DE ACTIVIDADES ART�STICAS"
                                    Width="100px"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblDocActTec" runat="server" CssClass="lblGrisTit" Text="PROFESORES DE ACTIVIDADES TECNOL�GICAS" Width="100px"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblDocIdiom" runat="server" CssClass="lblGrisTit" Text="PROFESORES DE IDIOMAS" Width="95px"></asp:Label></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="padding-right: 5px; width: 215px">
                            </td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblPersDirCGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 66px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblPersDirCGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblPersDirSGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblPersDirSGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblPersDocenteH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblPersDocenteM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblDocEdFisH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblDocEdFisM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblDocActArtH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 62px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblDocActArtM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblDocActTecH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblDocActTecM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 59px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblDocIdiomH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblDocIdiomM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:Label ID="lblPersAdminH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 54px; height: 26px" class="linaBajo Orila">
                                <asp:Label ID="lblPersAdminM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 215px; padding-right: 5px; height: 36px; text-align: left;" class="linaBajoAlto">
                                <asp:Label ID="lblPrimInc" runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td style="width: 59px; height: 36px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV445" runat="server" Columns="2" TabIndex="10101" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV446" runat="server" Columns="2" TabIndex="10102" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV447" runat="server" Columns="2" TabIndex="10103" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV448" runat="server" Columns="2" TabIndex="10104" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV449" runat="server" Columns="4" TabIndex="10105" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV450" runat="server" Columns="4" TabIndex="10106" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV451" runat="server" Columns="2" TabIndex="10107" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV452" runat="server" Columns="2" TabIndex="10108" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV453" runat="server" Columns="2" TabIndex="10109" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV454" runat="server" Columns="2" TabIndex="10110" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV455" runat="server" Columns="2" TabIndex="10111" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV456" runat="server" Columns="2" TabIndex="10112" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV457" runat="server" Columns="2" TabIndex="10113" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV458" runat="server" Columns="2" TabIndex="10114" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV459" runat="server" Columns="3" TabIndex="10115" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV460" runat="server" Columns="3" TabIndex="10116" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="PRIMARIA TERMINADA"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV461" runat="server" Columns="2" TabIndex="10201" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV462" runat="server" Columns="2" TabIndex="10202" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV463" runat="server" Columns="2" TabIndex="10203" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV464" runat="server" Columns="2" TabIndex="10204" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV465" runat="server" Columns="4" TabIndex="10205" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV466" runat="server" Columns="4" TabIndex="10206" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV467" runat="server" Columns="2" TabIndex="10207" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV468" runat="server" Columns="2" TabIndex="10208" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV469" runat="server" Columns="2" TabIndex="10209" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV470" runat="server" Columns="2" TabIndex="10210" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV471" runat="server" Columns="2" TabIndex="10211" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV472" runat="server" Columns="2" TabIndex="10212" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV473" runat="server" Columns="2" TabIndex="10213" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV474" runat="server" Columns="2" TabIndex="10214" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV475" runat="server" Columns="3" TabIndex="10215" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV476" runat="server" Columns="3" TabIndex="10216" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 215px; margin-right: 5px; text-align: left;" class="linaBajo">
                            <asp:Label ID="lblSecInc" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV477" runat="server" Columns="2" TabIndex="10301" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV478" runat="server" Columns="2" TabIndex="10302" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV479" runat="server" Columns="2" TabIndex="10303" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV480" runat="server" Columns="2" TabIndex="10304" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV481" runat="server" Columns="4" TabIndex="10305" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV482" runat="server" Columns="4" TabIndex="10306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV483" runat="server" Columns="2" TabIndex="10307" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV484" runat="server" Columns="2" TabIndex="10308" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV485" runat="server" Columns="2" TabIndex="10309" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV486" runat="server" Columns="2" TabIndex="10310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV487" runat="server" Columns="2" TabIndex="10311" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV488" runat="server" Columns="2" TabIndex="10312" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV489" runat="server" Columns="2" TabIndex="10313" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV490" runat="server" Columns="2" TabIndex="10314" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV491" runat="server" Columns="3" TabIndex="10315" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV492" runat="server" Columns="3" TabIndex="10316" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblSecTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="SECUNDARIA TERMINADA"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV493" runat="server" Columns="2" TabIndex="10401" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV494" runat="server" Columns="2" TabIndex="10402" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV495" runat="server" Columns="2" TabIndex="10403" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV496" runat="server" Columns="2" TabIndex="10404" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV497" runat="server" Columns="4" TabIndex="10405" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV498" runat="server" Columns="4" TabIndex="10406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV499" runat="server" Columns="2" TabIndex="10407" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV500" runat="server" Columns="2" TabIndex="10408" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV501" runat="server" Columns="2" TabIndex="10409" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV502" runat="server" Columns="2" TabIndex="10410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV503" runat="server" Columns="2" TabIndex="10411" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV504" runat="server" Columns="2" TabIndex="10412" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV505" runat="server" Columns="2" TabIndex="10413" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV506" runat="server" Columns="2" TabIndex="10414" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV507" runat="server" Columns="3" TabIndex="10415" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV508" runat="server" Columns="3" TabIndex="10416" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblProfTec" runat="server" CssClass="lblGrisTit" Height="17px" Text="PROFESIONAL T�CNICO"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV509" runat="server" Columns="2" TabIndex="10501" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV510" runat="server" Columns="2" TabIndex="10502" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV511" runat="server" Columns="2" TabIndex="10503" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV512" runat="server" Columns="2" TabIndex="10504" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV513" runat="server" Columns="4" TabIndex="10505" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV514" runat="server" Columns="4" TabIndex="10506" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV515" runat="server" Columns="2" TabIndex="10507" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV516" runat="server" Columns="2" TabIndex="10508" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV517" runat="server" Columns="2" TabIndex="10509" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV518" runat="server" Columns="2" TabIndex="10510" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV519" runat="server" Columns="2" TabIndex="10511" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV520" runat="server" Columns="2" TabIndex="10512" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV521" runat="server" Columns="2" TabIndex="10513" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV522" runat="server" Columns="2" TabIndex="10514" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo">
                                <asp:TextBox ID="txtV523" runat="server" Columns="3" TabIndex="10515" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px" class="linaBajo Orila">
                                <asp:TextBox ID="txtV524" runat="server" Columns="3" TabIndex="10516" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblBachInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO INCOMPLETO"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV525" runat="server" Columns="2" TabIndex="10601" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV526" runat="server" Columns="2" TabIndex="10602" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV527" runat="server" Columns="2" TabIndex="10603" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV528" runat="server" Columns="2" TabIndex="10604" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV529" runat="server" Columns="4" TabIndex="10605" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV530" runat="server" Columns="4" TabIndex="10606" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV531" runat="server" Columns="2" TabIndex="10607" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV532" runat="server" Columns="2" TabIndex="10608" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV533" runat="server" Columns="2" TabIndex="10609" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV534" runat="server" Columns="2" TabIndex="10610" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV535" runat="server" Columns="2" TabIndex="10611" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV536" runat="server" Columns="2" TabIndex="10612" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV537" runat="server" Columns="2" TabIndex="10613" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV538" runat="server" Columns="2" TabIndex="10614" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV539" runat="server" Columns="3" TabIndex="10615" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV540" runat="server" Columns="3" TabIndex="10616" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblBachTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO TERMINADO"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV541" runat="server" Columns="2" TabIndex="10701" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV542" runat="server" Columns="2" TabIndex="10702" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV543" runat="server" Columns="2" TabIndex="10703" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV544" runat="server" Columns="2" TabIndex="10704" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV545" runat="server" Columns="4" TabIndex="10705" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV546" runat="server" Columns="4" TabIndex="10706" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV547" runat="server" Columns="2" TabIndex="10707" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV548" runat="server" Columns="2" TabIndex="10708" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV549" runat="server" Columns="2" TabIndex="10709" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV550" runat="server" Columns="2" TabIndex="10710" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV551" runat="server" Columns="2" TabIndex="10711" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV552" runat="server" Columns="2" TabIndex="10712" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV553" runat="server" Columns="2" TabIndex="10713" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV554" runat="server" Columns="2" TabIndex="10714" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV555" runat="server" Columns="3" TabIndex="10715" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV556" runat="server" Columns="3" TabIndex="10716" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPreeInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR INCOMPLETA"
                                    Width="215px"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV557" runat="server" Columns="2" TabIndex="10801" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV558" runat="server" Columns="2" TabIndex="10802" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV559" runat="server" Columns="2" TabIndex="10803" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV560" runat="server" Columns="2" TabIndex="10804" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV561" runat="server" Columns="4" TabIndex="10805" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV562" runat="server" Columns="4" TabIndex="10806" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV563" runat="server" Columns="2" TabIndex="10807" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV564" runat="server" Columns="2" TabIndex="10808" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV565" runat="server" Columns="2" TabIndex="10809" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV566" runat="server" Columns="2" TabIndex="10810" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV567" runat="server" Columns="2" TabIndex="10811" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV568" runat="server" Columns="2" TabIndex="10812" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV569" runat="server" Columns="2" TabIndex="10813" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV570" runat="server" Columns="2" TabIndex="10814" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV571" runat="server" Columns="3" TabIndex="10815" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV572" runat="server" Columns="3" TabIndex="10816" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPreeTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR TERMINADA"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV573" runat="server" Columns="2" TabIndex="10901" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV574" runat="server" Columns="2" TabIndex="10902" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV575" runat="server" Columns="2" TabIndex="10903" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV576" runat="server" Columns="2" TabIndex="10904" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV577" runat="server" Columns="4" TabIndex="10905" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV578" runat="server" Columns="4" TabIndex="10906" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV579" runat="server" Columns="2" TabIndex="10907" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV580" runat="server" Columns="2" TabIndex="10908" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV581" runat="server" Columns="2" TabIndex="10909" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV582" runat="server" Columns="2" TabIndex="10910" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV583" runat="server" Columns="2" TabIndex="10911" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV584" runat="server" Columns="2" TabIndex="10912" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV585" runat="server" Columns="2" TabIndex="10913" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV586" runat="server" Columns="2" TabIndex="10914" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV587" runat="server" Columns="3" TabIndex="10915" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV588" runat="server" Columns="3" TabIndex="10916" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 15px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPrimInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA INCOMPLETA"></asp:Label></td>
                            <td style="width: 59px; height: 15px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV589" runat="server" Columns="2" TabIndex="11001" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV590" runat="server" Columns="2" TabIndex="11002" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV591" runat="server" Columns="2" TabIndex="11003" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV592" runat="server" Columns="2" TabIndex="11004" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV593" runat="server" Columns="4" TabIndex="11005" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV594" runat="server" Columns="4" TabIndex="11006" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV595" runat="server" Columns="2" TabIndex="11007" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV596" runat="server" Columns="2" TabIndex="11008" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV597" runat="server" Columns="2" TabIndex="11009" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV598" runat="server" Columns="2" TabIndex="11010" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV599" runat="server" Columns="2" TabIndex="11011" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV600" runat="server" Columns="2" TabIndex="11012" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV601" runat="server" Columns="2" TabIndex="11013" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV602" runat="server" Columns="2" TabIndex="11014" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV603" runat="server" Columns="3" TabIndex="11015" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV604" runat="server" Columns="3" TabIndex="11016" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA TERMINADA"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV605" runat="server" Columns="2" TabIndex="11101" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV606" runat="server" Columns="2" TabIndex="11102" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV607" runat="server" Columns="2" TabIndex="11103" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV608" runat="server" Columns="2" TabIndex="11104" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV609" runat="server" Columns="4" TabIndex="11105" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV610" runat="server" Columns="4" TabIndex="11106" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV611" runat="server" Columns="2" TabIndex="11107" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV612" runat="server" Columns="2" TabIndex="11108" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV613" runat="server" Columns="2" TabIndex="11109" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV614" runat="server" Columns="2" TabIndex="11110" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV615" runat="server" Columns="2" TabIndex="11111" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV616" runat="server" Columns="2" TabIndex="11112" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV617" runat="server" Columns="2" TabIndex="11113" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV618" runat="server" Columns="2" TabIndex="11114" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV619" runat="server" Columns="3" TabIndex="11115" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV620" runat="server" Columns="3" TabIndex="11116" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormSupInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR INCOMPLETA"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV621" runat="server" Columns="2" TabIndex="11201" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV622" runat="server" Columns="2" TabIndex="11202" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV623" runat="server" Columns="2" TabIndex="11203" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV624" runat="server" Columns="2" TabIndex="11204" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV625" runat="server" Columns="4" TabIndex="11205" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV626" runat="server" Columns="4" TabIndex="11206" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV627" runat="server" Columns="2" TabIndex="11207" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV628" runat="server" Columns="2" TabIndex="11208" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV629" runat="server" Columns="2" TabIndex="11209" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV630" runat="server" Columns="2" TabIndex="11210" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV631" runat="server" Columns="2" TabIndex="11211" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV632" runat="server" Columns="2" TabIndex="11212" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV633" runat="server" Columns="2" TabIndex="11213" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV634" runat="server" Columns="2" TabIndex="11214" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV635" runat="server" Columns="3" TabIndex="11215" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV636" runat="server" Columns="3" TabIndex="11216" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormSupPas" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, PASANTE"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV637" runat="server" Columns="2" TabIndex="11301" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV638" runat="server" Columns="2" TabIndex="11302" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV639" runat="server" Columns="2" TabIndex="11303" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV640" runat="server" Columns="2" TabIndex="11304" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV641" runat="server" Columns="4" TabIndex="11305" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV642" runat="server" Columns="4" TabIndex="11306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV643" runat="server" Columns="2" TabIndex="11307" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV644" runat="server" Columns="2" TabIndex="11308" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV645" runat="server" Columns="2" TabIndex="11309" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV646" runat="server" Columns="2" TabIndex="11310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV647" runat="server" Columns="2" TabIndex="11311" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV648" runat="server" Columns="2" TabIndex="11312" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV649" runat="server" Columns="2" TabIndex="11313" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV650" runat="server" Columns="2" TabIndex="11314" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV651" runat="server" Columns="3" TabIndex="11315" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV652" runat="server" Columns="3" TabIndex="11316" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 12px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormSupTit" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, TITULADO"></asp:Label></td>
                            <td style="width: 59px; height: 12px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV653" runat="server" Columns="2" TabIndex="11401" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV654" runat="server" Columns="2" TabIndex="11402" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV655" runat="server" Columns="2" TabIndex="11403" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV656" runat="server" Columns="2" TabIndex="11404" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV657" runat="server" Columns="4" TabIndex="11405" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV658" runat="server" Columns="4" TabIndex="11406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV659" runat="server" Columns="2" TabIndex="11407" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV660" runat="server" Columns="2" TabIndex="11408" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV661" runat="server" Columns="2" TabIndex="11409" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV662" runat="server" Columns="2" TabIndex="11410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV663" runat="server" Columns="2" TabIndex="11411" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV664" runat="server" Columns="2" TabIndex="11412" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV665" runat="server" Columns="2" TabIndex="11413" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV666" runat="server" Columns="2" TabIndex="11414" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV667" runat="server" Columns="3" TabIndex="11415" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV668" runat="server" Columns="3" TabIndex="11416" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblLicInc" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA INCOMPLETA"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV669" runat="server" Columns="2" TabIndex="11501" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV670" runat="server" Columns="2" TabIndex="11502" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV671" runat="server" Columns="2" TabIndex="11503" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV672" runat="server" Columns="2" TabIndex="11504" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV673" runat="server" Columns="4" TabIndex="11505" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV674" runat="server" Columns="4" TabIndex="11506" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV675" runat="server" Columns="2" TabIndex="11507" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV676" runat="server" Columns="2" TabIndex="11508" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV677" runat="server" Columns="2" TabIndex="11509" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV678" runat="server" Columns="2" TabIndex="11510" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV679" runat="server" Columns="2" TabIndex="11511" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV680" runat="server" Columns="2" TabIndex="11512" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV681" runat="server" Columns="2" TabIndex="11513" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV682" runat="server" Columns="2" TabIndex="11514" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV683" runat="server" Columns="3" TabIndex="11515" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV684" runat="server" Columns="3" TabIndex="11516" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblLicPas" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, PASANTE"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV685" runat="server" Columns="2" TabIndex="11601" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV686" runat="server" Columns="2" TabIndex="11602" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV687" runat="server" Columns="2" TabIndex="11603" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV688" runat="server" Columns="2" TabIndex="11604" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV689" runat="server" Columns="4" TabIndex="11605" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV690" runat="server" Columns="4" TabIndex="11606" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV691" runat="server" Columns="2" TabIndex="11607" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV692" runat="server" Columns="2" TabIndex="11608" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV693" runat="server" Columns="2" TabIndex="11609" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV694" runat="server" Columns="2" TabIndex="11610" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV695" runat="server" Columns="2" TabIndex="11611" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV696" runat="server" Columns="2" TabIndex="11612" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV697" runat="server" Columns="2" TabIndex="11613" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV698" runat="server" Columns="2" TabIndex="11614" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV699" runat="server" Columns="3" TabIndex="11615" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV700" runat="server" Columns="3" TabIndex="11616" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 21px; text-align: left" class="linaBajo">
                            <asp:Label ID="lblLicTit" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO"></asp:Label></td>
                            <td style="width: 59px; height: 21px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV701" runat="server" Columns="2" TabIndex="11701" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV702" runat="server" Columns="2" TabIndex="11702" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV703" runat="server" Columns="2" TabIndex="11703" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV704" runat="server" Columns="2" TabIndex="11704" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV705" runat="server" Columns="4" TabIndex="11705" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV706" runat="server" Columns="4" TabIndex="11706" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV707" runat="server" Columns="2" TabIndex="11707" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV708" runat="server" Columns="2" TabIndex="11708" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV709" runat="server" Columns="2" TabIndex="11709" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV710" runat="server" Columns="2" TabIndex="11710" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV711" runat="server" Columns="2" TabIndex="11711" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV712" runat="server" Columns="2" TabIndex="11712" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV713" runat="server" Columns="2" TabIndex="11713" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV714" runat="server" Columns="2" TabIndex="11714" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV715" runat="server" Columns="3" TabIndex="11715" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV716" runat="server" Columns="3" TabIndex="11716" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblMaestInc" runat="server" CssClass="lblGrisTit" Text="MAESTR�A INCOMPLETA"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV717" runat="server" Columns="2" TabIndex="11801" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV718" runat="server" Columns="2" TabIndex="11802" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV719" runat="server" Columns="2" TabIndex="11803" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV720" runat="server" Columns="2" TabIndex="11804" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV721" runat="server" Columns="4" TabIndex="11805" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV722" runat="server" Columns="4" TabIndex="11806" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV723" runat="server" Columns="2" TabIndex="11807" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV724" runat="server" Columns="2" TabIndex="11808" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV725" runat="server" Columns="2" TabIndex="11809" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV726" runat="server" Columns="2" TabIndex="11810" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV727" runat="server" Columns="2" TabIndex="11811" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV728" runat="server" Columns="2" TabIndex="11812" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV729" runat="server" Columns="2" TabIndex="11813" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV730" runat="server" Columns="2" TabIndex="11814" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV731" runat="server" Columns="3" TabIndex="11815" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV732" runat="server" Columns="3" TabIndex="11816" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblMaestGrad" runat="server" CssClass="lblGrisTit" Text="MAESTR�A, GRADUADO"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV733" runat="server" Columns="2" TabIndex="11901" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV734" runat="server" Columns="2" TabIndex="11902" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV735" runat="server" Columns="2" TabIndex="11903" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV736" runat="server" Columns="2" TabIndex="11904" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV737" runat="server" Columns="4" TabIndex="11905" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV738" runat="server" Columns="4" TabIndex="11906" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV739" runat="server" Columns="2" TabIndex="11907" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV740" runat="server" Columns="2" TabIndex="11908" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV741" runat="server" Columns="2" TabIndex="11909" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV742" runat="server" Columns="2" TabIndex="11910" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV743" runat="server" Columns="2" TabIndex="11911" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV744" runat="server" Columns="2" TabIndex="11912" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV745" runat="server" Columns="2" TabIndex="11913" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV746" runat="server" Columns="2" TabIndex="11914" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV747" runat="server" Columns="3" TabIndex="11915" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV748" runat="server" Columns="3" TabIndex="11916" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblDocInc" runat="server" CssClass="lblGrisTit" Text="DOCTORADO INCOMPLETO"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV749" runat="server" Columns="2" TabIndex="12001" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV750" runat="server" Columns="2" TabIndex="12002" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV751" runat="server" Columns="2" TabIndex="12003" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV752" runat="server" Columns="2" TabIndex="12004" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV753" runat="server" Columns="4" TabIndex="12005" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV754" runat="server" Columns="4" TabIndex="12006" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV755" runat="server" Columns="2" TabIndex="12007" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV756" runat="server" Columns="2" TabIndex="12008" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV757" runat="server" Columns="2" TabIndex="12009" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV758" runat="server" Columns="2" TabIndex="12010" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV759" runat="server" Columns="2" TabIndex="12011" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV760" runat="server" Columns="2" TabIndex="12012" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV761" runat="server" Columns="2" TabIndex="12013" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV762" runat="server" Columns="2" TabIndex="12014" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV763" runat="server" Columns="3" TabIndex="12015" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV764" runat="server" Columns="3" TabIndex="12016" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblDocGrad" runat="server" CssClass="lblGrisTit" Text="DOCTORADO, GRADUADO"></asp:Label></td>
                            <td style="width: 59px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV765" runat="server" Columns="2" TabIndex="12101" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV766" runat="server" Columns="2" TabIndex="12102" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV767" runat="server" Columns="2" TabIndex="12103" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV768" runat="server" Columns="2" TabIndex="12104" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV769" runat="server" Columns="4" TabIndex="12105" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV770" runat="server" Columns="4" TabIndex="12106" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV771" runat="server" Columns="2" TabIndex="12107" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV772" runat="server" Columns="2" TabIndex="12108" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV773" runat="server" Columns="2" TabIndex="12109" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV774" runat="server" Columns="2" TabIndex="12110" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV775" runat="server" Columns="2" TabIndex="12111" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV776" runat="server" Columns="2" TabIndex="12112" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV777" runat="server" Columns="2" TabIndex="12113" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV778" runat="server" Columns="2" TabIndex="12114" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV779" runat="server" Columns="3" TabIndex="12115" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV780" runat="server" Columns="3" TabIndex="12116" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 17px; text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS*"></asp:Label><br />
                                &nbsp;<asp:Label ID="lblEspecifique" runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE"></asp:Label></td>
                            <td style="width: 67px; height: 17px; text-align: center; vertical-align: top;">
                            </td>
                            <td style="width: 66px; height: 17px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 17px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 17px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 17px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 17px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 17px">
                            </td>
                            <td style="width: 67px; height: 17px">
                            </td>
                            <td style="width: 67px; height: 17px">
                            </td>
                            <td style="width: 62px; height: 17px">
                            </td>
                            <td style="width: 67px; height: 17px">
                            </td>
                            <td style="width: 54px; height: 17px">
                            </td>
                            <td style="width: 59px; height: 17px; text-align: center;">
                            </td>
                            <td style="width: 54px; height: 17px">
                            </td>
                            <td style="width: 54px; height: 17px">
                            </td>
                            <td style="width: 54px; height: 17px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" class="linaBajoAlto">
                                <asp:TextBox ID="txtV781" runat="server" Columns="30" TabIndex="12201" MaxLength="30" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajoAlto">
                                <asp:TextBox ID="txtV782" runat="server" Columns="2" TabIndex="12202" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajoAlto">
                                <asp:TextBox ID="txtV783" runat="server" Columns="2" TabIndex="12203" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajoAlto">
                                <asp:TextBox ID="txtV784" runat="server" Columns="2" TabIndex="12204" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajoAlto">
                                <asp:TextBox ID="txtV785" runat="server" Columns="2" TabIndex="12205" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV786" runat="server" Columns="4" TabIndex="12206" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV787" runat="server" Columns="4" TabIndex="12207" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV788" runat="server" Columns="2" TabIndex="12208" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV789" runat="server" Columns="2" TabIndex="12209" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV790" runat="server" Columns="2" TabIndex="12210" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV791" runat="server" Columns="2" TabIndex="12211" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV792" runat="server" Columns="2" TabIndex="12212" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV793" runat="server" Columns="2" TabIndex="12213" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV794" runat="server" Columns="2" TabIndex="12214" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV795" runat="server" Columns="2" TabIndex="12215" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV796" runat="server" Columns="3" TabIndex="12216" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto Orila">
                                <asp:TextBox ID="txtV797" runat="server" Columns="3" TabIndex="12217" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" class="linaBajo">
                                <asp:TextBox ID="txtV798" runat="server" Columns="30" TabIndex="12301" MaxLength="30" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV799" runat="server" Columns="2" TabIndex="12302" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV800" runat="server" Columns="2" TabIndex="12303" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV801" runat="server" Columns="2" TabIndex="12304" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV802" runat="server" Columns="2" TabIndex="12305" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV803" runat="server" Columns="4" TabIndex="12306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV804" runat="server" Columns="4" TabIndex="12307" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV805" runat="server" Columns="2" TabIndex="12308" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV806" runat="server" Columns="2" TabIndex="12309" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV807" runat="server" Columns="2" TabIndex="12310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV808" runat="server" Columns="2" TabIndex="12311" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV809" runat="server" Columns="2" TabIndex="12312" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV810" runat="server" Columns="2" TabIndex="12313" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV811" runat="server" Columns="2" TabIndex="12314" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV812" runat="server" Columns="2" TabIndex="12315" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV813" runat="server" Columns="3" TabIndex="12316" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo Orila">
                                <asp:TextBox ID="txtV814" runat="server" Columns="3" TabIndex="12317" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" class="linaBajo">
                                <asp:TextBox ID="txtV815" runat="server" Columns="30" TabIndex="12401" MaxLength="30" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV816" runat="server" Columns="2" TabIndex="12402" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV817" runat="server" Columns="2" TabIndex="12403" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV818" runat="server" Columns="2" TabIndex="12404" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV819" runat="server" Columns="2" TabIndex="12405" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV820" runat="server" Columns="4" TabIndex="12406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV821" runat="server" Columns="4" TabIndex="12407" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV822" runat="server" Columns="2" TabIndex="12408" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV823" runat="server" Columns="2" TabIndex="12409" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV824" runat="server" Columns="2" TabIndex="12410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV825" runat="server" Columns="2" TabIndex="12411" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV826" runat="server" Columns="2" TabIndex="12412" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV827" runat="server" Columns="2" TabIndex="12413" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV828" runat="server" Columns="2" TabIndex="12414" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV829" runat="server" Columns="2" TabIndex="12415" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV830" runat="server" Columns="3" TabIndex="12416" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo Orila">
                                <asp:TextBox ID="txtV831" runat="server" Columns="3" TabIndex="12417" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblSubtotales" runat="server" CssClass="lblGrisTit" Text="SUBTOTALES"></asp:Label></td>
                            <td style="text-align: center;" class="linaBajo">
                                            <asp:TextBox ID="txtV832" runat="server" Columns="2" TabIndex="12501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                            <asp:TextBox ID="txtV833" runat="server" Columns="2" TabIndex="12502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                            <asp:TextBox ID="txtV834" runat="server" Columns="2" TabIndex="12503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                            <asp:TextBox ID="txtV835" runat="server" Columns="2" TabIndex="12504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV836" runat="server" Columns="4" TabIndex="12505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV837" runat="server" Columns="4" TabIndex="12506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV838" runat="server" Columns="2" TabIndex="12507" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV839" runat="server" Columns="2" TabIndex="12508" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV840" runat="server" Columns="2" TabIndex="12509" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV841" runat="server" Columns="2" TabIndex="12510" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV842" runat="server" Columns="2" TabIndex="12511" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV843" runat="server" Columns="2" TabIndex="12512" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV844" runat="server" Columns="2" TabIndex="12513" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV845" runat="server" Columns="2" TabIndex="12514" CssClass="lblNegro" MaxLength="2" ></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV846" runat="server" Columns="3" TabIndex="12515" CssClass="lblNegro" MaxLength="3" ></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV847" runat="server" Columns="3" TabIndex="12516" CssClass="lblNegro" MaxLength="3" ></asp:TextBox></td>
                            <td class="Orila">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="text-align: right">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL PERSONAL (Suma de subtotales)"
                                    Width="275px"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                            <asp:TextBox ID="txtV848" runat="server" Columns="4" TabIndex="12601" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="text-align: left">
                                <table style="text-align: center">
                                    <tr>
                                        <td colspan="1" style="padding-bottom: 20px; text-align: left">
                                            <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="2. Sume el personal directivo con grupo, personal docente y personal docente especial, y an�telo seg�n el tiempo que dedica a la funci�n acad�mica."
                                                Width="900px"></asp:Label><br />
                                            <asp:Label ID="lblNota4" runat="server" CssClass="lblRojo" Text="NOTA: Si en la instituci�n no se utiliza el t�rmino tres cuartos de tiempo, no lo considere."
                                                Width="800px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" style="padding-bottom: 20px; text-align: left">
                                            <table>
                                                <tr>
                                                    <td colspan="3">
                                            <asp:Label ID="lblTiempoCompleto" runat="server" CssClass="lblGrisTit" Text="TIEMPO COMPLETO"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV849" runat="server" Columns="4" TabIndex="12701" CssClass="lblNegro" MaxLength="3"
                                                ></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                            <asp:Label ID="lblTiempo3Cuartos" runat="server" CssClass="lblGrisTit" Text="TRES CUARTOS DE TIEMPO"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV850" runat="server" Columns="4" TabIndex="12801" CssClass="lblNegro" MaxLength="3"
                                                ></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                            <asp:Label ID="lblMedioTiempo" runat="server" CssClass="lblGrisTit" Text="MEDIO TIEMPO"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV851" runat="server" Columns="4" TabIndex="12901" CssClass="lblNegro" MaxLength="3"
                                                ></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                            <asp:Label ID="lblPorHoras" runat="server" CssClass="lblGrisTit" Text="POR HORAS"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV852" runat="server" Columns="4" TabIndex="13001" CssClass="lblNegro" MaxLength="3"
                                                ></asp:TextBox></td>
                                                </tr>
                                                <tr valign="top">
                                                    <td colspan="3">
                                                        <table>
                                                            <tr>
                                                                <td valign="top">
                                            <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                                                                <td style="text-align:justify">
                                                        <asp:Label ID="lblNotaTotal4" runat="server" CssClass="lblGrisTit" Text="(Este total debe de coincidir con la suma de personal directivo con grupo, personal docente y personal docente especial reportado en la pregunta 1 de esta secci�n.)"
                                                Width="330px"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                            <asp:TextBox ID="txtV853" runat="server" Columns="4" TabIndex="13101" CssClass="lblNegro" MaxLength="4"
                                                ></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: text-top; text-align: left">
                                            <br />
                                            </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('Egresados_911_7P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Egresados_911_7P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                 &nbsp;
                </td>
                <td ><span  onclick="OpenPageCharged('Planteles_911_7P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Planteles_911_7P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
         <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 18;
                MaxRow = 35;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                 function OpenPageCharged(pagina){
                    Guion('781');
                    Guion('798');
                    Guion('815');
                    openPage(pagina);
                }
                
                function Guion(variable){
                    var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                    if (txtv != null){
                       if (txtv.value == "")
                          txtv.value = "_";  
                    }
                }
 		Disparador(<%=hidDisparador.Value %>);
          
function TABLE2_onclick() {

}

        </script> 
</asp:Content>
