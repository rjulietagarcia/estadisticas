<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-NE1(Alumnos Atendidos)" AutoEventWireup="true" CodeBehind="Atendidos_EI_NE1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.EI_NE1.Atendidos_EI_NE1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"   Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N INICIAL NO ESCOLARIZADA"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div></div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_EI_NE1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Atendidos_EI_NE1',true)"><a href="#" title="" class="activo"><span>GRUPOS Y NI�OS ATENDIDOS</span></a></li>
        <li onclick="openPage('Personal_EI_NE1',false)"><a href="#" title="" ><span>PADRES Y PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AVANCE Y ESPACIOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
     <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
          
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


            <table style="width: 800px">
        <tr>
            <td style="width: 997px;text-align:justify">
                <asp:Label ID="lblUBICACION" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="I. UBICACI�N DE GRUPOS"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 997px; text-align:justify">
                <asp:Label ID="lblInstruccionI1" runat="server" CssClass="lblRojo" Text="1. Escriba el nombre del municipio o delegaci�n  y la localidad o colonia en la cual est�n los gupos que se atienden en el m�dulo, y el n�mero de grupos."
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 997px; text-align: center">
        <table>
            <tr>
                <td style="height: 25px" nowrap="noWrap">
                </td>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lblMUNICIPIO" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUNICIPIO O DELEGACI�N"
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lblLOCALIDAD" runat="server" CssClass="lblGrisTit" Height="17px" Text="LOCALIDAD O COLONIA"
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lblGRUPOS" runat="server" CssClass="lblGrisTit" Height="17px" Text="NUM. DE GRUPOS"
                        Width="50px"></asp:Label></td>
            </tr>
            <tr>
                <td style="height: 25px" nowrap="noWrap">
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI1" runat="server" CssClass="lblGrisTit" Text="1"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV1" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  TabIndex="10201"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV2" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  TabIndex="10202"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV3" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"    TabIndex="10203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI2" runat="server" CssClass="lblGrisTit" Text="2"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV4" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  TabIndex="10301"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV5" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  TabIndex="10302"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV6" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"    TabIndex="10303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI3" runat="server" CssClass="lblGrisTit" Text="3"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV7" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  TabIndex="10401"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV8" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  TabIndex="10402"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV9" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"    TabIndex="10403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI4" runat="server" CssClass="lblGrisTit" Text="4"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV10" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV11" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV12" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="10503"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI5" runat="server" CssClass="lblGrisTit" Text="5"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV13" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV14" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV15" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="10603"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI6" runat="server" CssClass="lblGrisTit" Text="6"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV16" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV17" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV18" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="10703"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right; height: 26px;">
                    <asp:Label ID="lblI7" runat="server" CssClass="lblGrisTit" Text="7"
                        Width="30px"></asp:Label></td>
                <td style="height: 26px">
                    <asp:TextBox ID="txtV19" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="height: 26px">
                    <asp:TextBox ID="txtV20" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="height: 26px">
                    <asp:TextBox ID="txtV21" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="10803"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI8" runat="server" CssClass="lblGrisTit" Text="8"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV22" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV23" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV24" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="10903"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI9" runat="server" CssClass="lblGrisTit" Text="9"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV25" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV26" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV27" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11003"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI10" runat="server" CssClass="lblGrisTit" Text="10"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV28" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV29" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV30" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11103"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI11" runat="server" CssClass="lblGrisTit" Text="11"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV31" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV32" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV33" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI12" runat="server" CssClass="lblGrisTit" Text="12"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV34" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV35" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV36" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI13" runat="server" CssClass="lblGrisTit" Text="13"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV37" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV38" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV39" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI14" runat="server" CssClass="lblGrisTit" Text="14"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV40" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV41" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV42" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11503"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI15" runat="server" CssClass="lblGrisTit" Text="15"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV43" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV44" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV45" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11603"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI16" runat="server" CssClass="lblGrisTit" Text="16"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV46" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV47" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV48" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11703"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI17" runat="server" CssClass="lblGrisTit" Text="17"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV49" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV50" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV51" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11803"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblI18" runat="server" CssClass="lblGrisTit" Text="18"
                        Width="30px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV52" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV53" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV54" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"   TabIndex="11903"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblITotal" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV55" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro"   TabIndex="12001"></asp:TextBox></td>
            </tr>
        </table>
            </td>
        </tr>
        <tr>
            <td style="width: 997px;text-align:justify">
                    <asp:Label ID="lblNI�OS" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="II. NI�OS POR EDAD Y SEXO"
                        Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 997px; text-align:justify">
                    <asp:Label ID="lblInstruccionII1" runat="server" CssClass="lblRojo" Height="40px"
                        Text="1. Escriba el n�mero total de ni�os atendidos seg�n su edad, clasificandolos por sexo, conforme al municipio o delegaci�n  y localidad o colonia."
                        Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 997px; text-align: center;">
    <table>
        <tr>
            <td style="height: 25px" nowrap="noWrap">
            </td>
            <td colspan="2" style="height: 25px; text-align: center;">
                <asp:Label ID="lbl0a" runat="server" CssClass="lblNegro" Text="MENOS DE 1 A�O"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="height: 25px; text-align: center;">
                <asp:Label ID="lbl1a" runat="server" CssClass="lblNegro" Text="1 A�O"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="height: 25px; text-align: center;">
                <asp:Label ID="lbl2a" runat="server" CssClass="lblNegro" Text="2 A�OS"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="height: 25px; text-align: center;">
                <asp:Label ID="lbl3a" runat="server" CssClass="lblNegro" Text="3 A�OS"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="height: 25px; text-align: center;">
                <asp:Label ID="lbl4a" runat="server" CssClass="lblNegro" Text="4 A�OS"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="height: 25px; text-align: center;">
                <asp:Label ID="lblIITotal1" runat="server" CssClass="lblNegro" Text="TOTAL"
                    Width="100%" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 25px" nowrap="noWrap">
            </td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�os0" runat="server" CssClass="lblGrisTit" Text="NI�OS"
                    Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�as0" runat="server" CssClass="lblGrisTit" Text="NI�AS" Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�os1" runat="server" CssClass="lblGrisTit" Text="NI�OS"
                    Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�as1" runat="server" CssClass="lblGrisTit" Text="NI�AS" Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Text="NI�OS"
                    Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�as2" runat="server" CssClass="lblGrisTit" Text="NI�AS" Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�os3" runat="server" CssClass="lblGrisTit" Text="NI�OS"
                    Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�as3" runat="server" CssClass="lblGrisTit" Text="NI�AS" Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�os4" runat="server" CssClass="lblGrisTit" Text="NI�OS"
                    Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�as4" runat="server" CssClass="lblGrisTit" Text="NI�AS" Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�osT" runat="server" CssClass="lblGrisTit" Text="NI�OS"
                    Width="100%"></asp:Label></td>
            <td style="height: 25px; text-align: center;">
                <asp:Label ID="lblNi�asT" runat="server" CssClass="lblGrisTit" Text="NI�AS" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII1" runat="server" CssClass="lblGrisTit" Text="1"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV56" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV57" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV58" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV59" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV60" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20105"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV61" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20106"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV62" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20107"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV63" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20108"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV64" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20109"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV65" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20110"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV66" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20111"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV67" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20112"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII2" runat="server" CssClass="lblGrisTit" Text="2"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV68" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV69" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV70" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV71" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20204"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV72" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20205"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV73" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20206"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV74" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20207"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV75" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20208"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV76" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20209"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV77" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20210"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV78" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20211"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV79" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20212"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII3" runat="server" CssClass="lblGrisTit" Text="3"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV80" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV81" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV82" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV83" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20304"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV84" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20305"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV85" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20306"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV86" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20307"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV87" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20308"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV88" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20309"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV89" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20310"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV90" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20311"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV91" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20312"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII4" runat="server" CssClass="lblGrisTit" Text="4"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV92" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV93" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV94" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV95" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20404"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV96" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20405"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV97" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20406"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV98" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20407"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV99" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20408"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV100" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20409"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV101" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20410"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV102" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20411"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV103" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20412"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII5" runat="server" CssClass="lblGrisTit" Text="5"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV104" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV105" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV106" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20503"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV107" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20504"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV108" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20505"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV109" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20506"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV110" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20507"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV111" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20508"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV112" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20509"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV113" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20510"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV114" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20511"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV115" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20512"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII6" runat="server" CssClass="lblGrisTit" Text="6"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV116" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV117" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV118" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20603"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV119" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20604"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV120" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20605"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV121" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20606"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV122" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20607"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV123" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20608"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV124" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20609"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV125" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20610"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV126" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20611"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV127" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20612"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII7" runat="server" CssClass="lblGrisTit" Text="7"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV128" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV129" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20702"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV130" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20703"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV131" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20704"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV132" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20705"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV133" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20706"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV134" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20707"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV135" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20708"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV136" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20709"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV137" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20710"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV138" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20711"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV139" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20712"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII8" runat="server" CssClass="lblGrisTit" Text="8"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV140" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV141" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20802"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV142" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20803"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV143" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20804"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV144" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20805"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV145" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20806"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV146" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20807"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV147" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20808"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV148" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20809"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV149" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20810"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV150" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20811"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV151" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20812"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII9" runat="server" CssClass="lblGrisTit" Text="9"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV152" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20901"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV153" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20902"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV154" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20903"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV155" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20904"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV156" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20905"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV157" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20906"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV158" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20907"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV159" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20908"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV160" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20909"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV161" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20910"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV162" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20911"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV163" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20912"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII10" runat="server" CssClass="lblGrisTit" Text="10"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV164" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21001"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV165" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21002"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV166" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21003"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV167" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21004"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV168" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21005"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV169" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21006"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV170" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21007"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV171" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21008"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV172" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21009"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV173" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21010"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV174" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21011"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV175" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21012"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII11" runat="server" CssClass="lblGrisTit" Text="11"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV176" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21101"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV177" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21102"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV178" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21103"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV179" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21104"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV180" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21105"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV181" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21106"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV182" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21107"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV183" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21108"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV184" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21109"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV185" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21110"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV186" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21111"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV187" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21112"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII12" runat="server" CssClass="lblGrisTit" Text="12"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV188" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21201"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV189" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21202"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV190" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21203"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV191" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21204"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV192" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21205"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV193" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21206"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV194" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21207"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV195" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21208"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV196" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21209"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV197" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21210"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV198" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21211"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV199" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21212"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII13" runat="server" CssClass="lblGrisTit" Text="13"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV200" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21301"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV201" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21302"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV202" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21303"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV203" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21304"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV204" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21305"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV205" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21306"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV206" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21307"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV207" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21308"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV208" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21309"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV209" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21310"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV210" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21311"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV211" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21312"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII14" runat="server" CssClass="lblGrisTit" Text="14"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV212" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21401"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV213" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21402"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV214" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21403"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV215" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21404"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV216" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21405"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV217" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21406"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV218" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21407"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV219" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21408"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV220" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21409"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV221" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21410"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV222" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21411"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV223" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21412"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII15" runat="server" CssClass="lblGrisTit" Text="15"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV224" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21501"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV225" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21502"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV226" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21503"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV227" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21504"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV228" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21505"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV229" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21506"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV230" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21507"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV231" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21508"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV232" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21509"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV233" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21510"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV234" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21511"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV235" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21512"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII16" runat="server" CssClass="lblGrisTit" Text="16"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV236" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21601"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV237" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21602"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV238" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21603"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV239" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21604"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV240" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21605"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV241" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21606"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV242" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21607"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV243" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21608"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV244" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21609"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV245" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21610"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV246" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21611"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV247" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21612"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII17" runat="server" CssClass="lblGrisTit" Text="17"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV248" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21701"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV249" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21702"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV250" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21703"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV251" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21704"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV252" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21705"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV253" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21706"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV254" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21707"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV255" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21708"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV256" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21709"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV257" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21710"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV258" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21711"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV259" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21712"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblII18" runat="server" CssClass="lblGrisTit" Text="18"
                    Width="30px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV260" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21801"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV261" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21802"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV262" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21803"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV263" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21804"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV264" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21805"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV265" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21806"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV266" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21807"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV267" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21808"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV268" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21809"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV269" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21810"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV270" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21811"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV271" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21812"></asp:TextBox></td>
        </tr>
         <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIITotal2" runat="server" CssClass="lblGrisTit" Text="TOTAL"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV272" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21901"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV273" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21902"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV274" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21903"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV275" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21904"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV276" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21905"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV277" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21906"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV278" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21907"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV279" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21908"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV280" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21909"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV281" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21910"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV282" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21911"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV283" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21912"></asp:TextBox></td>
        </tr>
    </table>
            </td>
        </tr>
    </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('Identificacion_EI_NE1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Identificacion_EI_NE1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="OpenPageCharged('Personal_EI_NE1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Personal_EI_NE1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado"  ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 13;
                MaxRow = 21;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                 function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV2'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV4'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV5'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV7'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV8'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV10'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV11'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV13'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV14'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV16'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV17'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV19'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV20'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV22'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV23'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV25'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV26'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV28'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV29'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV31'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV32'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV34'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV35'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV37'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV38'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV40'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV41'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV43'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV44'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV46'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV47'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV49'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV50'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV52'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV53'));
                    openPage(ventana);
                    
                } 
  		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
