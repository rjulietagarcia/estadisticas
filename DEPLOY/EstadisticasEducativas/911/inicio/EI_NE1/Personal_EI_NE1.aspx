<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-NE1(Personal)" AutoEventWireup="true" CodeBehind="Personal_EI_NE1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.EI_NE1.Personal_EI_NE1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server" Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N INICIAL NO ESCOLARIZADA"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div></div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_EI_NE1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Atendidos_EI_NE1',true)"><a href="#" title=""><span>GRUPOS Y NI�OS ATENDIDOS</span></a></li><li onclick="openPage('Personal_EI_NE1',true)"><a href="#" title="" class="activo"><span>PADRES Y PERSONAL</span></a></li><li onclick="openPage('Educativo_EI_NE1',false)"><a href="#" title="" ><span>AVANCE Y ESPACIOS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul> 
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
     <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 900px">
            <tr>
                <td style="height: 17px;text-align:justify">
                    <asp:Label ID="lblPADRES" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="III. PADRES DE FAMILIA"
                        Width="300px"></asp:Label></td>
                <td style="height: 17px;text-align:justify">
                    <asp:Label ID="lblPERSONAL" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="IV. PERSONAL"
                        Width="100%"></asp:Label></td>
                <td style="height: 17px">
                </td>
            </tr>
            <tr>
                <td style="padding-right: 20px; text-align: left">
                    <asp:Label ID="lblInstruccionIII1" runat="server" CssClass="lblRojo" Text="1. Escriba por municipio o delegaci�n  y localidad o colonia, el n�mero de padres de familia que participan en el programa."
                        Width="100%"></asp:Label></td>
                <td style="padding-right: 20px; text-align: left">
                    <asp:Label ID="lblInstruccionIV1" runat="server" CssClass="lblRojo" Text="1. Escriba el n�mero total de educadores comunitarios que atienden los grupos, de acuerdo con el municipio o delegaci�n  y localidad o colonia, desglos�ndolos por sexo."
                        Width="300px"></asp:Label></td>
                <td style="text-align: left">
                    <asp:Label ID="lblInstruccionIV2" runat="server" CssClass="lblRojo" Text="2. Desglose el total de educadores comunitarios, de acuerdo con el nivel m�ximo de estudios y sexo. Nota: Si en la tabla correspondiente al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente en Otros."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td valign="top" style="text-align: center">
   <table>
        <tr>
            <td style="height: 53px">
            </td>
            <td style="text-align: center; height: 53px;">
                <asp:Label ID="lblNumPadres" runat="server" CssClass="lblGrisTit" Height="50px" Text="N�MERO DE PADRES DE FAMILIA"
                    Width="80px"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII1" runat="server" CssClass="lblGrisTit" Text="1"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV284" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII2" runat="server" CssClass="lblGrisTit" Text="2"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV285" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII3" runat="server" CssClass="lblGrisTit" Text="3"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV286" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII4" runat="server" CssClass="lblGrisTit" Text="4"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV287" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII5" runat="server" CssClass="lblGrisTit" Text="5"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV288" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII6" runat="server" CssClass="lblGrisTit" Text="6"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV289" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII7" runat="server" CssClass="lblGrisTit" Text="7"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV290" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII8" runat="server" CssClass="lblGrisTit" Text="8"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV291" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10801"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII9" runat="server" CssClass="lblGrisTit" Text="9"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV292" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right; height: 26px;">
                <asp:Label ID="lblIII10" runat="server" CssClass="lblGrisTit" Text="10"></asp:Label></td>
            <td style="text-align: center; height: 26px;">
                <asp:TextBox ID="txtV293" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII11" runat="server" CssClass="lblGrisTit" Text="11"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV294" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII12" runat="server" CssClass="lblGrisTit" Text="12"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV295" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII13" runat="server" CssClass="lblGrisTit" Text="13"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV296" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII14" runat="server" CssClass="lblGrisTit" Text="14"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV297" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII15" runat="server" CssClass="lblGrisTit" Text="15"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV298" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right; height: 26px;">
                <asp:Label ID="lblIII16" runat="server" CssClass="lblGrisTit" Text="16"></asp:Label></td>
            <td style="text-align: center; height: 26px;">
                <asp:TextBox ID="txtV299" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII17" runat="server" CssClass="lblGrisTit" Text="17"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV300" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIII18" runat="server" CssClass="lblGrisTit" Text="18"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV301" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIIITotal" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV302" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
        </tr>
    </table>
                </td>
                <td valign="top" style="text-align: center">
    <table>
        <tr>
            <td style="height: 25px">
            </td>
            <td colspan="2" style="text-align: center; height: 25px;">
                <asp:Label ID="lblEducadores" runat="server" CssClass="lblNegro" Height="17px" Text="EDUCADORES COMUNITARIOS"
                    Width="100%" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 25px">
            </td>
            <td style="text-align: center; height: 25px;">
                <asp:Label ID="lblIVHombres1" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 25px;">
                <asp:Label ID="lblIVMujeres1" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV1" runat="server" CssClass="lblGrisTit" Text="1"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV303" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV304" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV2" runat="server" CssClass="lblGrisTit" Text="2"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV305" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV306" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV3" runat="server" CssClass="lblGrisTit" Text="3"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV307" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV308" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12202"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV4" runat="server" CssClass="lblGrisTit" Text="4"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV309" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV310" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12302"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV5" runat="server" CssClass="lblGrisTit" Text="5"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV311" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV312" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12402"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV6" runat="server" CssClass="lblGrisTit" Text="6"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV313" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV314" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12502"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV7" runat="server" CssClass="lblGrisTit" Text="7"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV315" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV316" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12602"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV8" runat="server" CssClass="lblGrisTit" Text="8"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV317" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV318" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12702"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV9" runat="server" CssClass="lblGrisTit" Text="9"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV319" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV320" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12802"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV10" runat="server" CssClass="lblGrisTit" Text="10"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV321" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV322" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="12902"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV11" runat="server" CssClass="lblGrisTit" Text="11"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV323" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV324" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13002"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV12" runat="server" CssClass="lblGrisTit" Text="12"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV325" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV326" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13102"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV13" runat="server" CssClass="lblGrisTit" Text="13"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV327" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV328" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13202"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV14" runat="server" CssClass="lblGrisTit" Text="14"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV329" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV330" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13302"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV15" runat="server" CssClass="lblGrisTit" Text="15"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV331" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV332" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13402"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV16" runat="server" CssClass="lblGrisTit" Text="16"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV333" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV334" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13502"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV17" runat="server" CssClass="lblGrisTit" Text="17"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV335" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV336" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13602"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIV18" runat="server" CssClass="lblGrisTit" Text="18"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV337" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV338" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13702"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblIVTotal1" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV339" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV340" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13802"></asp:TextBox></td>
        </tr>
    </table>
                </td>
                <td style="text-align: center">
    
    <table>
        <tr>
            <td>
                <asp:Label ID="lblNivelE" runat="server" CssClass="lblNegro" Height="17px" Text="NIVEL EDUCATIVO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblIVHombres2" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblIVMujeres2" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblIVTota1" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel1" runat="server" CssClass="lblGrisTit" Height="17px" Text="PRIMARIA INCOMPLETA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV341" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV342" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="13902"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV343" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13903"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel2" runat="server" CssClass="lblGrisTit" Height="17px" Text="PRIMARIA TERMIADA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV344" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV345" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14002"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV346" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14003"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel3" runat="server" CssClass="lblGrisTit" Height="17px" Text="SECUNDARIA INCOMPLETA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV347" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV348" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV349" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14103"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel5" runat="server" CssClass="lblGrisTit" Height="17px" Text="SECUNDARIA TERMINADA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV350" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV351" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV352" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14203"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel6" runat="server" CssClass="lblGrisTit" Height="17px" Text="PROFESIONAL T�CNICO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV353" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV354" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV355" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14303"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel7" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO INCOMPLETO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV356" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV357" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV358" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14403"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel8" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO TERMINADO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV359" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV360" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV361" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14503"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; height: 26px;">
                <asp:Label ID="lblNivel9" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR INCOMPLETA"
                    Width="250px"></asp:Label></td>
            <td style="text-align: center; height: 26px;">
                <asp:TextBox ID="txtV362" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14601"></asp:TextBox></td>
            <td style="text-align: center; height: 26px;">
                <asp:TextBox ID="txtV363" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14602"></asp:TextBox></td>
            <td style="text-align: center; height: 26px;">
                <asp:TextBox ID="txtV364" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14603"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel10" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR TERMINADA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV365" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV366" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV367" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14703"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel11" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA INCOMPLETA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV368" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV369" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14802"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV370" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14803"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel12" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA TERMINADA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV371" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV372" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="14902"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV373" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="14903"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel13" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR INCOMPLETA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV374" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV375" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15002"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV376" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15003"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel14" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, PASANTE"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV377" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV378" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV379" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15103"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel15" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, TITULADO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV380" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV381" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV382" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15203"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel16" runat="server" CssClass="lblGrisTit" Height="17px" Text="LICENCIATURA INCOMPLETA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV383" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV384" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV385" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15303"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel17" runat="server" CssClass="lblGrisTit" Height="17px" Text="LICENCIATURA, PASANTE"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV386" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV387" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV388" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15403"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel18" runat="server" CssClass="lblGrisTit" Height="17px" Text="LICENCIATURA, TITULADO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV389" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV390" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV391" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15503"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel19" runat="server" CssClass="lblGrisTit" Height="17px" Text="MAESTRIA INCOMPLETA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV392" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV393" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15602"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV394" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15603"></asp:TextBox></td>
        </tr>
       <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel20" runat="server" CssClass="lblGrisTit" Height="17px" Text="MAESTRIA, GRADUADO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV395" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV396" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV397" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15703"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel21" runat="server" CssClass="lblGrisTit" Height="17px" Text="DOCTORADO INCOMPLETO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV398" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV399" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15802"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV400" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15803"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivel22" runat="server" CssClass="lblGrisTit" Height="17px" Text="DOCTORADO, GRADUADO"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV401" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV402" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="15902"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV403" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="15903"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivelOtros" runat="server" CssClass="lblGrisTit" Height="17px" Text="OTROS*"
                    Width="100%"></asp:Label></td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivelEspecifique" runat="server" CssClass="lblGrisTit" Height="17px" Text="*ESPECIFIQUE:"
                    Width="100%"></asp:Label></td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtV404" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="16001"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV405" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16002"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV406" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16003"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV407" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16004"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtV408" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="16101"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV409" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16102"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV410" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16103"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV411" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16104"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtV412" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="16201"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV413" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16202"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV414" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16203"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV415" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16204"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblSubtotal" runat="server" CssClass="lblGrisTit" Height="17px" Text="SUBTOTAL"
                    Width="100%"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV416" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16301"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV417" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16302"></asp:TextBox></td>
            <td>
                <asp:TextBox ID="txtV418" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16303"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Label ID="lblIVTota2" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtV419" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="16401"></asp:TextBox></td>
        </tr>
    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Atendidos_EI_NE1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Atendidos_EI_NE1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Educativo_EI_NE1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Educativo_EI_NE1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado"  ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 5;
                MaxRow = 68;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                 function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV404'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV408'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV412'));
                    openPage(ventana);
                    }
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
