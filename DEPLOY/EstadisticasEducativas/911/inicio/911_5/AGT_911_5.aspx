<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.5(Total)" AutoEventWireup="true" CodeBehind="AGT_911_5.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_5.AGT_911_5" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
    
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_911_5',true)"><a href="#" title=""><span>1�</span></a></li><li onclick="openPage('AG2_911_5',true)"><a href="#" title=""><span>2�</span></a></li><li onclick="openPage('AG3_911_5',true)"><a href="#" title=""  ><span>3�</span></a></li><li onclick="openPage('AGT_911_5',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li><li onclick="openPage('AGD_911_5',false)"><a href="#" title=""><span>DESGLOSE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
        <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    
      <center>
           

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table>
                        <tr>
                            <td style="width: 340px">
                                <table id="TABLE1" style="text-align: center">
                                    <tr>
                          
                            <td colspan="13" style="text-align:center;">
                              
                                <asp:Label ID="Label33" runat="server" CssClass="lblGrisTit" Font-Bold="True" 
                                Text="Estad�stica de alumnos por grado, sexo, tipo de ingreso y edad"></asp:Label>
                            </td>
              
                        </tr>
                                    <tr>
                                        <td colspan="1" rowspan="6" style="width: 50px; height: 3px; text-align: center; padding-right: 5px; padding-left: 5px;">
                                            <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                                        <td colspan="2" rowspan="" style="text-align: center; height: 3px;">
                                            </td>
                                        <td colspan="" rowspan="" style="width: 67px; text-align: center; height: 3px;">
                                            <asp:Label ID="lbl12_Menos" runat="server" CssClass="lblRojo" Text="Menos de 12 a�os"></asp:Label></td>
                                        <td style="width: 67px; height: 3px;">
                                            <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                                        <td style="width: 67px; height: 3px">
                                            <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                                        <td style="width: 67px; height: 3px">
                                            <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                                        <td style="width: 67px; height: 3px">
                                            <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os"></asp:Label></td>
                                        <td style="width: 67px; height: 3px">
                                            <asp:Label ID="lbl16" runat="server" CssClass="lblRojo" Text="16 a�os"></asp:Label></td>
                                        <td style="width: 67px; height: 3px">
                                            <asp:Label ID="lbl17" runat="server" CssClass="lblRojo" Text="17 a�os"></asp:Label></td>
                                        <td style="width: 67px; height: 3px">
                                            <asp:Label ID="lbl18_Mas" runat="server" CssClass="lblRojo" Text="18 a�os y m�s"></asp:Label></td>
                                        <td style="width: 67px; height: 3px;">
                                            <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                                        <td style="width: 54px; height: 3px">
                                            <asp:Label ID="lblGrupos1" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 85px; padding-right: 5px;" rowspan="2">
                                            <asp:Label ID="lblHombresTot" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                                        <td style="width: 120px; height: 26px; text-align: left">
                                            <asp:Label ID="lblNvoIngresoHombresTot" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                                        <td style="width: 67px; height: 26px;">
                                            <asp:TextBox ID="txtV122" runat="server" Columns="4" TabIndex="10101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px;">
                                            <asp:TextBox ID="txtV123" runat="server" Columns="4" TabIndex="10102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV124" runat="server" Columns="4" TabIndex="10103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV125" runat="server" Columns="4" TabIndex="10104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV126" runat="server" Columns="4" TabIndex="10105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV127" runat="server" Columns="4" TabIndex="10106" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV128" runat="server" Columns="4" TabIndex="10107" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV129" runat="server" Columns="4" TabIndex="10108" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px;">
                                            <asp:TextBox ID="txtV130" runat="server" Columns="4" TabIndex="10109" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 54px; height: 26px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px; height: 26px; text-align: left">
                                            <asp:Label ID="lblRepetidoresHombresTot" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                                        <td style="width: 67px; height: 26px;">
                                            <asp:TextBox ID="txtVBlank7" runat="server" BackColor="Silver" ReadOnly="true" onkeydown="return Arrows(event,this.tabIndex)" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" TabIndex="10201"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px;">
                                            <asp:TextBox ID="txtV131" runat="server" Columns="4" TabIndex="10202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV132" runat="server" Columns="4" TabIndex="10203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV133" runat="server" Columns="4" TabIndex="10204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV134" runat="server" Columns="4" TabIndex="10205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV135" runat="server" Columns="4" TabIndex="10206" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV136" runat="server" Columns="4" TabIndex="10207" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV137" runat="server" Columns="4" TabIndex="10208" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px;">
                                            <asp:TextBox ID="txtV138" runat="server" Columns="4" TabIndex="10209" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 54px; height: 26px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="width: 85px; margin-right: 5px; text-align: left;">
                                        <asp:Label ID="lblMujeresTot" runat="server" CssClass="lblRojo" Text="MUJERES" Height="17px"></asp:Label></td>
                                        <td style="width: 120px; height: 26px; text-align: left">
                                        <asp:Label ID="lblNvoIngresoMujeresTot" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO"></asp:Label></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV139" runat="server" Columns="4" TabIndex="10301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV140" runat="server" Columns="4" TabIndex="10302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV141" runat="server" Columns="4" TabIndex="10303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV142" runat="server" Columns="4" TabIndex="10304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV143" runat="server" Columns="4" TabIndex="10305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV144" runat="server" Columns="4" TabIndex="10306" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV145" runat="server" Columns="4" TabIndex="10307" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV146" runat="server" Columns="4" TabIndex="10308" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV147" runat="server" Columns="4" TabIndex="10309" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 54px; height: 26px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="width: 120px; height: 26px; text-align: left;">
                                        <asp:Label ID="lblRepetidoresMujeresTot" runat="server" CssClass="lblGrisTit" Text="REPETIDORES"></asp:Label></td>
                                        <td style="width: 67px; height: 26px; text-align: left">
                                            <asp:TextBox ID="txtVBlank6" runat="server" BackColor="Silver" ReadOnly="true" onkeydown="return Arrows(event,this.tabIndex)" BorderColor="Silver"
                                                BorderStyle="Solid" BorderWidth="2px" Columns="4" TabIndex="10401"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV148" runat="server" Columns="4" TabIndex="10402" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV149" runat="server" Columns="4" TabIndex="10403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV150" runat="server" Columns="4" TabIndex="10404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV151" runat="server" Columns="4" TabIndex="10405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV152" runat="server" Columns="4" TabIndex="10406" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV153" runat="server" Columns="4" TabIndex="10407" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV154" runat="server" Columns="4" TabIndex="10408" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV155" runat="server" Columns="4" TabIndex="10409" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 54px; height: 26px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                        <asp:Label ID="lblSubtotalTot" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                                        <td rowspan="1" style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV156" runat="server" Columns="4" TabIndex="10501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px; text-align: left">
                                            <asp:TextBox ID="txtV157" runat="server" Columns="4" TabIndex="10502" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV158" runat="server" Columns="4" TabIndex="10503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV159" runat="server" Columns="4" TabIndex="10504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV160" runat="server" Columns="4" TabIndex="10505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV161" runat="server" Columns="4" TabIndex="10506" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV162" runat="server" Columns="4" TabIndex="10507" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV163" runat="server" Columns="4" TabIndex="10508" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 67px; height: 26px">
                                            <asp:TextBox ID="txtV164" runat="server" Columns="4" TabIndex="10509" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="width: 54px; height: 26px">
                                            <asp:TextBox ID="txtV165" runat="server" Columns="3" TabIndex="10510" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                            height: 3px; text-align: center">
                                        </td>
                                        <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="width: 67px; height: 26px">
                                        </td>
                                        <td style="width: 67px; height: 26px; text-align: left">
                                        </td>
                                        <td style="width: 67px; height: 26px">
                                        </td>
                                        <td style="width: 67px; height: 26px">
                                        </td>
                                        <td style="width: 67px; height: 26px">
                                        </td>
                                        <td style="width: 67px; height: 26px">
                                        </td>
                                        <td style="width: 67px; height: 26px">
                                        </td>
                                        <td style="width: 67px; height: 26px">
                                        </td>
                                        <td style="width: 67px; height: 26px">
                                        </td>
                                        <td style="width: 54px; height: 26px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                
                    <hr />
                    <table align="center">
                        <tr>
                            <td ><span  onclick="openPage('AG3_911_5',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                            <td ><span  onclick="openPage('AG3_911_5',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                            <td style="width: 330px;">&nbsp;</td>
                            <td ><span  onclick="openPage('AGD_911_5',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                            <td ><span  onclick="openPage('AGD_911_5',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                        </tr>
                    </table>
                    
                    <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
