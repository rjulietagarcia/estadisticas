using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
using SEroot.WsEstadisticasEducativas;
using EstadisticasEducativas._911;
namespace EstadisticasEducativasInicio._911.inicio.ECC_12
{
    public partial class Anexo_ECC_12 : System.Web.UI.Page, ICallbackEventHandler
    {
       
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajasAnexos(this);

                Class911.LlenarDatosDB11(this.Page, controlDP, Class911.Doc_AnexoInicio, 0, hidListaTxtBoxs.Value); // DOCUMENTOS: 1... Variable Custrionario 2...Especial 3... Anexo  4... Carreras


                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                hidDisparador.Value = Class911.TiempoAutoGuardado();

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";

                //optVANX327.Attributes.Add("onclick", "OPTs2Txt()"); optVANX327.Enabled = controlDP.Estatus == 0;

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }
        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, Class911.Doc_AnexoInicio, HttpContext.Current); //Class911.RaiseCallbackEvent_Anexos(eventArgument); ;
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }
    }
}
