<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.6(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_6.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_6.Personal_911_6" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
    <script type="text/javascript">
        var _enter=true;
    </script>
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 2;
        MaxRow = 20;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>


    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_6',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_6',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_6',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_6',true)"><a href="#" title=""><span>3�</span></a></li>
        <li onclick="openPage('AGT_911_6',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_6',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_6',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="openPage('Inmueble_911_6',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div class="balloonstyle" id="tooltipayuda">
    <p>Favor de revisar la estad�stica de personal y en caso de observar informaci�n que no se apega a la realidad, realizar las adecuaciones en el m�dulo de Personal por Funci�n.</p>
    <p>En esta pantalla est� disponible un link para acceder al m�dulo de Personal por Funci�n  y realizar correcciones en la plantilla de personal, si utiliza esta opci�n, una vez realizados los cambios, deber� regresar a la estad�stica y presionar clic en el bot�n de Actualizar Datos.</p>
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
 <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>


            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

 
        <table style="width: 550px">
            <tr>
                <td>
                    <asp:Label ID="lblTitulo1" runat="server" CssClass="lblNegro" Text="La siguiente pregunta �nicamente deber� ser contestada por TELESECUNDARIAS."></asp:Label><br />
                    <asp:Label ID="lblTit2" runat="server" CssClass="lblRojo" Text="9. Escriba, por grado, el n�mero de directivos con grupo y docentes."></asp:Label><br />
                    &nbsp;<asp:Label ID="lblTitulo2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="IMPORTANTE:"></asp:Label>
                    <asp:Label ID="lblTitulo3" runat="server" CssClass="lblRojo" Text="Si un profesor atiende m�s de un grado, an�telo en el rubro correspondiente; el total debe coincidir con la suma de directivo con grupo m�s personal docente de la secci�n PERSONAL POR FUNCI�N."></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 192px">
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblPrimero1" runat="server" CssClass="lblGrisTit" Text="PRIMERO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV412" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10101" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblSegundo1" runat="server" CssClass="lblGrisTit" Text="SEGUNDO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV413" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10201" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblTercero1" runat="server" CssClass="lblGrisTit" Text="TERCERO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV414" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10301" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblMasDeUnGrado" runat="server" CssClass="lblGrisTit" Text="M�S DE UN GRADO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV415" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10401" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblGrisTit" Text="TOTAL"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV416" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10501" MaxLength="2"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <asp:Label ID="lblTitulo" runat="server" CssClass="lblRojo" Font-Size="16px" Text="II. PERSONAL POR FUNCI�N"></asp:Label><br />
        <br />
        <table style="width: 550px">
            <tr>
                <td style="height: 104px">
                    <asp:Label ID="lblTitulo4" runat="server" CssClass="lblRojo" Text="1. Escriba el personal seg�n la funci�n que realiza, independientemente de su nombramiento, tipo y fuente de pago. Si una persona desempe�a dos o m�s funciones, an�tela en aqu�lla a la que dedique m�s tiempo."></asp:Label><br />
                    <asp:Label ID="lblTit4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="Nota:"></asp:Label>
                    <asp:Label ID="lblTit41" runat="server" CssClass="lblNegro" Text="Considere exclusivamente al personal que labora en el turno al que se refiere este cuestionario, independientemente de que labore en otro turno."></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 449px">
                        <tr>
                            <td colspan="2" style="text-align: left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" colspan="2">
                                <asp:Label ID="lblPerDir" runat="server" CssClass="lblNegro" Font-Bold="True" 
                                Text="PERSONAL DIRECTIVO"></asp:Label>
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left; padding-left: 15px;">
                                <asp:Label ID="lblPerDirCG" runat="server" CssClass="lblRojo" Text="CON GRUPO"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV417" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10601" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left; padding-left: 15px;">
                                <asp:Label ID="lblPerDirSG" runat="server" CssClass="lblRojo" Text="SIN GRUPO"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV418" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10701" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblPerDoc" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PERSONAL DOCENTE (No incluya personal docente especial)"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV419" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10901" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblPerDocEsp" runat="server" CssClass="lblNegro" Font-Bold="True"
                                    Text="PERSONAL DOCENTE ESPECIAL"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 15px; width: 519px; text-align: left">
                                <asp:Label ID="lblProfEdFis" runat="server" CssClass="lblRojo" Text="PROFESORES DE EDUCACI�N F�SICA"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV420" runat="server" Columns="2" CssClass="lblNegro" TabIndex="11001" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="padding-left: 15px; width: 519px; text-align: left">
                                <asp:Label ID="lblProfActArt" runat="server" CssClass="lblRojo" Text="PROFESORES DE ACTIVIDADES ART�STICAS"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV421" runat="server" Columns="2" CssClass="lblNegro" TabIndex="11101" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="padding-left: 15px; width: 519px; text-align: left">
                                <asp:Label ID="lblProfActTec" runat="server" CssClass="lblRojo" Text="PROFESORES DE ACTIVIDADES TECNOL�GICAS"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV422" runat="server" Columns="2" CssClass="lblNegro" TabIndex="11201" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                           <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblPerAdmin" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV423" runat="server" Columns="3" CssClass="lblNegro" TabIndex="11301" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblTotalPer" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL DE PERSONAL"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV424" runat="server" Columns="4" CssClass="lblNegro" TabIndex="11401" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left">
                                &nbsp;</td>
                            <td style="width: 82px; text-align: right">
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblSeccion2" runat="server" CssClass="lblRojo" Text="2. Sume el personal directivo con grupo, personal docente y personal docente especial, y an�telo seg�n el tiempo que dedica a la funci�n acad�mica."></asp:Label><br />
                    <asp:Label ID="lblSeccion2Nota" runat="server" CssClass="lblNegro" Font-Bold="True" Text="Nota:"></asp:Label><asp:Label
                        ID="lblSeccion2Desc" runat="server" CssClass="lblNegro" Text="Si en la instituci�n no se utiliza el t�rmino tres cuartos de tiempo, no lo considere."></asp:Label><br />
                    <table style="width: 449px">
                        <tr>
                            <td style="text-align: left" colspan="2">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left; padding-left: 0px;">
                                <asp:Label ID="lblTC" runat="server" CssClass="lblGrisTit" Text="TIEMPO COMPLETO"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV426" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="11501"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left; padding-left: 0px;">
                                <asp:Label ID="lbl34T" runat="server" CssClass="lblGrisTit" Text="TRES CUARTOS DE TIEMPO"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV427" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="11601"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="padding-left: 0px; width: 519px; text-align: left">
                                <asp:Label ID="lblMT" runat="server" CssClass="lblGrisTit" Text="MEDIO TIEMPO"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV428" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="11701"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="padding-left: 0px; width: 519px; text-align: left">
                                <asp:Label ID="lblPH" runat="server" CssClass="lblGrisTit" Text="POR HORAS"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV429" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="11801"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblTotalSecc2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV430" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                    TabIndex="11901"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
                 <hr />
       
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGD_911_6',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_6',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Inmueble_911_6',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Inmueble_911_6',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
         <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        
        </center>        
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
        GetTabIndexes();  
        </script> 
</asp:Content>
