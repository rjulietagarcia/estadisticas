<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.4(Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_4.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_4.AGD_911_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script type="text/javascript" src="../../../tema/js/ArrowsHandler.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 19;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width: 1100px; height: 65px;">
        <div id="header">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <span>EDUCACI�N PRIMARIA</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
                </tr>
            </table>
        </div>
    </div>

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_4',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_4',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_4',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_4',true)"><a href="#" title=""><span>3�</span></a></li>
        <li onclick="openPage('AG4_911_4',true)"><a href="#" title=""><span>4�</span></a></li>
        <li onclick="openPage('AG5_911_4',true)"><a href="#" title=""><span>5�</span></a></li>
        <li onclick="openPage('AG6_911_4',true)"><a href="#" title=""><span>6�</span></a></li>
        <li onclick="openPage('AGT_911_4',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_4',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_4',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div class="balloonstyle" id="tooltipayuda">
    <p>Favor de ingresar la estad�stica de alumnos con discapacidad, alumnos con capacidades y aptitudes sobresalientes, y alumnos que fueron atendidos por la Unidad de Servicios de Apoyo a la Educaci�n Regular (USAER).</p>
    <p>El resto de las variables son llenadas autom�ticamente.</p>
    <p>En caso de existir inconsistencias en los datos deber� realizar las modificaciones en el sistema de Control Escolar.</p>
    
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 100%">
            <tr>
                <td style="vertical-align: top">
                    <table style="width: 366px">
                    <tr>
                        <td colspan="2" style="height: 44px; text-align: left;">
                            <asp:Label ID="lblTit2" runat="server" Text="2. De la existencia total, anote el n�mero de alumnos de nacionalidad extranjera, desglos�ndolo por sexo." CssClass="lblRojo"></asp:Label><br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="width: 360px">
                                <tr>
                                    <td style="width: 338px; text-align: left">
                                    </td>
                                    <td style="width: 60px">
                                        <asp:Label ID="lblHombres2" runat="server" Text="HOMBRES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:Label ID="lblMujeres2" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 59px">
                                        <asp:Label ID="lblTotal2" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 338px;">
                                        <asp:Label ID="lblEEUU" runat="server" Text="ESTADOS UNIDOS" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV622" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10101" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV623" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10102" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV624" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10103" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 338px;">
                                        <asp:Label ID="lblCanada" runat="server" Text="CANAD�" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV625" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10201" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV626" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10202" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV627" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10203" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 338px;">
                                        <asp:Label ID="lblCA" runat="server" Text="CENTROAM�RICA Y EL CARIBE" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV628" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10301" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV629" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10302" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV630" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10303" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 338px;">
                                        <asp:Label ID="lblSA" runat="server" Text="SUDAM�RICA" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV631" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10401" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV632" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10402" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV633" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10403" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 338px;">
                                        <asp:Label ID="lblAfrica" runat="server" Text="�FRICA" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV634" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10501" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV635" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10502" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV636" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10503" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 338px;">
                                        <asp:Label ID="lblAsia" runat="server" Text="ASIA" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV637" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10601" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV638" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10602" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV639" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10603" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 338px;">
                                        <asp:Label ID="lblEuropa" runat="server" Text="EUROPA" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV640" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10701" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV641" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10702" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV642" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10703" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 338px;">
                                        <asp:Label ID="lblOceania" runat="server" Text="OCEAN�A" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV643" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10801" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 60px">
                                        <asp:TextBox ID="txtV644" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10802" MaxLength="3"></asp:TextBox></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV645" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10803" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 338px; text-align: left">
                                    </td>
                                    <td style="width: 60px">
                                    </td>
                                    <td style="width: 60px">
                                        <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                    <td style="width: 59px">
                                        <asp:TextBox ID="txtV646" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10961" MaxLength="3"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
                <td style="vertical-align: top">
                    <table style="width: 366px">
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lblTit5" runat="server" Text="5. De la existencia total, anote el n�mero de alumnos con capacidades y aptitudes sobresalientes, desglos�ndolo por sexo." CssClass="lblRojo"></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="width: 166px">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblHombres5" runat="server" Text="HOMBRES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV653" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20701" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblMujeres5" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV654" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20801" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblTotal5" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV655" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20901" MaxLength="3"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table style="width: 366px">
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lblTit6" runat="server" Text="6. De la existencia total, anote el n�mero de alumnos que fueron atendidos por la Unidad de Servicios de Apoyo a la Educaci�n Regular (USAER), desglos�ndolo por sexo." CssClass="lblRojo"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="width: 166px">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblHombres6" runat="server" Text="HOMBRES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV656" runat="server" Columns="3" CssClass="lblNegro" TabIndex="21001" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblMujeres6" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV657" runat="server" Columns="3" CssClass="lblNegro" TabIndex="21101" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblTotal6" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV658" runat="server" Columns="3" CssClass="lblNegro" TabIndex="21201" MaxLength="3"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top"><table style="width: 366px">
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lblTit3" runat="server" CssClass="lblRojo" Text="3. De la existencia total, anote el n�mero de ni�os ind�genas, desglos�ndolo por sexo."></asp:Label><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="width: 166px">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV647" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20101" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblMujeres3" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV648" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20201" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblTotal3" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV649" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20301" MaxLength="3"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
                <td style="vertical-align: top" rowspan="2"><table style="width: 366px">
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lblTit7" runat="server" Text="7. Anote el n�mero total de grupos por grado." CssClass="lblRojo"></asp:Label><br/><br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="width: 166px">
                                <tr>
                                    <td style="text-align: left; height: 34px;">
                                        <asp:Label ID="Label2" runat="server" Text="GRADO" CssClass="lblNegro" Font-Bold="True"></asp:Label></td>
                                    <td style="height: 34px">
                                        <asp:Label ID="Label5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Text="PRIMERO"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV659" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="21301"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Text="SEGUNDO"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV660" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="21401"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Text="TERCERO"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV661" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="21501"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; height: 26px;">
                                        <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Text="CUARTO"></asp:Label></td>
                                    <td style="height: 26px">
                                        <asp:TextBox ID="txtV662" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="21601"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Text="QUINTO"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV663" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="21701"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Text="SEXTO"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV664" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="21801"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 34px; text-align: left">
                                        <asp:Label ID="Label4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"></asp:Label></td>
                                    <td style="height: 34px">
                                        <asp:TextBox ID="txtV665" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                            TabIndex="21901"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                    <table style="width: 366px">
                        <tr>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="lblTit4" runat="server" Text="4. De la existencia total, anote el n�mero de alumnos con discapacidad, desglos�ndolo por sexo." CssClass="lblRojo"></asp:Label><br /><br /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table style="width: 166px">
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblHombres4" runat="server" Text="HOMBRES" CssClass="lblGrisTit"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV650" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20401" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; height: 26px;">
                                            <asp:Label ID="lblMujeres4" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                        <td style="height: 26px">
                                            <asp:TextBox ID="txtV651" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20501" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblTotal4" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV652" runat="server" Columns="3" CssClass="lblNegro" TabIndex="20601" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                </td>
                <td rowspan="1" style="vertical-align: top">
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGT_911_4',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_4',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                 <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal_911_4',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_4',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
<div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
         
</asp:Content>
