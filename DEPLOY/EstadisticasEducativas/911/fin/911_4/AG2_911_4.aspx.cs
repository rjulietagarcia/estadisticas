using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_4
{
    public partial class AG2_911_4 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV109.Attributes["onkeypress"] = "return Num(event)";
                txtV110.Attributes["onkeypress"] = "return Num(event)";
                txtV111.Attributes["onkeypress"] = "return Num(event)";
                txtV112.Attributes["onkeypress"] = "return Num(event)";
                txtV113.Attributes["onkeypress"] = "return Num(event)";
                txtV114.Attributes["onkeypress"] = "return Num(event)";
                txtV115.Attributes["onkeypress"] = "return Num(event)";
                txtV116.Attributes["onkeypress"] = "return Num(event)";
                txtV117.Attributes["onkeypress"] = "return Num(event)";
                txtV118.Attributes["onkeypress"] = "return Num(event)";
                txtV119.Attributes["onkeypress"] = "return Num(event)";
                txtV120.Attributes["onkeypress"] = "return Num(event)";
                txtV121.Attributes["onkeypress"] = "return Num(event)";
                txtV122.Attributes["onkeypress"] = "return Num(event)";
                txtV123.Attributes["onkeypress"] = "return Num(event)";
                txtV124.Attributes["onkeypress"] = "return Num(event)";
                txtV125.Attributes["onkeypress"] = "return Num(event)";
                txtV126.Attributes["onkeypress"] = "return Num(event)";
                txtV127.Attributes["onkeypress"] = "return Num(event)";
                txtV128.Attributes["onkeypress"] = "return Num(event)";
                txtV129.Attributes["onkeypress"] = "return Num(event)";
                txtV130.Attributes["onkeypress"] = "return Num(event)";
                txtV131.Attributes["onkeypress"] = "return Num(event)";
                txtV132.Attributes["onkeypress"] = "return Num(event)";
                txtV133.Attributes["onkeypress"] = "return Num(event)";
                txtV134.Attributes["onkeypress"] = "return Num(event)";
                txtV135.Attributes["onkeypress"] = "return Num(event)";
                txtV136.Attributes["onkeypress"] = "return Num(event)";
                txtV137.Attributes["onkeypress"] = "return Num(event)";
                txtV138.Attributes["onkeypress"] = "return Num(event)";
                txtV139.Attributes["onkeypress"] = "return Num(event)";
                txtV140.Attributes["onkeypress"] = "return Num(event)";
                txtV141.Attributes["onkeypress"] = "return Num(event)";
                txtV142.Attributes["onkeypress"] = "return Num(event)";
                txtV143.Attributes["onkeypress"] = "return Num(event)";
                txtV144.Attributes["onkeypress"] = "return Num(event)";
                txtV145.Attributes["onkeypress"] = "return Num(event)";
                txtV146.Attributes["onkeypress"] = "return Num(event)";
                txtV147.Attributes["onkeypress"] = "return Num(event)";
                txtV148.Attributes["onkeypress"] = "return Num(event)";
                txtV149.Attributes["onkeypress"] = "return Num(event)";
                txtV150.Attributes["onkeypress"] = "return Num(event)";
                txtV151.Attributes["onkeypress"] = "return Num(event)";
                txtV152.Attributes["onkeypress"] = "return Num(event)";
                txtV153.Attributes["onkeypress"] = "return Num(event)";
                txtV154.Attributes["onkeypress"] = "return Num(event)";
                txtV155.Attributes["onkeypress"] = "return Num(event)";
                txtV156.Attributes["onkeypress"] = "return Num(event)";
                txtV157.Attributes["onkeypress"] = "return Num(event)";
                txtV158.Attributes["onkeypress"] = "return Num(event)";
                txtV159.Attributes["onkeypress"] = "return Num(event)";
                txtV160.Attributes["onkeypress"] = "return Num(event)";
                txtV161.Attributes["onkeypress"] = "return Num(event)";
                txtV162.Attributes["onkeypress"] = "return Num(event)";
                txtV163.Attributes["onkeypress"] = "return Num(event)";
                txtV164.Attributes["onkeypress"] = "return Num(event)";
                txtV165.Attributes["onkeypress"] = "return Num(event)";
                txtV166.Attributes["onkeypress"] = "return Num(event)";
                txtV167.Attributes["onkeypress"] = "return Num(event)";
                txtV168.Attributes["onkeypress"] = "return Num(event)";
                txtV169.Attributes["onkeypress"] = "return Num(event)";
                txtV170.Attributes["onkeypress"] = "return Num(event)";
                txtV171.Attributes["onkeypress"] = "return Num(event)";
                txtV172.Attributes["onkeypress"] = "return Num(event)";
                txtV173.Attributes["onkeypress"] = "return Num(event)";
                txtV174.Attributes["onkeypress"] = "return Num(event)";
                txtV175.Attributes["onkeypress"] = "return Num(event)";
                txtV176.Attributes["onkeypress"] = "return Num(event)";
                txtV177.Attributes["onkeypress"] = "return Num(event)";
                txtV178.Attributes["onkeypress"] = "return Num(event)";
                txtV179.Attributes["onkeypress"] = "return Num(event)";
                txtV180.Attributes["onkeypress"] = "return Num(event)";
                txtV181.Attributes["onkeypress"] = "return Num(event)";
                txtV182.Attributes["onkeypress"] = "return Num(event)";
                txtV183.Attributes["onkeypress"] = "return Num(event)";
                txtV184.Attributes["onkeypress"] = "return Num(event)";
                txtV185.Attributes["onkeypress"] = "return Num(event)";
                txtV186.Attributes["onkeypress"] = "return Num(event)";
                txtV187.Attributes["onkeypress"] = "return Num(event)";
                txtV188.Attributes["onkeypress"] = "return Num(event)";
                txtV189.Attributes["onkeypress"] = "return Num(event)";
                txtV190.Attributes["onkeypress"] = "return Num(event)";
                txtV191.Attributes["onkeypress"] = "return Num(event)";
                txtV192.Attributes["onkeypress"] = "return Num(event)";
                txtV193.Attributes["onkeypress"] = "return Num(event)";
                txtV194.Attributes["onkeypress"] = "return Num(event)";
                txtV195.Attributes["onkeypress"] = "return Num(event)";
                txtV196.Attributes["onkeypress"] = "return Num(event)";
                txtV197.Attributes["onkeypress"] = "return Num(event)";
                txtV198.Attributes["onkeypress"] = "return Num(event)";
                txtV199.Attributes["onkeypress"] = "return Num(event)";
                txtV200.Attributes["onkeypress"] = "return Num(event)";
                txtV201.Attributes["onkeypress"] = "return Num(event)";
                txtV202.Attributes["onkeypress"] = "return Num(event)";
                txtV203.Attributes["onkeypress"] = "return Num(event)";
                txtV204.Attributes["onkeypress"] = "return Num(event)";
                txtV205.Attributes["onkeypress"] = "return Num(event)";
                txtV206.Attributes["onkeypress"] = "return Num(event)";
                txtV207.Attributes["onkeypress"] = "return Num(event)";
                #endregion


                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();


                //lnkModificacionDatosAl.NavigateUrl = Class911.Liga_ModificacionDatosAlumno(controlDP);
                //lnkControlEscolarABC.NavigateUrl = Class911.Liga_ControlEscolar(controlDP);
                //lnkCalificaciones.NavigateUrl = Class911.Liga_Calificaciones(controlDP);

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        protected void lnkRefrescar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
            // Actualiza Info
            Class911.CargaInicialCuestionario(controlDP, 1);
            // Recarga datos
            hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);
            Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

            #region Volver a escribir JS Call Back
            String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
            String callbackScript_Variables;
            callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
            #endregion

        }

        //*********************************


    }
}
