using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;


using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_4
{
    public partial class AG5_911_4 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV379.Attributes["onkeypress"] = "return Num(event)";
                txtV380.Attributes["onkeypress"] = "return Num(event)";
                txtV381.Attributes["onkeypress"] = "return Num(event)";
                txtV382.Attributes["onkeypress"] = "return Num(event)";
                txtV383.Attributes["onkeypress"] = "return Num(event)";
                txtV384.Attributes["onkeypress"] = "return Num(event)";
                txtV385.Attributes["onkeypress"] = "return Num(event)";
                txtV386.Attributes["onkeypress"] = "return Num(event)";
                txtV387.Attributes["onkeypress"] = "return Num(event)";
                txtV388.Attributes["onkeypress"] = "return Num(event)";
                txtV389.Attributes["onkeypress"] = "return Num(event)";
                txtV390.Attributes["onkeypress"] = "return Num(event)";
                txtV391.Attributes["onkeypress"] = "return Num(event)";
                txtV392.Attributes["onkeypress"] = "return Num(event)";
                txtV393.Attributes["onkeypress"] = "return Num(event)";
                txtV394.Attributes["onkeypress"] = "return Num(event)";
                txtV395.Attributes["onkeypress"] = "return Num(event)";
                txtV396.Attributes["onkeypress"] = "return Num(event)";
                txtV397.Attributes["onkeypress"] = "return Num(event)";
                txtV398.Attributes["onkeypress"] = "return Num(event)";
                txtV399.Attributes["onkeypress"] = "return Num(event)";
                txtV400.Attributes["onkeypress"] = "return Num(event)";
                txtV401.Attributes["onkeypress"] = "return Num(event)";
                txtV402.Attributes["onkeypress"] = "return Num(event)";
                txtV403.Attributes["onkeypress"] = "return Num(event)";
                txtV404.Attributes["onkeypress"] = "return Num(event)";
                txtV405.Attributes["onkeypress"] = "return Num(event)";
                txtV406.Attributes["onkeypress"] = "return Num(event)";
                txtV407.Attributes["onkeypress"] = "return Num(event)";
                txtV408.Attributes["onkeypress"] = "return Num(event)";
                txtV409.Attributes["onkeypress"] = "return Num(event)";
                txtV410.Attributes["onkeypress"] = "return Num(event)";
                txtV411.Attributes["onkeypress"] = "return Num(event)";
                txtV412.Attributes["onkeypress"] = "return Num(event)";
                txtV413.Attributes["onkeypress"] = "return Num(event)";
                txtV414.Attributes["onkeypress"] = "return Num(event)";
                txtV415.Attributes["onkeypress"] = "return Num(event)";
                txtV416.Attributes["onkeypress"] = "return Num(event)";
                txtV417.Attributes["onkeypress"] = "return Num(event)";
                txtV418.Attributes["onkeypress"] = "return Num(event)";
                txtV419.Attributes["onkeypress"] = "return Num(event)";
                txtV420.Attributes["onkeypress"] = "return Num(event)";
                txtV421.Attributes["onkeypress"] = "return Num(event)";
                txtV422.Attributes["onkeypress"] = "return Num(event)";
                txtV423.Attributes["onkeypress"] = "return Num(event)";
                txtV424.Attributes["onkeypress"] = "return Num(event)";
                txtV425.Attributes["onkeypress"] = "return Num(event)";
                txtV426.Attributes["onkeypress"] = "return Num(event)";
                txtV427.Attributes["onkeypress"] = "return Num(event)";
                txtV428.Attributes["onkeypress"] = "return Num(event)";
                txtV429.Attributes["onkeypress"] = "return Num(event)";
                txtV430.Attributes["onkeypress"] = "return Num(event)";
                txtV431.Attributes["onkeypress"] = "return Num(event)";
                txtV432.Attributes["onkeypress"] = "return Num(event)";
                txtV433.Attributes["onkeypress"] = "return Num(event)";
                txtV434.Attributes["onkeypress"] = "return Num(event)";
                txtV435.Attributes["onkeypress"] = "return Num(event)";
                txtV436.Attributes["onkeypress"] = "return Num(event)";
                txtV437.Attributes["onkeypress"] = "return Num(event)";
                txtV438.Attributes["onkeypress"] = "return Num(event)";
                txtV439.Attributes["onkeypress"] = "return Num(event)";
                txtV440.Attributes["onkeypress"] = "return Num(event)";
                txtV441.Attributes["onkeypress"] = "return Num(event)";
                txtV442.Attributes["onkeypress"] = "return Num(event)";
                txtV443.Attributes["onkeypress"] = "return Num(event)";
                txtV444.Attributes["onkeypress"] = "return Num(event)";
                txtV445.Attributes["onkeypress"] = "return Num(event)";
                txtV446.Attributes["onkeypress"] = "return Num(event)";
                txtV447.Attributes["onkeypress"] = "return Num(event)";
                txtV448.Attributes["onkeypress"] = "return Num(event)";
                txtV449.Attributes["onkeypress"] = "return Num(event)";
                txtV450.Attributes["onkeypress"] = "return Num(event)";
                #endregion
                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
 #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();


                //lnkModificacionDatosAl.NavigateUrl = Class911.Liga_ModificacionDatosAlumno(controlDP);
                //lnkControlEscolarABC.NavigateUrl = Class911.Liga_ControlEscolar(controlDP);
                //lnkCalificaciones.NavigateUrl = Class911.Liga_Calificaciones(controlDP);

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        protected void lnkRefrescar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
            // Actualiza Info
            Class911.CargaInicialCuestionario(controlDP, 1);
            // Recarga datos
            hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);
            Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

            #region Volver a escribir JS Call Back
            String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
            String callbackScript_Variables;
            callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
            #endregion

        }

        //*********************************


    }
}
