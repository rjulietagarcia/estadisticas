using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_4
{
    public partial class AG6_911_4 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV451.Attributes["onkeypress"] = "return Num(event)";
                txtV452.Attributes["onkeypress"] = "return Num(event)";
                txtV453.Attributes["onkeypress"] = "return Num(event)";
                txtV454.Attributes["onkeypress"] = "return Num(event)";
                txtV455.Attributes["onkeypress"] = "return Num(event)";
                txtV456.Attributes["onkeypress"] = "return Num(event)";
                txtV457.Attributes["onkeypress"] = "return Num(event)";
                txtV458.Attributes["onkeypress"] = "return Num(event)";
                txtV459.Attributes["onkeypress"] = "return Num(event)";
                txtV460.Attributes["onkeypress"] = "return Num(event)";
                txtV461.Attributes["onkeypress"] = "return Num(event)";
                txtV462.Attributes["onkeypress"] = "return Num(event)";
                txtV463.Attributes["onkeypress"] = "return Num(event)";
                txtV464.Attributes["onkeypress"] = "return Num(event)";
                txtV465.Attributes["onkeypress"] = "return Num(event)";
                txtV466.Attributes["onkeypress"] = "return Num(event)";
                txtV467.Attributes["onkeypress"] = "return Num(event)";
                txtV468.Attributes["onkeypress"] = "return Num(event)";
                txtV469.Attributes["onkeypress"] = "return Num(event)";
                txtV470.Attributes["onkeypress"] = "return Num(event)";
                txtV471.Attributes["onkeypress"] = "return Num(event)";
                txtV472.Attributes["onkeypress"] = "return Num(event)";
                txtV473.Attributes["onkeypress"] = "return Num(event)";
                txtV474.Attributes["onkeypress"] = "return Num(event)";
                txtV475.Attributes["onkeypress"] = "return Num(event)";
                txtV476.Attributes["onkeypress"] = "return Num(event)";
                txtV477.Attributes["onkeypress"] = "return Num(event)";
                txtV478.Attributes["onkeypress"] = "return Num(event)";
                txtV479.Attributes["onkeypress"] = "return Num(event)";
                txtV480.Attributes["onkeypress"] = "return Num(event)";
                txtV481.Attributes["onkeypress"] = "return Num(event)";
                txtV482.Attributes["onkeypress"] = "return Num(event)";
                txtV483.Attributes["onkeypress"] = "return Num(event)";
                txtV484.Attributes["onkeypress"] = "return Num(event)";
                txtV485.Attributes["onkeypress"] = "return Num(event)";
                txtV486.Attributes["onkeypress"] = "return Num(event)";
                txtV487.Attributes["onkeypress"] = "return Num(event)";
                txtV488.Attributes["onkeypress"] = "return Num(event)";
                txtV489.Attributes["onkeypress"] = "return Num(event)";
                txtV490.Attributes["onkeypress"] = "return Num(event)";
                txtV491.Attributes["onkeypress"] = "return Num(event)";
                txtV492.Attributes["onkeypress"] = "return Num(event)";
                txtV493.Attributes["onkeypress"] = "return Num(event)";
                txtV494.Attributes["onkeypress"] = "return Num(event)";
                txtV495.Attributes["onkeypress"] = "return Num(event)";
                txtV496.Attributes["onkeypress"] = "return Num(event)";
                txtV497.Attributes["onkeypress"] = "return Num(event)";
                txtV498.Attributes["onkeypress"] = "return Num(event)";
                txtV499.Attributes["onkeypress"] = "return Num(event)";
                txtV500.Attributes["onkeypress"] = "return Num(event)";
                txtV501.Attributes["onkeypress"] = "return Num(event)";
                txtV502.Attributes["onkeypress"] = "return Num(event)";
                txtV503.Attributes["onkeypress"] = "return Num(event)";
                txtV504.Attributes["onkeypress"] = "return Num(event)";
                txtV505.Attributes["onkeypress"] = "return Num(event)";
                txtV506.Attributes["onkeypress"] = "return Num(event)";
                txtV507.Attributes["onkeypress"] = "return Num(event)";
                txtV508.Attributes["onkeypress"] = "return Num(event)";
                txtV509.Attributes["onkeypress"] = "return Num(event)";
                txtV510.Attributes["onkeypress"] = "return Num(event)";
                txtV511.Attributes["onkeypress"] = "return Num(event)";
                txtV512.Attributes["onkeypress"] = "return Num(event)";
                txtV513.Attributes["onkeypress"] = "return Num(event)";
                #endregion
                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                 #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();


                //lnkModificacionDatosAl.NavigateUrl = Class911.Liga_ModificacionDatosAlumno(controlDP);
                //lnkControlEscolarABC.NavigateUrl = Class911.Liga_ControlEscolar(controlDP);
                //lnkCalificaciones.NavigateUrl = Class911.Liga_Calificaciones(controlDP);

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        protected void lnkRefrescar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
            // Actualiza Info
            Class911.CargaInicialCuestionario(controlDP, 1);
            // Recarga datos
            hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);
            Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

            #region Volver a escribir JS Call Back
            String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
            String callbackScript_Variables;
            callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
            #endregion

        }

        //*********************************


    }
}
