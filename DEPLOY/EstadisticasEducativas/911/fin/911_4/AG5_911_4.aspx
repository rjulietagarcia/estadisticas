<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.4(5�)" AutoEventWireup="true" CodeBehind="AG5_911_4.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_4.AG5_911_4" %>

    

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script type="text/javascript" src="../../../tema/js/ArrowsHandler.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 9;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
     
    
    <div id="logo"></div>
    <div style="min-width: 1100px; height: 65px;">
        <div id="header">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <span>EDUCACI�N PRIMARIA</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
                </tr>
            </table>
        </div>
    </div>

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_4',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_4',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_4',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_4',true)"><a href="#" title=""><span>3�</span></a></li>
        <li onclick="openPage('AG4_911_4',true)"><a href="#" title=""><span>4�</span></a></li>
        <li onclick="openPage('AG5_911_4',true)"><a href="#" title="" class="activo"><span>5�</span></a></li>
        <li onclick="openPage('AG6_911_4',false)"><a href="#" title=""><span>6�</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div class="balloonstyle" id="tooltipayuda">
    <p>Revisar la cantidad de alumnos inscritos, existentes y aprobados por grado, edad y g�nero, y en caso de observar alguna inconsistencia en la informaci�n favor de realizar los ajustes pertinentes en el sistema de Control Escolar.</p>
    <p>La cantidad de alumnos aprobados se mostrar� cuando se haya concluido el ingreso de las calificaciones del quinto bimestre. Por lo tanto no deber� oficializar la estad�stica hasta que todas las calificaciones est�n en el sistema y la informaci�n en este cuestionario haya sido debidamente validada.</p>
    <p>Para calcular la edad de los alumnos se ha considerado a�os cumplidos al 8 de julio del a�o en curso, que es la fecha en que termina el ciclo escolar.</p>
    
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
           

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

         
           
         <table style="text-align:center">
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbl1" runat="server" Text="5�" CssClass="lblRojo" Font-Size="25px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td style="width: 81px">
                                <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV379" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10101" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV380" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10102" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV381" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10103" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV382" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10104" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV383" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10105" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV384" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10106" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV385" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10107" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV386" runat="server" Columns="4" ReadOnly="True" TabIndex="10108" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV387" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10201" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV388" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10202" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV389" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10203" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV390" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10204" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV391" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10205" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV392" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10206" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV393" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10207" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV394" runat="server" Columns="4" ReadOnly="True" TabIndex="10208" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosH" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV395" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10301" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV396" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10302" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV397" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10303" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV398" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10304" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV399" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10305" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV400" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10306" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV401" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10307" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV402" runat="server" Columns="4" ReadOnly="True" TabIndex="10308" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                         </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV403" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10401" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV404" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10402" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV405" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10403" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV406" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10404" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV407" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10405" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV408" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10406" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV409" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10407" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV410" runat="server" Columns="4" ReadOnly="True" TabIndex="10408" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV411" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10501" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV412" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10502" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV413" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10503" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV414" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10504" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV415" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10505" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV416" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10506" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV417" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10507" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV418" runat="server" Columns="4" ReadOnly="True" TabIndex="10508" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosM" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV419" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10601" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV420" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10602" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV421" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10603" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV422" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10604" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV423" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10605" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV424" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10606" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV425" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10607" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV426" runat="server" Columns="4" ReadOnly="True" TabIndex="10608" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td style="height: 102px">
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblSubtotal" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV427" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10701" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV428" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10702" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV429" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10703" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV430" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10704" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV431" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10705" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV432" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10706" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV433" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10707" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV434" runat="server" Columns="4" ReadOnly="True" TabIndex="10708" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV435" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10801" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV436" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10802" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV437" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10803" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV438" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10804" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV439" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10805" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV440" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10806" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV441" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10807" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV442" runat="server" Columns="4" ReadOnly="True" TabIndex="10808" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosS" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV443" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10901" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV444" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10902" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV445" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10903" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV446" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10904" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV447" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10905" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV448" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10906" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV449" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10907" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV450" runat="server" Columns="4" ReadOnly="True" TabIndex="10908" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px; height: 16px">
                            </td>
                            <td style="width: 132px; height: 16px">
                                &nbsp;</td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 81px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                            </td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 81px">
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV663" runat="server" Columns="2" ReadOnly="True" TabIndex="11101" CssClass="lblNegro" MaxLength="2">0</asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG4_911_4',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG4_911_4',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                 <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AG6_911_4',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG6_911_4',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
<div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
            <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';

 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
         
</asp:Content>


