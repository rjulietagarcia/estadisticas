<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.4(Total)" AutoEventWireup="true" CodeBehind="AGT_911_4.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_4.AGT_911_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
    
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script type="text/javascript" src="../../../tema/js/ArrowsHandler.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>
    
     <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width: 1100px; height: 65px;">
        <div id="header">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <span>EDUCACI�N PRIMARIA</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
                </tr>
            </table>
        </div>
    </div>

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_4',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_4',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_4',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_4',true)"><a href="#" title=""><span>3�</span></a></li>
        <li onclick="openPage('AG4_911_4',true)"><a href="#" title=""><span>4�</span></a></li>
        <li onclick="openPage('AG5_911_4',true)"><a href="#" title=""><span>5�</span></a></li>
        <li onclick="openPage('AG6_911_4',true)"><a href="#" title=""><span>6�</span></a></li>
        <li onclick="openPage('AGT_911_4',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_4',false)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div class="balloonstyle" id="tooltipayuda">
    <p>Revisar la cantidad de alumnos inscritos, existentes y aprobados por grado, edad y g�nero, y en caso de observar alguna inconsistencia en la informaci�n favor de realizar los ajustes pertinentes en el sistema de Control Escolar.</p>
    <p>La cantidad de alumnos aprobados se mostrar� cuando se haya concluido el ingreso de las calificaciones del quinto bimestre. Por lo tanto no deber� oficializar la estad�stica hasta que todas las calificaciones est�n en el sistema y la informaci�n en este cuestionario haya sido debidamente validada.</p>
    <p>Para calcular la edad de los alumnos se ha considerado a�os cumplidos al 8 de julio del a�o en curso, que es la fecha en que termina el ciclo escolar.</p>
    
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

         <table style="text-align:center">
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbl1" runat="server" Text="TOTAL" CssClass="lblRojo" Font-Size="25px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl_6" runat="server" Text="Menos de 6 a�os" CssClass="lblRojo" Width="48px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="Label1" runat="server" Text="6 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl7" runat="server" Text="7 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl8" runat="server" Text="8 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td style="width: 81px">
                                <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left; height: 32px;">
                                <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV514" runat="server" Columns="3" ReadOnly="True" MaxLength="3" TabIndex="10101" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV515" runat="server" Columns="3" ReadOnly="True" TabIndex="10102" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV516" runat="server" Columns="3" ReadOnly="True" TabIndex="10103" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV517" runat="server" Columns="3" ReadOnly="True" TabIndex="10104" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV518" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10105" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV519" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10106" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV520" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10107" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV521" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10108" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV522" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10109" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV523" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10110" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px; height: 32px;">
                                <asp:TextBox ID="txtV524" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10111" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV525" runat="server" Columns="4" ReadOnly="True" TabIndex="10112" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV526" runat="server" Columns="3" ReadOnly="True" TabIndex="10201" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV527" runat="server" Columns="3" ReadOnly="True" TabIndex="10202" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV528" runat="server" Columns="3" ReadOnly="True" TabIndex="10203" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV529" runat="server" Columns="3" ReadOnly="True" TabIndex="10204" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV530" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10205" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV531" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10206" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV532" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10207" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV533" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10208" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV534" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10209" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV535" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10210" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV536" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10211" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV537" runat="server" Columns="4" ReadOnly="True" TabIndex="10212" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosH" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV538" runat="server" Columns="3" ReadOnly="True" TabIndex="10301" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>                        
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV539" runat="server" Columns="3" ReadOnly="True" TabIndex="10302" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV540" runat="server" Columns="3" ReadOnly="True" TabIndex="10303" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV541" runat="server" Columns="3" ReadOnly="True" TabIndex="10304" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV542" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10305" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV543" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10306" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV544" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10307" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV545" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10308" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV546" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10309" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV547" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10310" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV548" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10311" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV549" runat="server" Columns="4" ReadOnly="True" TabIndex="10312" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                         </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV550" runat="server" Columns="3" ReadOnly="True" TabIndex="10401" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV551" runat="server" Columns="3" ReadOnly="True" TabIndex="10402" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV552" runat="server" Columns="3" ReadOnly="True" TabIndex="10403" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV553" runat="server" Columns="3" ReadOnly="True" TabIndex="10404" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV554" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10405" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV555" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10406" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV556" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10407" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV557" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10408" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV558" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10409" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV559" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10410" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV560" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10411" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV561" runat="server" Columns="4" ReadOnly="True" TabIndex="10412" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV562" runat="server" Columns="3" ReadOnly="True" TabIndex="10501" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV563" runat="server" Columns="3" ReadOnly="True" TabIndex="10502" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV564" runat="server" Columns="3" ReadOnly="True" TabIndex="10503" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV565" runat="server" Columns="3" ReadOnly="True" TabIndex="10504" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV566" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10505" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV567" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10506" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV568" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10507" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV569" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10508" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV570" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10509" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV571" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10510" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV572" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10511" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV573" runat="server" Columns="4" ReadOnly="True" TabIndex="10512" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosM" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV574" runat="server" Columns="3" ReadOnly="True" TabIndex="10601" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV575" runat="server" Columns="3" ReadOnly="True" TabIndex="10602" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV576" runat="server" Columns="3" ReadOnly="True" TabIndex="10603" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV577" runat="server" Columns="3" ReadOnly="True" TabIndex="10604" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV578" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10605" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV579" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10606" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV580" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10607" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV581" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10608" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV582" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10609" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV583" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10610" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV584" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10611" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV585" runat="server" Columns="4" ReadOnly="True" TabIndex="10612" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td style="height: 102px">
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblSubtotal" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV586" runat="server" Columns="3" ReadOnly="True" TabIndex="10701" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV587" runat="server" Columns="3" ReadOnly="True" TabIndex="10702" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV588" runat="server" Columns="3" ReadOnly="True" TabIndex="10703" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV589" runat="server" Columns="3" ReadOnly="True" TabIndex="10704" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV590" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10705" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV591" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10706" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV592" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10707" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV593" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10708" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV594" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10709" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV595" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10710" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV596" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10711" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV597" runat="server" Columns="4" ReadOnly="True" TabIndex="10712" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV598" runat="server" Columns="3" ReadOnly="True" TabIndex="10801" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV599" runat="server" Columns="3" ReadOnly="True" TabIndex="10802" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV600" runat="server" Columns="3" ReadOnly="True" TabIndex="10803" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV601" runat="server" Columns="3" ReadOnly="True" TabIndex="10804" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV602" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10805" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV603" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10806" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV604" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10807" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV605" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10808" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV606" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10809" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV607" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10810" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV608" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10811" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV609" runat="server" Columns="4" ReadOnly="True" TabIndex="10812" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosS" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV610" runat="server" Columns="3" ReadOnly="True" TabIndex="10901" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV611" runat="server" Columns="3" ReadOnly="True" TabIndex="10902" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV612" runat="server" Columns="3" ReadOnly="True" TabIndex="10903" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV613" runat="server" Columns="3" ReadOnly="True" TabIndex="10904" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV614" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10905" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV615" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10906" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV616" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10907" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV617" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10908" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV618" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10909" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV619" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10910" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV620" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10911" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV621" runat="server" Columns="4" ReadOnly="True" TabIndex="10912" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px; height: 16px">
                            </td>
                            <td style="width: 132px; height: 16px">
                                &nbsp;</td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 81px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                            </td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 81px">
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV665" runat="server" Columns="2" ReadOnly="True" TabIndex="11101" CssClass="lblNegro" MaxLength="2">0</asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG6_911_4',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG6_911_4',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                 <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AGD_911_4',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_4',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        <div class="divResultado" id="divResultado"></div> 
            <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
         
</asp:Content>
