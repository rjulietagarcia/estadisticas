using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
namespace EstadisticasEducativas._911.fin._911_4
{
    public partial class AG4_911_4 : System.Web.UI.Page, ICallbackEventHandler
    {

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV298.Attributes["onkeypress"] = "return Num(event)";
                txtV299.Attributes["onkeypress"] = "return Num(event)";
                txtV300.Attributes["onkeypress"] = "return Num(event)";
                txtV301.Attributes["onkeypress"] = "return Num(event)";
                txtV302.Attributes["onkeypress"] = "return Num(event)";
                txtV303.Attributes["onkeypress"] = "return Num(event)";
                txtV304.Attributes["onkeypress"] = "return Num(event)";
                txtV305.Attributes["onkeypress"] = "return Num(event)";
                txtV306.Attributes["onkeypress"] = "return Num(event)";
                txtV307.Attributes["onkeypress"] = "return Num(event)";
                txtV308.Attributes["onkeypress"] = "return Num(event)";
                txtV309.Attributes["onkeypress"] = "return Num(event)";
                txtV310.Attributes["onkeypress"] = "return Num(event)";
                txtV311.Attributes["onkeypress"] = "return Num(event)";
                txtV312.Attributes["onkeypress"] = "return Num(event)";
                txtV313.Attributes["onkeypress"] = "return Num(event)";
                txtV314.Attributes["onkeypress"] = "return Num(event)";
                txtV315.Attributes["onkeypress"] = "return Num(event)";
                txtV316.Attributes["onkeypress"] = "return Num(event)";
                txtV317.Attributes["onkeypress"] = "return Num(event)";
                txtV318.Attributes["onkeypress"] = "return Num(event)";
                txtV319.Attributes["onkeypress"] = "return Num(event)";
                txtV320.Attributes["onkeypress"] = "return Num(event)";
                txtV321.Attributes["onkeypress"] = "return Num(event)";
                txtV322.Attributes["onkeypress"] = "return Num(event)";
                txtV323.Attributes["onkeypress"] = "return Num(event)";
                txtV324.Attributes["onkeypress"] = "return Num(event)";
                txtV325.Attributes["onkeypress"] = "return Num(event)";
                txtV326.Attributes["onkeypress"] = "return Num(event)";
                txtV327.Attributes["onkeypress"] = "return Num(event)";
                txtV328.Attributes["onkeypress"] = "return Num(event)";
                txtV329.Attributes["onkeypress"] = "return Num(event)";
                txtV330.Attributes["onkeypress"] = "return Num(event)";
                txtV331.Attributes["onkeypress"] = "return Num(event)";
                txtV332.Attributes["onkeypress"] = "return Num(event)";
                txtV333.Attributes["onkeypress"] = "return Num(event)";
                txtV334.Attributes["onkeypress"] = "return Num(event)";
                txtV335.Attributes["onkeypress"] = "return Num(event)";
                txtV336.Attributes["onkeypress"] = "return Num(event)";
                txtV337.Attributes["onkeypress"] = "return Num(event)";
                txtV338.Attributes["onkeypress"] = "return Num(event)";
                txtV339.Attributes["onkeypress"] = "return Num(event)";
                txtV340.Attributes["onkeypress"] = "return Num(event)";
                txtV341.Attributes["onkeypress"] = "return Num(event)";
                txtV342.Attributes["onkeypress"] = "return Num(event)";
                txtV343.Attributes["onkeypress"] = "return Num(event)";
                txtV344.Attributes["onkeypress"] = "return Num(event)";
                txtV345.Attributes["onkeypress"] = "return Num(event)";
                txtV346.Attributes["onkeypress"] = "return Num(event)";
                txtV347.Attributes["onkeypress"] = "return Num(event)";
                txtV348.Attributes["onkeypress"] = "return Num(event)";
                txtV349.Attributes["onkeypress"] = "return Num(event)";
                txtV350.Attributes["onkeypress"] = "return Num(event)";
                txtV351.Attributes["onkeypress"] = "return Num(event)";
                txtV352.Attributes["onkeypress"] = "return Num(event)";
                txtV353.Attributes["onkeypress"] = "return Num(event)";
                txtV354.Attributes["onkeypress"] = "return Num(event)";
                txtV355.Attributes["onkeypress"] = "return Num(event)";
                txtV356.Attributes["onkeypress"] = "return Num(event)";
                txtV357.Attributes["onkeypress"] = "return Num(event)";
                txtV358.Attributes["onkeypress"] = "return Num(event)";
                txtV359.Attributes["onkeypress"] = "return Num(event)";
                txtV360.Attributes["onkeypress"] = "return Num(event)";
                txtV361.Attributes["onkeypress"] = "return Num(event)";
                txtV362.Attributes["onkeypress"] = "return Num(event)";
                txtV363.Attributes["onkeypress"] = "return Num(event)";
                txtV364.Attributes["onkeypress"] = "return Num(event)";
                txtV365.Attributes["onkeypress"] = "return Num(event)";
                txtV366.Attributes["onkeypress"] = "return Num(event)";
                txtV367.Attributes["onkeypress"] = "return Num(event)";
                txtV368.Attributes["onkeypress"] = "return Num(event)";
                txtV369.Attributes["onkeypress"] = "return Num(event)";
                txtV370.Attributes["onkeypress"] = "return Num(event)";
                txtV371.Attributes["onkeypress"] = "return Num(event)";
                txtV372.Attributes["onkeypress"] = "return Num(event)";
                txtV373.Attributes["onkeypress"] = "return Num(event)";
                txtV374.Attributes["onkeypress"] = "return Num(event)";
                txtV375.Attributes["onkeypress"] = "return Num(event)";
                txtV376.Attributes["onkeypress"] = "return Num(event)";
                txtV377.Attributes["onkeypress"] = "return Num(event)";
                txtV378.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();


                //lnkModificacionDatosAl.NavigateUrl = Class911.Liga_ModificacionDatosAlumno(controlDP);
                //lnkControlEscolarABC.NavigateUrl = Class911.Liga_ControlEscolar(controlDP);
                //lnkCalificaciones.NavigateUrl = Class911.Liga_Calificaciones(controlDP);


                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        protected void lnkRefrescar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
            // Actualiza Info
            Class911.CargaInicialCuestionario(controlDP, 1);
            // Recarga datos
            hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);
            Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

            #region Volver a escribir JS Call Back
            String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
            String callbackScript_Variables;
            callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
            #endregion

        }

        //*********************************


    }
}
