<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8P(4�, 5� y Total)" AutoEventWireup="true" CodeBehind="AG4_911_8P.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8P.AG4_911_8P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="/../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 14;
        MaxRow = 19;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1300px; height:65px;">
    <div id="header">


    <table style =" padding-left:300px;">
        <tr><td><span>PROFESIONAL T�CNICO MEDIO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1300px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_8P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('AGD_911_8P',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('AG1_911_8P',true)"><a href="#" title=""><span>1�, 2� y 3�</span></a></li>
        <li onclick="openPage('AG4_911_8P',true)"><a href="#" title="" class="activo"><span>4�, 5� y TOTAL</span></a></li>
        <li onclick="openPage('PROCEIES_911_8P',false)"><a href="#" title=""><span>PROCEIES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div> <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td style="width: 340px">
                    <table id="TABLE1" style="text-align: center" >
                        <tr>
                            <td colspan="16" rowspan="1" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Size="16px" Height="17px"
                                    Text="II. ALUMNOS POR EDAD"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="height: 3px; text-align: left">
                                <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Height="17px" Text="1. Escriba el total de alumnos, desglos�ndolo por grado, sexo, inscripci�n total, existencia, aprobados y edad. Verifique que la suma de los alumnos por edad sea igual al total."></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="height: 3px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 3px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 68px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 54px; height: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="7" style="width: 50px; height: 3px; text-align: center; padding-right: 5px; padding-left: 5px;">
                                <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="4� (Semestres 7 y 8)"></asp:Label></td>
                            <td colspan="2" rowspan="" style="text-align: center; height: 3px;">
                                </td>
                            <td colspan="" rowspan="" style="width: 67px; text-align: center; height: 3px;">
                                <asp:Label ID="lbl6_Menos" runat="server" CssClass="lblRojo" Text="14 a�os y menos"></asp:Label></td>
                            <td style="width: 67px; height: 3px;">
                                <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="15 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl7" runat="server" CssClass="lblRojo" Text="16 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="17 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="18 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="19 a�os"></asp:Label></td>
                            <td style="width: 68px; height: 3px">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="20 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="21 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="22 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="23 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl15_Mas" runat="server" CssClass="lblRojo" Text="24 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px;">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="25 a�os y m�s"></asp:Label></td>
                            <td style="width: 54px; height: 3px">
                                <asp:Label ID="lblGrupos1" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 85px; padding-right: 5px;" rowspan="3">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblNvoIngresoHombres1" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="TextBox25" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="TextBox26" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox27" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV292" runat="server" Columns="4" TabIndex="10101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV293" runat="server" Columns="4" TabIndex="10102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV294" runat="server" Columns="4" TabIndex="10103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                                <asp:TextBox ID="txtV295" runat="server" Columns="4" TabIndex="10104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV296" runat="server" Columns="4" TabIndex="10105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV297" runat="server" Columns="4" TabIndex="10106" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV298" runat="server" Columns="4" TabIndex="10107" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV299" runat="server" Columns="4" TabIndex="10108" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="txtV300" runat="server" Columns="4" TabIndex="10109" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV301" runat="server" Columns="4" TabIndex="10110" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblRepetidoresHombres1" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="TextBox24" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="TextBox23" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox22" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV302" runat="server" Columns="4" TabIndex="10201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV303" runat="server" Columns="4" TabIndex="10202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV304" runat="server" Columns="4" TabIndex="10203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                                <asp:TextBox ID="txtV305" runat="server" Columns="4" TabIndex="10204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV306" runat="server" Columns="4" TabIndex="10205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV307" runat="server" Columns="4" TabIndex="10206" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV308" runat="server" Columns="4" TabIndex="10207" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV309" runat="server" Columns="4" TabIndex="10208" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="txtV310" runat="server" Columns="4" TabIndex="10209" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV311" runat="server" Columns="4" TabIndex="10210" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td style="width: 120px; height: 26px; text-align: left">
                                <asp:TextBox ID="TextBox19" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox20" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox21" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV312" runat="server" Columns="4" TabIndex="10301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV313" runat="server" Columns="4" TabIndex="10302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV314" runat="server" Columns="4" TabIndex="10303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                                <asp:TextBox ID="txtV315" runat="server" Columns="4" TabIndex="10304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV316" runat="server" Columns="4" TabIndex="10305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV317" runat="server" Columns="4" TabIndex="10306" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV318" runat="server" Columns="4" TabIndex="10307" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV319" runat="server" Columns="4" TabIndex="10308" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV320" runat="server" Columns="4" TabIndex="10309" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV321" runat="server" Columns="4" TabIndex="10310" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 85px; margin-right: 5px; text-align: left;">
                            <asp:Label ID="lblMujeres" runat="server" CssClass="lblRojo" Text="MUJERES" Height="17px"></asp:Label></td>
                            <td style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox18" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox17" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox16" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV322" runat="server" Columns="4" TabIndex="10401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV323" runat="server" Columns="4" TabIndex="10402" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV324" runat="server" Columns="4" TabIndex="10403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                                <asp:TextBox ID="txtV325" runat="server" Columns="4" TabIndex="10404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV326" runat="server" Columns="4" TabIndex="10405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV327" runat="server" Columns="4" TabIndex="10406" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV328" runat="server" Columns="4" TabIndex="10407" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV329" runat="server" Columns="4" TabIndex="10408" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV330" runat="server" Columns="4" TabIndex="10409" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV331" runat="server" Columns="4" TabIndex="10410" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left;">
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="TextBox13" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox14" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox15" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV332" runat="server" Columns="4" TabIndex="10501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV333" runat="server" Columns="4" TabIndex="10502" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV334" runat="server" Columns="4" TabIndex="10503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                                <asp:TextBox ID="txtV335" runat="server" Columns="4" TabIndex="10504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV336" runat="server" Columns="4" TabIndex="10505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV337" runat="server" Columns="4" TabIndex="10506" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV338" runat="server" Columns="4" TabIndex="10507" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV339" runat="server" Columns="4" TabIndex="10508" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV340" runat="server" Columns="4" TabIndex="10509" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV341" runat="server" Columns="4" TabIndex="10510" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox10" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="TextBox11" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox12" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV342" runat="server" Columns="4" TabIndex="10601" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV343" runat="server" Columns="4" TabIndex="10602" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV344" runat="server" Columns="4" TabIndex="10603" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                                <asp:TextBox ID="txtV345" runat="server" Columns="4" TabIndex="10604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV346" runat="server" Columns="4" TabIndex="10605" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV347" runat="server" Columns="4" TabIndex="10606" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV348" runat="server" Columns="4" TabIndex="10607" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV349" runat="server" Columns="4" TabIndex="10608" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV350" runat="server" Columns="4" TabIndex="10609" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                            <asp:TextBox ID="txtV351" runat="server" Columns="4" TabIndex="10610" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 68px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 54px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="6" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="5� (Semestres 9 y 10)"></asp:Label></td>
                            <td rowspan="3" style="width: 40px; height: 26px; text-align: left">
                            <asp:Label ID="lblHombres2" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtBlank1" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox7" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox8" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox9" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV352" runat="server" Columns="4" TabIndex="10701" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV353" runat="server" Columns="4" TabIndex="10702" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                            <asp:TextBox ID="txtV354" runat="server" Columns="4" TabIndex="10703" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV355" runat="server" Columns="4" TabIndex="10704" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV356" runat="server" Columns="4" TabIndex="10705" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV357" runat="server" Columns="4" TabIndex="10706" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV358" runat="server" Columns="4" TabIndex="10707" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                            <asp:TextBox ID="txtV359" runat="server" Columns="4" TabIndex="10708" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                            <asp:TextBox ID="txtV360" runat="server" Columns="4" TabIndex="10709" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 50px; height: 3px; text-align: left">
                                <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtBlank2" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox4" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="TextBox5" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox6" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV361" runat="server" Columns="4" TabIndex="10801" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV362" runat="server" Columns="4" TabIndex="10802" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                            <asp:TextBox ID="txtV363" runat="server" Columns="4" TabIndex="10803" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV364" runat="server" Columns="4" TabIndex="10804" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV365" runat="server" Columns="4" TabIndex="10805" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV366" runat="server" Columns="4" TabIndex="10806" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV367" runat="server" Columns="4" TabIndex="10807" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV368" runat="server" Columns="4" TabIndex="10808" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV369" runat="server" Columns="4" TabIndex="10809" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="width: 50px; height: 3px; text-align: left">
                                <asp:TextBox ID="TextBox1" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtBlank8" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtBlank13" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtBlank11" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV370" runat="server" Columns="4" TabIndex="10901" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV371" runat="server" Columns="4" TabIndex="10902" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                            <asp:TextBox ID="txtV372" runat="server" Columns="4" TabIndex="10903" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV373" runat="server" Columns="4" TabIndex="10904" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV374" runat="server" Columns="4" TabIndex="10905" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV375" runat="server" Columns="4" TabIndex="10906" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV376" runat="server" Columns="4" TabIndex="10907" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV377" runat="server" Columns="4" TabIndex="10908" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV378" runat="server" Columns="4" TabIndex="10909" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="3" style="width: 50px; height: 3px; text-align: left">
                            <asp:Label ID="lblMujeres2" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtBlank3" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtBlank16" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox3" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="TextBox2" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV379" runat="server" Columns="4" TabIndex="11001" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV380" runat="server" Columns="4" TabIndex="11002" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                            <asp:TextBox ID="txtV381" runat="server" Columns="4" TabIndex="11003" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV382" runat="server" Columns="4" TabIndex="11004" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV383" runat="server" Columns="4" TabIndex="11005" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV384" runat="server" Columns="4" TabIndex="11006" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV385" runat="server" Columns="4" TabIndex="11007" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV386" runat="server" Columns="4" TabIndex="11008" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                            <asp:TextBox ID="txtV387" runat="server" Columns="4" TabIndex="11009" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 50px; height: 3px; text-align: left">
                                <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtBlank4" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtBlank9" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtBlank14" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtBlank12" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV388" runat="server" Columns="4" TabIndex="11101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV389" runat="server" Columns="4" TabIndex="11102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                            <asp:TextBox ID="txtV390" runat="server" Columns="4" TabIndex="11103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV391" runat="server" Columns="4" TabIndex="11104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV392" runat="server" Columns="4" TabIndex="11105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV393" runat="server" Columns="4" TabIndex="11106" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV394" runat="server" Columns="4" TabIndex="11107" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV395" runat="server" Columns="4" TabIndex="11108" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV396" runat="server" Columns="4" TabIndex="11109" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 50px; height: 3px; text-align: left">
                                <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtBlank5" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtBlank10" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtBlank15" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtBlank17" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV397" runat="server" Columns="4" TabIndex="11201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV398" runat="server" Columns="4" TabIndex="11202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 68px; height: 26px">
                            <asp:TextBox ID="txtV399" runat="server" Columns="4" TabIndex="11203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV400" runat="server" Columns="4" TabIndex="11204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV401" runat="server" Columns="4" TabIndex="11205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV402" runat="server" Columns="4" TabIndex="11206" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV403" runat="server" Columns="4" TabIndex="11207" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV404" runat="server" Columns="4" TabIndex="11208" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV405" runat="server" Columns="4" TabIndex="11209" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 68px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 54px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="6" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td rowspan="3" style="width: 40px; height: 26px; text-align: left">
                            <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: left;">
                                <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            <asp:TextBox ID="txtV406" runat="server" Columns="4" TabIndex="11301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV407" runat="server" Columns="4" TabIndex="11302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV408" runat="server" Columns="4" TabIndex="11303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV409" runat="server" Columns="4" TabIndex="11304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV410" runat="server" Columns="4" TabIndex="11305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV411" runat="server" Columns="4" TabIndex="11306" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV412" runat="server" Columns="4" TabIndex="11307" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV413" runat="server" Columns="4" TabIndex="11308" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV414" runat="server" Columns="4" TabIndex="11309" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV415" runat="server" Columns="4" TabIndex="11310" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV416" runat="server" Columns="4" TabIndex="11311" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV417" runat="server" Columns="4" TabIndex="11312" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV418" runat="server" Columns="4" TabIndex="11313" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center">
                            <asp:TextBox ID="txtV419" runat="server" Columns="4" TabIndex="11401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV420" runat="server" Columns="4" TabIndex="11402" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            <asp:TextBox ID="txtV421" runat="server" Columns="4" TabIndex="11403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV422" runat="server" Columns="4" TabIndex="11404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV423" runat="server" Columns="4" TabIndex="11405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV424" runat="server" Columns="4" TabIndex="11406" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV425" runat="server" Columns="4" TabIndex="11407" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV426" runat="server" Columns="4" TabIndex="11408" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV427" runat="server" Columns="4" TabIndex="11409" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV428" runat="server" Columns="4" TabIndex="11410" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV429" runat="server" Columns="4" TabIndex="11411" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV430" runat="server" Columns="4" TabIndex="11412" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV431" runat="server" Columns="4" TabIndex="11413" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center">
                            <asp:TextBox ID="txtV432" runat="server" Columns="4" TabIndex="11501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV433" runat="server" Columns="4" TabIndex="11502" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            <asp:TextBox ID="txtV434" runat="server" Columns="4" TabIndex="11503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV435" runat="server" Columns="4" TabIndex="11504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV436" runat="server" Columns="4" TabIndex="11505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV437" runat="server" Columns="4" TabIndex="11506" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV438" runat="server" Columns="4" TabIndex="11507" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV439" runat="server" Columns="4" TabIndex="11508" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV440" runat="server" Columns="4" TabIndex="11509" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV441" runat="server" Columns="4" TabIndex="11510" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV442" runat="server" Columns="4" TabIndex="11511" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV443" runat="server" Columns="4" TabIndex="11512" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV444" runat="server" Columns="4" TabIndex="11513" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="3" style="width: 50px; height: 3px; text-align: center">
                            <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV445" runat="server" Columns="4" TabIndex="11601" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            <asp:TextBox ID="txtV446" runat="server" Columns="4" TabIndex="11602" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV447" runat="server" Columns="4" TabIndex="11603" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV448" runat="server" Columns="4" TabIndex="11604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV449" runat="server" Columns="4" TabIndex="11605" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV450" runat="server" Columns="4" TabIndex="11606" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV451" runat="server" Columns="4" TabIndex="11607" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV452" runat="server" Columns="4" TabIndex="11608" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV453" runat="server" Columns="4" TabIndex="11609" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV454" runat="server" Columns="4" TabIndex="11610" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV455" runat="server" Columns="4" TabIndex="11611" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                            <asp:TextBox ID="txtV456" runat="server" Columns="4" TabIndex="11612" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV457" runat="server" Columns="4" TabIndex="11613" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV458" runat="server" Columns="4" TabIndex="11701" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV459" runat="server" Columns="4" TabIndex="11702" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV460" runat="server" Columns="4" TabIndex="11703" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV461" runat="server" Columns="4" TabIndex="11704" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV462" runat="server" Columns="4" TabIndex="11705" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV463" runat="server" Columns="4" TabIndex="11706" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV464" runat="server" Columns="4" TabIndex="11707" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV465" runat="server" Columns="4" TabIndex="11708" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV466" runat="server" Columns="4" TabIndex="11709" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV467" runat="server" Columns="4" TabIndex="11710" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV468" runat="server" Columns="4" TabIndex="11711" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV469" runat="server" Columns="4" TabIndex="11712" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV470" runat="server" Columns="4" TabIndex="11713" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV471" runat="server" Columns="4" TabIndex="11801" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV472" runat="server" Columns="4" TabIndex="11802" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV473" runat="server" Columns="4" TabIndex="11803" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV474" runat="server" Columns="4" TabIndex="11804" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV475" runat="server" Columns="4" TabIndex="11805" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV476" runat="server" Columns="4" TabIndex="11806" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV477" runat="server" Columns="4" TabIndex="11807" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV478" runat="server" Columns="4" TabIndex="11808" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV479" runat="server" Columns="4" TabIndex="11809" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV480" runat="server" Columns="4" TabIndex="11810" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV481" runat="server" Columns="4" TabIndex="11811" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV482" runat="server" Columns="4" TabIndex="11812" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV483" runat="server" Columns="4" TabIndex="11813" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG1_911_8P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG1_911_8P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('PROCEIES_911_8P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('PROCEIES_911_8P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
        
</asp:Content>
