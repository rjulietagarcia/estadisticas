<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8P(1�, 2� y 3�)" AutoEventWireup="true" CodeBehind="AG1_911_8P.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8P.AG1_911_8P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="/../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 14;
        MaxRow = 19;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1300px; height:65px;">
    <div id="header">


    <table style =" padding-left:300px;">
        <tr><td><span>PROFESIONAL T�CNICO MEDIO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1300px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_8P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('AGD_911_8P',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('AG1_911_8P',true)"><a href="#" title="" class="activo"><span>1�, 2� y 3�</span></a></li>
        <li onclick="openPage('AG4_911_8P',false)"><a href="#" title=""><span>4�, 5� y TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROCEIES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table id="TABLE1" style="text-align: center" >
                        <tr>
                            <td colspan="16"  style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Size="16px" Height="17px"
                                    Text="II. ALUMNOS POR EDAD"></asp:Label></td>
                            <td colspan="1" style="padding-bottom: 10px; width: 4px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="16"  style="height: 3px; text-align: left">
                                <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Height="17px" Text="1. Escriba el total de alumnos, desglos�ndolo por grado, sexo, inscripci�n total, existencia, aprobados y edad. Verifique que la suma de los alumnos por edad sea igual al total."></asp:Label></td>
                            <td colspan="1" style="width: 4px; height: 3px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td  style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2"  >
                            </td>
                            <td  >
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="7" style="width: 50px; height: 3px; text-align: center; padding-right: 5px; padding-left: 5px;" class="linaBajoAlto">
                                <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1� (Semestres 1 y 2)"></asp:Label></td>
                            <td colspan="2" rowspan="" style="text-align: center; height: 3px;" class="linaBajoAlto">
                                &nbsp;&nbsp;
                                </td>
                            <td colspan="" rowspan="" style="width: 67px; text-align: center; height: 3px;" class="linaBajoAlto">
                                <asp:Label ID="lbl6_Menos" runat="server" CssClass="lblRojo" Text="14 a�os y menos"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="15 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl7" runat="server" CssClass="lblRojo" Text="16 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="17 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="18 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="19 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="20 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="21 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="22 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="23 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lbl15_Mas" runat="server" CssClass="lblRojo" Text="24 a�os"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="25 a�os y m�s"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblGrupos1" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td  rowspan="3" class="linaBajoAlto">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblNvoIngresoHombres1" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL" Width="120px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV76" runat="server" Columns="4" TabIndex="10101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV77" runat="server" Columns="4" TabIndex="10102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV78" runat="server" Columns="4" TabIndex="10103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV79" runat="server" Columns="4" TabIndex="10104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV80" runat="server" Columns="4" TabIndex="10105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV81" runat="server" Columns="4" TabIndex="10106" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV82" runat="server" Columns="4" TabIndex="10107" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV83" runat="server" Columns="4" TabIndex="10108" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV84" runat="server" Columns="4" TabIndex="10109" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV85" runat="server" Columns="4" TabIndex="10110" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV86" runat="server" Columns="4" TabIndex="10111" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV87" runat="server" Columns="4" TabIndex="10112" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV88" runat="server" Columns="4" TabIndex="10113" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo">
                                <asp:Label ID="lblRepetidoresHombres1" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV89" runat="server" Columns="4" TabIndex="10201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV90" runat="server" Columns="4" TabIndex="10202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV91" runat="server" Columns="4" TabIndex="10203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV92" runat="server" Columns="4" TabIndex="10204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV93" runat="server" Columns="4" TabIndex="10205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV94" runat="server" Columns="4" TabIndex="10206" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV95" runat="server" Columns="4" TabIndex="10207" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV96" runat="server" Columns="4" TabIndex="10208" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV97" runat="server" Columns="4" TabIndex="10209" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV98" runat="server" Columns="4" TabIndex="10210" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV99" runat="server" Columns="4" TabIndex="10211" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV100" runat="server" Columns="4" TabIndex="10212" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV101" runat="server" Columns="4" TabIndex="10213" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV102" runat="server" Columns="4" TabIndex="10301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV103" runat="server" Columns="4" TabIndex="10302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV104" runat="server" Columns="4" TabIndex="10303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV105" runat="server" Columns="4" TabIndex="10304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV106" runat="server" Columns="4" TabIndex="10305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV107" runat="server" Columns="4" TabIndex="10306" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV108" runat="server" Columns="4" TabIndex="10307" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV109" runat="server" Columns="4" TabIndex="10308" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV110" runat="server" Columns="4" TabIndex="10309" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV111" runat="server" Columns="4" TabIndex="10310" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV112" runat="server" Columns="4" TabIndex="10311" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV113" runat="server" Columns="4" TabIndex="10312" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV114" runat="server" Columns="4" TabIndex="10313" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" class="linaBajo" >
                            <asp:Label ID="lblMujeres" runat="server" CssClass="lblRojo" Text="MUJERES" Height="17px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV115" runat="server" Columns="4" TabIndex="10401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV116" runat="server" Columns="4" TabIndex="10402" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV117" runat="server" Columns="4" TabIndex="10403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV118" runat="server" Columns="4" TabIndex="10404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV119" runat="server" Columns="4" TabIndex="10405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV120" runat="server" Columns="4" TabIndex="10406" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV121" runat="server" Columns="4" TabIndex="10407" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV122" runat="server" Columns="4" TabIndex="10408" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV123" runat="server" Columns="4" TabIndex="10409" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV124" runat="server" Columns="4" TabIndex="10410" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV125" runat="server" Columns="4" TabIndex="10411" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV126" runat="server" Columns="4" TabIndex="10412" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV127" runat="server" Columns="4" TabIndex="10413" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV128" runat="server" Columns="4" TabIndex="10501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV129" runat="server" Columns="4" TabIndex="10502" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV130" runat="server" Columns="4" TabIndex="10503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV131" runat="server" Columns="4" TabIndex="10504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV132" runat="server" Columns="4" TabIndex="10505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV133" runat="server" Columns="4" TabIndex="10506" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV134" runat="server" Columns="4" TabIndex="10507" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV135" runat="server" Columns="4" TabIndex="10508" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV136" runat="server" Columns="4" TabIndex="10509" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV137" runat="server" Columns="4" TabIndex="10510" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV138" runat="server" Columns="4" TabIndex="10511" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV139" runat="server" Columns="4" TabIndex="10512" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV140" runat="server" Columns="4" TabIndex="10513" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV141" runat="server" Columns="4" TabIndex="10601" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV142" runat="server" Columns="4" TabIndex="10602" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV143" runat="server" Columns="4" TabIndex="10603" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV144" runat="server" Columns="4" TabIndex="10604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV145" runat="server" Columns="4" TabIndex="10605" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV146" runat="server" Columns="4" TabIndex="10606" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV147" runat="server" Columns="4" TabIndex="10607" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV148" runat="server" Columns="4" TabIndex="10608" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV149" runat="server" Columns="4" TabIndex="10609" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV150" runat="server" Columns="4" TabIndex="10610" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV151" runat="server" Columns="4" TabIndex="10611" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV152" runat="server" Columns="4" TabIndex="10612" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV153" runat="server" Columns="4" TabIndex="10613" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td  >
                            </td>
                            <td colspan="2"  >
                            </td>
                            <td  >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td style="width: 54px; height: 26px">
                            </td>
                            <td style="width: 4px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="6" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center" class="linaBajoAlto">
                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2� (Semestres 3 y 4)"></asp:Label></td>
                            <td rowspan="3" class="linaBajoAlto" >
                            <asp:Label ID="lblHombres2" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td class="linaBajoAlto"  >
                                <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td class="linaBajoAlto" >
                            <asp:TextBox ID="txtBlank1" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV154" runat="server" Columns="4" TabIndex="10701" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV155" runat="server" Columns="4" TabIndex="10702" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV156" runat="server" Columns="4" TabIndex="10703" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV157" runat="server" Columns="4" TabIndex="10704" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV158" runat="server" Columns="4" TabIndex="10705" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV159" runat="server" Columns="4" TabIndex="10706" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV160" runat="server" Columns="4" TabIndex="10707" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV161" runat="server" Columns="4" TabIndex="10708" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV162" runat="server" Columns="4" TabIndex="10709" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV163" runat="server" Columns="4" TabIndex="10710" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV164" runat="server" Columns="4" TabIndex="10711" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV165" runat="server" Columns="4" TabIndex="10712" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank2" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV166" runat="server" Columns="4" TabIndex="10801" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV167" runat="server" Columns="4" TabIndex="10802" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV168" runat="server" Columns="4" TabIndex="10803" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV169" runat="server" Columns="4" TabIndex="10804" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV170" runat="server" Columns="4" TabIndex="10805" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV171" runat="server" Columns="4" TabIndex="10806" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV172" runat="server" Columns="4" TabIndex="10807" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV173" runat="server" Columns="4" TabIndex="10808" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV174" runat="server" Columns="4" TabIndex="10809" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV175" runat="server" Columns="4" TabIndex="10810" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV176" runat="server" Columns="4" TabIndex="10811" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV177" runat="server" Columns="4" TabIndex="10812" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo"  >
                                <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="TextBox1" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtV178" runat="server" Columns="4" TabIndex="10901" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV179" runat="server" Columns="4" TabIndex="10902" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV180" runat="server" Columns="4" TabIndex="10903" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV181" runat="server" Columns="4" TabIndex="10904" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV182" runat="server" Columns="4" TabIndex="10905" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV183" runat="server" Columns="4" TabIndex="10906" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV184" runat="server" Columns="4" TabIndex="10907" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV185" runat="server" Columns="4" TabIndex="10908" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV186" runat="server" Columns="4" TabIndex="10909" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV187" runat="server" Columns="4" TabIndex="10910" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV188" runat="server" Columns="4" TabIndex="10911" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV189" runat="server" Columns="4" TabIndex="10912" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" class="linaBajo">
                            <asp:Label ID="lblMujeres2" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td class="linaBajo"  >
                                <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank3" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV190" runat="server" Columns="4" TabIndex="11001" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV191" runat="server" Columns="4" TabIndex="11002" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV192" runat="server" Columns="4" TabIndex="11003" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV193" runat="server" Columns="4" TabIndex="11004" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV194" runat="server" Columns="4" TabIndex="11005" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV195" runat="server" Columns="4" TabIndex="11006" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV196" runat="server" Columns="4" TabIndex="11007" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV197" runat="server" Columns="4" TabIndex="11008" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV198" runat="server" Columns="4" TabIndex="11009" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV199" runat="server" Columns="4" TabIndex="11010" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV200" runat="server" Columns="4" TabIndex="11011" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV201" runat="server" Columns="4" TabIndex="11012" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank4" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV202" runat="server" Columns="4" TabIndex="11101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV203" runat="server" Columns="4" TabIndex="11102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV204" runat="server" Columns="4" TabIndex="11103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV205" runat="server" Columns="4" TabIndex="11104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV206" runat="server" Columns="4" TabIndex="11105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV207" runat="server" Columns="4" TabIndex="11106" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV208" runat="server" Columns="4" TabIndex="11107" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV209" runat="server" Columns="4" TabIndex="11108" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV210" runat="server" Columns="4" TabIndex="11109" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV211" runat="server" Columns="4" TabIndex="11110" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV212" runat="server" Columns="4" TabIndex="11111" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV213" runat="server" Columns="4" TabIndex="11112" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank5" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV214" runat="server" Columns="4" TabIndex="11201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV215" runat="server" Columns="4" TabIndex="11202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV216" runat="server" Columns="4" TabIndex="11203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV217" runat="server" Columns="4" TabIndex="11204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV218" runat="server" Columns="4" TabIndex="11205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV219" runat="server" Columns="4" TabIndex="11206" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV220" runat="server" Columns="4" TabIndex="11207" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV221" runat="server" Columns="4" TabIndex="11208" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV222" runat="server" Columns="4" TabIndex="11209" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV223" runat="server" Columns="4" TabIndex="11210" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV224" runat="server" Columns="4" TabIndex="11211" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV225" runat="server" Columns="4" TabIndex="11212" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td  style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2"  >
                            </td>
                            <td  >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td style="width: 54px; height: 26px">
                            </td>
                            <td style="width: 4px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="6" style="padding-right: 5px; padding-left: 5px; width: 50px;
                                height: 3px; text-align: center" class="linaBajoAlto">
                            <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3� (Semestres 5 y 6)"></asp:Label></td>
                            <td rowspan="3" class="linaBajoAlto" >
                            <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td  style="width: 67px; height: 26px; text-align: left;" class="linaBajoAlto">
                                <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td class="linaBajoAlto" >
                            <asp:TextBox ID="txtBlank8" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajoAlto" >
                            <asp:TextBox ID="txtBlank13" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV226" runat="server" Columns="4" TabIndex="11361" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV227" runat="server" Columns="4" TabIndex="11304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV228" runat="server" Columns="4" TabIndex="11305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV229" runat="server" Columns="4" TabIndex="11306" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV230" runat="server" Columns="4" TabIndex="11307" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV231" runat="server" Columns="4" TabIndex="11308" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV232" runat="server" Columns="4" TabIndex="11309" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV233" runat="server" Columns="4" TabIndex="11310" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV234" runat="server" Columns="4" TabIndex="11311" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV235" runat="server" Columns="4" TabIndex="11312" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajoAlto">
                            <asp:TextBox ID="txtV236" runat="server" Columns="4" TabIndex="11313" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo"  >
                                <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank9" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank14" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtV237" runat="server" Columns="4" TabIndex="11461" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV238" runat="server" Columns="4" TabIndex="11404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV239" runat="server" Columns="4" TabIndex="11405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV240" runat="server" Columns="4" TabIndex="11406" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV241" runat="server" Columns="4" TabIndex="11407" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV242" runat="server" Columns="4" TabIndex="11408" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV243" runat="server" Columns="4" TabIndex="11409" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV244" runat="server" Columns="4" TabIndex="11410" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV245" runat="server" Columns="4" TabIndex="11411" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV246" runat="server" Columns="4" TabIndex="11412" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV247" runat="server" Columns="4" TabIndex="11413" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo"  >
                                <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                                <asp:TextBox ID="TextBox2" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo"  >
                                <asp:TextBox ID="TextBox3" runat="server" BackColor="Silver" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV248" runat="server" Columns="4" TabIndex="11561" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV249" runat="server" Columns="4" TabIndex="11504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV250" runat="server" Columns="4" TabIndex="11505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV251" runat="server" Columns="4" TabIndex="11506" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV252" runat="server" Columns="4" TabIndex="11507" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV253" runat="server" Columns="4" TabIndex="11508" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV254" runat="server" Columns="4" TabIndex="11509" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV255" runat="server" Columns="4" TabIndex="11510" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV256" runat="server" Columns="4" TabIndex="11511" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV257" runat="server" Columns="4" TabIndex="11512" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV258" runat="server" Columns="4" TabIndex="11513" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 50px; height: 3px; text-align: center" class="linaBajo">
                            <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td class="linaBajo"  >
                                <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Text="INSCRIPCION TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank10" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtBlank15" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV259" runat="server" Columns="4" TabIndex="11661" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV260" runat="server" Columns="4" TabIndex="11604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV261" runat="server" Columns="4" TabIndex="11605" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV262" runat="server" Columns="4" TabIndex="11606" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV263" runat="server" Columns="4" TabIndex="11607" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV264" runat="server" Columns="4" TabIndex="11608" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV265" runat="server" Columns="4" TabIndex="11609" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV266" runat="server" Columns="4" TabIndex="11610" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV267" runat="server" Columns="4" TabIndex="11611" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV268" runat="server" Columns="4" TabIndex="11612" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV269" runat="server" Columns="4" TabIndex="11613" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo"  >
                                <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank11" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank16" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV270" runat="server" Columns="4" TabIndex="11761" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV271" runat="server" Columns="4" TabIndex="11704" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV272" runat="server" Columns="4" TabIndex="11705" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV273" runat="server" Columns="4" TabIndex="11706" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV274" runat="server" Columns="4" TabIndex="11707" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV275" runat="server" Columns="4" TabIndex="11708" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV276" runat="server" Columns="4" TabIndex="11709" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV277" runat="server" Columns="4" TabIndex="11710" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV278" runat="server" Columns="4" TabIndex="11711" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV279" runat="server" Columns="4" TabIndex="11712" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV280" runat="server" Columns="4" TabIndex="11713" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo"  >
                                <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Text="APROBADOS" Width="120px"></asp:Label></td>
                            <td class="linaBajo"  >
                            <asp:TextBox ID="txtBlank12" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo" >
                            <asp:TextBox ID="txtBlank17" runat="server" BackColor="Silver" BorderColor="Silver"
                                BorderStyle="Solid" BorderWidth="2px" Columns="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV281" runat="server" Columns="4" TabIndex="11861" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV282" runat="server" Columns="4" TabIndex="11804" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV283" runat="server" Columns="4" TabIndex="11805" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV284" runat="server" Columns="4" TabIndex="11806" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV285" runat="server" Columns="4" TabIndex="11807" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV286" runat="server" Columns="4" TabIndex="11808" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV287" runat="server" Columns="4" TabIndex="11809" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV288" runat="server" Columns="4" TabIndex="11810" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV289" runat="server" Columns="4" TabIndex="11811" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV290" runat="server" Columns="4" TabIndex="11812" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="linaBajo">
                            <asp:TextBox ID="txtV291" runat="server" Columns="4" TabIndex="11813" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px">
                            </td>
                        </tr>
                    </table>
                
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGD_911_8P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_8P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AG4_911_8P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG4_911_8P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        
       <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
         
</asp:Content>
