<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8P (Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_8P.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8P.AGD_911_8P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 32;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1300px; height:65px;">
    <div id="header">


    <table  style=" padding-left:300px;">
        <tr><td><span>PROFESIONAL T�CNICO MEDIO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1300px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_8P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('AGD_911_8P',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li>
        <li onclick="openPage('AG1_911_8P',false)"><a href="#" title=""><span>1�, 2� y 3�</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>4�, 5� y TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROCEIES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
 
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 1100px">
            <tr>
                <td>
                    <table style="width: 600px">
                        <tr>
                            <td style="text-align: center">
                    <table>
                        <tr>
                            <td colspan="7" style="text-align: left">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba el total de alumnos desglosando la inscripci�n total, la existencia, los aprobados en todas las asignaturas y los reprobados de una a cinco asignaturas, seg�n el sexo."
                        Width="100%"></asp:Label></td>
                            <td colspan="1" style="padding-right: 20px; padding-left: 20px">
                            </td>
                            <td colspan="3" rowspan="" style="width: 270px; text-align: left">
                                <asp:Label ID="lblInstruccion4" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="4. Escriba, por grado, el n�mero de grupos existentes." Width="370px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="7" style="height: 19px; text-align: left">
                            </td>
                            <td colspan="1" style="width: 3px; height: 19px; text-align: left">
                            </td>
                            <td colspan="2" style="width: 240px; text-align: center">
                            </td>
                            <td colspan="1" style="width: 120px; text-align: center">
                            </td>
                        </tr>
            <tr>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEMESTRES"
                        Width="80px"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblSexo" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEXO"
                        Width="80px"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="INSCRIPCI�N TOTAL" Width="100px"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EXISTENCIA"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="APROBADOS EN TODAS LAS ASIGNATURAS"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblReprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="REPROBADOS DE 1 A 5 ASIGNATURAS"
                        Width="100px"></asp:Label></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td colspan="2" rowspan="" style="width: 240px; text-align: center">
                </td>
                <td style="width: 120px; text-align: center">
                    <asp:Label ID="lblGRUPOS" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 Y 2"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV1" runat="server" Columns="5" MaxLength="5" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV2" runat="server" Columns="5" MaxLength="5" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV3" runat="server" Columns="5" MaxLength="5" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV4" runat="server" Columns="5" MaxLength="5" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td colspan="2" style="width: 240px; text-align: center">
                    <asp:Label ID="lblSem1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o. (1o. y 2o. semestres)"
                        Width="180px"></asp:Label></td>
                <td style="width: 120px; text-align: center">
                    <asp:TextBox ID="txtV70" runat="server" Columns="3" MaxLength="2" TabIndex="12301" CssClass="lblNegro" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV5" runat="server" Columns="5" MaxLength="5" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV6" runat="server" Columns="5" MaxLength="5" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV7" runat="server" Columns="5" MaxLength="5" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV8" runat="server" Columns="5" MaxLength="5" TabIndex="10204" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td colspan="2" style="width: 240px; text-align: center">
                    <asp:Label ID="lblSem2y3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o. (3o. y 4o. semestres)"
                        Width="180px"></asp:Label></td>
                <td style="width: 120px; text-align: center">
                    <asp:TextBox ID="txtV71" runat="server" Columns="3" MaxLength="2" TabIndex="12401" CssClass="lblNegro" ></asp:TextBox></td>
            </tr><tr>
                <td rowspan="2" style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 Y 4"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px; height: 26px;">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px; height: 26px;">
                    <asp:TextBox ID="txtV9" runat="server" Columns="5" MaxLength="5" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px; height: 26px;">
                    <asp:TextBox ID="txtV10" runat="server" Columns="5" MaxLength="5" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px; height: 26px;">
                    <asp:TextBox ID="txtV11" runat="server" Columns="5" MaxLength="5" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px; height: 26px;">
                    <asp:TextBox ID="txtV12" runat="server" Columns="5" MaxLength="5" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center; height: 26px;">
                </td>
                <td colspan="2" style="width: 240px; text-align: center; height: 26px;">
                    <asp:Label ID="lblSem4y5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o. (5o. y 6o. semestres)"
                        Width="180px"></asp:Label></td>
                <td style="width: 120px; text-align: center; height: 26px;">
                    <asp:TextBox ID="txtV72" runat="server" Columns="3" MaxLength="2" TabIndex="12501" CssClass="lblNegro" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV13" runat="server" Columns="5" MaxLength="5" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV14" runat="server" Columns="5" MaxLength="5" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV15" runat="server" Columns="5" MaxLength="5" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV16" runat="server" Columns="5" MaxLength="5" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td colspan="2" style="width: 240px; text-align: center">
                    <asp:Label ID="lblSem7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o. (7o. y 8o. semestres)"
                        Width="180px"></asp:Label></td>
                <td style="width: 120px; text-align: center">
                    <asp:TextBox ID="txtV73" runat="server" Columns="3" MaxLength="2" TabIndex="12601" CssClass="lblNegro" ></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 Y 6"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV17" runat="server" Columns="5" MaxLength="5" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV18" runat="server" Columns="5" MaxLength="5" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV19" runat="server" Columns="5" MaxLength="5" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV20" runat="server" Columns="5" MaxLength="5" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td colspan="2" style="width: 240px; text-align: center">
                    <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5o. (9o. y 10o. semestres)"
                        Width="180px"></asp:Label></td>
                <td style="width: 120px; text-align: center">
                    <asp:TextBox ID="txtV74" runat="server" Columns="3" MaxLength="2" TabIndex="12701" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV21" runat="server" Columns="5" MaxLength="5" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV22" runat="server" Columns="5" MaxLength="5" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV23" runat="server" Columns="5" MaxLength="5" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV24" runat="server" Columns="5" MaxLength="5" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td style="text-align: center" colspan="2">
                    <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="180px"></asp:Label></td>
                <td style="width: 120px; text-align: center">
                    <asp:TextBox ID="txtV75" runat="server" Columns="3" MaxLength="3" TabIndex="12801" CssClass="lblNegro" ></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 Y 8"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lbl6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV25" runat="server" Columns="5" MaxLength="5" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV26" runat="server" Columns="5" MaxLength="5" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV27" runat="server" Columns="5" MaxLength="5" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV28" runat="server" Columns="5" MaxLength="5" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td style="width: 120px; text-align: center">
                </td>
                <td style="text-align: left; width: 120px;">
                    </td>
                <td style="vertical-align: top; text-align: left">
                </td>
            </tr>
            <tr>
                <td style="text-align: center; width: 100px;">
                    <asp:Label ID="lblMuj4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV29" runat="server" Columns="5" MaxLength="5" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV30" runat="server" Columns="5" MaxLength="5" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV31" runat="server" Columns="5" MaxLength="5" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV32" runat="server" Columns="5" MaxLength="5" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td colspan="3" rowspan="2" style="width: 3px; text-align: center">
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Las preguntas 5 y 6 �nicamente deber�n ser contestadas por las escuelas del CONALEP."
                        Width="370px"></asp:Label></td>
            </tr>
                        <tr>
                            <td rowspan="2" style="text-align: center; width: 100px;">
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5o."
                                    Width="100%"></asp:Label></td>
                            <td rowspan="2" style="text-align: center; width: 100px;">
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="9 Y 10"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center; width: 100px;">
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center; width: 100px;">
                                <asp:TextBox ID="txtV33" runat="server" Columns="5" MaxLength="5" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 100px;">
                                <asp:TextBox ID="txtV34" runat="server" Columns="5" MaxLength="5" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 100px;">
                                <asp:TextBox ID="txtV35" runat="server" Columns="5" MaxLength="5" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 100px;">
                                <asp:TextBox ID="txtV36" runat="server" Columns="5" MaxLength="5" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 3px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100px; height: 26px;">
                                <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center; width: 100px; height: 26px;">
                                <asp:TextBox ID="txtV37" runat="server" Columns="5" MaxLength="5" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 100px; height: 26px;">
                                <asp:TextBox ID="txtV38" runat="server" Columns="5" MaxLength="5" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 100px; height: 26px;">
                                <asp:TextBox ID="txtV39" runat="server" Columns="5" MaxLength="5" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; width: 100px; height: 26px;">
                                <asp:TextBox ID="txtV40" runat="server" Columns="5" MaxLength="5" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 3px; text-align: center; height: 26px;">
                            </td>
                            <td colspan="3" rowspan="2" style="text-align: left">
                                <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5. De los alumnos inscritos en el PROCEIES, escriba cu�ntos de ellos cursaron y aprobaron todas las asignaturas optativas completamentarias para obtener la equivalencia al bachillerato."
                                    Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td colspan="3" style="text-align: center; width: 100px;">
                    <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV41" runat="server" Columns="5" MaxLength="5" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV42" runat="server" Columns="5" MaxLength="5" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV43" runat="server" Columns="5" MaxLength="5" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 100px;">
                    <asp:TextBox ID="txtV44" runat="server" Columns="5" MaxLength="5" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 3px; text-align: center">
                </td>
            </tr>
                        <tr>
                            <td colspan="3" style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="text-align: center">
                    <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="7" style="height: 19px; text-align: left">
                    <asp:Label ID="lblInstruccion3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. De la existencia total, escriba el n�mero de alumnos de nacionalidad extranjera, deslos�ndolo por sexo."
                        Width="100%"></asp:Label></td>
                            <td colspan="1" style="width: 3px; height: 19px; text-align: left">
                            </td>
                            <td colspan="1" style="text-align: center">
                                <asp:TextBox ID="txtV494" runat="server" Columns="5" MaxLength="5" TabIndex="13001" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="1" style="text-align: center">
                                <asp:TextBox ID="txtV495" runat="server" Columns="5" MaxLength="5" TabIndex="13002" CssClass="lblNegro"></asp:TextBox></td>
                            <td colspan="1" style="text-align: center">
                                <asp:TextBox ID="txtV496" runat="server" Columns="5" MaxLength="5" TabIndex="13003" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: center">
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotal3a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="text-align: left">
                                </td>
                            <td style="text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left; height: 26px;">
                    <asp:Label ID="lblEU" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ESTADOS UNIDOS"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV45" runat="server" Columns="4" MaxLength="4" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                    <asp:TextBox ID="txtV46" runat="server" Columns="4" MaxLength="4" TabIndex="11302" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                    <asp:TextBox ID="txtV47" runat="server" Columns="4" MaxLength="4" TabIndex="11303" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                            </td>
                            <td style="width: 3px; text-align: center; height: 26px;">
                            </td>
                            <td colspan="3" rowspan="3" style="height: 26px; text-align: left">
                                <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6. Escriba el n�mero de alumnos egresados de generaciones anteriores que sursaron y aprobaron en este ciclo escolar todas las asignaturas optativas complementarias para obtener la equivalencia al bachillerato."
                                    Width="370px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 16px; text-align: left">
                    <asp:Label ID="lblCANADA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CANAD�"
                        Width="100%"></asp:Label></td>
                            <td style="height: 16px; text-align: center">
                    <asp:TextBox ID="txtV48" runat="server" Columns="4" MaxLength="4" TabIndex="11401" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="height: 16px; text-align: center">
                    <asp:TextBox ID="txtV49" runat="server" Columns="4" MaxLength="4" TabIndex="11402" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="height: 16px; text-align: center">
                    <asp:TextBox ID="txtV50" runat="server" Columns="4" MaxLength="4" TabIndex="11403" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="height: 16px; text-align: center">
                            </td>
                            <td style="width: 3px; height: 16px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblCAYC" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CENTROAM�RICA Y EL CARIBE"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV51" runat="server" Columns="4" MaxLength="4" TabIndex="11501" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV52" runat="server" Columns="4" MaxLength="4" TabIndex="11502" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV53" runat="server" Columns="4" MaxLength="4" TabIndex="11503" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblSUDAM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUDAM�RICA"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV54" runat="server" Columns="4" MaxLength="4" TabIndex="11601" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV55" runat="server" Columns="4" MaxLength="4" TabIndex="11602" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV56" runat="server" Columns="4" MaxLength="4" TabIndex="11603" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblAFRICA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="�FRICA"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV57" runat="server" Columns="4" MaxLength="4" TabIndex="11701" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV58" runat="server" Columns="4" MaxLength="4" TabIndex="11702" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV59" runat="server" Columns="4" MaxLength="4" TabIndex="11703" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV497" runat="server" Columns="5" MaxLength="5" TabIndex="13101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV498" runat="server" Columns="5" MaxLength="5" TabIndex="13102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV499" runat="server" Columns="5" MaxLength="5" TabIndex="13103" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblASIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ASIA"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV60" runat="server" Columns="4" MaxLength="4" TabIndex="11801" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV61" runat="server" Columns="4" MaxLength="4" TabIndex="11802" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV62" runat="server" Columns="4" MaxLength="4" TabIndex="11803" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblEUROPA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EUROPA"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV63" runat="server" Columns="4" MaxLength="4" TabIndex="11901" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV64" runat="server" Columns="4" MaxLength="4" TabIndex="11902" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV65" runat="server" Columns="4" MaxLength="4" TabIndex="11903" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblOCEANIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OCEAN�A"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV66" runat="server" Columns="4" MaxLength="4" TabIndex="12001" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV67" runat="server" Columns="4" MaxLength="4" TabIndex="12002" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV68" runat="server" Columns="4" MaxLength="4" TabIndex="12003" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                    <asp:Label ID="lblTotal3b" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV69" runat="server" Columns="4" MaxLength="4" TabIndex="12161" CssClass="lblNegro" ></asp:TextBox></td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 3px; text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                        </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('ACG_911_8P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('ACG_911_8P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AG1_911_8P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG1_911_8P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
         
</asp:Content>
