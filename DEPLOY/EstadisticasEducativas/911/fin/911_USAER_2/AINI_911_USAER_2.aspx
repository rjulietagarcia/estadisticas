<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-2(Inicial)" AutoEventWireup="true" CodeBehind="AINI_911_USAER_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_USAER_2.AINI_911_USAER_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js">
    function TABLE1_onclick() {
    }
    </script> 
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 62;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%> 

     <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_2',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_2',true)"><a href="#" title="" class="activo"><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_2',false)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PRIMARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>SECUNDARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL Y RECURSOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <%--<li onclick="openPage('Anexo_911_USAER_2')"><a href="#" title=""><span>ANEXO</span></a></li>--%>
        <li onclick="openPage('Oficializar_911_USAER_2')"><a href="#" title=""><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
       
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 420px">
            <tr>
                <td style="width: 340px">
                    <table id="TABLE1" style="text-align: center; width: 420px;" >
                        <tr>
                            <td colspan="4" rowspan="1" style="text-align: left; width: 400px;">
                                <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Escriba la poblaci�n total con necesidades educativas especiales atendida en educaci�n inicial, por escuela y sexo."
                                    Width="400px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4" rowspan="1" style="text-align: center; width: 400px;">
                                <asp:Label ID="lblInicial" runat="server" CssClass="lblRojo" Text="INICIAL" Width="400px"></asp:Label>
                                <asp:Label ID="lblPoblacion" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL"
                                    Width="400px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center; width: 150px; height: 19px;">
                                </td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="text-align: center; width: 67px;">
                                &nbsp;</td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center; width: 150px;">
                                <asp:Label ID="lblEscuela" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblRojo" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblRojo" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                &nbsp;<asp:Label ID="lblTotal1" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 150px;" colspan="1">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV43" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV44" runat="server" Columns="3" TabIndex="10102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV45" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 150px;" colspan="1">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV46" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV47" runat="server" Columns="3" TabIndex="10202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV48" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 150px;" colspan="1">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV49" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV50" runat="server" Columns="3" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV51" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; width: 150px;" colspan="1">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV52" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV53" runat="server" Columns="3" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV54" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center; width: 150px;">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV55" runat="server" Columns="3" TabIndex="10501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV56" runat="server" Columns="3" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV57" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center; width: 150px;">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV58" runat="server" Columns="3" TabIndex="10601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV59" runat="server" Columns="3" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV60" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center; width: 150px;">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV61" runat="server" Columns="3" TabIndex="10701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV62" runat="server" Columns="3" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV63" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center; width: 150px;">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV64" runat="server" Columns="3" TabIndex="10801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV65" runat="server" Columns="3" TabIndex="10802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV66" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center; width: 150px;">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV67" runat="server" Columns="3" TabIndex="10901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV68" runat="server" Columns="3" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV69" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center; width: 150px;">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV70" runat="server" Columns="3" TabIndex="11001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV71" runat="server" Columns="3" TabIndex="11002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV72" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center; width: 150px;">
                                <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV73" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV74" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV75" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 150px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" rowspan="1" style="width: 400px; text-align: left">
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba, divididos por sexo y escuela, el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                                    Width="400px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 150px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNOS"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" style="text-align: center; width: 200px;">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="200px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 150px; text-align: center">
                            </td>
                            <td style="width: 200px; text-align: center">
                                <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 200px; text-align: center">
                                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 200px; text-align: center">
                                <asp:Label ID="lblTotal3" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV76" runat="server" Columns="3" TabIndex="20101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV77" runat="server" Columns="3" TabIndex="20102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV78" runat="server" Columns="3" TabIndex="20103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV79" runat="server" Columns="3" TabIndex="20201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV80" runat="server" Columns="3" TabIndex="20202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV81" runat="server" Columns="3" TabIndex="20203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV82" runat="server" Columns="3" TabIndex="20301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV83" runat="server" Columns="3" TabIndex="20302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV84" runat="server" Columns="3" TabIndex="20303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV85" runat="server" Columns="3" TabIndex="20401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV86" runat="server" Columns="3" TabIndex="20402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV87" runat="server" Columns="3" TabIndex="20403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV88" runat="server" Columns="3" TabIndex="20501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV89" runat="server" Columns="3" TabIndex="20502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV90" runat="server" Columns="3" TabIndex="20503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV91" runat="server" Columns="3" TabIndex="20601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV92" runat="server" Columns="3" TabIndex="20602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV93" runat="server" Columns="3" TabIndex="20603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV94" runat="server" Columns="3" TabIndex="20701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV95" runat="server" Columns="3" TabIndex="20702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV96" runat="server" Columns="3" TabIndex="20703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV97" runat="server" Columns="3" TabIndex="20801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV98" runat="server" Columns="3" TabIndex="20802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV99" runat="server" Columns="3" TabIndex="20803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: left; width: 150px;">
                                <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Text="TOTAL" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV100" runat="server" Columns="3" TabIndex="20961" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 150px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AE_911_USAER_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AE_911_USAER_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('APRE_911_USAER_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('APRE_911_USAER_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
          
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
        
</asp:Content>
