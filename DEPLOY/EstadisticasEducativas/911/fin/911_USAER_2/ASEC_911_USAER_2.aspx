<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-2(Secundaria)" AutoEventWireup="true" CodeBehind="ASEC_911_USAER_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_USAER_2.ASEC_911_USAER_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js">
    function TABLE1_onclick() {
    }
    </script> 
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 62;
        MaxRow = 22;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%> 

    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">


    <table style =" padding-left:300px;">
        <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_2',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_2',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_2',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_2',true)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_2',true)"><a href="#" title="" class="activo"><span>SECUNDARIA</span></a></li>
        <li onclick="openPage('AGD_911_USAER_2',false)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL Y RECURSOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <%--<li onclick="openPage('Anexo_911_USAER_2')"><a href="#" title=""><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div>    
       <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
         
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table id="TABLE1" style="text-align: center; width: 750px;" >
                        <tr>
                            <td colspan="10" rowspan="1" style="text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="lblInstrucciones5" runat="server" CssClass="lblRojo" Text="5. Escriba la poblaci�n total con necesidades educativas especiales atendida en secundaria, por escuela, grado y sexo."
                                    Width="750px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblSecundaria" runat="server" CssClass="lblRojo" Text="SECUNDARIA" Width="500px"></asp:Label>
                                </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajoAlto" rowspan="3" style="text-align: center">
                                <asp:Label ID="lblEscuela" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td colspan="6" rowspan="1" style="padding-bottom: 10px; text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPoblacion" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL POR GRADO"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" rowspan="1" style="padding-bottom: 10px; text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPoblacionTot" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL (SUMA)"
                                    Width="200px"></asp:Label></td>
                            <td class="Orila" colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="2" class="linaBajo">
                                <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1�" Width="67px"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2�" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="2" class="linaBajo">
                                <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3�" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;" colspan="3" class="linaBajo">
                                &nbsp;</td>
                            <td class="Orila" colspan="1" style="width: 67px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" class="linaBajo">
                                &nbsp;<asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 68px; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombresTot" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeresTot" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblTotal51" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV448" runat="server" Columns="4" TabIndex="10101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV449" runat="server" Columns="4" TabIndex="10102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV450" runat="server" Columns="4" TabIndex="10103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV451" runat="server" Columns="4" TabIndex="10104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV452" runat="server" Columns="4" TabIndex="10105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV453" runat="server" Columns="4" TabIndex="10106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV454" runat="server" Columns="4" TabIndex="10107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV455" runat="server" Columns="4" TabIndex="10108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV456" runat="server" Columns="4" TabIndex="10109" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV457" runat="server" Columns="4" TabIndex="10201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV458" runat="server" Columns="4" TabIndex="10202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV459" runat="server" Columns="4" TabIndex="10203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV460" runat="server" Columns="4" TabIndex="10204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV461" runat="server" Columns="4" TabIndex="10205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV462" runat="server" Columns="4" TabIndex="10206" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV463" runat="server" Columns="4" TabIndex="10207" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV464" runat="server" Columns="4" TabIndex="10208" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV465" runat="server" Columns="4" TabIndex="10209" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="linaBajo">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV466" runat="server" Columns="4" TabIndex="10301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV467" runat="server" Columns="4" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV468" runat="server" Columns="4" TabIndex="10303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV469" runat="server" Columns="4" TabIndex="10304" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV470" runat="server" Columns="4" TabIndex="10305" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV471" runat="server" Columns="4" TabIndex="10306" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV472" runat="server" Columns="4" TabIndex="10307" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV473" runat="server" Columns="4" TabIndex="10308" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV474" runat="server" Columns="4" TabIndex="10309" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;" class="linaBajo">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV475" runat="server" Columns="4" TabIndex="10401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV476" runat="server" Columns="4" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV477" runat="server" Columns="4" TabIndex="10403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV478" runat="server" Columns="4" TabIndex="10404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV479" runat="server" Columns="4" TabIndex="10405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV480" runat="server" Columns="4" TabIndex="10406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV481" runat="server" Columns="4" TabIndex="10407" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV482" runat="server" Columns="4" TabIndex="10408" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV483" runat="server" Columns="4" TabIndex="10409" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV484" runat="server" Columns="4" TabIndex="10501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV485" runat="server" Columns="4" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV486" runat="server" Columns="4" TabIndex="10503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV487" runat="server" Columns="4" TabIndex="10504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV488" runat="server" Columns="4" TabIndex="10505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV489" runat="server" Columns="4" TabIndex="10506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV490" runat="server" Columns="4" TabIndex="10507" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV491" runat="server" Columns="4" TabIndex="10508" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV492" runat="server" Columns="4" TabIndex="10509" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV493" runat="server" Columns="4" TabIndex="10601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV494" runat="server" Columns="4" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV495" runat="server" Columns="4" TabIndex="10603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV496" runat="server" Columns="4" TabIndex="10604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV497" runat="server" Columns="4" TabIndex="10605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV498" runat="server" Columns="4" TabIndex="10606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV499" runat="server" Columns="4" TabIndex="10607" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV500" runat="server" Columns="4" TabIndex="10608" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV501" runat="server" Columns="4" TabIndex="10609" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV502" runat="server" Columns="4" TabIndex="10701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV503" runat="server" Columns="4" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV504" runat="server" Columns="4" TabIndex="10703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV505" runat="server" Columns="4" TabIndex="10704" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV506" runat="server" Columns="4" TabIndex="10705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV507" runat="server" Columns="4" TabIndex="10706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV508" runat="server" Columns="4" TabIndex="10707" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV509" runat="server" Columns="4" TabIndex="10708" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV510" runat="server" Columns="4" TabIndex="10709" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV511" runat="server" Columns="4" TabIndex="10801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV512" runat="server" Columns="4" TabIndex="10802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV513" runat="server" Columns="4" TabIndex="10803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV514" runat="server" Columns="4" TabIndex="10804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV515" runat="server" Columns="4" TabIndex="10805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV516" runat="server" Columns="4" TabIndex="10806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV517" runat="server" Columns="4" TabIndex="10807" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV518" runat="server" Columns="4" TabIndex="10808" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV519" runat="server" Columns="4" TabIndex="10809" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV520" runat="server" Columns="4" TabIndex="10901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV521" runat="server" Columns="4" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV522" runat="server" Columns="4" TabIndex="10903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV523" runat="server" Columns="4" TabIndex="10904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV524" runat="server" Columns="4" TabIndex="10905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV525" runat="server" Columns="4" TabIndex="10906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV526" runat="server" Columns="4" TabIndex="10907" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV527" runat="server" Columns="4" TabIndex="10908" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV528" runat="server" Columns="4" TabIndex="10909" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV529" runat="server" Columns="4" TabIndex="11001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV530" runat="server" Columns="4" TabIndex="11002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV531" runat="server" Columns="4" TabIndex="11003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV532" runat="server" Columns="4" TabIndex="11004" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV533" runat="server" Columns="4" TabIndex="11005" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV534" runat="server" Columns="4" TabIndex="11006" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV535" runat="server" Columns="4" TabIndex="11007" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV536" runat="server" Columns="4" TabIndex="11008" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV537" runat="server" Columns="4" TabIndex="11009" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblTotal52" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV538" runat="server" Columns="4" TabIndex="11101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV539" runat="server" Columns="4" TabIndex="11102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV540" runat="server" Columns="4" TabIndex="11103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 68px;" class="linaBajo">
                                <asp:TextBox ID="txtV541" runat="server" Columns="4" TabIndex="11104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV542" runat="server" Columns="4" TabIndex="11105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV543" runat="server" Columns="4" TabIndex="11106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV544" runat="server" Columns="4" TabIndex="11107" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV545" runat="server" Columns="4" TabIndex="11108" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV546" runat="server" Columns="4" TabIndex="11109" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                    </table>
        </center>
        <center>
            <table id="Table2" >
            <tr>
            <td colspan="6" style="text-align:left;">
            <asp:Label ID="lblNota5" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba, divididos por sexo y escuela, el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                                    Width="720px"></asp:Label>
            </td>
            </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNOS"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" style="text-align: center">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="200px"></asp:Label></td>
                </tr>
                <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 68px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal53" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left; height: 26px;">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV547" runat="server" Columns="4" TabIndex="11301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV548" runat="server" Columns="4" TabIndex="11302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV549" runat="server" Columns="4" TabIndex="11303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV550" runat="server" Columns="4" TabIndex="11401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV551" runat="server" Columns="4" TabIndex="11402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV552" runat="server" Columns="4" TabIndex="11403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left; height: 27px;">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV553" runat="server" Columns="4" TabIndex="11501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV554" runat="server" Columns="4" TabIndex="11502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV555" runat="server" Columns="4" TabIndex="11503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV556" runat="server" Columns="4" TabIndex="11601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV557" runat="server" Columns="4" TabIndex="11602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV558" runat="server" Columns="4" TabIndex="11603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left; height: 26px;">
                                <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV559" runat="server" Columns="4" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV560" runat="server" Columns="4" TabIndex="11702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV561" runat="server" Columns="4" TabIndex="11703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV562" runat="server" Columns="4" TabIndex="11801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV563" runat="server" Columns="4" TabIndex="11802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV564" runat="server" Columns="4" TabIndex="11803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV565" runat="server" Columns="4" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV566" runat="server" Columns="4" TabIndex="11902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV567" runat="server" Columns="4" TabIndex="11903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV568" runat="server" Columns="4" TabIndex="12001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV569" runat="server" Columns="4" TabIndex="12002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV570" runat="server" Columns="4" TabIndex="12003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" rowspan="1" style="text-align: left; height: 28px;">
                                <asp:Label ID="lblTotal54" runat="server" CssClass="lblRojo" Text="TOTAL" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV571" runat="server" Columns="4" TabIndex="12161" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                </tr>
            </table>
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('APRIM_911_USAER_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('APRIM_911_USAER_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AGD_911_USAER_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_USAER_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>
