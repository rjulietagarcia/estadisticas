<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-2(Primaria)" AutoEventWireup="true" CodeBehind="APRIM_911_USAER_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_USAER_2.APRIM_911_USAER_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 62;
        MaxRow = 22;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%> 

    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_2',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_2',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_2',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_2',true)"><a href="#" title="" class="activo"><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_2',false)"><a href="#" title=""><span>SECUNDARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL Y RECURSOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <%--<li onclick="openPage('Anexo_911_USAER_2')"><a href="#" title=""><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
         
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 750px">
            <tr>
                <td>
                    <table id="TABLE1" style="text-align: center; width: 750px;" >
                        <tr>
                            <td colspan="16" rowspan="1" style="text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="4. Escriba la poblaci�n total con necesidades educativas especiales atendida en educaci�n primaria, por escuela, grado y sexo."
                                    Width="750px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPrimaria" runat="server" CssClass="lblRojo" Text="PRIMARIA" Width="500px"></asp:Label>
                                </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajoAlto" rowspan="3" style="text-align: center">
                                <asp:Label ID="lblEscuela" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td colspan="12" rowspan="1" style="padding-bottom: 10px; text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPoblacion" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL POR GRADO"
                                    Width="200px"></asp:Label></td>
                            <td class="linaBajoAlto" colspan="3" rowspan="2" style="width: 67px; text-align: center">
                                <asp:Label ID="lblPoblacionTot" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL (SUMA)"
                                    Width="200px"></asp:Label></td>
                            <td class="Orila" colspan="1" rowspan="2" style="width: 67px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="2" class="linaBajo">
                                <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1�" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="2" class="linaBajo">
                                <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2" class="linaBajo">
                                <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2" class="linaBajo">
                                &nbsp;<asp:Label ID="lbl4" runat="server" CssClass="lblRojo" Text="4�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2" class="linaBajo">
                                <asp:Label ID="lbl5" runat="server" CssClass="lblRojo" Text="5�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="2" class="linaBajo">
                                <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="6�" Width="67px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" class="linaBajo">
                                &nbsp;<asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombres5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 69px;" class="linaBajo">
                                <asp:Label ID="lblMujeres5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombres6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeres6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombresTot" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeresTot" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblTot41" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td class="Orila" style="width: 67px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV258" runat="server" Columns="4" TabIndex="10101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV259" runat="server" Columns="4" TabIndex="10102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV260" runat="server" Columns="4" TabIndex="10103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV261" runat="server" Columns="4" TabIndex="10104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV262" runat="server" Columns="4" TabIndex="10105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV263" runat="server" Columns="4" TabIndex="10106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV264" runat="server" Columns="4" TabIndex="10107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV265" runat="server" Columns="4" TabIndex="10108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV266" runat="server" Columns="4" TabIndex="10109" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV267" runat="server" Columns="4" TabIndex="10110" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV268" runat="server" Columns="4" TabIndex="10111" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV269" runat="server" Columns="4" TabIndex="10112" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV270" runat="server" Columns="4" TabIndex="10113" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV271" runat="server" Columns="4" TabIndex="10114" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV272" runat="server" Columns="4" TabIndex="10115" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV273" runat="server" Columns="4" TabIndex="10201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV274" runat="server" Columns="4" TabIndex="10202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV275" runat="server" Columns="4" TabIndex="10203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV276" runat="server" Columns="4" TabIndex="10204" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV277" runat="server" Columns="4" TabIndex="10205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV278" runat="server" Columns="4" TabIndex="10206" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV279" runat="server" Columns="4" TabIndex="10207" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV280" runat="server" Columns="4" TabIndex="10208" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV281" runat="server" Columns="4" TabIndex="10209" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV282" runat="server" Columns="4" TabIndex="10210" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV283" runat="server" Columns="4" TabIndex="10211" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV284" runat="server" Columns="4" TabIndex="10212" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV285" runat="server" Columns="4" TabIndex="10213" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV286" runat="server" Columns="4" TabIndex="10214" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV287" runat="server" Columns="4" TabIndex="10215" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center; height: 28px;" class="linaBajo">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 28px;" class="linaBajo">
                                <asp:TextBox ID="txtV288" runat="server" Columns="4" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 28px;" class="linaBajo">
                                <asp:TextBox ID="txtV289" runat="server" Columns="4" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 28px;" class="linaBajo">
                                <asp:TextBox ID="txtV290" runat="server" Columns="4" TabIndex="10303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 28px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV291" runat="server" Columns="4" TabIndex="10304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 28px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV292" runat="server" Columns="4" TabIndex="10305" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV293" runat="server" Columns="4" TabIndex="10306" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV294" runat="server" Columns="4" TabIndex="10307" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV295" runat="server" Columns="4" TabIndex="10308" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV296" runat="server" Columns="4" TabIndex="10309" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 28px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV297" runat="server" Columns="4" TabIndex="10310" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV298" runat="server" Columns="4" TabIndex="10311" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV299" runat="server" Columns="4" TabIndex="10312" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV300" runat="server" Columns="4" TabIndex="10313" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV301" runat="server" Columns="4" TabIndex="10314" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV302" runat="server" Columns="4" TabIndex="10315" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 28px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" class="linaBajo">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV303" runat="server" Columns="4" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV304" runat="server" Columns="4" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV305" runat="server" Columns="4" TabIndex="10403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV306" runat="server" Columns="4" TabIndex="10404" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV307" runat="server" Columns="4" TabIndex="10405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV308" runat="server" Columns="4" TabIndex="10406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV309" runat="server" Columns="4" TabIndex="10407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV310" runat="server" Columns="4" TabIndex="10408" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV311" runat="server" Columns="4" TabIndex="10409" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV312" runat="server" Columns="4" TabIndex="10410" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV313" runat="server" Columns="4" TabIndex="10411" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV314" runat="server" Columns="4" TabIndex="10412" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV315" runat="server" Columns="4" TabIndex="10413" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV316" runat="server" Columns="4" TabIndex="10414" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV317" runat="server" Columns="4" TabIndex="10415" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV318" runat="server" Columns="4" TabIndex="10501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV319" runat="server" Columns="4" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV320" runat="server" Columns="4" TabIndex="10503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV321" runat="server" Columns="4" TabIndex="10504" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV322" runat="server" Columns="4" TabIndex="10505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV323" runat="server" Columns="4" TabIndex="10506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV324" runat="server" Columns="4" TabIndex="10507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV325" runat="server" Columns="4" TabIndex="10508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV326" runat="server" Columns="4" TabIndex="10509" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV327" runat="server" Columns="4" TabIndex="10510" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV328" runat="server" Columns="4" TabIndex="10511" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV329" runat="server" Columns="4" TabIndex="10512" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV330" runat="server" Columns="4" TabIndex="10513" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV331" runat="server" Columns="4" TabIndex="10514" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV332" runat="server" Columns="4" TabIndex="10515" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV333" runat="server" Columns="4" TabIndex="10601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV334" runat="server" Columns="4" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV335" runat="server" Columns="4" TabIndex="10603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV336" runat="server" Columns="4" TabIndex="10604" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV337" runat="server" Columns="4" TabIndex="10605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV338" runat="server" Columns="4" TabIndex="10606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV339" runat="server" Columns="4" TabIndex="10607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV340" runat="server" Columns="4" TabIndex="10608" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV341" runat="server" Columns="4" TabIndex="10609" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV342" runat="server" Columns="4" TabIndex="10610" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV343" runat="server" Columns="4" TabIndex="10611" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV344" runat="server" Columns="4" TabIndex="10612" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV345" runat="server" Columns="4" TabIndex="10613" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV346" runat="server" Columns="4" TabIndex="10614" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV347" runat="server" Columns="4" TabIndex="10615" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV348" runat="server" Columns="4" TabIndex="10701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV349" runat="server" Columns="4" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV350" runat="server" Columns="4" TabIndex="10703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV351" runat="server" Columns="4" TabIndex="10704" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV352" runat="server" Columns="4" TabIndex="10705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV353" runat="server" Columns="4" TabIndex="10706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV354" runat="server" Columns="4" TabIndex="10707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV355" runat="server" Columns="4" TabIndex="10708" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV356" runat="server" Columns="4" TabIndex="10709" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV357" runat="server" Columns="4" TabIndex="10710" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV358" runat="server" Columns="4" TabIndex="10711" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV359" runat="server" Columns="4" TabIndex="10712" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV360" runat="server" Columns="4" TabIndex="10713" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV361" runat="server" Columns="4" TabIndex="10714" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV362" runat="server" Columns="4" TabIndex="10715" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV363" runat="server" Columns="4" TabIndex="10801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV364" runat="server" Columns="4" TabIndex="10802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV365" runat="server" Columns="4" TabIndex="10803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV366" runat="server" Columns="4" TabIndex="10804" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV367" runat="server" Columns="4" TabIndex="10805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV368" runat="server" Columns="4" TabIndex="10806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV369" runat="server" Columns="4" TabIndex="10807" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV370" runat="server" Columns="4" TabIndex="10808" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV371" runat="server" Columns="4" TabIndex="10809" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV372" runat="server" Columns="4" TabIndex="10810" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV373" runat="server" Columns="4" TabIndex="10811" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV374" runat="server" Columns="4" TabIndex="10812" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV375" runat="server" Columns="4" TabIndex="10813" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV376" runat="server" Columns="4" TabIndex="10814" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV377" runat="server" Columns="4" TabIndex="10815" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV378" runat="server" Columns="4" TabIndex="10901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV379" runat="server" Columns="4" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV380" runat="server" Columns="4" TabIndex="10903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV381" runat="server" Columns="4" TabIndex="10904" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV382" runat="server" Columns="4" TabIndex="10905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV383" runat="server" Columns="4" TabIndex="10906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV384" runat="server" Columns="4" TabIndex="10907" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV385" runat="server" Columns="4" TabIndex="10908" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV386" runat="server" Columns="4" TabIndex="10909" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV387" runat="server" Columns="4" TabIndex="10910" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV388" runat="server" Columns="4" TabIndex="10911" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV389" runat="server" Columns="4" TabIndex="10912" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV390" runat="server" Columns="4" TabIndex="10913" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV391" runat="server" Columns="4" TabIndex="10914" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV392" runat="server" Columns="4" TabIndex="10915" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV393" runat="server" Columns="4" TabIndex="11001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV394" runat="server" Columns="4" TabIndex="11002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV395" runat="server" Columns="4" TabIndex="11003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV396" runat="server" Columns="4" TabIndex="11004" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV397" runat="server" Columns="4" TabIndex="11005" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV398" runat="server" Columns="4" TabIndex="11006" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV399" runat="server" Columns="4" TabIndex="11007" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV400" runat="server" Columns="4" TabIndex="11008" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV401" runat="server" Columns="4" TabIndex="11009" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV402" runat="server" Columns="4" TabIndex="11010" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV403" runat="server" Columns="4" TabIndex="11011" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV404" runat="server" Columns="4" TabIndex="11012" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV405" runat="server" Columns="4" TabIndex="11013" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV406" runat="server" Columns="4" TabIndex="11014" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV407" runat="server" Columns="4" TabIndex="11015" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 26px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 9px;" class="linaBajo">
                                <asp:Label ID="lblTotal42" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center; height: 9px;" class="linaBajo">
                                <asp:TextBox ID="txtV408" runat="server" Columns="4" TabIndex="11101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; height: 9px;" class="linaBajo">
                                <asp:TextBox ID="txtV409" runat="server" Columns="4" TabIndex="11102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; height: 9px;" class="linaBajo">
                                <asp:TextBox ID="txtV410" runat="server" Columns="4" TabIndex="11103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 9px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV411" runat="server" Columns="4" TabIndex="11104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 9px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV412" runat="server" Columns="4" TabIndex="11105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV413" runat="server" Columns="4" TabIndex="11106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV414" runat="server" Columns="4" TabIndex="11107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV415" runat="server" Columns="4" TabIndex="11108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV416" runat="server" Columns="4" TabIndex="11109" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 9px; text-align: center; width: 69px;" class="linaBajo">
                                <asp:TextBox ID="txtV417" runat="server" Columns="4" TabIndex="11110" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV418" runat="server" Columns="4" TabIndex="11111" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV419" runat="server" Columns="4" TabIndex="11112" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV420" runat="server" Columns="4" TabIndex="11113" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV421" runat="server" Columns="4" TabIndex="11114" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 9px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV422" runat="server" Columns="4" TabIndex="11115" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td class="Orila" style="width: 67px; height: 9px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 69px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        
                    </table>
            <table id="Table2" style="text-align: center; " >
            <tr>
            <td colspan="3" style="text-align:left;">
            <asp:Label ID="lblNota4" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba, divididos por sexo y escuela, el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                                    Width="520px"><br /><br /></asp:Label>
            </td>
            </tr>
                <tr>
                    <td rowspan="1" style="text-align: center">
                                <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNOS"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" style="text-align: center">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="200px"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="1" style="height: 26px; text-align: left">
                    </td>
                    <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombres7" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeres7" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal43" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left; height: 26px;">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV423" runat="server" Columns="4" TabIndex="11301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV424" runat="server" Columns="4" TabIndex="11302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV425" runat="server" Columns="4" TabIndex="11303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left; height: 28px;">
                                <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV426" runat="server" Columns="4" TabIndex="11401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV427" runat="server" Columns="4" TabIndex="11402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 28px; text-align: center">
                                <asp:TextBox ID="txtV428" runat="server" Columns="4" TabIndex="11403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV429" runat="server" Columns="4" TabIndex="11501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV430" runat="server" Columns="4" TabIndex="11502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV431" runat="server" Columns="4" TabIndex="11503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV432" runat="server" Columns="4" TabIndex="11601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV433" runat="server" Columns="4" TabIndex="11602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV434" runat="server" Columns="4" TabIndex="11603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV435" runat="server" Columns="4" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV436" runat="server" Columns="4" TabIndex="11702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV437" runat="server" Columns="4" TabIndex="11703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV438" runat="server" Columns="4" TabIndex="11801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV439" runat="server" Columns="4" TabIndex="11802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV440" runat="server" Columns="4" TabIndex="11803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left">
                                <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV441" runat="server" Columns="4" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV442" runat="server" Columns="4" TabIndex="11902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV443" runat="server" Columns="4" TabIndex="11903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV444" runat="server" Columns="4" TabIndex="12001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV445" runat="server" Columns="4" TabIndex="12002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV446" runat="server" Columns="4" TabIndex="12003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="text-align: left">
                                <asp:Label ID="lblTotal44" runat="server" CssClass="lblRojo" Text="TOTAL" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV447" runat="server" Columns="4" TabIndex="12161" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                </tr>
            </table>
                </td>
            </tr>
        </table>
          </center>
        <center>
            &nbsp;</center>
        <center>
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('APRE_911_USAER_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('APRE_911_USAER_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('ASEC_911_USAER_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('ASEC_911_USAER_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
<%--				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>--%>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
