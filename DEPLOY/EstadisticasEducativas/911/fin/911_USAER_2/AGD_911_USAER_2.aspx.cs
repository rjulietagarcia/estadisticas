using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
     
namespace EstadisticasEducativas._911.fin._911_USAER_2
{
    public partial class AGD_911_USAER_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV572.Attributes["onkeypress"] = "return Num(event)";
                txtV573.Attributes["onkeypress"] = "return Num(event)";
                txtV574.Attributes["onkeypress"] = "return Num(event)";
                txtV575.Attributes["onkeypress"] = "return Num(event)";
                txtV576.Attributes["onkeypress"] = "return Num(event)";
                txtV577.Attributes["onkeypress"] = "return Num(event)";
                txtV578.Attributes["onkeypress"] = "return Num(event)";
                txtV579.Attributes["onkeypress"] = "return Num(event)";
                txtV580.Attributes["onkeypress"] = "return Num(event)";
                txtV581.Attributes["onkeypress"] = "return Num(event)";
                txtV582.Attributes["onkeypress"] = "return Num(event)";
                txtV583.Attributes["onkeypress"] = "return Num(event)";
                txtV584.Attributes["onkeypress"] = "return Num(event)";
                txtV585.Attributes["onkeypress"] = "return Num(event)";
                txtV586.Attributes["onkeypress"] = "return Num(event)";
                txtV587.Attributes["onkeypress"] = "return Num(event)";
                txtV588.Attributes["onkeypress"] = "return Num(event)";
                txtV589.Attributes["onkeypress"] = "return Num(event)";
                txtV590.Attributes["onkeypress"] = "return Num(event)";
                txtV591.Attributes["onkeypress"] = "return Num(event)";
                txtV592.Attributes["onkeypress"] = "return Num(event)";
                txtV593.Attributes["onkeypress"] = "return Num(event)";
                txtV594.Attributes["onkeypress"] = "return Num(event)";
                txtV595.Attributes["onkeypress"] = "return Num(event)";
                txtV596.Attributes["onkeypress"] = "return Num(event)";
                txtV597.Attributes["onkeypress"] = "return Num(event)";
                txtV598.Attributes["onkeypress"] = "return Num(event)";
                txtV599.Attributes["onkeypress"] = "return Num(event)";
                txtV600.Attributes["onkeypress"] = "return Num(event)";
                txtV601.Attributes["onkeypress"] = "return Num(event)";
                txtV602.Attributes["onkeypress"] = "return Num(event)";
                txtV603.Attributes["onkeypress"] = "return Num(event)";
                txtV604.Attributes["onkeypress"] = "return Num(event)";
                txtV605.Attributes["onkeypress"] = "return Num(event)";
                txtV606.Attributes["onkeypress"] = "return Num(event)";
                txtV607.Attributes["onkeypress"] = "return Num(event)";
                txtV608.Attributes["onkeypress"] = "return Num(event)";
                txtV609.Attributes["onkeypress"] = "return Num(event)";
                txtV610.Attributes["onkeypress"] = "return Num(event)";
                txtV611.Attributes["onkeypress"] = "return Num(event)";
                txtV612.Attributes["onkeypress"] = "return Num(event)";
                txtV613.Attributes["onkeypress"] = "return Num(event)";
                txtV614.Attributes["onkeypress"] = "return Num(event)";
                txtV615.Attributes["onkeypress"] = "return Num(event)";
                txtV616.Attributes["onkeypress"] = "return Num(event)";
                txtV617.Attributes["onkeypress"] = "return Num(event)";
                txtV618.Attributes["onkeypress"] = "return Num(event)";
                txtV619.Attributes["onkeypress"] = "return Num(event)";
                txtV620.Attributes["onkeypress"] = "return Num(event)";
                txtV621.Attributes["onkeypress"] = "return Num(event)";
                txtV622.Attributes["onkeypress"] = "return Num(event)";
                txtV623.Attributes["onkeypress"] = "return Num(event)";
                txtV624.Attributes["onkeypress"] = "return Num(event)";
                txtV625.Attributes["onkeypress"] = "return Num(event)";
                txtV626.Attributes["onkeypress"] = "return Num(event)";
                txtV627.Attributes["onkeypress"] = "return Num(event)";
                txtV628.Attributes["onkeypress"] = "return Num(event)";
                txtV629.Attributes["onkeypress"] = "return Num(event)";
                txtV630.Attributes["onkeypress"] = "return Num(event)";
                txtV631.Attributes["onkeypress"] = "return Num(event)";
                txtV632.Attributes["onkeypress"] = "return Num(event)";
                txtV633.Attributes["onkeypress"] = "return Num(event)";
                txtV634.Attributes["onkeypress"] = "return Num(event)";
                txtV635.Attributes["onkeypress"] = "return Num(event)";
                txtV636.Attributes["onkeypress"] = "return Num(event)";
                txtV637.Attributes["onkeypress"] = "return Num(event)";
                txtV638.Attributes["onkeypress"] = "return Num(event)";
                txtV639.Attributes["onkeypress"] = "return Num(event)";
                txtV640.Attributes["onkeypress"] = "return Num(event)";
                txtV641.Attributes["onkeypress"] = "return Num(event)";
                txtV642.Attributes["onkeypress"] = "return Num(event)";
                txtV643.Attributes["onkeypress"] = "return Num(event)";
                txtV644.Attributes["onkeypress"] = "return Num(event)";
                txtV645.Attributes["onkeypress"] = "return Num(event)";
                txtV646.Attributes["onkeypress"] = "return Num(event)";
                txtV647.Attributes["onkeypress"] = "return Num(event)";
                txtV648.Attributes["onkeypress"] = "return Num(event)";
                txtV649.Attributes["onkeypress"] = "return Num(event)";
                txtV650.Attributes["onkeypress"] = "return Num(event)";
                txtV651.Attributes["onkeypress"] = "return Num(event)";
                txtV652.Attributes["onkeypress"] = "return Num(event)";
                txtV653.Attributes["onkeypress"] = "return Num(event)";
                txtV654.Attributes["onkeypress"] = "return Num(event)";
                txtV655.Attributes["onkeypress"] = "return Num(event)";
                txtV656.Attributes["onkeypress"] = "return Num(event)";
                txtV657.Attributes["onkeypress"] = "return Num(event)";
                txtV658.Attributes["onkeypress"] = "return Num(event)";
                txtV659.Attributes["onkeypress"] = "return Num(event)";
                txtV660.Attributes["onkeypress"] = "return Num(event)";
                txtV661.Attributes["onkeypress"] = "return Num(event)";
                txtV662.Attributes["onkeypress"] = "return Num(event)";
                txtV663.Attributes["onkeypress"] = "return Num(event)";
                txtV664.Attributes["onkeypress"] = "return Num(event)";
                txtV665.Attributes["onkeypress"] = "return Num(event)";
                txtV666.Attributes["onkeypress"] = "return Num(event)";
                txtV667.Attributes["onkeypress"] = "return Num(event)";
                txtV668.Attributes["onkeypress"] = "return Num(event)";
                txtV669.Attributes["onkeypress"] = "return Num(event)";
                txtV670.Attributes["onkeypress"] = "return Num(event)";
                txtV671.Attributes["onkeypress"] = "return Num(event)";
                txtV672.Attributes["onkeypress"] = "return Num(event)";
                txtV673.Attributes["onkeypress"] = "return Num(event)";
                txtV674.Attributes["onkeypress"] = "return Num(event)";
                txtV675.Attributes["onkeypress"] = "return Num(event)";
                txtV676.Attributes["onkeypress"] = "return Num(event)";
                txtV677.Attributes["onkeypress"] = "return Num(event)";
                txtV678.Attributes["onkeypress"] = "return Num(event)";
                txtV679.Attributes["onkeypress"] = "return Num(event)";
                txtV680.Attributes["onkeypress"] = "return Num(event)";
                txtV681.Attributes["onkeypress"] = "return Num(event)";
                txtV682.Attributes["onkeypress"] = "return Num(event)";
                txtV683.Attributes["onkeypress"] = "return Num(event)";
                txtV684.Attributes["onkeypress"] = "return Num(event)";
                txtV685.Attributes["onkeypress"] = "return Num(event)";
                txtV686.Attributes["onkeypress"] = "return Num(event)";
                txtV687.Attributes["onkeypress"] = "return Num(event)";
                txtV688.Attributes["onkeypress"] = "return Num(event)";
                txtV689.Attributes["onkeypress"] = "return Num(event)";
                txtV690.Attributes["onkeypress"] = "return Num(event)";
                txtV691.Attributes["onkeypress"] = "return Num(event)";
                txtV692.Attributes["onkeypress"] = "return Num(event)";
                txtV693.Attributes["onkeypress"] = "return Num(event)";
                txtV694.Attributes["onkeypress"] = "return Num(event)";
                txtV695.Attributes["onkeypress"] = "return Num(event)";
                txtV696.Attributes["onkeypress"] = "return Num(event)";
                txtV697.Attributes["onkeypress"] = "return Num(event)";
                txtV698.Attributes["onkeypress"] = "return Num(event)";
                txtV699.Attributes["onkeypress"] = "return Num(event)";
                txtV700.Attributes["onkeypress"] = "return Num(event)";
                txtV701.Attributes["onkeypress"] = "return Num(event)";
                txtV702.Attributes["onkeypress"] = "return Num(event)";
                txtV703.Attributes["onkeypress"] = "return Num(event)";
                txtV704.Attributes["onkeypress"] = "return Num(event)";
                txtV705.Attributes["onkeypress"] = "return Num(event)";
                txtV706.Attributes["onkeypress"] = "return Num(event)";
                txtV707.Attributes["onkeypress"] = "return Num(event)";
                txtV708.Attributes["onkeypress"] = "return Num(event)";
                txtV709.Attributes["onkeypress"] = "return Num(event)";
                txtV710.Attributes["onkeypress"] = "return Num(event)";
                txtV711.Attributes["onkeypress"] = "return Num(event)";
                txtV712.Attributes["onkeypress"] = "return Num(event)";
                txtV713.Attributes["onkeypress"] = "return Num(event)";
                txtV714.Attributes["onkeypress"] = "return Num(event)";
                txtV715.Attributes["onkeypress"] = "return Num(event)";
                txtV716.Attributes["onkeypress"] = "return Num(event)";
                txtV717.Attributes["onkeypress"] = "return Num(event)";
                txtV718.Attributes["onkeypress"] = "return Num(event)";
                txtV719.Attributes["onkeypress"] = "return Num(event)";
                txtV720.Attributes["onkeypress"] = "return Num(event)";
                txtV721.Attributes["onkeypress"] = "return Num(event)";
                txtV722.Attributes["onkeypress"] = "return Num(event)";
                txtV723.Attributes["onkeypress"] = "return Num(event)";
                txtV724.Attributes["onkeypress"] = "return Num(event)";
                txtV725.Attributes["onkeypress"] = "return Num(event)";
                txtV726.Attributes["onkeypress"] = "return Num(event)";
                txtV727.Attributes["onkeypress"] = "return Num(event)";
                txtV728.Attributes["onkeypress"] = "return Num(event)";
                txtV729.Attributes["onkeypress"] = "return Num(event)";
                txtV730.Attributes["onkeypress"] = "return Num(event)";
                txtV731.Attributes["onkeypress"] = "return Num(event)";
                txtV732.Attributes["onkeypress"] = "return Num(event)";
                txtV733.Attributes["onkeypress"] = "return Num(event)";
                txtV734.Attributes["onkeypress"] = "return Num(event)";
                txtV735.Attributes["onkeypress"] = "return Num(event)";
                txtV736.Attributes["onkeypress"] = "return Num(event)";
                txtV737.Attributes["onkeypress"] = "return Num(event)";
                txtV738.Attributes["onkeypress"] = "return Num(event)";
                txtV739.Attributes["onkeypress"] = "return Num(event)";
                txtV740.Attributes["onkeypress"] = "return Num(event)";
                txtV741.Attributes["onkeypress"] = "return Num(event)";
                txtV742.Attributes["onkeypress"] = "return Num(event)";
                txtV743.Attributes["onkeypress"] = "return Num(event)";
                txtV744.Attributes["onkeypress"] = "return Num(event)";
                txtV745.Attributes["onkeypress"] = "return Num(event)";
                txtV746.Attributes["onkeypress"] = "return Num(event)";
                txtV747.Attributes["onkeypress"] = "return Num(event)";
                txtV748.Attributes["onkeypress"] = "return Num(event)";
                txtV749.Attributes["onkeypress"] = "return Num(event)";
                txtV750.Attributes["onkeypress"] = "return Num(event)";
                txtV751.Attributes["onkeypress"] = "return Num(event)";
                txtV752.Attributes["onkeypress"] = "return Num(event)";
                txtV753.Attributes["onkeypress"] = "return Num(event)";
                txtV754.Attributes["onkeypress"] = "return Num(event)";
                txtV755.Attributes["onkeypress"] = "return Num(event)";
                txtV756.Attributes["onkeypress"] = "return Num(event)";
                txtV757.Attributes["onkeypress"] = "return Num(event)";
                txtV758.Attributes["onkeypress"] = "return Num(event)";
                txtV759.Attributes["onkeypress"] = "return Num(event)";
                txtV760.Attributes["onkeypress"] = "return Num(event)";
                txtV761.Attributes["onkeypress"] = "return Num(event)";
                txtV762.Attributes["onkeypress"] = "return Num(event)";
                txtV763.Attributes["onkeypress"] = "return Num(event)";
                txtV764.Attributes["onkeypress"] = "return Num(event)";
                txtV765.Attributes["onkeypress"] = "return Num(event)";
                txtV766.Attributes["onkeypress"] = "return Num(event)";
                txtV767.Attributes["onkeypress"] = "return Num(event)";
                txtV768.Attributes["onkeypress"] = "return Num(event)";
                txtV769.Attributes["onkeypress"] = "return Num(event)";
                txtV770.Attributes["onkeypress"] = "return Num(event)";
                txtV771.Attributes["onkeypress"] = "return Num(event)";
                txtV772.Attributes["onkeypress"] = "return Num(event)";
                txtV773.Attributes["onkeypress"] = "return Num(event)";
                txtV774.Attributes["onkeypress"] = "return Num(event)";
                txtV775.Attributes["onkeypress"] = "return Num(event)";
                txtV776.Attributes["onkeypress"] = "return Num(event)";
                txtV777.Attributes["onkeypress"] = "return Num(event)";
                txtV778.Attributes["onkeypress"] = "return Num(event)";
                txtV779.Attributes["onkeypress"] = "return Num(event)";
                txtV780.Attributes["onkeypress"] = "return Num(event)";
                txtV781.Attributes["onkeypress"] = "return Num(event)";
                txtV782.Attributes["onkeypress"] = "return Num(event)";
                txtV783.Attributes["onkeypress"] = "return Num(event)";
                txtV784.Attributes["onkeypress"] = "return Num(event)";
                txtV785.Attributes["onkeypress"] = "return Num(event)";
                txtV786.Attributes["onkeypress"] = "return Num(event)";
                txtV787.Attributes["onkeypress"] = "return Num(event)";
                txtV788.Attributes["onkeypress"] = "return Num(event)";
                txtV789.Attributes["onkeypress"] = "return Num(event)";
                txtV790.Attributes["onkeypress"] = "return Num(event)";
                txtV791.Attributes["onkeypress"] = "return Num(event)";
                txtV792.Attributes["onkeypress"] = "return Num(event)";
                txtV793.Attributes["onkeypress"] = "return Num(event)";
                txtV794.Attributes["onkeypress"] = "return Num(event)";
                txtV795.Attributes["onkeypress"] = "return Num(event)";
                txtV796.Attributes["onkeypress"] = "return Num(event)";
                txtV797.Attributes["onkeypress"] = "return Num(event)";
                txtV798.Attributes["onkeypress"] = "return Num(event)";
                txtV799.Attributes["onkeypress"] = "return Num(event)";
                txtV800.Attributes["onkeypress"] = "return Num(event)";
                txtV801.Attributes["onkeypress"] = "return Num(event)";
                txtV802.Attributes["onkeypress"] = "return Num(event)";
                txtV803.Attributes["onkeypress"] = "return Num(event)";
                txtV804.Attributes["onkeypress"] = "return Num(event)";
                txtV805.Attributes["onkeypress"] = "return Num(event)";
                txtV806.Attributes["onkeypress"] = "return Num(event)";
                txtV807.Attributes["onkeypress"] = "return Num(event)";
                txtV808.Attributes["onkeypress"] = "return Num(event)";
                txtV809.Attributes["onkeypress"] = "return Num(event)";
                txtV810.Attributes["onkeypress"] = "return Num(event)";
                txtV811.Attributes["onkeypress"] = "return Num(event)";
                txtV812.Attributes["onkeypress"] = "return Num(event)";
                txtV813.Attributes["onkeypress"] = "return Num(event)";
                txtV814.Attributes["onkeypress"] = "return Num(event)";
                txtV815.Attributes["onkeypress"] = "return Num(event)";
                txtV816.Attributes["onkeypress"] = "return Num(event)";
                txtV817.Attributes["onkeypress"] = "return Num(event)";
                txtV818.Attributes["onkeypress"] = "return Num(event)";
                txtV819.Attributes["onkeypress"] = "return Num(event)";
                txtV820.Attributes["onkeypress"] = "return Num(event)";
                txtV821.Attributes["onkeypress"] = "return Num(event)";
                txtV822.Attributes["onkeypress"] = "return Num(event)";
                txtV823.Attributes["onkeypress"] = "return Num(event)";
                txtV824.Attributes["onkeypress"] = "return Num(event)";
                txtV825.Attributes["onkeypress"] = "return Num(event)";
                txtV826.Attributes["onkeypress"] = "return Num(event)";
                txtV827.Attributes["onkeypress"] = "return Num(event)";
                txtV828.Attributes["onkeypress"] = "return Num(event)";
                txtV829.Attributes["onkeypress"] = "return Num(event)";
                txtV830.Attributes["onkeypress"] = "return Num(event)";
                txtV831.Attributes["onkeypress"] = "return Num(event)";
                txtV832.Attributes["onkeypress"] = "return Num(event)";
                txtV833.Attributes["onkeypress"] = "return Num(event)";
                txtV834.Attributes["onkeypress"] = "return Num(event)";
                txtV835.Attributes["onkeypress"] = "return Num(event)";
                txtV836.Attributes["onkeypress"] = "return Num(event)";
                txtV837.Attributes["onkeypress"] = "return Num(event)";
                txtV838.Attributes["onkeypress"] = "return Num(event)";
                txtV839.Attributes["onkeypress"] = "return Num(event)";
                txtV840.Attributes["onkeypress"] = "return Num(event)";
                txtV841.Attributes["onkeypress"] = "return Num(event)";
                txtV842.Attributes["onkeypress"] = "return Num(event)";
                txtV843.Attributes["onkeypress"] = "return Num(event)";
                txtV844.Attributes["onkeypress"] = "return Num(event)";
                txtV845.Attributes["onkeypress"] = "return Num(event)";
                txtV846.Attributes["onkeypress"] = "return Num(event)";
                txtV847.Attributes["onkeypress"] = "return Num(event)";
                txtV848.Attributes["onkeypress"] = "return Num(event)";
                txtV849.Attributes["onkeypress"] = "return Num(event)";
                txtV850.Attributes["onkeypress"] = "return Num(event)";
                txtV851.Attributes["onkeypress"] = "return Num(event)";
                txtV852.Attributes["onkeypress"] = "return Num(event)";
                txtV853.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************


    }
}
