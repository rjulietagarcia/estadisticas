using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using SEroot.WsInmuebles;
using SEroot.WsEstadisticasEducativas;
using SEroot.WsCentrosDeTrabajo;
using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
 
namespace EstadisticasEducativas._911.fin._911_USAER_2
{
    public partial class Identificacion_911_USAER_2 : System.Web.UI.Page
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                usr = SeguridadSE.GetUsuario(HttpContext.Current);
                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;

                #region Carga Parametros Y Hidden's
                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;
                #endregion

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                //-------Ingresador para el control del Popup
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                if (controlDP != null)
                {
                    LlenarMotivos();
                    EstadoBotones(controlDP);
                }
                //-------Fin de Nuevo Codigo
                if (controlDP.Estatus == 0)
                {
                    Label lbl = new Label();
                    lbl.Text =  Class911.CargaInicialCuestionario(controlDP, 1);
                    pnlFallas.Controls.Add(lbl);

                    pnlOficializado.Visible = false;
                }
                else
                    pnlOficializado.Visible = true;

                CargaDatos((int)cctSeleccionado.CctntId, (int)cctSeleccionado.InmuebleId);
            }
        }

        private void CargaDatos(int id_CT, int id_Inm)
        {
            string claveCCT = "";


            if (id_Inm > 0)
            {
                #region Inmuebles
                Service_Inmuebles wsin = new Service_Inmuebles();
                SEroot.WsInmuebles.DatosIdentificacionInDP dp = wsin.Load_Inmueble(id_Inm);

                txtClaveInmueble.Text = dp.ClaveInmueble;
                txtMunicipio.Text = dp.Municipio;
                txtLocalidad.Text = dp.Localidad;
                txtColonia.Text = dp.Colonia;
                txtCalle.Text = dp.Calle;
                txtEntreCalle.Text = dp.EntreCalle;
                txtYCalle.Text = dp.YCalle;
                txtCPosterior.Text = dp.CallePosterior;
                txtNumeroExterior.Text = dp.Numero.ToString();
                txtNumeroInterior.Text = dp.NumeroInterior;
                txtCodigoPostal.Text = dp.CodigoPostal;

                #endregion
            }



            if (id_CT > 0)
            {
                #region CT
                DatosIdentificacionCTDP CtDp = new DatosIdentificacionCTDP();
                Service_CentrosDeTrabajo wsct = new Service_CentrosDeTrabajo();
                CtDp = wsct.GetDatosIdentificacion(id_CT);

                txtTipoCT.ReadOnly = true;
                txtClasificador.ReadOnly = true;
                txtIdentificador.ReadOnly = true;
                txtDepNormativa.ReadOnly = true;
                txtDepOperativa.ReadOnly = true;
                txtServicio.ReadOnly = true;
                txtSostenimiento.ReadOnly = true;

                txtCCTEnt.Text = CtDp.Clavect.Substring(0, 2);
                txtCCTIden.Text = CtDp.Clavect.Substring(2, 3);
                txtCCTCons.Text = CtDp.Clavect.Substring(5, 4);
                txtCCTDV.Text = CtDp.Clavect.Substring(9, 1);
                claveCCT = txtCCTEnt.Text + txtCCTIden.Text + txtCCTCons.Text + txtCCTDV.Text;

                txtFechaFundacion.Text = CtDp.FechaFundacion;
                txtFechaAlta.Text = CtDp.FechaAlta;
                txtTurno.Text = CtDp.Turno;
                txtTurnoD.Text = CtDp.DTurno;
                txtNombreCT.Text = CtDp.NombreCT;

                txtRegion.Text = CtDp.Region.ToString();
                txtZona.Text = CtDp.Zona.ToString();
                txtTipoCT.Text = CtDp.TipoCT;
                txtClasificador.Text = CtDp.Clasificador;
                txtIdentificador.Text = CtDp.Identificador;

                txtDepNormativa.Text = CtDp.DepNormativa;
                txtDepOperativa.Text = CtDp.DepOperativa;
                txtServicio.Text = CtDp.Servicio;
                txtSostenimiento.Text = CtDp.Sostenimiento;
                txtTipoCTD.Text = CtDp.DTipoCT;

                txtClasificadorD.Text = CtDp.DClasificador;
                txtIdentificadorD.Text = CtDp.DIdentificador;
                txtDepNormativaD.Text = CtDp.DDepNormativa;
                txtDepOperativaD.Text = CtDp.DDepOperativa;
                txtServicioD.Text = CtDp.DServicio;

                txtSostenimientoD.Text = CtDp.DSostenimiento;
                tels.InnerHtml = CtDp.TelefonosTable;
                txtDirector.Text = CtDp.Director;

                #endregion
            }

        }
        //Agregados para la Funcionalidad de oficializar con motivo
        protected void cmdOficializar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);

            string[] usr = User.Identity.Name.Split('|');

            string resValidaCuestionario = "";
            string resValidaAnexo = "";
            if (ddlMotivos.SelectedValue != "0")
                controlDP.Estatus = int.Parse(ddlMotivos.SelectedValue);
            else
            {
                //CargarTodoYGuardar(controlDP);


                if (true)
                {

                    resValidaCuestionario = "";//ValidarCuestionario(controlDP);
                    resValidaAnexo = "";// ValidarAnexo(controlDP);
                }

            }
            if ((resValidaCuestionario == "" && resValidaAnexo == "" && ddlMotivos.SelectedValue == "0") || ddlMotivos.SelectedValue != "0")
            {
                if (ddlMotivos.SelectedValue == "0")
                    controlDP.Estatus = 10;

                wsEstadisiticas.Oficializar_Cuestionario(controlDP, int.Parse(usr[0]));

                EstadoBotones(controlDP);
                pnlOficializado.Visible = true;
            }

        }
        private void LlenarMotivos()
        {
            
            ddlMotivos.Items.Add(new ListItem("1.- La escuela est� en proceso de clausura", "1"));
            ddlMotivos.Items.Add(new ListItem("2.- Falta de personal Docente", "2"));
            ddlMotivos.Items.Add(new ListItem("3.- Falta de alumnos", "3"));
            ddlMotivos.Items.Add(new ListItem("4.- Incumplimiento del Director", "4"));
            ddlMotivos.Items.Add(new ListItem("5.- Escuelas de nueva creaci�n", "5"));
            ddlMotivos.Items.Add(new ListItem("6.- Causa administrativa", "6"));
            ddlMotivos.Items.Add(new ListItem("7.- No corresponde la fecha de levantamiento con el inicio de cursos de la escuela", "7"));
            ddlMotivos.Items.Add(new ListItem("8.- Compactaci�n de turno", "8"));
            ddlMotivos.Items.Add(new ListItem("9.- Cambio de Turno", "9"));
            //10.- Cerrado
        }
        private void EstadoBotones(ControlDP controlDP)
        {
            if (controlDP.Estatus == 0)
            {
                cmdOficializar.Enabled = true;
                cmdGenerarComprobante.Enabled = false;
                cmdImprimircuestionario.Enabled = true;

                ddlMotivos.Enabled = true;
            }
            else
            {
                cmdOficializar.Enabled = false;
                ddlMotivos.Enabled = false;
                cmdGenerarComprobante.Enabled = true;
                cmdImprimircuestionario.Enabled = true; // ??

                if (controlDP.Estatus != 10)
                    ddlMotivos.SelectedValue = controlDP.Estatus.ToString();
            }
        }
        protected void cmdGenerarComprobante_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarComprobante_PDF(int.Parse(hidIdCtrl.Value));
        }
        protected void cmdImprimircuestionario_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarCuestionario_Bacio_PDF(int.Parse(hidIdCtrl.Value));
        }

       

    }
}