<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-2(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_USAER_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_USAER_2.Personal_911_USAER_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js">
    </script> 
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 10;
        MaxRow = 39;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%> 

    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">


    <table style =" padding-left:300px;">
        <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="OpenPageCharged('Identificacion_911_USAER_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="OpenPageCharged('AE_911_USAER_2',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="OpenPageCharged('AINI_911_USAER_2',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="OpenPageCharged('APRE_911_USAER_2',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="OpenPageCharged('APRIM_911_USAER_2',true)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="OpenPageCharged('ASEC_911_USAER_2',true)"><a href="#" title=""><span>SECUNDARIA</span></a></li>
        <li onclick="OpenPageCharged('AGD_911_USAER_2',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="OpenPageCharged('Personal_911_USAER_2',true)"><a href="#" title="" class="activo"><span>PERSONAL Y RECURSOS</span></a></li>
        <li onclick="OpenPageCharged('Inmueble_911_USAER_2',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div>    
    <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
         

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

         
         <table>
         <tr>
            <td style="text-align: left">
        
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblRojo" Font-Size="16px" Text="II. PERSONAL POR FUNCI�N"></asp:Label>
            </td>
         </tr>
         <tr>
            <td style="text-align: left">
        
        
                                <asp:Label ID="lblDirector" runat="server" CssClass="lblRojo" Font-Size="14px" Text="DIRECTOR"></asp:Label></td>
         </tr>
         <tr>
            <td style="height: 192px; text-align: left;">
            <table  >
              <tr>
              <td class="linaBajoAlto">        <table  style="text-align: center;" >
                <tr>
                            <td colspan="3"  style="text-align: left; vertical-align: top;">
                                <asp:Label ID="lblDir1" runat="server" CssClass="lblRojo" Text="1. Seleccione la situaci�n del director."
                                    Width="270px"></asp:Label></td>
                </tr>
                <tr>
                            <td >
                                <asp:Label ID="lblDirCGpo" runat="server" CssClass="lblGrisTit" Text="CON GRUPO" Width="90px"></asp:Label></td>
                            <td  >
                            </td>
                            <td >
                                <asp:Label ID="lblDirSGpo" runat="server" CssClass="lblGrisTit" Text="SIN GRUPO" Width="90px"></asp:Label></td>
                </tr>
                <tr>
                            <td >
                                <asp:RadioButton ID="optV854" runat="server" GroupName="ConSinGpo" 
                                    Text=" " />
                                <asp:TextBox ID="txtV854" runat="server" Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                            <td  >
                            </td>
                            <td >
                                <asp:RadioButton ID="optV855" runat="server" GroupName="ConSinGpo" 
                                    Text=" " />
                                <asp:TextBox ID="txtV855" runat="server" Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3" >
                                <asp:Label ID="lblDir3" runat="server" CssClass="lblRojo" Text="3. Indique el sexo del director."
                                    Width="270px"></asp:Label></td>
                </tr>
                <tr>
                            <td >
                                <asp:Label ID="lblDirHombre" runat="server" CssClass="lblGrisTit" Text="HOMBRE"></asp:Label></td>
                            <td  >
                            </td>
                            <td >
                                <asp:Label ID="lblDirMujer" runat="server" CssClass="lblGrisTit" Text="MUJER"></asp:Label></td>
                </tr>
                <tr>
                            <td >
                                <asp:RadioButton ID="optV860" runat="server" GroupName="SexoDir"  Text=" " />
                                <asp:TextBox ID="txtV860" runat="server" Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                            <td  >
                            </td>
                            <td >
                                <asp:RadioButton ID="optV861" runat="server" GroupName="SexoDir"  Text=" " />
                                <asp:TextBox ID="txtV861" runat="server" Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                </tr>
            </table>
            </td>
              <td class="linaBajoAlto" style="width: 443px; text-align: right">
                 <table  style="text-align: center;" >
                <tr>
                            <td colspan="4"  style="width: 270px;
                                text-align: left">
                                <asp:Label ID="lblDir2" runat="server" CssClass="lblRojo" Text="2. Escriba la clave y el nombre del nivel m�ximo de estudios y de su especialidad del director."
                                    Width="350px"></asp:Label></td>
                </tr>
                <tr>
                            <td >
                            </td>
                            <td colspan="3" >
                            </td>
                </tr>
                <tr>
                            <td style="text-align: left" >
                                <asp:Label ID="lblNivelDir2" runat="server" CssClass="lblGrisTit" Text="NIVEL"></asp:Label></td>
                            <td colspan="3" style="text-align: left" >
                                <asp:TextBox ID="txtV856" runat="server" Width="26px" Enabled="false" Columns="2" CssClass="lblNegro" MaxLength="2" ></asp:TextBox>
                                <asp:TextBox ID="txtV857" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox>
                                <asp:DropDownList ID="cboV857"  runat="server"   CssClass="lblNegro">
                                </asp:DropDownList></td>
                </tr>
                <tr>
                            <td style="text-align: left" >
                                <asp:Label ID="lblEspecialidadDir2" runat="server" CssClass="lblGrisTit" Text="ESPECIALIDAD" Width="90px"></asp:Label></td>
                            <td colspan="3" style="text-align: left" >
                                <asp:TextBox ID="txtV858" runat="server" Width="26px" Enabled = "false" ></asp:TextBox>
                                <asp:TextBox ID="txtV859" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox>
                                <asp:DropDownList ID="cboV859" runat="server"   CssClass="lblNegro">
                                
                                </asp:DropDownList></td>
                </tr>
                <tr>
                            <td >
                                <asp:Label ID="lblOtroDir2" runat="server" CssClass="lblGrisTit" Text="OTRO"></asp:Label></td>
                            <td colspan="3" >
                                <asp:TextBox ID="txtV1073" runat="server" Columns="30"  CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                </tr>
            </table>
              </td>
                  <td class="Orila">
                      &nbsp;</td>
              </tr>
                <tr>
                    <td class="linaBajo" colspan="2">
                <table  style="text-align: center;" >
                <tr>
                            <td colspan="2">
                                <asp:Label ID="lblDir4" runat="server" CssClass="lblRojo" Text="4. Seleccione la situaci�n acad�mica del director."></asp:Label></td>
                </tr>
                <tr>
                            <td style="text-align: right"  >
                                <asp:Label ID="lblPosgradoDir4" runat="server" CssClass="lblGrisTit" Text="POSGRADO" ></asp:Label></td>
                            <td >
                                <asp:RadioButton ID="optV862" runat="server" GroupName="SituacionAcadDir" 
                                    Text=" " />
                                <asp:TextBox ID="txtV862" runat="server" Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                </tr>
                <tr>
                            <td style="text-align: right"  >
                                <asp:Label ID="lblLicTitDir4" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO" ></asp:Label></td>
                            <td >
                                <asp:RadioButton ID="optV863" runat="server" GroupName="SituacionAcadDir" 
                                    Text=" " />
                                <asp:TextBox ID="txtV863" runat="server" Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                </tr>
                <tr>
                            <td style="text-align: right"  > 
                                <asp:Label ID="lblLicNoTitDir4" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, NO TITULADO"
                                    ></asp:Label></td>
                            <td >
                                <asp:RadioButton ID="optV864" runat="server" GroupName="SituacionAcadDir" 
                                    Text=" " />
                                <asp:TextBox ID="txtV864" runat="server" Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                </tr>
                <tr>
                            <td style="text-align: right"  >
                                <asp:Label ID="lblLicEstudDir4" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, ESTUDIANTE"
                                    ></asp:Label></td>
                            <td >
                                <asp:RadioButton ID="optV865" runat="server" GroupName="SituacionAcadDir" 
                                    Text=" " />
                                <asp:TextBox ID="txtV865" runat="server" Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                </tr>
            </table>
                    </td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
            </table>
                <br />
            </td>
         </tr>
         <tr>
            <td style="text-align: left">
    
        
        
         
        
        
          
        
                                <asp:Label ID="lblMtrosApoyo" runat="server" CssClass="lblRojo" Font-Size="14px" Text="MAESTROS DE APOYO"
                                   ></asp:Label></td>
         </tr>
         <tr>
            <td style="text-align: left">
        
                                <asp:Label ID="lblInstrucciones5" runat="server" CssClass="lblRojo" Text="5. Escriba la cantidad de maestros de apoyo, por formaci�n, seg�n su situaci�n acad�mica y sexo."></asp:Label></td>
         </tr>
         <tr>
            <td style="text-align: left">
        
            <table  style="text-align: center;" >
            <tr>
                <td rowspan="3" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblFormacion" runat="server" CssClass="lblRojo" Text="FORMACI�N" ></asp:Label></td>
                <td colspan="6" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblLicenciatura5" runat="server" CssClass="lblRojo" Text="LICENCIATURA"></asp:Label></td>
                <td colspan="2" rowspan="2" style=" text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblTotal51" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                <td class="Orila" colspan="1" rowspan="2" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="linaBajo">
                                <asp:Label ID="lblTitulados5" runat="server" CssClass="lblRojo" Text="TITULADOS" ></asp:Label></td>
                <td colspan="2" class="linaBajo">
                                <asp:Label ID="lblNoTitulados5" runat="server" CssClass="lblRojo" Text="NO TITULADOS" ></asp:Label></td>
                <td colspan="2" class="linaBajo">
                                <asp:Label ID="lblEstudiantes5" runat="server" CssClass="lblRojo" Text="ESTUDIANTES" ></asp:Label></td>
            </tr>
            <tr>
                            <td class="linaBajo">
                                <asp:Label ID="lblHombresTit5" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblMujeresTit5" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                            <td class="linaBajo">
                                &nbsp;<asp:Label ID="lblHombresNTit5" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblMujeresNTit5" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblHombresEst5" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblMujeresEst5" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblHombresTot5" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td colspan="" style=" text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeresTot5" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                <td class="Orila" colspan="1" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left;" class="linaBajo">
                                <asp:Label ID="lblDefMental" runat="server" CssClass="lblGrisTit" Text="EN DEFICIENCIA MENTAL" ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV866" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV867" runat="server" Columns="3" TabIndex="10102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV868" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV869" runat="server" Columns="3" TabIndex="10104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV870" runat="server" Columns="3" TabIndex="10105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV871" runat="server" Columns="3" TabIndex="10106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV872" runat="server" Columns="3" TabIndex="10107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV873" runat="server" Columns="3" TabIndex="10108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr >
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblAudYLeng" runat="server" CssClass="lblGrisTit" Text="EN AUDICI�N Y LENGUAJE"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV874" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV875" runat="server" Columns="3" TabIndex="10202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV876" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV877" runat="server" Columns="3" TabIndex="10204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV878" runat="server" Columns="3" TabIndex="10205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV879" runat="server" Columns="3" TabIndex="10206" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV880" runat="server" Columns="3" TabIndex="10207" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV881" runat="server" Columns="3" TabIndex="10208" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblAprendizaje" runat="server" CssClass="lblGrisTit" Text="EN APRENDIZAJE"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV882" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV883" runat="server" Columns="3" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV884" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV885" runat="server" Columns="3" TabIndex="10304" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV886" runat="server" Columns="3" TabIndex="10305" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV887" runat="server" Columns="3" TabIndex="10306" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV888" runat="server" Columns="3" TabIndex="10307" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV889" runat="server" Columns="3" TabIndex="10308" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblDefVisual" runat="server" CssClass="lblGrisTit" Text="EN DEFICIENCIA VISUAL"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV890" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV891" runat="server" Columns="3" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV892" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV893" runat="server" Columns="3" TabIndex="10404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV894" runat="server" Columns="3" TabIndex="10405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV895" runat="server" Columns="3" TabIndex="10406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV896" runat="server" Columns="3" TabIndex="10407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV897" runat="server" Columns="3" TabIndex="10408" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblLocomotor" runat="server" CssClass="lblGrisTit" Text="EN APARATO LOCOMOTOR"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV898" runat="server" Columns="3" TabIndex="10501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV899" runat="server" Columns="3" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV900" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV901" runat="server" Columns="3" TabIndex="10504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV902" runat="server" Columns="3" TabIndex="10505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV903" runat="server" Columns="3" TabIndex="10506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV904" runat="server" Columns="3" TabIndex="10507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV905" runat="server" Columns="3" TabIndex="10508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblInadSociales" runat="server" CssClass="lblGrisTit" Text="EN INADAPTADOS SOCIALES"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV906" runat="server" Columns="3" TabIndex="10601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV907" runat="server" Columns="3" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV908" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV909" runat="server" Columns="3" TabIndex="10604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV910" runat="server" Columns="3" TabIndex="10605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV911" runat="server" Columns="3" TabIndex="10606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV912" runat="server" Columns="3" TabIndex="10607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV913" runat="server" Columns="3" TabIndex="10608" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblPedagogo" runat="server" CssClass="lblGrisTit" Text="PEDAGOGO" ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV914" runat="server" Columns="3" TabIndex="10701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV915" runat="server" Columns="3" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV916" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV917" runat="server" Columns="3" TabIndex="10704" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV918" runat="server" Columns="3" TabIndex="10705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV919" runat="server" Columns="3" TabIndex="10706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV920" runat="server" Columns="3" TabIndex="10707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV921" runat="server" Columns="3" TabIndex="10708" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblPsicologo" runat="server" CssClass="lblGrisTit" Text="PSIC�LOGO" ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV922" runat="server" Columns="3" TabIndex="10801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV923" runat="server" Columns="3" TabIndex="10802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV924" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV925" runat="server" Columns="3" TabIndex="10804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV926" runat="server" Columns="3" TabIndex="10805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV927" runat="server" Columns="3" TabIndex="10806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV928" runat="server" Columns="3" TabIndex="10807" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV929" runat="server" Columns="3" TabIndex="10808" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblMtroPree" runat="server" CssClass="lblGrisTit" Text="MAESTRO DE PREESCOLAR"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV930" runat="server" Columns="3" TabIndex="10901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV931" runat="server" Columns="3" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV932" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV933" runat="server" Columns="3" TabIndex="10904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV934" runat="server" Columns="3" TabIndex="10905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV935" runat="server" Columns="3" TabIndex="10906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV936" runat="server" Columns="3" TabIndex="10907" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV937" runat="server" Columns="3" TabIndex="10908" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblMtroPrim" runat="server" CssClass="lblGrisTit" Text="MAESTRO DE PRIMARIA"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV938" runat="server" Columns="3" TabIndex="11001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV939" runat="server" Columns="3" TabIndex="11002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV940" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV941" runat="server" Columns="3" TabIndex="11004" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV942" runat="server" Columns="3" TabIndex="11005" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV943" runat="server" Columns="3" TabIndex="11006" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV944" runat="server" Columns="3" TabIndex="11007" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV945" runat="server" Columns="3" TabIndex="11008" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblMtroSec" runat="server" CssClass="lblGrisTit" Text="MAESTRO DE SECUNDARIA"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV946" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV947" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV948" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV949" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV950" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV951" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV952" runat="server" Columns="3" TabIndex="11107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV953" runat="server" Columns="3" TabIndex="11108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblIntEducativa" runat="server" CssClass="lblGrisTit" Text="EN INTEGRACI�N EDUCATIVA"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV954" runat="server" Columns="3" TabIndex="11201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV955" runat="server" Columns="3" TabIndex="11202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV956" runat="server" Columns="3" TabIndex="11203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV957" runat="server" Columns="3" TabIndex="11204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV958" runat="server" Columns="3" TabIndex="11205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV959" runat="server" Columns="3" TabIndex="11206" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV960" runat="server" Columns="3" TabIndex="11207" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV961" runat="server" Columns="3" TabIndex="11208" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td colspan="9" class="Orila">
                                <asp:Label ID="lblNota5" runat="server" CssClass="lblRojo" Text="Si existen maestros de apoyo con formaci�n diferente de las especificadas, escriba la cantidad de personal de acuerdo con su situaci�n acad�mica y sexo." Width="575px"
                                   ></asp:Label>&nbsp;</td><td class="Orila">
                                       &nbsp;</td>
            </tr>
            <tr>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV962" runat="server" Columns="30" TabIndex="11401" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV963" runat="server" Columns="3" TabIndex="11402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV964" runat="server" Columns="3" TabIndex="11403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV965" runat="server" Columns="3" TabIndex="11404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV966" runat="server" Columns="3" TabIndex="11405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV967" runat="server" Columns="3" TabIndex="11406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV968" runat="server" Columns="3" TabIndex="11407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV969" runat="server" Columns="3" TabIndex="11408" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV970" runat="server" Columns="3" TabIndex="11409" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV971" runat="server" Columns="30" TabIndex="11501" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV972" runat="server" Columns="3" TabIndex="11502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV973" runat="server" Columns="3" TabIndex="11503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV974" runat="server" Columns="3" TabIndex="11504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV975" runat="server" Columns="3" TabIndex="11505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV976" runat="server" Columns="3" TabIndex="11506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV977" runat="server" Columns="3" TabIndex="11507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV978" runat="server" Columns="3" TabIndex="11508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV979" runat="server" Columns="3" TabIndex="11509" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV980" runat="server" Columns="30" TabIndex="11601" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV981" runat="server" Columns="3" TabIndex="11602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV982" runat="server" Columns="3" TabIndex="11603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV983" runat="server" Columns="3" TabIndex="11604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV984" runat="server" Columns="3" TabIndex="11605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV985" runat="server" Columns="3" TabIndex="11606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV986" runat="server" Columns="3" TabIndex="11607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV987" runat="server" Columns="3" TabIndex="11608" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV988" runat="server" Columns="3" TabIndex="11609" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                            <td class="linaBajo">
                                <asp:Label ID="lblTotal52" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV989" runat="server" Columns="3" TabIndex="11801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV990" runat="server" Columns="3" TabIndex="11802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV991" runat="server" Columns="3" TabIndex="11803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV992" runat="server" Columns="3" TabIndex="11804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV993" runat="server" Columns="3" TabIndex="11805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV994" runat="server" Columns="3" TabIndex="11806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV995" runat="server" Columns="3" TabIndex="11807" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV996" runat="server" Columns="3" TabIndex="11808" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="padding-left: 10px; text-align: center">
                </td>
                <td>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
                <br />
            </td>
         </tr>
             <tr>
                 <td style="text-align: left">
        
                                <asp:Label ID="lblPersParadoc" runat="server" CssClass="lblRojo" Font-Size="14px" Text="PERSONAL PARADOCENTE"></asp:Label></td>
             </tr>
             <tr>
                 <td style="text-align: left">
                                <asp:Label ID="lblInstrucciones6" runat="server" CssClass="lblRojo" Text="6. Escriba la cantidad de personal paradocente, por formaci�n, seg�n su situaci�n acad�mica y sexo."></asp:Label></td>
             </tr>
             <tr>
                 <td style="text-align: left">
        
            <table  style="text-align: center;" >
                <tr>
                            <td colspan="9">
                                </td>
                    <td colspan="1">
                    </td>
                </tr>
                <tr>
                    <td class="linaBajoAlto" colspan="1" rowspan="3" style="text-align: left">
                                <asp:Label ID="lblFormacion6" runat="server" CssClass="lblRojo" Text="FORMACI�N" ></asp:Label></td>
                            <td colspan="6" class="linaBajoAlto">
                                <asp:Label ID="lblLicenciatura6" runat="server" CssClass="lblRojo" Text="LICENCIATURA" ></asp:Label></td>
                    <td class="linaBajoAlto" colspan="2" rowspan="2">
                                <asp:Label ID="lblTotal61" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                    <td class="Orila" colspan="1" rowspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td colspan="2" class="linaBajo">
                                <asp:Label ID="lblTitulados6" runat="server" CssClass="lblRojo" Text="TITULADOS" ></asp:Label></td>
                            <td colspan="2" class="linaBajo">
                                <asp:Label ID="lblNoTitulados6" runat="server" CssClass="lblRojo" Text="NO TITULADOS" ></asp:Label></td>
                            <td colspan="2" class="linaBajo">
                                <asp:Label ID="lblEstudiantes6" runat="server" CssClass="lblRojo" Text="ESTUDIANTES" ></asp:Label></td>
                </tr>
                <tr>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblHombresTit6" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblMujeresTit6" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblHombresNTit6" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblMujeresNTit6" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblHombresEst6" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblMujeresEst6" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                            <td style="padding-left: 10px; text-align: center" colspan="" class="linaBajo">
                                <asp:Label ID="lblHombresTot6" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:Label ID="lblMujeresTot6" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td style="text-align: left" colspan="" class="linaBajo">
                                <asp:Label ID="lblPsicologo6" runat="server" CssClass="lblGrisTit" Text="PSIC�LOGO"
                                    ></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV997" runat="server" Columns="3" TabIndex="11901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV998" runat="server" Columns="3" TabIndex="11902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV999" runat="server" Columns="3" TabIndex="11903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1000" runat="server" Columns="3" TabIndex="11904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1001" runat="server" Columns="3" TabIndex="11905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1002" runat="server" Columns="3" TabIndex="11906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1003" runat="server" Columns="3" TabIndex="11907" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1004" runat="server" Columns="3" TabIndex="11908" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td style="text-align: left" colspan="" class="linaBajo" >
                                <asp:Label ID="lblTrabSoc6" runat="server" CssClass="lblGrisTit" Text="TRABAJADOR SOCIAL"
                                    ></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1005" runat="server" Columns="3" TabIndex="12001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1006" runat="server" Columns="3" TabIndex="12002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1007" runat="server" Columns="3" TabIndex="12003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1008" runat="server" Columns="3" TabIndex="12004" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1009" runat="server" Columns="3" TabIndex="12005" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1010" runat="server" Columns="3" TabIndex="12006" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1011" runat="server" Columns="3" TabIndex="12007" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1012" runat="server" Columns="3" TabIndex="12008" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td style="text-align: left" colspan="" class="linaBajo" >
                                <asp:Label ID="lblMtroEsp6" runat="server" CssClass="lblGrisTit" Text="MAESTRO ESPECIALISTA"
                                    ></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1013" runat="server" Columns="3" TabIndex="12101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1014" runat="server" Columns="3" TabIndex="12102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1015" runat="server" Columns="3" TabIndex="12103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1016" runat="server" Columns="3" TabIndex="12104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1017" runat="server" Columns="3" TabIndex="12105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1018" runat="server" Columns="3" TabIndex="12106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1019" runat="server" Columns="3" TabIndex="12107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1020" runat="server" Columns="3" TabIndex="12108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td colspan="2" style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblEspecialidad6" runat="server" CssClass="lblGrisTit" Text="ESCRIBA LA ESPECIALIDAD DEL MAESTRO"
                                    Width="270px"></asp:Label></td>
                            <td colspan="3" class="linaBajo">
                                <asp:TextBox ID="txtV1021" runat="server" Columns="30" TabIndex="12301" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                &nbsp;</td>
                            <td colspan="" class="linaBajo">
                                &nbsp;</td>
                            <td colspan="" class="linaBajo">
                                &nbsp;</td>
                            <td colspan="" class="linaBajo">
                                &nbsp;</td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td colspan="9" style="text-align: left" class="linaBajo"> 
                                <asp:Label ID="lblNota6" runat="server" CssClass="lblRojo" Text="Si existe personal paradocente con formaci�n diferente a las especificadas, escriba la cantidad de personal de acuerdo a su situaci�n acad�mica y sexo."
                                    Width="646px"></asp:Label></td>
                    <td class="Orila" colspan="1" style="text-align: left">
                        &nbsp;&nbsp;</td>
                </tr>
                <tr>
                            <td style="text-align: left" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1022" runat="server" Columns="30" TabIndex="12501" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1023" runat="server" Columns="3" TabIndex="12502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1024" runat="server" Columns="3" TabIndex="12503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1025" runat="server" Columns="3" TabIndex="12504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1026" runat="server" Columns="3" TabIndex="12505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1027" runat="server" Columns="3" TabIndex="12506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1028" runat="server" Columns="3" TabIndex="12507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1029" runat="server" Columns="3" TabIndex="12508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1030" runat="server" Columns="3" TabIndex="12509" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td style="text-align: left" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1031" runat="server" Columns="30" TabIndex="12601" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1032" runat="server" Columns="3" TabIndex="12602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1033" runat="server" Columns="3" TabIndex="12603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1034" runat="server" Columns="3" TabIndex="12604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1035" runat="server" Columns="3" TabIndex="12605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1036" runat="server" Columns="3" TabIndex="12606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1037" runat="server" Columns="3" TabIndex="12607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1038" runat="server" Columns="3" TabIndex="12608" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1039" runat="server" Columns="3" TabIndex="12609" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td style="text-align: left" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1040" runat="server" Columns="30" TabIndex="12701" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1041" runat="server" Columns="3" TabIndex="12702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1042" runat="server" Columns="3" TabIndex="12703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1043" runat="server" Columns="3" TabIndex="12704" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1044" runat="server" Columns="3" TabIndex="12705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1045" runat="server" Columns="3" TabIndex="12706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1046" runat="server" Columns="3" TabIndex="12707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1047" runat="server" Columns="3" TabIndex="12708" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1048" runat="server" Columns="3" TabIndex="12709" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td style="text-align: left" colspan="" class="linaBajo">
                                <asp:Label ID="lblTotal62" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1049" runat="server" Columns="3" TabIndex="12901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1050" runat="server" Columns="3" TabIndex="12902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1051" runat="server" Columns="3" TabIndex="12903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1052" runat="server" Columns="3" TabIndex="12904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1053" runat="server" Columns="3" TabIndex="12905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1054" runat="server" Columns="3" TabIndex="12906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center" colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1055" runat="server" Columns="3" TabIndex="12907" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td colspan="" class="linaBajo">
                                <asp:TextBox ID="txtV1056" runat="server" Columns="3" TabIndex="12908" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" colspan="1">
                        &nbsp;</td>
                </tr>
            </table>
                     <br />
                 </td>
             </tr>
             <tr>
                 <td style="text-align: left">
        
                                <asp:Label ID="lblPersAmin" runat="server" CssClass="lblRojo" Font-Size="14px" Text="PERSONAL ADMINISTRATIVO"></asp:Label></td>
             </tr>
             <tr>
                 <td style="text-align: left">
        
                                <asp:Label ID="lblInstrucciones7" runat="server" CssClass="lblRojo" Text="7. Escriba el n�mero de secretarias(os) y de otro personal administrativo que labore en la unidad."></asp:Label></td>
             </tr>
             <tr>
                 <td style="text-align: left">
        
            <table  style="text-align: center;" >
                <tr>
                            <td colspan="2"  class="linaBajoAlto">
                                <asp:Label ID="lblSecretarias7" runat="server" CssClass="lblRojo" Text="SECRETARIAS(OS)"
                                    ></asp:Label></td>
                            <td class="Orila">
                                &nbsp;</td>
                            <td colspan="2" class="linaBajoAlto">
                                <asp:Label ID="lblOtro7" runat="server" CssClass="lblRojo" Text="OTRO*"
                                    ></asp:Label></td>
                            <td class="Orila">
                                &nbsp;</td>
                            <td colspan="2" class="linaBajoAlto">
                                <asp:Label ID="lblTotal7" runat="server" CssClass="lblRojo" Text="TOTAL"
                                    ></asp:Label></td>
                    <td class="Orila" colspan="1" style="width: 124px">
                        &nbsp;</td>
                </tr>
                <tr >
                            <td class="linaBajo"  >
                                <asp:Label ID="lblHombresSec" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                   ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblMujeresSec" runat="server" CssClass="lblGrisTit" Text="MUJERES"
                                   ></asp:Label></td>
                            <td class="Orila">
                                &nbsp;</td>
                            <td class="linaBajo">
                                <asp:Label ID="lblHombresOtro7" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblMujeresOtro7" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                            <td class="Orila">
                                &nbsp;</td>
                            <td class="linaBajo">
                                <asp:Label ID="lblHombresTot7" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblMujeresTot7" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                    <td class="Orila" style="width: 124px">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td class="linaBajo"  >
                                <asp:TextBox ID="txtV1074" runat="server" Columns="3" TabIndex="13101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV1075" runat="server" Columns="3" TabIndex="13102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV1076" runat="server" Columns="3" TabIndex="13103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV1077" runat="server" Columns="3" TabIndex="13104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV1078" runat="server" Columns="3" TabIndex="13105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV1079" runat="server" Columns="3" TabIndex="13106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" style="width: 124px">
                        &nbsp;</td>
                </tr>
                <tr>
                            <td colspan="2"  >
                                <asp:Label ID="lblEspecifique7" runat="server" CssClass="lblRojo" Text="*ESPECIFIQUE:" ></asp:Label></td>
                    <td colspan="5">
                                <asp:TextBox ID="txtV1060" runat="server" Columns="30" TabIndex="13301" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td>
                            </td>
                    <td style="width: 124px">
                    </td>
                </tr>
                <tr>
                            <td  >
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                    <td style="width: 124px">
                    </td>
                </tr>
                <tr>
                    <td colspan="9">
                                <asp:Label ID="lblTotalPers" runat="server" CssClass="lblRojo" Text="TOTAL DE PERSONAL (Considere al personal reportado en los apartados 1, 5, 6 y 7)"></asp:Label>
                        <asp:TextBox ID="txtV1080" runat="server" Columns="4" TabIndex="13401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                </tr>
            </table>
                     <br />
                 </td>
             </tr>
             <tr>
                 <td style="text-align: left">
        
                                <asp:Label ID="lblRecursos" runat="server" CssClass="lblRojo" Font-Size="16px" Text="III. RECURSOS"
                                   ></asp:Label></td>
             </tr>
             <tr>
                 <td style="text-align: left">
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad existente de los siguientes recursos."
                                   ></asp:Label></td>
             </tr>
             <tr>
                 <td style="text-align: left">
        
            <table  >
                <tr>
                    <td colspan="11" >
                                </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right" >
                                <asp:Label ID="lblEscritorios" runat="server" CssClass="lblGrisTit" Text="ESCRITORIOS" ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1061" runat="server" Columns="3" TabIndex="13601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 9px">
                                &nbsp;</td>
                            <td colspan="2" style="text-align: right">
                                <asp:Label ID="lblPizarrones" runat="server" CssClass="lblGrisTit" Text="PIZARRONES" ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1064" runat="server" Columns="3" TabIndex="13602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center; width: 16px;">
                                &nbsp;</td>
                            <td colspan="2" style="text-align: right">
                                <asp:Label ID="lblTelevisor" runat="server" CssClass="lblGrisTit" Text="TELEVISORES" ></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV1067" runat="server" Columns="3" TabIndex="13603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right" >
                                <asp:Label ID="lblSillas" runat="server" CssClass="lblGrisTit" Text="SILLAS APILABLES"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1062" runat="server" Columns="3" TabIndex="13701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 9px">
                                &nbsp;</td>
                            <td colspan="2" style="text-align: right">
                                <asp:Label ID="lblArchivero" runat="server" CssClass="lblGrisTit" Text="ARCHIVEROS" ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1065" runat="server" Columns="3" TabIndex="13702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center; width: 16px;">
                                &nbsp;</td>
                            <td colspan="2" style="text-align: right">
                                <asp:Label ID="lblComputadora" runat="server" CssClass="lblGrisTit" Text="COMPUTADORAS"
                                    ></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV1068" runat="server" Columns="3" TabIndex="13703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right" >
                                <asp:Label ID="lblGabinite" runat="server" CssClass="lblGrisTit" Text="GABINETES UNIVERSALES"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1063" runat="server" Columns="3" TabIndex="13801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 9px">
                                &nbsp;</td>
                            <td colspan="2" style="text-align: right" >
                                <asp:Label ID="lblMaqEscribir" runat="server" CssClass="lblGrisTit" Text="M�QUINAS DE ESCRIBIR EL�CTRICAS"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1066" runat="server" Columns="3" TabIndex="13802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center; width: 16px;">
                                &nbsp;</td>
                            <td colspan="2" style="text-align: right">
                                <asp:Label ID="lblImpresora" runat="server" CssClass="lblGrisTit" Text="IMPRESORAS" ></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV1069" runat="server" Columns="3" TabIndex="13803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                </tr>
            </table>
                 </td>
             </tr>
         </table>
                          </center>
       
        
       
        
       
        <center>
        
        
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        
        
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('AGD_911_USAER_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('AGD_911_USAER_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="OpenPageCharged('Inmueble_911_USAER_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Inmueble_911_USAER_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
       
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                function PintaOPTs(){
                     marcar('V854');
                     marcar('V855');
                     marcar('V860');
                     marcar('V861');
                     marcar('V862');
                     marcar('V863');
                     marcar('V864');
                     marcar('V865');
                }  
                
                function marcar(variable){
                     var txtv = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                     if (txtv != null) {
                         var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                         if (txtv.value == 'X'){
                             chk.checked = true;
                         } else {
                             chk.checked = false;
                         } 
                     }
                }
                
                function OPTs2Txt(){
                     marcarTXT('V854');
                     marcarTXT('V855');
                     marcarTXT('V860');
                     marcarTXT('V861');
                     marcarTXT('V862');
                     marcarTXT('V863');
                     marcarTXT('V864');
                     marcarTXT('V865');
                }    
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         
                         var txt = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                } 
                function SleccionarCBOs(){
                    var opcion = document.getElementById('ctl00_cphMainMaster_txtV856').value;
                    document.getElementById('ctl00_cphMainMaster_cboV857').options[opcion].checked = true;
                    //document.getElementById('txtV857').value = document.getElementById('cboV857').options[opcion].text;
                     
                    var opcion2 = document.getElementById('ctl00_cphMainMaster_txtV858').value;
                    document.getElementById('ctl00_cphMainMaster_cboV859').options[opcion2].checked = true;
                    //document.getElementById('txtV859').value = document.getElementById('cboV859').options[opcion].text;
                }
                function cboChangeV857(obj){
                        if (obj!= null){
                       	    var texto 
   	                        var indice = obj.selectedIndex ;
   	                        var valor = obj.options[indice].value ;
   	                        var textoEscogido = obj.options[indice].text ;
   	                        document.getElementById('ctl00_cphMainMaster_txtV856').value = valor;
   	                        document.getElementById('ctl00_cphMainMaster_txtV857').value = textoEscogido;
   	                    }
                }
                
                function cboChangeV859(obj){
                        if (obj!= null){
                       	    var texto 
   	                        var indice = obj.selectedIndex ;
   	                        var valor = obj.options[indice].value ;
   	                        var textoEscogido = obj.options[indice].text ;
   	                        document.getElementById('ctl00_cphMainMaster_txtV858').value = valor;
   	                        document.getElementById('ctl00_cphMainMaster_txtV859').value = textoEscogido;
   	                    }
                }
                
                function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana,ir){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV857'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV859'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1073'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV962'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV971'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV980'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1022'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1031'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1040'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1060'));
                    openPage(ventana,ir);
                }           
                
//                if (obj!= null){
//                       	    var texto 
//   	                        texto = "El numero de opciones del select: " + obj.length ;
//   	                        var indice = obj.selectedIndex ;
//   	                        texto += "\nIndice de la opcion escogida: " + indice ;
//   	                        var valor = obj.options[indice].value ;
//   	                        texto += "\nValor de la opcion escogida: " + valor ;
//   	                        var textoEscogido = obj.options[indice].text ;
//   	                        texto += "\nTexto de la opcion escogida: " + textoEscogido ;
//   	                        alert(texto); 
//   	                    } 
                    PintaOPTs();
 //                   SleccionarCBOs();    
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>
