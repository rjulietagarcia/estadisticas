<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-2(Preescolar)" AutoEventWireup="true" CodeBehind="APRE_911_USAER_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_USAER_2.APRE_911_USAER_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 62;
        MaxRow = 22;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%> 


    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_2',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_2',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_2',true)"><a href="#" title="" class="activo"><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_2',false)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>SECUNDARIA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL Y RECURSOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <%--<li onclick="openPage('Anexo_911_USAER_2')"><a href="#" title=""><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div>    
       <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
        
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table id="TABLE1" >
                        <tr>
                            <td colspan="13"  style="text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="lblInstrucciones3" runat="server" CssClass="lblRojo" Text="3. Escriba la poblaci�n total con necesidades educativas especiales atendida en educaci�n preescolar, por escuela, grado y sexo."
                                    Width="750px"></asp:Label></td>
                            <td  >
                            </td>
                        </tr>
                        <tr>
                            <td colspan="13"  style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPreescolar" runat="server" CssClass="lblRojo" Text="PREESCOLAR" ></asp:Label>
                                </td>
                            <td  >
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajoAlto"  rowspan="3" style="width: 67px; text-align: center">
                                <asp:Label ID="lblEscuela" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td colspan="9"  style="padding-bottom: 10px; text-align: center; vertical-align: bottom; height: 13px;" class="linaBajoAlto">
                                <asp:Label ID="lblPoblacion" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL POR GRADO"
                                    Width="200px"></asp:Label></td>
                            <td class="linaBajoAlto" colspan="3" rowspan="2" style="width: 67px; text-align: center">
                                <asp:Label ID="lblPoblacionTot" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL (SUMA)"
                                    Width="200px"></asp:Label></td>
                            <td class="Orila"  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="3" class="linaBajo">
                                <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1�" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="3" class="linaBajo">
                                <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2�" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" colspan="3" class="linaBajo">
                                <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3�" Width="67px"></asp:Label></td>
                            <td class="Orila" >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center;" class="linaBajo">
                                &nbsp;<asp:Label ID="lblTotal1" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblTotal2" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblTotal3" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;" class="linaBajo">
                                <asp:Label ID="lblHombresTot" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeresTot" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; text-align: center" class="linaBajo">
                                <asp:Label ID="lblTotal31" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV101" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV102" runat="server" Columns="3" TabIndex="10102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV103" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV104" runat="server" Columns="3" TabIndex="10104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV105" runat="server" Columns="3" TabIndex="10105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV106" runat="server" Columns="3" TabIndex="10106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV107" runat="server" Columns="3" TabIndex="10107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV108" runat="server" Columns="3" TabIndex="10108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV109" runat="server" Columns="3" TabIndex="10109" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV110" runat="server" Columns="4" TabIndex="10110" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV111" runat="server" Columns="4" TabIndex="10111" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV112" runat="server" Columns="4" TabIndex="10112" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV113" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV114" runat="server" Columns="3" TabIndex="10202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV115" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV116" runat="server" Columns="3" TabIndex="10204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV117" runat="server" Columns="3" TabIndex="10205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV118" runat="server" Columns="3" TabIndex="10206" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV119" runat="server" Columns="3" TabIndex="10207" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV120" runat="server" Columns="3" TabIndex="10208" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV121" runat="server" Columns="3" TabIndex="10209" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV122" runat="server" Columns="4" TabIndex="10210" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV123" runat="server" Columns="4" TabIndex="10211" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV124" runat="server" Columns="4" TabIndex="10212" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" class="linaBajo">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV125" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV126" runat="server" Columns="3" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV127" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV128" runat="server" Columns="3" TabIndex="10304" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV129" runat="server" Columns="3" TabIndex="10305" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV130" runat="server" Columns="3" TabIndex="10306" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV131" runat="server" Columns="3" TabIndex="10307" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV132" runat="server" Columns="3" TabIndex="10308" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV133" runat="server" Columns="3" TabIndex="10309" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV134" runat="server" Columns="4" TabIndex="10310" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV135" runat="server" Columns="4" TabIndex="10311" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV136" runat="server" Columns="4" TabIndex="10312" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td  style="text-align: center;" class="linaBajo">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV137" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV138" runat="server" Columns="3" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV139" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV140" runat="server" Columns="3" TabIndex="10404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV141" runat="server" Columns="3" TabIndex="10405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV142" runat="server" Columns="3" TabIndex="10406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV143" runat="server" Columns="3" TabIndex="10407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV144" runat="server" Columns="3" TabIndex="10408" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV145" runat="server" Columns="3" TabIndex="10409" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV146" runat="server" Columns="4" TabIndex="10410" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV147" runat="server" Columns="4" TabIndex="10411" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV148" runat="server" Columns="4" TabIndex="10412" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td  style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV149" runat="server" Columns="3" TabIndex="10501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV150" runat="server" Columns="3" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV151" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV152" runat="server" Columns="3" TabIndex="10504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV153" runat="server" Columns="3" TabIndex="10505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV154" runat="server" Columns="3" TabIndex="10506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV155" runat="server" Columns="3" TabIndex="10507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV156" runat="server" Columns="3" TabIndex="10508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV157" runat="server" Columns="3" TabIndex="10509" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV158" runat="server" Columns="4" TabIndex="10510" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV159" runat="server" Columns="4" TabIndex="10511" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV160" runat="server" Columns="4" TabIndex="10512" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td  style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV161" runat="server" Columns="3" TabIndex="10601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV162" runat="server" Columns="3" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV163" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV164" runat="server" Columns="3" TabIndex="10604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV165" runat="server" Columns="3" TabIndex="10605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV166" runat="server" Columns="3" TabIndex="10606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV167" runat="server" Columns="3" TabIndex="10607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV168" runat="server" Columns="3" TabIndex="10608" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV169" runat="server" Columns="3" TabIndex="10609" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV170" runat="server" Columns="4" TabIndex="10610" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV171" runat="server" Columns="4" TabIndex="10611" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV172" runat="server" Columns="4" TabIndex="10612" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td  style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV173" runat="server" Columns="3" TabIndex="10701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV174" runat="server" Columns="3" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV175" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV176" runat="server" Columns="3" TabIndex="10704" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV177" runat="server" Columns="3" TabIndex="10705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV178" runat="server" Columns="3" TabIndex="10706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV179" runat="server" Columns="3" TabIndex="10707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV180" runat="server" Columns="3" TabIndex="10708" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV181" runat="server" Columns="3" TabIndex="10709" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV182" runat="server" Columns="4" TabIndex="10710" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV183" runat="server" Columns="4" TabIndex="10711" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV184" runat="server" Columns="4" TabIndex="10712" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td  style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV185" runat="server" Columns="3" TabIndex="10801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV186" runat="server" Columns="3" TabIndex="10802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV187" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV188" runat="server" Columns="3" TabIndex="10804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV189" runat="server" Columns="3" TabIndex="10805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV190" runat="server" Columns="3" TabIndex="10806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV191" runat="server" Columns="3" TabIndex="10807" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV192" runat="server" Columns="3" TabIndex="10808" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV193" runat="server" Columns="3" TabIndex="10809" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV194" runat="server" Columns="4" TabIndex="10810" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV195" runat="server" Columns="4" TabIndex="10811" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV196" runat="server" Columns="4" TabIndex="10812" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td  style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV197" runat="server" Columns="3" TabIndex="10901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV198" runat="server" Columns="3" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV199" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV200" runat="server" Columns="3" TabIndex="10904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV201" runat="server" Columns="3" TabIndex="10905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV202" runat="server" Columns="3" TabIndex="10906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV203" runat="server" Columns="3" TabIndex="10907" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV204" runat="server" Columns="3" TabIndex="10908" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV205" runat="server" Columns="3" TabIndex="10909" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV206" runat="server" Columns="4" TabIndex="10910" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV207" runat="server" Columns="4" TabIndex="10911" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV208" runat="server" Columns="4" TabIndex="10912" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td  style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV209" runat="server" Columns="3" TabIndex="11001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV210" runat="server" Columns="3" TabIndex="11002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV211" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV212" runat="server" Columns="3" TabIndex="11004" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV213" runat="server" Columns="3" TabIndex="11005" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV214" runat="server" Columns="3" TabIndex="11006" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV215" runat="server" Columns="3" TabIndex="11007" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV216" runat="server" Columns="3" TabIndex="11008" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV217" runat="server" Columns="3" TabIndex="11009" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV218" runat="server" Columns="4" TabIndex="11010" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV219" runat="server" Columns="4" TabIndex="11011" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV220" runat="server" Columns="4" TabIndex="11012" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td  style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblTotal32" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV221" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV222" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;" class="linaBajo">
                                <asp:TextBox ID="txtV223" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV224" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV225" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV226" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV227" runat="server" Columns="3" TabIndex="11107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV228" runat="server" Columns="3" TabIndex="11108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV229" runat="server" Columns="3" TabIndex="11109" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;" class="linaBajo">
                                <asp:TextBox ID="txtV230" runat="server" Columns="4" TabIndex="11110" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV231" runat="server" Columns="4" TabIndex="11111" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV232" runat="server" Columns="5" TabIndex="11112" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                    </table>
         </center>
        <center>
            &nbsp;</center>

        <center>
                                
            <table id="Table2">
            <tr>
            <td colspan="6" style="text-align:left;">
            <asp:Label ID="lblNota3"   runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba, por sexo, el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                                    Width="100%"></asp:Label><br /><br />
            </td>
            </tr>
                <tr>
                            <td colspan="3"  style="text-align: center">
                                <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNOS"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" style="text-align: center">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="200px"></asp:Label></td>
                </tr>
                <tr>
                            <td   style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal4" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left; height: 26px;">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV233" runat="server" Columns="4" TabIndex="11301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV234" runat="server" Columns="4" TabIndex="11302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV235" runat="server" Columns="4" TabIndex="11303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV236" runat="server" Columns="4" TabIndex="11401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV237" runat="server" Columns="4" TabIndex="11402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV238" runat="server" Columns="4" TabIndex="11403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV239" runat="server" Columns="4" TabIndex="11501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV240" runat="server" Columns="4" TabIndex="11502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV241" runat="server" Columns="4" TabIndex="11503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV242" runat="server" Columns="4" TabIndex="11601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV243" runat="server" Columns="4" TabIndex="11602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV244" runat="server" Columns="4" TabIndex="11603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV245" runat="server" Columns="4" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV246" runat="server" Columns="4" TabIndex="11702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV247" runat="server" Columns="4" TabIndex="11703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV248" runat="server" Columns="4" TabIndex="11801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV249" runat="server" Columns="4" TabIndex="11802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV250" runat="server" Columns="4" TabIndex="11803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV251" runat="server" Columns="4" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV252" runat="server" Columns="4" TabIndex="11902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV253" runat="server" Columns="4" TabIndex="11903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV254" runat="server" Columns="4" TabIndex="12001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV255" runat="server" Columns="4" TabIndex="12002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV256" runat="server" Columns="4" TabIndex="12003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                </tr>
                <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV257" runat="server" Columns="4" TabIndex="12161" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                </tr>
            </table>
   
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AINI_911_USAER_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AINI_911_USAER_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('APRIM_911_USAER_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('APRIM_911_USAER_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
         
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>
