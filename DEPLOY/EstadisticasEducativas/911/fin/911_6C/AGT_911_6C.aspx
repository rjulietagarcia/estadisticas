<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.6C (Alumnos Total)" AutoEventWireup="true" CodeBehind="AGT_911_6C.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_6C.AGT_911_6C" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">   
   
    <%--Agregado--%>
    <script type="text/javascript">
         var _enter=true;
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>CAPACITACI�N PARA EL TRABAJO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_6C',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_6C',true)"><a href="#" title=""><span>ALUMNOS POR ESPECIALIDAD</span></a></li>
        <li onclick="openPage('AGT_911_6C',true)"><a href="#" title="" class="activo"><span>TOTAL ALUMNOS</span></a></li>
        <li onclick="openPage('Personal_911_6C',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title=""  class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				  <td>
            <%--a aqui--%>


        <table >
                        <tr>
                            <td colspan="11" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                    Text="II. TOTAL DE ALUMNOS Y ESPECIALIDADES" ></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="11" style="text-align: left; padding-bottom: 20px;">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba el total de especialidades impartidas."
                        ></asp:Label><br />
                                <asp:TextBox ID="txtV216" runat="server" Columns="3" MaxLength="3" TabIndex="10101" CssClass="lblNegro"></asp:TextBox>
                                <br />
                                <br />
                                <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Sume los desgloses de alumnos y grupos de las especialidades y an�telos a continuaci�n."
                                    ></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="11" style="text-align: left; height: 26px;">
                                <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL DE GRUPOS"
                                    Width="194px"></asp:Label>
                                <asp:TextBox ID="txtV150" runat="server" Columns="3" MaxLength="3" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
        </table>
         </center>
  
        <center>
            <table >
                <tr>
                <td>
                    </td>
                <td>
                    </td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="MENOS DE 15 A�OS" ></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15- 19 A�OS"
                        ></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="20-24 A�OS"
                        ></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblReprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="25-34 A�OS"
                        ></asp:Label></td>
                <td style=" text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="35-44 A�OS"
                        ></asp:Label></td>
                <td style=" text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="45-54 A�OS"
                        ></asp:Label></td>
                <td style=" text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="55-64 A�OS"
                        ></asp:Label></td>
                <td style=" text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="65 A�OS Y M�S"
                        ></asp:Label></td>
                <td style=" text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        ></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCION TOTAL" Width="115px"
                      ></asp:Label></td>
                <td class="linaBajo">
                                <asp:TextBox ID="txtV1" runat="server" Columns="4" MaxLength="4" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                                <asp:TextBox ID="txtV2" runat="server" Columns="4" MaxLength="4" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                                <asp:TextBox ID="txtV3" runat="server" Columns="4" MaxLength="4" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                                <asp:TextBox ID="txtV4" runat="server" Columns="4" MaxLength="4" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV5" runat="server" Columns="4" MaxLength="4" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV6" runat="server" Columns="4" MaxLength="4" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV7" runat="server" Columns="4" MaxLength="4" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV8" runat="server" Columns="4" MaxLength="4" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV9" runat="server" Columns="5" MaxLength="5" TabIndex="20109" CssClass="lblNegro"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                      ></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV10" runat="server" Columns="4" MaxLength="4" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV11" runat="server" Columns="4" MaxLength="4" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV12" runat="server" Columns="4" MaxLength="4" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV13" runat="server" Columns="4" MaxLength="4" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV14" runat="server" Columns="4" MaxLength="4" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV15" runat="server" Columns="4" MaxLength="4" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV16" runat="server" Columns="4" MaxLength="4" TabIndex="20207" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV17" runat="server" Columns="4" MaxLength="4" TabIndex="20208" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV18" runat="server" Columns="5" MaxLength="5" TabIndex="20209" CssClass="lblNegro"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ACREDITADOS"
                      ></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV19" runat="server" Columns="4" MaxLength="4" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV20" runat="server" Columns="4" MaxLength="4" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV21" runat="server" Columns="4" MaxLength="4" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV22" runat="server" Columns="4" MaxLength="4" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV23" runat="server" Columns="4" MaxLength="4" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV24" runat="server" Columns="4" MaxLength="4" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV25" runat="server" Columns="4" MaxLength="4" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV26" runat="server" Columns="4" MaxLength="4" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV27" runat="server" Columns="5" MaxLength="5" TabIndex="20309" CssClass="lblNegro"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        ></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCION TOTAL"
                      ></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV28" runat="server" Columns="4" MaxLength="4" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV29" runat="server" Columns="4" MaxLength="4" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV30" runat="server" Columns="4" MaxLength="4" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV31" runat="server" Columns="4" MaxLength="4" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV32" runat="server" Columns="4" MaxLength="4" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV33" runat="server" Columns="4" MaxLength="4" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV34" runat="server" Columns="4" MaxLength="4" TabIndex="20407" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV35" runat="server" Columns="4" MaxLength="4" TabIndex="20408" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV36" runat="server" Columns="5" MaxLength="5" TabIndex="20409" CssClass="lblNegro"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA" ></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV37" runat="server" Columns="4" MaxLength="4" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV38" runat="server" Columns="4" MaxLength="4" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV39" runat="server" Columns="4" MaxLength="4" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV40" runat="server" Columns="4" MaxLength="4" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV41" runat="server" Columns="4" MaxLength="4" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV42" runat="server" Columns="4" MaxLength="4" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV43" runat="server" Columns="4" MaxLength="4" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV44" runat="server" Columns="4" MaxLength="4" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV45" runat="server" Columns="5" MaxLength="5" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ACREDITADOS"
                      ></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV46" runat="server" Columns="4" MaxLength="4" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV47" runat="server" Columns="4" MaxLength="4" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV48" runat="server" Columns="4" MaxLength="4" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV49" runat="server" Columns="4" MaxLength="4" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV50" runat="server" Columns="4" MaxLength="4" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV51" runat="server" Columns="4" MaxLength="4" TabIndex="20606" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV52" runat="server" Columns="4" MaxLength="4" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV53" runat="server" Columns="4" MaxLength="4" TabIndex="20608" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV54" runat="server" Columns="5" MaxLength="5" TabIndex="20609" CssClass="lblNegro"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
        </table>
   
       <hr />
        <table align="center">
            <tr>
                <td style="height: 47px" ><span  onclick="openPage('ACG_911_6C',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td style="height: 47px" ><span  onclick="openPage('ACG_911_6C',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td style="height: 47px" ><span  onclick="openPage('Personal_911_6C',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td style="height: 47px" ><span  onclick="openPage('Personal_911_6C',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />&nbsp;
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
         
        
       <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
