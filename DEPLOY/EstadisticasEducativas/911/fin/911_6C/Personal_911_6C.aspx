<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.6C(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_6C.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_6C.Personal_911_6C" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
 <%--Agregado--%>
    <script type="text/javascript">
         var _enter=true;
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">


    <table  style=" padding-left:300px;">
        <tr><td><span>CAPACITACI�N PARAA EL TRABAJO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_6C',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_6C',true)"><a href="#" title=""><span>ALUMNOS POR ESPECIALIDAD</span></a></li>
        <li onclick="openPage('AGT_911_6C',true)"><a href="#" title=""><span>TOTAL ALUMNOS</span></a></li>
        <li onclick="openPage('Personal_911_6C',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="openPage('Inmueble_911_6C',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

   
                    <table >
                        <tr>
                            <td colspan="11"  style=" text-align: left;">
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="II. PERSONAL POR FUNCI�N"
                                    Width="250px" Font-Size="16px"></asp:Label><br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de personal que realiza funciones de directivo (con y sin grupo), docente, docente especial (profesor de educaci�n f�sica, actividades art�sticas, tecnol�gicas y de idiomas), y administrativo, auxiliar y de servicios, independientemente de su nombramiento, tipo y fuente de pago, desgl�selos seg�n su funci�n, nivel m�ximo de estudios y sexo."
                                    Width="883px"></asp:Label><br />
                                <br />
                                <asp:Label ID="lblNota1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Notas:"
                                    Width="714px"></asp:Label><br />
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="a) Si una persona desempe�a dos o m�s funciones an�tela en aquella a la que dedique m�s tiempo"
                                    Width="790px"></asp:Label><br />
                                <asp:Label ID="lblNota3" runat="server" CssClass="lblRojo" Text="b) Si en la tabla corresponde al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente o en OTROS"
                                    Width="770px"></asp:Label><br />
                                <br />
                            </td>
                        </tr>
                        </table>
                        <table>
                        <tr>
                            <td   >
                                <asp:Label ID="lblNivelEd" runat="server" CssClass="lblRojo" Text="NIVEL EDUCATIVO" Width="200px"></asp:Label></td>
                            <td colspan="2" >
                                <asp:Label ID="lblPersDirCG" runat="server" CssClass="lblRojo" Text="PERSONAL DIRECTIVO CON GRUPO" Width="126px"></asp:Label></td>
                            <td colspan="2" >
                                <asp:Label ID="lblPersDirSG" runat="server" CssClass="lblRojo" Text="PERSONAL DIRECTIVO SIN GRUPO"  Width="126px"></asp:Label></td>
                            <td colspan="2"  >
                                <asp:Label ID="lblPersDocente" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE" Width="85px"></asp:Label></td>
                            <td colspan="2" >
                                <asp:Label ID="lblDocEdFis" runat="server" CssClass="lblRojo" Text="PERSONAL DE SERVICIOS PROFESIONALES ESPECIALES" Width="110px"></asp:Label></td>
                            <td colspan="2" >
                                <asp:Label ID="lblPersAdmin" runat="server" CssClass="lblRojo" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS" Height="54px" Width="95px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" >
                            </td>
                            <td >
                                <asp:Label ID="lblPersDirCGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblPersDirCGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblPersDirSGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblPersDirSGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblPersDocenteH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblPersDocenteM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblDocEdFisH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblDocEdFisM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblDocActArtH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td >
                                <asp:Label ID="lblDocActArtM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" >
                                <asp:Label ID="lblPrimInc" runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV55" runat="server" Columns="1" TabIndex="10101" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV56" runat="server" Columns="1" TabIndex="10102" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV156" runat="server" Columns="1" TabIndex="10103" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV157" runat="server" Columns="1" TabIndex="10104" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV57" runat="server" Columns="3" TabIndex="10105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV58" runat="server" Columns="3" TabIndex="10106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV158" runat="server" Columns="2" TabIndex="10107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV159" runat="server" Columns="2" TabIndex="10108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV59" runat="server" Columns="2" TabIndex="10109" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV60" runat="server" Columns="2" TabIndex="10110" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" >
                                <asp:Label ID="lblPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="PRIMARIA"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV61" runat="server" Columns="1" TabIndex="10201" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV62" runat="server" Columns="1" TabIndex="10202" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV160" runat="server" Columns="1" TabIndex="10203" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV161" runat="server" Columns="1" TabIndex="10204" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV63" runat="server" Columns="3" TabIndex="10205" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV64" runat="server" Columns="3" TabIndex="10206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV162" runat="server" Columns="2" TabIndex="10207" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV163" runat="server" Columns="2" TabIndex="10208" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV65" runat="server" Columns="2" TabIndex="10209" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV66" runat="server" Columns="2" TabIndex="10210" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <asp:Label ID="lblSecTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="SECUNDARIA"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV67" runat="server" Columns="1" TabIndex="10301" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV68" runat="server" Columns="1" TabIndex="10302" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV164" runat="server" Columns="1" TabIndex="10303" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV165" runat="server" Columns="1" TabIndex="10304" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV69" runat="server" Columns="3" TabIndex="10305" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV70" runat="server" Columns="3" TabIndex="10306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV166" runat="server" Columns="2" TabIndex="10307" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV167" runat="server" Columns="2" TabIndex="10308" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV71" runat="server" Columns="2" TabIndex="10309" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV72" runat="server" Columns="2" TabIndex="10310" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"  >
                                <asp:Label ID="lblProfTec" runat="server" CssClass="lblGrisTit" Height="17px" Text="PROFESIONAL T�CNICO"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV73" runat="server" Columns="1" TabIndex="10401" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV74" runat="server" Columns="1" TabIndex="10402" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV168" runat="server" Columns="1" TabIndex="10403" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV169" runat="server" Columns="1" TabIndex="10404" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV75" runat="server" Columns="3" TabIndex="10405" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV76" runat="server" Columns="3" TabIndex="10406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV170" runat="server" Columns="2" TabIndex="10407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV171" runat="server" Columns="2" TabIndex="10408" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV77" runat="server" Columns="2" TabIndex="10409" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV78" runat="server" Columns="2" TabIndex="10410" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"  >
                                <asp:Label ID="lblBachTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV79" runat="server" Columns="1" TabIndex="10501" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV80" runat="server" Columns="1" TabIndex="10502" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV172" runat="server" Columns="1" TabIndex="10503" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV173" runat="server" Columns="1" TabIndex="10504" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV81" runat="server" Columns="3" TabIndex="10505" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV82" runat="server" Columns="3" TabIndex="10506" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV174" runat="server" Columns="2" TabIndex="10507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV175" runat="server" Columns="2" TabIndex="10508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV83" runat="server" Columns="2" TabIndex="10509" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV84" runat="server" Columns="2" TabIndex="10510" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"  >
                                <asp:Label ID="lblNormPreeTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV85" runat="server" Columns="1" TabIndex="10601" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV86" runat="server" Columns="1" TabIndex="10602" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV176" runat="server" Columns="1" TabIndex="10603" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV177" runat="server" Columns="1" TabIndex="10604" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV87" runat="server" Columns="3" TabIndex="10605" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV88" runat="server" Columns="3" TabIndex="10606" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV178" runat="server" Columns="2" TabIndex="10607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV179" runat="server" Columns="2" TabIndex="10608" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV89" runat="server" Columns="2" TabIndex="10609" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV90" runat="server" Columns="2" TabIndex="10610" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"  >
                                <asp:Label ID="lblNormPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV91" runat="server" Columns="1" TabIndex="10701" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV92" runat="server" Columns="1" TabIndex="10702" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV180" runat="server" Columns="1" TabIndex="10703" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV181" runat="server" Columns="1" TabIndex="10704" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV93" runat="server" Columns="3" TabIndex="10705" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV94" runat="server" Columns="3" TabIndex="10706" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV182" runat="server" Columns="2" TabIndex="10707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV183" runat="server" Columns="2" TabIndex="10708" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV95" runat="server" Columns="2" TabIndex="10709" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV96" runat="server" Columns="2" TabIndex="10710" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"  >
                                <asp:Label ID="lblNormSupInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV97" runat="server" Columns="1" TabIndex="10801" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV98" runat="server" Columns="1" TabIndex="10802" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV184" runat="server" Columns="1" TabIndex="10803" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV185" runat="server" Columns="1" TabIndex="10804" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV99" runat="server" Columns="3" TabIndex="10805" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV100" runat="server" Columns="3" TabIndex="10806" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV186" runat="server" Columns="2" TabIndex="10807" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV187" runat="server" Columns="2" TabIndex="10808" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV101" runat="server" Columns="2" TabIndex="10809" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV102" runat="server" Columns="2" TabIndex="10810" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"  >
                                <asp:Label ID="lblLicInc" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV103" runat="server" Columns="1" TabIndex="10901" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV104" runat="server" Columns="1" TabIndex="10902" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV188" runat="server" Columns="1" TabIndex="10903" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV189" runat="server" Columns="1" TabIndex="10904" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV105" runat="server" Columns="3" TabIndex="10905" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV106" runat="server" Columns="3" TabIndex="10906" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV190" runat="server" Columns="2" TabIndex="10907" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV191" runat="server" Columns="2" TabIndex="10908" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV107" runat="server" Columns="2" TabIndex="10909" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV108" runat="server" Columns="2" TabIndex="10910" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" >
                                <asp:Label ID="lblMaestInc" runat="server" CssClass="lblGrisTit" Text="MAESTR�A"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV109" runat="server" Columns="1" TabIndex="11001" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV110" runat="server" Columns="1" TabIndex="11002" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV192" runat="server" Columns="1" TabIndex="11003" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV193" runat="server" Columns="1" TabIndex="11004" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV111" runat="server" Columns="3" TabIndex="11005" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV112" runat="server" Columns="3" TabIndex="11006" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV194" runat="server" Columns="2" TabIndex="11007" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV195" runat="server" Columns="2" TabIndex="11008" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV113" runat="server" Columns="2" TabIndex="11009" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV114" runat="server" Columns="2" TabIndex="11010" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"  >
                                <asp:Label ID="lblDocInc" runat="server" CssClass="lblGrisTit" Text="DOCTORADO"></asp:Label></td>
                            <td >
                                <asp:TextBox ID="txtV115" runat="server" Columns="1" TabIndex="11101" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV116" runat="server" Columns="1" TabIndex="11102" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV196" runat="server" Columns="1" TabIndex="11103" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV197" runat="server" Columns="1" TabIndex="11104" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV117" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV118" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV198" runat="server" Columns="2" TabIndex="11107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV199" runat="server" Columns="2" TabIndex="11108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV119" runat="server" Columns="2" TabIndex="11109" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV120" runat="server" Columns="2" TabIndex="11110" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;"  >
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS*"></asp:Label><asp:Label ID="lblEspecifique" runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE"></asp:Label></td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td >
                            </td>
                        </tr>
                        <tr>
                            <td  >
                                <asp:TextBox ID="txtV121" runat="server" Columns="30" TabIndex="20101" Width="189px" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV122" runat="server" Columns="1" TabIndex="20102" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV123" runat="server" Columns="1" TabIndex="20103" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV200" runat="server" Columns="1" TabIndex="20104" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV201" runat="server" Columns="1" TabIndex="20105" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV124" runat="server" Columns="3" TabIndex="20106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV125" runat="server" Columns="3" TabIndex="20107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV202" runat="server" Columns="2" TabIndex="20108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV203" runat="server" Columns="2" TabIndex="20109" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV126" runat="server" Columns="2" TabIndex="20110" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV127" runat="server" Columns="2" TabIndex="20111" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td  >
                                <asp:TextBox ID="txtV128" runat="server" Columns="30" TabIndex="20201" Width="189px" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV129" runat="server" Columns="1" TabIndex="20202" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV130" runat="server" Columns="1" TabIndex="20203" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV204" runat="server" Columns="1" TabIndex="20204" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV205" runat="server" Columns="1" TabIndex="20205" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV131" runat="server" Columns="3" TabIndex="20206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV132" runat="server" Columns="3" TabIndex="20207" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV206" runat="server" Columns="2" TabIndex="20208" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV207" runat="server" Columns="2" TabIndex="20209" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV133" runat="server" Columns="2" TabIndex="20210" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV134" runat="server" Columns="2" TabIndex="20211" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td  style=" height: 30px;">
                                <asp:TextBox ID="txtV135" runat="server" Columns="30" TabIndex="20301" Width="189px" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV136" runat="server" Columns="1" TabIndex="20302" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV137" runat="server" Columns="1" TabIndex="20303" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV208" runat="server" Columns="1" TabIndex="20304" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV209" runat="server" Columns="1" TabIndex="20305" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV138" runat="server" Columns="3" TabIndex="20306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV139" runat="server" Columns="3" TabIndex="20307" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV210" runat="server" Columns="2" TabIndex="20308" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV211" runat="server" Columns="2" TabIndex="20309" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV140" runat="server" Columns="2" TabIndex="20310" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV141" runat="server" Columns="2" TabIndex="20311" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td  >
                                <asp:Label ID="lblSubtotales" runat="server" CssClass="lblGrisTit" Text="SUBTOTALES"></asp:Label></td>
                            <td style="width: 66px;  height: 25px;">
                                <asp:TextBox ID="txtV142" runat="server" Columns="2" TabIndex="20401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 66px;  height: 25px;">
                                <asp:TextBox ID="txtV143" runat="server" Columns="2" TabIndex="20402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 66px;  height: 25px;">
                                <asp:TextBox ID="txtV212" runat="server" Columns="2" TabIndex="20403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV213" runat="server" Columns="2" TabIndex="20404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td >
                                <asp:TextBox ID="txtV144" runat="server" Columns="4" TabIndex="20405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 66px;  height: 25px;">
                                <asp:TextBox ID="txtV145" runat="server" Columns="4" TabIndex="20406" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 66px;  height: 25px;">
                                <asp:TextBox ID="txtV214" runat="server" Columns="2" TabIndex="20407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 66px;  height: 25px;">
                                <asp:TextBox ID="txtV215" runat="server" Columns="2" TabIndex="20408" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 66px;  height: 25px;">
                                <asp:TextBox ID="txtV146" runat="server" Columns="2" TabIndex="20409" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" height: 25px;">
                                <asp:TextBox ID="txtV147" runat="server" Columns="2" TabIndex="20410" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 66px; height: 25px">
                                </td>
                                <td colspan="8" style="height: 25px; text-align: right">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL DE PERSONAL (Suma de subtotales)"></asp:Label></td>
                                <td style="height: 25px">
                                <asp:TextBox ID="txtV148" runat="server" Columns="4" TabIndex="20411" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="width: 66px; height: 25px">
                                </td>
                                <td colspan="8" style="height: 25px; text-align: right">
                                </td>
                                <td style="height: 25px">
                                </td>
                            </tr>
                        </table>
                        <table>
                        <tr>
                            <td colspan="3"  style="text-align: left">
                                <asp:Label ID="lblInstrucciones2" runat="server" CssClass="lblRojo" Text="2. Escriba el personal docente, seg�n el tiempo que dedica a la funci�n acad�mica"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="Nota: Si en la instituci�n no se utiliza el t�rmino tres cuartos de tiempo, no lo considere"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                            <asp:Label ID="lblSecretarios" runat="server" CssClass="lblGrisTit" Text="TIEMPO COMPLETO"></asp:Label></td>
                            <td>
                                            <asp:TextBox ID="txtV151" runat="server" Columns="4" TabIndex="30101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 162px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                            <asp:Label ID="lblAdjuntos" runat="server" CssClass="lblGrisTit" Text="TRES CUARTOS DE TIEMPO"></asp:Label></td>
                            <td style="text-align: center">
                                            <asp:TextBox ID="txtV152" runat="server" Columns="4" TabIndex="30201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 162px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                            <asp:Label ID="lblAdjCambAct" runat="server" CssClass="lblGrisTit" Text="MEDIO TIEMPO"></asp:Label></td>
                            <td style="text-align: center">
                                            <asp:TextBox ID="txtV153" runat="server" Columns="4" TabIndex="30301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 162px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                            <asp:Label ID="lblProyectos" runat="server" CssClass="lblGrisTit" Text="POR HORAS"></asp:Label></td>
                            <td style="text-align: center">
                                            <asp:TextBox ID="txtV154" runat="server" Columns="4" TabIndex="30401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 162px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td  style="vertical-align: top; height: 100px; text-align: left;">
                                <asp:Label ID="lblEspeciales" runat="server" CssClass="lblRojo" Text="TOTAL" Width="65px"></asp:Label>
                                <br />
                                <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Size="12px" Text="(Este total debe coincidir con la suma de personal docente reportado en la pregunta 1 de esta secci�n.)"
                                    Width="153px"></asp:Label></td>
                            <td style="vertical-align: top; text-align: center">
                                            <asp:TextBox ID="txtV155" runat="server" Columns="4" TabIndex="30501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="vertical-align: top; width: 162px; text-align: center">
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                 
    
       <hr />
        <table align="center">
            <tr>
                <td style="height: 47px" ><span  onclick="openPage('AGT_911_6C',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td style="height: 47px" ><span  onclick="openPage('AGT_911_6C',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td style="height: 47px" ><span  onclick="openPage('Inmueble_911_6C',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td style="height: 47px" ><span  onclick="openPage('Inmueble_911_6C',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />&nbsp;
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
           GetTabIndexes();
        </script> 
</asp:Content>
