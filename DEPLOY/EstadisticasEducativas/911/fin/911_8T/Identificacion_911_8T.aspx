<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8T(Identificaci�n)"
    AutoEventWireup="true" Codebehind="Identificacion_911_8T.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8T.Identificacion_911_8T" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <script type="text/javascript">
        var _enter=true;
    </script>

    <%--Agregado--%>

    <script language="javascript" type="text/javascript" src="../../../tema/js/ArrowsHandler.js"></script>

    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 17;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>

    <%--Agregado--%>
    <div id="logo">
    </div>
    <div style="min-width: 1300px; height: 65px;">
        <div id="header" style=" padding-left:300px;">
            <table>
                <tr>
                    <td>
                        <span>BACHILLERATO TECNOL�GICO</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
                </tr>
            </table>
        </div>
    </div>
    <div id="menu" style="min-width: 1300px;">
        <ul class="left">
            <li onclick="openPage('Identificacion', true)"><a href="#" title="" class="activo"><span>IDENTIFICACI�N</span></a></li>
            <li onclick="openPage('ACG',true)"><a href="#" title="" ><span>ALUMNOS POR CARRERA</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ALUMNOS POR EDAD</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL Y PLANTELES</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
         </ul>
        <ul class="right">
            <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    <div id="tooltipayuda" class="balloonstyle">
        <p>
            Favor de revisar los datos de identificaci�n de su plantel y en caso de observar
            alg�n error u omisi�n, indicar las correcciones necesarias en la secci�n de observaciones
            que se encuentra en el men� OFICIALIZACI�N.</p>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>

        <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0"
            cellpadding="0" align="center" border="0">
            <tr>
                <td class="EsqSupIzq">
                </td>
                <td class="RepSup">
                </td>
                <td class="EsqSupDer">
                </td>
            </tr>
            <tr>
                <td class="RepLatIzq">
                </td>
                <td>
                    <%--a aqui--%>
                    <table>
                        <tr>
                            <td align="center">
                                <table style="width: 800px" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                             <td colspan="5" style="text-align:right;">
                                
                               
                                                 <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                                                 </cc1:ToolkitScriptManager>
                                                 <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
                                 </asp:ScriptManagerProxy>



                                                 
                                
                                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
                                 TargetControlID="bOficializar"
                                PopupControlID="pOficializar" 
                                BackgroundCssClass="modalBackgroundGeneral" 
                                CancelControlID="ibClose" />   
                                 
                                
                                
                                <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" runat="server" TargetControlID="pInner" Corners="All" Radius="6"  >
                                </cc1:RoundedCornersExtender>
                               
<table style="text-align:right; padding-left:500px;">
                                <tr>
                                <td align="right">
                                <asp:Button ID="bOficializar" runat="server" CssClass="botones2"  Text="MOTIVO DE NO CAPTURA" />
                                </td>
                                <td align="right">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                                <ContentTemplate>
                                <asp:Button ID="cmdImprimircuestionario"  runat="server" CssClass="botones2" Text="IMPRIMIR CUESTIONARIO VACIO" OnClientClick="javascript:AbrirPDF()" />
                                </ContentTemplate>
                                </asp:UpdatePanel>
                                </td>
                                </tr>
                                </table>
                                <br />
                                <%--INICIO DEL MODAL POPUP PANEL--%>
                                <asp:Panel ID="pOficializar" runat="server" CssClass="modalPopupGeneral"  >
                                <asp:Panel ID="pInner"  runat="server"  style="background-color:White;  " >
                                
                                 <%--inicio tabla de aqui--%>
                                        <table onkeydown="javascript:enter(event);" class="fondot" id="Table1" cellspacing="0" cellpadding="0" align="center" border="0">
				                            <tr>
					                            <td class="EsqSupIzq"></td>
					                            <td class="RepSup"></td>
					                            <td class="EsqSupDer"></td>
				                            </tr>
				                            <tr>
					                            <td class="RepLatIzq"> </td>
				                                <td>
                                        <%--a aqui--%>

                             
                                                    <table id="Table2" cellspacing="0" cellpadding="0" width="440" border="0"> 
                                                            <tr> 
                                                                <td> 
                                                                </td> 
                                                            </tr> 
                                                            <tr> 
                                                                <td> 
                                                                    <table style="width: 620px">
                                                <tr>
                                                    <td colspan="2" style="height: 28px; text-align:center;">
                                                    <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Text="MOTIVO DE NO CAPTURA" Font-Size="16pt"></asp:Label>
                                                     <br /><br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td  colspan="2" >
                                                       </td>
                                                       
                                                </tr>
                                                <tr>
                                                    <td style="width: 310px">
                                                        &nbsp;<asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="Declaro los datos ingresados en el cuestionario como ver�dicos y finalizo la captura de los mismos." Width="264px"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Text="No fue posible contestar el cuestionario por el siguiente motivo:"
                                                            Width="222px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 310px">
                                                        <asp:Button ID="cmdOficializar" runat="server" CssClass="botones2" Text="Oficializar" OnClick="cmdOficializar_Click" Enabled="False" /></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlMotivos" runat="server" Width="270px" CssClass="lblNegro" >
                                                        </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 310px">
                                                        &nbsp;</td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 310px">
                                                        <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Text="Generar comprobante de captura."></asp:Label></td>
                                                    <td>
                                                      
 

                                                    <asp:UpdatePanel ID="upSect" runat="server" >
                                                    <ContentTemplate>
                                                        <asp:Button ID="cmdGenerarComprobante" runat="server" CssClass="botones2" Text="Aceptar" OnClientClick="javascript:AbrirPDFComprobante()" />
                                                        </ContentTemplate>
                                                        </asp:UpdatePanel>

                                                   </td>
                                                </tr>
                                                 
                                              
                                                
                                                <tr>
                                                    <td style="width: 310px">
                                                      </td> 
                                                    <td>
                                                    <br />
                                                      
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 310px"></td>
                                                    <td><br />
                                                    
                                                    
                                                    </td>
                                                </tr>
                                            </table>
                                                                </td> 
                                                            </tr> 
                                                            <tr> 
                                                                <td> 
                                                                </td> 
                                                            </tr> 
                                                        </table> 
                                        <div > 
                                            <asp:Label ID="lblResultado" runat="server"></asp:Label>
                                        </div>

                                       <%--cierre tabla de aqui--%>
				                                </td>
			                                    <td class="RepLatDer">&nbsp;</td>
			                                </tr>
			                                <tr>
				                                <td class="EsqInfIzq"></td>
				                                <td class="RepInf"></td>
				                                <td class="EsqInfDer"></td>
			                                </tr>
			                            <!--</TBODY>-->
			                            </table>
                                        <%--a aqui--%>
                                 
                                
                                </asp:Panel>
                                <asp:ImageButton ID="ibClose" runat="server" ImageUrl="~/tema/images/close.png"  CssClass="popupCloseGeneral"/>
                                </asp:Panel>
                                
                                <%--FIN DEL MODAL POPUP PANEL--%>
                                 
                            </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblClaveInmueble" runat="server" CssClass="lblRojo" Text="Inmueble:"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtClaveInmueble" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="10" ReadOnly="True" TabIndex="10101" BorderStyle="Groove"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblMunicipio" runat="server" CssClass="lblRojo" Text="Municipio:"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtMunicipio" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="50" ReadOnly="True" TabIndex="10201" BorderStyle="Groove"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblLocalidad" runat="server" CssClass="lblRojo" Text="Localidad:"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtLocalidad" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="70" ReadOnly="True" TabIndex="10301" BorderStyle="Groove"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblColonia" runat="server" CssClass="lblRojo" Text="Colonia:"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtColonia" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="70" ReadOnly="True" TabIndex="10401" BorderStyle="Groove"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblCalle" runat="server" CssClass="lblRojo" Text="Calle:"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtCalle" runat="server" CssClass="lblNegro" Font-Size="11px" Columns="30"
                                                ReadOnly="True" TabIndex="10501" BorderStyle="Groove"></asp:TextBox>
                                            <asp:Label ID="lblEntreCalle" runat="server" CssClass="lblRojo" Text="Entre Calle:"></asp:Label>
                                            <asp:TextBox ID="txtEntreCalle" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="30" ReadOnly="True" TabIndex="10502" BorderStyle="Groove"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblYCalle" runat="server" CssClass="lblRojo" Text="Y Calle:"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtYCalle" runat="server" CssClass="lblNegro" Font-Size="11px" Columns="30"
                                                ReadOnly="True" TabIndex="10601" BorderStyle="Groove"></asp:TextBox>
                                            <asp:Label ID="lblCPosterior" runat="server" CssClass="lblRojo" Text="C.Posterior:"></asp:Label>
                                            <asp:TextBox ID="txtCPosterior" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="30" ReadOnly="True" TabIndex="10602" BorderStyle="Groove"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblNumeroExterior" runat="server" CssClass="lblRojo" Text="N�mero Exterior:"></asp:Label>
                                            &nbsp; &nbsp;
                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtNumeroExterior" runat="server" Columns="6" CssClass="lblNegro"
                                                Font-Size="11px" ReadOnly="True" TabIndex="10801" BorderStyle="Groove"></asp:TextBox>
                                            <asp:Label ID="lblNumeroInterior" runat="server" CssClass="lblRojo" Text="N�mero Interior:"></asp:Label>
                                            <asp:TextBox ID="txtNumeroInterior" runat="server" Columns="7" CssClass="lblNegro"
                                                Font-Size="11px" ReadOnly="True" TabIndex="10802" BorderStyle="Groove"></asp:TextBox>
                                            <asp:Label ID="lblCodigoPostal" runat="server" CssClass="lblRojo" Text="C�digo Postal:"></asp:Label>
                                            <asp:TextBox ID="txtCodigoPostal" runat="server" Columns="7" CssClass="lblNegro"
                                                Font-Size="11px" ReadOnly="True" TabIndex="10803" BorderStyle="Groove"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="height: 27px">
                                        </td>
                                        <td colspan="1" style="height: 27px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblTipoCT" runat="server" Text="Tipo de Centro de Trabajo:" CssClass="lblRojo"
                                                Width="149px"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtTipoCT" runat="server" CssClass="lblNegro" Font-Size="11px" Columns="4"
                                                AutoPostBack="True" MaxLength="1" TabIndex="11001" ReadOnly="True" BorderStyle="Groove"></asp:TextBox>&nbsp;<asp:TextBox
                                                    ID="txtTipoCTD" runat="server" AutoPostBack="True" BorderStyle="Groove" Columns="4"
                                                    CssClass="lblNegro" Font-Size="11px" MaxLength="1" ReadOnly="True" TabIndex="11002"
                                                    Width="530px"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px; height: 13px;" align="left">
                                            <asp:Label ID="lblClasificador" runat="server" Text="Clasificador:" CssClass="lblRojo"></asp:Label></td>
                                        <td style="height: 13px" colspan="3" align="left">
                                            <asp:TextBox ID="txtClasificador" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="4" AutoPostBack="True" MaxLength="1" TabIndex="11101" ReadOnly="True"
                                                BorderStyle="Groove"></asp:TextBox>&nbsp;<asp:TextBox ID="txtClasificadorD" runat="server"
                                                    AutoPostBack="True" BorderStyle="Groove" Columns="4" CssClass="lblNegro" Font-Size="11px"
                                                    MaxLength="1" ReadOnly="True" TabIndex="11102" Width="530px"></asp:TextBox></td>
                                        <td align="left" colspan="1" style="height: 13px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblIdentificador" runat="server" Text="Identificador:" CssClass="lblRojo"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtIdentificador" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="4" AutoPostBack="True" MaxLength="2" TabIndex="11201" ReadOnly="True"
                                                BorderStyle="Groove"></asp:TextBox>
                                            <asp:TextBox ID="txtIdentificadorD" runat="server" AutoPostBack="True" BorderStyle="Groove"
                                                Columns="4" CssClass="lblNegro" Font-Size="11px" MaxLength="1" ReadOnly="True"
                                                TabIndex="11202" Width="530px"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px; height: 23px;" align="left">
                                            <asp:Label ID="lblDepNormativa" runat="server" Text="Dep. Normativa:" CssClass="lblRojo"></asp:Label></td>
                                        <td colspan="3" style="height: 23px" align="left">
                                            <asp:TextBox ID="txtDepNormativa" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="4" AutoPostBack="True" MaxLength="2" TabIndex="11301" BorderStyle="Groove"></asp:TextBox>&nbsp;<asp:TextBox
                                                    ID="txtDepNormativaD" runat="server" AutoPostBack="True" BorderStyle="Groove"
                                                    Columns="4" CssClass="lblNegro" Font-Size="11px" MaxLength="1" ReadOnly="True"
                                                    TabIndex="11302" Width="530px"></asp:TextBox></td>
                                        <td align="left" colspan="1" style="height: 23px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblDepOperativa" runat="server" Text="Dep. Operativa:" CssClass="lblRojo"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtDepOperativa" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="4" AutoPostBack="True" MaxLength="2" TabIndex="11401" BorderStyle="Groove"></asp:TextBox>
                                            <asp:TextBox ID="txtDepOperativaD" runat="server" AutoPostBack="True" BorderStyle="Groove"
                                                Columns="4" CssClass="lblNegro" Font-Size="11px" MaxLength="1" ReadOnly="True"
                                                TabIndex="11402" Width="530px"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblServicio" runat="server" Text="Servicio:" CssClass="lblRojo"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtServicio" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="4" AutoPostBack="True" MaxLength="3" TabIndex="11501" BorderStyle="Groove"></asp:TextBox>
                                            <asp:TextBox ID="txtServicioD" runat="server" AutoPostBack="True" BorderStyle="Groove"
                                                Columns="4" CssClass="lblNegro" Font-Size="11px" MaxLength="1" ReadOnly="True"
                                                TabIndex="11502" Width="530px"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px; height: 20px;" align="left">
                                            <asp:Label ID="lblSostenimiento" runat="server" Text="Sostenimiento:" CssClass="lblRojo"></asp:Label></td>
                                        <td style="height: 20px" colspan="3" align="left">
                                            <asp:TextBox ID="txtSostenimiento" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="4" AutoPostBack="True" MaxLength="2" TabIndex="11601" BorderStyle="Groove"></asp:TextBox>
                                            <asp:TextBox ID="txtSostenimientoD" runat="server" AutoPostBack="True" BorderStyle="Groove"
                                                Columns="4" CssClass="lblNegro" Font-Size="11px" MaxLength="1" ReadOnly="True"
                                                TabIndex="11602" Width="530px"></asp:TextBox></td>
                                        <td align="left" colspan="1" style="height: 20px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 190px">
                                            <asp:Label ID="lblTurno" runat="server" Text="Turno:" CssClass="lblRojo"></asp:Label></td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtTurno" runat="server" CssClass="lblNegro" Font-Size="11px" Columns="4"
                                                AutoPostBack="True" MaxLength="3" TabIndex="11701" BorderStyle="Groove"></asp:TextBox>
                                            <asp:TextBox ID="txtTurnoD" runat="server" AutoPostBack="True" BorderStyle="Groove"
                                                Columns="4" CssClass="lblNegro" Font-Size="11px" MaxLength="1" ReadOnly="True"
                                                TabIndex="11702" Width="530px"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblCCT" runat="server" CssClass="lblRojo" Text="Clave de Centro de Trabajo:"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtCCTEnt" runat="server" CssClass="lblNegro" Font-Size="11px" Columns="10"
                                                Width="21px" TabIndex="11901" ReadOnly="True" BorderStyle="Outset"></asp:TextBox>
                                            <asp:TextBox ID="txtCCTIden" runat="server" Columns="10" CssClass="lblNegro" Font-Size="11px"
                                                Width="36px" TabIndex="11902" ReadOnly="True" BorderStyle="Outset"></asp:TextBox>
                                            <asp:TextBox ID="txtCCTCons" runat="server" Columns="10" CssClass="lblNegro" Font-Size="11px"
                                                Width="55px" AutoPostBack="True" MaxLength="4" TabIndex="11903" ReadOnly="True"
                                                BorderStyle="Outset"></asp:TextBox>
                                            <asp:TextBox ID="txtCCTDV" runat="server" Columns="10" CssClass="lblNegro" Font-Size="11px"
                                                Width="20px" TabIndex="11904" ReadOnly="True" BorderStyle="Outset"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px; height: 16px" align="left">
                                            <asp:Label ID="lblFechaFundacion" runat="server" CssClass="lblRojo" Text="Fecha de Fundaci�n:"></asp:Label></td>
                                        <td colspan="3" style="height: 16px" align="left">
                                            <asp:TextBox ID="txtFechaFundacion" runat="server" Columns="10" CssClass="lblNegro"
                                                Font-Size="11px" Width="70px" TabIndex="12101" ReadOnly="True" BorderStyle="Groove"></asp:TextBox>
                                            &nbsp;<asp:Label ID="lblFechaAlta" runat="server" CssClass="lblRojo" Text="Fecha de Alta:"></asp:Label>
                                            <asp:TextBox ID="txtFechaAlta" runat="server" Columns="10" CssClass="lblNegro" Font-Size="11px"
                                                Width="70px" TabIndex="12102" ReadOnly="True" BorderStyle="Groove"></asp:TextBox></td>
                                        <td align="left" colspan="1" style="height: 16px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px" align="left">
                                            <asp:Label ID="lblNombreCT" runat="server" Text="Nombre del Centro de Trabajo:" CssClass="lblRojo"
                                                Width="172px"></asp:Label></td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="txtNombreCT" runat="server" CssClass="lblNegro" Font-Size="11px"
                                                Columns="70" Width="450px" MaxLength="100" TabIndex="12103" BorderStyle="Groove"
                                                ReadOnly="True"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 190px">
                                        </td>
                                        <td align="left" colspan="3">
                                            &nbsp;</td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 190px">
                                        </td>
                                        <td align="left" colspan="3">
                                            <div id="tels" runat="server">
                                            </div>
                                        </td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 190px">
                                        </td>
                                        <td align="left" colspan="3">
                                        </td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 190px">
                                            <asp:Label ID="lblDirector" runat="server" CssClass="lblRojo" Text="Director:" Width="72px"></asp:Label></td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtDirector" runat="server" BorderStyle="Groove" Columns="70" CssClass="lblNegro"
                                                Font-Size="11px" MaxLength="100" ReadOnly="True" TabIndex="12104" Width="450px"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 190px">
                                        </td>
                                        <td align="left" colspan="3">
                                        </td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 190px">
                                            <asp:Label ID="lblRegion" runat="server" CssClass="lblRojo" Text="Regi�n:" Width="52px"></asp:Label></td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtRegion" runat="server" BorderStyle="Groove" Columns="10" CssClass="lblNegro"
                                                Font-Size="11px" ReadOnly="True" TabIndex="12105" Width="70px"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 190px">
                                            <asp:Label ID="lblZona" runat="server" CssClass="lblRojo" Text="Zona:" Width="50px"></asp:Label></td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtZona" runat="server" BorderStyle="Groove" Columns="10" CssClass="lblNegro"
                                                Font-Size="11px" ReadOnly="True" TabIndex="12106" Width="70px"></asp:TextBox></td>
                                        <td align="left" colspan="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" style="height: 8px">
                                            &nbsp;</td>
                                        <td align="left" colspan="1" style="height: 8px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" style="height: 8px">
                                        </td>
                                        <td align="left" colspan="1" style="height: 8px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" style="height: 8px">
                                        </td>
                                        <td align="left" colspan="1" style="height: 8px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" style="width: 190px; height: 8px">
                                            &nbsp;</td>
                                        <td align="left" colspan="1" style="height: 8px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" style="height: 8px">
                                            &nbsp;</td>
                                        <td align="left" colspan="1" style="height: 8px">
                                        </td>
                                    </tr>
                                </table>
                                <asp:Label ID="lblMsg" runat="server" CssClass="msg"></asp:Label></td>
                        </tr>
                    </table>
                    <hr />
                    <table align="center">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td >
                            </td>
                            <td style="font-weight: bold">
                                <span onclick="openPage('ACG',true)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td>
                            <td style="font-weight: bold">
                                <span onclick="openPage('ACG',true)"><a href="#" title="">
                                    <img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                        </tr>
                    </table>
                    <%--cierre tabla de aqui--%>
                </td>
                <td class="RepLatDer">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="EsqInfIzq">
                </td>
                <td class="RepInf">
                </td>
                <td class="EsqInfDer">
                </td>
            </tr>
            <!--</TBODY>-->
        </table>
        <%--a aqui--%>
          <asp:HiddenField ID="hidRuta" runat="server" />  
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hidIdCtrl" runat="server" value="" style="font-weight: bold" />
    </center>

    <script type="text/javascript">
        function openPage(page, ir)
        {
            if (ir)
                window.location.href=page+"_911_8T.aspx";
        }
    </script>

    <%--Agregado--%>

    <script type="text/javascript">
       GetTabIndexes();
           


function AbrirPDFComprobante(){
               var ruta = document.getElementById("ctl00_cphMainMaster_hidIdCtrl").value
               if (ruta != ""){
                    window.open ('../Reportes/Reporte.aspx?OCE=1&ctrl='+ruta,'','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668')
               }
           }             
            function AbrirPDF(){
               var ruta = document.getElementById("ctl00_cphMainMaster_hidIdCtrl").value
               if (ruta != ""){
                    window.open ('../Reportes/Reporte.aspx?OCE=2&ctrl='+ruta,'','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668')
               }
           }

    </script>

    <%--Agregado--%>
</asp:Content>
