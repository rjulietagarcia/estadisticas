using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;


namespace EstadisticasEducativas._911.fin._911_8T
{
    public partial class AG1_911_8T : System.Web.UI.Page, ICallbackEventHandler
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV92.Attributes["onkeypress"] = "return Num(event)";
                txtV93.Attributes["onkeypress"] = "return Num(event)";
                txtV94.Attributes["onkeypress"] = "return Num(event)";
                txtV95.Attributes["onkeypress"] = "return Num(event)";
                txtV96.Attributes["onkeypress"] = "return Num(event)";
                txtV97.Attributes["onkeypress"] = "return Num(event)";
                txtV98.Attributes["onkeypress"] = "return Num(event)";
                txtV99.Attributes["onkeypress"] = "return Num(event)";
                txtV100.Attributes["onkeypress"] = "return Num(event)";
                txtV101.Attributes["onkeypress"] = "return Num(event)";
                txtV102.Attributes["onkeypress"] = "return Num(event)";
                txtV103.Attributes["onkeypress"] = "return Num(event)";
                txtV104.Attributes["onkeypress"] = "return Num(event)";
                txtV105.Attributes["onkeypress"] = "return Num(event)";
                txtV106.Attributes["onkeypress"] = "return Num(event)";
                txtV107.Attributes["onkeypress"] = "return Num(event)";
                txtV108.Attributes["onkeypress"] = "return Num(event)";
                txtV109.Attributes["onkeypress"] = "return Num(event)";
                txtV110.Attributes["onkeypress"] = "return Num(event)";
                txtV111.Attributes["onkeypress"] = "return Num(event)";
                txtV112.Attributes["onkeypress"] = "return Num(event)";
                txtV113.Attributes["onkeypress"] = "return Num(event)";
                txtV114.Attributes["onkeypress"] = "return Num(event)";
                txtV115.Attributes["onkeypress"] = "return Num(event)";
                txtV116.Attributes["onkeypress"] = "return Num(event)";
                txtV117.Attributes["onkeypress"] = "return Num(event)";
                txtV118.Attributes["onkeypress"] = "return Num(event)";
                txtV119.Attributes["onkeypress"] = "return Num(event)";
                txtV120.Attributes["onkeypress"] = "return Num(event)";
                txtV121.Attributes["onkeypress"] = "return Num(event)";
                txtV122.Attributes["onkeypress"] = "return Num(event)";
                txtV123.Attributes["onkeypress"] = "return Num(event)";
                txtV124.Attributes["onkeypress"] = "return Num(event)";
                txtV125.Attributes["onkeypress"] = "return Num(event)";
                txtV126.Attributes["onkeypress"] = "return Num(event)";
                txtV127.Attributes["onkeypress"] = "return Num(event)";
                txtV128.Attributes["onkeypress"] = "return Num(event)";
                txtV129.Attributes["onkeypress"] = "return Num(event)";
                txtV130.Attributes["onkeypress"] = "return Num(event)";
                txtV131.Attributes["onkeypress"] = "return Num(event)";
                txtV132.Attributes["onkeypress"] = "return Num(event)";
                txtV133.Attributes["onkeypress"] = "return Num(event)";
                txtV134.Attributes["onkeypress"] = "return Num(event)";
                txtV135.Attributes["onkeypress"] = "return Num(event)";
                txtV136.Attributes["onkeypress"] = "return Num(event)";
                txtV137.Attributes["onkeypress"] = "return Num(event)";
                txtV138.Attributes["onkeypress"] = "return Num(event)";
                txtV139.Attributes["onkeypress"] = "return Num(event)";
                txtV140.Attributes["onkeypress"] = "return Num(event)";
                txtV141.Attributes["onkeypress"] = "return Num(event)";
                txtV142.Attributes["onkeypress"] = "return Num(event)";
                txtV143.Attributes["onkeypress"] = "return Num(event)";
                txtV144.Attributes["onkeypress"] = "return Num(event)";
                txtV145.Attributes["onkeypress"] = "return Num(event)";
                txtV146.Attributes["onkeypress"] = "return Num(event)";
                txtV147.Attributes["onkeypress"] = "return Num(event)";
                txtV148.Attributes["onkeypress"] = "return Num(event)";
                txtV149.Attributes["onkeypress"] = "return Num(event)";
                txtV150.Attributes["onkeypress"] = "return Num(event)";
                txtV151.Attributes["onkeypress"] = "return Num(event)";
                txtV152.Attributes["onkeypress"] = "return Num(event)";
                txtV153.Attributes["onkeypress"] = "return Num(event)";
                txtV154.Attributes["onkeypress"] = "return Num(event)";
                txtV155.Attributes["onkeypress"] = "return Num(event)";
                txtV156.Attributes["onkeypress"] = "return Num(event)";
                txtV157.Attributes["onkeypress"] = "return Num(event)";
                txtV158.Attributes["onkeypress"] = "return Num(event)";
                txtV159.Attributes["onkeypress"] = "return Num(event)";
                txtV160.Attributes["onkeypress"] = "return Num(event)";
                txtV161.Attributes["onkeypress"] = "return Num(event)";
                txtV162.Attributes["onkeypress"] = "return Num(event)";
                txtV163.Attributes["onkeypress"] = "return Num(event)";
                txtV164.Attributes["onkeypress"] = "return Num(event)";
                txtV165.Attributes["onkeypress"] = "return Num(event)";
                txtV166.Attributes["onkeypress"] = "return Num(event)";
                txtV167.Attributes["onkeypress"] = "return Num(event)";
                txtV168.Attributes["onkeypress"] = "return Num(event)";
                txtV169.Attributes["onkeypress"] = "return Num(event)";
                txtV170.Attributes["onkeypress"] = "return Num(event)";
                txtV171.Attributes["onkeypress"] = "return Num(event)";
                txtV172.Attributes["onkeypress"] = "return Num(event)";
                txtV173.Attributes["onkeypress"] = "return Num(event)";
                txtV174.Attributes["onkeypress"] = "return Num(event)";
                txtV175.Attributes["onkeypress"] = "return Num(event)";
                txtV176.Attributes["onkeypress"] = "return Num(event)";
                txtV177.Attributes["onkeypress"] = "return Num(event)";
                txtV178.Attributes["onkeypress"] = "return Num(event)";
                txtV179.Attributes["onkeypress"] = "return Num(event)";
                txtV180.Attributes["onkeypress"] = "return Num(event)";
                txtV181.Attributes["onkeypress"] = "return Num(event)";
                txtV182.Attributes["onkeypress"] = "return Num(event)";
                txtV183.Attributes["onkeypress"] = "return Num(event)";
                txtV184.Attributes["onkeypress"] = "return Num(event)";
                txtV185.Attributes["onkeypress"] = "return Num(event)";
                txtV186.Attributes["onkeypress"] = "return Num(event)";
                txtV187.Attributes["onkeypress"] = "return Num(event)";
                txtV188.Attributes["onkeypress"] = "return Num(event)";
                txtV189.Attributes["onkeypress"] = "return Num(event)";
                txtV190.Attributes["onkeypress"] = "return Num(event)";
                txtV191.Attributes["onkeypress"] = "return Num(event)";
                txtV192.Attributes["onkeypress"] = "return Num(event)";
                txtV193.Attributes["onkeypress"] = "return Num(event)";
                txtV194.Attributes["onkeypress"] = "return Num(event)";
                txtV195.Attributes["onkeypress"] = "return Num(event)";
                txtV196.Attributes["onkeypress"] = "return Num(event)";
                txtV197.Attributes["onkeypress"] = "return Num(event)";
                txtV198.Attributes["onkeypress"] = "return Num(event)";
                txtV199.Attributes["onkeypress"] = "return Num(event)";
                txtV200.Attributes["onkeypress"] = "return Num(event)";
                txtV201.Attributes["onkeypress"] = "return Num(event)";
                txtV202.Attributes["onkeypress"] = "return Num(event)";
                txtV203.Attributes["onkeypress"] = "return Num(event)";
                txtV204.Attributes["onkeypress"] = "return Num(event)";
                txtV205.Attributes["onkeypress"] = "return Num(event)";
                txtV206.Attributes["onkeypress"] = "return Num(event)";
                txtV207.Attributes["onkeypress"] = "return Num(event)";
                txtV208.Attributes["onkeypress"] = "return Num(event)";
                txtV209.Attributes["onkeypress"] = "return Num(event)";
                txtV210.Attributes["onkeypress"] = "return Num(event)";
                txtV211.Attributes["onkeypress"] = "return Num(event)";
                txtV212.Attributes["onkeypress"] = "return Num(event)";
                txtV213.Attributes["onkeypress"] = "return Num(event)";
                txtV214.Attributes["onkeypress"] = "return Num(event)";
                txtV215.Attributes["onkeypress"] = "return Num(event)";
                txtV216.Attributes["onkeypress"] = "return Num(event)";
                txtV217.Attributes["onkeypress"] = "return Num(event)";
                txtV218.Attributes["onkeypress"] = "return Num(event)";
                txtV219.Attributes["onkeypress"] = "return Num(event)";
                txtV220.Attributes["onkeypress"] = "return Num(event)";
                txtV221.Attributes["onkeypress"] = "return Num(event)";
                txtV222.Attributes["onkeypress"] = "return Num(event)";
                txtV223.Attributes["onkeypress"] = "return Num(event)";
                txtV224.Attributes["onkeypress"] = "return Num(event)";
                txtV225.Attributes["onkeypress"] = "return Num(event)";
                txtV226.Attributes["onkeypress"] = "return Num(event)";
                txtV227.Attributes["onkeypress"] = "return Num(event)";
                txtV228.Attributes["onkeypress"] = "return Num(event)";
                txtV229.Attributes["onkeypress"] = "return Num(event)";
                txtV230.Attributes["onkeypress"] = "return Num(event)";
                txtV231.Attributes["onkeypress"] = "return Num(event)";
                txtV232.Attributes["onkeypress"] = "return Num(event)";
                txtV233.Attributes["onkeypress"] = "return Num(event)";
                txtV234.Attributes["onkeypress"] = "return Num(event)";
                txtV235.Attributes["onkeypress"] = "return Num(event)";
                txtV236.Attributes["onkeypress"] = "return Num(event)";
                txtV237.Attributes["onkeypress"] = "return Num(event)";
                txtV238.Attributes["onkeypress"] = "return Num(event)";
                txtV239.Attributes["onkeypress"] = "return Num(event)";
                txtV240.Attributes["onkeypress"] = "return Num(event)";
                txtV241.Attributes["onkeypress"] = "return Num(event)";
                txtV242.Attributes["onkeypress"] = "return Num(event)";
                txtV243.Attributes["onkeypress"] = "return Num(event)";
                txtV244.Attributes["onkeypress"] = "return Num(event)";
                txtV245.Attributes["onkeypress"] = "return Num(event)";
                txtV246.Attributes["onkeypress"] = "return Num(event)";
                txtV247.Attributes["onkeypress"] = "return Num(event)";
                txtV248.Attributes["onkeypress"] = "return Num(event)";
                txtV249.Attributes["onkeypress"] = "return Num(event)";
                txtV250.Attributes["onkeypress"] = "return Num(event)";
                txtV251.Attributes["onkeypress"] = "return Num(event)";
                txtV252.Attributes["onkeypress"] = "return Num(event)";
                txtV253.Attributes["onkeypress"] = "return Num(event)";
                txtV254.Attributes["onkeypress"] = "return Num(event)";
                txtV255.Attributes["onkeypress"] = "return Num(event)";
                txtV256.Attributes["onkeypress"] = "return Num(event)";
                txtV257.Attributes["onkeypress"] = "return Num(event)";
                txtV258.Attributes["onkeypress"] = "return Num(event)";
                txtV259.Attributes["onkeypress"] = "return Num(event)";
                txtV260.Attributes["onkeypress"] = "return Num(event)";
                txtV261.Attributes["onkeypress"] = "return Num(event)";
                txtV262.Attributes["onkeypress"] = "return Num(event)";
                txtV263.Attributes["onkeypress"] = "return Num(event)";
                txtV264.Attributes["onkeypress"] = "return Num(event)";
                txtV265.Attributes["onkeypress"] = "return Num(event)";
                txtV266.Attributes["onkeypress"] = "return Num(event)";
                txtV267.Attributes["onkeypress"] = "return Num(event)";
                txtV268.Attributes["onkeypress"] = "return Num(event)";
                txtV269.Attributes["onkeypress"] = "return Num(event)";
                txtV270.Attributes["onkeypress"] = "return Num(event)";
                txtV271.Attributes["onkeypress"] = "return Num(event)";
                txtV272.Attributes["onkeypress"] = "return Num(event)";
                txtV273.Attributes["onkeypress"] = "return Num(event)";
                txtV274.Attributes["onkeypress"] = "return Num(event)";
                txtV275.Attributes["onkeypress"] = "return Num(event)";
                txtV276.Attributes["onkeypress"] = "return Num(event)";
                txtV277.Attributes["onkeypress"] = "return Num(event)";
                txtV278.Attributes["onkeypress"] = "return Num(event)";
                txtV279.Attributes["onkeypress"] = "return Num(event)";
                txtV280.Attributes["onkeypress"] = "return Num(event)";
                txtV281.Attributes["onkeypress"] = "return Num(event)";
                txtV282.Attributes["onkeypress"] = "return Num(event)";
                txtV283.Attributes["onkeypress"] = "return Num(event)";
                txtV284.Attributes["onkeypress"] = "return Num(event)";
                txtV285.Attributes["onkeypress"] = "return Num(event)";
                txtV286.Attributes["onkeypress"] = "return Num(event)";
                txtV287.Attributes["onkeypress"] = "return Num(event)";
                txtV288.Attributes["onkeypress"] = "return Num(event)";
                txtV289.Attributes["onkeypress"] = "return Num(event)";
                txtV290.Attributes["onkeypress"] = "return Num(event)";
                txtV291.Attributes["onkeypress"] = "return Num(event)";
                txtV292.Attributes["onkeypress"] = "return Num(event)";
                txtV293.Attributes["onkeypress"] = "return Num(event)";
                txtV294.Attributes["onkeypress"] = "return Num(event)";
                txtV295.Attributes["onkeypress"] = "return Num(event)";
                txtV296.Attributes["onkeypress"] = "return Num(event)";
                txtV297.Attributes["onkeypress"] = "return Num(event)";
                txtV298.Attributes["onkeypress"] = "return Num(event)";
                txtV299.Attributes["onkeypress"] = "return Num(event)";
                txtV300.Attributes["onkeypress"] = "return Num(event)";
                txtV301.Attributes["onkeypress"] = "return Num(event)";
                txtV302.Attributes["onkeypress"] = "return Num(event)";
                txtV303.Attributes["onkeypress"] = "return Num(event)";
                txtV304.Attributes["onkeypress"] = "return Num(event)";
                txtV305.Attributes["onkeypress"] = "return Num(event)";
                txtV306.Attributes["onkeypress"] = "return Num(event)";
                txtV307.Attributes["onkeypress"] = "return Num(event)";
                txtV308.Attributes["onkeypress"] = "return Num(event)";
                txtV309.Attributes["onkeypress"] = "return Num(event)";
                txtV310.Attributes["onkeypress"] = "return Num(event)";
                txtV311.Attributes["onkeypress"] = "return Num(event)";
                txtV312.Attributes["onkeypress"] = "return Num(event)";
                txtV313.Attributes["onkeypress"] = "return Num(event)";
                txtV314.Attributes["onkeypress"] = "return Num(event)";
                txtV315.Attributes["onkeypress"] = "return Num(event)";
                txtV316.Attributes["onkeypress"] = "return Num(event)";
                txtV317.Attributes["onkeypress"] = "return Num(event)";
                txtV318.Attributes["onkeypress"] = "return Num(event)";
                txtV319.Attributes["onkeypress"] = "return Num(event)";
                txtV320.Attributes["onkeypress"] = "return Num(event)";
                txtV321.Attributes["onkeypress"] = "return Num(event)";
                txtV322.Attributes["onkeypress"] = "return Num(event)";
                txtV323.Attributes["onkeypress"] = "return Num(event)";
                txtV324.Attributes["onkeypress"] = "return Num(event)";
                txtV325.Attributes["onkeypress"] = "return Num(event)";
                txtV326.Attributes["onkeypress"] = "return Num(event)";
                txtV327.Attributes["onkeypress"] = "return Num(event)";
                txtV328.Attributes["onkeypress"] = "return Num(event)";
                txtV329.Attributes["onkeypress"] = "return Num(event)";
                txtV330.Attributes["onkeypress"] = "return Num(event)";
                txtV331.Attributes["onkeypress"] = "return Num(event)";
                txtV332.Attributes["onkeypress"] = "return Num(event)";
                txtV333.Attributes["onkeypress"] = "return Num(event)";
                txtV334.Attributes["onkeypress"] = "return Num(event)";
                txtV335.Attributes["onkeypress"] = "return Num(event)";
                txtV336.Attributes["onkeypress"] = "return Num(event)";
                txtV337.Attributes["onkeypress"] = "return Num(event)";
                txtV338.Attributes["onkeypress"] = "return Num(event)";
                txtV339.Attributes["onkeypress"] = "return Num(event)";
                txtV340.Attributes["onkeypress"] = "return Num(event)";
                txtV341.Attributes["onkeypress"] = "return Num(event)";
                txtV342.Attributes["onkeypress"] = "return Num(event)";
                txtV343.Attributes["onkeypress"] = "return Num(event)";
                txtV344.Attributes["onkeypress"] = "return Num(event)";
                txtV345.Attributes["onkeypress"] = "return Num(event)";
                txtV346.Attributes["onkeypress"] = "return Num(event)";
                txtV347.Attributes["onkeypress"] = "return Num(event)";
                txtV348.Attributes["onkeypress"] = "return Num(event)";
                txtV349.Attributes["onkeypress"] = "return Num(event)";
                txtV350.Attributes["onkeypress"] = "return Num(event)";
                txtV351.Attributes["onkeypress"] = "return Num(event)";
                txtV352.Attributes["onkeypress"] = "return Num(event)";
                txtV353.Attributes["onkeypress"] = "return Num(event)";
                txtV354.Attributes["onkeypress"] = "return Num(event)";
                txtV355.Attributes["onkeypress"] = "return Num(event)";
                txtV356.Attributes["onkeypress"] = "return Num(event)";
                txtV357.Attributes["onkeypress"] = "return Num(event)";
                txtV358.Attributes["onkeypress"] = "return Num(event)";
                txtV359.Attributes["onkeypress"] = "return Num(event)";
                txtV360.Attributes["onkeypress"] = "return Num(event)";
                txtV361.Attributes["onkeypress"] = "return Num(event)";
                txtV362.Attributes["onkeypress"] = "return Num(event)";
                txtV363.Attributes["onkeypress"] = "return Num(event)";
                txtV364.Attributes["onkeypress"] = "return Num(event)";
                txtV365.Attributes["onkeypress"] = "return Num(event)";
                txtV366.Attributes["onkeypress"] = "return Num(event)";
                txtV367.Attributes["onkeypress"] = "return Num(event)";
                txtV368.Attributes["onkeypress"] = "return Num(event)";
                txtV369.Attributes["onkeypress"] = "return Num(event)";
                txtV370.Attributes["onkeypress"] = "return Num(event)";
                txtV371.Attributes["onkeypress"] = "return Num(event)";
                txtV372.Attributes["onkeypress"] = "return Num(event)";
                txtV373.Attributes["onkeypress"] = "return Num(event)";
                txtV374.Attributes["onkeypress"] = "return Num(event)";
                txtV375.Attributes["onkeypress"] = "return Num(event)";
                txtV376.Attributes["onkeypress"] = "return Num(event)";
                txtV377.Attributes["onkeypress"] = "return Num(event)";
                txtV378.Attributes["onkeypress"] = "return Num(event)";
                txtV379.Attributes["onkeypress"] = "return Num(event)";
                txtV380.Attributes["onkeypress"] = "return Num(event)";
                txtV381.Attributes["onkeypress"] = "return Num(event)";
                txtV382.Attributes["onkeypress"] = "return Num(event)";
                txtV383.Attributes["onkeypress"] = "return Num(event)";
                txtV384.Attributes["onkeypress"] = "return Num(event)";
                txtV385.Attributes["onkeypress"] = "return Num(event)";
                txtV386.Attributes["onkeypress"] = "return Num(event)";
                txtV387.Attributes["onkeypress"] = "return Num(event)";
                txtV388.Attributes["onkeypress"] = "return Num(event)";
                txtV389.Attributes["onkeypress"] = "return Num(event)";
                txtV390.Attributes["onkeypress"] = "return Num(event)";
                txtV391.Attributes["onkeypress"] = "return Num(event)";
                txtV392.Attributes["onkeypress"] = "return Num(event)";
                txtV393.Attributes["onkeypress"] = "return Num(event)";
                txtV394.Attributes["onkeypress"] = "return Num(event)";
                txtV395.Attributes["onkeypress"] = "return Num(event)";
                txtV396.Attributes["onkeypress"] = "return Num(event)";
                txtV397.Attributes["onkeypress"] = "return Num(event)";
                txtV398.Attributes["onkeypress"] = "return Num(event)";
                txtV399.Attributes["onkeypress"] = "return Num(event)";
                txtV400.Attributes["onkeypress"] = "return Num(event)";
                txtV401.Attributes["onkeypress"] = "return Num(event)";
                txtV402.Attributes["onkeypress"] = "return Num(event)";
                txtV403.Attributes["onkeypress"] = "return Num(event)";
                txtV404.Attributes["onkeypress"] = "return Num(event)";
                txtV405.Attributes["onkeypress"] = "return Num(event)";
                txtV406.Attributes["onkeypress"] = "return Num(event)";
                txtV407.Attributes["onkeypress"] = "return Num(event)";
                txtV408.Attributes["onkeypress"] = "return Num(event)";
                txtV409.Attributes["onkeypress"] = "return Num(event)";
                txtV410.Attributes["onkeypress"] = "return Num(event)";
                txtV411.Attributes["onkeypress"] = "return Num(event)";
                txtV412.Attributes["onkeypress"] = "return Num(event)";
                txtV413.Attributes["onkeypress"] = "return Num(event)";
                txtV414.Attributes["onkeypress"] = "return Num(event)";
                txtV415.Attributes["onkeypress"] = "return Num(event)";
                txtV416.Attributes["onkeypress"] = "return Num(event)";
                txtV417.Attributes["onkeypress"] = "return Num(event)";
                txtV418.Attributes["onkeypress"] = "return Num(event)";
                txtV419.Attributes["onkeypress"] = "return Num(event)";
                txtV420.Attributes["onkeypress"] = "return Num(event)";
                txtV421.Attributes["onkeypress"] = "return Num(event)";
                txtV422.Attributes["onkeypress"] = "return Num(event)";
                txtV423.Attributes["onkeypress"] = "return Num(event)";
                txtV424.Attributes["onkeypress"] = "return Num(event)";
                txtV425.Attributes["onkeypress"] = "return Num(event)";
                txtV426.Attributes["onkeypress"] = "return Num(event)";
                txtV427.Attributes["onkeypress"] = "return Num(event)";
                txtV428.Attributes["onkeypress"] = "return Num(event)";
                txtV429.Attributes["onkeypress"] = "return Num(event)";
                txtV430.Attributes["onkeypress"] = "return Num(event)";
                txtV431.Attributes["onkeypress"] = "return Num(event)";
                txtV432.Attributes["onkeypress"] = "return Num(event)";
                txtV433.Attributes["onkeypress"] = "return Num(event)";
                txtV434.Attributes["onkeypress"] = "return Num(event)";
                txtV435.Attributes["onkeypress"] = "return Num(event)";
                txtV436.Attributes["onkeypress"] = "return Num(event)";
                txtV437.Attributes["onkeypress"] = "return Num(event)";
                txtV438.Attributes["onkeypress"] = "return Num(event)";
                txtV439.Attributes["onkeypress"] = "return Num(event)";
                txtV440.Attributes["onkeypress"] = "return Num(event)";
                txtV441.Attributes["onkeypress"] = "return Num(event)";
                txtV442.Attributes["onkeypress"] = "return Num(event)";
                txtV443.Attributes["onkeypress"] = "return Num(event)";
                txtV444.Attributes["onkeypress"] = "return Num(event)";
                txtV445.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, Class911.Doc_Cuestionario, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }
        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }
    }
}
