<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignMenu.Master" AutoEventWireup="true" CodeBehind="BuscaCT.aspx.cs" Inherits="EstadisticasEducativas._911.fin.WebForm1" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
 
        <br />
        <br />
        <center>
        <div style="text-align:center; width:100%">
             <center>
         
            <br />
                 
                
           <table class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"></td>
				    <td>
				        <table id="Table4" cellSpacing="4" cellPadding="0" width="100%" border="0">										
								<tr>
									<td class="Titulo" style="FONT-WEIGHT: bold" align="center" >
									    Fin de Cursos 2008-2009
                                    </td>
								</tr>
						</table>
						<br />
				  
                        
			            <table   border="0" cellpadding="0" cellspacing="2"  >
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label ID="Label1" runat="server" CssClass="lblEtiqueta" Font-Size="14px" Text="Ciclo:"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlCiclo" runat="server">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label ID="lblNivel" runat="server"  Font-Size="14px" Text="Nivel:" CssClass="lblEtiqueta" ></asp:Label>
                                    </td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlNivel" runat="server" >
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblRegion" runat="server" Text="Regi�n:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtRegion" runat="server" Width="55px" TabIndex="1"
                                        MaxLength="12" Font-Size="14px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblZona" runat="server" Text="Zona:" Width="43px" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtZona" runat="server" Width="55px" TabIndex="2"
                                        MaxLength="3"></asp:TextBox></td>
                            </tr>
                            <tr id="trNiv">
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblClave" runat="server" Text="Centro de Trabajo:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtClaveCT" runat="server" Width="110px" TabIndex="3"
                                        MaxLength="10"></asp:TextBox>
                                    <asp:DropDownList ID="ddlCentroTrabajo" runat="server" Visible="False">
                                    </asp:DropDownList></td>
                            </tr>
                        </table>   
               
                         
                        <br />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="67px" TabIndex="4" OnClick="btnBuscar_Click"   /><br />
                        </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>


            <br />
            <br />
            <br />
          
            <br />
         
                    <asp:Label ID="lblMsg" runat="server"  ></asp:Label>
         
            </center>
        </div>
        </center>
        <br />
     
                <center>
                
                <div style="text-align: center">
                   <center>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        PageSize="50" CellPadding="4" OnPageIndexChanging="GridView1_PageIndexChanging"
                        OnSorting="GridView1_Sorting" OnRowCommand="RowCommand" EmptyDataText="Busqueda sin registros">
                        <PagerSettings PageButtonCount="20" />
                        <Columns>
                            <asp:CommandField ShowSelectButton="True" HeaderText="911" SelectText="&lt;img src='../../tema/images/iconEsc.gif' alt='Ver cuestionario' style='border:0;'&gt;" />
                            
                            <asp:BoundField DataField="Id_CT" HeaderText="Id_CT" SortExpression="Id_CT" Visible="False">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ID_CCTNT"  SortExpression="ID_CCTNT" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID_CCTNT" runat="server" Text='<%# Bind("ID_CCTNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="CveCT" HeaderText="CveCT" SortExpression="CveCT">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomCT" HeaderText="NomCT" SortExpression="NomCT">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveTurno" HeaderText="CveTurno" SortExpression="CveTurno">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomTurno" HeaderText="Turno" SortExpression="NomTurno">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveRegion" HeaderText="Region" SortExpression="CveRegion">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveZona" HeaderText="Zona" SortExpression="CveZona">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Calle" HeaderText="Calle" SortExpression="Calle">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Numero" HeaderText="Numero" SortExpression="Numero">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomColonia" HeaderText="Colonia" SortExpression="NomColonia">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomMunicipio" HeaderText="Municipio" SortExpression="NomMunicipio">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    </center>
                </div>
                </center>
      
        <script type="text/javascript" src="../../tema/js/wait.js"></script>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>            
                <div id="wait" style="position:fixed; text-align:center">                
                    <img src="../../tema/images/indicator_mozilla_blu.gif" alt=''/>            
                </div>        
            </ProgressTemplate>        
        </asp:UpdateProgress>

       


    <script type="text/javascript">
        function enter(e) 
        {
            if (_enter) 
            {
                if (event.keyCode==13)
                {
                    event.keyCode=9; 
                    return event.keyCode;
                }    
            }
        }
        function inicio()
        {
            document.getElementById("txtClaveCT").focus();
        }    
        function Num(evt) {
            var nav4 = window.Event ? true : false;
	        var key = nav4 ? evt.which : evt.keyCode;
            return (key <= 13 || (key >= 48 && key <= 57));
        }
    
       var _enter=true;
        function Vent911(Pagina)
        {
            var v911 = false;
            try{
//               v911 = window.open ("", "ventana911", "width=1,height=1,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
//               v911.close();   
               v911 = window.open (Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
               
                v911.location=Pagina;
                v911.focus();
              
            }
            catch(err)
            {
                alert(err);
            } 
           
          //  v911 = window.open (Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
            //v911 = window.open (Pagina, "ventana911", "");
        }
        //var v911 = false;
//        function Vent911(Pagina)
//        {
//            if (v911 && !v911.closed)
//            { 
//                v911.close();
//            } 
//            window.name
//            v911 = window.open (Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
//            //v911 = window.open (Pagina, "ventana911", "");
//        }
    </script>
</asp:Content>
