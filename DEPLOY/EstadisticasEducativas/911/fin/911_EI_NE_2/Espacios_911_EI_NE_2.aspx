<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-NE2(Espacios)" AutoEventWireup="true" CodeBehind="Espacios_911_EI_NE_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_EI_NE_2.Espacios_911_EI_NE_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 9;
        MaxRow = 22;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">


    <table style=" padding-left:200px;">
        <tr><td><span>EDUCACI�N INICIAL NO ESCOLARIZADA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="OpenPageCharged('Identificacion_911_EI_NE_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="OpenPageCharged('Grupos_911_EI_NE_2',true)"><a href="#" title=""><span>GRUPOS</span></a></li>
        <li onclick="OpenPageCharged('AG_911_EI_NE_2',true)"><a href="#" title=""><span>NI�OS</span></a></li>
        <li onclick="OpenPageCharged('Personal_911_EI_NE_2',true)"><a href="#" title=""><span>PADRES DE FAMILIA Y PERSONAL</span></a></li>
        <li onclick="OpenPageCharged('Programa_911_EI_NE_2',true)"><a href="#" title=""><span>PROGRAMA</span></a></li>
        <li onclick="OpenPageCharged('Espacios_911_EI_NE_2',true)"><a href="#" title="" class="activo"><span>ESPACIOS</span></a></li>
        <li onclick="OpenPageCharged('Inmueble_911_EI_NE_2',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
       <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
    </div>  <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

     <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table id="TABLE1" style="text-align: center">
                        <tr>
                            <td colspan="9" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <asp:Label ID="lblEspacios" runat="server" CssClass="lblRojo" Font-Bold="True" Text="VI. ESPACIOS EDUCATIVOS"
                                    Width="250px" Font-Size="16px"></asp:Label><br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba el n�mero de espacios educativos seg�n su tipo, por delegaci�n o municipio y localidad o colonia."
                                    Width="850px"></asp:Label><br />
                            </td>
                            <td colspan="1" style="padding-right: 5px; padding-left: 5px; width: 2px; height: 3px;
                                text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 67px; height: 3px; text-align: center; padding-right: 5px; padding-left: 5px;">
                                </td>
                            <td style="text-align: center">
                                </td>
                            <td style="text-align: center">
                                </td>
                            <td style="text-align: center">
                                </td>
                            <td style="text-align: center">
                                </td>
                            <td style="text-align: center">
                                </td>
                            <td colspan="3" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblRojo" Text="OTROS" Width="100px"></asp:Label></td>
                            <td class="Orila" colspan="1" style="width: 2px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="padding-right: 5px; padding-left: 5px; width: 67px;
                                height: 3px; text-align: center">
                            </td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPatio" runat="server" CssClass="lblRojo" Text="PATIO" Width="100px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblAula" runat="server" CssClass="lblRojo" Text="AULA" Width="100px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblCasaPart" runat="server" CssClass="lblRojo" Text="CASA PARTICULAR" Width="100px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblComisaria" runat="server" CssClass="lblRojo" Text="COMISAR�A" Width="100px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblCasaCult" runat="server" CssClass="lblRojo" Text="CASA DE CULTURA"
                                    Width="100px"></asp:Label></td>
                            <td colspan="" style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV422" runat="server" Columns="12" TabIndex="10101" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td colspan="" style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV423" runat="server" Columns="12" TabIndex="10102" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td colspan="" style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV424" runat="server" Columns="12" TabIndex="10103" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="Orila" colspan="1" style="width: 2px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr1" runat="server">
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblLoc1" runat="server" CssClass="lblGrisTit" Text="1" Height="17px" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV425" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV426" runat="server" Columns="3" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV427" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV428" runat="server" Columns="3" TabIndex="10304" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV429" runat="server" Columns="3" TabIndex="10305" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV430" runat="server" Columns="3" TabIndex="10306" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV431" runat="server" Columns="3" TabIndex="10307" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV432" runat="server" Columns="3" TabIndex="10308" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr2" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc2" runat="server" CssClass="lblGrisTit" Height="17px" Text="2" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV433" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV434" runat="server" Columns="3" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV435" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV436" runat="server" Columns="3" TabIndex="10404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV437" runat="server" Columns="3" TabIndex="10405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV438" runat="server" Columns="3" TabIndex="10406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV439" runat="server" Columns="3" TabIndex="10407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV440" runat="server" Columns="3" TabIndex="10408" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr3" runat="server">
                            <td class="linaBajoS">
                            <asp:Label ID="lblLoc3" runat="server" CssClass="lblGrisTit" Text="3" Height="17px" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV441" runat="server" Columns="3" TabIndex="10501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV442" runat="server" Columns="3" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV443" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV444" runat="server" Columns="3" TabIndex="10504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV445" runat="server" Columns="3" TabIndex="10505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV446" runat="server" Columns="3" TabIndex="10506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV447" runat="server" Columns="3" TabIndex="10507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV448" runat="server" Columns="3" TabIndex="10508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="tr4" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc4" runat="server" CssClass="lblGrisTit" Height="17px" Text="4" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV449" runat="server" Columns="3" TabIndex="10601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV450" runat="server" Columns="3" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV451" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV452" runat="server" Columns="3" TabIndex="10604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV453" runat="server" Columns="3" TabIndex="10605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV454" runat="server" Columns="3" TabIndex="10606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV455" runat="server" Columns="3" TabIndex="10607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV456" runat="server" Columns="3" TabIndex="10608" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr5" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc5" runat="server" CssClass="lblGrisTit" Height="17px" Text="5" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV457" runat="server" Columns="3" TabIndex="10701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV458" runat="server" Columns="3" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV459" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV460" runat="server" Columns="3" TabIndex="10704" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV461" runat="server" Columns="3" TabIndex="10705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV462" runat="server" Columns="3" TabIndex="10706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV463" runat="server" Columns="3" TabIndex="10707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV464" runat="server" Columns="3" TabIndex="10708" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr6" runat="server">
                            <td class="linaBajoS" >
                                <asp:Label ID="lblLoc6" runat="server" CssClass="lblGrisTit" Height="17px" Text="6" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV465" runat="server" Columns="3" TabIndex="10801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV466" runat="server" Columns="3" TabIndex="10802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV467" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV468" runat="server" Columns="3" TabIndex="10804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV469" runat="server" Columns="3" TabIndex="10805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV470" runat="server" Columns="3" TabIndex="10806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV471" runat="server" Columns="3" TabIndex="10807" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV472" runat="server" Columns="3" TabIndex="10808" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr7" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc7" runat="server" CssClass="lblGrisTit" Height="17px" Text="7" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV473" runat="server" Columns="3" TabIndex="10901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV474" runat="server" Columns="3" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV475" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV476" runat="server" Columns="3" TabIndex="10904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV477" runat="server" Columns="3" TabIndex="10905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV478" runat="server" Columns="3" TabIndex="10906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV479" runat="server" Columns="3" TabIndex="10907" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV480" runat="server" Columns="3" TabIndex="10908" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr8" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc8" runat="server" CssClass="lblGrisTit" Height="17px" Text="8"
                                    Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV481" runat="server" Columns="3" TabIndex="11001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV482" runat="server" Columns="3" TabIndex="11002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV483" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV484" runat="server" Columns="3" TabIndex="11004" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV485" runat="server" Columns="3" TabIndex="11005" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV486" runat="server" Columns="3" TabIndex="11006" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV487" runat="server" Columns="3" TabIndex="11007" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV488" runat="server" Columns="3" TabIndex="11008" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr9" runat="server">
                            <td class="linaBajoS" >
                                <asp:Label ID="lblLoc9" runat="server" CssClass="lblGrisTit" Height="17px" Text="9" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV489" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV490" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV491" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV492" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV493" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV494" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV495" runat="server" Columns="3" TabIndex="11107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV496" runat="server" Columns="3" TabIndex="11108" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr10" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc10" runat="server" CssClass="lblGrisTit" Height="17px" Text="10" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV497" runat="server" Columns="3" TabIndex="11201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV498" runat="server" Columns="3" TabIndex="11202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV499" runat="server" Columns="3" TabIndex="11203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV500" runat="server" Columns="3" TabIndex="11204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV501" runat="server" Columns="3" TabIndex="11205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV502" runat="server" Columns="3" TabIndex="11206" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV503" runat="server" Columns="3" TabIndex="11207" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV504" runat="server" Columns="3" TabIndex="11208" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr11" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc11" runat="server" CssClass="lblGrisTit" Height="17px" Text="11" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV505" runat="server" Columns="3" TabIndex="11301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV506" runat="server" Columns="3" TabIndex="11302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV507" runat="server" Columns="3" TabIndex="11303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV508" runat="server" Columns="3" TabIndex="11304" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV509" runat="server" Columns="3" TabIndex="11305" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV510" runat="server" Columns="3" TabIndex="11306" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV511" runat="server" Columns="3" TabIndex="11307" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV512" runat="server" Columns="3" TabIndex="11308" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr12" runat="server">
                            <td class="linaBajoS" >
                                <asp:Label ID="lblLoc12" runat="server" CssClass="lblGrisTit" Height="17px" Text="12" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV513" runat="server" Columns="3" TabIndex="11401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV514" runat="server" Columns="3" TabIndex="11402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV515" runat="server" Columns="3" TabIndex="11403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV516" runat="server" Columns="3" TabIndex="11404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV517" runat="server" Columns="3" TabIndex="11405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV518" runat="server" Columns="3" TabIndex="11406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV519" runat="server" Columns="3" TabIndex="11407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV520" runat="server" Columns="3" TabIndex="11408" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr13" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc13" runat="server" CssClass="lblGrisTit" Height="17px" Text="13" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV521" runat="server" Columns="3" TabIndex="11501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV522" runat="server" Columns="3" TabIndex="11502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV523" runat="server" Columns="3" TabIndex="11503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV524" runat="server" Columns="3" TabIndex="11504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV525" runat="server" Columns="3" TabIndex="11505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV526" runat="server" Columns="3" TabIndex="11506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV527" runat="server" Columns="3" TabIndex="11507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV528" runat="server" Columns="3" TabIndex="11508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr14" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc14" runat="server" CssClass="lblGrisTit" Height="17px" Text="14" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV529" runat="server" Columns="3" TabIndex="11601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV530" runat="server" Columns="3" TabIndex="11602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV531" runat="server" Columns="3" TabIndex="11603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV532" runat="server" Columns="3" TabIndex="11604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV533" runat="server" Columns="3" TabIndex="11605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV534" runat="server" Columns="3" TabIndex="11606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV535" runat="server" Columns="3" TabIndex="11607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV536" runat="server" Columns="3" TabIndex="11608" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr15" runat="server">
                            <td class="linaBajoS" >
                                <asp:Label ID="lblLoc15" runat="server" CssClass="lblGrisTit" Text="15" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV537" runat="server" Columns="3" TabIndex="11701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV538" runat="server" Columns="3" TabIndex="11702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV539" runat="server" Columns="3" TabIndex="11703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV540" runat="server" Columns="3" TabIndex="11704" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV541" runat="server" Columns="3" TabIndex="11705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV542" runat="server" Columns="3" TabIndex="11706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV543" runat="server" Columns="3" TabIndex="11707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV544" runat="server" Columns="3" TabIndex="11708" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr16" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc16" runat="server" CssClass="lblGrisTit" Text="16" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV545" runat="server" Columns="3" TabIndex="11801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV546" runat="server" Columns="3" TabIndex="11802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV547" runat="server" Columns="3" TabIndex="11803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV548" runat="server" Columns="3" TabIndex="11804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV549" runat="server" Columns="3" TabIndex="11805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV550" runat="server" Columns="3" TabIndex="11806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV551" runat="server" Columns="3" TabIndex="11807" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV552" runat="server" Columns="3" TabIndex="11808" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr17" runat="server">
                            <td class="linaBajo" >
                            <asp:Label ID="lblLoc17" runat="server" CssClass="lblGrisTit" Text="17" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV719" runat="server" Columns="3" TabIndex="11901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV720" runat="server" Columns="3" TabIndex="11902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV721" runat="server" Columns="3" TabIndex="11903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV722" runat="server" Columns="3" TabIndex="11904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV723" runat="server" Columns="3" TabIndex="11905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV724" runat="server" Columns="3" TabIndex="11906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV725" runat="server" Columns="3" TabIndex="11907" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV726" runat="server" Columns="3" TabIndex="11908" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr18" runat="server">
                            <td class="linaBajoS" >
                                <asp:Label ID="lblLoc18" runat="server" CssClass="lblGrisTit" Text="18" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV727" runat="server" Columns="3" TabIndex="12001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV728" runat="server" Columns="3" TabIndex="12002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV729" runat="server" Columns="3" TabIndex="12003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV730" runat="server" Columns="3" TabIndex="12004" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV731" runat="server" Columns="3" TabIndex="12005" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV732" runat="server" Columns="3" TabIndex="12006" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV733" runat="server" Columns="3" TabIndex="12007" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV734" runat="server" Columns="3" TabIndex="12008" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV553" runat="server" Columns="3" TabIndex="12101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV554" runat="server" Columns="3" TabIndex="12102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV555" runat="server" Columns="3" TabIndex="12103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV556" runat="server" Columns="3" TabIndex="12104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV557" runat="server" Columns="3" TabIndex="12105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV558" runat="server" Columns="3" TabIndex="12106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV559" runat="server" Columns="3" TabIndex="12107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV560" runat="server" Columns="3" TabIndex="12108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 2px">
                                &nbsp;</td>
                        </tr>
                    </table>
                                    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('Programa_911_EI_NE_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Programa_911_EI_NE_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="OpenPageCharged('Inmueble_911_EI_NE_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Inmueble_911_EI_NE_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
      <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <br />
        
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                 function ValidaTexto(obj){
                    if((obj.value == '0')||(obj.value == '')){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana,ir){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV422'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV423'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV424'));
                    
                    openPage(ventana,ir);
                }                                        
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>
