<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-NE2(Grupos)" AutoEventWireup="true" CodeBehind="Grupos_911_EI_NE_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_EI_NE_2.Grupos_911_EI_NE_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 62;
        MaxRow = 20;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">


    <table style=" padding-left:200px;">
        <tr><td><span>EDUCACI�N INICIAL NO ESCOLARIZADA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="OpenPageCharged('Identificacion_911_EI_NE_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="OpenPageCharged('Grupos_911_EI_NE_2',true)"><a href="#" title="" class="activo"><span>GRUPOS</span></a></li>
        <li onclick="OpenPageCharged('AG_911_EI_NE_2',false)"><a href="#" title=""><span>NI�OS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PADRES DE FAMILIA Y PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROGRAMA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ESPACIOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table id="TABLE1" style="text-align: center" >
                        <tr>
                            <td colspan="4" style="text-align: left">
                                <asp:Label ID="lblGrupos" runat="server" CssClass="lblRojo" Font-Bold="True" Text="I. UBICACI�N DE GRUPOS"
                                    Width="250px" Font-Size="16px"></asp:Label>
                                <br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba el nombre del municipio o delegaci�n y la localidad o colonia en la cual est�n los grupos que se atienden en el m�dulo, y el n�mero de grupos."
                                    Width="600px"></asp:Label><br />
                            </td>
                            <td colspan="1" style="width: 6px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1">
                                </td>
                            <td style="text-align: center; width: 180px; height: 26px;" class="linaBajoAlto">
                                <asp:Label ID="lblMunicipio" runat="server" CssClass="lblRojo" Text="MUNICIPIO O DELEGACI�N" Width="180px"></asp:Label></td>
                            <td style="text-align: center; width: 180px; height: 26px;" class="linaBajoAlto">
                                <asp:Label ID="lblLocalidad" runat="server" CssClass="lblRojo" Text="LOCALIDAD O COLONIA" Width="180px"></asp:Label></td>
                            <td style="text-align: center; width: 170px; height: 26px;" class="linaBajoAlto">
                                <asp:Label ID="lblNGrupos" runat="server" CssClass="lblRojo" Text="N�MERO DE GRUPOS" Width="160px"></asp:Label></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblLoc1" runat="server" CssClass="lblGrisTit" Text="1" Height="17px" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV1" runat="server" Columns="30" TabIndex="10101" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV2" runat="server" Columns="30" TabIndex="10102" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV3" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc2" runat="server" CssClass="lblGrisTit" Height="17px" Text="2" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV4" runat="server" Columns="30" TabIndex="10201" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV5" runat="server" Columns="30" TabIndex="10202" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV6" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo">
                            <asp:Label ID="lblLoc3" runat="server" CssClass="lblGrisTit" Text="3" Height="17px" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV7" runat="server" Columns="30" TabIndex="10301" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV8" runat="server" Columns="30" TabIndex="10302" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV9" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc4" runat="server" CssClass="lblGrisTit" Height="17px" Text="4" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV10" runat="server" Columns="30" TabIndex="10401" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV11" runat="server" Columns="30" TabIndex="10402" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV12" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc5" runat="server" CssClass="lblGrisTit" Height="17px" Text="5" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV13" runat="server" Columns="30" TabIndex="10501" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV14" runat="server" Columns="30" TabIndex="10502" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV15" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc6" runat="server" CssClass="lblGrisTit" Height="17px" Text="6" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV16" runat="server" Columns="30" TabIndex="10601" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV17" runat="server" Columns="30" TabIndex="10602" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV18" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc7" runat="server" CssClass="lblGrisTit" Height="17px" Text="7" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV19" runat="server" Columns="30" TabIndex="10701" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV20" runat="server" Columns="30" TabIndex="10702" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV21" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc8" runat="server" CssClass="lblGrisTit" Height="17px" Text="8"
                                    Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV22" runat="server" Columns="30" TabIndex="10801" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV23" runat="server" Columns="30" TabIndex="10802" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV24" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc9" runat="server" CssClass="lblGrisTit" Height="17px" Text="9" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV25" runat="server" Columns="30" TabIndex="10901" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV26" runat="server" Columns="30" TabIndex="10902" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV27" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc10" runat="server" CssClass="lblGrisTit" Height="17px" Text="10" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV28" runat="server" Columns="30" TabIndex="11001" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV29" runat="server" Columns="30" TabIndex="11002" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV30" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc11" runat="server" CssClass="lblGrisTit" Height="17px" Text="11" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV31" runat="server" Columns="30" TabIndex="11101" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV32" runat="server" Columns="30" TabIndex="11102" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV33" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc12" runat="server" CssClass="lblGrisTit" Height="17px" Text="12" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV34" runat="server" Columns="30" TabIndex="11201" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV35" runat="server" Columns="30" TabIndex="11202" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV36" runat="server" Columns="3" TabIndex="11203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc13" runat="server" CssClass="lblGrisTit" Height="17px" Text="13" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV37" runat="server" Columns="30" TabIndex="11301" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV38" runat="server" Columns="30" TabIndex="11302" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV39" runat="server" Columns="3" TabIndex="11303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc14" runat="server" CssClass="lblGrisTit" Height="17px" Text="14" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV40" runat="server" Columns="30" TabIndex="11401" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV41" runat="server" Columns="30" TabIndex="11402" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV42" runat="server" Columns="3" TabIndex="11403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc15" runat="server" CssClass="lblGrisTit" Text="15" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV43" runat="server" Columns="30" TabIndex="11501" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV44" runat="server" Columns="30" TabIndex="11502" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV45" runat="server" Columns="3" TabIndex="11503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                          
                                <asp:Label ID="lblLoc16" runat="server" CssClass="lblGrisTit" Text="16" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV46" runat="server" Columns="30" TabIndex="11601" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV47" runat="server" Columns="30" TabIndex="11602" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV48" runat="server" Columns="3" TabIndex="11603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                            <asp:Label ID="lblLoc17" runat="server" CssClass="lblGrisTit" Text="17" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV562" runat="server" Columns="30" TabIndex="11701" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV563" runat="server" Columns="30" TabIndex="11702" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV564" runat="server" Columns="3" TabIndex="11703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc18" runat="server" CssClass="lblGrisTit" Text="18" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV565" runat="server" Columns="30" TabIndex="11801" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV566" runat="server" Columns="30" TabIndex="11802" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV567" runat="server" Columns="3" TabIndex="11803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td >
                                </td>
                            <td >
                            </td>
                            <td class="linaBajo" >
                                <asp:Label ID="lblTotal1" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV49" runat="server" Columns="3" TabIndex="11961" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 6px;">
                                &nbsp;</td>
                        </tr>
                    </table>
                   
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('Identificacion_911_EI_NE_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Identificacion_911_EI_NE_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="OpenPageCharged('AG_911_EI_NE_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('AG_911_EI_NE_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
       
        <input id="hidDisparador" type="hidden" runat = "server" value="60" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }    
                
                function OpenPageCharged(ventana,ir){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV2'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV4'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV5'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV7'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV8'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV10'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV11'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV13'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV14'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV16'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV17'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV19'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV20'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV22'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV23'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV25'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV26'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV28'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV29'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV31'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV32'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV34'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV35'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV37'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV38'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV40'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV41'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV43'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV44'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV46'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV47'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV562'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV563'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV565'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV566'));
                    
                    openPage(ventana,ir); 
                }                 
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
