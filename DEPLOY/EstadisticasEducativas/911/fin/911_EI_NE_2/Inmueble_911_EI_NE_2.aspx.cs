using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
     
namespace EstadisticasEducativas._911.fin._911_EI_NE_2
{
    public partial class Inmueble_911_EI_NE_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion


                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";

                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                llenaRadios();

                LeerDatosInmueble(controlDP);

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }
        }

        private void llenaRadios()
        {
            rblPregunta1.Items.Add(new ListItem("SI", "SI"));
            rblPregunta1.Items.Add(new ListItem("NO", "NO"));

            rblPregunta2.Items.Add(new ListItem("SI", "SI"));
            rblPregunta2.Items.Add(new ListItem("NO", "NO"));

            rblPregunta3.Items.Add(new ListItem("SI", "SI"));
            rblPregunta3.Items.Add(new ListItem("NO", "NO"));

            rblPregunta4.Items.Add(new ListItem("SI", "SI"));
            rblPregunta4.Items.Add(new ListItem("NO", "NO"));

            ListItem item5s = new ListItem("SI", "SI");
            ListItem item5n = new ListItem("NO", "NO");
            item5s.Attributes.Add("onclick", "ActivarCaja(true);");
            item5n.Attributes.Add("onclick", "ActivarCaja(false);");
            rblPregunta5.Items.Add(item5s);
            rblPregunta5.Items.Add(item5n);
        }
        public void LeerDatosInmueble(ControlDP controlDP)
        {
            ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
            SEroot.WsEstadisticasEducativas.InmuebleDP inmuebleDP = wsEstadisticas.CargaCuestionarioInmueble(controlDP.ID_Control);
            if (inmuebleDP != null)
            {
                rblPregunta1.Items[0].Selected = inmuebleDP.a;
                rblPregunta1.Items[1].Selected = !inmuebleDP.a;

                rblPregunta2.Items[0].Selected = inmuebleDP.b;
                rblPregunta2.Items[1].Selected = !inmuebleDP.b;

                rblPregunta3.Items[0].Selected = inmuebleDP.c;
                rblPregunta3.Items[1].Selected = !inmuebleDP.c;

                rblPregunta4.Items[0].Selected = inmuebleDP.d;
                rblPregunta4.Items[1].Selected = !inmuebleDP.d;

                rblPregunta5.Items[0].Selected = inmuebleDP.e;
                rblPregunta5.Items[1].Selected = !inmuebleDP.e;

                txtEspecifique.Text = inmuebleDP.especifique;
            }

            if (controlDP.Estatus != 0)
            {
                rblPregunta1.Enabled = false;
                rblPregunta2.Enabled = false;
                rblPregunta3.Enabled = false;
                rblPregunta4.Enabled = false;
                rblPregunta5.Enabled = false;
                txtEspecifique.Enabled = false;
            }
        }

        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.GuardarDatosInmueble(HttpContext.Current,eventArgument);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************

    }
}
