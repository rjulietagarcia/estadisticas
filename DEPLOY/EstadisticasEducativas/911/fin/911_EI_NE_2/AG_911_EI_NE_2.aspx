<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-NE2(Alumnos)" AutoEventWireup="true" CodeBehind="AG_911_EI_NE_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_EI_NE_2.AG_911_EI_NE_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 20;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">


    <table style=" padding-left:200px;">
        <tr><td><span>EDUCACI�N INICIAL NO ESCOLARIZADA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_EI_NE_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Grupos_911_EI_NE_2',true)"><a href="#" title=""><span>GRUPOS</span></a></li>
        <li onclick="openPage('AG_911_EI_NE_2',true)"><a href="#" title="" class="activo"><span>NI�OS</span></a></li>
        <li onclick="openPage('Personal_911_EI_NE_2',false)"><a href="#" title=""><span>PADRES DE FAMILIA Y PERSONAL</span></a></li>
         <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROGRAMA</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ESPACIOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
        </ul>
    </div>    
   <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

    <center>
    
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

 
            
                    <table  style="text-align: center" >
                        <tr>
                            <td colspan="13" rowspan="1" style="text-align: left" >
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="II. NI�OS POR EDAD Y SEXO"
                                    Width="250px" Font-Size="16px"></asp:Label><br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba el n�mero total de ni�os atendidos al 4 de julio del presente a�o, seg�n su edad, desglos�ndolo por sexo, conforme al municipio o delegaci�n y la localidad o colonia."
                                    Width="850px"></asp:Label><br />
                            </td>
                            <td colspan="1" rowspan="1" style="width: 11px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="2" class="linaBajoAlto" >&nbsp;
                                </td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblMenos1" runat="server" CssClass="lblRojo" Text="MENOS DE 1 A�O" ></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lbl1A�o" runat="server" CssClass="lblRojo" Text="1 A�O" ></asp:Label></td>
                            <td colspan="2" rowspan="1" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lbl2A�os" runat="server" CssClass="lblRojo" Text="2 A�OS" ></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lbl3A�os" runat="server" CssClass="lblRojo" Text="3 A�OS" ></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lbl4A�os" runat="server" CssClass="lblRojo" Text="4 A�OS" ></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblTotal11" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                            <td class="Orila" colspan="1" style="width: 11px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�osM1" runat="server" CssClass="lblGrisTit" Text="NI�OS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�asM1" runat="server" CssClass="lblGrisTit" Text="NI�AS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�os1" runat="server" CssClass="lblGrisTit" Text="NI�OS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�as1" runat="server" CssClass="lblGrisTit" Text="NI�AS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�os2" runat="server" CssClass="lblGrisTit" Text="NI�OS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�as2" runat="server" CssClass="lblGrisTit" Text="NI�AS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�os3" runat="server" CssClass="lblGrisTit" Text="NI�OS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�as3" runat="server" CssClass="lblGrisTit" Text="NI�AS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�os4" runat="server" CssClass="lblGrisTit" Text="NI�OS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�as4" runat="server" CssClass="lblGrisTit" Text="NI�AS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�osTot" runat="server" CssClass="lblGrisTit" Text="NI�OS"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:Label ID="lblNi�asTot" runat="server" CssClass="lblGrisTit" Text="NI�AS"></asp:Label></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr1" runat="server">
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc1" runat="server" CssClass="lblGrisTit" Text="1" Height="17px" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV50" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV51" runat="server" Columns="3" TabIndex="10102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV52" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV53" runat="server" Columns="3" TabIndex="10104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV54" runat="server" Columns="3" TabIndex="10105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV55" runat="server" Columns="3" TabIndex="10106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV56" runat="server" Columns="3" TabIndex="10107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV57" runat="server" Columns="3" TabIndex="10108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV58" runat="server" Columns="3" TabIndex="10109" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV59" runat="server" Columns="3" TabIndex="10110" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV60" runat="server" Columns="3" TabIndex="10111" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV61" runat="server" Columns="3" TabIndex="10112" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr2" runat="server" >
                            <td class="linaBajo" >
                                <asp:Label ID="lblLoc2" runat="server" CssClass="lblGrisTit" Height="17px" Text="2" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV62" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV63" runat="server" Columns="3" TabIndex="10202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV64" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV65" runat="server" Columns="3" TabIndex="10204" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV66" runat="server" Columns="3" TabIndex="10205" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV67" runat="server" Columns="3" TabIndex="10206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV68" runat="server" Columns="3" TabIndex="10207" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV69" runat="server" Columns="3" TabIndex="10208" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV70" runat="server" Columns="3" TabIndex="10209" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV71" runat="server" Columns="3" TabIndex="10210" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV72" runat="server" Columns="3" TabIndex="10211" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV73" runat="server" Columns="3" TabIndex="10212" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr3" runat="server">
                            <td class="linaBajoS" >
                            <asp:Label ID="lblLoc3" runat="server" CssClass="lblGrisTit" Text="3" Height="17px" Width="67px"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV74" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV75" runat="server" Columns="3" TabIndex="10302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV76" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV77" runat="server" Columns="3" TabIndex="10304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV78" runat="server" Columns="3" TabIndex="10305" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV79" runat="server" Columns="3" TabIndex="10306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV80" runat="server" Columns="3" TabIndex="10307" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV81" runat="server" Columns="3" TabIndex="10308" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV82" runat="server" Columns="3" TabIndex="10309" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV83" runat="server" Columns="3" TabIndex="10310" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV84" runat="server" Columns="3" TabIndex="10311" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV85" runat="server" Columns="3" TabIndex="10312" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr4" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc4" runat="server" CssClass="lblGrisTit" Height="17px" Text="4" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV86" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV87" runat="server" Columns="3" TabIndex="10402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV88" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV89" runat="server" Columns="3" TabIndex="10404" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV90" runat="server" Columns="3" TabIndex="10405" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV91" runat="server" Columns="3" TabIndex="10406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV92" runat="server" Columns="3" TabIndex="10407" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV93" runat="server" Columns="3" TabIndex="10408" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV94" runat="server" Columns="3" TabIndex="10409" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV95" runat="server" Columns="3" TabIndex="10410" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV96" runat="server" Columns="3" TabIndex="10411" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV97" runat="server" Columns="3" TabIndex="10412" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr5" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc5" runat="server" CssClass="lblGrisTit" Height="17px" Text="5" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV98" runat="server" Columns="3" TabIndex="10501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV99" runat="server" Columns="3" TabIndex="10502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV100" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV101" runat="server" Columns="3" TabIndex="10504" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV102" runat="server" Columns="3" TabIndex="10505" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV103" runat="server" Columns="3" TabIndex="10506" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV104" runat="server" Columns="3" TabIndex="10507" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV105" runat="server" Columns="3" TabIndex="10508" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV106" runat="server" Columns="3" TabIndex="10509" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV107" runat="server" Columns="3" TabIndex="10510" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV108" runat="server" Columns="3" TabIndex="10511" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV109" runat="server" Columns="3" TabIndex="10512" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr6" runat="server">
                            <td rowspan="1" class="linaBajoS" >
                                <asp:Label ID="lblLoc6" runat="server" CssClass="lblGrisTit" Height="17px" Text="6" Width="67px"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV110" runat="server" Columns="3" TabIndex="10601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV111" runat="server" Columns="3" TabIndex="10602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV112" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV113" runat="server" Columns="3" TabIndex="10604" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV114" runat="server" Columns="3" TabIndex="10605" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV115" runat="server" Columns="3" TabIndex="10606" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV116" runat="server" Columns="3" TabIndex="10607" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV117" runat="server" Columns="3" TabIndex="10608" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV118" runat="server" Columns="3" TabIndex="10609" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV119" runat="server" Columns="3" TabIndex="10610" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV120" runat="server" Columns="3" TabIndex="10611" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV121" runat="server" Columns="3" TabIndex="10612" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr7" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc7" runat="server" CssClass="lblGrisTit" Height="17px" Text="7" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV122" runat="server" Columns="3" TabIndex="10701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV123" runat="server" Columns="3" TabIndex="10702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV124" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV125" runat="server" Columns="3" TabIndex="10704" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV126" runat="server" Columns="3" TabIndex="10705" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV127" runat="server" Columns="3" TabIndex="10706" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV128" runat="server" Columns="3" TabIndex="10707" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV129" runat="server" Columns="3" TabIndex="10708" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV130" runat="server" Columns="3" TabIndex="10709" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV131" runat="server" Columns="3" TabIndex="10710" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV132" runat="server" Columns="3" TabIndex="10711" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV133" runat="server" Columns="3" TabIndex="10712" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr8" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc8" runat="server" CssClass="lblGrisTit" Height="17px" Text="8"
                                    Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV134" runat="server" Columns="3" TabIndex="10801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV135" runat="server" Columns="3" TabIndex="10802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV136" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV137" runat="server" Columns="3" TabIndex="10804" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV138" runat="server" Columns="3" TabIndex="10805" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV139" runat="server" Columns="3" TabIndex="10806" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV140" runat="server" Columns="3" TabIndex="10807" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV141" runat="server" Columns="3" TabIndex="10808" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV142" runat="server" Columns="3" TabIndex="10809" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV143" runat="server" Columns="3" TabIndex="10810" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV144" runat="server" Columns="3" TabIndex="10811" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV145" runat="server" Columns="3" TabIndex="10812" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr9" runat="server">
                            <td rowspan="1" class="linaBajoS" >
                                <asp:Label ID="lblLoc9" runat="server" CssClass="lblGrisTit" Height="17px" Text="9" Width="67px"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV146" runat="server" Columns="3" TabIndex="10901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV147" runat="server" Columns="3" TabIndex="10902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV148" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV149" runat="server" Columns="3" TabIndex="10904" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV150" runat="server" Columns="3" TabIndex="10905" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV151" runat="server" Columns="3" TabIndex="10906" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV152" runat="server" Columns="3" TabIndex="10907" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV153" runat="server" Columns="3" TabIndex="10908" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV154" runat="server" Columns="3" TabIndex="10909" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV155" runat="server" Columns="3" TabIndex="10910" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV156" runat="server" Columns="3" TabIndex="10911" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV157" runat="server" Columns="3" TabIndex="10912" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr10" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc10" runat="server" CssClass="lblGrisTit" Height="17px" Text="10" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV158" runat="server" Columns="3" TabIndex="11001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV159" runat="server" Columns="3" TabIndex="11002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV160" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV161" runat="server" Columns="3" TabIndex="11004" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV162" runat="server" Columns="3" TabIndex="11005" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV163" runat="server" Columns="3" TabIndex="11006" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV164" runat="server" Columns="3" TabIndex="11007" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV165" runat="server" Columns="3" TabIndex="11008" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV166" runat="server" Columns="3" TabIndex="11009" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV167" runat="server" Columns="3" TabIndex="11010" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV168" runat="server" Columns="3" TabIndex="11011" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV169" runat="server" Columns="3" TabIndex="11012" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr11" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc11" runat="server" CssClass="lblGrisTit" Height="17px" Text="11" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV170" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV171" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV172" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV173" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV174" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV175" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV176" runat="server" Columns="3" TabIndex="11107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV177" runat="server" Columns="3" TabIndex="11108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV178" runat="server" Columns="3" TabIndex="11109" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV179" runat="server" Columns="3" TabIndex="11110" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV180" runat="server" Columns="3" TabIndex="11111" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV181" runat="server" Columns="3" TabIndex="11112" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr12" runat="server">
                            <td rowspan="1" class="linaBajoS" >
                                <asp:Label ID="lblLoc12" runat="server" CssClass="lblGrisTit" Height="17px" Text="12" Width="67px"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV182" runat="server" Columns="3" TabIndex="11201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV183" runat="server" Columns="3" TabIndex="11202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV184" runat="server" Columns="3" TabIndex="11203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV185" runat="server" Columns="3" TabIndex="11204" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV186" runat="server" Columns="3" TabIndex="11205" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV187" runat="server" Columns="3" TabIndex="11206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV188" runat="server" Columns="3" TabIndex="11207" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV189" runat="server" Columns="3" TabIndex="11208" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV190" runat="server" Columns="3" TabIndex="11209" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV191" runat="server" Columns="3" TabIndex="11210" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV192" runat="server" Columns="3" TabIndex="11211" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV193" runat="server" Columns="3" TabIndex="11212" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr13" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc13" runat="server" CssClass="lblGrisTit" Height="17px" Text="13" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV194" runat="server" Columns="3" TabIndex="11301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV195" runat="server" Columns="3" TabIndex="11302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV196" runat="server" Columns="3" TabIndex="11303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV197" runat="server" Columns="3" TabIndex="11304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV198" runat="server" Columns="3" TabIndex="11305" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV199" runat="server" Columns="3" TabIndex="11306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV200" runat="server" Columns="3" TabIndex="11307" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV201" runat="server" Columns="3" TabIndex="11308" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV202" runat="server" Columns="3" TabIndex="11309" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV203" runat="server" Columns="3" TabIndex="11310" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV204" runat="server" Columns="3" TabIndex="11311" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV205" runat="server" Columns="3" TabIndex="11312" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr14" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc14" runat="server" CssClass="lblGrisTit" Height="17px" Text="14" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV206" runat="server" Columns="3" TabIndex="11401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV207" runat="server" Columns="3" TabIndex="11402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV208" runat="server" Columns="3" TabIndex="11403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV209" runat="server" Columns="3" TabIndex="11404" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV210" runat="server" Columns="3" TabIndex="11405" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV211" runat="server" Columns="3" TabIndex="11406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV212" runat="server" Columns="3" TabIndex="11407" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV213" runat="server" Columns="3" TabIndex="11408" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV214" runat="server" Columns="3" TabIndex="11409" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV215" runat="server" Columns="3" TabIndex="11410" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV216" runat="server" Columns="3" TabIndex="11411" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV217" runat="server" Columns="3" TabIndex="11412" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr15" runat="server">
                            <td rowspan="1" class="linaBajoS" >
                                <asp:Label ID="lblLoc15" runat="server" CssClass="lblGrisTit" Text="15" Width="67px"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV218" runat="server" Columns="3" TabIndex="11501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV219" runat="server" Columns="3" TabIndex="11502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV220" runat="server" Columns="3" TabIndex="11503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV221" runat="server" Columns="3" TabIndex="11504" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV222" runat="server" Columns="3" TabIndex="11505" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV223" runat="server" Columns="3" TabIndex="11506" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV224" runat="server" Columns="3" TabIndex="11507" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV225" runat="server" Columns="3" TabIndex="11508" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV226" runat="server" Columns="3" TabIndex="11509" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV227" runat="server" Columns="3" TabIndex="11510" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV228" runat="server" Columns="3" TabIndex="11511" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV229" runat="server" Columns="3" TabIndex="11512" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr16" runat="server">
                            <td rowspan="1" class="linaBajo" >
                                <asp:Label ID="lblLoc16" runat="server" CssClass="lblGrisTit" Text="16" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV230" runat="server" Columns="3" TabIndex="11601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV231" runat="server" Columns="3" TabIndex="11602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV232" runat="server" Columns="3" TabIndex="11603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV233" runat="server" Columns="3" TabIndex="11604" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV234" runat="server" Columns="3" TabIndex="11605" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV235" runat="server" Columns="3" TabIndex="11606" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV236" runat="server" Columns="3" TabIndex="11607" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV237" runat="server" Columns="3" TabIndex="11608" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV238" runat="server" Columns="3" TabIndex="11609" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV239" runat="server" Columns="3" TabIndex="11610" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV240" runat="server" Columns="3" TabIndex="11611" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV241" runat="server" Columns="3" TabIndex="11612" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr17" runat="server">
                            <td rowspan="1" class="linaBajo" >
                            <asp:Label ID="lblLoc17" runat="server" CssClass="lblGrisTit" Text="17" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV568" runat="server" Columns="3" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV569" runat="server" Columns="3" TabIndex="11702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV570" runat="server" Columns="3" TabIndex="11703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV571" runat="server" Columns="3" TabIndex="11704" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV572" runat="server" Columns="3" TabIndex="11705" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV573" runat="server" Columns="3" TabIndex="11706" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV574" runat="server" Columns="3" TabIndex="11707" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV575" runat="server" Columns="3" TabIndex="11708" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV576" runat="server" Columns="3" TabIndex="11709" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV577" runat="server" Columns="3" TabIndex="11710" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV578" runat="server" Columns="3" TabIndex="11711" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV579" runat="server" Columns="3" TabIndex="11712" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr18" runat="server" >
                            <td rowspan="1" class="linaBajoS" >
                                <asp:Label ID="lblLoc18" runat="server" CssClass="lblGrisTit" Text="18" Width="67px"></asp:Label></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV580" runat="server" Columns="3" TabIndex="11801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV581" runat="server" Columns="3" TabIndex="11802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV582" runat="server" Columns="3" TabIndex="11803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV583" runat="server" Columns="3" TabIndex="11804" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV584" runat="server" Columns="3" TabIndex="11805" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV585" runat="server" Columns="3" TabIndex="11806" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV586" runat="server" Columns="3" TabIndex="11807" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV587" runat="server" Columns="3" TabIndex="11808" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV588" runat="server" Columns="3" TabIndex="11809" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV589" runat="server" Columns="3" TabIndex="11810" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoE" >
                                <asp:TextBox ID="txtV590" runat="server" Columns="3" TabIndex="11811" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoS" >
                                <asp:TextBox ID="txtV591" runat="server" Columns="3" TabIndex="11812" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 67px; height: 26px; text-align: left" class="linaBajo">
                                <asp:Label ID="lblTotal12" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV242" runat="server" Columns="3" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV243" runat="server" Columns="3" TabIndex="11902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV244" runat="server" Columns="3" TabIndex="11903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV245" runat="server" Columns="3" TabIndex="11904" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV246" runat="server" Columns="3" TabIndex="11905" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV247" runat="server" Columns="3" TabIndex="11906" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV248" runat="server" Columns="3" TabIndex="11907" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV249" runat="server" Columns="3" TabIndex="11908" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV250" runat="server" Columns="3" TabIndex="11909" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV251" runat="server" Columns="3" TabIndex="11910" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajoV" >
                                <asp:TextBox ID="txtV252" runat="server" Columns="3" TabIndex="11911" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV253" runat="server" Columns="3" TabIndex="11912" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" style="width: 11px">
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                 
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Grupos_911_EI_NE_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Grupos_911_EI_NE_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal_911_EI_NE_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_EI_NE_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

    
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
       
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
