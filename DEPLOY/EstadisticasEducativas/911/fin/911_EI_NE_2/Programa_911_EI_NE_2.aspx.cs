using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
     
namespace EstadisticasEducativas._911.fin._911_EI_NE_2
{
    public partial class Programa_911_EI_NE_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV303.Attributes["onkeypress"] = "return Num(event)";
                txtV304.Attributes["onkeypress"] = "return Num(event)";
                txtV305.Attributes["onkeypress"] = "return Num(event)";
                txtV306.Attributes["onkeypress"] = "return Num(event)";
                txtV307.Attributes["onkeypress"] = "return Num(event)";
                txtV308.Attributes["onkeypress"] = "return Num(event)";
                txtV309.Attributes["onkeypress"] = "return Num(event)";
                txtV310.Attributes["onkeypress"] = "return Num(event)";
                txtV311.Attributes["onkeypress"] = "return Num(event)";
                txtV312.Attributes["onkeypress"] = "return Num(event)";
                txtV313.Attributes["onkeypress"] = "return Num(event)";
                txtV314.Attributes["onkeypress"] = "return Num(event)";
                txtV315.Attributes["onkeypress"] = "return Num(event)";
                txtV316.Attributes["onkeypress"] = "return Num(event)";
                txtV317.Attributes["onkeypress"] = "return Num(event)";
                txtV318.Attributes["onkeypress"] = "return Num(event)";
                txtV319.Attributes["onkeypress"] = "return Num(event)";
                txtV320.Attributes["onkeypress"] = "return Num(event)";
                txtV321.Attributes["onkeypress"] = "return Num(event)";
                txtV322.Attributes["onkeypress"] = "return Num(event)";
                txtV323.Attributes["onkeypress"] = "return Num(event)";
                txtV324.Attributes["onkeypress"] = "return Num(event)";
                txtV325.Attributes["onkeypress"] = "return Num(event)";
                txtV326.Attributes["onkeypress"] = "return Num(event)";
                txtV327.Attributes["onkeypress"] = "return Num(event)";
                txtV328.Attributes["onkeypress"] = "return Num(event)";
                txtV329.Attributes["onkeypress"] = "return Num(event)";
                txtV330.Attributes["onkeypress"] = "return Num(event)";
                txtV331.Attributes["onkeypress"] = "return Num(event)";
                txtV332.Attributes["onkeypress"] = "return Num(event)";
                txtV333.Attributes["onkeypress"] = "return Num(event)";
                txtV334.Attributes["onkeypress"] = "return Num(event)";
                txtV335.Attributes["onkeypress"] = "return Num(event)";
                txtV336.Attributes["onkeypress"] = "return Num(event)";
                txtV337.Attributes["onkeypress"] = "return Num(event)";
                txtV338.Attributes["onkeypress"] = "return Num(event)";
                txtV339.Attributes["onkeypress"] = "return Num(event)";
                txtV340.Attributes["onkeypress"] = "return Num(event)";
                txtV341.Attributes["onkeypress"] = "return Num(event)";
                txtV342.Attributes["onkeypress"] = "return Num(event)";
                txtV343.Attributes["onkeypress"] = "return Num(event)";
                txtV344.Attributes["onkeypress"] = "return Num(event)";
                txtV345.Attributes["onkeypress"] = "return Num(event)";
                txtV346.Attributes["onkeypress"] = "return Num(event)";
                txtV347.Attributes["onkeypress"] = "return Num(event)";
                txtV348.Attributes["onkeypress"] = "return Num(event)";
                txtV349.Attributes["onkeypress"] = "return Num(event)";
                txtV350.Attributes["onkeypress"] = "return Num(event)";
                txtV351.Attributes["onkeypress"] = "return Num(event)";
                txtV352.Attributes["onkeypress"] = "return Num(event)";
                txtV353.Attributes["onkeypress"] = "return Num(event)";
                txtV354.Attributes["onkeypress"] = "return Num(event)";
                txtV355.Attributes["onkeypress"] = "return Num(event)";
                txtV356.Attributes["onkeypress"] = "return Num(event)";
                txtV357.Attributes["onkeypress"] = "return Num(event)";
                txtV358.Attributes["onkeypress"] = "return Num(event)";
                txtV359.Attributes["onkeypress"] = "return Num(event)";
                txtV360.Attributes["onkeypress"] = "return Num(event)";
                txtV361.Attributes["onkeypress"] = "return Num(event)";
                txtV362.Attributes["onkeypress"] = "return Num(event)";
                txtV363.Attributes["onkeypress"] = "return Num(event)";
                txtV364.Attributes["onkeypress"] = "return Num(event)";
                txtV365.Attributes["onkeypress"] = "return Num(event)";
                txtV366.Attributes["onkeypress"] = "return Num(event)";
                txtV367.Attributes["onkeypress"] = "return Num(event)";
                txtV368.Attributes["onkeypress"] = "return Num(event)";
                txtV369.Attributes["onkeypress"] = "return Num(event)";
                txtV370.Attributes["onkeypress"] = "return Num(event)";
                txtV371.Attributes["onkeypress"] = "return Num(event)";
                txtV372.Attributes["onkeypress"] = "return Num(event)";
                txtV373.Attributes["onkeypress"] = "return Num(event)";
                txtV374.Attributes["onkeypress"] = "return Num(event)";
                txtV375.Attributes["onkeypress"] = "return Num(event)";
                txtV376.Attributes["onkeypress"] = "return Num(event)";
                txtV377.Attributes["onkeypress"] = "return Num(event)";
                txtV378.Attributes["onkeypress"] = "return Num(event)";
                txtV379.Attributes["onkeypress"] = "return Num(event)";
                txtV380.Attributes["onkeypress"] = "return Num(event)";
                txtV381.Attributes["onkeypress"] = "return Num(event)";
                txtV382.Attributes["onkeypress"] = "return Num(event)";
                txtV383.Attributes["onkeypress"] = "return Num(event)";
                txtV384.Attributes["onkeypress"] = "return Num(event)";
                txtV385.Attributes["onkeypress"] = "return Num(event)";
                txtV386.Attributes["onkeypress"] = "return Num(event)";
                txtV387.Attributes["onkeypress"] = "return Num(event)";
                txtV388.Attributes["onkeypress"] = "return Num(event)";
                txtV389.Attributes["onkeypress"] = "return Num(event)";
                txtV390.Attributes["onkeypress"] = "return Num(event)";
                txtV391.Attributes["onkeypress"] = "return Num(event)";
                txtV392.Attributes["onkeypress"] = "return Num(event)";
                txtV393.Attributes["onkeypress"] = "return Num(event)";
                txtV394.Attributes["onkeypress"] = "return Num(event)";
                txtV395.Attributes["onkeypress"] = "return Num(event)";
                txtV396.Attributes["onkeypress"] = "return Num(event)";
                txtV397.Attributes["onkeypress"] = "return Num(event)";
                txtV398.Attributes["onkeypress"] = "return Num(event)";
                txtV399.Attributes["onkeypress"] = "return Num(event)";
                txtV400.Attributes["onkeypress"] = "return Num(event)";
                txtV401.Attributes["onkeypress"] = "return Num(event)";
                txtV402.Attributes["onkeypress"] = "return Num(event)";
                txtV403.Attributes["onkeypress"] = "return Num(event)";
                txtV404.Attributes["onkeypress"] = "return Num(event)";
                txtV405.Attributes["onkeypress"] = "return Num(event)";
                txtV406.Attributes["onkeypress"] = "return Num(event)";
                txtV407.Attributes["onkeypress"] = "return Num(event)";
                txtV408.Attributes["onkeypress"] = "return Num(event)";
                txtV409.Attributes["onkeypress"] = "return Num(event)";
                txtV410.Attributes["onkeypress"] = "return Num(event)";
                txtV411.Attributes["onkeypress"] = "return Num(event)";
                txtV412.Attributes["onkeypress"] = "return Num(event)";
                txtV413.Attributes["onkeypress"] = "return Num(event)";
                txtV414.Attributes["onkeypress"] = "return Num(event)";
                txtV705.Attributes["onkeypress"] = "return Num(event)";
                txtV706.Attributes["onkeypress"] = "return Num(event)";
                txtV707.Attributes["onkeypress"] = "return Num(event)";
                txtV708.Attributes["onkeypress"] = "return Num(event)";
                txtV709.Attributes["onkeypress"] = "return Num(event)";
                txtV710.Attributes["onkeypress"] = "return Num(event)";
                txtV711.Attributes["onkeypress"] = "return Num(event)";
                txtV712.Attributes["onkeypress"] = "return Num(event)";
                txtV713.Attributes["onkeypress"] = "return Num(event)";
                txtV714.Attributes["onkeypress"] = "return Num(event)";
                txtV715.Attributes["onkeypress"] = "return Num(event)";
                txtV716.Attributes["onkeypress"] = "return Num(event)";
                txtV717.Attributes["onkeypress"] = "return Num(event)";
                txtV718.Attributes["onkeypress"] = "return Num(event)";
                txtV415.Attributes["onkeypress"] = "return Num(event)";
                txtV416.Attributes["onkeypress"] = "return Num(event)";
                txtV417.Attributes["onkeypress"] = "return Num(event)";
                txtV418.Attributes["onkeypress"] = "return Num(event)";
                txtV419.Attributes["onkeypress"] = "return Num(event)";
                txtV420.Attributes["onkeypress"] = "return Num(event)";
                txtV421.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;

                //ActivarTRs(controlDP);
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************
        void ActivarTRs(ControlDP controlDP)
        {
            ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
            string lista = "txtV3,3|txtV6,6|txtV9,9|txtV12,12|txtV15,15|txtV18,18|txtV21,21|txtV24,24|txtV27,27|txtV30,30|txtV33,33|txtV36,36|txtV39,39|txtV42,42|txtV45,45|txtV48,48|txtV564,564|txtV567,567|";
            string[] Cajas = lista.Split('|');

            System.Text.StringBuilder listaNumerosVariables = new System.Text.StringBuilder();
            for (int NoVariable = 0; NoVariable < Cajas.Length - 1; NoVariable++)
            {
                if (Cajas[NoVariable].Split(',')[1] != "0")
                    listaNumerosVariables.Append(Cajas[NoVariable].Split(',')[1]);
                if (NoVariable < Cajas.Length - 2 && listaNumerosVariables.Length > 0)
                    listaNumerosVariables.Append(",");
            }
            VariablesDP[] lista_VariableDP = wsEstadisiticas.LeerVariableEncuesta_Lista(controlDP, Class911.Doc_Cuestionario, 0, listaNumerosVariables.ToString());

            foreach (VariablesDP varDP in lista_VariableDP)
            {
                int x = 0;
                if (varDP.valor == DBNull.Value)
                    varDP.valor = x;
                if (varDP.valor.ToString() == "0")
                {

                    switch (varDP.var)
                    {
                        case 3:
                            txt_disabled(ref tr1);
                            break;
                        case 6:
                            txt_disabled(ref tr2);
                            break;
                        case 9:
                            txt_disabled(ref tr3);
                            break;
                        case 12:
                            txt_disabled(ref tr4);
                            break;
                        case 15:
                            txt_disabled(ref tr5);
                            break;
                        case 18:
                            txt_disabled(ref tr6);
                            break;
                        case 21:
                            txt_disabled(ref tr7);
                            break;
                        case 24:
                            txt_disabled(ref tr8);
                            break;
                        case 27:
                            txt_disabled(ref tr9);
                            break;
                        case 30:
                            txt_disabled(ref tr10);
                            break;
                        case 33:
                            txt_disabled(ref tr11);
                            break;
                        case 36:
                            txt_disabled(ref tr12);
                            break;
                        case 39:
                            txt_disabled(ref tr13);
                            break;
                        case 42:
                            txt_disabled(ref tr14);
                            break;
                        case 45:
                            txt_disabled(ref tr15);
                            break;
                        case 48:
                            txt_disabled(ref tr16);
                            break;
                        case 564:
                            txt_disabled(ref tr17);
                            break;
                        case 567:
                            txt_disabled(ref tr18);
                            break;
                    }
                }
            }
        }
        void txt_disabled(ref HtmlTableRow row)
        {
            row.Disabled = true;
            for (int i = 1; i < row.Cells.Count - 1; i++)
            {
                Control control = row.Cells[i].Controls[1];
                if (control.ID.Substring(0, 4) == "txtV")
                {
                    TextBox txt = (TextBox)control;
                    txt.ReadOnly = true;
                }
            }
        }
    }
}
