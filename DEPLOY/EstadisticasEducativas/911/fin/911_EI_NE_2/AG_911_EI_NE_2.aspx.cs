using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
     
namespace EstadisticasEducativas._911.fin._911_EI_NE_2
{
    public partial class AG_911_EI_NE_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();


        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV50.Attributes["onkeypress"] = "return Num(event)";
                txtV51.Attributes["onkeypress"] = "return Num(event)";
                txtV52.Attributes["onkeypress"] = "return Num(event)";
                txtV53.Attributes["onkeypress"] = "return Num(event)";
                txtV54.Attributes["onkeypress"] = "return Num(event)";
                txtV55.Attributes["onkeypress"] = "return Num(event)";
                txtV56.Attributes["onkeypress"] = "return Num(event)";
                txtV57.Attributes["onkeypress"] = "return Num(event)";
                txtV58.Attributes["onkeypress"] = "return Num(event)";
                txtV59.Attributes["onkeypress"] = "return Num(event)";
                txtV60.Attributes["onkeypress"] = "return Num(event)";
                txtV61.Attributes["onkeypress"] = "return Num(event)";
                txtV62.Attributes["onkeypress"] = "return Num(event)";
                txtV63.Attributes["onkeypress"] = "return Num(event)";
                txtV64.Attributes["onkeypress"] = "return Num(event)";
                txtV65.Attributes["onkeypress"] = "return Num(event)";
                txtV66.Attributes["onkeypress"] = "return Num(event)";
                txtV67.Attributes["onkeypress"] = "return Num(event)";
                txtV68.Attributes["onkeypress"] = "return Num(event)";
                txtV69.Attributes["onkeypress"] = "return Num(event)";
                txtV70.Attributes["onkeypress"] = "return Num(event)";
                txtV71.Attributes["onkeypress"] = "return Num(event)";
                txtV72.Attributes["onkeypress"] = "return Num(event)";
                txtV73.Attributes["onkeypress"] = "return Num(event)";
                txtV74.Attributes["onkeypress"] = "return Num(event)";
                txtV75.Attributes["onkeypress"] = "return Num(event)";
                txtV76.Attributes["onkeypress"] = "return Num(event)";
                txtV77.Attributes["onkeypress"] = "return Num(event)";
                txtV78.Attributes["onkeypress"] = "return Num(event)";
                txtV79.Attributes["onkeypress"] = "return Num(event)";
                txtV80.Attributes["onkeypress"] = "return Num(event)";
                txtV81.Attributes["onkeypress"] = "return Num(event)";
                txtV82.Attributes["onkeypress"] = "return Num(event)";
                txtV83.Attributes["onkeypress"] = "return Num(event)";
                txtV84.Attributes["onkeypress"] = "return Num(event)";
                txtV85.Attributes["onkeypress"] = "return Num(event)";
                txtV86.Attributes["onkeypress"] = "return Num(event)";
                txtV87.Attributes["onkeypress"] = "return Num(event)";
                txtV88.Attributes["onkeypress"] = "return Num(event)";
                txtV89.Attributes["onkeypress"] = "return Num(event)";
                txtV90.Attributes["onkeypress"] = "return Num(event)";
                txtV91.Attributes["onkeypress"] = "return Num(event)";
                txtV92.Attributes["onkeypress"] = "return Num(event)";
                txtV93.Attributes["onkeypress"] = "return Num(event)";
                txtV94.Attributes["onkeypress"] = "return Num(event)";
                txtV95.Attributes["onkeypress"] = "return Num(event)";
                txtV96.Attributes["onkeypress"] = "return Num(event)";
                txtV97.Attributes["onkeypress"] = "return Num(event)";
                txtV98.Attributes["onkeypress"] = "return Num(event)";
                txtV99.Attributes["onkeypress"] = "return Num(event)";
                txtV100.Attributes["onkeypress"] = "return Num(event)";
                txtV101.Attributes["onkeypress"] = "return Num(event)";
                txtV102.Attributes["onkeypress"] = "return Num(event)";
                txtV103.Attributes["onkeypress"] = "return Num(event)";
                txtV104.Attributes["onkeypress"] = "return Num(event)";
                txtV105.Attributes["onkeypress"] = "return Num(event)";
                txtV106.Attributes["onkeypress"] = "return Num(event)";
                txtV107.Attributes["onkeypress"] = "return Num(event)";
                txtV108.Attributes["onkeypress"] = "return Num(event)";
                txtV109.Attributes["onkeypress"] = "return Num(event)";
                txtV110.Attributes["onkeypress"] = "return Num(event)";
                txtV111.Attributes["onkeypress"] = "return Num(event)";
                txtV112.Attributes["onkeypress"] = "return Num(event)";
                txtV113.Attributes["onkeypress"] = "return Num(event)";
                txtV114.Attributes["onkeypress"] = "return Num(event)";
                txtV115.Attributes["onkeypress"] = "return Num(event)";
                txtV116.Attributes["onkeypress"] = "return Num(event)";
                txtV117.Attributes["onkeypress"] = "return Num(event)";
                txtV118.Attributes["onkeypress"] = "return Num(event)";
                txtV119.Attributes["onkeypress"] = "return Num(event)";
                txtV120.Attributes["onkeypress"] = "return Num(event)";
                txtV121.Attributes["onkeypress"] = "return Num(event)";
                txtV122.Attributes["onkeypress"] = "return Num(event)";
                txtV123.Attributes["onkeypress"] = "return Num(event)";
                txtV124.Attributes["onkeypress"] = "return Num(event)";
                txtV125.Attributes["onkeypress"] = "return Num(event)";
                txtV126.Attributes["onkeypress"] = "return Num(event)";
                txtV127.Attributes["onkeypress"] = "return Num(event)";
                txtV128.Attributes["onkeypress"] = "return Num(event)";
                txtV129.Attributes["onkeypress"] = "return Num(event)";
                txtV130.Attributes["onkeypress"] = "return Num(event)";
                txtV131.Attributes["onkeypress"] = "return Num(event)";
                txtV132.Attributes["onkeypress"] = "return Num(event)";
                txtV133.Attributes["onkeypress"] = "return Num(event)";
                txtV134.Attributes["onkeypress"] = "return Num(event)";
                txtV135.Attributes["onkeypress"] = "return Num(event)";
                txtV136.Attributes["onkeypress"] = "return Num(event)";
                txtV137.Attributes["onkeypress"] = "return Num(event)";
                txtV138.Attributes["onkeypress"] = "return Num(event)";
                txtV139.Attributes["onkeypress"] = "return Num(event)";
                txtV140.Attributes["onkeypress"] = "return Num(event)";
                txtV141.Attributes["onkeypress"] = "return Num(event)";
                txtV142.Attributes["onkeypress"] = "return Num(event)";
                txtV143.Attributes["onkeypress"] = "return Num(event)";
                txtV144.Attributes["onkeypress"] = "return Num(event)";
                txtV145.Attributes["onkeypress"] = "return Num(event)";
                txtV146.Attributes["onkeypress"] = "return Num(event)";
                txtV147.Attributes["onkeypress"] = "return Num(event)";
                txtV148.Attributes["onkeypress"] = "return Num(event)";
                txtV149.Attributes["onkeypress"] = "return Num(event)";
                txtV150.Attributes["onkeypress"] = "return Num(event)";
                txtV151.Attributes["onkeypress"] = "return Num(event)";
                txtV152.Attributes["onkeypress"] = "return Num(event)";
                txtV153.Attributes["onkeypress"] = "return Num(event)";
                txtV154.Attributes["onkeypress"] = "return Num(event)";
                txtV155.Attributes["onkeypress"] = "return Num(event)";
                txtV156.Attributes["onkeypress"] = "return Num(event)";
                txtV157.Attributes["onkeypress"] = "return Num(event)";
                txtV158.Attributes["onkeypress"] = "return Num(event)";
                txtV159.Attributes["onkeypress"] = "return Num(event)";
                txtV160.Attributes["onkeypress"] = "return Num(event)";
                txtV161.Attributes["onkeypress"] = "return Num(event)";
                txtV162.Attributes["onkeypress"] = "return Num(event)";
                txtV163.Attributes["onkeypress"] = "return Num(event)";
                txtV164.Attributes["onkeypress"] = "return Num(event)";
                txtV165.Attributes["onkeypress"] = "return Num(event)";
                txtV166.Attributes["onkeypress"] = "return Num(event)";
                txtV167.Attributes["onkeypress"] = "return Num(event)";
                txtV168.Attributes["onkeypress"] = "return Num(event)";
                txtV169.Attributes["onkeypress"] = "return Num(event)";
                txtV170.Attributes["onkeypress"] = "return Num(event)";
                txtV171.Attributes["onkeypress"] = "return Num(event)";
                txtV172.Attributes["onkeypress"] = "return Num(event)";
                txtV173.Attributes["onkeypress"] = "return Num(event)";
                txtV174.Attributes["onkeypress"] = "return Num(event)";
                txtV175.Attributes["onkeypress"] = "return Num(event)";
                txtV176.Attributes["onkeypress"] = "return Num(event)";
                txtV177.Attributes["onkeypress"] = "return Num(event)";
                txtV178.Attributes["onkeypress"] = "return Num(event)";
                txtV179.Attributes["onkeypress"] = "return Num(event)";
                txtV180.Attributes["onkeypress"] = "return Num(event)";
                txtV181.Attributes["onkeypress"] = "return Num(event)";
                txtV182.Attributes["onkeypress"] = "return Num(event)";
                txtV183.Attributes["onkeypress"] = "return Num(event)";
                txtV184.Attributes["onkeypress"] = "return Num(event)";
                txtV185.Attributes["onkeypress"] = "return Num(event)";
                txtV186.Attributes["onkeypress"] = "return Num(event)";
                txtV187.Attributes["onkeypress"] = "return Num(event)";
                txtV188.Attributes["onkeypress"] = "return Num(event)";
                txtV189.Attributes["onkeypress"] = "return Num(event)";
                txtV190.Attributes["onkeypress"] = "return Num(event)";
                txtV191.Attributes["onkeypress"] = "return Num(event)";
                txtV192.Attributes["onkeypress"] = "return Num(event)";
                txtV193.Attributes["onkeypress"] = "return Num(event)";
                txtV194.Attributes["onkeypress"] = "return Num(event)";
                txtV195.Attributes["onkeypress"] = "return Num(event)";
                txtV196.Attributes["onkeypress"] = "return Num(event)";
                txtV197.Attributes["onkeypress"] = "return Num(event)";
                txtV198.Attributes["onkeypress"] = "return Num(event)";
                txtV199.Attributes["onkeypress"] = "return Num(event)";
                txtV200.Attributes["onkeypress"] = "return Num(event)";
                txtV201.Attributes["onkeypress"] = "return Num(event)";
                txtV202.Attributes["onkeypress"] = "return Num(event)";
                txtV203.Attributes["onkeypress"] = "return Num(event)";
                txtV204.Attributes["onkeypress"] = "return Num(event)";
                txtV205.Attributes["onkeypress"] = "return Num(event)";
                txtV206.Attributes["onkeypress"] = "return Num(event)";
                txtV207.Attributes["onkeypress"] = "return Num(event)";
                txtV208.Attributes["onkeypress"] = "return Num(event)";
                txtV209.Attributes["onkeypress"] = "return Num(event)";
                txtV210.Attributes["onkeypress"] = "return Num(event)";
                txtV211.Attributes["onkeypress"] = "return Num(event)";
                txtV212.Attributes["onkeypress"] = "return Num(event)";
                txtV213.Attributes["onkeypress"] = "return Num(event)";
                txtV214.Attributes["onkeypress"] = "return Num(event)";
                txtV215.Attributes["onkeypress"] = "return Num(event)";
                txtV216.Attributes["onkeypress"] = "return Num(event)";
                txtV217.Attributes["onkeypress"] = "return Num(event)";
                txtV218.Attributes["onkeypress"] = "return Num(event)";
                txtV219.Attributes["onkeypress"] = "return Num(event)";
                txtV220.Attributes["onkeypress"] = "return Num(event)";
                txtV221.Attributes["onkeypress"] = "return Num(event)";
                txtV222.Attributes["onkeypress"] = "return Num(event)";
                txtV223.Attributes["onkeypress"] = "return Num(event)";
                txtV224.Attributes["onkeypress"] = "return Num(event)";
                txtV225.Attributes["onkeypress"] = "return Num(event)";
                txtV226.Attributes["onkeypress"] = "return Num(event)";
                txtV227.Attributes["onkeypress"] = "return Num(event)";
                txtV228.Attributes["onkeypress"] = "return Num(event)";
                txtV229.Attributes["onkeypress"] = "return Num(event)";
                txtV230.Attributes["onkeypress"] = "return Num(event)";
                txtV231.Attributes["onkeypress"] = "return Num(event)";
                txtV232.Attributes["onkeypress"] = "return Num(event)";
                txtV233.Attributes["onkeypress"] = "return Num(event)";
                txtV234.Attributes["onkeypress"] = "return Num(event)";
                txtV235.Attributes["onkeypress"] = "return Num(event)";
                txtV236.Attributes["onkeypress"] = "return Num(event)";
                txtV237.Attributes["onkeypress"] = "return Num(event)";
                txtV238.Attributes["onkeypress"] = "return Num(event)";
                txtV239.Attributes["onkeypress"] = "return Num(event)";
                txtV240.Attributes["onkeypress"] = "return Num(event)";
                txtV241.Attributes["onkeypress"] = "return Num(event)";
                txtV568.Attributes["onkeypress"] = "return Num(event)";
                txtV569.Attributes["onkeypress"] = "return Num(event)";
                txtV570.Attributes["onkeypress"] = "return Num(event)";
                txtV571.Attributes["onkeypress"] = "return Num(event)";
                txtV572.Attributes["onkeypress"] = "return Num(event)";
                txtV573.Attributes["onkeypress"] = "return Num(event)";
                txtV574.Attributes["onkeypress"] = "return Num(event)";
                txtV575.Attributes["onkeypress"] = "return Num(event)";
                txtV576.Attributes["onkeypress"] = "return Num(event)";
                txtV577.Attributes["onkeypress"] = "return Num(event)";
                txtV578.Attributes["onkeypress"] = "return Num(event)";
                txtV579.Attributes["onkeypress"] = "return Num(event)";
                txtV580.Attributes["onkeypress"] = "return Num(event)";
                txtV581.Attributes["onkeypress"] = "return Num(event)";
                txtV582.Attributes["onkeypress"] = "return Num(event)";
                txtV583.Attributes["onkeypress"] = "return Num(event)";
                txtV584.Attributes["onkeypress"] = "return Num(event)";
                txtV585.Attributes["onkeypress"] = "return Num(event)";
                txtV586.Attributes["onkeypress"] = "return Num(event)";
                txtV587.Attributes["onkeypress"] = "return Num(event)";
                txtV588.Attributes["onkeypress"] = "return Num(event)";
                txtV589.Attributes["onkeypress"] = "return Num(event)";
                txtV590.Attributes["onkeypress"] = "return Num(event)";
                txtV591.Attributes["onkeypress"] = "return Num(event)";
                txtV242.Attributes["onkeypress"] = "return Num(event)";
                txtV243.Attributes["onkeypress"] = "return Num(event)";
                txtV244.Attributes["onkeypress"] = "return Num(event)";
                txtV245.Attributes["onkeypress"] = "return Num(event)";
                txtV246.Attributes["onkeypress"] = "return Num(event)";
                txtV247.Attributes["onkeypress"] = "return Num(event)";
                txtV248.Attributes["onkeypress"] = "return Num(event)";
                txtV249.Attributes["onkeypress"] = "return Num(event)";
                txtV250.Attributes["onkeypress"] = "return Num(event)";
                txtV251.Attributes["onkeypress"] = "return Num(event)";
                txtV252.Attributes["onkeypress"] = "return Num(event)";
                txtV253.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

               
                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this,controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP,1,0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
                //ActivarTRs(controlDP);
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************

        void ActivarTRs(ControlDP controlDP)
        {
             ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
             string lista = "txtV3,3|txtV6,6|txtV9,9|txtV12,12|txtV15,15|txtV18,18|txtV21,21|txtV24,24|txtV27,27|txtV30,30|txtV33,33|txtV36,36|txtV39,39|txtV42,42|txtV45,45|txtV48,48|txtV564,564|txtV567,567|";
             string[] Cajas = lista.Split('|');

             System.Text.StringBuilder listaNumerosVariables = new System.Text.StringBuilder();
            for (int NoVariable = 0; NoVariable < Cajas.Length - 1; NoVariable++)
            {
                if (Cajas[NoVariable].Split(',')[1] != "0")
                    listaNumerosVariables.Append(Cajas[NoVariable].Split(',')[1]);
                if (NoVariable < Cajas.Length - 2 && listaNumerosVariables.Length > 0)
                    listaNumerosVariables.Append(",");
            }
            VariablesDP[] lista_VariableDP = wsEstadisiticas.LeerVariableEncuesta_Lista(controlDP, Class911.Doc_Cuestionario, 0, listaNumerosVariables.ToString());
            
            foreach (VariablesDP varDP in lista_VariableDP)
            {
                int x = 0;
                if (varDP.valor == DBNull.Value)
                    varDP.valor = x;
                if (varDP.valor.ToString() == "0")
                {

                    switch (varDP.var)
                    {
                        case 3:
                            txt_disabled(ref tr1);
                            break;
                        case 6:
                            txt_disabled(ref tr2);
                            break;
                        case 9:
                            txt_disabled(ref tr3);
                            break;
                        case 12:
                            txt_disabled(ref tr4);
                            break;
                        case 15:
                            txt_disabled(ref tr5);
                            break;
                        case 18:
                            txt_disabled(ref tr6);
                            break;
                        case 21:
                            txt_disabled(ref tr7);
                            break;
                        case 24:
                            txt_disabled(ref tr8);
                            break;
                        case 27:
                            txt_disabled(ref tr9);
                            break;
                        case 30:
                            txt_disabled(ref tr10);
                            break;
                        case 33:
                            txt_disabled(ref tr11);
                            break;
                        case 36:
                            txt_disabled(ref tr12);
                            break;
                        case 39:
                            txt_disabled(ref tr13);
                            break;
                        case 42:
                            txt_disabled(ref tr14);
                            break;
                        case 45:
                            txt_disabled(ref tr15);
                            break;
                        case 48:
                            txt_disabled(ref tr16);
                            break;
                        case 564:
                            txt_disabled(ref tr17);
                            break;
                        case 567:
                            txt_disabled(ref tr18);
                            break;
                    }
                }
            }
        }
        void txt_disabled(ref HtmlTableRow row)
        {
            row.Disabled = true;
            for (int i = 1; i < row.Cells.Count-1; i++)
            {
                Control control = row.Cells[i].Controls[1];
                if (control.ID.Substring(0, 4) == "txtV")
                {
                    TextBox txt = (TextBox)control;
                    txt.ReadOnly = true;
                    
                }
            }
        }
    }
}
