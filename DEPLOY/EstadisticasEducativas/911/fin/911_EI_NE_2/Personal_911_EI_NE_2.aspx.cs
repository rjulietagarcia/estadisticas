using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
     
namespace EstadisticasEducativas._911.fin._911_EI_NE_2
{
    public partial class Personal_911_EI_NE_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV254.Attributes["onkeypress"] = "return Num(event)";
                txtV255.Attributes["onkeypress"] = "return Num(event)";
                txtV256.Attributes["onkeypress"] = "return Num(event)";
                txtV257.Attributes["onkeypress"] = "return Num(event)";
                txtV258.Attributes["onkeypress"] = "return Num(event)";
                txtV259.Attributes["onkeypress"] = "return Num(event)";
                txtV260.Attributes["onkeypress"] = "return Num(event)";
                txtV261.Attributes["onkeypress"] = "return Num(event)";
                txtV262.Attributes["onkeypress"] = "return Num(event)";
                txtV263.Attributes["onkeypress"] = "return Num(event)";
                txtV264.Attributes["onkeypress"] = "return Num(event)";
                txtV265.Attributes["onkeypress"] = "return Num(event)";
                txtV266.Attributes["onkeypress"] = "return Num(event)";
                txtV267.Attributes["onkeypress"] = "return Num(event)";
                txtV268.Attributes["onkeypress"] = "return Num(event)";
                txtV269.Attributes["onkeypress"] = "return Num(event)";
                txtV592.Attributes["onkeypress"] = "return Num(event)";
                txtV593.Attributes["onkeypress"] = "return Num(event)";
                txtV270.Attributes["onkeypress"] = "return Num(event)";
                txtV271.Attributes["onkeypress"] = "return Num(event)";
                txtV272.Attributes["onkeypress"] = "return Num(event)";
                txtV273.Attributes["onkeypress"] = "return Num(event)";
                txtV274.Attributes["onkeypress"] = "return Num(event)";
                txtV275.Attributes["onkeypress"] = "return Num(event)";
                txtV276.Attributes["onkeypress"] = "return Num(event)";
                txtV277.Attributes["onkeypress"] = "return Num(event)";
                txtV278.Attributes["onkeypress"] = "return Num(event)";
                txtV279.Attributes["onkeypress"] = "return Num(event)";
                txtV280.Attributes["onkeypress"] = "return Num(event)";
                txtV281.Attributes["onkeypress"] = "return Num(event)";
                txtV282.Attributes["onkeypress"] = "return Num(event)";
                txtV283.Attributes["onkeypress"] = "return Num(event)";
                txtV284.Attributes["onkeypress"] = "return Num(event)";
                txtV285.Attributes["onkeypress"] = "return Num(event)";
                txtV286.Attributes["onkeypress"] = "return Num(event)";
                txtV594.Attributes["onkeypress"] = "return Num(event)";
                txtV595.Attributes["onkeypress"] = "return Num(event)";
                txtV287.Attributes["onkeypress"] = "return Num(event)";
                txtV596.Attributes["onkeypress"] = "return Num(event)";
                txtV597.Attributes["onkeypress"] = "return Num(event)";
                txtV598.Attributes["onkeypress"] = "return Num(event)";
                txtV599.Attributes["onkeypress"] = "return Num(event)";
                txtV600.Attributes["onkeypress"] = "return Num(event)";
                txtV601.Attributes["onkeypress"] = "return Num(event)";
                txtV602.Attributes["onkeypress"] = "return Num(event)";
                txtV603.Attributes["onkeypress"] = "return Num(event)";
                txtV604.Attributes["onkeypress"] = "return Num(event)";
                txtV605.Attributes["onkeypress"] = "return Num(event)";
                txtV606.Attributes["onkeypress"] = "return Num(event)";
                txtV607.Attributes["onkeypress"] = "return Num(event)";
                txtV608.Attributes["onkeypress"] = "return Num(event)";
                txtV609.Attributes["onkeypress"] = "return Num(event)";
                txtV610.Attributes["onkeypress"] = "return Num(event)";
                txtV611.Attributes["onkeypress"] = "return Num(event)";
                txtV612.Attributes["onkeypress"] = "return Num(event)";
                txtV613.Attributes["onkeypress"] = "return Num(event)";
                txtV614.Attributes["onkeypress"] = "return Num(event)";
                txtV615.Attributes["onkeypress"] = "return Num(event)";
                txtV616.Attributes["onkeypress"] = "return Num(event)";
                txtV617.Attributes["onkeypress"] = "return Num(event)";
                txtV618.Attributes["onkeypress"] = "return Num(event)";
                txtV619.Attributes["onkeypress"] = "return Num(event)";
                txtV620.Attributes["onkeypress"] = "return Num(event)";
                txtV621.Attributes["onkeypress"] = "return Num(event)";
                txtV622.Attributes["onkeypress"] = "return Num(event)";
                txtV623.Attributes["onkeypress"] = "return Num(event)";
                txtV624.Attributes["onkeypress"] = "return Num(event)";
                txtV625.Attributes["onkeypress"] = "return Num(event)";
                txtV626.Attributes["onkeypress"] = "return Num(event)";
                txtV627.Attributes["onkeypress"] = "return Num(event)";
                txtV628.Attributes["onkeypress"] = "return Num(event)";
                txtV629.Attributes["onkeypress"] = "return Num(event)";
                txtV630.Attributes["onkeypress"] = "return Num(event)";
                txtV631.Attributes["onkeypress"] = "return Num(event)";
                txtV632.Attributes["onkeypress"] = "return Num(event)";
                txtV633.Attributes["onkeypress"] = "return Num(event)";
                txtV634.Attributes["onkeypress"] = "return Num(event)";
                txtV635.Attributes["onkeypress"] = "return Num(event)";
                txtV636.Attributes["onkeypress"] = "return Num(event)";
                txtV637.Attributes["onkeypress"] = "return Num(event)";
                txtV638.Attributes["onkeypress"] = "return Num(event)";
                txtV639.Attributes["onkeypress"] = "return Num(event)";
                txtV640.Attributes["onkeypress"] = "return Num(event)";
                txtV641.Attributes["onkeypress"] = "return Num(event)";
                txtV642.Attributes["onkeypress"] = "return Num(event)";
                txtV643.Attributes["onkeypress"] = "return Num(event)";
                txtV644.Attributes["onkeypress"] = "return Num(event)";
                txtV645.Attributes["onkeypress"] = "return Num(event)";
                txtV646.Attributes["onkeypress"] = "return Num(event)";
                txtV647.Attributes["onkeypress"] = "return Num(event)";
                txtV648.Attributes["onkeypress"] = "return Num(event)";
                txtV649.Attributes["onkeypress"] = "return Num(event)";
                txtV650.Attributes["onkeypress"] = "return Num(event)";
                txtV651.Attributes["onkeypress"] = "return Num(event)";
                txtV652.Attributes["onkeypress"] = "return Num(event)";
                txtV653.Attributes["onkeypress"] = "return Num(event)";
                txtV654.Attributes["onkeypress"] = "return Num(event)";
                txtV655.Attributes["onkeypress"] = "return Num(event)";
                txtV656.Attributes["onkeypress"] = "return Num(event)";
                txtV657.Attributes["onkeypress"] = "return Num(event)";
                txtV658.Attributes["onkeypress"] = "return Num(event)";
                txtV660.Attributes["onkeypress"] = "return Num(event)";
                txtV661.Attributes["onkeypress"] = "return Num(event)";
                txtV662.Attributes["onkeypress"] = "return Num(event)";
                txtV664.Attributes["onkeypress"] = "return Num(event)";
                txtV665.Attributes["onkeypress"] = "return Num(event)";
                txtV666.Attributes["onkeypress"] = "return Num(event)";
                txtV668.Attributes["onkeypress"] = "return Num(event)";
                txtV669.Attributes["onkeypress"] = "return Num(event)";
                txtV670.Attributes["onkeypress"] = "return Num(event)";
                txtV671.Attributes["onkeypress"] = "return Num(event)";
                txtV672.Attributes["onkeypress"] = "return Num(event)";
                txtV673.Attributes["onkeypress"] = "return Num(event)";
                txtV674.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
                //ActivarTRs(controlDP);
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************


        void ActivarTRs(ControlDP controlDP)
        {
            ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
            string lista = "txtV3,3|txtV6,6|txtV9,9|txtV12,12|txtV15,15|txtV18,18|txtV21,21|txtV24,24|txtV27,27|txtV30,30|txtV33,33|txtV36,36|txtV39,39|txtV42,42|txtV45,45|txtV48,48|txtV564,564|txtV567,567|";
            string[] Cajas = lista.Split('|');

            System.Text.StringBuilder listaNumerosVariables = new System.Text.StringBuilder();
            for (int NoVariable = 0; NoVariable < Cajas.Length - 1; NoVariable++)
            {
                if (Cajas[NoVariable].Split(',')[1] != "0")
                    listaNumerosVariables.Append(Cajas[NoVariable].Split(',')[1]);
                if (NoVariable < Cajas.Length - 2 && listaNumerosVariables.Length > 0)
                    listaNumerosVariables.Append(",");
            }
            VariablesDP[] lista_VariableDP = wsEstadisiticas.LeerVariableEncuesta_Lista(controlDP, Class911.Doc_Cuestionario, 0, listaNumerosVariables.ToString());

            foreach (VariablesDP varDP in lista_VariableDP)
            {
                int x = 0;
                if (varDP.valor == DBNull.Value)
                    varDP.valor = x;
                if (varDP.valor.ToString() == "0")
                {

                    switch (varDP.var)
                    {
                        case 3:
                            txt_disabled(ref trA_1);
                            txt_disabled(ref trB_1);
                            break;
                        case 6:
                            txt_disabled(ref trA_2);
                            txt_disabled(ref trB_2);
                            break;
                        case 9:
                            txt_disabled(ref trA_3);
                            txt_disabled(ref trB_3);
                            break;
                        case 12:
                            txt_disabled(ref trA_4);
                            txt_disabled(ref trB_4);
                            break;
                        case 15:
                            txt_disabled(ref trA_5);
                            txt_disabled(ref trB_5);
                            break;
                        case 18:
                            txt_disabled(ref trA_6);
                            txt_disabled(ref trB_6);
                            break;
                        case 21:
                            txt_disabled(ref trA_7);
                            txt_disabled(ref trB_7);
                            break;
                        case 24:
                            txt_disabled(ref trA_8);
                            txt_disabled(ref trB_8);
                            break;
                        case 27:
                            txt_disabled(ref trA_9);
                            txt_disabled(ref trB_9);
                            break;
                        case 30:
                            txt_disabled(ref trA_10);
                            txt_disabled(ref trB_10);
                            break;
                        case 33:
                            txt_disabled(ref trA_11);
                            txt_disabled(ref trB_11);
                            break;
                        case 36:
                            txt_disabled(ref trA_12);
                            txt_disabled(ref trB_12);
                            break;
                        case 39:
                            txt_disabled(ref trA_13); 
                            txt_disabled(ref trB_13);
                            break;
                        case 42:
                            txt_disabled(ref trA_14);
                            txt_disabled(ref trB_14);
                            break;
                        case 45:
                            txt_disabled(ref trA_15);
                            txt_disabled(ref trB_15);
                            break;
                        case 48:
                            txt_disabled(ref trA_16);
                            txt_disabled(ref trB_16);
                            break;
                        case 564:
                            txt_disabled(ref trA_17);
                            txt_disabled(ref trB_17);
                            break;
                        case 567:
                            txt_disabled(ref trA_18);
                            txt_disabled(ref trB_18);
                            break;
                    }
                }
            }
        }
        void txt_disabled(ref HtmlTableRow row)
        {
            row.Disabled = true;
            for (int i = 1; i < row.Cells.Count - 1; i++)
            {
                Control control = row.Cells[i].Controls[1];
                if (control.ID.Substring(0, 4) == "txtV")
                {
                    TextBox txt = (TextBox)control;
                    txt.ReadOnly = true;
                }
            }
        }
    }
}
