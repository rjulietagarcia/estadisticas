<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-NE2(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_EI_NE_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_EI_NE_2.Personal_911_EI_NE_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 29;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">

    <table style=" padding-left:200px;">
        <tr><td><span>EDUCACI�N INICIAL NO ESCOLARIZADA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="OpenPageCharged('Identificacion_911_EI_NE_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="OpenPageCharged('Grupos_911_EI_NE_2',true)"><a href="#" title=""><span>GRUPOS</span></a></li><li onclick="OpenPageCharged('AG_911_EI_NE_2',true)"><a href="#" title=""><span>NI�OS</span></a></li><li onclick="OpenPageCharged('Personal_911_EI_NE_2',true)"><a href="#" title="" class="activo"><span>PADRES DE FAMILIA Y PERSONAL</span></a></li><li onclick="OpenPageCharged('Programa_911_EI_NE_2',false)"><a href="#" title=""><span>PROGRAMA</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ESPACIOS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
          
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

      
            <table>
               <tr>
                   <td style="vertical-align: top">
                     <table>
                        <tr>
                             <td style="text-align: left"> 
                                <asp:Label ID="lblPadres" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                    Text="III. PADRES DE FAMILIA" Width="250px">
                                </asp:Label>
                                <br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba, por municipio o delegaci�n y localidad o colonia, el n�mero de padres que participan en el programa al 4 de julio del presente a�o."
                                    Width="250px"
                                ></asp:Label>
                           </td>
                        </tr> 
                     <tr>
                        <td> 
           <table id="Table2" style="text-align: center">
              
                <tr>
                    <td colspan="2" style="text-align: center;" class="linaBajoAlto" >
                                <asp:Label ID="lblPadresTit" runat="server" CssClass="lblRojo" Text="PADRES DE FAMILIA" ></asp:Label></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_1" runat="server">
                    <td style="text-align: right; " class="linaBajo">
                                <asp:Label ID="lblLoc31" runat="server" CssClass="lblGrisTit" Text="1" Height="17px" ></asp:Label>
                    </td>
                    <td  class="linaBajo">
                                <asp:TextBox ID="txtV254" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro" MaxLength="3"></asp:TextBox>
                    </td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_2" runat="server">
                    <td style="text-align: right;" class="linaBajo">
                                <asp:Label ID="lblLoc32" runat="server" CssClass="lblGrisTit" Height="17px" Text="2" ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV255" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_3" runat="server">
                    <td style="text-align: right; " class="linaBajo">
                            <asp:Label ID="lblLoc33" runat="server" CssClass="lblGrisTit" Text="3" Height="17px" ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV256" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_4" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc34" runat="server" CssClass="lblGrisTit" Height="17px" Text="4"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV257" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_5" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc35" runat="server" CssClass="lblGrisTit" Height="17px" Text="5"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV258" runat="server" Columns="3" TabIndex="10501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_6" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc36" runat="server" CssClass="lblGrisTit" Height="17px" Text="6"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV259" runat="server" Columns="3" TabIndex="10601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_7" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc37" runat="server" CssClass="lblGrisTit" Height="17px" Text="7"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV260" runat="server" Columns="3" TabIndex="10701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_8" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc38" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="8" ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV261" runat="server" Columns="3" TabIndex="10801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_9" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc39" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="9" ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV262" runat="server" Columns="3" TabIndex="10901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_10" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc310" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="10" ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV263" runat="server" Columns="3" TabIndex="11001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_11" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc311" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="11" ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV264" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_12" runat="server">
                    <td style="text-align: right" class="linaBajo" >
                                <asp:Label ID="lblLoc312" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="12" ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV265" runat="server" Columns="3" TabIndex="11201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trA_13" runat="server">
                    <td style="text-align: right;" class="linaBajo">
                                <asp:Label ID="lblLoc313" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="13" ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV266" runat="server" Columns="3" TabIndex="11301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_14" runat="server">
                    <td style="text-align: right;" class="linaBajo">
                                <asp:Label ID="lblLoc314" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="14" ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV267" runat="server" Columns="3" TabIndex="11401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_15" runat="server">
                    <td style="text-align: right;" class="linaBajo">
                                <asp:Label ID="lblLoc315" runat="server" CssClass="lblGrisTit" Text="15" ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV268" runat="server" Columns="3" TabIndex="11501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_16" runat="server">
                    <td style="text-align: right; " class="linaBajo">
                                <asp:Label ID="lblLoc316" runat="server" CssClass="lblGrisTit" Text="16" ></asp:Label></td>
                            <td  class="linaBajo" >
                                <asp:TextBox ID="txtV269" runat="server" Columns="3" TabIndex="11601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_17" runat="server">
                    <td style="text-align: right;" class="linaBajo">
                                <asp:Label ID="lblLoc317" runat="server" CssClass="lblGrisTit" Text="17" ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV592" runat="server" Columns="3" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trA_18" runat="server">
                    <td style="text-align: right;" class="linaBajo">
                                <asp:Label ID="lblLoc318" runat="server" CssClass="lblGrisTit" Text="18" ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV593" runat="server" Columns="3" TabIndex="11801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right;" class="linaBajo">
                                <asp:Label ID="lblTotal3" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV270" runat="server" Columns="3" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
            </table>
                     </td>
                     </tr> 
                     </table>
                               
                
                   </td>
                   <td style="vertical-align: top">
                   <table><tr><td style="text-align: left">
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblRojo" Font-Size="16px" Text="IV. PERSONAL"
                                    Width="250px"></asp:Label><br />
                                <asp:Label ID="lblInstrucciones41" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Escriba el n�mero total de educadores comunitarios que atienden los grupos, de acuerdo con el municipio o delegaci�n y localidad o colonia."
                                    Width="250px"></asp:Label>
                                    </td></tr></table>
                   <table id="Table3" style="text-align: center">
               
                <tr>
                    <td style="text-align: center; height: 26px;" class="linaBajoAlto" colspan="2">
                                <asp:Label ID="lblEducadoresTit" runat="server" CssClass="lblRojo" Text="EDUCADORES COMUNITARIOS" ></asp:Label></td>
                   <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_1" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Height="17px" Text="1"
                                    ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV271" runat="server" Columns="3" TabIndex="20101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_2" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Height="17px" Text="2"
                                    ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV272" runat="server" Columns="3" TabIndex="20201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_3" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Height="17px" Text="3"
                                    ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV273" runat="server" Columns="3" TabIndex="20301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_4" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Height="17px" Text="4"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV274" runat="server" Columns="3" TabIndex="20401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_5" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Height="17px" Text="5"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV275" runat="server" Columns="3" TabIndex="20501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_6" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Height="17px" Text="6"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV276" runat="server" Columns="3" TabIndex="20601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_7" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Height="17px" Text="7"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV277" runat="server" Columns="3" TabIndex="20701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_8" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Height="17px" Text="8"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV278" runat="server" Columns="3" TabIndex="20801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_9" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Height="17px" Text="9"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV279" runat="server" Columns="3" TabIndex="20901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_10" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Height="17px" Text="10"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV280" runat="server" Columns="3" TabIndex="21001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_11" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Height="17px" Text="11"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV281" runat="server" Columns="3" TabIndex="21101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_12" runat="server">
                    <td style="text-align: right" class="linaBajo">
                                <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Height="17px" Text="12"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV282" runat="server" Columns="3" TabIndex="21201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila">
                        &nbsp;</td>
                </tr>
                <tr id="trB_13" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Height="17px" Text="13"
                                    ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV283" runat="server" Columns="3" TabIndex="21301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_14" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Height="17px" Text="14"
                                    ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV284" runat="server" Columns="3" TabIndex="21401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                   <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_15" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Text="15" ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV285" runat="server" Columns="3" TabIndex="21501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_16" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Text="16" ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV286" runat="server" Columns="3" TabIndex="21601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_17" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label19" runat="server" CssClass="lblGrisTit" Text="17" ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV594" runat="server" Columns="3" TabIndex="21701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr id="trB_18" runat="server">
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Text="18" ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV595" runat="server" Columns="3" TabIndex="21801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="height: 26px; text-align: right;" class="linaBajo">
                                <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                            <td style="height: 26px; text-align: center;" class="linaBajo">
                                <asp:TextBox ID="txtV287" runat="server" Columns="3" TabIndex="21901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" >
                        &nbsp;</td>
                </tr>
            </table>
                   </td>
                   <td><table id="Table4" style="text-align: center">
                        <tr>
                            <td colspan="4" style="text-align: left" 
                                >
                                <asp:Label ID="lblInstrucciones42" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Desglose el total de educadores comunitarios, de acuerdo con el nivel m�ximo de estudios y sexo. Nota: Si en la tabla correspondiente al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente o en OTROS."
                                    Width="400px"></asp:Label></td>
                            <td colspan="1" style="width: 10px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblNivelTit" runat="server" CssClass="lblRojo" Text="NIVEL EDUCATIVO"
                                    Width="150px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblHombres42" runat="server" CssClass="lblRojo" Text="HOMBRES" ></asp:Label></td>
                            <td colspan="" style="height: 19px; text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblMujeres42" runat="server" CssClass="lblRojo" Text="MUJERES" ></asp:Label></td>
                            <td colspan="" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblTotal421" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                           <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblPrimInc" runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV596" runat="server" Columns="3" TabIndex="30101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV597" runat="server" Columns="3" TabIndex="30102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV598" runat="server" Columns="3" TabIndex="30103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblPrimTerm" runat="server" CssClass="lblGrisTit" Text="PRIMARIA TERMINADA"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV599" runat="server" Columns="3" TabIndex="30201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV600" runat="server" Columns="3" TabIndex="30202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV601" runat="server" Columns="3" TabIndex="30203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                                
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblSecInc" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA INCOMPLETA"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV602" runat="server" Columns="3" TabIndex="30301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV603" runat="server" Columns="3" TabIndex="30302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV604" runat="server" Columns="3" TabIndex="30303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblSecTerm" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA TERMINADA"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV605" runat="server" Columns="3" TabIndex="30401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV606" runat="server" Columns="3" TabIndex="30402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV607" runat="server" Columns="3" TabIndex="30403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblProfTec" runat="server" CssClass="lblGrisTit" Text="PROFESIONAL T�CNICO"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV608" runat="server" Columns="3" TabIndex="30501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV609" runat="server" Columns="3" TabIndex="30502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV610" runat="server" Columns="3" TabIndex="30503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblBachInc" runat="server" CssClass="lblGrisTit" Text="BACHILLERATO INCOMPLETO"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV611" runat="server" Columns="3" TabIndex="30601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV612" runat="server" Columns="3" TabIndex="30602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV613" runat="server" Columns="3" TabIndex="30603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblBachTerm" runat="server" CssClass="lblGrisTit" Text="BACHILLERATO TERMINADO"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV614" runat="server" Columns="3" TabIndex="30701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV615" runat="server" Columns="3" TabIndex="30702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV616" runat="server" Columns="3" TabIndex="30703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPreInc" runat="server" CssClass="lblGrisTit" Text="NORMAL PREESCOLAR INCOMPLETA"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV617" runat="server" Columns="3" TabIndex="30801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV618" runat="server" Columns="3" TabIndex="30802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV619" runat="server" Columns="3" TabIndex="30803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                           <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPreTerm" runat="server" CssClass="lblGrisTit" Text="NORMAL PREESCOLAR TERMINADA"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV620" runat="server" Columns="3" TabIndex="30901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV621" runat="server" Columns="3" TabIndex="30902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV622" runat="server" Columns="3" TabIndex="30903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPrimInc" runat="server" CssClass="lblGrisTit" Text="NORMAL PRIMARIA INCOMPLETA"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV623" runat="server" Columns="3" TabIndex="31001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV624" runat="server" Columns="3" TabIndex="31002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV625" runat="server" Columns="3" TabIndex="31003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPrimTerm" runat="server" CssClass="lblGrisTit" Text="NORMAL PRIMARIA TERMINADA"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV626" runat="server" Columns="3" TabIndex="31101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV627" runat="server" Columns="3" TabIndex="31102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV628" runat="server" Columns="3" TabIndex="31103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormSupInc" runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR INCOMPLETA"
                                    ></asp:Label></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV629" runat="server" Columns="3" TabIndex="31201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV630" runat="server" Columns="3" TabIndex="31202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo" >
                                <asp:TextBox ID="txtV631" runat="server" Columns="3" TabIndex="31203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormSupPas" runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR, PASANTE"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV632" runat="server" Columns="3" TabIndex="31301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV633" runat="server" Columns="3" TabIndex="31302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV634" runat="server" Columns="3" TabIndex="31303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormSupTit" runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR, TITULADO"
                                    ></asp:Label></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV635" runat="server" Columns="3" TabIndex="31401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV636" runat="server" Columns="3" TabIndex="31402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td  class="linaBajo">
                                <asp:TextBox ID="txtV637" runat="server" Columns="3" TabIndex="31403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblLicInc" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA INCOMPLETA"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV638" runat="server" Columns="3" TabIndex="31501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV639" runat="server" Columns="3" TabIndex="31502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV640" runat="server" Columns="3" TabIndex="31503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                           <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblLicPas" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, PASANTE"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV641" runat="server" Columns="3" TabIndex="31601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV642" runat="server" Columns="3" TabIndex="31602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV643" runat="server" Columns="3" TabIndex="31603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblLicTit" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV644" runat="server" Columns="3" TabIndex="31701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV645" runat="server" Columns="3" TabIndex="31702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV646" runat="server" Columns="3" TabIndex="31703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblMaestInc" runat="server" CssClass="lblGrisTit" Text="MAESTR�A INCOMPLETA"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV647" runat="server" Columns="3" TabIndex="31801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV648" runat="server" Columns="3" TabIndex="31802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV649" runat="server" Columns="3" TabIndex="31803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblMaestGrad" runat="server" CssClass="lblGrisTit" Text="MAESTR�A, GRADUADO"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV650" runat="server" Columns="3" TabIndex="31901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV651" runat="server" Columns="3" TabIndex="31902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV652" runat="server" Columns="3" TabIndex="31903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblDocInc" runat="server" CssClass="lblGrisTit" Text="DOCTORADO INCOMPLETO"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV653" runat="server" Columns="3" TabIndex="32001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV654" runat="server" Columns="3" TabIndex="32002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV655" runat="server" Columns="3" TabIndex="32003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblDocGrad" runat="server" CssClass="lblGrisTit" Text="DOCTORADO, GRADUADO"
                                    ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV656" runat="server" Columns="3" TabIndex="32101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV657" runat="server" Columns="3" TabIndex="32102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV658" runat="server" Columns="3" TabIndex="32103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajo" colspan="4">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS (ESPECIFIQUE)"
                                    ></asp:Label></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 67px; height: 26px; text-align: left;" class="linaBajo">
                                <asp:TextBox ID="txtV659" runat="server" Columns="30" TabIndex="32301" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV660" runat="server" Columns="3" TabIndex="32302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV661" runat="server" Columns="3" TabIndex="32303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV662" runat="server" Columns="3" TabIndex="32304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 67px; height: 26px; text-align: left;" class="linaBajo">
                                <asp:TextBox ID="txtV663" runat="server" Columns="30" TabIndex="32401" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV664" runat="server" Columns="3" TabIndex="32402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV665" runat="server" Columns="3" TabIndex="32403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV666" runat="server" Columns="3" TabIndex="32404" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 67px; height: 26px; text-align: left;" class="linaBajo">
                                <asp:TextBox ID="txtV667" runat="server" Columns="30" TabIndex="32501" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV668" runat="server" Columns="3" TabIndex="32502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV669" runat="server" Columns="3" TabIndex="32503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV670" runat="server" Columns="3" TabIndex="32504" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: left;" class="linaBajo">
                                <asp:Label ID="lblSubtotal42" runat="server" CssClass="lblRojo" Text="SUBTOTAL" ></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV671" runat="server" Columns="3" TabIndex="32601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV672" runat="server" Columns="3" TabIndex="32602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV673" runat="server" Columns="3" TabIndex="32603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: right;" colspan="3" class="linaBajo">
                                <asp:Label ID="lblTotal422" runat="server" CssClass="lblRojo" Text="TOTAL" Width="150px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV674" runat="server" Columns="3" TabIndex="32761" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila" >
                        &nbsp;</td>
                        </tr>
                    </table>
                   </td>
               </tr>
            </table>
        </center>
        <center>
            &nbsp;</center>
        <center>
            &nbsp;</center>
        <center>
            &nbsp;</center>
        <center>
            &nbsp;</center>
        <center>
    
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('AG_911_EI_NE_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('AG_911_EI_NE_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="OpenPageCharged('Programa_911_EI_NE_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Programa_911_EI_NE_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
<%--				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>--%>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                function ValidaTexto(obj){
                    if((obj.value == '')||(obj.value == '0')){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana,ir){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV659'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV663'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV667'));
                    
                    openPage(ventana,ir);
                }                                          
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
