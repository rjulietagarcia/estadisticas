<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8G (Personal)" AutoEventWireup="true" CodeBehind="Personal_911_8G.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_8G.Personal_911_8G" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 18;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">


    <table style=" padding-left:200px;">
        <tr><td><span>BACHILLERATO GENERAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8G',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos1_911_8G',true)"><a href="#" title=""><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('Alumnos2_911_8G',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Personal_911_8G',true)"><a href="#" title="" class="activo"><span>PERSONAL Y PLANTELES</span></a></li>
        <li onclick="openPage('Inmueble_911_8G',false)"><a href="#" title="" ><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div> 
 <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td valign="top">
                    <table style="width: 450px">
                        <tr>
                            <td>
                                <asp:Label ID="lblPERSONAL" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Font-Size="16px" Text="III. PERSONAL POR FUNCI�N" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccionIII1" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="1. Escriba el personal seg�n la funci�n que realiz, independientemente de su nombramiento, tipo y fuente de pago. Si una persona desempe�a dos o m�s funciones, an�tela en aqu�lla a la quededique m�s tiempo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblDirectivo" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PERSONAL DIRECTIVO"
                        Width="100%"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblConGrupo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CON GRUPO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV446" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10101"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblSinGrupo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SIN GRUPO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV447" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblDocente" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PERSONAL DOCENTE"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV448" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblDocenteEsp" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PERSONAL DOCENTE ESPECIAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblEdFisica" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PROFESORES DE EDUCACI�N F�SICA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV449" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblActArt" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PROFESORES DE ACTIVIDADES ART�STICAS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV450" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblActTec" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PROFESORES DE ACTIVIDADES TECNOL�GICAS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV451" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblIdiomas" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PROFESORES DE IDIOMAS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV452" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblAdministrativo" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV453" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblTotalP" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL DE PERSONAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV454" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccionIII2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Sume el personal directivo con grupo, personal docente y personal docente especial, y an�telo seg�n el tiempo que dedica a la funci�n acad�mica."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblTCompleto" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TIEMPO COMPLETO"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV500" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblT3cuartos" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TRES CUARTOS DE TIEMPO"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV501" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblTmedio" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MEDIO TIEMPO"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV502" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblTHoras" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="POR HORAS"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV503" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="height: 26px">
                    <asp:Label ID="lblTTotal" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="height: 26px">
                    <asp:TextBox ID="txtV504" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table style="width: 550px">
                        <tr>
                            <td>
                                <asp:Label ID="lblPLANTELES" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                    Text="IV. PLANTELES DE EXTENSI�N" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccionIV1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Escriba el total de alumnos que se atendieron en los planteles de extensi�n; desglose la inscripci�n total, la existencia, los aprobados en todas las asignaturas y los reprobados de una a cinco asignaturas seg�n el sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: center" class="linaBajoAlto ">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRADO"
                        Width="100%"></asp:Label></td>
                <td rowspan="1" style="text-align: center" class="linaBajoAlto ">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SEMESTRE"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto ">
                    <asp:Label ID="lblSexo" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SEXO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto ">
                    <asp:Label ID="lblInsTotal" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="75px"></asp:Label></td>
                <td class="linaBajoAlto ">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto ">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblNegro" Font-Bold="True" Text="APROBADOS EN TODAS LAS ASIGNATURAS"
                        Width="85px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto ">
                    <asp:Label ID="lblReprobados" runat="server" CssClass="lblNegro" Font-Bold="True" Text="REPROBADOS DE 1 A 5 ASIGNATURAS"
                        Width="85px"></asp:Label></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblG1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 y 2"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV455" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV456" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV457" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV458" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV459" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV460" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV461" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV462" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20204"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblG2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 y 4"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV463" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV464" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV465" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV466" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20304"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV467" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV468" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV469" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV470" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20404"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblG3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 y 6"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV471" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV472" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV473" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV474" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20504"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV475" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV476" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV477" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV478" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20604"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblG4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 y 8"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV479" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV480" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV481" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV482" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20704"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV483" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV484" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV485" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV486" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20804"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblTotalT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV487" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo" colspan="">
                    <asp:TextBox ID="txtV488" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV489" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV490" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="20904"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccionIV2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Escriba, por grado, los grupos existentes."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td style="text-align: center">
                    <asp:Label ID="lblGrupos2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o. (1o. y 2o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV491" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21001"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o. (3o. y 4o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV492" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21101"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o. (5o. y 6o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV493" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21201"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o. (7o. y 8o. semestres)"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV494" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21301"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalGrupos" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV495" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21401"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblInstruccionIV3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Escriba el personal docente que atiende a los alumnos de extensi�n, desglos�ndolo por sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table style="height: 85px">
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblHombres" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV496" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21501"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMujeres" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV497" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21601"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalPersonal" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV498" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21701"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Alumnos2_911_8G',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Alumnos2_911_8G',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Inmueble_911_8G',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Inmueble_911_8G',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>

