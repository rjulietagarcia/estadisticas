<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="CUESTIONARIO DE INTEGRACI�N EDUCATIVA" AutoEventWireup="true" CodeBehind="Anexo_911_8G.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8G.Anexo_911_8G" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript" src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 25;
        MaxRow = 15;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">


    <table style=" padding-left:200px;">
        <tr><td><span>BACHILLERATO GENERAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8G',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos1_911_8G',true)"><a href="#" title=""><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('Alumnos2_911_8G',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Personal_911_8G',true)"><a href="#" title=""><span>PERSONAL Y PLANTELES</span></a></li>
        <li onclick="openPage('Inmueble_911_8G',true)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="openPage('Anexo_911_8G',true)"><a href="#" title="" class="activo"><span>ANEXO</span></a></li>
        <li onclick="openPage('Oficializar_911_8G',false)"><a href="#" title=""><span>OFICIALIZACI�N</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
       <center>
      
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

         <table onkeydown="enter(event)">
            <tr>
                <td>
                    <asp:Label ID="Label36" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="IMPORTANTE:" ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label37" runat="server" CssClass="lblGris" Font-Bold="True" Text="El objetivo del presente cuestionario es recuperar informaci�n acerca de la poblaci�n estudiantil que presenta Necesidades Educativas Especiales y la condici�n con la que se asocian, inscritos a las escuelas de Educaci�n Inicial, B�sica, Media Superior y Normal del Sistema Educativo Nacional que permita realizar acciones para elevar la calidad de la respuesta educativa que se ofrece a estas alumnas y alumnos."></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label38" runat="server" CssClass="lblGris" Font-Bold="True" Text="Para responder este cuestionario tenga presente las recomendaciones que a continuaci�n se mencionan, de acuerdo con el nivel educativo al que pertenecen."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label39" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Inicial: Completar la columna de Lactantes y Maternales. Cuando ofrezca Educaci�n Preescolar, completar 1�, 2� y 3� grado y el total. Este nivel no contestar� el apartado 6."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label40" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Preescolar: Completar solamente 1�, 2�, 3� grado y el total. Este nivel no contestar� el apartado 6."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label41" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Primaria: Completar solamente 1�, 2�, 3�, 4�, 5�, 6� grado y el total."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label42" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Secundaria: Completar solamente 1�, 2�, 3� grado y el total."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label43" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Media Superior y Normal: Completar por ciclo escolar cursado, 1� incluye primer y segundo semestre; 2� incluye tercer y cuarto semestre; 3� incluye quinto y sexto semestre; 4� incluye s�ptimo y octavo semestre; y el total. Estos niveles no contestar�n las preguntas 4 y 5."
                        ></asp:Label></td>
            </tr>
        </table>
        <br />
        <br />
        <table>
            <tr>
                <td colspan="25" style="text-align: justify">
                    <asp:Label ID="Label35" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="II. Alumnos con Necesidades Educativas Especiales"></asp:Label></td>
                <td colspan="1" style="text-align: justify">
                </td>
            </tr>
            <tr>
                <td colspan="16" style="text-align: justify">
                    <asp:Label ID="Label34" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. De la existencia total, escriba el n�mero de alumnos con Necesidades Educaivas Especiales:" Width="444px"></asp:Label>
                    <asp:TextBox ID="txtVANX660" runat="server" Columns="4" MaxLength="4" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                <td colspan="9" style="text-align: left">
                    </td>
                <td colspan="1" style="text-align: left">
                </td>
            </tr>
            <tr>
                <td colspan="25" rowspan="1" style="text-align: left">
                    <asp:Label ID="Label33" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. De los alumnos reportados en el punto anterior, desglose de acuerdo con el grado y sexo, seg�n la discapacidad o condici�n que presentan. Para ser preciso en la informaci�n se recomienda revisar el glosario." Width="872px"></asp:Label></td>
                <td colspan="1" rowspan="1" style="text-align: left">
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblCondicion" runat="server" CssClass="lblNegro" Font-Bold="True"
                        Text="Condici�n o discapacidad con la que se asocian las NEE de los alumnos"
                        ></asp:Label></td>
                <td colspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblInicial" runat="server" CssClass="lblNegro" Font-Bold="True" Text="Lactantes y Maternales"
                        ></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="1�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="2�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="3�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="4�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl5o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="5�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl6o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="6�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTotal1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="Total"></asp:Label></td>
                <td class="Orila" colspan="1" style="text-align: center">
                </td>
            </tr>
            <tr>
                <td style="text-align: center;" class="linaBajo">
                    <asp:Label ID="lblInicialH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:Label ID="lblInicialM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajo Sombreado">
                    <asp:Label ID="lblInicialT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:Label ID="lbl1oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:Label ID="lbl2oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl3oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center; width: 42px;" class="linaBajo">
                    <asp:Label ID="lbl3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:Label ID="lbl3oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl4oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl4oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:Label ID="lbl4oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl5oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl5oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:Label ID="lbl5oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl6oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl6oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:Label ID="lbl6oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblTotal1H" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblTotal1M" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:Label ID="lblTotal1T" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td class="Orila" style="text-align: center">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="Ceguera" ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX15" runat="server" Columns="2" MaxLength="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX16" runat="server" Columns="2" MaxLength="2" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX17" runat="server" Columns="3" MaxLength="3" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX18" runat="server" Columns="2" MaxLength="2" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX19" runat="server" Columns="2" MaxLength="2" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX20" runat="server" Columns="3" MaxLength="3" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX21" runat="server" Columns="2" MaxLength="2" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX22" runat="server" Columns="2" MaxLength="2" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX23" runat="server" Columns="3" MaxLength="3" TabIndex="20109" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX24" runat="server" Columns="2" MaxLength="2" TabIndex="20110" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX25" runat="server" Columns="2" MaxLength="2" TabIndex="20111" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX26" runat="server" Columns="3" MaxLength="3" TabIndex="20112" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX27" runat="server" Columns="2" MaxLength="2" TabIndex="20113" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX28" runat="server" Columns="2" MaxLength="2" TabIndex="20114" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX29" runat="server" Columns="3" MaxLength="3" TabIndex="20115" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX30" runat="server" Columns="2" MaxLength="2" TabIndex="20116" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX31" runat="server" Columns="2" MaxLength="2" TabIndex="20117" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX32" runat="server" Columns="3" MaxLength="3" TabIndex="20118" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX33" runat="server" Columns="2" MaxLength="2" TabIndex="20119" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX34" runat="server" Columns="2" MaxLength="2" TabIndex="20120" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX35" runat="server" Columns="3" MaxLength="3" TabIndex="20121" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX36" runat="server" Columns="2" MaxLength="2" TabIndex="20122" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX37" runat="server" Columns="2" MaxLength="2" TabIndex="20123" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX38" runat="server" Columns="3" MaxLength="3" TabIndex="20124" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblVision" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Baja visi�n"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX39" runat="server" Columns="2" MaxLength="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX40" runat="server" Columns="2" MaxLength="2" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX41" runat="server" Columns="3" MaxLength="3" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX42" runat="server" Columns="2" MaxLength="2" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX43" runat="server" Columns="2" MaxLength="2" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX44" runat="server" Columns="3" MaxLength="3" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX45" runat="server" Columns="2" MaxLength="2" TabIndex="20207" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX46" runat="server" Columns="2" MaxLength="2" TabIndex="20208" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX47" runat="server" Columns="3" MaxLength="3" TabIndex="20209" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX48" runat="server" Columns="2" MaxLength="2" TabIndex="20210" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX49" runat="server" Columns="2" MaxLength="2" TabIndex="20211" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX50" runat="server" Columns="3" MaxLength="3" TabIndex="20212" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX51" runat="server" Columns="2" MaxLength="2" TabIndex="20213" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX52" runat="server" Columns="2" MaxLength="2" TabIndex="20214" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX53" runat="server" Columns="3" MaxLength="3" TabIndex="20215" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX54" runat="server" Columns="2" MaxLength="2" TabIndex="20216" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX55" runat="server" Columns="2" MaxLength="2" TabIndex="20217" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX56" runat="server" Columns="3" MaxLength="3" TabIndex="20218" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX57" runat="server" Columns="2" MaxLength="2" TabIndex="20219" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX58" runat="server" Columns="2" MaxLength="2" TabIndex="20220" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX59" runat="server" Columns="3" MaxLength="3" TabIndex="20221" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX60" runat="server" Columns="2" MaxLength="2" TabIndex="20222" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX61" runat="server" Columns="2" MaxLength="2" TabIndex="20223" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX62" runat="server" Columns="3" MaxLength="3" TabIndex="20224" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Sordera"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX63" runat="server" Columns="2" MaxLength="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX64" runat="server" Columns="2" MaxLength="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX65" runat="server" Columns="3" MaxLength="3" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX66" runat="server" Columns="2" MaxLength="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX67" runat="server" Columns="2" MaxLength="2" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX68" runat="server" Columns="3" MaxLength="3" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX69" runat="server" Columns="2" MaxLength="2" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX70" runat="server" Columns="2" MaxLength="2" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX71" runat="server" Columns="3" MaxLength="3" TabIndex="20309" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX72" runat="server" Columns="2" MaxLength="2" TabIndex="20310" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX73" runat="server" Columns="2" MaxLength="2" TabIndex="20311" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX74" runat="server" Columns="3" MaxLength="3" TabIndex="20312" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX75" runat="server" Columns="2" MaxLength="2" TabIndex="20313" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX76" runat="server" Columns="2" MaxLength="2" TabIndex="20314" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX77" runat="server" Columns="3" MaxLength="3" TabIndex="20315" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX78" runat="server" Columns="2" MaxLength="2" TabIndex="20316" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX79" runat="server" Columns="2" MaxLength="2" TabIndex="20317" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX80" runat="server" Columns="3" MaxLength="3" TabIndex="20318" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX81" runat="server" Columns="2" MaxLength="2" TabIndex="20319" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX82" runat="server" Columns="2" MaxLength="2" TabIndex="20320" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX83" runat="server" Columns="3" MaxLength="3" TabIndex="20321" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX84" runat="server" Columns="2" MaxLength="2" TabIndex="20322" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX85" runat="server" Columns="2" MaxLength="2" TabIndex="20323" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX86" runat="server" Columns="3" MaxLength="3" TabIndex="20324" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblHipoacusia" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hipoacusia"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX87" runat="server" Columns="2" MaxLength="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX88" runat="server" Columns="2" MaxLength="2" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX89" runat="server" Columns="3" MaxLength="3" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX90" runat="server" Columns="2" MaxLength="2" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX91" runat="server" Columns="2" MaxLength="2" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX92" runat="server" Columns="3" MaxLength="3" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX93" runat="server" Columns="2" MaxLength="2" TabIndex="20407" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX94" runat="server" Columns="2" MaxLength="2" TabIndex="20408" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX95" runat="server" Columns="3" MaxLength="3" TabIndex="20409" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX96" runat="server" Columns="2" MaxLength="2" TabIndex="20410" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX97" runat="server" Columns="2" MaxLength="2" TabIndex="20411" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX98" runat="server" Columns="3" MaxLength="3" TabIndex="20412" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX99" runat="server" Columns="2" MaxLength="2" TabIndex="20413" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX100" runat="server" Columns="2" MaxLength="2" TabIndex="20414" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX101" runat="server" Columns="3" MaxLength="3" TabIndex="20415" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX102" runat="server" Columns="2" MaxLength="2" TabIndex="20416" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX103" runat="server" Columns="2" MaxLength="2" TabIndex="20417" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX104" runat="server" Columns="3" MaxLength="3" TabIndex="20418" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX105" runat="server" Columns="2" MaxLength="2" TabIndex="20419" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX106" runat="server" Columns="2" MaxLength="2" TabIndex="20420" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX107" runat="server" Columns="3" MaxLength="3" TabIndex="20421" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX108" runat="server" Columns="2" MaxLength="2" TabIndex="20422" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX109" runat="server" Columns="2" MaxLength="2" TabIndex="20423" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX110" runat="server" Columns="3" MaxLength="3" TabIndex="20424" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMotriz" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Discapacidad motriz"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX111" runat="server" Columns="2" MaxLength="2" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX112" runat="server" Columns="2" MaxLength="2" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX113" runat="server" Columns="3" MaxLength="3" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX114" runat="server" Columns="2" MaxLength="2" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX115" runat="server" Columns="2" MaxLength="2" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX116" runat="server" Columns="3" MaxLength="3" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX117" runat="server" Columns="2" MaxLength="2" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX118" runat="server" Columns="2" MaxLength="2" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX119" runat="server" Columns="3" MaxLength="3" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX120" runat="server" Columns="2" MaxLength="2" TabIndex="20510" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX121" runat="server" Columns="2" MaxLength="2" TabIndex="20511" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX122" runat="server" Columns="3" MaxLength="3" TabIndex="20512" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX123" runat="server" Columns="2" MaxLength="2" TabIndex="20513" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX124" runat="server" Columns="2" MaxLength="2" TabIndex="20514" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX125" runat="server" Columns="3" MaxLength="3" TabIndex="20515" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX126" runat="server" Columns="2" MaxLength="2" TabIndex="20516" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX127" runat="server" Columns="2" MaxLength="2" TabIndex="20517" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX128" runat="server" Columns="3" MaxLength="3" TabIndex="20518" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX129" runat="server" Columns="2" MaxLength="2" TabIndex="20519" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX130" runat="server" Columns="2" MaxLength="2" TabIndex="20520" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX131" runat="server" Columns="3" MaxLength="3" TabIndex="20521" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX132" runat="server" Columns="2" MaxLength="2" TabIndex="20522" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX133" runat="server" Columns="2" MaxLength="2" TabIndex="20523" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX134" runat="server" Columns="3" MaxLength="3" TabIndex="20524" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblIntelectual" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Discapacidad Intelectual"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX135" runat="server" Columns="2" MaxLength="2" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX136" runat="server" Columns="2" MaxLength="2" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX137" runat="server" Columns="3" MaxLength="3" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX138" runat="server" Columns="2" MaxLength="2" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX139" runat="server" Columns="2" MaxLength="2" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX140" runat="server" Columns="3" MaxLength="3" TabIndex="20606" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX141" runat="server" Columns="2" MaxLength="2" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX142" runat="server" Columns="2" MaxLength="2" TabIndex="20608" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX143" runat="server" Columns="3" MaxLength="3" TabIndex="20609" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX144" runat="server" Columns="2" MaxLength="2" TabIndex="20610" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX145" runat="server" Columns="2" MaxLength="2" TabIndex="20611" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX146" runat="server" Columns="3" MaxLength="3" TabIndex="20612" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX147" runat="server" Columns="2" MaxLength="2" TabIndex="20613" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX148" runat="server" Columns="2" MaxLength="2" TabIndex="20614" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX149" runat="server" Columns="3" MaxLength="3" TabIndex="20615" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX150" runat="server" Columns="2" MaxLength="2" TabIndex="20616" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX151" runat="server" Columns="2" MaxLength="2" TabIndex="20617" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX152" runat="server" Columns="3" MaxLength="3" TabIndex="20618" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX153" runat="server" Columns="2" MaxLength="2" TabIndex="20619" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX154" runat="server" Columns="2" MaxLength="2" TabIndex="20620" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX155" runat="server" Columns="3" MaxLength="3" TabIndex="20621" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX156" runat="server" Columns="2" MaxLength="2" TabIndex="20622" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX157" runat="server" Columns="2" MaxLength="2" TabIndex="20623" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX158" runat="server" Columns="3" MaxLength="3" TabIndex="20624" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMultiple" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Discapacidad M�ltiple"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX159" runat="server" Columns="2" MaxLength="2" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX160" runat="server" Columns="2" MaxLength="2" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX161" runat="server" Columns="3" MaxLength="3" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX162" runat="server" Columns="2" MaxLength="2" TabIndex="20704" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX163" runat="server" Columns="2" MaxLength="2" TabIndex="20705" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX164" runat="server" Columns="3" MaxLength="3" TabIndex="20706" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX165" runat="server" Columns="2" MaxLength="2" TabIndex="20707" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX166" runat="server" Columns="2" MaxLength="2" TabIndex="20708" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX167" runat="server" Columns="3" MaxLength="3" TabIndex="20709" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX168" runat="server" Columns="2" MaxLength="2" TabIndex="20710" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX169" runat="server" Columns="2" MaxLength="2" TabIndex="20711" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX170" runat="server" Columns="3" MaxLength="3" TabIndex="20712" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX171" runat="server" Columns="2" MaxLength="2" TabIndex="20713" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX172" runat="server" Columns="2" MaxLength="2" TabIndex="20714" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX173" runat="server" Columns="3" MaxLength="3" TabIndex="20715" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX174" runat="server" Columns="2" MaxLength="2" TabIndex="20716" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX175" runat="server" Columns="2" MaxLength="2" TabIndex="20717" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX176" runat="server" Columns="3" MaxLength="3" TabIndex="20718" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX177" runat="server" Columns="2" MaxLength="2" TabIndex="20719" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX178" runat="server" Columns="2" MaxLength="2" TabIndex="20720" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX179" runat="server" Columns="3" MaxLength="3" TabIndex="20721" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX180" runat="server" Columns="2" MaxLength="2" TabIndex="20722" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX181" runat="server" Columns="2" MaxLength="2" TabIndex="20723" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX182" runat="server" Columns="3" MaxLength="3" TabIndex="20724" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblAutismo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Autismo"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX183" runat="server" Columns="2" MaxLength="2" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX184" runat="server" Columns="2" MaxLength="2" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX185" runat="server" Columns="3" MaxLength="3" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX186" runat="server" Columns="2" MaxLength="2" TabIndex="20804" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX187" runat="server" Columns="2" MaxLength="2" TabIndex="20805" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX188" runat="server" Columns="3" MaxLength="3" TabIndex="20806" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX189" runat="server" Columns="2" MaxLength="2" TabIndex="20807" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX190" runat="server" Columns="2" MaxLength="2" TabIndex="20808" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX191" runat="server" Columns="3" MaxLength="3" TabIndex="20809" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX192" runat="server" Columns="2" MaxLength="2" TabIndex="20810" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX193" runat="server" Columns="2" MaxLength="2" TabIndex="20811" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX194" runat="server" Columns="3" MaxLength="3" TabIndex="20812" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX195" runat="server" Columns="2" MaxLength="2" TabIndex="20813" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX196" runat="server" Columns="2" MaxLength="2" TabIndex="20814" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX197" runat="server" Columns="3" MaxLength="3" TabIndex="20815" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX198" runat="server" Columns="2" MaxLength="2" TabIndex="20816" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX199" runat="server" Columns="2" MaxLength="2" TabIndex="20817" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX200" runat="server" Columns="3" MaxLength="3" TabIndex="20818" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX201" runat="server" Columns="2" MaxLength="2" TabIndex="20819" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX202" runat="server" Columns="2" MaxLength="2" TabIndex="20820" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX203" runat="server" Columns="3" MaxLength="3" TabIndex="20821" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX204" runat="server" Columns="2" MaxLength="2" TabIndex="20822" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX205" runat="server" Columns="2" MaxLength="2" TabIndex="20823" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX206" runat="server" Columns="3" MaxLength="3" TabIndex="20824" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblSobresalientes" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Aptitudes sobresalientes"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX207" runat="server" Columns="2" MaxLength="2" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX208" runat="server" Columns="2" MaxLength="2" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX209" runat="server" Columns="3" MaxLength="3" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX210" runat="server" Columns="2" MaxLength="2" TabIndex="20904" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX211" runat="server" Columns="2" MaxLength="2" TabIndex="20905" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX212" runat="server" Columns="3" MaxLength="3" TabIndex="20906" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX213" runat="server" Columns="2" MaxLength="2" TabIndex="20907" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX214" runat="server" Columns="2" MaxLength="2" TabIndex="20908" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX215" runat="server" Columns="3" MaxLength="3" TabIndex="20909" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX216" runat="server" Columns="2" MaxLength="2" TabIndex="20910" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX217" runat="server" Columns="2" MaxLength="2" TabIndex="20911" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX218" runat="server" Columns="3" MaxLength="3" TabIndex="20912" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX219" runat="server" Columns="2" MaxLength="2" TabIndex="20913" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX220" runat="server" Columns="2" MaxLength="2" TabIndex="20914" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX221" runat="server" Columns="3" MaxLength="3" TabIndex="20915" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX222" runat="server" Columns="2" MaxLength="2" TabIndex="20916" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX223" runat="server" Columns="2" MaxLength="2" TabIndex="20917" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX224" runat="server" Columns="3" MaxLength="3" TabIndex="20918" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX225" runat="server" Columns="2" MaxLength="2" TabIndex="20919" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX226" runat="server" Columns="2" MaxLength="2" TabIndex="20920" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX227" runat="server" Columns="3" MaxLength="3" TabIndex="20921" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX228" runat="server" Columns="2" MaxLength="2" TabIndex="20922" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX229" runat="server" Columns="2" MaxLength="2" TabIndex="20923" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX230" runat="server" Columns="3" MaxLength="3" TabIndex="20924" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblComunicacion" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Problemas de comunicaci�n"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX231" runat="server" Columns="2" MaxLength="2" TabIndex="21001" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX232" runat="server" Columns="2" MaxLength="2" TabIndex="21002" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX233" runat="server" Columns="3" MaxLength="3" TabIndex="21003" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX234" runat="server" Columns="2" MaxLength="2" TabIndex="21004" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX235" runat="server" Columns="2" MaxLength="2" TabIndex="21005" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX236" runat="server" Columns="3" MaxLength="3" TabIndex="21006" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX237" runat="server" Columns="2" MaxLength="2" TabIndex="21007" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX238" runat="server" Columns="2" MaxLength="2" TabIndex="21008" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX239" runat="server" Columns="3" MaxLength="3" TabIndex="21009" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX240" runat="server" Columns="2" MaxLength="2" TabIndex="21010" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX241" runat="server" Columns="2" MaxLength="2" TabIndex="21011" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX242" runat="server" Columns="3" MaxLength="3" TabIndex="21012" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX243" runat="server" Columns="2" MaxLength="2" TabIndex="21013" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX244" runat="server" Columns="2" MaxLength="2" TabIndex="21014" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX245" runat="server" Columns="3" MaxLength="3" TabIndex="21015" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX246" runat="server" Columns="2" MaxLength="2" TabIndex="21016" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX247" runat="server" Columns="2" MaxLength="2" TabIndex="21017" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX248" runat="server" Columns="3" MaxLength="3" TabIndex="21018" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX249" runat="server" Columns="2" MaxLength="2" TabIndex="21019" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX250" runat="server" Columns="2" MaxLength="2" TabIndex="21020" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX251" runat="server" Columns="3" MaxLength="3" TabIndex="21021" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX252" runat="server" Columns="2" MaxLength="2" TabIndex="21022" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX253" runat="server" Columns="2" MaxLength="2" TabIndex="21023" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX254" runat="server" Columns="3" MaxLength="3" TabIndex="21024" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="Conducta" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Problemas de conducta"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX255" runat="server" Columns="2" MaxLength="2" TabIndex="21101" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX256" runat="server" Columns="2" MaxLength="2" TabIndex="21102" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX257" runat="server" Columns="3" MaxLength="3" TabIndex="21103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX258" runat="server" Columns="2" MaxLength="2" TabIndex="21104" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX259" runat="server" Columns="2" MaxLength="2" TabIndex="21105" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX260" runat="server" Columns="3" MaxLength="3" TabIndex="21106" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX261" runat="server" Columns="2" MaxLength="2" TabIndex="21107" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX262" runat="server" Columns="2" MaxLength="2" TabIndex="21108" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX263" runat="server" Columns="3" MaxLength="3" TabIndex="21109" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX264" runat="server" Columns="2" MaxLength="2" TabIndex="21110" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX265" runat="server" Columns="2" MaxLength="2" TabIndex="21111" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX266" runat="server" Columns="3" MaxLength="3" TabIndex="21112" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX267" runat="server" Columns="2" MaxLength="2" TabIndex="21113" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX268" runat="server" Columns="2" MaxLength="2" TabIndex="21114" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX269" runat="server" Columns="3" MaxLength="3" TabIndex="21115" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX270" runat="server" Columns="2" MaxLength="2" TabIndex="21116" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX271" runat="server" Columns="2" MaxLength="2" TabIndex="21117" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX272" runat="server" Columns="3" MaxLength="3" TabIndex="21118" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX273" runat="server" Columns="2" MaxLength="2" TabIndex="21119" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX274" runat="server" Columns="2" MaxLength="2" TabIndex="21120" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX275" runat="server" Columns="3" MaxLength="3" TabIndex="21121" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX276" runat="server" Columns="2" MaxLength="2" TabIndex="21122" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX277" runat="server" Columns="2" MaxLength="2" TabIndex="21123" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX278" runat="server" Columns="3" MaxLength="3" TabIndex="21124" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblOtro" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Otro"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX279" runat="server" Columns="2" MaxLength="2" TabIndex="21201" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX280" runat="server" Columns="2" MaxLength="2" TabIndex="21202" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX281" runat="server" Columns="3" MaxLength="3" TabIndex="21203" CssClass="lblNegro
                    "></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX282" runat="server" Columns="2" MaxLength="2" TabIndex="21204" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX283" runat="server" Columns="2" MaxLength="2" TabIndex="21205" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX284" runat="server" Columns="3" MaxLength="3" TabIndex="21206" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX285" runat="server" Columns="2" MaxLength="2" TabIndex="21207" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX286" runat="server" Columns="2" MaxLength="2" TabIndex="21208" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX287" runat="server" Columns="3" MaxLength="3" TabIndex="21209" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX288" runat="server" Columns="2" MaxLength="2" TabIndex="21210" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX289" runat="server" Columns="2" MaxLength="2" TabIndex="21211" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX290" runat="server" Columns="3" MaxLength="3" TabIndex="21212" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX291" runat="server" Columns="2" MaxLength="2" TabIndex="21213" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX292" runat="server" Columns="2" MaxLength="2" TabIndex="21214" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX293" runat="server" Columns="3" MaxLength="3" TabIndex="21215" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX294" runat="server" Columns="2" MaxLength="2" TabIndex="21216" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX295" runat="server" Columns="2" MaxLength="2" TabIndex="21217" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX296" runat="server" Columns="3" MaxLength="3" TabIndex="21218" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX297" runat="server" Columns="2" MaxLength="2" TabIndex="21219" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX298" runat="server" Columns="2" MaxLength="2" TabIndex="21220" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX299" runat="server" Columns="3" MaxLength="3" TabIndex="21221" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX300" runat="server" Columns="2" MaxLength="2" TabIndex="21222" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX301" runat="server" Columns="2" MaxLength="2" TabIndex="21223" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX302" runat="server" Columns="3" MaxLength="3" TabIndex="21224" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo Sombreado">
                    <asp:Label ID="lblTotal2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX303" runat="server" Columns="2" MaxLength="2" TabIndex="21301" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX304" runat="server" Columns="2" MaxLength="2" TabIndex="21302" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX305" runat="server" Columns="3" MaxLength="3" TabIndex="21303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX306" runat="server" Columns="2" MaxLength="2" TabIndex="21304" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX307" runat="server" Columns="2" MaxLength="2" TabIndex="21305" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX308" runat="server" Columns="3" MaxLength="3" TabIndex="21306" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX309" runat="server" Columns="2" MaxLength="2" TabIndex="21307" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX310" runat="server" Columns="2" MaxLength="2" TabIndex="21308" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX311" runat="server" Columns="3" MaxLength="3" TabIndex="21309" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX312" runat="server" Columns="2" MaxLength="2" TabIndex="21310" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado" style="width: 42px">
                    <asp:TextBox ID="txtVANX313" runat="server" Columns="2" MaxLength="2" TabIndex="21311" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX314" runat="server" Columns="3" MaxLength="3" TabIndex="21312" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX315" runat="server" Columns="2" MaxLength="2" TabIndex="21313" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX316" runat="server" Columns="2" MaxLength="2" TabIndex="21314" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX317" runat="server" Columns="3" MaxLength="3" TabIndex="21315" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX318" runat="server" Columns="2" MaxLength="2" TabIndex="21316" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX319" runat="server" Columns="2" MaxLength="2" TabIndex="21317" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX320" runat="server" Columns="3" MaxLength="3" TabIndex="21318" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX321" runat="server" Columns="2" MaxLength="2" TabIndex="21319" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX322" runat="server" Columns="2" MaxLength="2" TabIndex="21320" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX323" runat="server" Columns="3" MaxLength="3" TabIndex="21321" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX324" runat="server" Columns="2" MaxLength="2" TabIndex="21322" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX325" runat="server" Columns="2" MaxLength="2" TabIndex="21323" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtVANX326" runat="server" Columns="3" MaxLength="3" TabIndex="21324" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            
        </table>
        <br />
        <table style="width: 1000px">
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="lblALUMNOS" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="III. Apoyo de Educaci�n Especial" ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label30" runat="server" CssClass="lblRojo" Font-Bold="True" Text='3. Si la escuela cuenta con alg�n servicio de apoyo de Educaci�n Especial o ninguno, marque con una "X" en la columna correspondiente, si recibe apoyo de otro servicio, especifique . Marque qu� tipo de atenci�n recibe.'
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center" valign="top" colspan="2">
                    <table>
                        <tr>
                            <td style="width: 265px" colspan="4">
                                <asp:Label ID="Label22" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TIPO DE SERVICIO"
                                    ></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left; vertical-align: top; width: 265px;">
                                <asp:RadioButton ID="optVANX327" runat="server" GroupName="Servicio" TabIndex="30101" onkeydown="return Arrows(event,this.tabIndex)"/>
                                <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="UNIDAD DE SERVICIOS DE APOYO A LA EDUCACI�N REGULAR (USAER)"></asp:Label>
                                <asp:TextBox ID="txtVANX327" runat="server" Width="16px"  style="visibility:hidden;" Height="12px" ></asp:TextBox></td>
                            <td style="vertical-align: top; width: 178px; text-align: left">
                                <asp:RadioButton ID="optVANX328" runat="server" GroupName="Servicio" TabIndex="30102" onkeydown="return Arrows(event,this.tabIndex)"/>
                                <asp:Label ID="Label19" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CENTRO DE ATENCI�N M�LTIPLE (CAM)"></asp:Label>
                                <asp:TextBox ID="txtVANX328" runat="server" Width="1px" style="visibility:hidden;" Height="12px" ></asp:TextBox></td>
                            <td style="vertical-align: top; width: 109px; text-align: left">
                                <asp:RadioButton ID="optVANX329" runat="server" GroupName="Servicio" TabIndex="30103" onkeydown="return Arrows(event,this.tabIndex)" />
                                <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NINGUNO"></asp:Label>
                                <asp:TextBox ID="txtVANX329" runat="server" Width="1px" style="visibility:hidden;" Height="12px" ></asp:TextBox></td>
                            <td style="vertical-align: top; text-align: left">
                                <asp:RadioButton ID="optVANX330" runat="server" GroupName="Servicio" TabIndex="30104" onkeydown="return Arrows(event,this.tabIndex)" />
                                <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRO (ESPECIFIQUE)"></asp:Label>
                                <asp:TextBox ID="txtVANX330" runat="server" Width="1px" style="visibility:hidden;" Height="12px" ></asp:TextBox>
                                <asp:TextBox ID="txtVANX659" runat="server" Columns="60" CssClass="lblNegro" MaxLength="60"
                                    TabIndex="30105"></asp:TextBox></td><%--onkeypress="return EscribeOtros(event);"--%>
                                    
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left" valign="top">
                    <table>
                        <tr>
                            <td style="height: 19px" colspan="4">
                                <asp:Label ID="Label23" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TIPO DE ATENCI�N"
                                    ></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                </td>
                            <td>
                                <asp:RadioButton ID="optVANX332" runat="server" TabIndex="30201" onkeydown="return Arrows(event,this.tabIndex)" GroupName="Atencion"/>
                                <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ITINERANTE"></asp:Label>
                                <asp:TextBox ID="txtVANX332" runat="server" Width="16px" style="visibility:hidden;" ></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                                <asp:RadioButton ID="optVANX331" runat="server" TabIndex="30202" onkeydown="return Arrows(event,this.tabIndex)" GroupName="Atencion"/>
                                <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PERMANENTE" ></asp:Label>
                                <asp:TextBox ID="txtVANX331" runat="server" Width="16px"  style="visibility:hidden;" ></asp:TextBox>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label31" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="IV. Respuesta Educativa" ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Escriba el n�mero de alumnos con Necesidades Educativas Especiales que cuentan con Informe de Evaluaci�n Psicopedag�gica."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX645" runat="server" Columns="2" MaxLength="2" TabIndex="30401" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX646" runat="server" Columns="2" MaxLength="2" TabIndex="30402" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX647" runat="server" Columns="3" MaxLength="3" TabIndex="30403" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5. Escriba el n�mero de alumnos con Necesidades Educativas Especiales que cuentan con Propuesta Curricular Adaptada."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX648" runat="server" Columns="2" MaxLength="2" TabIndex="30601" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX649" runat="server" Columns="2" MaxLength="2" TabIndex="30602" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX650" runat="server" Columns="3" MaxLength="3" TabIndex="30603" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6. Del total de alumnos con Necesidades Educativas Especiales escriba el n�mero de promovidos y no promovidos desglos�ndolos por sexo."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center" valign="top" colspan="2">
                    &nbsp;<table>
                        <tr>
                            <td style="height: 70px;">
                    <table>
                        <tr>
                            <td colspan="8">
                                <asp:Label ID="Label13" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PROMOVIDOS/APROBADOS"
                                    ></asp:Label></td>
                            <td colspan="1" style="width: 47px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX651" runat="server" Columns="3" MaxLength="3" TabIndex="30801" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 8px">
                            </td>
                            <td>
                                <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX652" runat="server" Columns="3" MaxLength="3" TabIndex="30802" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 10px">
                            </td>
                            <td>
                                <asp:Label ID="Label28" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX653" runat="server" Columns="3" MaxLength="3" TabIndex="30803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 47px">
                                &nbsp; &nbsp; &nbsp; &nbsp;
                            </td>
                        </tr>
                    </table>
                            </td>
                            <td style="height: 70px;">
                    <table>
                        <tr>
                            <td colspan="8" style="height: 20px">
                                <asp:Label ID="Label14" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NO PROMOVIDOS/REPROBADOS"
                                    ></asp:Label></td>
                            <td colspan="1" style="width: 74px; height: 20px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX654" runat="server" Columns="3" MaxLength="3" TabIndex="30804" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 14px">
                            </td>
                            <td>
                                <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX655" runat="server" Columns="3" MaxLength="3" TabIndex="30805" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 7px">
                            </td>
                            <td>
                                <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX656" runat="server" Columns="3" MaxLength="3" TabIndex="30806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 74px">
                                &nbsp; &nbsp; &nbsp; &nbsp;
                            </td>
                        </tr>
                    </table>
                            </td>
                            <td style="width: 100px; height: 70px;">
                                <table>
                                    <tr>
                                        <td style="width: 120px">
                                            <asp:Label ID="Label44" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                                ></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px">
                                            <asp:TextBox ID="txtVANX657" runat="server" Columns="3" MaxLength="3" TabIndex="30807" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label32" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="V. Alumnos Hospitalizados" ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7. Escriba el n�mero de alumnos que por motivos de salud estuvieron hospitalizados y se ausentaron por m�s de un mes del sal�n de clases."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; height: 64px;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX661" runat="server" Columns="2" MaxLength="2" TabIndex="31001" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX662" runat="server" Columns="2" MaxLength="2" TabIndex="31002" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX663" runat="server" Columns="3" MaxLength="3" TabIndex="31003" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        &nbsp;<br />
           
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Inmueble_911_8G',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir a p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Inmueble_911_8G',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Oficializar_911_8G',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Oficializar_911_8G',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir a p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
     <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
        
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
         
         <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                 function PintaOPTs(){
                     marcar('VANX327');
                     marcar('VANX328');
                     marcar('VANX329');
                     marcar('VANX330');
                     marcar('VANX331');
                     marcar('VANX332');
                }  
                
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             } 
                              if (variable == 'VANX330')
                                     if (chk.checked) document.getElementById('ctl00_cphMainMaster_txtVANX659').disabled = false;
                                     else  document.getElementById('ctl00_cphMainMaster_txtVANX659').disabled = true;
                                     
//                           if (variable == 'VANX329'){
//                             if(chk.checked){
//                                 document.getElementById('optVANX331').checked = false;
//                                 document.getElementById('optVANX331').disabled = true;
//                                 document.getElementById('optVANX332').checked = false;
//                                 document.getElementById('optVANX332').disabled = true;
//                             }
//                             else{
//                                 document.getElementById('optVANX331').disabled = false;
//                                 document.getElementById('optVANX332').disabled = false;
//                             }
//                           }         
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }
                
                function OPTs2Txt(){
                     marcarTXT('VANX329');
                     marcarTXT('VANX327');
                     marcarTXT('VANX328');
                     marcarTXT('VANX330');
                     marcarTXT('VANX331');
                     marcarTXT('VANX332');
                }    
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         
                         var txt = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                           
//                         if (variable == 'VANX329'){
//                             if(chk.checked){
//                                 document.getElementById('optVANX331').checked = false;
//                                 document.getElementById('optVANX331').disabled = true;
//                                 document.getElementById('optVANX332').checked = false;
//                                 document.getElementById('optVANX332').disabled = true;
//                             }
//                             else{
//                                 document.getElementById('optVANX331').disabled = false;
//                                 document.getElementById('optVANX332').disabled = false;
//                             }
//                         }  
                         if (variable == 'VANX330'){
                               if (chk.checked) document.getElementById('ctl00_cphMainMaster_txtVANX659').disabled = false;
                               else  {
                                    
                                  document.getElementById('ctl00_cphMainMaster_txtVANX659').disabled = true;
                                  document.getElementById('ctl00_cphMainMaster_txtVANX659').value = '';
                               }
                         }
                     }
                } 
                
             
               PintaOPTs();
               Disparador(<%=hidDisparador.Value %>);             
        </script>
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
