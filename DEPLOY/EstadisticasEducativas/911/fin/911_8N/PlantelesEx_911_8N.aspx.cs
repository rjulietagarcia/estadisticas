using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_8N
{
    public partial class PlantelesEx_911_8N : System.Web.UI.Page, ICallbackEventHandler
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV531.Attributes["onkeypress"] = "return Num(event)";
                txtV537.Attributes["onkeypress"] = "return Num(event)";
                txtV538.Attributes["onkeypress"] = "return Num(event)";
                txtV539.Attributes["onkeypress"] = "return Num(event)";
                txtV540.Attributes["onkeypress"] = "return Num(event)";
                txtV541.Attributes["onkeypress"] = "return Num(event)";
                txtV542.Attributes["onkeypress"] = "return Num(event)";
                txtV543.Attributes["onkeypress"] = "return Num(event)";
                txtV544.Attributes["onkeypress"] = "return Num(event)";
                txtV545.Attributes["onkeypress"] = "return Num(event)";
                txtV546.Attributes["onkeypress"] = "return Num(event)";
                txtV547.Attributes["onkeypress"] = "return Num(event)";
                txtV548.Attributes["onkeypress"] = "return Num(event)";
                txtV549.Attributes["onkeypress"] = "return Num(event)";
                txtV550.Attributes["onkeypress"] = "return Num(event)";
                txtV551.Attributes["onkeypress"] = "return Num(event)";
                txtV552.Attributes["onkeypress"] = "return Num(event)";
                txtV553.Attributes["onkeypress"] = "return Num(event)";
                txtV554.Attributes["onkeypress"] = "return Num(event)";
                txtV555.Attributes["onkeypress"] = "return Num(event)";
                txtV556.Attributes["onkeypress"] = "return Num(event)";
                txtV557.Attributes["onkeypress"] = "return Num(event)";
                txtV558.Attributes["onkeypress"] = "return Num(event)";
                txtV559.Attributes["onkeypress"] = "return Num(event)";
                txtV559.Attributes["onkeypress"] = "return Num(event)";
                txtV560.Attributes["onkeypress"] = "return Num(event)";
                txtV561.Attributes["onkeypress"] = "return Num(event)";
                txtV562.Attributes["onkeypress"] = "return Num(event)";
                txtV563.Attributes["onkeypress"] = "return Num(event)";
                txtV564.Attributes["onkeypress"] = "return Num(event)";
                #endregion


                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion


                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }
        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }
    }
}
