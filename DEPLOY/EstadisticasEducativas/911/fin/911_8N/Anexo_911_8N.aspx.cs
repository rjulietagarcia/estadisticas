using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_8N
{
    public partial class Anexo_911_8N : System.Web.UI.Page, ICallbackEventHandler
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtVANX15.Attributes["onkeypress"] = "return Num(event)";
                txtVANX16.Attributes["onkeypress"] = "return Num(event)";
                txtVANX17.Attributes["onkeypress"] = "return Num(event)";
                txtVANX18.Attributes["onkeypress"] = "return Num(event)";
                txtVANX19.Attributes["onkeypress"] = "return Num(event)";
                txtVANX20.Attributes["onkeypress"] = "return Num(event)";
                txtVANX21.Attributes["onkeypress"] = "return Num(event)";
                txtVANX22.Attributes["onkeypress"] = "return Num(event)";
                txtVANX23.Attributes["onkeypress"] = "return Num(event)";
                txtVANX24.Attributes["onkeypress"] = "return Num(event)";
                txtVANX25.Attributes["onkeypress"] = "return Num(event)";
                txtVANX26.Attributes["onkeypress"] = "return Num(event)";
                txtVANX27.Attributes["onkeypress"] = "return Num(event)";
                txtVANX28.Attributes["onkeypress"] = "return Num(event)";
                txtVANX29.Attributes["onkeypress"] = "return Num(event)";
                txtVANX30.Attributes["onkeypress"] = "return Num(event)";
                txtVANX31.Attributes["onkeypress"] = "return Num(event)";
                txtVANX32.Attributes["onkeypress"] = "return Num(event)";
                txtVANX33.Attributes["onkeypress"] = "return Num(event)";
                txtVANX34.Attributes["onkeypress"] = "return Num(event)";
                txtVANX35.Attributes["onkeypress"] = "return Num(event)";
                txtVANX36.Attributes["onkeypress"] = "return Num(event)";
                txtVANX37.Attributes["onkeypress"] = "return Num(event)";
                txtVANX38.Attributes["onkeypress"] = "return Num(event)";
                txtVANX39.Attributes["onkeypress"] = "return Num(event)";
                txtVANX40.Attributes["onkeypress"] = "return Num(event)";
                txtVANX41.Attributes["onkeypress"] = "return Num(event)";
                txtVANX42.Attributes["onkeypress"] = "return Num(event)";
                txtVANX43.Attributes["onkeypress"] = "return Num(event)";
                txtVANX44.Attributes["onkeypress"] = "return Num(event)";
                txtVANX45.Attributes["onkeypress"] = "return Num(event)";
                txtVANX46.Attributes["onkeypress"] = "return Num(event)";
                txtVANX47.Attributes["onkeypress"] = "return Num(event)";
                txtVANX48.Attributes["onkeypress"] = "return Num(event)";
                txtVANX49.Attributes["onkeypress"] = "return Num(event)";
                txtVANX50.Attributes["onkeypress"] = "return Num(event)";
                txtVANX51.Attributes["onkeypress"] = "return Num(event)";
                txtVANX52.Attributes["onkeypress"] = "return Num(event)";
                txtVANX53.Attributes["onkeypress"] = "return Num(event)";
                txtVANX54.Attributes["onkeypress"] = "return Num(event)";
                txtVANX55.Attributes["onkeypress"] = "return Num(event)";
                txtVANX56.Attributes["onkeypress"] = "return Num(event)";
                txtVANX57.Attributes["onkeypress"] = "return Num(event)";
                txtVANX58.Attributes["onkeypress"] = "return Num(event)";
                txtVANX59.Attributes["onkeypress"] = "return Num(event)";
                txtVANX60.Attributes["onkeypress"] = "return Num(event)";
                txtVANX61.Attributes["onkeypress"] = "return Num(event)";
                txtVANX62.Attributes["onkeypress"] = "return Num(event)";
                txtVANX63.Attributes["onkeypress"] = "return Num(event)";
                txtVANX64.Attributes["onkeypress"] = "return Num(event)";
                txtVANX65.Attributes["onkeypress"] = "return Num(event)";
                txtVANX66.Attributes["onkeypress"] = "return Num(event)";
                txtVANX67.Attributes["onkeypress"] = "return Num(event)";
                txtVANX68.Attributes["onkeypress"] = "return Num(event)";
                txtVANX69.Attributes["onkeypress"] = "return Num(event)";
                txtVANX70.Attributes["onkeypress"] = "return Num(event)";
                txtVANX71.Attributes["onkeypress"] = "return Num(event)";
                txtVANX72.Attributes["onkeypress"] = "return Num(event)";
                txtVANX73.Attributes["onkeypress"] = "return Num(event)";
                txtVANX74.Attributes["onkeypress"] = "return Num(event)";
                txtVANX75.Attributes["onkeypress"] = "return Num(event)";
                txtVANX76.Attributes["onkeypress"] = "return Num(event)";
                txtVANX77.Attributes["onkeypress"] = "return Num(event)";
                txtVANX78.Attributes["onkeypress"] = "return Num(event)";
                txtVANX79.Attributes["onkeypress"] = "return Num(event)";
                txtVANX80.Attributes["onkeypress"] = "return Num(event)";
                txtVANX81.Attributes["onkeypress"] = "return Num(event)";
                txtVANX82.Attributes["onkeypress"] = "return Num(event)";
                txtVANX83.Attributes["onkeypress"] = "return Num(event)";
                txtVANX84.Attributes["onkeypress"] = "return Num(event)";
                txtVANX85.Attributes["onkeypress"] = "return Num(event)";
                txtVANX86.Attributes["onkeypress"] = "return Num(event)";
                txtVANX87.Attributes["onkeypress"] = "return Num(event)";
                txtVANX88.Attributes["onkeypress"] = "return Num(event)";
                txtVANX89.Attributes["onkeypress"] = "return Num(event)";
                txtVANX90.Attributes["onkeypress"] = "return Num(event)";
                txtVANX91.Attributes["onkeypress"] = "return Num(event)";
                txtVANX92.Attributes["onkeypress"] = "return Num(event)";
                txtVANX93.Attributes["onkeypress"] = "return Num(event)";
                txtVANX94.Attributes["onkeypress"] = "return Num(event)";
                txtVANX95.Attributes["onkeypress"] = "return Num(event)";
                txtVANX96.Attributes["onkeypress"] = "return Num(event)";
                txtVANX97.Attributes["onkeypress"] = "return Num(event)";
                txtVANX98.Attributes["onkeypress"] = "return Num(event)";
                txtVANX99.Attributes["onkeypress"] = "return Num(event)";
                txtVANX100.Attributes["onkeypress"] = "return Num(event)";
                txtVANX101.Attributes["onkeypress"] = "return Num(event)";
                txtVANX102.Attributes["onkeypress"] = "return Num(event)";
                txtVANX103.Attributes["onkeypress"] = "return Num(event)";
                txtVANX104.Attributes["onkeypress"] = "return Num(event)";
                txtVANX105.Attributes["onkeypress"] = "return Num(event)";
                txtVANX106.Attributes["onkeypress"] = "return Num(event)";
                txtVANX107.Attributes["onkeypress"] = "return Num(event)";
                txtVANX108.Attributes["onkeypress"] = "return Num(event)";
                txtVANX109.Attributes["onkeypress"] = "return Num(event)";
                txtVANX110.Attributes["onkeypress"] = "return Num(event)";
                txtVANX111.Attributes["onkeypress"] = "return Num(event)";
                txtVANX112.Attributes["onkeypress"] = "return Num(event)";
                txtVANX113.Attributes["onkeypress"] = "return Num(event)";
                txtVANX114.Attributes["onkeypress"] = "return Num(event)";
                txtVANX115.Attributes["onkeypress"] = "return Num(event)";
                txtVANX116.Attributes["onkeypress"] = "return Num(event)";
                txtVANX117.Attributes["onkeypress"] = "return Num(event)";
                txtVANX118.Attributes["onkeypress"] = "return Num(event)";
                txtVANX119.Attributes["onkeypress"] = "return Num(event)";
                txtVANX120.Attributes["onkeypress"] = "return Num(event)";
                txtVANX121.Attributes["onkeypress"] = "return Num(event)";
                txtVANX122.Attributes["onkeypress"] = "return Num(event)";
                txtVANX123.Attributes["onkeypress"] = "return Num(event)";
                txtVANX124.Attributes["onkeypress"] = "return Num(event)";
                txtVANX125.Attributes["onkeypress"] = "return Num(event)";
                txtVANX126.Attributes["onkeypress"] = "return Num(event)";
                txtVANX127.Attributes["onkeypress"] = "return Num(event)";
                txtVANX128.Attributes["onkeypress"] = "return Num(event)";
                txtVANX129.Attributes["onkeypress"] = "return Num(event)";
                txtVANX130.Attributes["onkeypress"] = "return Num(event)";
                txtVANX131.Attributes["onkeypress"] = "return Num(event)";
                txtVANX132.Attributes["onkeypress"] = "return Num(event)";
                txtVANX133.Attributes["onkeypress"] = "return Num(event)";
                txtVANX134.Attributes["onkeypress"] = "return Num(event)";
                txtVANX135.Attributes["onkeypress"] = "return Num(event)";
                txtVANX136.Attributes["onkeypress"] = "return Num(event)";
                txtVANX137.Attributes["onkeypress"] = "return Num(event)";
                txtVANX138.Attributes["onkeypress"] = "return Num(event)";
                txtVANX139.Attributes["onkeypress"] = "return Num(event)";
                txtVANX140.Attributes["onkeypress"] = "return Num(event)";
                txtVANX141.Attributes["onkeypress"] = "return Num(event)";
                txtVANX142.Attributes["onkeypress"] = "return Num(event)";
                txtVANX143.Attributes["onkeypress"] = "return Num(event)";
                txtVANX144.Attributes["onkeypress"] = "return Num(event)";
                txtVANX145.Attributes["onkeypress"] = "return Num(event)";
                txtVANX146.Attributes["onkeypress"] = "return Num(event)";
                txtVANX147.Attributes["onkeypress"] = "return Num(event)";
                txtVANX148.Attributes["onkeypress"] = "return Num(event)";
                txtVANX149.Attributes["onkeypress"] = "return Num(event)";
                txtVANX150.Attributes["onkeypress"] = "return Num(event)";
                txtVANX151.Attributes["onkeypress"] = "return Num(event)";
                txtVANX152.Attributes["onkeypress"] = "return Num(event)";
                txtVANX153.Attributes["onkeypress"] = "return Num(event)";
                txtVANX154.Attributes["onkeypress"] = "return Num(event)";
                txtVANX155.Attributes["onkeypress"] = "return Num(event)";
                txtVANX156.Attributes["onkeypress"] = "return Num(event)";
                txtVANX157.Attributes["onkeypress"] = "return Num(event)";
                txtVANX158.Attributes["onkeypress"] = "return Num(event)";
                txtVANX159.Attributes["onkeypress"] = "return Num(event)";
                txtVANX160.Attributes["onkeypress"] = "return Num(event)";
                txtVANX161.Attributes["onkeypress"] = "return Num(event)";
                txtVANX162.Attributes["onkeypress"] = "return Num(event)";
                txtVANX163.Attributes["onkeypress"] = "return Num(event)";
                txtVANX164.Attributes["onkeypress"] = "return Num(event)";
                txtVANX165.Attributes["onkeypress"] = "return Num(event)";
                txtVANX166.Attributes["onkeypress"] = "return Num(event)";
                txtVANX167.Attributes["onkeypress"] = "return Num(event)";
                txtVANX168.Attributes["onkeypress"] = "return Num(event)";
                txtVANX169.Attributes["onkeypress"] = "return Num(event)";
                txtVANX170.Attributes["onkeypress"] = "return Num(event)";
                txtVANX171.Attributes["onkeypress"] = "return Num(event)";
                txtVANX172.Attributes["onkeypress"] = "return Num(event)";
                txtVANX173.Attributes["onkeypress"] = "return Num(event)";
                txtVANX174.Attributes["onkeypress"] = "return Num(event)";
                txtVANX175.Attributes["onkeypress"] = "return Num(event)";
                txtVANX176.Attributes["onkeypress"] = "return Num(event)";
                txtVANX177.Attributes["onkeypress"] = "return Num(event)";
                txtVANX178.Attributes["onkeypress"] = "return Num(event)";
                txtVANX179.Attributes["onkeypress"] = "return Num(event)";
                txtVANX180.Attributes["onkeypress"] = "return Num(event)";
                txtVANX181.Attributes["onkeypress"] = "return Num(event)";
                txtVANX182.Attributes["onkeypress"] = "return Num(event)";
                txtVANX183.Attributes["onkeypress"] = "return Num(event)";
                txtVANX184.Attributes["onkeypress"] = "return Num(event)";
                txtVANX185.Attributes["onkeypress"] = "return Num(event)";
                txtVANX186.Attributes["onkeypress"] = "return Num(event)";
                txtVANX187.Attributes["onkeypress"] = "return Num(event)";
                txtVANX188.Attributes["onkeypress"] = "return Num(event)";
                txtVANX189.Attributes["onkeypress"] = "return Num(event)";
                txtVANX190.Attributes["onkeypress"] = "return Num(event)";
                txtVANX191.Attributes["onkeypress"] = "return Num(event)";
                txtVANX192.Attributes["onkeypress"] = "return Num(event)";
                txtVANX193.Attributes["onkeypress"] = "return Num(event)";
                txtVANX194.Attributes["onkeypress"] = "return Num(event)";
                txtVANX195.Attributes["onkeypress"] = "return Num(event)";
                txtVANX196.Attributes["onkeypress"] = "return Num(event)";
                txtVANX197.Attributes["onkeypress"] = "return Num(event)";
                txtVANX198.Attributes["onkeypress"] = "return Num(event)";
                txtVANX199.Attributes["onkeypress"] = "return Num(event)";
                txtVANX200.Attributes["onkeypress"] = "return Num(event)";
                txtVANX201.Attributes["onkeypress"] = "return Num(event)";
                txtVANX202.Attributes["onkeypress"] = "return Num(event)";
                txtVANX203.Attributes["onkeypress"] = "return Num(event)";
                txtVANX204.Attributes["onkeypress"] = "return Num(event)";
                txtVANX205.Attributes["onkeypress"] = "return Num(event)";
                txtVANX206.Attributes["onkeypress"] = "return Num(event)";
                txtVANX207.Attributes["onkeypress"] = "return Num(event)";
                txtVANX208.Attributes["onkeypress"] = "return Num(event)";
                txtVANX209.Attributes["onkeypress"] = "return Num(event)";
                txtVANX210.Attributes["onkeypress"] = "return Num(event)";
                txtVANX211.Attributes["onkeypress"] = "return Num(event)";
                txtVANX212.Attributes["onkeypress"] = "return Num(event)";
                txtVANX213.Attributes["onkeypress"] = "return Num(event)";
                txtVANX214.Attributes["onkeypress"] = "return Num(event)";
                txtVANX215.Attributes["onkeypress"] = "return Num(event)";
                txtVANX216.Attributes["onkeypress"] = "return Num(event)";
                txtVANX217.Attributes["onkeypress"] = "return Num(event)";
                txtVANX218.Attributes["onkeypress"] = "return Num(event)";
                txtVANX219.Attributes["onkeypress"] = "return Num(event)";
                txtVANX220.Attributes["onkeypress"] = "return Num(event)";
                txtVANX221.Attributes["onkeypress"] = "return Num(event)";
                txtVANX222.Attributes["onkeypress"] = "return Num(event)";
                txtVANX223.Attributes["onkeypress"] = "return Num(event)";
                txtVANX224.Attributes["onkeypress"] = "return Num(event)";
                txtVANX225.Attributes["onkeypress"] = "return Num(event)";
                txtVANX226.Attributes["onkeypress"] = "return Num(event)";
                txtVANX227.Attributes["onkeypress"] = "return Num(event)";
                txtVANX228.Attributes["onkeypress"] = "return Num(event)";
                txtVANX229.Attributes["onkeypress"] = "return Num(event)";
                txtVANX230.Attributes["onkeypress"] = "return Num(event)";
                txtVANX231.Attributes["onkeypress"] = "return Num(event)";
                txtVANX232.Attributes["onkeypress"] = "return Num(event)";
                txtVANX233.Attributes["onkeypress"] = "return Num(event)";
                txtVANX234.Attributes["onkeypress"] = "return Num(event)";
                txtVANX235.Attributes["onkeypress"] = "return Num(event)";
                txtVANX236.Attributes["onkeypress"] = "return Num(event)";
                txtVANX237.Attributes["onkeypress"] = "return Num(event)";
                txtVANX238.Attributes["onkeypress"] = "return Num(event)";
                txtVANX239.Attributes["onkeypress"] = "return Num(event)";
                txtVANX240.Attributes["onkeypress"] = "return Num(event)";
                txtVANX241.Attributes["onkeypress"] = "return Num(event)";
                txtVANX242.Attributes["onkeypress"] = "return Num(event)";
                txtVANX243.Attributes["onkeypress"] = "return Num(event)";
                txtVANX244.Attributes["onkeypress"] = "return Num(event)";
                txtVANX245.Attributes["onkeypress"] = "return Num(event)";
                txtVANX246.Attributes["onkeypress"] = "return Num(event)";
                txtVANX247.Attributes["onkeypress"] = "return Num(event)";
                txtVANX248.Attributes["onkeypress"] = "return Num(event)";
                txtVANX249.Attributes["onkeypress"] = "return Num(event)";
                txtVANX250.Attributes["onkeypress"] = "return Num(event)";
                txtVANX251.Attributes["onkeypress"] = "return Num(event)";
                txtVANX252.Attributes["onkeypress"] = "return Num(event)";
                txtVANX253.Attributes["onkeypress"] = "return Num(event)";
                txtVANX254.Attributes["onkeypress"] = "return Num(event)";
                txtVANX255.Attributes["onkeypress"] = "return Num(event)";
                txtVANX256.Attributes["onkeypress"] = "return Num(event)";
                txtVANX257.Attributes["onkeypress"] = "return Num(event)";
                txtVANX258.Attributes["onkeypress"] = "return Num(event)";
                txtVANX259.Attributes["onkeypress"] = "return Num(event)";
                txtVANX260.Attributes["onkeypress"] = "return Num(event)";
                txtVANX261.Attributes["onkeypress"] = "return Num(event)";
                txtVANX262.Attributes["onkeypress"] = "return Num(event)";
                txtVANX263.Attributes["onkeypress"] = "return Num(event)";
                txtVANX264.Attributes["onkeypress"] = "return Num(event)";
                txtVANX265.Attributes["onkeypress"] = "return Num(event)";
                txtVANX266.Attributes["onkeypress"] = "return Num(event)";
                txtVANX267.Attributes["onkeypress"] = "return Num(event)";
                txtVANX268.Attributes["onkeypress"] = "return Num(event)";
                txtVANX269.Attributes["onkeypress"] = "return Num(event)";
                txtVANX270.Attributes["onkeypress"] = "return Num(event)";
                txtVANX271.Attributes["onkeypress"] = "return Num(event)";
                txtVANX272.Attributes["onkeypress"] = "return Num(event)";
                txtVANX273.Attributes["onkeypress"] = "return Num(event)";
                txtVANX274.Attributes["onkeypress"] = "return Num(event)";
                txtVANX275.Attributes["onkeypress"] = "return Num(event)";
                txtVANX276.Attributes["onkeypress"] = "return Num(event)";
                txtVANX277.Attributes["onkeypress"] = "return Num(event)";
                txtVANX278.Attributes["onkeypress"] = "return Num(event)";
                txtVANX279.Attributes["onkeypress"] = "return Num(event)";
                txtVANX280.Attributes["onkeypress"] = "return Num(event)";
                txtVANX281.Attributes["onkeypress"] = "return Num(event)";
                txtVANX282.Attributes["onkeypress"] = "return Num(event)";
                txtVANX283.Attributes["onkeypress"] = "return Num(event)";
                txtVANX284.Attributes["onkeypress"] = "return Num(event)";
                txtVANX285.Attributes["onkeypress"] = "return Num(event)";
                txtVANX286.Attributes["onkeypress"] = "return Num(event)";
                txtVANX287.Attributes["onkeypress"] = "return Num(event)";
                txtVANX288.Attributes["onkeypress"] = "return Num(event)";
                txtVANX289.Attributes["onkeypress"] = "return Num(event)";
                txtVANX290.Attributes["onkeypress"] = "return Num(event)";
                txtVANX291.Attributes["onkeypress"] = "return Num(event)";
                txtVANX292.Attributes["onkeypress"] = "return Num(event)";
                txtVANX293.Attributes["onkeypress"] = "return Num(event)";
                txtVANX294.Attributes["onkeypress"] = "return Num(event)";
                txtVANX295.Attributes["onkeypress"] = "return Num(event)";
                txtVANX296.Attributes["onkeypress"] = "return Num(event)";
                txtVANX297.Attributes["onkeypress"] = "return Num(event)";
                txtVANX298.Attributes["onkeypress"] = "return Num(event)";
                txtVANX299.Attributes["onkeypress"] = "return Num(event)";
                txtVANX300.Attributes["onkeypress"] = "return Num(event)";
                txtVANX301.Attributes["onkeypress"] = "return Num(event)";
                txtVANX302.Attributes["onkeypress"] = "return Num(event)";
                txtVANX303.Attributes["onkeypress"] = "return Num(event)";
                txtVANX304.Attributes["onkeypress"] = "return Num(event)";
                txtVANX305.Attributes["onkeypress"] = "return Num(event)";
                txtVANX306.Attributes["onkeypress"] = "return Num(event)";
                txtVANX307.Attributes["onkeypress"] = "return Num(event)";
                txtVANX308.Attributes["onkeypress"] = "return Num(event)";
                txtVANX309.Attributes["onkeypress"] = "return Num(event)";
                txtVANX310.Attributes["onkeypress"] = "return Num(event)";
                txtVANX311.Attributes["onkeypress"] = "return Num(event)";
                txtVANX312.Attributes["onkeypress"] = "return Num(event)";
                txtVANX313.Attributes["onkeypress"] = "return Num(event)";
                txtVANX314.Attributes["onkeypress"] = "return Num(event)";
                txtVANX315.Attributes["onkeypress"] = "return Num(event)";
                txtVANX316.Attributes["onkeypress"] = "return Num(event)";
                txtVANX317.Attributes["onkeypress"] = "return Num(event)";
                txtVANX318.Attributes["onkeypress"] = "return Num(event)";
                txtVANX319.Attributes["onkeypress"] = "return Num(event)";
                txtVANX320.Attributes["onkeypress"] = "return Num(event)";
                txtVANX321.Attributes["onkeypress"] = "return Num(event)";
                txtVANX322.Attributes["onkeypress"] = "return Num(event)";
                txtVANX323.Attributes["onkeypress"] = "return Num(event)";
                txtVANX324.Attributes["onkeypress"] = "return Num(event)";
                txtVANX325.Attributes["onkeypress"] = "return Num(event)";
                txtVANX326.Attributes["onkeypress"] = "return Num(event)";
                txtVANX660.Attributes["onkeypress"] = "return Num(event)";
                txtVANX645.Attributes["onkeypress"] = "return Num(event)";
                txtVANX646.Attributes["onkeypress"] = "return Num(event)";
                txtVANX647.Attributes["onkeypress"] = "return Num(event)";
                txtVANX648.Attributes["onkeypress"] = "return Num(event)";
                txtVANX649.Attributes["onkeypress"] = "return Num(event)";
                txtVANX650.Attributes["onkeypress"] = "return Num(event)";
                txtVANX651.Attributes["onkeypress"] = "return Num(event)";
                txtVANX652.Attributes["onkeypress"] = "return Num(event)";
                txtVANX653.Attributes["onkeypress"] = "return Num(event)";
                txtVANX654.Attributes["onkeypress"] = "return Num(event)";
                txtVANX655.Attributes["onkeypress"] = "return Num(event)";
                txtVANX656.Attributes["onkeypress"] = "return Num(event)";
                txtVANX657.Attributes["onkeypress"] = "return Num(event)";
                txtVANX661.Attributes["onkeypress"] = "return Num(event)";
                txtVANX662.Attributes["onkeypress"] = "return Num(event)";
                txtVANX663.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion


                //Cargar los datos para la carga inicial

                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajasAnexos(this);

                Class911.LlenarDatosDB11(this.Page, controlDP, 3, 0, hidListaTxtBoxs.Value); // DOCUMENTOS: 1... Variable Custrionario 2...Especial 3... Anexo  4... Carreras


                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                hidDisparador.Value = Class911.TiempoAutoGuardado();

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";

                optVANX327.Attributes.Add("onclick", "OPTs2Txt()"); optVANX327.Enabled = controlDP.Estatus == 0;
                optVANX328.Attributes.Add("onclick", "OPTs2Txt()"); optVANX328.Enabled = controlDP.Estatus == 0;
                optVANX329.Attributes.Add("onclick", "OPTs2Txt()"); optVANX329.Enabled = controlDP.Estatus == 0;
                optVANX330.Attributes.Add("onclick", "OPTs2Txt()"); optVANX330.Enabled = controlDP.Estatus == 0;
                optVANX331.Attributes.Add("onclick", "OPTs2Txt()"); optVANX331.Enabled = controlDP.Estatus == 0;
                optVANX332.Attributes.Add("onclick", "OPTs2Txt()"); optVANX332.Enabled = controlDP.Estatus == 0;

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }
            txtVANX659.Attributes.Remove("onkeypress");
        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 3, HttpContext.Current); //Class911.RaiseCallbackEvent_Anexos(eventArgument); ;
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }
    }
}
