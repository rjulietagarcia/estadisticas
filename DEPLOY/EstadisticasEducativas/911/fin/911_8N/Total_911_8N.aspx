<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="Total_911_8N.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8N.Total_911_8N" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    
    <%--Agregado--%>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 16;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">
    <table>
        <tr><td style="width:120px;"></td><td><span>EDUCACIÓN NORMAL</span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCiclo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCentroTrabajo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8N',true)"><a href="#" title="" ><span>IDENTIFICACIÓN</span></a></li>
        <li onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title="" ><span>CARACTERÍSTICAS Y PERSONAL</span></a></li>
        <li onclick="openPage('ACG_911_8N',true)"><a href="#" title="" ><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('Total_911_8N',true)"><a href="#" title="" class="activo"><span>TOTAL DE LICENCIATURAS</span></a></li>
        <li onclick="openPage('TotalAlumn_911_8N',false)"><a href="#" title=""><span>TOTAL ALUMNOS POR GRUPO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PLANTELES DE EXTENSIÓN</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACIÓN</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

         <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

          
            <table style="width: 830px">
                        <tr>
                            <td colspan="6" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                                    Text="TOTAL DE LAS LICENCIATURAS O PROGRAMAS DE POSGRADO" Width="100%"></asp:Label></td>
                        </tr>
            </table>
            <table style="width:auto;">
            <tr >
                <td class="linaBajoAlto"></td>
                <td class="linaBajoAlto" style="width: 136px"></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblHombres" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="80px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblMujeres" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="MUJERES" Width="100px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblGrupos" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                        Width="100px"></asp:Label>
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajoAlto" style="width:136px">
                    <asp:Label ID="lblInscTotal1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV31" runat="server" Columns="5" MaxLength="5" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV32" runat="server" Columns="5" MaxLength="5" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV33" runat="server" Columns="5" MaxLength="5" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV34" runat="server" Columns="5" MaxLength="5" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="lblExistencia1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV35" runat="server" Columns="5" MaxLength="5" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV36" runat="server" Columns="5" MaxLength="5" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV37" runat="server" Columns="5" MaxLength="5" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="lblAprobados1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV38" runat="server" Columns="5" MaxLength="5" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV39" runat="server" Columns="5" MaxLength="5" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV40" runat="server" Columns="5" MaxLength="5" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV41" runat="server" Columns="5" MaxLength="5" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV42" runat="server" Columns="5" MaxLength="5" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV43" runat="server" Columns="5" MaxLength="5" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajoAlto">
                    <asp:TextBox ID="txtV44" runat="server" Columns="5" MaxLength="5" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV45" runat="server" Columns="5" MaxLength="5" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV46" runat="server" Columns="5" MaxLength="5" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV47" runat="server" Columns="5" MaxLength="5" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV48" runat="server" Columns="5" MaxLength="5" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV49" runat="server" Columns="5" MaxLength="5" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV50" runat="server" Columns="5" MaxLength="5" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV51" runat="server" Columns="5" MaxLength="5" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV52" runat="server" Columns="5" MaxLength="5" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV53" runat="server" Columns="5" MaxLength="5" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajoAlto">
                    <asp:TextBox ID="txtV54" runat="server" Columns="5" MaxLength="5" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV55" runat="server" Columns="5" MaxLength="5" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV56" runat="server" Columns="5" MaxLength="5" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV57" runat="server" Columns="5" MaxLength="5" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV58" runat="server" Columns="5" MaxLength="5" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV59" runat="server" Columns="5" MaxLength="5" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV60" runat="server" Columns="5" MaxLength="5" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV61" runat="server" Columns="5" MaxLength="5" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV62" runat="server" Columns="5" MaxLength="5" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV63" runat="server" Columns="5" MaxLength="5" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajoAlto">
                    <asp:TextBox ID="txtV64" runat="server" Columns="5" MaxLength="5" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV65" runat="server" Columns="5" MaxLength="5" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV66" runat="server" Columns="5" MaxLength="5" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV67" runat="server" Columns="5" MaxLength="5" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV68" runat="server" Columns="5" MaxLength="5" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV69" runat="server" Columns="5" MaxLength="5" TabIndex="11202" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV70" runat="server" Columns="5" MaxLength="5" TabIndex="11203" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV71" runat="server" Columns="5" MaxLength="5" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV72" runat="server" Columns="5" MaxLength="5" TabIndex="11302" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV73" runat="server" Columns="5" MaxLength="5" TabIndex="11303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajoAlto">
                    <asp:TextBox ID="txtV74" runat="server" Columns="5" MaxLength="5" TabIndex="11304" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV75" runat="server" Columns="5" MaxLength="5" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV76" runat="server" Columns="5" MaxLength="5" TabIndex="11402" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV77" runat="server" Columns="5" MaxLength="5" TabIndex="11403" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label22" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV78" runat="server" Columns="5" MaxLength="5" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV79" runat="server" Columns="5" MaxLength="5" TabIndex="11502" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV80" runat="server" Columns="5" MaxLength="5" TabIndex="11503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
        </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('ACG_911_8N',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir página previa" /></a></span></td> 
                <td ><span  onclick="openPage('ACG_911_8N',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('TotalAlumn_911_8N',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('TotalAlumn_911_8N',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir página siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         </center>
         
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando información por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
               
                    
          <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        <%--Agregado--%>
            <script type="text/javascript">
                GetTabIndexes();
            </script>
        <%--Agregado--%>
</asp:Content>
