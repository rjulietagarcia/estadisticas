<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="ACG_911_8N.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8N.ACG_911_8N" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    
    <%--Agregado--%>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 20;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">
    <table>
        <tr><td style="width:120px;"></td><td><span>EDUCACIÓN NORMAL</span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCiclo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCentroTrabajo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8N',true)"><a href="#" title="" ><span>IDENTIFICACIÓN</span></a></li>
        <li onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title="" ><span>CARACTERÍSTICAS Y PERSONAL</span></a></li>
        <li onclick="openPage('ACG_911_8N',true)"><a href="#" title="" class="activo"><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('Total_911_8N',false)"><a href="#" title="" ><span>TOTAL DE LICENCIATURAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL ALUMNOS POR GRUPO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PLANTELES DE EXTENSIÓN</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACIÓN</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

         <center>
 
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

          
                    <table style="width: 830px">
                        <tr>
                            <td colspan="6" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                    Text="III. ALUMNOS Y GRUPOS" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: left">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="A) Escriba para cada licenciatura o programa de posgrado el número de alumnos desglosándolo por grado, sexo, inscripción total, existencia y aprobados, así como el número de grupos existentes, por grado."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="height: 19px; text-align: left">
                            </td>
                        </tr>
            </table>
            <table>
                <tr>
                            <td colspan="2" style="height: 19px; text-align: left">
                                <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="NOMBRE DE LA LICENCIATURA O PROGRAMA DE POSGRADO"></asp:Label>
                            </td>
                            <td colspan="4" style="height: 19px; text-align: left">
                                &nbsp;<asp:TextBox ID="txtVA1" runat="server" Columns="30" MaxLength="30" TabIndex="10101" CssClass="lblNegro"></asp:TextBox>
                                &nbsp; &nbsp; &nbsp;
                                <asp:Label id="Label10" runat="server" Text="ID" Font-Bold="True" CssClass="lblRojo"></asp:Label>
                                <asp:TextBox ID="txtVA0" runat="server" BackColor="Bisque" Columns="5" Font-Bold="True"
                                    MaxLength="5" ReadOnly="True" Style="text-align: right" Width="24px">0</asp:TextBox>
                           </td>
                        </tr>
            </table>
            <table>
                <tr>
                    <td style=" text-align: right">
                        &nbsp;<asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DURACIÓN"
                            Width="60px"></asp:Label>
                    </td>
                    <td colspan="2" style=" text-align: left">
                        <asp:TextBox ID="txtVA2" runat="server" Columns="30" MaxLength="30" TabIndex="10301" CssClass="lblNegro"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table style="width:auto;">
            <tr >
                <td class="linaBajoAlto"></td>
                <td class="linaBajoAlto" style="width: 136px"></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblHombres" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="80px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblMujeres" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="MUJERES" Width="100px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100px"></asp:Label>
                </td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblGrupos" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                        Width="100px"></asp:Label>
                </td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajoAlto" style="width:136px">
                    <asp:Label ID="lblInscTotal1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA4" runat="server" Columns="5" MaxLength="5" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA5" runat="server" Columns="5" MaxLength="5" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA6" runat="server" Columns="5" MaxLength="5" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA7" runat="server" Columns="5" MaxLength="5" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="lblExistencia1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA8" runat="server" Columns="5" MaxLength="5" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA9" runat="server" Columns="5" MaxLength="5" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA10" runat="server" Columns="5" MaxLength="5" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="lblAprobados1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA11" runat="server" Columns="5" MaxLength="5" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA12" runat="server" Columns="5" MaxLength="5" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA13" runat="server" Columns="5" MaxLength="5" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA14" runat="server" Columns="5" MaxLength="5" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA15" runat="server" Columns="5" MaxLength="5" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA16" runat="server" Columns="5" MaxLength="5" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajoAlto">
                    <asp:TextBox ID="txtVA17" runat="server" Columns="5" MaxLength="5" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA18" runat="server" Columns="5" MaxLength="5" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA19" runat="server" Columns="5" MaxLength="5" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA20" runat="server" Columns="5" MaxLength="5" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA21" runat="server" Columns="5" MaxLength="5" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA22" runat="server" Columns="5" MaxLength="5" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA23" runat="server" Columns="5" MaxLength="5" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA24" runat="server" Columns="5" MaxLength="5" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA25" runat="server" Columns="5" MaxLength="5" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA26" runat="server" Columns="5" MaxLength="5" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajoAlto">
                    <asp:TextBox ID="txtVA27" runat="server" Columns="5" MaxLength="5" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA28" runat="server" Columns="5" MaxLength="5" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA29" runat="server" Columns="5" MaxLength="5" TabIndex="11202" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA30" runat="server" Columns="5" MaxLength="5" TabIndex="11203" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA31" runat="server" Columns="5" MaxLength="5" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA32" runat="server" Columns="5" MaxLength="5" TabIndex="11302" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA33" runat="server" Columns="5" MaxLength="5" TabIndex="11303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA34" runat="server" Columns="5" MaxLength="5" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA35" runat="server" Columns="5" MaxLength="5" TabIndex="11402" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA36" runat="server" Columns="5" MaxLength="5" TabIndex="11403" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajoAlto">
                    <asp:TextBox ID="txtVA37" runat="server" Columns="5" MaxLength="5" TabIndex="11404" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA38" runat="server" Columns="5" MaxLength="5" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA39" runat="server" Columns="5" MaxLength="5" TabIndex="11502" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA40" runat="server" Columns="5" MaxLength="5" TabIndex="11503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA41" runat="server" Columns="5" MaxLength="5" TabIndex="11601" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA42" runat="server" Columns="5" MaxLength="5" TabIndex="11602" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA43" runat="server" Columns="5" MaxLength="5" TabIndex="11603" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCIÓN TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA44" runat="server" Columns="5" MaxLength="5" TabIndex="11701" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA45" runat="server" Columns="5" MaxLength="5" TabIndex="11702" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA46" runat="server" Columns="5" MaxLength="5" TabIndex="11703" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajoAlto">
                    <asp:TextBox ID="txtVA47" runat="server" Columns="5" MaxLength="5" TabIndex="11704" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA48" runat="server" Columns="5" MaxLength="5" TabIndex="11801" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA49" runat="server" Columns="5" MaxLength="5" TabIndex="11802" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA50" runat="server" Columns="5" MaxLength="5" TabIndex="11803" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
            <tr >
                <td  class="linaBajo" style="width: 136px">
                    <asp:Label ID="Label22" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA51" runat="server" Columns="5" MaxLength="5" TabIndex="11901" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA52" runat="server" Columns="5" MaxLength="5" TabIndex="11902" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA53" runat="server" Columns="5" MaxLength="5" TabIndex="11903" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">&nbsp;</td>
                <td class="Orila">&nbsp;
                </td>
            </tr>
        </table>
        
          <table>
          <tr>
                    <td>
                        <asp:LinkButton ID="lnkEliminarCarrera" runat="server" OnClick="lnkEliminarCarrera_Click">Eliminar Carrera Actual</asp:LinkButton>
                    </td>
                </tr>
                <tr><td>
                    Carreras:
                </td>
                    <td style="font-weight: bold;">
                       <asp:DropDownList ID="ddlID_Carrera" runat="server"  onchange='OnChange(this);'>
                       </asp:DropDownList > de 
                       <asp:TextBox ID="lblUnodeN" runat="server" Columns="5" MaxLength="5" Width="24px" BackColor="Bisque" Font-Bold="True" ReadOnly="True">0</asp:TextBox>
                    </td>
                    <td id="td_Nuevo" runat="server"><span onclick = "navegarCarreras_new(-1);" >&nbsp;<a href="#">Agregar Otra <strong>*</strong></a></span></td>
                </tr>
            </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir página previa" /></a></span></td> 
                <td ><span  onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Total_911_8N',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Total_911_8N',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir página siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         </center>
         
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando información por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
         
         <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                var ID_Carrera =0; 
                
               
                function OnChange(dropdown)
                {
                    var myindex  = dropdown.selectedIndex
                    var SelValue = dropdown.options[myindex].value
                    navegarCarreras_new(SelValue);
                    return true;
                }
                
                function navegarCarreras_new(id_carrera){
                     ID_Carrera = (id_carrera);
                     var ID_Carreratmp = ID_Carrera; 
                                        
                    if (id_carrera == -1)
                    {
                       var totalRegs = document.getElementById('<%=lblUnodeN.ClientID%>').value;
                       ID_Carrera = eval(totalRegs) + 1; 
                       alert(ID_Carrera);
                    }
                
                    openPage('ACG_911_8N');
                }
           
          
                function __ReceiveServerData_Variables(rValue){
                   if (rValue != null){
                        document.getElementById("divWait").style.visibility = "hidden";
                        var rows = rValue.split('|');
                        var vi = 1;
                        if (rValue != "")  LimpiarFallas();
                        var detalles="<ul>";
                        for(vi = 1; vi < rows.length; vi ++){
                            var res = rows[vi];
                            var colums = res.split('!');
                            var caja = document.getElementById("ctl00_cphMainMaster_txtV" + colums[0]);
                            if (caja!=null){
                                 caja.className = "txtVarFallas";
                            }
                            detalles = detalles + "<li>" + colums[1]+ "</li>";
                       }
                       if (CambiarPagina != "") {  // para que no se borre la informacion a menos que se mueva de la pagina
                            var obj = document.getElementById('divResultado');
                            obj.innerHTML =  detalles + "</ul>";
                       }
                       var Cambiar = "No";
                       if ( rValue != "" && CambiarPagina != "" ){
                             if(ir1)
                            {
                               
                                Cambiar = "Si";
                            }
                            else
                            {
                                alert("Se encontraron errores de captura\nDebe corregirlos para avanzar a la siguiente p"+'\u00e1'+"gina.");
                                Cambiar = "No";
                            }
                       }
                       else if(rValue == "" && CambiarPagina != ""){
                          Cambiar = "Si";
                       }
                       if (Cambiar == "Si"){
                             window.location.href=CambiarPagina+".aspx?idC=" + ID_Carrera;
                       } 
                   }
                }
                
                                
              
                function OcultarNuevoRegistro(){
                   if (document.getElementById("ctl00_cphMainMaster_txtVA6").disabled== true){
                      document.getElementById("NuevoReg").style.display = "none";
                   }
                }
                

          
               
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
</asp:Content>
