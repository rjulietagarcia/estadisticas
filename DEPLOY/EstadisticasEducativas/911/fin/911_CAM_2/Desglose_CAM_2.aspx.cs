using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;
     

namespace EstadisticasEducativas._911.fin.CAM_2
{
    public partial class Desglose_CAM_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV872.Attributes["onkeypress"] = "return Num(event)";
                txtV873.Attributes["onkeypress"] = "return Num(event)";
                txtV874.Attributes["onkeypress"] = "return Num(event)";
                txtV875.Attributes["onkeypress"] = "return Num(event)";
                txtV876.Attributes["onkeypress"] = "return Num(event)";
                txtV877.Attributes["onkeypress"] = "return Num(event)";
                txtV878.Attributes["onkeypress"] = "return Num(event)";
                txtV879.Attributes["onkeypress"] = "return Num(event)";
                txtV880.Attributes["onkeypress"] = "return Num(event)";
                txtV881.Attributes["onkeypress"] = "return Num(event)";
                txtV882.Attributes["onkeypress"] = "return Num(event)";
                txtV883.Attributes["onkeypress"] = "return Num(event)";
                txtV884.Attributes["onkeypress"] = "return Num(event)";
                txtV885.Attributes["onkeypress"] = "return Num(event)";
                txtV886.Attributes["onkeypress"] = "return Num(event)";
                txtV887.Attributes["onkeypress"] = "return Num(event)";
                txtV888.Attributes["onkeypress"] = "return Num(event)";
                txtV889.Attributes["onkeypress"] = "return Num(event)";
                txtV890.Attributes["onkeypress"] = "return Num(event)";
                txtV891.Attributes["onkeypress"] = "return Num(event)";
                txtV892.Attributes["onkeypress"] = "return Num(event)";
                txtV893.Attributes["onkeypress"] = "return Num(event)";
                txtV894.Attributes["onkeypress"] = "return Num(event)";
                txtV895.Attributes["onkeypress"] = "return Num(event)";
                txtV896.Attributes["onkeypress"] = "return Num(event)";
                txtV897.Attributes["onkeypress"] = "return Num(event)";
                txtV898.Attributes["onkeypress"] = "return Num(event)";
                txtV899.Attributes["onkeypress"] = "return Num(event)";
                txtV900.Attributes["onkeypress"] = "return Num(event)";
                txtV901.Attributes["onkeypress"] = "return Num(event)";
                txtV902.Attributes["onkeypress"] = "return Num(event)";
                txtV903.Attributes["onkeypress"] = "return Num(event)";
                txtV904.Attributes["onkeypress"] = "return Num(event)";
                txtV905.Attributes["onkeypress"] = "return Num(event)";
                txtV906.Attributes["onkeypress"] = "return Num(event)";
                txtV907.Attributes["onkeypress"] = "return Num(event)";
                txtV908.Attributes["onkeypress"] = "return Num(event)";
                txtV909.Attributes["onkeypress"] = "return Num(event)";
                txtV910.Attributes["onkeypress"] = "return Num(event)";
                txtV911.Attributes["onkeypress"] = "return Num(event)";
                txtV912.Attributes["onkeypress"] = "return Num(event)";
                txtV913.Attributes["onkeypress"] = "return Num(event)";
                txtV914.Attributes["onkeypress"] = "return Num(event)";
                txtV915.Attributes["onkeypress"] = "return Num(event)";
                txtV916.Attributes["onkeypress"] = "return Num(event)";
                txtV917.Attributes["onkeypress"] = "return Num(event)";
                txtV918.Attributes["onkeypress"] = "return Num(event)";
                txtV919.Attributes["onkeypress"] = "return Num(event)";
                txtV920.Attributes["onkeypress"] = "return Num(event)";
                txtV921.Attributes["onkeypress"] = "return Num(event)";
                txtV922.Attributes["onkeypress"] = "return Num(event)";
                txtV923.Attributes["onkeypress"] = "return Num(event)";
                txtV924.Attributes["onkeypress"] = "return Num(event)";
                txtV925.Attributes["onkeypress"] = "return Num(event)";
                txtV926.Attributes["onkeypress"] = "return Num(event)";
                txtV927.Attributes["onkeypress"] = "return Num(event)";
                txtV928.Attributes["onkeypress"] = "return Num(event)";
                txtV929.Attributes["onkeypress"] = "return Num(event)";
                txtV930.Attributes["onkeypress"] = "return Num(event)";
                txtV931.Attributes["onkeypress"] = "return Num(event)";
                txtV932.Attributes["onkeypress"] = "return Num(event)";
                txtV933.Attributes["onkeypress"] = "return Num(event)";
                txtV934.Attributes["onkeypress"] = "return Num(event)";
                txtV935.Attributes["onkeypress"] = "return Num(event)";
                txtV936.Attributes["onkeypress"] = "return Num(event)";
                txtV937.Attributes["onkeypress"] = "return Num(event)";
                txtV938.Attributes["onkeypress"] = "return Num(event)";
                txtV939.Attributes["onkeypress"] = "return Num(event)";
                txtV940.Attributes["onkeypress"] = "return Num(event)";
                txtV941.Attributes["onkeypress"] = "return Num(event)";
                txtV942.Attributes["onkeypress"] = "return Num(event)";
                txtV943.Attributes["onkeypress"] = "return Num(event)";
                txtV944.Attributes["onkeypress"] = "return Num(event)";
                txtV945.Attributes["onkeypress"] = "return Num(event)";
                txtV946.Attributes["onkeypress"] = "return Num(event)";
                txtV947.Attributes["onkeypress"] = "return Num(event)";
                txtV948.Attributes["onkeypress"] = "return Num(event)";
                txtV949.Attributes["onkeypress"] = "return Num(event)";
                txtV950.Attributes["onkeypress"] = "return Num(event)";
                txtV951.Attributes["onkeypress"] = "return Num(event)";
                txtV952.Attributes["onkeypress"] = "return Num(event)";
                txtV953.Attributes["onkeypress"] = "return Num(event)";
                txtV954.Attributes["onkeypress"] = "return Num(event)";
                txtV955.Attributes["onkeypress"] = "return Num(event)";
                txtV956.Attributes["onkeypress"] = "return Num(event)";
                txtV957.Attributes["onkeypress"] = "return Num(event)";
                txtV958.Attributes["onkeypress"] = "return Num(event)";
                txtV959.Attributes["onkeypress"] = "return Num(event)";
                txtV960.Attributes["onkeypress"] = "return Num(event)";
                txtV961.Attributes["onkeypress"] = "return Num(event)";
                txtV962.Attributes["onkeypress"] = "return Num(event)";
                txtV963.Attributes["onkeypress"] = "return Num(event)";
                txtV964.Attributes["onkeypress"] = "return Num(event)";
                txtV965.Attributes["onkeypress"] = "return Num(event)";
                txtV966.Attributes["onkeypress"] = "return Num(event)";
                txtV967.Attributes["onkeypress"] = "return Num(event)";
                txtV968.Attributes["onkeypress"] = "return Num(event)";
                txtV969.Attributes["onkeypress"] = "return Num(event)";
                txtV970.Attributes["onkeypress"] = "return Num(event)";
                txtV971.Attributes["onkeypress"] = "return Num(event)";
                txtV972.Attributes["onkeypress"] = "return Num(event)";
                txtV973.Attributes["onkeypress"] = "return Num(event)";
                txtV974.Attributes["onkeypress"] = "return Num(event)";
                txtV975.Attributes["onkeypress"] = "return Num(event)";
                txtV976.Attributes["onkeypress"] = "return Num(event)";
                txtV977.Attributes["onkeypress"] = "return Num(event)";
                txtV978.Attributes["onkeypress"] = "return Num(event)";
                txtV979.Attributes["onkeypress"] = "return Num(event)";
                txtV980.Attributes["onkeypress"] = "return Num(event)";
                txtV981.Attributes["onkeypress"] = "return Num(event)";
                txtV982.Attributes["onkeypress"] = "return Num(event)";
                txtV983.Attributes["onkeypress"] = "return Num(event)";
                txtV984.Attributes["onkeypress"] = "return Num(event)";
                txtV985.Attributes["onkeypress"] = "return Num(event)";
                txtV986.Attributes["onkeypress"] = "return Num(event)";
                txtV987.Attributes["onkeypress"] = "return Num(event)";
                txtV988.Attributes["onkeypress"] = "return Num(event)";
                txtV989.Attributes["onkeypress"] = "return Num(event)";
                txtV990.Attributes["onkeypress"] = "return Num(event)";
                txtV991.Attributes["onkeypress"] = "return Num(event)";
                txtV992.Attributes["onkeypress"] = "return Num(event)";
                txtV993.Attributes["onkeypress"] = "return Num(event)";
                txtV994.Attributes["onkeypress"] = "return Num(event)";
                txtV995.Attributes["onkeypress"] = "return Num(event)";
                txtV996.Attributes["onkeypress"] = "return Num(event)";
                txtV997.Attributes["onkeypress"] = "return Num(event)";
                txtV998.Attributes["onkeypress"] = "return Num(event)";
                txtV999.Attributes["onkeypress"] = "return Num(event)";
                txtV1000.Attributes["onkeypress"] = "return Num(event)";
                txtV1001.Attributes["onkeypress"] = "return Num(event)";
                txtV1002.Attributes["onkeypress"] = "return Num(event)";
                txtV1003.Attributes["onkeypress"] = "return Num(event)";
                txtV1004.Attributes["onkeypress"] = "return Num(event)";
                txtV1005.Attributes["onkeypress"] = "return Num(event)";
                txtV1006.Attributes["onkeypress"] = "return Num(event)";
                txtV1007.Attributes["onkeypress"] = "return Num(event)";
                txtV1008.Attributes["onkeypress"] = "return Num(event)";
                txtV1009.Attributes["onkeypress"] = "return Num(event)";
                txtV1010.Attributes["onkeypress"] = "return Num(event)";
                txtV1011.Attributes["onkeypress"] = "return Num(event)";
                txtV1012.Attributes["onkeypress"] = "return Num(event)";
                txtV1013.Attributes["onkeypress"] = "return Num(event)";
                txtV1014.Attributes["onkeypress"] = "return Num(event)";
                txtV1015.Attributes["onkeypress"] = "return Num(event)";
                txtV1016.Attributes["onkeypress"] = "return Num(event)";
                txtV1017.Attributes["onkeypress"] = "return Num(event)";
                txtV1018.Attributes["onkeypress"] = "return Num(event)";
                txtV1019.Attributes["onkeypress"] = "return Num(event)";
                txtV1020.Attributes["onkeypress"] = "return Num(event)";
                txtV1021.Attributes["onkeypress"] = "return Num(event)";
                txtV1022.Attributes["onkeypress"] = "return Num(event)";
                txtV1023.Attributes["onkeypress"] = "return Num(event)";
                txtV1024.Attributes["onkeypress"] = "return Num(event)";
                txtV1025.Attributes["onkeypress"] = "return Num(event)";
                txtV1026.Attributes["onkeypress"] = "return Num(event)";
                txtV1027.Attributes["onkeypress"] = "return Num(event)";
                txtV1028.Attributes["onkeypress"] = "return Num(event)";
                txtV1029.Attributes["onkeypress"] = "return Num(event)";
                txtV1030.Attributes["onkeypress"] = "return Num(event)";
                txtV1031.Attributes["onkeypress"] = "return Num(event)";
                txtV1032.Attributes["onkeypress"] = "return Num(event)";
                txtV1033.Attributes["onkeypress"] = "return Num(event)";
                txtV1034.Attributes["onkeypress"] = "return Num(event)";
                txtV1035.Attributes["onkeypress"] = "return Num(event)";
                txtV1036.Attributes["onkeypress"] = "return Num(event)";
                txtV1037.Attributes["onkeypress"] = "return Num(event)";
                txtV1038.Attributes["onkeypress"] = "return Num(event)";
                txtV1039.Attributes["onkeypress"] = "return Num(event)";
                txtV1040.Attributes["onkeypress"] = "return Num(event)";
                txtV1041.Attributes["onkeypress"] = "return Num(event)";
                txtV1042.Attributes["onkeypress"] = "return Num(event)";
                txtV1043.Attributes["onkeypress"] = "return Num(event)";
                txtV1044.Attributes["onkeypress"] = "return Num(event)";
                txtV1045.Attributes["onkeypress"] = "return Num(event)";
                txtV1046.Attributes["onkeypress"] = "return Num(event)";
                txtV1047.Attributes["onkeypress"] = "return Num(event)";
                txtV1048.Attributes["onkeypress"] = "return Num(event)";
                txtV1049.Attributes["onkeypress"] = "return Num(event)";
                txtV1050.Attributes["onkeypress"] = "return Num(event)";
                txtV1051.Attributes["onkeypress"] = "return Num(event)";
                txtV1052.Attributes["onkeypress"] = "return Num(event)";
                txtV1053.Attributes["onkeypress"] = "return Num(event)";
                txtV1054.Attributes["onkeypress"] = "return Num(event)";
                txtV1055.Attributes["onkeypress"] = "return Num(event)";
                txtV1056.Attributes["onkeypress"] = "return Num(event)";
                txtV1057.Attributes["onkeypress"] = "return Num(event)";
                txtV1058.Attributes["onkeypress"] = "return Num(event)";
                txtV1059.Attributes["onkeypress"] = "return Num(event)";
                txtV1060.Attributes["onkeypress"] = "return Num(event)";
                txtV1061.Attributes["onkeypress"] = "return Num(event)";
                txtV1062.Attributes["onkeypress"] = "return Num(event)";
                txtV1063.Attributes["onkeypress"] = "return Num(event)";
                txtV1064.Attributes["onkeypress"] = "return Num(event)";
                txtV1065.Attributes["onkeypress"] = "return Num(event)";
                txtV1066.Attributes["onkeypress"] = "return Num(event)";
                txtV1067.Attributes["onkeypress"] = "return Num(event)";
                txtV1068.Attributes["onkeypress"] = "return Num(event)";
                txtV1069.Attributes["onkeypress"] = "return Num(event)";
                txtV1070.Attributes["onkeypress"] = "return Num(event)";
                txtV1071.Attributes["onkeypress"] = "return Num(event)";
                txtV1072.Attributes["onkeypress"] = "return Num(event)";
                txtV1073.Attributes["onkeypress"] = "return Num(event)";
                txtV1074.Attributes["onkeypress"] = "return Num(event)";
                txtV1075.Attributes["onkeypress"] = "return Num(event)";
                txtV1076.Attributes["onkeypress"] = "return Num(event)";
                txtV1077.Attributes["onkeypress"] = "return Num(event)";
                txtV1078.Attributes["onkeypress"] = "return Num(event)";
                txtV1079.Attributes["onkeypress"] = "return Num(event)";
                txtV1080.Attributes["onkeypress"] = "return Num(event)";
                txtV1081.Attributes["onkeypress"] = "return Num(event)";
                txtV1082.Attributes["onkeypress"] = "return Num(event)";
                txtV1083.Attributes["onkeypress"] = "return Num(event)";
                txtV1084.Attributes["onkeypress"] = "return Num(event)";
                txtV1085.Attributes["onkeypress"] = "return Num(event)";
                txtV1086.Attributes["onkeypress"] = "return Num(event)";
                txtV1087.Attributes["onkeypress"] = "return Num(event)";
                txtV1088.Attributes["onkeypress"] = "return Num(event)";
                txtV1089.Attributes["onkeypress"] = "return Num(event)";
                txtV1090.Attributes["onkeypress"] = "return Num(event)";
                txtV1091.Attributes["onkeypress"] = "return Num(event)";
                txtV1092.Attributes["onkeypress"] = "return Num(event)";
                txtV1093.Attributes["onkeypress"] = "return Num(event)";
                txtV1094.Attributes["onkeypress"] = "return Num(event)";
                txtV1095.Attributes["onkeypress"] = "return Num(event)";
                txtV1096.Attributes["onkeypress"] = "return Num(event)";
                txtV1097.Attributes["onkeypress"] = "return Num(event)";
                txtV1098.Attributes["onkeypress"] = "return Num(event)";
                txtV1099.Attributes["onkeypress"] = "return Num(event)";
                txtV1100.Attributes["onkeypress"] = "return Num(event)";
                txtV1101.Attributes["onkeypress"] = "return Num(event)";
                txtV1102.Attributes["onkeypress"] = "return Num(event)";
                txtV1103.Attributes["onkeypress"] = "return Num(event)";
                txtV1104.Attributes["onkeypress"] = "return Num(event)";
                txtV1105.Attributes["onkeypress"] = "return Num(event)";
                txtV1106.Attributes["onkeypress"] = "return Num(event)";
                txtV1107.Attributes["onkeypress"] = "return Num(event)";
                txtV1108.Attributes["onkeypress"] = "return Num(event)";
                txtV1109.Attributes["onkeypress"] = "return Num(event)";
                txtV1110.Attributes["onkeypress"] = "return Num(event)";
                txtV1111.Attributes["onkeypress"] = "return Num(event)";
                txtV1112.Attributes["onkeypress"] = "return Num(event)";
                txtV1113.Attributes["onkeypress"] = "return Num(event)";
                txtV1114.Attributes["onkeypress"] = "return Num(event)";
                txtV1115.Attributes["onkeypress"] = "return Num(event)";
                txtV1116.Attributes["onkeypress"] = "return Num(event)";
                txtV1117.Attributes["onkeypress"] = "return Num(event)";
                txtV1118.Attributes["onkeypress"] = "return Num(event)";
                txtV1119.Attributes["onkeypress"] = "return Num(event)";
                txtV1120.Attributes["onkeypress"] = "return Num(event)";
                txtV1121.Attributes["onkeypress"] = "return Num(event)";
                txtV1122.Attributes["onkeypress"] = "return Num(event)";
                txtV1123.Attributes["onkeypress"] = "return Num(event)";
                txtV1124.Attributes["onkeypress"] = "return Num(event)";
                txtV1125.Attributes["onkeypress"] = "return Num(event)";
                txtV1126.Attributes["onkeypress"] = "return Num(event)";
                txtV1127.Attributes["onkeypress"] = "return Num(event)";
                txtV1128.Attributes["onkeypress"] = "return Num(event)";
                txtV1129.Attributes["onkeypress"] = "return Num(event)";
                txtV1130.Attributes["onkeypress"] = "return Num(event)";
                txtV1131.Attributes["onkeypress"] = "return Num(event)";
                txtV1616.Attributes["onkeypress"] = "return Num(event)";
                txtV1617.Attributes["onkeypress"] = "return Num(event)";
                txtV1618.Attributes["onkeypress"] = "return Num(event)";
                txtV1619.Attributes["onkeypress"] = "return Num(event)";
                txtV1620.Attributes["onkeypress"] = "return Num(event)";
                txtV1621.Attributes["onkeypress"] = "return Num(event)";
                txtV1622.Attributes["onkeypress"] = "return Num(event)";
                txtV1623.Attributes["onkeypress"] = "return Num(event)";
                txtV1624.Attributes["onkeypress"] = "return Num(event)";
                txtV1625.Attributes["onkeypress"] = "return Num(event)";
                txtV1626.Attributes["onkeypress"] = "return Num(event)";
                txtV1627.Attributes["onkeypress"] = "return Num(event)";
                txtV1628.Attributes["onkeypress"] = "return Num(event)";
                txtV1132.Attributes["onkeypress"] = "return Num(event)";
                txtV1133.Attributes["onkeypress"] = "return Num(event)";
                txtV1134.Attributes["onkeypress"] = "return Num(event)";
                txtV1135.Attributes["onkeypress"] = "return Num(event)";
                txtV1136.Attributes["onkeypress"] = "return Num(event)";
                txtV1137.Attributes["onkeypress"] = "return Num(event)";
                txtV1138.Attributes["onkeypress"] = "return Num(event)";
                txtV1139.Attributes["onkeypress"] = "return Num(event)";
                txtV1140.Attributes["onkeypress"] = "return Num(event)";
                txtV1141.Attributes["onkeypress"] = "return Num(event)";
                txtV1142.Attributes["onkeypress"] = "return Num(event)";
                txtV1143.Attributes["onkeypress"] = "return Num(event)";
                txtV1144.Attributes["onkeypress"] = "return Num(event)";
                txtV1145.Attributes["onkeypress"] = "return Num(event)";
                txtV1146.Attributes["onkeypress"] = "return Num(event)";
                txtV1147.Attributes["onkeypress"] = "return Num(event)";
                txtV1148.Attributes["onkeypress"] = "return Num(event)";
                txtV1149.Attributes["onkeypress"] = "return Num(event)";
                txtV1150.Attributes["onkeypress"] = "return Num(event)";
                txtV1151.Attributes["onkeypress"] = "return Num(event)";
                txtV1152.Attributes["onkeypress"] = "return Num(event)";
                txtV1153.Attributes["onkeypress"] = "return Num(event)";
                txtV1154.Attributes["onkeypress"] = "return Num(event)";
                txtV1155.Attributes["onkeypress"] = "return Num(event)";
                txtV1156.Attributes["onkeypress"] = "return Num(event)";
                #endregion


                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        //*********************************


    }
}

