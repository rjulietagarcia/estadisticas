<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="CAM-2(Prim. y Sec.)" AutoEventWireup="true" CodeBehind="AG2_CAM_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin.CAM_2.AG2_CAM_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
<%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 42;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
     <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N ESPECIAL - CENTRO DE ATENCI�N M�LTIPLE</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1500px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_CAM_2',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_CAM_2',true)"><a href="#" title=""><span>INICIAL Y PREESCOLAR</span></a></li>
        <li onclick="openPage('AG2_CAM_2',true)"><a href="#" title="" class="activo"><span>PRIMARIA Y SECUNDARIA</span></a></li>
        <li onclick="openPage('AG3_CAM_2',false)"><a href="#" title=""><span>CAP. P/TRABAJO Y ATENCI�N COMP.</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL DOCENTE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
       <%-- <li onclick="openPage('Anexo')"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
              </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td colspan="16" style="text-align: justify">
                    <asp:Label ID="lblInstruccion3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Escriba el n�mero de alumnos por sexo y el n�mero de grupos de educaci�n primaria, seg�n los rubros que se indican."
                        Width="100%"></asp:Label></td>
                <td colspan="1" style="text-align: justify">
                </td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center">
                </td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionI3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N INICIAL"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionT3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblBajas3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="BAJAS"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblExistencia3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblAprobados3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblReprobados3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="REPROBADOS"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblIntegrados3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INTEGRADOS A EDUCACI�N REGULAR"
                        Width="100px"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblGrupos3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
                <td class="Orila" rowspan="2" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center">
                </td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom31" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj31" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom32" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj32" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom33" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj33" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom34" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj34" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom35" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj35" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom36" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj36" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom37" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj37" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="height: 26px; text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lbl31o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV134" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV135" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV136" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV137" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV138" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV139" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV140" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV141" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV142" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10109"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV143" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10110"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV144" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10111"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV145" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10112"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV146" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10113"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV147" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10114"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV148" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10115"></asp:TextBox></td>
                <td class="Orila" style="height: 26px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 26px; text-align: left" class="linaBajo">
                    <asp:Label ID="lbl32o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV149" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV150" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV151" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV152" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV153" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV154" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV155" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV156" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV157" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10209"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV158" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10210"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV159" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10211"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV160" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10212"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV161" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10213"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV162" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10214"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV163" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10215"></asp:TextBox></td>
                <td class="Orila" style="height: 26px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 26px; text-align: left" class="linaBajo">
                    <asp:Label ID="lbl33o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV164" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV165" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV166" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV167" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV168" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV169" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV170" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV171" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV172" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10309"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV173" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10310"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV174" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10311"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV175" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10312"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV176" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10313"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV177" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10314"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV178" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10315"></asp:TextBox></td>
                <td class="Orila" style="height: 26px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 26px; text-align: left" class="linaBajo">
                    <asp:Label ID="lbl34o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV179" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV180" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV181" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV182" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV183" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV184" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV185" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV186" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV187" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10409"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV188" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10410"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV189" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10411"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV190" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10412"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV191" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10413"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV192" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10414"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV193" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10415"></asp:TextBox></td>
                <td class="Orila" style="height: 26px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 26px; text-align: left" class="linaBajo">
                    <asp:Label ID="lbl35o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5o."
                        Width="100%"></asp:Label></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV194" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV195" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV196" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV197" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV198" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV199" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV200" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV201" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV202" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10509"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV203" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10510"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV204" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10511"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV205" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10512"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV206" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10513"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV207" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10514"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV208" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10515"></asp:TextBox></td>
                <td class="Orila" style="height: 26px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 26px; text-align: left" class="linaBajo">
                    <asp:Label ID="lbl36o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="6o."
                        Width="100%"></asp:Label></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV209" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV210" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV211" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV212" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV213" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV214" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV215" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV216" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV217" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10609"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV218" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10610"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV219" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10611"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV220" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10612"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV221" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10613"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV222" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10614"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV223" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10615"></asp:TextBox></td>
                <td class="Orila" style="height: 26px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 26px; text-align: left" class="linaBajo">
                    <asp:Label ID="lblTotal31" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV224" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV225" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV226" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV227" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV228" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV229" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV230" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV231" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV232" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10709"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV233" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10710"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV234" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10711"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV235" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10712"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV236" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10713"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV237" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10714"></asp:TextBox></td>
                <td style="height: 26px; text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV238" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10715"></asp:TextBox></td>
                <td class="Orila" style="height: 26px; text-align: center">
                    &nbsp;</td>
            </tr>
        </table>
        <br />
   
    
    <table>
        <tr>
            <td colspan="4" style="text-align: justify">
                <asp:Label ID="lblInstruccion3b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro de inscripcion total, escriba el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblSituacion3" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal32" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblCeguera3" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV239" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV240" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV241" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblVisual3" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV242" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV243" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV244" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblSordera3" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV245" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV246" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV247" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblAuditiva3" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV248" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV249" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV250" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblMotriz3" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV251" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV252" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV253" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblIntelectual3" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV254" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV255" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV256" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="text-align: left">
                <asp:Label ID="lblSobresalientes3" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV257" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV258" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV259" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblOtros3" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV260" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV261" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV262" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblTotal33" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV263" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV264" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV265" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td colspan="16" style="height: 15px; text-align: justify">
                <asp:Label ID="lblInstruccion4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Escriba el n�mero de alumnos por sexo y el n�mero de grupos de educaci�n secundaria, seg�n los rubros que se indican."
                    Width="100%"></asp:Label></td>
            <td colspan="1" style="height: 15px; text-align: justify">
            </td>
        </tr>
        <tr>
            <td colspan="1" style="text-align: center">
            </td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblInscripcionI4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N INICIAL"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblInscripcionT4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblBajas4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="BAJAS"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblExistencia4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EXISTENCIA"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblAprobados4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="APROBADOS"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblReprobados4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="REPROBADOS"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblIntegrados4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INTEGRADOS A EDUCACI�N REGULAR"
                    Width="100px"></asp:Label></td>
            <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblGrupos4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                    Width="100%"></asp:Label></td>
            <td class="Orila" rowspan="2" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center">
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom41" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj41" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom42" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj42" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom43" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj43" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom44" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj44" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom45" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj45" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom46" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj46" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom47" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj47" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 26px; text-align: center" class="linaBajoAlto">
                <asp:Label ID="lbl41o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                    Width="100%"></asp:Label></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV266" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV267" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV268" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV269" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV270" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20105"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV271" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20106"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV272" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20107"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV273" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20108"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV274" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20109"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV275" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20110"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV276" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20111"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV277" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20112"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV278" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20113"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV279" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20114"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV280" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20115"></asp:TextBox></td>
            <td class="Orila" style="height: 26px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:Label ID="lbl42o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                    Width="100%"></asp:Label></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV281" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV282" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV283" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV284" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20204"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV285" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20205"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV286" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20206"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV287" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20207"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV288" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20208"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV289" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20209"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV290" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20210"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV291" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20211"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV292" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20212"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV293" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20213"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV294" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20214"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV295" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20215"></asp:TextBox></td>
            <td class="Orila" style="height: 26px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:Label ID="lbl43o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                    Width="100%"></asp:Label></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV296" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV297" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV298" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV299" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20304"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV300" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20305"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV301" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20306"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV302" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20307"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV303" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20308"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV304" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20309"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV305" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20310"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV306" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20311"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV307" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20312"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV308" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20313"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV309" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20314"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV310" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20315"></asp:TextBox></td>
            <td class="Orila" style="height: 26px; text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 26px; text-align: center" class="linaBajo">
                &nbsp;<asp:Label ID="lblTotal41" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                    Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV311" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV312" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV313" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV314" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20404"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV315" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20405"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV316" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20406"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV317" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20407"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV318" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20408"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV319" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20409"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV320" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20410"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV321" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20411"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV322" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20412"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV323" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20413"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV324" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20414"></asp:TextBox></td>
            <td style="height: 26px; text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV325" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20415"></asp:TextBox></td>
            <td class="Orila" style="height: 26px; text-align: center">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td colspan="4" style="text-align: justify">
                <asp:Label ID="lblInstruccion4b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro de inscripcion total, escriba el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblSituacion4" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal42" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblCeguera4" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV326" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV327" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV328" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30103"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblVisual4" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV329" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV330" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV331" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30203"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblSordera4" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV332" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV333" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV334" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30303"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblAuditiva4" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV335" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV336" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV337" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30403"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblMotriz4" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV338" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV339" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV340" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30503"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblIntelectual4" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV341" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV342" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV343" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30503"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="text-align: left">
                <asp:Label ID="lblSobresalientes4" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV344" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV345" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30602"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV346" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30603"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblOtros4" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV347" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV348" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV349" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30703"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblTotal43" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV350" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV351" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30802"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV352" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30803"></asp:TextBox></td>
        </tr>
    </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG1_CAM_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG1_CAM_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AG3_CAM_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG3_CAM_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
         <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
