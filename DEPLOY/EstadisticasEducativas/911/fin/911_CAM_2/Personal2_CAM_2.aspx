<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="CAM-2(Personal)" AutoEventWireup="true" CodeBehind="Personal2_CAM_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin.CAM_2.Personal2_CAM_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
<%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 25;
        MaxRow = 39;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
      <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N ESPECIAL - CENTRO DE ATENCI�N M�LTIPLE</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1500px;">
      <ul class="left">
        <li onclick="OpenPageCharged('Identificacion_911_CAM_2',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="OpenPageCharged('AG1_CAM_2',true)"><a href="#" title=""><span>INICIAL Y PREESCOLAR</span></a></li>
        <li onclick="OpenPageCharged('AG2_CAM_2',true)"><a href="#" title=""><span>PRIMARIA Y SECUNDARIA</span></a></li>
        <li onclick="OpenPageCharged('AG3_CAM_2',true)"><a href="#" title=""><span>CAP. P/TRABAJO Y ATENCI�N COMP.</span></a></li>
        <li onclick="OpenPageCharged('Desglose_CAM_2',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="OpenPageCharged('Personal1_CAM_2',true)"><a href="#" title=""><span>PERSONAL DOCENTE</span></a></li>
        <li onclick="OpenPageCharged('Personal2_CAM_2',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="OpenPageCharged('Inmueble_911_CAM_2',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
   <%-- <li onclick="openPage('Anexo')"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
 
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
 
        <table>
            <tr>
                <td valign="top">
                    <table style="width: 650px">
                        <tr>
                            <td>
                                <asp:Label ID="lblPARADOCENTE" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PERSONAL PARADOCENTE"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccion6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6. Registre la cantidad de personal paradocente por formaci�n, seg�n su situaci�n acad�mica y sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td style="text-align: center">
                                        </td>
                                        <td colspan="8" style="text-align: center">
                                            <asp:Label ID="lblLicenciatura" runat="server" CssClass="lblRojo" Font-Bold="True" Text="L I C E N C I A T U R A"
                                                Width="100%"></asp:Label></td>
                                        <td colspan="1" style="text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                        </td>
                                        <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblTitulados" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TITULADOS"
                                                Width="100%"></asp:Label></td>
                                        <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblNoTitulados" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NO TITULADOS"
                                                Width="100%"></asp:Label></td>
                                        <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblEstudiantes" runat="server" CssClass="lblNegro" Font-Bold="True" Text="ESTUDIANTES"
                                                Width="100%"></asp:Label></td>
                                        <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblTotal61" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                                Width="100%"></asp:Label></td>
                                        <td class="Orila" colspan="1" style="text-align: center">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblFormacion" runat="server" CssClass="lblRojo" Font-Bold="True" Text="FORMACI�N"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lblHom61" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lblMuj61" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lblHom62" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lblMuj62" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lblHom63" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lblMuj63" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center" class="linaBajo">
                                            <asp:Label ID="lblHom64" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                                Width="100%"></asp:Label></td>
                                        <td style="width: 57px; text-align: center" class="linaBajo">
                                            <asp:Label ID="lblMuj64" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                                Width="100%"></asp:Label></td>
                                        <td class="Orila" style="width: 57px; text-align: center">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajoAlto">
                                            <asp:Label ID="lblMaestroLenguaje" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO DE LENGUAJE"
                                                Width="100%"></asp:Label></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1337" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1338" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1339" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1340" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1341" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1342" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1343" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1344" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:Label ID="lblTerapista" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERAPISTA F�SICO"
                                                Width="100%"></asp:Label></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1345" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1346" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1347" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1348" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1349" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1350" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1351" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1352" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:Label ID="lblPsicologo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PSIC�LOGO"
                                                Width="100%"></asp:Label></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1353" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1354" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1355" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1356" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1357" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1358" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1359" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1360" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:Label ID="lblTrabajadorS" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TRABAJADOR SOCIAL"
                                                Width="100%"></asp:Label></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1361" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1362" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1363" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1364" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1365" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1366" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1367" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1368" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:Label ID="lblMedico" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M�DICO"
                                                Width="100%"></asp:Label></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1369" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1370" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1371" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1372" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1373" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1374" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1375" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1376" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:Label ID="lblMaestroEdF" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO DE EDUCACI�N F�SICA"
                                                Width="100%"></asp:Label></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1377" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1378" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1379" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1380" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1381" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1382" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1383" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1384" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:Label ID="lblMaestroEsp" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO ESPECIALISTA"
                                                Width="100%"></asp:Label></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1385" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1386" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1387" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1388" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1389" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1390" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1391" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1392" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left" class="linaBajo">
                                            <asp:Label ID="lblEspecialidad6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Escriba la especialidad del maestro."
                                                Width="100%"></asp:Label></td>
                                        <td colspan="7" style="text-align: left" class="linaBajo">
                                            &nbsp;<asp:TextBox ID="txtV1393" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                                        <td class="Orila" colspan="1" style="text-align: left">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                            <td colspan="10" class="linaBajo" style="text-align: left; height:30px;">
                                 <asp:Label ID="lblInstruccion5b" runat="server" CssClass="lblRojo" 
                                     Font-Bold="True" Text="Si existe personal paradocente con formaci�n diferente de las especialidades, registre la cantidad de personal de acuerdo con su situaci�n acad�mica y sexo."
                                    Width="600px"></asp:Label>
                           </td>
                        </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV1394" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1395" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1396" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1397" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1398" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1399" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1400" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1401" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10908"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1402" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10909"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV1403" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1404" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1405" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1406" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1407" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1408" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1409" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1410" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11008"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1411" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11009"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV1412" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1413" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1414" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1415" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1416" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1417" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1418" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1419" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11108"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1420" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11109"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV1421" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1422" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1423" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1424" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1425" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1426" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1427" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1428" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11208"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1429" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11209"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV1430" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1431" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1432" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1433" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1434" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1435" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1436" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1437" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11308"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1438" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11309"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV1439" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1440" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1441" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1442" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1443" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1444" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1445" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1446" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11408"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1447" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11409"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" class="linaBajo">
                                            <asp:TextBox ID="txtV1448" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1449" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1450" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1451" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1452" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1453" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1454" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
                                        <td class="linaBajo">
                                            <asp:TextBox ID="txtV1455" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11508"></asp:TextBox></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1456" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11509"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="3" class="linaBajo">
                                            <asp:Label ID="lblTotal62" runat="server" CssClass="lblGrisTit" Text="TOTAL (Suma de HOM y MUJ)"
                                                Width="100%"></asp:Label></td>
                                        <td style="width: 57px" class="linaBajo">
                                            <asp:TextBox ID="txtV1457" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11510"></asp:TextBox></td>
                                        <td class="Orila" style="width: 57px">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table style="width: 450px">
                        <tr>
                            <td>
                                <asp:Label ID="lblADMINISTRATIVO" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PERSONAL ADMINISTRATIVO"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccion7" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7. Registre la cantidad de personal administrativo por sexo, seg�n la funci�n que desempe�e."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccion7b" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Si existe personal administrativo que desempe�e labores diferentes de las que est�n se�aladas, describalo y registre su n�mero."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <table>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblHombres7" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblMujeres7" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblNi�era7" runat="server" CssClass="lblGrisTit" Text="NI�ERA" Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1458" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1459" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblAuxiliar7" runat="server" CssClass="lblGrisTit" Text="AUXILIAR ADMINISTRATIVO"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1460" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1461" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblSecretaria7" runat="server" CssClass="lblGrisTit" Text="SECRETARIA" Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1462" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1463" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblVigilante7" runat="server" CssClass="lblGrisTit" Text="VIGILANTE" Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1464" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1465" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblIntendente7" runat="server" CssClass="lblGrisTit" Text="ASISTENTE DE SERVICIOS (INTENDENTE)"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1466" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1467" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px; text-align: left">
                                            <asp:Label ID="lblChofer7" runat="server" CssClass="lblGrisTit" Text="CHOFER" Width="100%"></asp:Label></td>
                                        <td style="height: 26px; text-align: center">
                                            <asp:TextBox ID="txtV1468" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
                                        <td style="height: 26px; text-align: center">
                                            <asp:TextBox ID="txtV1469" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1470" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1471" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20802"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1472" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20803"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1473" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20901"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1474" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20902"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1475" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20903"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1476" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21001"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1477" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21002"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1478" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21003"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1479" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21101"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1480" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21102"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1481" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21103"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1482" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21201"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1483" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21202"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1484" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21203"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1485" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21301"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1486" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21302"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1487" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21303"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1488" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21401"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1489" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21402"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1490" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21403"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1491" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21501"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1492" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21502"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1493" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21503"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV1494" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21601"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1495" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21602"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1496" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21603"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px; text-align: left">
                                            <asp:TextBox ID="txtV1497" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21701"></asp:TextBox></td>
                                        <td style="height: 26px; text-align: center">
                                            <asp:TextBox ID="txtV1498" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21702"></asp:TextBox></td>
                                        <td style="height: 26px; text-align: center">
                                            <asp:TextBox ID="txtV1499" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21703"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblTotal71" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV1500" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21801"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTotal72" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL DE PERSONAL (Considere al personal reportado en los apartados 1, 5, 6 y 7.)"
                                                Width="300px"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV1501" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30101"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: justify">
                                <asp:Label ID="lblInstruccion8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8.  Registre el personal con licencia limitada."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: right">
                                <asp:TextBox ID="txtV1502" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="30201"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: justify">
                                <asp:Label ID="lblInstruccion9" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9. Escriba el personal comisionado."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblInterno9" runat="server" CssClass="lblGrisTit" Text="INTERNO" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV1503" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="30301"></asp:TextBox></td>
                                        <td nowrap="nowrap" style="width: 40px">
                                        </td>
                                        <td>
                                            <asp:Label ID="lblExterno9" runat="server" CssClass="lblGrisTit" Text="EXTERNO" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV1504" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="30401"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 19px; text-align: justify">
                                <asp:Label ID="lblInstruccion10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10. Registre el personal becado."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:TextBox ID="txtV1505" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="30501"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('Personal1_CAM_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Personal1_CAM_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="OpenPageCharged('Inmueble_911_CAM_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Inmueble_911_CAM_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
    <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
       </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana,ir){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1393'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1394'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1403'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1412'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1421'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1430'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1439'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1448'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1470'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1473'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1476'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1479'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1482'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1485'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1488'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1491'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1494'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1497'));
                    openPage(ventana,ir);
                }                                
 		Disparador(<%=hidDisparador.Value %>);
            GetTabIndexes();
        </script> 
</asp:Content>
