<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="ECC-22(3er. Nivel)" AutoEventWireup="true" CodeBehind="Alumnos3_ECC_22.aspx.cs" Inherits="EstadisticasEducativas._911.fin.ECC_22.Alumnos3_ECC_22" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N COMUNITARIA RURAL PRIMARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_ECC_22',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos1_ECC_22',true)"><a href="#" title="" ><span>PRIMER NIVEL</span></a></li>
        <li onclick="openPage('Alumnos2_ECC_22',true)"><a href="#" title=""><span>SEGUNDO NIVEL</span></a></li>
        <li onclick="openPage('Alumnos3_ECC_22',true)"><a href="#" title="" class="activo"><span>TERCER NIVEL</span></a></li>
        <li onclick="openPage('Anexo_ECC_22',false)"><a href="#" title=""><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td colspan="2">
    
        <table>
            <tr>
                <td colspan="15" style="text-align: center">
                    <asp:Label ID="lblTituloTabla" runat="server" CssClass="lblNegro" Text="Estad�stica de alumnos por nivel, ciclo (a�o), sexo, inscripci�n total, existencia, aprobados y edad"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="Orila" colspan="1" style="text-align: center">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblmenos6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl6a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl7a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl8a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl9a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl10a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl11a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl12a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom; width: 64px;" class="linaBajoAlto">
                    <asp:Label ID="lbl13a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl14a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl15a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 y m�s a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1er ciclo (1er a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblHombresC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV652" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV653" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV654" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV655" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV656" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV657" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV658" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV659" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV660" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV661" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10110"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV662" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10111"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV663" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10112"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV664" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV665" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV666" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV667" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV668" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV669" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV670" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV671" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV672" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV673" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10210"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV674" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10211"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV675" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10212"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV676" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV677" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV678" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV679" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV680" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV681" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV682" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV683" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS">
                    <asp:TextBox ID="txtV684" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV685" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10310"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV686" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10311"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV687" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10312"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblMujeresC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV688" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV689" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV690" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV691" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV692" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV693" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV694" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV695" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV696" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV697" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10410"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV698" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10411"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV699" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10412"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV700" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV701" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV702" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV703" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV704" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV705" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV706" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV707" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV708" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV709" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10510"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV710" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10511"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV711" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10512"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV712" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV713" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV714" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV715" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV716" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV717" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV718" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV719" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS">
                    <asp:TextBox ID="txtV720" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV721" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10610"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV722" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10611"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV723" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10612"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV724" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV725" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV726" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV727" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV728" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV729" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV730" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV731" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV732" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV733" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10710"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV734" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10711"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV735" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10712"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV736" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV737" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV738" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV739" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV740" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV741" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV742" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV743" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10808"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV744" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV745" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10810"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV746" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10811"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV747" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10812"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV748" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV749" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV750" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV751" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV752" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV753" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV754" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV755" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10908"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV756" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV757" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10910"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV758" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10911"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV759" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10912"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            
             <tr>
                <td>
                </td>
                <td>
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom; width: 64px;" class="linaBajoAlto">
                    <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 y m�s a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom; text-align: center">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2do ciclo (2do a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblHombresC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV760" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV761" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV762" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV763" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV764" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV765" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV766" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV767" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11008"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV768" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV769" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11010"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV770" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11011"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV771" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11012"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV772" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV773" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV774" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV775" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV776" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV777" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV778" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV779" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11108"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV780" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV781" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11110"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV782" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11111"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV783" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11112"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV784" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV785" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV786" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV787" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV788" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV789" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV790" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV791" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11208"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS">
                    <asp:TextBox ID="txtV792" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV793" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11210"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV794" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11211"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV795" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11212"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblMujeresC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV796" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV797" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV798" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV799" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV800" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV801" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV802" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV803" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11308"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV804" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV805" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11310"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV806" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11311"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV807" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11312"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV808" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV809" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV810" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV811" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV812" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV813" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV814" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV815" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11408"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV816" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV817" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11410"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV818" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11411"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV819" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11412"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV820" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV821" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV822" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV823" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV824" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV825" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV826" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV827" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11508"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS">
                    <asp:TextBox ID="txtV828" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11509"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV829" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11510"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV830" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11511"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV831" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11512"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV832" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV833" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV834" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV835" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV836" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV837" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV838" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV839" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11608"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV840" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV841" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11610"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV842" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11611"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV843" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11612"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV844" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV845" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV846" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV847" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV848" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV849" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV850" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV851" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11708"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV852" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV853" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11710"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV854" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11711"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV855" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11712"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV856" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV857" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV858" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV859" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV860" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV861" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV862" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV863" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11808"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV864" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV865" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11810"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV866" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11811"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV867" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11812"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td>
                </td>
                <td>
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom; width: 64px;" class="linaBajoAlto">
                    <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label22" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label23" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 y m�s a�os"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label24" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3er ciclo (3er a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblHombresC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV868" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV869" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV870" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV871" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV872" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV873" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV874" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV875" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11908"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV876" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV877" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11910"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV878" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11911"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV879" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11912"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV880" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV881" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV882" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV883" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV884" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV885" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV886" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV887" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12008"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV888" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV889" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12010"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV890" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12011"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV891" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12012"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosHC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV892" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV893" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV894" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV895" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV896" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV897" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV898" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV899" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12108"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS">
                    <asp:TextBox ID="txtV900" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV901" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12110"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV902" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12111"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV903" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12112"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblMujeresC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV904" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV905" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV906" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV907" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV908" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV909" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV910" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV911" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12208"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV912" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV913" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12210"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV914" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12211"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV915" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12212"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV916" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV917" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV918" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV919" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV920" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV921" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV922" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV923" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12308"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo">
                    <asp:TextBox ID="txtV924" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV925" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12310"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV926" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12311"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV927" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12312"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV928" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV929" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV930" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV931" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV932" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV933" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV934" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV935" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12408"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS">
                    <asp:TextBox ID="txtV936" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV937" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12410"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV938" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12411"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV939" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12412"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV940" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV941" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV942" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV943" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV944" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV945" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV946" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV947" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12508"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV948" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV949" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12510"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV950" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12511"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV951" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12512"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV952" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV953" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV954" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV955" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV956" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV957" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV958" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV959" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12608"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV960" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV961" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12610"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV962" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12611"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV963" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12612"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV964" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV965" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV966" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV967" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV968" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV969" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV970" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV971" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12708"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV972" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV973" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12710"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV974" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12711"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV975" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12712"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="1" style="height: 34px">
                </td>
                <td rowspan="1" style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="border-bottom: gray thin solid; height: 34px; text-align: left">
                </td>
                <td style="height: 34px; text-align: center">
                </td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                </td>
                <td rowspan="3" class="linaBajo Sombreado">
                    <asp:Label ID="lblTotalGeneral" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV976" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV977" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV978" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV979" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV980" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV981" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV982" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV983" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12808"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV984" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV985" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12810"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV986" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12811"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV987" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12812"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV988" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV989" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV990" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV991" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV992" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV993" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV994" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV995" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12908"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV996" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV997" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12910"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV998" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12911"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV999" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12912"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1000" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1001" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1002" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1003" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1004" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1005" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1006" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1007" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13008"></asp:TextBox></td>
                <td style="text-align: center; width: 64px;" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1008" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1009" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13010"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1010" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13011"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV1011" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13012"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            
            
        </table>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblPERSONAL" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="II. PERSONAL" Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInstuccionII1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Escriba el n�mero de instructores comunitarios."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotalInstructores" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                    <asp:TextBox ID="txtV1012" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" TabIndex="2" ></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
              
            </tr>
        </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Alumnos2_ECC_22',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Alumnos2_ECC_22',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Anexo_ECC_22',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_ECC_22',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
            <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
         <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>
