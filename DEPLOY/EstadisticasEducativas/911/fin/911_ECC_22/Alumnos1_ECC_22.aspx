<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="ECC-22(1er. Nivel)" AutoEventWireup="true" CodeBehind="Alumnos1_ECC_22.aspx.cs" Inherits="EstadisticasEducativas._911.fin.ECC_22.Alumnos1_ECC_22" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <script type="text/javascript">
        var _enter=true;
    </script>
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 28;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N COMUNITARIA RURAL PRIMARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_ECC_22',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos1_ECC_22',true)"><a href="#" title="" class="activo"><span>PRIMER NIVEL</span></a></li>
        <li onclick="openPage('Alumnos2_ECC_22',false)"><a href="#" title=""><span>SEGUNDO NIVEL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TERCER NIVEL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
     </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td >
                    <asp:Label ID="lblALUMNOS" runat="server" CssClass="lblRojo" Font-Size="16px" Text="I. ALUMNOS"></asp:Label></td>
                
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblInstrucci�nI1" runat="server" CssClass="lblRojo" Text="1. Seleccione el tipo de servicio."></asp:Label></td>
                
            </tr>
            <tr>
                <td >
                        <table >
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblComunitario" runat="server" CssClass="lblGrisTit" Height="17px"
                                        Text="Cursos comunitarios" Width="100%"></asp:Label></td>
                                <td >
                                    <asp:RadioButton ID="optV1" runat="server" GroupName="servicio" TabIndex="10101" />
                                    <asp:TextBox ID="txtV1" runat="server"  style="visibility:hidden;" ></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblPAEPI" runat="server" CssClass="lblGrisTit" Height="17px" Text="Proyecto de Atenci�n Educativa a la Poblaci�n Ind�gena (PAEPI)"
                                        Width="100%"></asp:Label></td>
                                <td >
                                    <asp:RadioButton ID="optV2" runat="server" GroupName="servicio" TabIndex="10102" />
                                    <asp:TextBox ID="txtV2" runat="server"  style="visibility:hidden;" ></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblPAEPIAM" runat="server" CssClass="lblGrisTit" Height="17px" Text="Proyecto de Atenci�n Educativa a la Poblaci�n Infantil Agr�cola Migrante (PAEPIAM)"
                                        Width="100%"></asp:Label></td>
                                <td >
                                    <asp:RadioButton ID="optV3" runat="server" GroupName="servicio" TabIndex="10103" />
                                    <asp:TextBox ID="txtV3" runat="server" style="visibility:hidden;" ></asp:TextBox></td>
                            </tr>
                        </table>
                </td>
              
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblInstrucci�nI2" runat="server" CssClass="lblRojo" Text="2. �El proyecto es de aulas compartidas?"></asp:Label></td>
               
            </tr>
            <tr>
                <td style="text-align: left">
                        <table>
                            <tr>
                                <td >
                                    <asp:RadioButton ID="optV1014" runat="server" GroupName="compartidas" Text="S�" TabIndex="10201" />
                                    <asp:TextBox ID="txtV1014" runat="server" style="visibility:hidden; width:20px;" ></asp:TextBox></td>
                                <td >
                                    <asp:RadioButton ID="optV1015" runat="server" GroupName="compartidas" Text="NO" TabIndex="10202" />
                                    <asp:TextBox ID="txtV1015" runat="server" style="visibility:hidden; width:20px;" ></asp:TextBox></td>
                            </tr>
                        </table>
                </td>
               
            </tr>
            <tr>
                <td  style=" width: 900px" >
                    <asp:Label ID="lblInstrucci�nI3" runat="server" CssClass="lblRojo" Text="3. En esta p�gina y las dos siguientes, escriba el total de alumnos, desglos�ndolo por nivel, ciclo (a�o), sexo, inscripci�n total, existencia, aprobados y edad. Verifique que la suma de los subtotales de los alumnos por edad sea igual al total."></asp:Label></td>
            </tr>
            
        </table>
        <table>
            <tr>
                <td colspan="15" >
                    <asp:Label ID="lblTituloTabla" runat="server" CssClass="lblNegro" Text="Estad�stica de alumnos por nivel, ciclo (a�o), sexo, inscripci�n total, existencia, aprobados y edad"
                         Font-Bold="True"></asp:Label></td>
                <td colspan="1">
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td >
                </td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblmenos6a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl6a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl7a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl8a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl9a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl10a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl11a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl12a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl13a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl14a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl15a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 a�os y m�s"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1er ciclo (1er a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" style="text-align: left" class=" linaBajoS linaBajoAlto">
                    <asp:Label ID="lblHombresC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="INSCRIPCI�N TOTAL" Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV4" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV5" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV6" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV7" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV8" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20105"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV9" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20106"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV10" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20107"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV11" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20108"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV12" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20109"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV13" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20110"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV14" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20111"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV15" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20112"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="EXISTENCIA" Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV16" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV17" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV18" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV19" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20204"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV20" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20205"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV21" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20206"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV22" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20207"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV23" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20208"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV24" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20209"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV25" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20210"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV26" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20211"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV27" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20212"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV28" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV29" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV30" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV31" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20304"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV32" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20305"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV33" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20306"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV34" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20307"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV35" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20308"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV36" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20309"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV37" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20310"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV38" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20311"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV39" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20312"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
           
                 <tr>
                <td rowspan="3" style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblMujeresC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="INSCRIPCI�N TOTAL" Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV40" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV41" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV42" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV43" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20404"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV44" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20405"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV45" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20406"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV46" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20407"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV47" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20408"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV48" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20409"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV49" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20410"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV50" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20411"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV51" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20412"></asp:TextBox></td>
                     <td class="Orila">
                         &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="EXISTENCIA" Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV52" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV53" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV54" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20503"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV55" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20504"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV56" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20505"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV57" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20506"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV58" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20507"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV59" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20508"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV60" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20509"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV61" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20510"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV62" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20511"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV63" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20512"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV64" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV65" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV66" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20603"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV67" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20604"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV68" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20605"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV69" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20606"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV70" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20607"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV71" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20608"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV72" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20609"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV73" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20610"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV74" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20611"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV75" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20612"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>

            <tr>
                <td rowspan="3" style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="INSCRIPCI�N TOTAL" Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV76" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV77" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20702"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV78" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20703"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV79" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20704"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV80" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20705"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV81" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20706"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV82" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20707"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV83" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20708"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV84" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20709"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV85" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20710"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV86" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20711"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV87" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20712"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="EXISTENCIA" Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV88" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV89" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20802"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV90" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20803"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV91" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20804"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV92" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20805"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV93" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20806"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV94" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20807"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV95" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20808"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV96" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20809"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV97" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20810"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV98" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20811"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV99" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20812"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV100" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20901"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV101" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20902"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV102" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20903"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV103" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20904"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV104" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20905"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV105" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20906"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV106" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20907"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV107" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20908"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV108" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20909"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV109" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20910"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV110" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20911"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV111" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20912"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td></td>
                <td>
                </td>
                <td >
                </td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 a�os y m�s"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2do ciclo (2do a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblHombresC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV112" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21001"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV113" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21002"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV114" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21003"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV115" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21004"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV116" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21005"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV117" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21006"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV118" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21007"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV119" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21008"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV120" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21009"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV121" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21010"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV122" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21011"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV123" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21012"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            
           
            
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV124" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21101"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV125" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21102"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV126" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21103"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV127" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21104"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV128" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21105"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV129" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21106"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV130" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21107"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV131" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21108"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV132" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21109"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV133" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21110"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV134" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21111"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV135" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21112"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV136" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21201"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV137" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21202"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV138" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21203"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV139" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21204"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV140" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21205"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV141" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21206"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV142" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21207"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV143" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21208"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV144" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21209"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV145" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21210"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV146" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21211"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV147" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21212"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblMujeresC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV148" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21301"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV149" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21302"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV150" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21303"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV151" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21304"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV152" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21305"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV153" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21306"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV154" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21307"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV155" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21308"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV156" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21309"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV157" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21310"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV158" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21311"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV159" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21312"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV160" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21401"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV161" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21402"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV162" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21403"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV163" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21404"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV164" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21405"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV165" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21406"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV166" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21407"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV167" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21408"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV168" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21409"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV169" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21410"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV170" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21411"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV171" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21412"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV172" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21501"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV173" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21502"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV174" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21503"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV175" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21504"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV176" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21505"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV177" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21506"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV178" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21507"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV179" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21508"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV180" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21509"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV181" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21510"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV182" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21511"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV183" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21512"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV184" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21601"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV185" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21602"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV186" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21603"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV187" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21604"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV188" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21605"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV189" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21606"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV190" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21607"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV191" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21608"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV192" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21609"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV193" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21610"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV194" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21611"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV195" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21612"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV196" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21701"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV197" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21702"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV198" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21703"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV199" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21704"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV200" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21705"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV201" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21706"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV202" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21707"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV203" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21708"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV204" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21709"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV205" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21710"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV206" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21711"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV207" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21712"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV208" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21801"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV209" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21802"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV210" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21803"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV211" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21804"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV212" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21805"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV213" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21806"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV214" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21807"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV215" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21808"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV216" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21809"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV217" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21810"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV218" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21811"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV219" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21812"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
    
           <tr>
                <td></td>
                <td>
                </td>
                <td >
                </td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label22" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label23" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 a�os y m�s"
                        Width="60px"></asp:Label></td>
                <td style=" vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label24" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom">
                    &nbsp;</td>
            </tr>
    
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3er ciclo (3er a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblHombresC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV220" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21901"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV221" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21902"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV222" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21903"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV223" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21904"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV224" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21905"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV225" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21906"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV226" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21907"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV227" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21908"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV228" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21909"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV229" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21910"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV230" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21911"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV231" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21912"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV232" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22001"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV233" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22002"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV234" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22003"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV235" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22004"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV236" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22005"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV237" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22006"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV238" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22007"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV239" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22008"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV240" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22009"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV241" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22010"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV242" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22011"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV243" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22012"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosH3C" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV244" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22101"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV245" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22102"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV246" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22103"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV247" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22104"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV248" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22105"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV249" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22106"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV250" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22107"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV251" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22108"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV252" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22109"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV253" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22110"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV254" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22111"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV255" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22112"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblMujeresC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV256" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22201"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV257" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22202"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV258" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22203"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV259" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22204"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV260" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22205"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV261" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22206"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV262" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22207"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV263" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22208"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV264" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22209"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV265" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22210"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV266" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22211"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV267" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22212"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV268" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22301"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV269" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22302"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV270" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22303"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV271" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22304"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV272" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22305"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV273" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22306"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV274" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22307"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV275" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22308"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV276" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22309"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV277" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22310"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV278" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22311"></asp:TextBox></td>
                <td class="linaBajo" >
                    <asp:TextBox ID="txtV279" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22312"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV280" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22401"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV281" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22402"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV282" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22403"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV283" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22404"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV284" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22405"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV285" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22406"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV286" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22407"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV287" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22408"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV288" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22409"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV289" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22410"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV290" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22411"></asp:TextBox></td>
                <td class="linaBajoS" >
                    <asp:TextBox ID="txtV291" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22412"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV292" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22501"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV293" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22502"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV294" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22503"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV295" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22504"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV296" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22505"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV297" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22506"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV298" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22507"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV299" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22508"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV300" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22509"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV301" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22510"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV302" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22511"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV303" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22512"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV304" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22601"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV305" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22602"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV306" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22603"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV307" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22604"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV308" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22605"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV309" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22606"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV310" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22607"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV311" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22608"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV312" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22609"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV313" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22610"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV314" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22611"></asp:TextBox></td>
                <td class="linaBajo Sombreado" >
                    <asp:TextBox ID="txtV315" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22612"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV316" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22701"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV317" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22702"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV318" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22703"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV319" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22704"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV320" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22705"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV321" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22706"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV322" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22707"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV323" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22708"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV324" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22709"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV325" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22710"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV326" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22711"></asp:TextBox></td>
                <td class="linaBajoS Sombreado" >
                    <asp:TextBox ID="txtV327" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22712"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
        </table>
                
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_ECC_22',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_ECC_22',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Alumnos2_ECC_22',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Alumnos2_ECC_22',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /> 
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                function PintaOPTs(){
                     marcar('V1');
                     marcar('V2');
                     marcar('V3');
                     marcar('V1014');
                     marcar('V1015');
//                     var x = document.getElementById('hidListaTxtBoxs').value;
//                     var c = x.split('|');
//                     var i;
//                     for (i=0;i < c.length-1; i++)
//                     {
//                         document.writeln(c[i].split(',')[0]+'.Attributes["onkeypress"] = "return Num(event)";');
//                     }
                }  
                
                function marcar(variable){
                     var txtv = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                     if (txtv != null) {
                         var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                         if (txtv.value == 'X'){
                             chk.checked = true;
                         } else {
                             chk.checked = false;
                         } 
                     }
                }
                
                function OPTs2Txt(){
                     marcarTXT('V1');
                     marcarTXT('V2');
                     marcarTXT('V3');
                     marcarTXT('V1014');
                     marcarTXT('V1015');
                }    
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         
                         var txt = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }
                PintaOPTs();             
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>
