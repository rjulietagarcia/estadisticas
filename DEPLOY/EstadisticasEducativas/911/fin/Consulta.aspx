﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasicMenu.Master" AutoEventWireup="true" CodeBehind="Consulta.aspx.cs" Inherits="EstadisticasEducativas._911.fin.Consulta" Title="Consulta" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
    </asp:ScriptManagerProxy>
    
  
        <br />
        <br />
        <center>
        <div style="text-align:center; width:100%">
             <center>
                
           <table class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"></td>
				    <td>
				        <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">										
								<tr>
									<td class="Titulo" style="FONT-WEIGHT: bold" align="center" >
                                        Oficializados</td>
								</tr>
						</table>
						<br />
				   <asp:UpdatePanel ID="upBusqueda" runat="server">
                        <ContentTemplate> 
                        
			            <table   border="0" cellpadding="0" cellspacing="2"  >
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label ID="Label1" runat="server" CssClass="lblEtiqueta" Font-Size="14px" Text="Ciclo Escolar:"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlCiclo" runat="server" Enabled="false">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                 <td align="left" style="width: 133px">
                                    <asp:Label ID="lblEntidad" runat="server" CssClass="lblEtiqueta" Font-Size="14px" Text="Entidad:"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                   <asp:DropDownList ID="ddlEntidad" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEntidad_SelectedIndexChanged">
                                   <asp:ListItem Value="0">Seleccione un Estado</asp:ListItem>
                                   </asp:DropDownList></td>
                            </tr> 
                             <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblRegion" runat="server" Text="Región:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                   
                                        <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="True" Enabled="false" >
                                            <asp:ListItem Value="0">Seleccione una Región</asp:ListItem>
                                    </asp:DropDownList>
                                     
                                        </td>
                            </tr>
                            
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label ID="lblNivel" runat="server"  Font-Size="14px" Text="Nivel:" CssClass="lblEtiqueta" ></asp:Label>
                                    </td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlNivel" runat="server" OnSelectedIndexChanged="ddlNivel_SelectedIndexChanged">
                                    <asp:ListItem Value="0">Seleccione un Nivel</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                           
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblZona" runat="server" Text="Zona:" Width="43px" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtZona" runat="server" Width="55px" TabIndex="2"
                                        MaxLength="3"></asp:TextBox></td>
                            </tr>
                            <tr id="trNiv">
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblClave" runat="server" Text="Centro de Trabajo:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtClaveCT" runat="server" Width="110px" TabIndex="3"
                                        MaxLength="10"></asp:TextBox>
                                    <asp:DropDownList ID="ddlCentroTrabajo" runat="server" Visible="False">
                                    </asp:DropDownList></td>
                            </tr>
                             <tr>
                                    <td align="left" style="width: 133px">
                                        <asp:Label ID="lblEstatus" runat="server" CssClass="lblEtiqueta" Font-Size="14px" Text="Estatus:" ></asp:Label>
                                    </td>
                                    <td align="left" style="width: 120px">
                                        <asp:DropDownList ID="ddlEstatus" runat="server">
                                        </asp:DropDownList></td>
                             </tr>
                             <tr>
                             <td align="left" style="width: 133px">
                                 <asp:Label ID="lblTipoCaptura" runat="server" Text="Tipo de Captura"></asp:Label></td>
                                 <td align="left" style="width: 120px">
                                     <asp:RadioButtonList ID="rdLista1" runat="server" RepeatDirection="Horizontal"
                                     AutoPostBack="True" OnSelectedIndexChanged="rdLista1_SelectedIndexChanged" >
                                                        <asp:ListItem Text="Inicio" Value="1" ></asp:ListItem>
                                                        <asp:ListItem Text="Fin" Value="2" Selected="True"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                 </td>
                             </tr>
                            
                        </table>   
               <%-- </ContentTemplate>
                        </asp:UpdatePanel> --%>
                         
                        <br />
                        
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="67px" TabIndex="4" OnClick="btnBuscar_Click"   />
                        
                        <br /> 
                         <img style="cursor: pointer;" id="imgExcel" alt="Exportar a Excel" title="Exportar a Excel" onclick="__doPostBack('ctl00$cphMainMaster$btnExportar','')"
                                                        src="../../App_Themes/SEP2010/Imagenes/Fondo/toExcel.JPG" />
                        <%--<asp:ImageButton ID="btnExportar2" Enabled="false" runat="server"  OnClick="btnExportar_Click" ImageUrl="~/App_Themes/SEP2010/Imagenes/Fondo/toExcel.JPG"  />--%>
                        
                 </ContentTemplate>
                        </asp:UpdatePanel> 
                     <asp:Button ID="btnExportar" style="display:none;" runat="server" Text=""  OnClick="btnExportar_Click"    />
                    
                
                        <br />
                        </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
                <div>
          
            <br />
        <asp:UpdatePanel ID="upMsg" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <%--<asp:AsyncPostBackTrigger ControlID="btnExportar" EventName="Click" />--%>
                    <asp:AsyncPostBackTrigger ControlID="ddlNivel" EventName="SelectedIndexChanged" />
                </Triggers>
                <ContentTemplate> 
                    <asp:Label ID="lblMsg" runat="server"  ></asp:Label>
              </ContentTemplate>
            </asp:UpdatePanel> 
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">

            <ProgressTemplate>

            <asp:Image ID="Image1" runat="server" ImageUrl="../../tema/images/loading.gif" />
            <!-- this is an animated progress gif that will be shown while progress delay -->

            </ProgressTemplate>
            </asp:UpdateProgress>
               
            </div>
            </center>
        </div>
        </center>
                    
        <br />
 
                <center>
                <div id = "up_container" style="text-align: center">
                   <center>
                   <asp:UpdatePanel ID="update" runat="server" >  
                     <ContentTemplate>   
                     
                   <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" 
                        PageSize="50" CellPadding="4" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="RowCommand"
                         EmptyDataText="Busqueda sin registros" OnRowCreated="GridView1_RowCreated">
                        <PagerSettings PageButtonCount="20" />
                      <Columns>
                      
                          <asp:TemplateField HeaderText="DES">
                           <ItemTemplate>
                           <%--<%# (bool)(Eval("ESTATUS").ToString()=="NO OFICIALIZADO")%>--%>
                           <asp:ImageButton CommandArgument='<%# Container.DataItemIndex%>' ID="imgON" runat="server" ImageUrl="~/tema/images/setting.gif"  Visible='<%# (bool)(Eval("ESTATUS").ToString()!="NO OFICIALIZADO")%>' CommandName="des" style=" width:28px; height:28px; border:0px;" AlternateText="Desoficializar" /> 
                           <asp:Image runat="server" ID="imgOff" ImageUrl="~/tema/images/settingoff.gif" Visible='<%# (bool)(Eval("ESTATUS").ToString()=="NO OFICIALIZADO")%>'  style=" width:28px; height:28px; border:0px;" AlternateText="No oficializado"/>
                           </ItemTemplate>
                           
                           </asp:TemplateField>
                          
<%--                         <asp:ButtonField CommandName="des" HeaderText="DES" Text="&lt;img src='../../tema/images/setting.gif' alt='Desoficializar' width=28 height=28 style='border:0;'&gt;"/>
--%>
                        
                                              
                            <asp:BoundField DataField="ID_CONTROL" HeaderText="ID_CONTROL" SortExpression="ID_CONTROL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_CENTROTRABAJO" HeaderText="ID_CENTROTRABAJO" SortExpression="ID_CENTROTRABAJO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_INMUEBLE" HeaderText="INMUEBLE" SortExpression="ID_INMUEBLE">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ID_CCTNT"  SortExpression="ID_CCTNT" >
                                <ItemTemplate>
                                    <asp:Label ID="lblID_CCTNT" runat="server" Text='<%# Bind("ID_CCTNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="CCT" HeaderText="CCT"   SortExpression="CCT">
                                <ItemStyle HorizontalAlign="Left" Font-Bold="true" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_TURNO" HeaderText="TURNO" SortExpression="ID_TURNO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TELEFONO" HeaderText="TELEFONO" SortExpression="TELEFONO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="REGION" HeaderText="REGION" SortExpression="REGION">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_ZONA" HeaderText="ID_ZONA" SortExpression="ID_ZONA">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ZONA" HeaderText="ZONA" SortExpression="ZONA">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_NIVEL" HeaderText="ID_NIVEL" SortExpression="ID_NIVEL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NIVEL" HeaderText="NIVEL" SortExpression="NIVEL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_SUBNIVEL" HeaderText="ID_SUBNIVEL" SortExpression="ID_SUBNIVEL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SUBNIVEL" HeaderText="SUBNIVEL" SortExpression="SUBNIVEL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_SOSTENIMIENTO" HeaderText="ID_SOSTENIMIENTO" SortExpression="ID_SOSTENIMIENTO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SOSTENIMIENTO" Visible="false" HeaderText="SOSTENIMIENTO" SortExpression="SOSTENIMIENTO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                           <%-- <asp:TemplateField HeaderText="ESTATUS"  SortExpression="ESTATUS" >
                                <ItemTemplate>
                                    <asp:Label ID="lblESTATUSGRID" runat="server" Text='<%# Bind("ESTATUS") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>--%>
                            <asp:BoundField DataField="ESTATUS" HeaderText="ESTATUS" SortExpression="ESTATUS">
                                <ItemStyle HorizontalAlign="Left" Font-Bold="true" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                   </ContentTemplate>  
                <Triggers>  
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />  
                   <%-- <asp:AsyncPostBackTrigger ControlID="btnExportar" EventName="Click" />  --%>
                    <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />

                </Triggers>  
              </asp:UpdatePanel>  
              
                   </center>
                </div>
        <cc1:UpdatePanelAnimationExtender ID="upae" BehaviorID="animation" runat="server" TargetControlID="update">  
                     <Animations>  
         <OnUpdating>  
             <Sequence>  
                 
                 <ScriptAction Script="var b = $find('animation'); b._originalHeight = b._element.offsetHeight;" />  
                   
                
                 <Parallel duration="0">  
                     <EnableAction AnimationTarget="btnBuscar" Enabled="false" />  
                     <%--<EnableAction AnimationTarget="btnExportar" Enabled="false" />  --%>
                     
                   
                     
                    
                 </Parallel>  
                 <StyleAction Attribute="overflow" Value="hidden" />  
                 <Parallel duration=".25" Fps="30">  
                 </Parallel>  
             </Sequence>  
         </OnUpdating>  
         <OnUpdated>  
             <Sequence>  
                
                 <Parallel duration=".25" Fps="30">  
                 </Parallel>  
                               
                 <Parallel duration="0">  
                     <EnableAction AnimationTarget="btnBuscar" Enabled="true" />
                    <%-- <EnableAction AnimationTarget="btnExportar" Enabled="true" />--%>
                 </Parallel>                              
             </Sequence>  
         </OnUpdated>  
     </Animations>  
</cc1:UpdatePanelAnimationExtender>
                </center>
         
      
                
                
                
                
                
                
                
                
         
 
    <script type="text/javascript">
        function enter(e) 
        {
            if (_enter) 
            {
                if (event.keyCode==13)
                {
                    event.keyCode=9; 
                    return event.keyCode;
                }    
            }
        }
        function inicio()
        {
            document.getElementById("txtClaveCT").focus();
        }    
        function Num(evt) {
            var nav4 = window.Event ? true : false;
	        var key = nav4 ? evt.which : evt.keyCode;
            return (key <= 13 || (key >= 48 && key <= 57));
        }
    
        var _enter=true;
        function Vent911(niv,subniv,cnt,cic)
        {
            var v911 = false;
            try{
                var Pagina="";
                if(niv==11 && subniv==1){	//	911_2	Preescolar General
                    Pagina="911_2/Identificacion_911_2.aspx";
                }
                if(niv==11 && subniv==3){	//	ECC_21	Preescolar CONAFE
                    Pagina="911_ECC_21/Identificacion_911_ECC_21.aspx";
                }
                if(niv==12 && subniv==4){	//	911_4	Primaria General
                    Pagina="911_4/Identificacion_911_4.aspx";
                }
                if(niv==12 && subniv==6){	//	ECC_22	Primaria CONAFE
                    Pagina="911_ECC_22/Identificacion_911_ECC_22.aspx";
                }
                if(niv==13 && subniv==7){	//	911_6	Secundaria
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==13 && subniv==8){	//	911_6	Secundaria 2
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==13 && subniv==9){	//	911_6	Secundaria 3
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==13 && subniv==10){	//	911_6	Telesecundaria
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==13 && subniv==11){	//	911_6	Secundaria 5
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==22 && subniv==22){	//	911_8G	Bachillerato General
                    Pagina="911_8G/Identificacion_911_8G.aspx";
                }
                if(niv==22 && subniv==23){	//	911_8T	Bachillerato Tecnologico
                    Pagina="911_8T/Identificacion_911_8T.aspx";
                }
                if(niv==21 && subniv==0){	//	911_8P	Profesional Técnico
                    Pagina="911_8P/Identificacion_911_8P.aspx";
                }
                if(niv==51 && subniv==50){	//	CAM_2	CAM Educacion Especial
                    Pagina="911_CAM_2/Identificacion_911_CAM_2.aspx";
                }
                if(niv==51 && subniv==51){	//	USAER_2	USAER Unidad de Servicios de Apoyo a la Educación Regular
                    Pagina="911_USAER_2/Identificacion_911_USAER_2.aspx";
                }
                if(niv==42 && subniv==42){	//	EI_NE2	Inicial NO Escolarizada
                    Pagina="911_EI_NE_2/Identificacion_911_EI_NE_2.aspx";
                }
                if(niv==41 && subniv==40){	//	EI_2	Inicial Escolarizada
                    Pagina="911_EI_2/Identificacion_911_EI_2.aspx";
                }
                if(niv==61 && subniv==63){	//	911_6C	Formación para el Trabajo
                    Pagina="911_6C/Identificacion_911_6C.aspx";
                }
                Pagina=Pagina + "?cnt=" + cnt + "&cic=" + cic;
                window.open(Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
             
            }
            catch(err)
            {
                alert(err);
            } 
           
        }
        
    </script>
</asp:Content>
