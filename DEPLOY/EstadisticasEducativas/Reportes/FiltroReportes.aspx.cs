using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Mx.Gob.Nl.Educacion;
using SEroot.WsEstadisticasEducativas;
using EstadisticasEducativas._911;


namespace EstadisticasEducativas.Reportes
{
    public partial class FiltroReportes : System.Web.UI.Page
    {
        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadiscticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //txtRegion.Attributes["onkeypress"] = "return Num(event)";
                //txtZona.Attributes["onkeypress"] = "return Num(event)";
                btnReporte.Attributes["onblur"] = "javascript:_enter=true;";
                btnReporte.Attributes["onfocus"] = "javascript:_enter=false;";
                rdLista1.SelectedValue = "2";
                Entidad();
                Nivel();
                Filtros();
                Ciclos();
            }
        }

        UsuarioSeDP usr = null;
        protected void getUsr()
        {
            if (usr == null)
                usr = SeguridadSE.GetUsuario(HttpContext.Current);
        }

        private void Filtros()
        {
            UsuarioSeDP nuevoUser = SeguridadSE.GetUsuario(HttpContext.Current);

            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            int region = int.Parse(usr[2]);
            int ID_Entidad = nuevoUser.EntidadDP.EntidadId;
            int zona = int.Parse(usr[3]);
            string[] centroTrabajo = usr[5].Split(',');
            SEroot.WsCentrosDeTrabajo.ZonaDP zonaDP = ws.Load_Zona(zona);
            string ID_NivelEducacion = usr[7];

            switch (ID_NivelTrabajo)
            {
                case 0://FEDERAL
                    break;
                case 1://SE ESTATAL
                    //Bloquear el estado y mostrar solamente el estado al cual pertenece
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);

                    this.ddlEntidad.Enabled = false;

                    break;
                case 2://Region
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;
                    break;
                case 3://ZONA
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;


                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;

                    //this.txtZona.Text = zona.ToString();
                    //this.txtZona.Enabled = false;

                    break;
                case 5://SEPA
                case 4://CCT
                    //Entidad
                    this.ddlEntidad.SelectedValue = ID_Entidad.ToString();
                    setRegion(ID_Entidad);
                    this.ddlEntidad.Enabled = false;

                    //Region
                    this.ddlRegion.SelectedValue = region.ToString();
                    this.ddlRegion.Enabled = false;
                    //Zona
                    //this.txtZona.Text = zona.ToString();
                    //this.txtZona.Enabled = false;


                    //Es director de mas de una escuela
                    if (centroTrabajo.Length > 1)
                    {
                        //txtZona.Text = "";
                        //txtZona.Enabled = false;

                        //txtClaveCT.Visible = false; txtClaveCT.Text = "";
                        //ddlCentroTrabajo.Visible = true;
                        //for (int i = 0; i < centroTrabajo.Length; i++)
                        //{
                        //    SEroot.WsCentrosDeTrabajo.CentroTrabajoDP ctDP = ws.Load_CT(int.Parse(centroTrabajo[i]));

                        //    ddlCentroTrabajo.Items.Add(new ListItem(ctDP.clave, ctDP.clave));
                        //}
                        //ddlCentroTrabajo.SelectedIndex = 0;
                    }
                    //Es director de SOLO una escuela                        
                    else
                    {
                       
                        if (centroTrabajo.Length == 1)
                        {
                            int ID_Ciclo = int.Parse(nuevoUser.Selecciones.CicloEscolarSeleccionado.CicloescolarId.ToString()); //5; //int.Parse(ddlCiclo.SelectedValue);
                            int ID_Usuario = int.Parse(usr[0]);


                            string lbl = nuevoUser.Selecciones.CentroTrabajoSeleccionado.CctntId.ToString();


                            SeguridadSE.SetCctNt(this.Page, int.Parse(lbl));
                            int id_cctnt = int.Parse(lbl);
                            ControlDP controlDP = wsEstadiscticas.ObtenerDatosEncuestaID_CCTNT(id_cctnt, Class911.ID_Fin, ID_Ciclo, ID_Usuario);
                            Class911.SetControlSeleccionado(HttpContext.Current, controlDP);
                            string path = Ruta(controlDP.ID_Cuestionario);
                            Response.Write("<script language='javascript'> window.open('" + path + "', 'window','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668');</script>");
                        }
                    }
                    //txtClaveCT.Enabled = false;
                    break;
            }
        }

        private string Ruta(int id_cuestionario)
        {
            string abrirVentana = "";
            switch (id_cuestionario)
            {
                case 1://	2	11	1	911_2	Preescolar General
                    abrirVentana = "911_2/Identificacion_911_2.aspx";
                    break;
                case 7://	2	11	3	ECC_21	Preescolar CONAFE
                    abrirVentana = "911_ECC_21/Identificacion_911_ECC_21.aspx";
                    break;
                case 2://	2	12	4	911_4	Primaria General
                    abrirVentana = "911_4/Identificacion_911_4.aspx";
                    break;
                case 8://	2	12	6	ECC_22	Primaria CONAFE
                    abrirVentana = "911_ECC_22/Identificacion_911_ECC_22.aspx";
                    break;
                case 3://	2	13	7	911_6	Secundaria
                    abrirVentana = "911_6/Identificacion_911_6.aspx";
                    break;
                case 5://	2	22	22	911_8G	Bachillerato General
                    abrirVentana = "911_8G/Identificacion_911_8G.aspx";
                    break;
                case 6://	2	22	23	911_8T	Bachillerato Tecnologico
                    abrirVentana = "911_8T/Identificacion_911_8T.aspx";
                    break;
                case 4://	2	21	0	911_8P	Profesional T�cnico
                    abrirVentana = "911_8P/Identificacion_911_8P.aspx";
                    break;
                case 9://	2	51	50	CAM_2	CAM Educacion Especial
                    abrirVentana = "911_CAM_2/Identificacion_911_CAM_2.aspx";
                    break;
                case 11://	2	51	51	USAER_2	USAER Unidad de Servicios de Apoyo a la Educaci�n Regular
                    abrirVentana = "911_USAER_2/Identificacion_911_USAER_2.aspx";
                    break;
                case 12://	2	42	42	EI_NE2	Inicial NO Escolarizada
                    abrirVentana = "911_EI_NE_2/Identificacion_911_EI_NE_2.aspx";
                    break;
                case 13://	2	41	40	EI_2	Inicial Escolarizada
                    abrirVentana = "911_EI_2/Identificacion_911_EI_2.aspx";
                    break;
                case 14://	2	61	63	911_6C	Formaci�n para el Trabajo
                    abrirVentana = "911_6C/Identificacion_911_6C.aspx";
                    break;
                case 30://	2	11	2	911_121	Preescolar Ind�gena   
                    abrirVentana = "911_121/Identificacion_911_121.aspx";
                    break;
                case 31://	2	12	5	911_122	Primaria Ind�gena
                    abrirVentana = "911_122/Identificacion_911_122.aspx";
                    break;
                case 33://	2	61	63	911_6C	Formaci�n para el Trabajo
                    abrirVentana = "911_8N/Identificacion_911_8N.aspx";
                    break;
            }
            return abrirVentana;
        }
        private void Entidad()
        {
            SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
            SEroot.WsEstadisticasEducativas.CatListaEntidad911DP[] listaEntidades = wsEstadisticas.Lista_CatListaEntidad911(223);//EL ID PAIS = 223 es M�XICO
            if (listaEntidades != null)
            {
                ddlEntidad.Items.Add(new ListItem("NACIONAL", "-1"));
            }
            for (int i = 0; i < listaEntidades.Length; i++)
            {
                string valor = listaEntidades[i].ID_Entidad.ToString();
                string texto = listaEntidades[i].Nombre;
                ddlEntidad.Items.Add(new ListItem(texto, valor));

            }

        }
        private void setNivel(int idEntidad)
        {
            if (idEntidad != -1)
            {
                ddlNivel.Items.Clear();
                ddlNivel.Items.Add(new ListItem("Todos los Cuestionarios", "-1"));
                ddlNivel.Enabled = false;
            }
            else
            {
                Nivel();
            }
        }
        private void Nivel()
        {
            string[] usr = User.Identity.Name.Split('|');
            int ID_NivelTrabajo = int.Parse(usr[1]);
            int tipoCuestionario = int.Parse(rdLista1.SelectedValue);

            ddlNivel.Enabled = true;
            ServiceEstadisticas ws = null;

            //Si no es usuario de CCT se llena con todos los niveles
            if (ID_NivelTrabajo < 4)
            {
                ws = new ServiceEstadisticas();
                CatListaNiveles911DP[] listaNiveles = ws.Lista_CatListaNiveles911(tipoCuestionario);
                ddlNivel.DataSource = listaNiveles;
                ddlNivel.DataTextField = "Descrip";
                ddlNivel.DataValueField = "ID_Cuestionario";
                ddlNivel.DataBind();
                ddlNivel.Items.Insert(0, new ListItem("- Todos -", "-1"));
                ddlNivel.Enabled = true;
            }
            else
            {
                ddlNivel.Enabled = false;
                ddlNivel.Items.Add(new ListItem("- No Disponible -", "0"));
            }
        }

        private void Ciclos()
        {
            SEroot.WSEscolar.WsEscolar wsEscolar = new SEroot.WSEscolar.WsEscolar();
            SEroot.WSEscolar.DsCiclosEscolares dsCiclos = wsEscolar.ListaCicloEscolarCombo(1);//Anual

            ddlCiclo.DataSource = dsCiclos.CicloEscolar;
            ddlCiclo.DataValueField = "Id_CicloEscolar";
            ddlCiclo.DataTextField = "Nombre";
            ddlCiclo.DataBind();

            getUsr();

            ListItem itemActual = ddlCiclo.Items.FindByValue(usr.Selecciones.CicloEscolarSeleccionado.CicloescolarId.ToString());
            if (itemActual != null)
            {
                itemActual.Selected = true;
            }

        }

        public void setRegion(int ID_Entidad)
        {
            SEroot.WsCentrosDeTrabajo.DsCctRegion Ds = new SEroot.WsCentrosDeTrabajo.DsCctRegion();

            if (ID_Entidad > 0)//Solo cuando es un estado consulta
            {
                Ds = ws.ListaRegiones(223, ID_Entidad);
                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddlRegion.Items.Clear();
                    this.ddlRegion.Items.Add(new ListItem("Todas las regiones", "-1"));
                    this.ddlRegion.AppendDataBoundItems = true;
                    this.ddlRegion.DataSource = Ds;
                    this.ddlRegion.DataMember = "Region";
                    this.ddlRegion.DataTextField = "Nombre";
                    this.ddlRegion.DataValueField = "ID_Region";
                    //this.ddlRegion.SelectedIndex = 1;
                    this.ddlRegion.DataBind();

                    this.ddlRegion.Enabled = true;//Una vez que tiene contenido, desplegar
                }
            }
            else// de lo Contrario limpia el Dropdownlist
            {
                this.ddlRegion.Items.Clear();
                this.ddlRegion.Items.Add(new ListItem("Seleccione una Regi�n", "-1"));
                this.ddlRegion.Enabled = false;
            }



        }
        protected void ddlEntidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ID_Entidad = int.Parse(ddlEntidad.SelectedValue);
            setRegion(ID_Entidad);
            setNivel(ID_Entidad);
        }

        protected void ddlNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMsg.Text = "";
        }

        protected void btnReporte_Click(object sender, EventArgs e)
        {
            string idCicloEscolar = ddlCiclo.SelectedValue;
            string idTipoCuestionario = rdLista1.SelectedValue;
            
            //string idNivel = ddlNivel.SelectedValue.Split('_')[0];
            //string idSubNivel = ddlNivel.SelectedValue.Split('_')[1];

            string idEntidad = ddlEntidad.SelectedValue;
            string idRegion = ddlRegion.SelectedValue;
            string idCuestionario = ddlNivel.SelectedValue;
            //string idZona = txtZona.Text == "" ? "-1" : txtZona.Text;


            string url = "RptAvanceCapturado.aspx?ciclo=" + idCicloEscolar + "&tipo=" + idTipoCuestionario;
            //url += (idNivel == "-1" ? "" : ("&niv=" + idNivel));
            //url += (idSubNivel == "-1" ? "" : ("&sub=" + idSubNivel));
            url += (idEntidad == "-1" ? "" : ("&ent=" + idEntidad));
            url += (idRegion == "-1" ? "" : ("&region=" + idRegion));
            url += (idCuestionario == "-1" ? "" : ("&c=" + idCuestionario));
            //url += (idZona == "-1" ? "" : ("&zon=" + idZona));
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Carga Reporte", cinetkit.Scriptor.JSNuevaVentana(url, 640, 480), false);
        }

        protected void rdLista1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Nivel();
        }
    }
}
