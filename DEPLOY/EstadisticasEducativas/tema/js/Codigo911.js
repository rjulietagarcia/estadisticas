    function  denyPage(){ 
          alert("Es necesario capturar sin errores la informaci"+'\u00f3'+"n de la p"+'\u00e1'+"gina anterior\npara poder avanzar a la siguiente.");
    }
    var ir1;
    function  openPage(page,ir){ 
          ir1=ir;
          CambiarPagina = page;  
          EnviarDatosServer("Ejecutar");
    }
    function ofiMotivado(page)
    {
        window.location.href=page+".aspx?motivado=true";
    }
    function motivadoOficializar()
    {
        var path = window.location.href;
        var param = path.match('true');
        if(param=='true')
        {
            document.getElementById("lista1").style.display='none';
            document.getElementById("lista2").style.display='inline';
        }
    }

    function ReceiveServerData_Variables(rValue){
        _gRValue = rValue;
        window.setTimeout('__ReceiveServerData_Variables(_gRValue)',0);
    }
    function LimpiarFallas(){
       var lista = document.getElementById(hidListaTxtBoxs).value;
       var cajas = lista.split('|');
       var i = 0;
       for (i = 0; i < cajas.length-1; i ++)
       {
            var txt = cajas[i].split(',');
            var obj = document.getElementById(txt[0]);
            obj.value=obj.value.toUpperCase();
            if(isNaN(obj.value))
            {
                obj.className='txtVarCorrecto txtleft';
            }
            else
            {
                obj.className='txtVarCorrecto';
            }
       }
    }
     function __ReceiveServerData_Variables(rValue)
     {
        if (rValue != null)
        {
            document.getElementById("divWait").style.visibility = "hidden";
            var rows = rValue.split('|');
            var vi = 1;
            if (rValue != "")  
                LimpiarFallas();
            var detalles="<ul>";
            for(vi = 1; vi < rows.length; vi ++)
            {
                var res = rows[vi];
                var colums = res.split('!');
                                             //Si se quiere mostrar las fallas en otra parte txtVFALLAS
                                             //Cambio a mayusculas porque en los anexos no pinta fallas
                var caja = document.getElementById("ctl00_cphMainMaster_txtV" + colums[0].toUpperCase());
                if (caja == null)
                {
                   caja = document.getElementById("ctl00_cphMainMaster_txtVANX" + colums[0].toUpperCase());
                }
                if (caja!=null)
                {
                    if(isNaN(caja.value))
                    {
                        caja.className='txtVarFallas txtleft';
                    }
                    else
                    {
                        caja.className='txtVarFallas';
                    }
                    caja.title = colums[1];
                    detalles = detalles + "<li><span style=\"cursor:pointer;\" onclick = \"javascript:document.getElementById('" + caja.id + "').focus();\">" + colums[1]+ "</span></li>";     
                }
                else
                {
                    detalles = detalles + "<li>" + colums[1]+ "</li>";
                }
           }
          
           if (CambiarPagina != "") 
           {  // para que no se borre la informacion a menos que se mueva de la pagina
                var obj = document.getElementById("divResultado");
                obj.innerHTML =  detalles + "</ul>";
           }
           var Cambiar = "No";
           if ( rValue != "" && CambiarPagina != "" )
           {
                if(ir1)
                {
                    Cambiar = "Si";
                }
                else
                {
                    alert("Se encontraron errores de captura\nDebe corregirlos para avanzar a la siguiente p"+'\u00e1'+"gina.");
                    Cambiar = "No";
                }
                    
                   
//              if(confirm("Se encontraron errores de captura\n  Desea cambiar de p"+'\u00e1'+"gina sin corregirlos?")){
//                    Cambiar = "Si";
//              }
           }
           else if(rValue == "" && CambiarPagina != ""){
              Cambiar = "Si";
           }
           if (Cambiar == "Si"){
                window.location.href=CambiarPagina+".aspx";
           }
       }
    }
    function EnviarDatosServer(EjecutarValidacion){
              try
              {
                 if (document.getElementById("divWait").style.visibility == "visible"){
                     alert("Se ejecuto mientras trabajaba, se omitir"+'\u00e1'+" operaci"+'\u00f3'+"n. Deber"+'\u00e1'+" de volver a realizar su ultimo movimiento.");
                 }
                 else{
                     //if (EjecutarValidacion=="Ejecutar") 
                     document.getElementById("divWait").style.visibility = "visible";
                     var lista = document.getElementById(hidListaTxtBoxs).value;
                    
                     var rows = lista.split('|');
                     var Salida = "NoVariable=Valor=ReadOnly|"; 
                     var i;
                     var valor = 0;
                     var NoVariable; 
                        
                     for( i = 0; i < rows.length-1; i++ )
                     {
                      
                        var Datos = rows[i].split(',');
                        
                        var caja =  document.getElementById(Datos[0])
                          
                        if (caja != null){
                                valor = caja.value;
                                //if (Datos[0] == 'ctl00_cphMainMaster_txtVANX659') {valor ="'" + valor + "'";}
                                //if (trim(valor) == ""){valor = 0;}
                                NoVariable = Datos[1];
                                
                                if (caja.readOnly){
                                    ReadOnly = "false";}
                                else
                                    ReadOnly = "true";
                                
                                Salida = Salida + NoVariable + "=" + valor + "=" + ReadOnly + "|";
                        }
                    }
                    //EjecutarValidacion = "NoEjecutar/Ejecutar"
                 
                    Salida = Salida + "!" + EjecutarValidacion + "!" + document.getElementById("ctl00_cphMainMaster_hidIdCtrl").value;
                    CallServer_Variables(Salida,"");
                 }
              }
            catch(err)
              {
                  alert("fallas "  + err);
              }
    }
    var FirstTime = true;
    function Disparador(segundos){
        CambiarPagina = "";
        if (!FirstTime){
             
             EnviarDatosServer("NoEjecutar");
         }
        else{
               EstilosInicio();
               document.getElementById("divWait").style.visibility = "hidden";
        }
        //alert(FirstTime);
        FirstTime = false;
        var segs = eval(segundos * 1000);
        window.setTimeout("Disparador("+segundos+")",segs);
    }
   function EstilosInicio(){
    if(document.getElementById(hidListaTxtBoxs)!=null)
    {
       var lista = document.getElementById(hidListaTxtBoxs).value;
       var cajas = lista.split('|');
       var i = 0;
       for (i = 0; i < cajas.length-1; i ++)
       {
            var txt = cajas[i].split(',');
            var obj = document.getElementById(txt[0]);
            
            obj.title = '';
            if (obj.readOnly||obj.className=='txtVarSW'||obj.className=='txtVarSW txtleft')
            {
                 
                 if(isNaN(obj.value))
                 {
                    obj.className='txtVarSW txtleft';
                 }
                 else
                 {
                    obj.className='txtVarSW';
                 }
                
                 obj.disabled=true;
            }
            else
            {
                if(isNaN(obj.value))
                {
                    obj.className='txtVarCorrecto txtleft';
                }
                else
                {
                    obj.className='txtVarCorrecto txtright';
                }
            }
       }
    }
       //
       if(document.getElementById('ctl00_cphMainMaster_pnlOficializado')!=null)
       {
            x=document.getElementsByTagName("input");
            for (i=0;i<x.length;i++)
            {
                
                if(isNaN(x[i].value))
                 {
                    x[i].className='txtVarSW txtleft';
                 }
                 else
                 {
                    x[i].className='txtVarSW';
                 }
                
                 x[i].disabled=true;
            }
            y=document.getElementsByTagName("select");
            for (i=0;i<y.length;i++)
            {
                
                if(isNaN(y[i].value))
                 {
                    y[i].className='txtVarSW txtleft';
                 }
                 else
                 {
                    y[i].className='txtVarSW';
                 }
                
                 y[i].disabled=true;
            }
       }
    }           
                
    function trim(stringToTrim) {
        return stringToTrim.replace(/^\s+|\s+$/g,"");
    }
    function ltrim(stringToTrim) {
        return stringToTrim.replace(/^\s+/,"");
    }
    function rtrim(stringToTrim) {
        return stringToTrim.replace(/\s+$/,"");
    }
   function ErrorServidor(oMensaje, oContexto) {alert(oMensaje);}

   function enter(e)
   {
       if (_enter)
       {
//           if (event.keyCode==13)
//           {
//               event.keyCode=9; 
//               return event.keyCode;
//           }    
       }
   }
   
   function Num(evt){
       return  keyRestrict(evt,'1234567890');
    }
    
    function keyRestrict(e, validchars) {
         var key='', keychar='';
         key = (document.all) ? e.keyCode : e.which;
         if (key == null) return true;
         keychar = String.fromCharCode(key);
         keychar = keychar.toLowerCase();
         validchars = validchars.toLowerCase();
         if (validchars.indexOf(keychar) != -1)
          return true;
         if (  key==8 || key==127 || key==0)
          return true;
         return false;
    } 
    function keyRestrictAndEnter(e, validchars,boton) {
         var key='', keychar='';
         key = e.keyCode;
         
         if (key == null) return true;
         keychar = String.fromCharCode(key);
         keychar = keychar.toLowerCase();
         validchars = validchars.toLowerCase();
         if (validchars.indexOf(keychar) != -1)
          return true;
         //if ( key==null || key==0 || key==8 || key==9 || key==27 )
         // return true;
         if (key==13) __doPostBack(boton,'');
         return false;
    }
    
    function setAreasEstudio(idSelect,idTxt1,idTxt2)
    {
        var select = document.getElementById(idSelect);
        var txt1 = document.getElementById(idTxt1);
        var txt2 = document.getElementById(idTxt2);
        
      
        
            var i=select.selectedIndex;
            var tx=select.options[i].text;
            if(tx!='Otros')
            {
                txt1.value=tx.substring(0,1);
                txt2.value=tx.substring(3);
//                txt1.disabled = true;
//                txt2.disabled = true;
            }
            else
            {
                txt1.value='';
                txt2.value='';
                txt1.disabled = false;
                txt2.disabled = false;
                
            }
       
    }
    //pimer textbox el de el texto, segundo textbox el de el valor
    function OnChangeDropXid(dropdown,idtxt,idtxtnro)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    var SelText = dropdown.options[myindex].text;
                    if(myindex == 0)
                    {
                        document.getElementById(idtxt).value  = '_';
                        if(idtxtnro!=null)
                            document.getElementById(idtxtnro).value  = '0';
                    }
                    else
                    {
                        document.getElementById(idtxt).value  = SelText;
                        if(idtxtnro!=null)
                            document.getElementById(idtxtnro).value  = SelValue;
                    }
                    
                  return true;
                }
                
                
//funcion para anexo inicio
function hideshow(op)
                {
                   var number = 0;
                   switch(op)
                   {
                        
                        case "show1" :
                        
                        x=document.getElementById('tcompu').getElementsByTagName("input");

                        for (i=0;i<x.length;i++)
                        {
                            x[i].style.display = '';
                        }
                        document.getElementById('tcompu').style.display = ''; 
                        break;
                        case "hide1" :
                        
                        x=document.getElementById('tcompu').getElementsByTagName("input");

                        for (i=0;i<x.length;i++)
                        {
                            x[i].style.display = 'none';
                        } 
                        document.getElementById('tcompu').style.display = 'none';
                        number = 92;
                        for(i=0; i < 13;i++)
                        {
                            if(number != 97 && number != 103)
                            {
                                document.getElementById('ctl00_cphMainMaster_optVANX'+number).checked = false;
                            }
                            number++;
                        }
                        document.getElementById('ctl00_cphMainMaster_optVANX114').checked = true;
                        document.getElementById('ctl00_cphMainMaster_optVANX34').checked = true;
                        document.getElementById('ctl00_cphMainMaster_optVANX37').checked = true;
                        document.getElementById('ctl00_cphMainMaster_optVANX40').checked = true;
                        break;
                         
                        case "show2" :
                        
                        x=document.getElementById('trecursos').getElementsByTagName("input");

                        for (i=0;i<x.length;i++)
                        {
                            x[i].style.display = '';
                        }
                        document.getElementById('trecursos').style.display = ''; 
                        break;
                        case "hide2" :
                        
                        x=document.getElementById('trecursos').getElementsByTagName("input");

                        for (i=0;i<x.length;i++)
                        {
                            x[i].style.display = 'none';
                        } 
                        document.getElementById('trecursos').style.display = 'none';
                        number = 86;
                        for(i=0; i < 4;i++)
                        {
                            document.getElementById('ctl00_cphMainMaster_optVANX'+number).checked = false;
                            number++;
                        }
                        document.getElementById('ctl00_cphMainMaster_optVANX73').checked = true;
                        document.getElementById('ctl00_cphMainMaster_optVANX83').checked = true;
                        break;
                        case "noRed" :
                            document.getElementById('ctl00_cphMainMaster_optVANX41').checked = false;
                            document.getElementById('ctl00_cphMainMaster_optVANX43').checked = false;
                            document.getElementById('ctl00_cphMainMaster_optVANX44').checked = false;
                            return false;
                        break;
                        case "opcionRed" :
                            document.getElementById('ctl00_cphMainMaster_optVANX39').checked = true;
                        break;
                   }
                  
                }
                
                
                