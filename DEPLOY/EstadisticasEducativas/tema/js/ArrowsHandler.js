        
        //Define un arreglo de 3 dimensiones
        function MultiDimensionalArray(iTables,iRows,iCols) 
        { 
        var i; 
        var j;
        var k; 
           var a = new Array(iTables); 
           for (i=0; i < iTables; i++) 
           { 
               a[i] = new Array(iRows); 
               for (j=0; j < iRows; j++) 
               { 
                   a[i][j] = new Array(iCols);
                   for (k=0;k< iCols;k++)
                     {a[i][j][k] = "";
                     }
               } 
           } 
           return(a); 
        } 


        //Obtiene todas las cajas de texto de la p�gina e identifica las que tengan dentro
        //del ID el texto "txtV" y guarda ese ID en un arreglo multidimensional de acuerdo
        //a su Tab Index
        function GetTabIndexes()
            {
            var Texts = document.getElementsByTagName('input');
            var tabla;
            var columna;
            var fila;
            for (i=0;i<Texts.length;i++)
            {
                if((Texts[i].id.indexOf("txtV") >= 0 || Texts[i].id.indexOf("optV") >= 0) && Texts[i].tabIndex!=0)
                {tabla = (Texts[i].tabIndex/10000)|0;
                 fila = ((Texts[i].tabIndex%10000)/100)|0;
                 columna = Texts[i].tabIndex%100;
                 if(columna > 90)
                    {TabIndexesArray[tabla-1][fila-1][(columna-90)*4-4] = Texts[i].id;
                     TabIndexesArray[tabla-1][fila-1][(columna-90)*4-3] = Texts[i].id;
                     TabIndexesArray[tabla-1][fila-1][(columna-90)*4-2] = Texts[i].id;
                     TabIndexesArray[tabla-1][fila-1][(columna-90)*4-1] = Texts[i].id;
                    }
                 if(columna > 60)
                    {TabIndexesArray[tabla-1][fila-1][(columna-60)*3-3] = Texts[i].id;
                     TabIndexesArray[tabla-1][fila-1][(columna-60)*3-2] = Texts[i].id;
                     TabIndexesArray[tabla-1][fila-1][(columna-60)*3-1] = Texts[i].id;
                    }
                 else if(columna > 30)
                    {TabIndexesArray[tabla-1][fila-1][(columna-30)*2-2] = Texts[i].id;
                     TabIndexesArray[tabla-1][fila-1][(columna-30)*2-1] = Texts[i].id;
                    } 
                 else 
                    {TabIndexesArray[tabla-1][fila-1][columna-1] = Texts[i].id;
                    }
                    TabIndexesArray[tabla-1][fila-1][MaxCol-1] = 1;
                 }
                 //alert(Texts[i].id + '-' + Texts[i].tabIndex + '-' + tabla + '-' + fila + '-' + columna + '-' + TabIndexesArray[tabla][fila][columna]);
                  
                 
                 
                
            }
            
            SigCaja=document.getElementById(TabIndexesArray[0][0][0]);
                if(SigCaja!=null&&SigCaja.disabled==false)
                {
                    SigCaja.focus();SigCaja.select();return false;
                }
            }
       
       
       //Encuentra la �ltima fila no vacia de una tabla
       function FindLastRow(tabla)
       {var row=MaxRow-1;
        while(TabIndexesArray[tabla][row][MaxCol-1]=="")
        {row--;
        }
        return row;
       }
       
       
       //Encuentra la �ltima columna no vacia de la fila indicada de una tabla
       //Si la fila est� vacia se recorre a la fila anterior
       function FindLastColumn(tabla, row)
       {var col=MaxCol-2;
        if(TabIndexesArray[tabla][row][MaxCol-1]==1)
           {while(TabIndexesArray[tabla][row][col]=="")
            {if(col>0)
                {col--;}
             else
                {if(row>0)
                    {return FindLastColumn(tabla,row-1);
                    }
                 else
                    {return "99|99";
                    }
                }
            }
            return row+"|"+col;
           }
        else
           {return FindLastColumn(tabla,row-1);
           }
       }
       
       
       //Recibe la flecha que se presiono y el Tab Index de la caja en la que se encontraba el foco y
       //busca en base a eso el nombre de la caja de texto a la que tiene que moverse en el arreglo
       //multidimensional que llenamos a partir de los Tab Index
       function Arrows(evt, tabOrigen)
       { var key = evt.keyCode;
        if(tabOrigen = -1)
            {
                if(evt.target==null)
                    {tabOrigen=evt.srcElement.tabIndex;
                    }
                else
                    {tabOrigen = evt.target.tabIndex;
                    }                
            }
        
        tabla = (tabOrigen/10000)|0;
        fila = ((tabOrigen%10000)/100)|0;
        columna = tabOrigen%100;
        
        if (key == 37)//Ir a la IZQUIERDA
         {if(columna > 60)
            {columna = (columna-60)*3-2;
            }
          if(columna > 30)
            {columna = (columna-30)*2-1;
            }
          
         if(columna > 1) //Verificar que haya algo a la izquierda, tiene que estar al menos en la columna 2
              {SigCaja = document.getElementById(TabIndexesArray[tabla-1][fila-1][columna-2]);
              }
          else
              {//Como no hay negativos el else implica que est� en la primera columna
                {if(fila > 1)//Si al menos hay una fila antes
                    {arribaAlFinal = FindLastColumn(tabla-1, fila-2).split('|');//Encuentra la �ltima columna no-vacia en la fila anterior de la misma tabla
                     
                     SigCaja = document.getElementById(TabIndexesArray[tabla-1][arribaAlFinal[0]][arribaAlFinal[1]]);
                    }
                 else
                    {//Como no hay negativos el else implica que est� en la primera fila
                     if(tabla > 1)//Si hay al menos una tabla antes
                        {ultimaFila = FindLastRow(tabla-2);//Encuentra la �ltima fila con datos en la tabla anterior
                         arribaAlFinal = FindLastColumn(tabla-2, ultimaFila).split('|');//Encuentra la �ltima columna no-vacia en la �ltima fila de la tabla anterior
                         SigCaja = document.getElementById(TabIndexesArray[tabla-2][arribaAlFinal[0]][arribaAlFinal[1]]);                         
                        }
                     else
                        {//Como no hay negativos el else implica que est� en la primera tabla
                         //Como ya no hay ni columnas, ni filas ni tablas antes no te puedes mover a la izquierda asi que no nos sirve la flecha a la izquierda
                         return false;
                        }
                    }
                }
              }
         if(SigCaja!=null)
          {
           if(SigCaja.disabled==false && SigCaja.style.display != 'none')
           {
            SigCaja.focus();
            SigCaja.select();
            return false;
           }
           if(SigCaja.id.indexOf("opt")!=-1)
                return false;
//              {if(!SigCaja.checked)SigCaja.checked=false;}  
//           else
//              {SigCaja.select();
//              }              
          }    
          else
          {
            return false;
          }

         }
         
         
         
         
         if (key == 38)//Ir hacia ARRIBA
         {  if(columna > 60)
            {columna = (columna-60)*3;
            }
          if(columna > 30)
            {columna = (columna-30)*2;
            }       
           if(fila > 1) //Verificar que haya algo arriba, debe estar por lo menos en la fila 2
              {if(TabIndexesArray[tabla-1][fila-2][columna-1]!="")
                   {SigCaja = document.getElementById(TabIndexesArray[tabla-1][fila-2][columna-1]);
                   }
               else//Si no hay nada arriba irse a la �ltima caja de la fila anterior
                   {arribaAlFinal = FindLastColumn(tabla-1, fila-2).split('|');
                    SigCaja = document.getElementById(TabIndexesArray[tabla-1][arribaAlFinal[0]][arribaAlFinal[1]]);
                   }
              }
          else
              {//Como no hay negativos el else implica que est� en la primera columna
                {if(tabla > 1)//Si al menos hay una tabla antes
                    {
                         ultimaFila = FindLastRow(tabla-2);//Encuentra la �ltima fila con campos en la tabla anterior
                         arribaAlFinal = FindLastColumn(tabla-2, ultimaFila).split('|');//Encuentra la �ltima columna no-vacia en la �ltima fila de la tabla anterior
                         SigCaja = document.getElementById(TabIndexesArray[tabla-2][arribaAlFinal[0]][arribaAlFinal[1]]);                         
                     }
                 else
                     {//Como no hay negativos el else implica que est� en la primera tabla
                      //Como est�mos en la primera fila de la primera tabla no podemos ir m�s arriba
                      return false;
                     }
                }
              }
        if(SigCaja!=null)
        {
            if(SigCaja.disabled==false && SigCaja.style.display != 'none')
            {
                SigCaja.focus();
                SigCaja.select();
                return false;
            }
            if(SigCaja.id.indexOf("opt")!=-1)
                return false;
//              {if(!SigCaja.checked)SigCaja.checked=false;}  
//           else
//              {SigCaja.select();
//              }              
          }    
          else
          {
            return false;
          }

         }
         
         
        if (key == 39 || key==9 || key==13 || key==27) //Ir hacia la DERECHA
         {if(columna > 60)
            {columna = (columna-60)*3;
            }
          if(columna > 30)
            {columna = (columna-30)*2;
            }
          
        //Obtiene la �ltima celda de la fila (para saber donde termina esa fila)
        ultimaCelda = FindLastColumn(tabla-1,fila-1).split('|');
        if(columna < parseInt(ultimaCelda[1])+1) //Verificar que haya algo a la derecha, tiene que estar hasta una celda antes de la �ltima para poder entrar
              {SigCaja = document.getElementById(TabIndexesArray[tabla-1][fila-1][columna]);}
          else
              {//Si a la derecha ya no hay nada, hay que ver que hay hac�a abajo
                ultimaFila = FindLastRow(tabla-1)+1;
                {if(fila < ultimaFila)//Si al menos hay una fila despu�s
                    {if(TabIndexesArray[tabla-1][fila][MaxCol-1]=="")
                        SigCaja = document.getElementById(TabIndexesArray[tabla-1][fila+1][0]); //Se va a la primera posici�n de la siguiente fila de la misma tabla
                     else
                        SigCaja = document.getElementById(TabIndexesArray[tabla-1][fila][0]);
                    }
                 else
                    {//Si ya no hay columnas ni filas en la tabla, intenta ir a la que sigue
                     if(tabla < 3)//Se supone que no habr� mas de 3 tablas por p�gina, as� que debe estar antes de la tercer tabla
                        {
                         SigCaja = document.getElementById(TabIndexesArray[tabla][0][0]);
                        }
                     else
                        {//Como no hay negativos el else implica que est� en la primera tabla
                         //Como ya no hay ni columnas, ni filas ni tablas antes no te puedes mover a la izquierda asi que no nos sirve la flecha a la izquierda
                         return false;
                        }
                    }
                }
              }
         if(SigCaja!=null)
          {
           if(SigCaja.disabled==false && SigCaja.style.display != 'none')
           {
            
            SigCaja.focus();
            SigCaja.select();
            return false;
           }
           if(SigCaja.id.indexOf("opt")!=-1)
                return false;
//              {if(!SigCaja.checked)SigCaja.checked=false;}  
//           else
//              {SigCaja.select();
//              }
           if(key == 9 || key == 13) 
           {return false;
           }             
          }    
          else
          {
            return false;
          }

         }
         
         

         if (key == 40)//Ir hacia ABAJO
         {  if(columna > 60)
            {columna = (columna-60)*3;
            }
          if(columna > 30)
            {columna = (columna-30)*2;
            }       
          ultimaFila = FindLastRow(tabla-1);
          if(fila <= ultimaFila) //Verificar que haya algo abajo, debe estar por lo menos una fila antes de la final
              {if(TabIndexesArray[tabla-1][fila][columna-1]!="")
                   {SigCaja = document.getElementById(TabIndexesArray[tabla-1][fila][columna-1]);
                   }
               else//Si no hay nada abajo irse a la �ltima caja de la fila siguiente
                   {if(TabIndexesArray[tabla-1][fila][MaxCol-1]=="")
                       {SigCaja = document.getElementById(TabIndexesArray[tabla-1][fila+1][0]);
                       }
                    else
                       {abajoAlFinal = FindLastColumn(tabla-1, fila).split('|');
                        SigCaja = document.getElementById(TabIndexesArray[tabla-1][abajoAlFinal[0]][abajoAlFinal[1]]);
                       }
                   }
              }
          else
              {//Significa que est�mos en la �ltima fila
                {if(tabla < 3)//Si al menos hay una tabla despu�s
                    {
                     if(TabIndexesArray[tabla][0][0]!="")
                        {SigCaja = document.getElementById(TabIndexesArray[tabla][0][0]);
                        }
                     else
                        {//Si no hay nada en la primera columna de la primara fila de la siguiente tabla es que est� vacia
                         return false;
                        }
                     }
                 else
                     {//Como ya no hay tablas antes no te puedes mover hacia abajo asi que no nos sirve la flecha abajo
                      return false;
                     }
                }
              }
         if(SigCaja!=null)
          {
           if(SigCaja.disabled==false && SigCaja.style.display != 'none')
           {
            SigCaja.focus();
            SigCaja.select();
            return false;
           }
           if(SigCaja.id.indexOf("opt")!=-1)
                return false;     
          }    
          else
          {
            return false;
          }

        }
         
         
        if (key == 32 && evt.srcElement.id.indexOf('opt')!=-1)
        {
            if(evt.srcElement.checked){
                evt.srcElement.checked=false;
                
                }
            else{
                evt.srcElement.checked=true;
               
                }
        }
         
        if (key == 8)
            {return true;
            }

      } 
