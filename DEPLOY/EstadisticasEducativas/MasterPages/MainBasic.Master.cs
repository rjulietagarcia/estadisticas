using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AjaxControlToolkit;
using System.Security.Permissions;
using System.IO;
//using Mx.Com.Cimar.Supports.Web.Pages;

namespace SERaiz.MasterPages
{
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    public partial class MainBasic : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           Page.Header.Title = "SEP - Estad�sticas 911";

           DateTime buildDate = new FileInfo(Assembly.GetExecutingAssembly().Location).LastWriteTime;
           lblAssembly.Text ="Fecha de Versi�n: "+ buildDate.ToString();
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            this.Page.Form.SkinID = "Login1";
        } 
        public void SetMessage(int kind, string msg)
        {
            // Se implementa pero en esta p�gina no aplica.
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // Clear the embedded header styles that ASP.NET automatically adds for the menu
            ArrayList selectorStyles = (ArrayList)GetPrivateField(Page.Header.StyleSheet, "_selectorStyles");
            if (selectorStyles != null)
                selectorStyles.Clear();
        }
        private object GetPrivateField(object container, string fieldName)
        {
            Type type = container.GetType();
            FieldInfo fieldInfo = type.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
            return fieldInfo.GetValue(container);
        }
    }
}
