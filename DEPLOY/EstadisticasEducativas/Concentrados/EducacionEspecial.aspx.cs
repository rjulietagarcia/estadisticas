using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace EstadisticasEducativas.EducacionEspecial
{
    public partial class EducacionEspecial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Listas();
                ObtenerNecesidadesEducativas();
                RegistraAcceso();
            }
        }

        private void RegistraAcceso()
        {
            SEroot.WsRefSeguridad.ControlSeguridad wsSeg = new SEroot.WsRefSeguridad.ControlSeguridad();
            SEroot.WsRefSeguridad.AccesoLogDP LogDP = new SEroot.WsRefSeguridad.AccesoLogDP();
            LogDP.logId = 0;
            LogDP.opcionId = 4;
            LogDP.usuarioId = 0;
            LogDP.fecha = DateTime.Now.ToString();
            LogDP.ip = this.Request.UserHostAddress;
            string resultado = wsSeg.GuardaAcceso(LogDP);
        }

        protected void Listas()
        {
            ddlCiclo.Items.Add(new ListItem("2005-2006", "F2005"));
            ddlCiclo.Items.Add(new ListItem("2006-2007", "F2006"));
            ddlCiclo.SelectedIndex = 1;

            ddlNivel.Items.Add(new ListItem("", "0"));
            ddlNivel.Items.Add(new ListItem("PREESCOLAR", "11"));
            ddlNivel.Items.Add(new ListItem("PRIMARIA", "12"));
            ddlNivel.Items.Add(new ListItem("SECUNDARIA", "13"));
            ddlNivel.Items.Add(new ListItem("TECNICO PROFESIONAL", "21"));
            ddlNivel.Items.Add(new ListItem("BACHILLERATO", "22"));
            ddlNivel.Items.Add(new ListItem("LICENCIATURA", "32"));
            ddlNivel.Items.Add(new ListItem("INICIAL", "41"));
            ddlNivel.Items.Add(new ListItem("INICIAL NO ESCOLARIZADA", "42"));
            ddlNivel.Items.Add(new ListItem("ESPECIAL", "51"));
            ddlNivel.Items.Add(new ListItem("ADULTOS", "61"));
            ddlNivel.SelectedIndex = 1;

            ddlRegion.Items.Add(new ListItem("", "0"));
            ddlRegion.Items.Add(new ListItem("REGION 1", "1"));
            ddlRegion.Items.Add(new ListItem("REGION 2", "2"));
            ddlRegion.Items.Add(new ListItem("REGION 3", "3"));
            ddlRegion.Items.Add(new ListItem("REGION 4", "4"));
            ddlRegion.Items.Add(new ListItem("REGION 5", "5"));
            ddlRegion.Items.Add(new ListItem("REGION 6", "6"));
            ddlRegion.Items.Add(new ListItem("REGION 7", "7"));
            ddlRegion.Items.Add(new ListItem("REGION 8", "8"));
            ddlRegion.Items.Add(new ListItem("REGION 9", "9"));
            ddlRegion.Items.Add(new ListItem("REGION 10", "10"));
            ddlRegion.Items.Add(new ListItem("REGION 11", "11"));
            ddlRegion.Items.Add(new ListItem("REGION 12", "12"));
            ddlRegion.SelectedIndex = 0;

            SEroot.WsInmuebles.Service_Inmuebles wsInm = new SEroot.WsInmuebles.Service_Inmuebles();
            SEroot.WsInmuebles.DsListaMunicipioCombo municipios = wsInm.ListaMunicipio(223, 19);
            ddlMunicipio.DataSource = municipios;
            ddlMunicipio.DataValueField = municipios.Municipio.Id_MunicipioColumn.ColumnName;
            ddlMunicipio.DataTextField = municipios.Municipio.NombreColumn.ColumnName;
            ddlMunicipio.DataBind();
            if (ddlMunicipio.Items.Count > 1)
            {
                ddlMunicipio.Items.Insert(0, "");
            }
            ddlMunicipio.SelectedIndex = 0;
	    }

        protected void ObtenerNecesidadesEducativas()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" where len(cct)>0 " );
            if (ddlCiclo.SelectedItem.Value != "0")
            {
                sb.Append(" and periodo='" + ddlCiclo.SelectedItem.Value + "'");
            }
            if (ddlNivel.SelectedItem.Value != "0")
            {
                sb.Append(" and nivel=" + ddlNivel.SelectedItem.Value);
            }
            if (ddlRegion.SelectedItem.Value != "0")
            {
                sb.Append(" and region=" + ddlRegion.SelectedItem.Value);
            }
            if (ddlMunicipio.SelectedItem.Value.ToString().Length > 0)
            {
                sb.Append(" and municipio=" + ddlMunicipio.SelectedItem.Value);
            }

            SEroot.WsEstadisticasEducativas.ServiceEstadisticas ws = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
            SEroot.WsEstadisticasEducativas.DsEducacionEspecial DsNEE = new SEroot.WsEstadisticasEducativas.DsEducacionEspecial();
            DsNEE = ws.ListaNecesidadesEducativas(sb.ToString());
            sb.Length = 0;

            sb.Append("<table style='width: 790px' border='1' cellpadding='0' cellspacing='0'> ");
            sb.Append("<tr style='FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #ffffff; FONT-FAMILY: Verdana, Tahoma, Arial; BACKGROUND-COLOR: #cc0000'> ");

            sb.Append("<th align='left' rowspan='2'>CONDICIÓN CON LA QUE SE ASOCIAN LAS (NEE) DE LOS ALUMNOS</th> ");

            if (ddlNivel.SelectedItem.Value == "41" || ddlNivel.SelectedItem.Value == "42" || ddlNivel.SelectedItem.Value == "0")
            {
                sb.Append("<th colspan='3'>LACTANTES Y MATERNALES</th> ");
            }
            sb.Append("<th colspan='3'>1°</th> ");
            sb.Append("<th colspan='3'>2°</th> ");
            sb.Append("<th colspan='3'>3°</th> ");
            if (ddlNivel.SelectedItem.Value == "12" || ddlNivel.SelectedItem.Value == "0")
            {
                sb.Append("<th colspan='3'>4°</th> ");
                sb.Append("<th colspan='3'>5°</th> ");
                sb.Append("<th colspan='3'>6°</th> ");
            }
            sb.Append("<th colspan='3'>TOTAL</th> ");
            sb.Append("</tr> ");
            sb.Append("<tr style='FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #ffffff; FONT-FAMILY: Verdana, Tahoma, Arial; BACKGROUND-COLOR: #cc0000'> ");

            if (ddlNivel.SelectedItem.Value == "41" || ddlNivel.SelectedItem.Value == "42" || ddlNivel.SelectedItem.Value == "0")
            {
                sb.Append("<th>H</th><th>M</th><th>T</th> ");
            }
            sb.Append("<th>H</th><th>M</th><th>T</th> ");
            sb.Append("<th>H</th><th>M</th><th>T</th> ");
            sb.Append("<th>H</th><th>M</th><th>T</th> ");
            if (ddlNivel.SelectedItem.Value == "12" || ddlNivel.SelectedItem.Value == "0")
            {
                sb.Append("<th>H</th><th>M</th><th>T</th> ");
                sb.Append("<th>H</th><th>M</th><th>T</th> ");
                sb.Append("<th>H</th><th>M</th><th>T</th> ");
            }
            sb.Append("<th>H</th><th>M</th><th>T</th> ");
            sb.Append("</tr> ");

            int cantCiclos = 0;
            if (DsNEE != null && DsNEE.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in DsNEE.Tables[0].Rows)
                {
                    cantCiclos++;
                    if (cantCiclos % 2 == 0)
                    {
                        sb.Append("<tr style='FONT-SIZE: 11px; FONT-FAMILY: Arial'>");
                        sb.Append("<td bgcolor='#dcdcdc' align='left' width='250' style='FONT-WEIGHT: bold'>" + dr["NES_ESPECIAL"].ToString() + "</td>");
                        if (ddlNivel.SelectedItem.Value == "41" || ddlNivel.SelectedItem.Value == "42" || ddlNivel.SelectedItem.Value == "0")
                        {
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["H_LYM"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["M_LYM"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["T_LYM"].ToString() + "</td>");
                        }
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["H1"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["M1"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["T1"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["H2"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["M2"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["T2"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["H3"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["M3"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["T3"].ToString() + "</td>");
                        if (ddlNivel.SelectedItem.Value == "12" || ddlNivel.SelectedItem.Value == "0")
                        {
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["H4"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["M4"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["T4"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["H5"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["M5"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["T5"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["H6"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["M6"].ToString() + "</td>");
                            sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["T6"].ToString() + "</td>");
                        }
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["HT"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["MT"].ToString() + "</td>");
                        sb.Append("<td bgcolor='#dcdcdc' align='center' width='60'>" + dr["TT"].ToString() + "</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr style='FONT-SIZE: 11px; FONT-FAMILY: Arial'>");
                        sb.Append("<td align='left' width='250' style='FONT-WEIGHT: bold'>" + dr["NES_ESPECIAL"].ToString() + "</td>");
                        if (ddlNivel.SelectedItem.Value == "41" || ddlNivel.SelectedItem.Value == "42" || ddlNivel.SelectedItem.Value == "0")
                        {
                            sb.Append("<td align='center' width='60'>" + dr["H_LYM"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["M_LYM"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["T_LYM"].ToString() + "</td>");
                        }
                        sb.Append("<td align='center' width='60'>" + dr["H1"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["M1"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["T1"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["H2"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["M2"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["T2"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["H3"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["M3"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["T3"].ToString() + "</td>");
                        if (ddlNivel.SelectedItem.Value == "12" || ddlNivel.SelectedItem.Value == "0")
                        {
                            sb.Append("<td align='center' width='60'>" + dr["H4"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["M4"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["T4"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["H5"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["M5"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["T5"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["H6"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["M6"].ToString() + "</td>");
                            sb.Append("<td align='center' width='60'>" + dr["T6"].ToString() + "</td>");
                        }
                        sb.Append("<td align='center' width='60'>" + dr["HT"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["MT"].ToString() + "</td>");
                        sb.Append("<td align='center' width='60'>" + dr["TT"].ToString() + "</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else // no hay registros
            {
                sb.Append("<tr style='FONT-SIZE: 11px; FONT-FAMILY: Arial'>");
                sb.Append("<td align='left' width='250' style='FONT-WEIGHT: bold'>" + "&nbsp;" + "</td>");
                if (ddlNivel.SelectedItem.Value == "41" || ddlNivel.SelectedItem.Value == "42" || ddlNivel.SelectedItem.Value == "0")
                {
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                }
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                if (ddlNivel.SelectedItem.Value == "12" || ddlNivel.SelectedItem.Value == "0")
                {
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                    sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                }
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("<td align='center' width='60'>" + "&nbsp;" + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            nee.InnerHtml = sb.ToString();
        }

        protected void ddlNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObtenerNecesidadesEducativas();
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlMunicipio.SelectedIndex = 0;
            ObtenerNecesidadesEducativas();
        }

        protected void ddlMunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObtenerNecesidadesEducativas();
        }

        protected void ddlCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObtenerNecesidadesEducativas();
        }
    }
}
