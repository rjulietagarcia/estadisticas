<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EducacionEspecial.aspx.cs" Inherits="EstadisticasEducativas.EducacionEspecial.EducacionEspecial" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Educación Especial</title>
    <link href="../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin-top:0px">
    <table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0">
        <tr>
            <td width="100%" align="center">
                <table id="Table2" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td><img alt="" src="../tema/images/img_nuevo_leon_unido.jpg" style="width: 232px; height: 113px" /></td>
                        <td><img alt="" src="../tema/images/banner_nl.jpg" /></td>
                        <td><img alt="" style="WIDTH: 224px; HEIGHT: 122px" src="../tema/images/nlogo_se.jpg" /></td>
                    </tr>
                    <tr>
                    <td colspan="3">
                        <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" height="35" style="font-weight: bold; font-size: 13px; color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600; width:100px"><a style="color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600" href="/se/portal/Index.aspx">Inicio</a></td>
                            <td style="FONT-WEIGHT: bold; FONT-SIZE: 16px; COLOR: #ffffff; FONT-FAMILY: Verdana, Tahoma, Arial; BACKGROUND-COLOR: #006600" align="center" height="35">Estadísticas Educativas</td>
                            <td align="center" height="35" style="font-weight: bold; font-size: 16px; color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600; width:100px"></td>
                        </tr>
                        </table>
                    </td>
                    </tr>
               		<tr>
            			<td style="text-align:center" colspan="3"><asp:label id="Label1" CssClass="titulopagina" runat="server" ForeColor="#006600" >Educación Especial</asp:label></td>
		            </tr>
                </table>
            </td>
        </tr>
    </table>

    <form id="form1" runat="server">
    <div align="center">
        <table>
            <tr>
                <td align="left">
                    <asp:Label ID="lblCiclo" runat="server" CssClass="lblRojo" Text="Ciclo Escolar:"></asp:Label></td>
                <td align="left">
                    <asp:DropDownList ID="ddlCiclo" runat="server" AutoPostBack="True" CssClass="lblGrisTit" OnSelectedIndexChanged="ddlCiclo_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblNivel" runat="server" Text="Nivel:" CssClass="lblRojo"></asp:Label></td>
                <td align="left">
                    <asp:DropDownList ID="ddlNivel" runat="server" AutoPostBack="True" CssClass="lblGrisTit" OnSelectedIndexChanged="ddlNivel_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblRegion" runat="server" Text="Región:" CssClass="lblRojo"></asp:Label></td>
                <td align="left">
                    <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="True" CssClass="lblGrisTit" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="lblMunicipio" runat="server" Text="Municipio:" CssClass="lblRojo"></asp:Label></td>
                <td align="left">
                    <asp:DropDownList ID="ddlMunicipio" runat="server" AutoPostBack="True" CssClass="lblGrisTit" OnSelectedIndexChanged="ddlMunicipio_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
        </table>
        <br />
        <div align="center" id="nee" runat="server"></div>
    </div>
    </form>
</body>
</html>
