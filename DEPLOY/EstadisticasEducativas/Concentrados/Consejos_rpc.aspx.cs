using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace EstadisticasEducativas.Concentrados
{
    public partial class Consejos_rpc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RegistraAcceso();
            }
        }
        private void RegistraAcceso()
        {
            SEroot.WsRefSeguridad.ControlSeguridad wsSeg = new SEroot.WsRefSeguridad.ControlSeguridad();
            SEroot.WsRefSeguridad.AccesoLogDP LogDP = new SEroot.WsRefSeguridad.AccesoLogDP();
            LogDP.logId = 0;
            LogDP.opcionId = 3;
            LogDP.usuarioId = 0;
            LogDP.fecha = DateTime.Now.ToString();
            LogDP.ip = this.Request.UserHostAddress;
            string resultado = wsSeg.GuardaAcceso(LogDP);
        }
    }
}
