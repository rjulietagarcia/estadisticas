using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace EstadisticasEducativas.Concentrados
{
    public partial class GraficasInmuebles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                EducacionBasica();
                RegistraAcceso();
            }
        }
        private void RegistraAcceso()
        {
            SEroot.WsRefSeguridad.ControlSeguridad wsSeg = new SEroot.WsRefSeguridad.ControlSeguridad();
            SEroot.WsRefSeguridad.AccesoLogDP LogDP = new SEroot.WsRefSeguridad.AccesoLogDP();
            LogDP.logId = 0;
            LogDP.opcionId = 6;
            LogDP.usuarioId = 0;
            LogDP.fecha = DateTime.Now.ToString();
            LogDP.ip = this.Request.UserHostAddress;
            string resultado = wsSeg.GuardaAcceso(LogDP);
        }
        protected void rbtNivel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbtNivel.Items[0].Selected == true)
            {
                EducacionBasica();
            }
            if (rbtNivel.Items[1].Selected == true)
            {
                Preescolar();
            }
            if (rbtNivel.Items[2].Selected == true)
            {
                Primaria();
            }
            if (rbtNivel.Items[3].Selected == true)
            {
                Secundaria();
            }
        }

        private void EducacionBasica()
        {
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Educaci�n B�sica</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeonGIE0708bas.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/AbasoloGIE0708bas.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/ArroyoGIE0708bas.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/HiguerasGIE0708bas.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/MonterreyGIE0708bas.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AgualeguasGIE0708bas.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CossGIE0708bas.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HualahuisesGIE0708bas.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ParasGIE0708bas.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AllendeGIE0708bas.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GonzalezGIE0708bas.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/IturbideGIE0708bas.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/PesqueriaGIE0708bas.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AnahuacGIE0708bas.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GaleanaGIE0708bas.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LampazosGIE0708bas.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RayonesGIE0708bas.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ApodacaGIE0708bas.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GarciaGIE0708bas.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LinaresGIE0708bas.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SabinasGIE0708bas.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AramberriGIE0708bas.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BravoGIE0708bas.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HerrerasGIE0708bas.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SalinasGIE0708bas.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/JuarezGIE0708bas.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/EscobedoGIE0708bas.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AldamasGIE0708bas.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_NicolasGIE0708bas.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BustamanteGIE0708bas.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TeranGIE0708bas.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RamonesGIE0708bas.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_PedroGIE0708bas.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CadereytaGIE0708bas.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TrevinoGIE0708bas.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MarinGIE0708bas.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SantiagoGIE0708bas.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CarmenGIE0708bas.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZaragozaGIE0708bas.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/OcampoGIE0708bas.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_CatarinaGIE0708bas.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CerralvoGIE0708bas.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZuazuaGIE0708bas.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MierGIE0708bas.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VallecilloGIE0708bas.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ChinaGIE0708bas.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GuadalupeGIE0708bas.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MinaGIE0708bas.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VillaldamaGIE0708bas.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CienegaGIE0708bas.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HidalgoGIE0708bas.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MontemorelosGIE0708bas.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        private void Preescolar()
        {
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Preescolar</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeonGIE0708pre.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/AbasoloGIE0708pre.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/ArroyoGIE0708pre.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/HiguerasGIE0708pre.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/MonterreyGIE0708pre.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AgualeguasGIE0708pre.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CossGIE0708pre.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HualahuisesGIE0708pre.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ParasGIE0708pre.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AllendeGIE0708pre.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GonzalezGIE0708pre.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/IturbideGIE0708pre.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/PesqueriaGIE0708pre.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AnahuacGIE0708pre.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GaleanaGIE0708pre.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LampazosGIE0708pre.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RayonesGIE0708pre.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ApodacaGIE0708pre.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GarciaGIE0708pre.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LinaresGIE0708pre.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SabinasGIE0708pre.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AramberriGIE0708pre.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BravoGIE0708pre.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HerrerasGIE0708pre.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SalinasGIE0708pre.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/JuarezGIE0708pre.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/EscobedoGIE0708pre.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AldamasGIE0708pre.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_NicolasGIE0708pre.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BustamanteGIE0708pre.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TeranGIE0708pre.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RamonesGIE0708pre.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_PedroGIE0708pre.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CadereytaGIE0708pre.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TrevinoGIE0708pre.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MarinGIE0708pre.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SantiagoGIE0708pre.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CarmenGIE0708pre.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZaragozaGIE0708pre.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/OcampoGIE0708pre.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_CatarinaGIE0708pre.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CerralvoGIE0708pre.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZuazuaGIE0708pre.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MierGIE0708pre.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VallecilloGIE0708pre.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ChinaGIE0708pre.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GuadalupeGIE0708pre.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MinaGIE0708pre.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VillaldamaGIE0708pre.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CienegaGIE0708pre.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HidalgoGIE0708pre.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MontemorelosGIE0708pre.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        private void Primaria()
        {
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Primaria</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeonGIE0708pri.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/AbasoloGIE0708pri.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/ArroyoGIE0708pri.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/HiguerasGIE0708pri.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/MonterreyGIE0708pri.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AgualeguasGIE0708pri.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CossGIE0708pri.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HualahuisesGIE0708pri.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ParasGIE0708pri.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AllendeGIE0708pri.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GonzalezGIE0708pri.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/IturbideGIE0708pri.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/PesqueriaGIE0708pri.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AnahuacGIE0708pri.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GaleanaGIE0708pri.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LampazosGIE0708pri.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RayonesGIE0708pri.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ApodacaGIE0708pri.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GarciaGIE0708pri.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LinaresGIE0708pri.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SabinasGIE0708pri.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AramberriGIE0708pri.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BravoGIE0708pri.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HerrerasGIE0708pri.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SalinasGIE0708pri.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/JuarezGIE0708pri.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/EscobedoGIE0708pri.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AldamasGIE0708pri.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_NicolasGIE0708pri.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BustamanteGIE0708pri.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TeranGIE0708pri.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RamonesGIE0708pri.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_PedroGIE0708pri.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CadereytaGIE0708pri.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TrevinoGIE0708pri.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MarinGIE0708pri.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SantiagoGIE0708pri.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CarmenGIE0708pri.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZaragozaGIE0708pri.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/OcampoGIE0708pri.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_CatarinaGIE0708pri.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CerralvoGIE0708pri.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZuazuaGIE0708pri.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MierGIE0708pri.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VallecilloGIE0708pri.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ChinaGIE0708pri.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GuadalupeGIE0708pri.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MinaGIE0708pri.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VillaldamaGIE0708pri.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CienegaGIE0708pri.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HidalgoGIE0708pri.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MontemorelosGIE0708pri.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        private void Secundaria()
        {
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Secundaria</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeonGIE0708sec.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/AbasoloGIE0708sec.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/ArroyoGIE0708sec.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/HiguerasGIE0708sec.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/MonterreyGIE0708sec.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AgualeguasGIE0708sec.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CossGIE0708sec.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HualahuisesGIE0708sec.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ParasGIE0708sec.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AllendeGIE0708sec.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GonzalezGIE0708sec.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/IturbideGIE0708sec.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/PesqueriaGIE0708sec.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AnahuacGIE0708sec.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GaleanaGIE0708sec.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LampazosGIE0708sec.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RayonesGIE0708sec.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ApodacaGIE0708sec.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GarciaGIE0708sec.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LinaresGIE0708sec.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SabinasGIE0708sec.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AramberriGIE0708sec.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BravoGIE0708sec.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HerrerasGIE0708sec.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SalinasGIE0708sec.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/JuarezGIE0708sec.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/EscobedoGIE0708sec.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AldamasGIE0708sec.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_NicolasGIE0708sec.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BustamanteGIE0708sec.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TeranGIE0708sec.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RamonesGIE0708sec.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_PedroGIE0708sec.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CadereytaGIE0708sec.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TrevinoGIE0708sec.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MarinGIE0708sec.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SantiagoGIE0708sec.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CarmenGIE0708sec.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZaragozaGIE0708sec.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/OcampoGIE0708sec.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_CatarinaGIE0708sec.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CerralvoGIE0708sec.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZuazuaGIE0708sec.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MierGIE0708sec.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VallecilloGIE0708sec.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ChinaGIE0708sec.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GuadalupeGIE0708sec.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MinaGIE0708sec.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VillaldamaGIE0708sec.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CienegaGIE0708sec.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HidalgoGIE0708sec.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MontemorelosGIE0708sec.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

    }
}
