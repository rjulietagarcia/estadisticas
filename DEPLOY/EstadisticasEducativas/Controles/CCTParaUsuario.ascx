<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CCTParaUsuario.ascx.cs" Inherits="SeguridadGlobal.CCTParaUsuario" %>
<table class="tnormal" width="100%">
    <tr>
        <td class="lblEtiqueta" style="width: 133px">Nivel de Trabajo:</td>
        <td>
            <asp:DropDownList ID="ddlNivelTrabajo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlNivelTrabajo_SelectedIndexChanged" Width="100%">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td class="lblEtiqueta" style="width: 133px">
            Entidad:</td>
        <td>
            <asp:DropDownList ID="ddlEntidad" runat="server" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="ddlEntidad_SelectedIndexChanged" Width="100%">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td class="lblEtiqueta" style="width: 133px">
            Region:</td>
        <td>
            <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" Width="100%">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td class="lblEtiqueta" style="width: 133px">
            Sostenimiento:</td>
        <td>
            <asp:DropDownList ID="ddlSostenimiento" runat="server" Width="100%" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="ddlSostenimiento_SelectedIndexChanged">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td class="lblEtiqueta" style="width: 133px">
            Zona:</td>
        <td>
            <asp:DropDownList ID="ddlZona" runat="server" AutoPostBack="True" Enabled="False" OnSelectedIndexChanged="ddlZona_SelectedIndexChanged" Width="100%">
            </asp:DropDownList></td>
    </tr>
    <tr>
        <td class="lblEtiqueta" style="width: 133px">Centro de Trabajo:</td>
        <td>
            <asp:DropDownList ID="ddlCentroTrabajo" runat="server" Enabled="False" OnSelectedIndexChanged="ddlCentroTrabajo_SelectedIndexChanged" Width="100%" AutoPostBack="True">
            </asp:DropDownList></td>
    </tr>    
</table>
<br />
<table class="tnormal" width="100%">
    <tr>
        <td style="text-align: center" class="lblSubtitulo1">Centro de Trabajo Seleccionado
        </td>
    </tr>
    <tr>
        <td>
            <table class="tnormal" width="100%">
                <tr>
                    <td width="30" class="lblEtiqueta">
                        Clave:
                    </td>
                    <td class="lbldatos">
                        <asp:Literal ID="literalClaveSeleccionado" runat="server" Text="(Ninguno)"></asp:Literal>, Turno: <asp:Literal ID="literalTurno" runat="server" Text="(Ninguno)"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td width="30" class="lblEtiqueta">Nombre :</td>
                    <td class="lbldatos">
                        <asp:Literal ID="literalNombreSeleccionado" runat="server" Text="(Ninguno)"></asp:Literal>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="hdnTipo" runat="server" Value="0" />