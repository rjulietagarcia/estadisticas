using System;
using System.Collections.Generic;
using System.Text;
using Mx.Gob.Nl.Educacion.Querys;
using System.Data.Common;
using Mx.Gob.Nl.Educacion.Model;
using System.Collections.Specialized;

namespace Mx.Gob.Nl.Educacion.Especial
{
    public class EspecialSeguridad
    {
        /// <summary>
        /// Realiza la entrada de un usuario.
        /// </summary>
        /// <param name="con">conexi�n de base de datos.</param>
        /// <param name="username">nombre del usuario.</param>
        /// <param name="password">contrase�a del usuario.</param>
        /// <returns>cadena del usuario si es exitosa; de otra manera, cadena con n�mero de usuario en -1.</returns>
        public static String LoginIn(DbConnection con, String username, String password) {
            String resultado = "";
            DbTransaction tran = null;
            con.Open();
            try
            {
                LoginUsuarioQryDP[] logins = 
                    LoginUsuarioQryMD.ListForLoginPassword(con,
                        tran,0,1000, username, password);                    
                if ((logins != null) && (logins.Length != 0))
                {
                    #region llenado de cadena extra�a
                    string encabezadoUsuario =
                        logins[0].UsuarioId.ToString()+"|"+
                        logins[0].NiveltrabajoId.ToString()+"|"+
                        logins[0].RegionId.ToString()+"|"+
                        logins[0].ZonaId.ToString()+"|"+
                        logins[0].SectorId.ToString();
                    string centroTrabajo = "|";
                    string nivelEducacion = "";
                    string sostenimiento = "|";
                    foreach(LoginUsuarioQryDP login in logins) {
                        if (centroTrabajo.Length != 1)
                            centroTrabajo+= ",";
                        centroTrabajo+=login.CentrotrabajoId.ToString();
                        if (nivelEducacion.Length != 0)
                            nivelEducacion += ",";
                        nivelEducacion += login.NiveleducacionId.ToString();
                        if (sostenimiento.Length != 1)
                            sostenimiento += ",";
                        sostenimiento += login.SostenimientoId.ToString();
                    }
                    resultado = encabezadoUsuario + centroTrabajo + "|" + logins[0].PerfilId+"|"+nivelEducacion + sostenimiento;
                    //resultado = "184130|4|1|999|999|13429,13430|71|13,12|21,21";
                    #endregion
                    #region valida si hay restricci�n
                    if (GetParametro(con, tran, 19).Trim() == "1")
                        resultado = RestriccionAcceso(con, tran, resultado);
                    #endregion

                    #region restriccion por si el bitactivo es cero y el usuario si existe pero no se le permite acceder GELEN
                    if (logins[0].Bit_Activo==bool.Parse("false"))
                        {
                            resultado = resultado+"|false";
                        }
                    #endregion
                }
                else
                    resultado = "-1||||||||";
            }
            finally
            {
                con.Close();
            }
            return resultado;
        }        

        /// <summary>
        /// Revisa las restricciones de acceso.
        /// </summary>
        /// <param name="con">conexi�n de base de datos.</param>
        /// <param name="tran">transacci�n activa.</param>
        /// <param name="username">nombre del usuario.</param>
        /// <returns>Cadena con los datos del usuario cuando no hay restricci�n; si la hay, regresa cadena con el n�mero de usuario -1.</returns>
        internal static String RestriccionAcceso(DbConnection con, DbTransaction tran, String username) 
        {
            string resulta = username;
            if ((username != "") && (username.IndexOf('|') != -1)) //si tiene acceso
            {
                string[] credenciales = username.Split('|');
                #region Restricci�n cuando es director
                if (Int32.Parse(credenciales[1]) == 4)                       // si es director
                {
                    DateTime datenow = DateTime.Now;
                    if (((datenow.Hour >= 7) && (datenow.Hour <= 18)) &&  // horario valido entre 7:00:01 am y 18:59:59 pm
                       (datenow.DayOfWeek != DayOfWeek.Saturday) &&
                       (datenow.DayOfWeek != DayOfWeek.Sunday)) // si no es sabado o domingo
                    {
                        if (TieneRestriccionAccesoQryMD.CountForRestriccion(con, tran,
                            credenciales[7],4,credenciales[8],"","") == 0)
                        {
                            resulta = "-1||||||||";
                        }
                    }
                }
                #endregion
            }
            return resulta;
        
        }

        /// <summary>
        /// Obtiene un par�metro de trabajo.
        /// </summary>
        /// <param name="con">conexi�n a la bd.</param>
        /// <param name="tran">transacci�n activa.</param>
        /// <param name="parametroId"># de par�metro</param>
        /// <returns>Cadena con el valor del par�mtro; sino se encuentra el par�metors, cadena vac�a.</returns>
        internal static string GetParametro(DbConnection con, DbTransaction tran, int parametroId)
        {
            ParametroSistemaQryDP[] restriccion = ParametroSistemaQryMD.ListForParametro(con, tran, 0, 1, parametroId);
            if ((restriccion != null) && (restriccion.Length != 0))
                return restriccion[0].Valor;
            else
                return "";
        }

        /// <summary>
        /// Obtiene un diccionario con la lista de opciones v�lidas para el usuario.
        /// </summary>
        /// <param name="con">conexi�n de BD.</param>
        /// <param name="username">nombre del usuario.</param>
        /// <returns>Cadena con acciones y permisos.</returns>
        public static string GetUserPermision(DbConnection con, string username)
        {
            //StringDictionary sd = new StringDictionary();
            string resultado = "";
            con.Open();
            try
            {
                
                DbTransaction tran = null;
                Int64 cnt = AccionMD.Count(con, tran);
                if (cnt == 0)
                    cnt = 1;
                int sid = Convert.ToInt32(username.Split('|')[0]);
                OpcionesPermitidasQryDP[] acciones = OpcionesPermitidasQryMD.ListForUsuario(con, tran, 0, 1, sid);
                // AccionDP[] acciones = AccionMD.List(con, tran, 0, cnt);
                foreach (OpcionesPermitidasQryDP accion in acciones) 
                {
                    string llave = accion.Llave;
                        //accion.SistemaId.ToString() + "|" +
                        //    accion.ModuloId.ToString() + "|" +
                        //    accion.OpcionId.ToString();
                    //if (!sd.ContainsKey(llave))
                    //{
                        if (resultado.Length != 0)
                            resultado += ",";

                        resultado += llave + "=true";
                        //sd.Add(llave,
                        //        "true");
                    //}
                }
            }
            finally
            { 
                con.Close(); 
            }

            return resultado;
        }

        /// <summary>
        /// Checa que el arreglo tenga elementos.
        /// </summary>
        /// <param name="source">Arreglo de origen-</param>
        /// <returns>verdadero, si tiene elementos; de otra manera, falso.</returns>
        internal static bool ChecaTieneElementos(Array source)
        {
            return ((source != null) && (source.Length != 0));
        }

        /// <summary>
        /// Checa que el arreglo se encuentre vac�o.
        /// </summary>
        /// <param name="source">Arreglo de origen-</param>
        /// <returns>verdadero, si no tiene elementos; si los tiene, falso.</returns>
        internal static bool ChecaNoTieneElementos(Array source)
        {
            return ((source == null) || (source.Length == 0));
        }

        /// <summary>
        /// Calcula el permiso sobre una opcion.
        /// </summary
        /// <param name="con">conexi�n de base de datos.</param>
        /// <param name="tran">transacci�n activa de base de datos.</param>
        /// <param name="username">nombre del usuario.</param>
        /// <param name="accion">acci�n activa.</param>
        /// <returns>verdadero, si tiene permiso el usuario sobre una opci�n; si no lo tiene, falso.</returns>
        internal static bool calculaPermiso(DbConnection con, DbTransaction tran, string username, AccionDP accion)
        {
            byte lSistema = accion.SistemaId;
            byte lModulo = accion.ModuloId;
            byte lOpcion = accion.OpcionId;

            // Root o raiz del Sitemap.
            if ((lSistema == 0) && (lModulo == 0) && (lOpcion == 0))
                return true;

            #region Obtengo la informacion del usuario
            CicloEscolarActualQryDP[] ciclo = CicloEscolarActualQryMD.ListForTipoCiclo(con, tran, 0, 1, 1);

            short cicloEscolarId = -1;
            // No est� listo para manejar ciclo escolar.
            //if (ciclo.Length != 0)
            //    cicloEscolarId = ciclo[0].CicloescolarId;

            int sid = Convert.ToInt32(username.Split('|')[0]);
            if (sid == -1)
                return false;
            #endregion

            #region Checo si tiene restringido el acceso (solo test)

            //UsuarioAccionDenegadaQryDP[] deny =
            //    UsuarioAccionDenegadaQryMD.ListForOpcionesDenegadas(
            //        con, tran, 0, 1, lSistema, lModulo, lOpcion, sid,
            //        true, cicloEscolarId);
            //if (ChecaTieneElementos(deny))
            //    return false;
            #endregion

            #region Checo si tiene acceso directo
            Array grant =
                ((Array)(UsuarioAccionQryMD.ListForOpcionesAsignadas(
                    con, tran,
                    0, 1, lSistema, lModulo, lOpcion, sid,
                    true, cicloEscolarId)));
            if (!ChecaNoTieneElementos(grant))
                return true;
            #endregion

            #region Checo si tiene acceso por el perfil anterior
            grant =
                OpcionesPerfilesAnterioresQryMD.ListForOpcionesAsignadas(
                    con, tran, 
                    0, 1, lSistema, lModulo, lOpcion, sid,
                    true, cicloEscolarId);
            if (!ChecaNoTieneElementos(grant))
                return true;
            #endregion

            #region Checo si tiene acceso por el perfil nuevo
            //grant =
            //    OpcionesperfileseducacionQryMD.ListForOpcionesAsignadas(
            //        con, tran,
            //        0, 1, lSistema, lModulo, lOpcion, sid,
            //        true, cicloEscolarId);
            //if (!ChecaNoTieneElementos(grant))
            //    return true;
            #endregion

            return false;
        
        }

        /// <summary>
        /// Reemplaza un Url en tiempo de dise�o, por el Url donde si se encuentra la p�gina.
        /// </summary>
        /// <param name="source">Url Origen</param>
        /// <param name="sistema"></param>
        /// <returns></returns>
        internal static string changeRawPath(string source, SistemaPuertoQryDP sistema)
        {
            if ((sistema.CarpetaSistema != null) && (sistema.CarpetaSistema.Length != 0))
            {
                Uri url = new Uri(source);
                string port = url.Port.ToString();
                if ((sistema.Puerto != null) && (sistema.Puerto.Length != 0))
                {

                    source = url.PathAndQuery.ToUpper();
                    if (source.StartsWith("~/"))
                        source = source.Remove(0, 2);
                    if (source.StartsWith("/"))
                        source = source.Remove(0, 1);
                    source = "http://localhost:" + port + "/" + sistema.CarpetaSistema + source;
                    return source;
                }
            }
            return source;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="con"></param>
        /// <param name="sourceUrl"></param>
        /// <returns></returns>
        internal static SistemaPuertoQryDP getSistema(DbConnection con, string sourceUrl)
        {
            Uri url = new Uri(sourceUrl);
            string source = sourceUrl.ToUpper();
            if (source.StartsWith("~/"))
                source = source.Remove(0, 2);
            if (source.StartsWith("/"))
                source = source.Remove(0, 1);
            string port = url.Port.ToString();
            SistemaPuertoQryDP[] sis = SistemaPuertoQryBiz.LoadListForPuerto(con, 0, 1, port);

            
            string[] carpetas = source.Split('/');
            if (carpetas.Length != 0)
            {
                if (sis.Length != 0)
                    return sis[0];
            }
            return new SistemaPuertoQryDP();
        }

        /// <summary>
        /// <example>
        /// [WebMethod]
        /// public string[] EsValidaPorURL(int usuarioId, string url)
        /// {
        ///     url = Mx.Gob.Nl.Educacion.Especial.EspecialSeguridad.CambiaPath((DbConnection)con, url);
        ///             return Usuario.EsValida(con, usuarioId, url);
        /// }
        /// </example>
        /// </summary>
        /// <param name="con"></param>
        /// <param name="rawUrl"></param>
        /// <returns></returns>
        public static string CambiaPath(DbConnection con, string rawUrl) 
        {
            if (rawUrl.Contains(":"))
            {
                return changeRawPath(rawUrl,
                            getSistema(con, rawUrl));
            }
            else
                return rawUrl;
        }

        public static String GetLlavesCarpeta(DbConnection con, int usuarioId, string sourceUri) { 
            // 
            int longSegments = 0;
            if (!sourceUri.ToUpper().Contains("HTTP")) {
                sourceUri = "http://local" + sourceUri;
            }
            Uri url = new Uri(sourceUri);

            //if ((url.Port != 80) && (url.Port != 8044)) {
            //    url = new Uri(CambiaPath(con, sourceUri));
            //}
            con.Open();
            try
            {
                bool conAsterisco = false;
                byte idSistema = 0;
                byte idModulo = 0;
                byte idOpcion = 0;
                longSegments = url.Segments.Length;
                if (longSegments > 3)
                {
                    CarpetaSistemaQryDP[] carpetasSistemas =
                        CarpetaSistemaQryMD.ListForCarpeta(con, null, 0, 1000, url.Segments[longSegments - 3]);
                    if ((carpetasSistemas != null) && (carpetasSistemas.Length != 0))
                    {
                        idSistema = carpetasSistemas[0].SistemaId;
                    }
                    if (idSistema != 0)
                    {
                        CarpetaModuloQryDP[] carpetasModulo =
                            CarpetaModuloQryMD.ListForCarpeta(con, null, 0, 1000, idSistema, url.Segments[longSegments - 2]);
                        if ((carpetasModulo != null) && (carpetasModulo.Length != 0))
                        {
                            idModulo = carpetasModulo[0].ModuloId;
                        }
                        if (idModulo != 0)
                        {

                            CarpetaOpcionQryDP[] carpetasOpcion =
                                CarpetaOpcionQryMD.ListForCarpeta(con, null, 0, 1000, idSistema, idModulo, url.Segments[longSegments - 1] + "%");
                            idOpcion = 0;

                            string[] parametros;
                            if (url.Query.Length != 0)
                            {
                                parametros = url.Query.Split('&');
                            }
                            else
                            { 
                                parametros = new string[1];
                                parametros[0] = "";
                            }
                            for (int i = parametros.Length; i >= 0; i--)
                            {
                                string colaParametros = "";
                                for (int j = 0; j < i; j++) {
                                    if (colaParametros.Length != 0)
                                        colaParametros += "&";
                                    colaParametros += parametros[j];
                                }

                                foreach (CarpetaOpcionQryDP carpetaOpcion in carpetasOpcion)
                                {
                                    if (carpetaOpcion.CarpetaOpcion.ToUpper().Contains(colaParametros.ToUpper()))
                                    {
                                        idOpcion = carpetaOpcion.OpcionId;
                                        conAsterisco = carpetaOpcion.Abreviatura.Trim().Contains("*");
                                        break;
                                    }
                                }
                                if (idOpcion != 0) {
                                    break;
                                }
                            }
                        }

                    }
                }
                PermisoLlaveCarpetaQryDP[] permisos = PermisoLlaveCarpetaQryMD.ListForLlaves(con, null, 0, 100,
                    usuarioId, idSistema, idModulo, idOpcion);
                if (!((permisos != null) & (permisos.Length != 0)))
                {
                    // Cuando no se tiene los permisos.
                    idSistema = 0;
                    idModulo = 0;
                    if (idOpcion != 0)
                    {
                        idOpcion = 0;
                        return String.Format("{0}|{1}|{2}|{3}", idSistema, idModulo, idOpcion, 2);
                    }
                    else
                        idOpcion = 0;
                }
                return String.Format("{0}|{1}|{2}|{3}", idSistema, idModulo, idOpcion, conAsterisco ? 1 : 0);
                //
            }
            finally 
            {
                con.Close();
            }

        }
    }
}
