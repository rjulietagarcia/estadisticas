using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilEducacionSostenimientoPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilEducacionSostenimientoPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilEducacionSostenimientoPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// PerfilEducacionSostenimientoPk perfileducacionsostenimientoPk = new PerfilEducacionSostenimientoPk(perfileducacionsostenimiento);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("PerfilEducacionSostenimientoPk")]
    public class PerfilEducacionSostenimientoPk
    {
        #region Definicion de campos privados.
        private Byte sostenimientoId;
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Byte sistemaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de PerfilEducacionSostenimientoPk.
        /// </summary>
        /// <param name="sostenimientoId">Descripción sostenimientoId del tipo Byte.</param>
        /// <param name="perfilEducacionId">Descripción perfilEducacionId del tipo Int16.</param>
        /// <param name="niveltrabajoId">Descripción niveltrabajoId del tipo Byte.</param>
        /// <param name="sistemaId">Descripción sistemaId del tipo Byte.</param>
        public PerfilEducacionSostenimientoPk(Byte sostenimientoId, Int16 perfilEducacionId, Byte niveltrabajoId, Byte sistemaId) 
        {
            this.sostenimientoId = sostenimientoId;
            this.perfilEducacionId = perfilEducacionId;
            this.niveltrabajoId = niveltrabajoId;
            this.sistemaId = sistemaId;
        }
        /// <summary>
        /// Constructor normal de PerfilEducacionSostenimientoPk.
        /// </summary>
        public PerfilEducacionSostenimientoPk() 
        {
        }
        #endregion.
    }
}
