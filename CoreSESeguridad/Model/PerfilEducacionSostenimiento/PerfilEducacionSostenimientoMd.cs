using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class PerfilEducacionSostenimientoMD
    {
        private PerfilEducacionSostenimientoDP perfilEducacionSostenimiento = null;

        public PerfilEducacionSostenimientoMD(PerfilEducacionSostenimientoDP perfilEducacionSostenimiento)
        {
            this.perfilEducacionSostenimiento = perfilEducacionSostenimiento;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert PerfilEducacionSostenimiento
            DbCommand com = con.CreateCommand();
            String insertPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sostenimiento",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Sistema");
            com.CommandText = insertPerfilEducacionSostenimiento;
            #endregion
            #region Parametros Insert PerfilEducacionSostenimiento
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.SostenimientoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionSostenimiento.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.NiveltrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacionSostenimiento.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfilEducacionSostenimiento.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfilEducacionSostenimiento.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.SistemaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert PerfilEducacionSostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertPerfilEducacionSostenimiento = String.Format(PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoInsert, "", 
                    perfilEducacionSostenimiento.SostenimientoId.ToString(),
                    perfilEducacionSostenimiento.PerfilEducacionId.ToString(),
                    perfilEducacionSostenimiento.NiveltrabajoId.ToString(),
                    perfilEducacionSostenimiento.BitActivo.ToString(),
                    perfilEducacionSostenimiento.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(perfilEducacionSostenimiento.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    perfilEducacionSostenimiento.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorInsertPerfilEducacionSostenimiento);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find PerfilEducacionSostenimiento
            DbCommand com = con.CreateCommand();
            String findPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sostenimiento",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Id_Sistema");
            com.CommandText = findPerfilEducacionSostenimiento;
            #endregion
            #region Parametros Find PerfilEducacionSostenimiento
            Common.CreateParameter(com, String.Format("{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.SostenimientoId);
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionSostenimiento.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.SistemaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find PerfilEducacionSostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoFind, "", 
                    perfilEducacionSostenimiento.SostenimientoId.ToString(),
                    perfilEducacionSostenimiento.PerfilEducacionId.ToString(),
                    perfilEducacionSostenimiento.NiveltrabajoId.ToString(),
                    perfilEducacionSostenimiento.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindPerfilEducacionSostenimiento);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete PerfilEducacionSostenimiento
            DbCommand com = con.CreateCommand();
            String deletePerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sostenimiento",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Id_Sistema");
            com.CommandText = deletePerfilEducacionSostenimiento;
            #endregion
            #region Parametros Delete PerfilEducacionSostenimiento
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.SostenimientoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionSostenimiento.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.NiveltrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.SistemaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete PerfilEducacionSostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeletePerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoDelete, "", 
                    perfilEducacionSostenimiento.SostenimientoId.ToString(),
                    perfilEducacionSostenimiento.PerfilEducacionId.ToString(),
                    perfilEducacionSostenimiento.NiveltrabajoId.ToString(),
                    perfilEducacionSostenimiento.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeletePerfilEducacionSostenimiento);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update PerfilEducacionSostenimiento
            DbCommand com = con.CreateCommand();
            String updatePerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Sostenimiento",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Id_Sistema");
            com.CommandText = updatePerfilEducacionSostenimiento;
            #endregion
            #region Parametros Update PerfilEducacionSostenimiento
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacionSostenimiento.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfilEducacionSostenimiento.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfilEducacionSostenimiento.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.SostenimientoId);
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionSostenimiento.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionSostenimiento.SistemaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update PerfilEducacionSostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdatePerfilEducacionSostenimiento = String.Format(PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoUpdate, "", 
                    perfilEducacionSostenimiento.BitActivo.ToString(),
                    perfilEducacionSostenimiento.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(perfilEducacionSostenimiento.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    perfilEducacionSostenimiento.SostenimientoId.ToString(),
                    perfilEducacionSostenimiento.PerfilEducacionId.ToString(),
                    perfilEducacionSostenimiento.NiveltrabajoId.ToString(),
                    perfilEducacionSostenimiento.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdatePerfilEducacionSostenimiento);
                #endregion
            }
            return resultado;
        }
        protected static PerfilEducacionSostenimientoDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilEducacionSostenimientoDP perfilEducacionSostenimiento = new PerfilEducacionSostenimientoDP();
            perfilEducacionSostenimiento.SostenimientoId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            perfilEducacionSostenimiento.PerfilEducacionId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            perfilEducacionSostenimiento.NiveltrabajoId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            perfilEducacionSostenimiento.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            perfilEducacionSostenimiento.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            perfilEducacionSostenimiento.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            perfilEducacionSostenimiento.SistemaId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            return perfilEducacionSostenimiento;
            #endregion
        }
        public static PerfilEducacionSostenimientoDP Load(DbConnection con, DbTransaction tran, PerfilEducacionSostenimientoPk pk)
        {
            #region SQL Load PerfilEducacionSostenimiento
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sostenimiento",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Id_Sistema");
            com.CommandText = loadPerfilEducacionSostenimiento;
            #endregion
            #region Parametros Load PerfilEducacionSostenimiento
            Common.CreateParameter(com, String.Format("{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SostenimientoId);
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            #endregion
            PerfilEducacionSostenimientoDP perfilEducacionSostenimiento;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load PerfilEducacionSostenimiento
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        perfilEducacionSostenimiento = ReadRow(dr);
                    }
                    else
                        perfilEducacionSostenimiento = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load PerfilEducacionSostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoSelect, "", 
                    pk.SostenimientoId.ToString(),
                    pk.PerfilEducacionId.ToString(),
                    pk.NiveltrabajoId.ToString(),
                    pk.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadPerfilEducacionSostenimiento);
                perfilEducacionSostenimiento = null;
                #endregion
            }
            return perfilEducacionSostenimiento;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilEducacionSostenimiento
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoCount,"");
            com.CommandText = countPerfilEducacionSostenimiento;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacionSostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPerfilEducacionSostenimiento);
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionSostenimientoDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilEducacionSostenimiento
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture, PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoSelectAll, "", ConstantesGlobales.IncluyeRows);
            listPerfilEducacionSostenimiento = listPerfilEducacionSostenimiento.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPerfilEducacionSostenimiento, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionSostenimiento
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionSostenimiento
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionSostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPerfilEducacionSostenimiento = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoResx.PerfilEducacionSostenimientoSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPerfilEducacionSostenimiento);
                #endregion
            }
            return (PerfilEducacionSostenimientoDP[])lista.ToArray(typeof(PerfilEducacionSostenimientoDP));
        }
    }
}
