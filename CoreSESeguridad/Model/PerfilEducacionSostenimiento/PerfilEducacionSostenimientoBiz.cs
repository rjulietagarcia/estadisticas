using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class PerfilEducacionSostenimientoBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, PerfilEducacionSostenimientoDP perfilEducacionSostenimiento)
        {
            #region SaveDetail
            #region FK Sostenimiento
            SostenimientoDP sostenimiento = perfilEducacionSostenimiento.Sostenimiento;
            if (sostenimiento != null) 
            {
                SostenimientoMD sostenimientoMd = new SostenimientoMD(sostenimiento);
                if (!sostenimientoMd.Find(con, tran))
                    sostenimientoMd.Insert(con, tran);
            }
            #endregion
            #region FK PerfilEducacion
            PerfilEducacionDP perfilEducacion = perfilEducacionSostenimiento.PerfilEducacion;
            if (perfilEducacion != null) 
            {
                PerfilEducacionMD perfilEducacionMd = new PerfilEducacionMD(perfilEducacion);
                if (!perfilEducacionMd.Find(con, tran))
                    perfilEducacionMd.Insert(con, tran);
            }
            #endregion
            #region FK Sistema
            SistemaDP sistema = perfilEducacionSostenimiento.Sistema;
            if (sistema != null) 
            {
                SistemaMD sistemaMd = new SistemaMD(sistema);
                if (!sistemaMd.Find(con, tran))
                    sistemaMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, PerfilEducacionSostenimientoDP perfilEducacionSostenimiento)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, perfilEducacionSostenimiento);
                        resultado = InternalSave(con, tran, perfilEducacionSostenimiento);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla perfilEducacionSostenimiento!";
                        throw new BusinessException("Error interno al intentar guardar perfilEducacionSostenimiento!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla perfilEducacionSostenimiento!";
                throw new BusinessException("Error interno al intentar guardar perfilEducacionSostenimiento!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, PerfilEducacionSostenimientoDP perfilEducacionSostenimiento)
        {
            PerfilEducacionSostenimientoMD perfilEducacionSostenimientoMd = new PerfilEducacionSostenimientoMD(perfilEducacionSostenimiento);
            String resultado = "";
            if (perfilEducacionSostenimientoMd.Find(con, tran))
            {
                PerfilEducacionSostenimientoDP perfilEducacionSostenimientoAnterior = PerfilEducacionSostenimientoMD.Load(con, tran, perfilEducacionSostenimiento.Pk);
                if (perfilEducacionSostenimientoMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla perfilEducacionSostenimiento!";
                    throw new BusinessException("No se pude actualizar la tabla perfilEducacionSostenimiento!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (perfilEducacionSostenimientoMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla perfilEducacionSostenimiento!";
                    throw new BusinessException("No se pude insertar en la tabla perfilEducacionSostenimiento!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, PerfilEducacionSostenimientoDP perfilEducacionSostenimiento)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, perfilEducacionSostenimiento);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla perfilEducacionSostenimiento!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla perfilEducacionSostenimiento!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla perfilEducacionSostenimiento!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla perfilEducacionSostenimiento!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, PerfilEducacionSostenimientoDP perfilEducacionSostenimiento)
        {
            String resultado = "";
            PerfilEducacionSostenimientoMD perfilEducacionSostenimientoMd = new PerfilEducacionSostenimientoMD(perfilEducacionSostenimiento);
            if (perfilEducacionSostenimientoMd.Find(con, tran))
            {
                if (perfilEducacionSostenimientoMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla perfilEducacionSostenimiento!";
                    throw new BusinessException("No se pude eliminar en la tabla perfilEducacionSostenimiento!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla perfilEducacionSostenimiento!";
                throw new BusinessException("No se pude eliminar en la tabla perfilEducacionSostenimiento!");
            }
            return resultado;
        }

        internal static PerfilEducacionSostenimientoDP LoadDetail(DbConnection con, DbTransaction tran, PerfilEducacionSostenimientoDP perfilEducacionSostenimientoAnterior, PerfilEducacionSostenimientoDP perfilEducacionSostenimiento) 
        {
            #region FK Sostenimiento
            SostenimientoPk sostenimientoPk = new SostenimientoPk();
            if (perfilEducacionSostenimientoAnterior != null)
            {
                if (sostenimientoPk.Equals(perfilEducacionSostenimientoAnterior.Pk))
                    perfilEducacionSostenimiento.Sostenimiento = perfilEducacionSostenimientoAnterior.Sostenimiento;
                else
                    perfilEducacionSostenimiento.Sostenimiento = SostenimientoMD.Load(con, tran, sostenimientoPk);
            }
            else
                perfilEducacionSostenimiento.Sostenimiento = SostenimientoMD.Load(con, tran, sostenimientoPk);
            #endregion
            #region FK PerfilEducacion
            PerfilEducacionPk perfilEducacionPk = new PerfilEducacionPk();
            if (perfilEducacionSostenimientoAnterior != null)
            {
                if (perfilEducacionPk.Equals(perfilEducacionSostenimientoAnterior.Pk))
                    perfilEducacionSostenimiento.PerfilEducacion = perfilEducacionSostenimientoAnterior.PerfilEducacion;
                else
                    perfilEducacionSostenimiento.PerfilEducacion = PerfilEducacionMD.Load(con, tran, perfilEducacionPk);
            }
            else
                perfilEducacionSostenimiento.PerfilEducacion = PerfilEducacionMD.Load(con, tran, perfilEducacionPk);
            #endregion
            #region FK Sistema
            SistemaPk sistemaPk = new SistemaPk();
            if (perfilEducacionSostenimientoAnterior != null)
            {
                if (sistemaPk.Equals(perfilEducacionSostenimientoAnterior.Pk))
                    perfilEducacionSostenimiento.Sistema = perfilEducacionSostenimientoAnterior.Sistema;
                else
                    perfilEducacionSostenimiento.Sistema = SistemaMD.Load(con, tran, sistemaPk);
            }
            else
                perfilEducacionSostenimiento.Sistema = SistemaMD.Load(con, tran, sistemaPk);
            #endregion
            return perfilEducacionSostenimiento;
        }
        public static PerfilEducacionSostenimientoDP Load(DbConnection con, PerfilEducacionSostenimientoPk pk)
        {
            PerfilEducacionSostenimientoDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionSostenimientoMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un PerfilEducacionSostenimiento!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilEducacionSostenimientoMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilEducacionSostenimiento!", ex);
            }
            return resultado;
        }

        public static PerfilEducacionSostenimientoDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PerfilEducacionSostenimientoDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionSostenimientoMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilEducacionSostenimiento!", ex);
            }
            return resultado;

        }
    }
}
