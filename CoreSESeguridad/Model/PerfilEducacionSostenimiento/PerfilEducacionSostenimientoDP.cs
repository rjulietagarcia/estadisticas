using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilEducacionSostenimiento".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilEducacionSostenimiento".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Sostenimiento</term><description>Descripcion Sostenimiento</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacion</term><description>Descripcion PerfilEducacion</description>
    ///    </item>
    ///    <item>
    ///        <term>Sistema</term><description>Descripcion Sistema</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilEducacionSostenimientoDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// PerfilEducacionSostenimientoDTO perfileducacionsostenimiento = new PerfilEducacionSostenimientoDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("PerfilEducacionSostenimiento")]
    public class PerfilEducacionSostenimientoDP
    {
        #region Definicion de campos privados.
        private Byte sostenimientoId;
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Byte sistemaId;
        private SostenimientoDP sostenimiento;
        private PerfilEducacionDP perfilEducacion;
        private SistemaDP sistema;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// Sostenimiento
        /// </summary> 
        [XmlElement("Sostenimiento")]
        public SostenimientoDP Sostenimiento
        {
            get {
                    return sostenimiento; 
            }
            set {
                    sostenimiento = value; 
            }
        }

        /// <summary>
        /// PerfilEducacion
        /// </summary> 
        [XmlElement("PerfilEducacion")]
        public PerfilEducacionDP PerfilEducacion
        {
            get {
                    return perfilEducacion; 
            }
            set {
                    perfilEducacion = value; 
            }
        }

        /// <summary>
        /// Sistema
        /// </summary> 
        [XmlElement("Sistema")]
        public SistemaDP Sistema
        {
            get {
                    return sistema; 
            }
            set {
                    sistema = value; 
            }
        }

        /// <summary>
        /// Llave primaria de PerfilEducacionSostenimientoPk
        /// </summary>
        [XmlElement("Pk")]
        public PerfilEducacionSostenimientoPk Pk {
            get {
                    return new PerfilEducacionSostenimientoPk( sostenimientoId, perfilEducacionId, niveltrabajoId, sistemaId );
            }
        }
        #endregion.
    }
}
