using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class ModuloBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, ModuloDP modulo)
        {
            #region SaveDetail
            #region FK Sistema
            SistemaDP sistema = modulo.Sistema;
            if (sistema != null) 
            {
                SistemaMD sistemaMd = new SistemaMD(sistema);
                if (!sistemaMd.Find(con, tran))
                    sistemaMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, ModuloDP modulo)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, modulo);
                        resultado = InternalSave(con, tran, modulo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla modulo!";
                        throw new BusinessException("Error interno al intentar guardar modulo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla modulo!";
                throw new BusinessException("Error interno al intentar guardar modulo!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, ModuloDP modulo)
        {
            ModuloMD moduloMd = new ModuloMD(modulo);
            String resultado = "";
            if (moduloMd.Find(con, tran))
            {
                ModuloDP moduloAnterior = ModuloMD.Load(con, tran, modulo.Pk);
                if (moduloMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla modulo!";
                    throw new BusinessException("No se pude actualizar la tabla modulo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (moduloMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla modulo!";
                    throw new BusinessException("No se pude insertar en la tabla modulo!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, ModuloDP modulo)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, modulo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla modulo!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla modulo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla modulo!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla modulo!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, ModuloDP modulo)
        {
            String resultado = "";
            ModuloMD moduloMd = new ModuloMD(modulo);
            if (moduloMd.Find(con, tran))
            {
                if (moduloMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla modulo!";
                    throw new BusinessException("No se pude eliminar en la tabla modulo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla modulo!";
                throw new BusinessException("No se pude eliminar en la tabla modulo!");
            }
            return resultado;
        }

        internal static ModuloDP LoadDetail(DbConnection con, DbTransaction tran, ModuloDP moduloAnterior, ModuloDP modulo) 
        {
            #region FK Sistema
            SistemaPk sistemaPk = new SistemaPk();
            if (moduloAnterior != null)
            {
                if (sistemaPk.Equals(moduloAnterior.Pk))
                    modulo.Sistema = moduloAnterior.Sistema;
                else
                    modulo.Sistema = SistemaMD.Load(con, tran, sistemaPk);
            }
            else
                modulo.Sistema = SistemaMD.Load(con, tran, sistemaPk);
            #endregion
            return modulo;
        }
        public static ModuloDP Load(DbConnection con, ModuloPk pk)
        {
            ModuloDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ModuloMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Modulo!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ModuloMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Modulo!", ex);
            }
            return resultado;
        }

        public static ModuloDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            ModuloDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ModuloMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Modulo!", ex);
            }
            return resultado;

        }
    }
}
