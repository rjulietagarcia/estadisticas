using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "ModuloPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: viernes, 29 de mayo de 2009.</Para>
    /// <Para>Hora: 10:02:32 a.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "ModuloPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "ModuloPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// ModuloPk moduloPk = new ModuloPk(modulo);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("ModuloPk")]
    public class ModuloPk
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Byte moduloId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de ModuloPk.
        /// </summary>
        /// <param name="sistemaId">Descripción sistemaId del tipo Byte.</param>
        /// <param name="moduloId">Descripción moduloId del tipo Byte.</param>
        public ModuloPk(Byte sistemaId, Byte moduloId) 
        {
            this.sistemaId = sistemaId;
            this.moduloId = moduloId;
        }
        /// <summary>
        /// Constructor normal de ModuloPk.
        /// </summary>
        public ModuloPk() 
        {
        }
        #endregion.
    }
}
