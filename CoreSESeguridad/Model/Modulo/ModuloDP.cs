using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Modulo".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: viernes, 29 de mayo de 2009.</Para>
    /// <Para>Hora: 10:02:32 a.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Modulo".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>Abreviatura</term><description>Descripcion Abreviatura</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaInicio</term><description>Descripcion FechaInicio</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaFin</term><description>Descripcion FechaFin</description>
    ///    </item>
    ///    <item>
    ///        <term>CarpetaModulo</term><description>Descripcion CarpetaModulo</description>
    ///    </item>
    ///    <item>
    ///        <term>Sistema</term><description>Descripcion Sistema</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "ModuloDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// ModuloDTO modulo = new ModuloDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Modulo")]
    public class ModuloDP
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Byte moduloId;
        private String nombre;
        private String abreviatura;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private String fechaInicio;
        private String fechaFin;
        private String carpetaModulo;
        private SistemaDP sistema;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// Abreviatura
        /// </summary> 
        [XmlElement("Abreviatura")]
        public String Abreviatura
        {
            get {
                    return abreviatura; 
            }
            set {
                    abreviatura = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// FechaInicio
        /// </summary> 
        [XmlElement("FechaInicio")]
        public String FechaInicio
        {
            get {
                    return fechaInicio; 
            }
            set {
                    fechaInicio = value; 
            }
        }

        /// <summary>
        /// FechaFin
        /// </summary> 
        [XmlElement("FechaFin")]
        public String FechaFin
        {
            get {
                    return fechaFin; 
            }
            set {
                    fechaFin = value; 
            }
        }

        /// <summary>
        /// CarpetaModulo
        /// </summary> 
        [XmlElement("CarpetaModulo")]
        public String CarpetaModulo
        {
            get {
                    return carpetaModulo; 
            }
            set {
                    carpetaModulo = value; 
            }
        }

        /// <summary>
        /// Sistema
        /// </summary> 
        [XmlElement("Sistema")]
        public SistemaDP Sistema
        {
            get {
                    return sistema; 
            }
            set {
                    sistema = value; 
            }
        }

        /// <summary>
        /// Llave primaria de ModuloPk
        /// </summary>
        [XmlElement("Pk")]
        public ModuloPk Pk {
            get {
                    return new ModuloPk( sistemaId, moduloId );
            }
        }
        #endregion.
    }
}
