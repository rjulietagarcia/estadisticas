using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class ModuloMD
    {
        private ModuloDP modulo = null;

        public ModuloMD(ModuloDP modulo)
        {
            this.modulo = modulo;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Modulo
            DbCommand com = con.CreateCommand();
            String insertModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Carpeta_Modulo");
            com.CommandText = insertModulo;
            #endregion
            #region Parametros Insert Modulo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, modulo.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, modulo.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, modulo.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, modulo.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, modulo.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, modulo.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(modulo.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(modulo.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(modulo.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Carpeta_Modulo",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 255, ParameterDirection.Input, modulo.CarpetaModulo);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Modulo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertModulo = String.Format(Modulo.ModuloResx.ModuloInsert, "", 
                    modulo.SistemaId.ToString(),
                    modulo.ModuloId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,modulo.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,modulo.Abreviatura),
                    modulo.BitActivo.ToString(),
                    modulo.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(modulo.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(modulo.FechaInicio).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(modulo.FechaFin).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,modulo.CarpetaModulo));
                System.Diagnostics.Debug.WriteLine(errorInsertModulo);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Modulo
            DbCommand com = con.CreateCommand();
            String findModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo");
            com.CommandText = findModulo;
            #endregion
            #region Parametros Find Modulo
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, modulo.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, modulo.ModuloId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Modulo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloFind, "", 
                    modulo.SistemaId.ToString(),
                    modulo.ModuloId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindModulo);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Modulo
            DbCommand com = con.CreateCommand();
            String deleteModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo");
            com.CommandText = deleteModulo;
            #endregion
            #region Parametros Delete Modulo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, modulo.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, modulo.ModuloId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Modulo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloDelete, "", 
                    modulo.SistemaId.ToString(),
                    modulo.ModuloId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteModulo);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Modulo
            DbCommand com = con.CreateCommand();
            String updateModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Carpeta_Modulo",
                        "Id_Sistema",
                        "Id_Modulo");
            com.CommandText = updateModulo;
            #endregion
            #region Parametros Update Modulo
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, modulo.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, modulo.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, modulo.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, modulo.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(modulo.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(modulo.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(modulo.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Carpeta_Modulo",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 255, ParameterDirection.Input, modulo.CarpetaModulo);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, modulo.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, modulo.ModuloId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Modulo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateModulo = String.Format(Modulo.ModuloResx.ModuloUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,modulo.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,modulo.Abreviatura),
                    modulo.BitActivo.ToString(),
                    modulo.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(modulo.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(modulo.FechaInicio).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(modulo.FechaFin).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteString,modulo.CarpetaModulo),
                    modulo.SistemaId.ToString(),
                    modulo.ModuloId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateModulo);
                #endregion
            }
            return resultado;
        }
        protected static ModuloDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ModuloDP modulo = new ModuloDP();
            modulo.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            modulo.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            modulo.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            modulo.Abreviatura = dr.IsDBNull(3) ? "" : dr.GetString(3);
            modulo.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            modulo.UsuarioId = dr.IsDBNull(5) ? 0 : dr.GetInt32(5);
            modulo.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            modulo.FechaInicio = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            modulo.FechaFin = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            modulo.CarpetaModulo = dr.IsDBNull(9) ? "" : dr.GetString(9);
            return modulo;
            #endregion
        }
        public static ModuloDP Load(DbConnection con, DbTransaction tran, ModuloPk pk)
        {
            #region SQL Load Modulo
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo");
            com.CommandText = loadModulo;
            #endregion
            #region Parametros Load Modulo
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.ModuloId);
            #endregion
            ModuloDP modulo;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Modulo
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        modulo = ReadRow(dr);
                    }
                    else
                        modulo = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Modulo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloSelect, "", 
                    pk.SistemaId.ToString(),
                    pk.ModuloId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadModulo);
                modulo = null;
                #endregion
            }
            return modulo;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Modulo
            DbCommand com = con.CreateCommand();
            String countModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloCount,"");
            com.CommandText = countModulo;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Modulo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountModulo);
                #endregion
            }
            return resultado;
        }
        public static ModuloDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Modulo
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listModulo = String.Format(CultureInfo.CurrentCulture, Modulo.ModuloResx.ModuloSelectAll, "", ConstantesGlobales.IncluyeRows);
            listModulo = listModulo.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listModulo, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Modulo
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Modulo
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Modulo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloResx.ModuloSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaModulo);
                #endregion
            }
            return (ModuloDP[])lista.ToArray(typeof(ModuloDP));
        }
    }
}
