using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class ColoniaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, ColoniaDP colonia)
        {
            #region SaveDetail
            #region FK Municipio
            MunicipioDP municipio = colonia.Municipio;
            if (municipio != null) 
            {
                MunicipioMD municipioMd = new MunicipioMD(municipio);
                if (!municipioMd.Find(con, tran))
                    municipioMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, ColoniaDP colonia)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, colonia);
                        resultado = InternalSave(con, tran, colonia);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla colonia!";
                        throw new BusinessException("Error interno al intentar guardar colonia!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla colonia!";
                throw new BusinessException("Error interno al intentar guardar colonia!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, ColoniaDP colonia)
        {
            ColoniaMD coloniaMd = new ColoniaMD(colonia);
            String resultado = "";
            if (coloniaMd.Find(con, tran))
            {
                ColoniaDP coloniaAnterior = ColoniaMD.Load(con, tran, colonia.Pk);
                if (coloniaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla colonia!";
                    throw new BusinessException("No se pude actualizar la tabla colonia!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (coloniaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla colonia!";
                    throw new BusinessException("No se pude insertar en la tabla colonia!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, ColoniaDP colonia)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, colonia);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla colonia!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla colonia!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla colonia!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla colonia!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, ColoniaDP colonia)
        {
            String resultado = "";
            ColoniaMD coloniaMd = new ColoniaMD(colonia);
            if (coloniaMd.Find(con, tran))
            {
                if (coloniaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla colonia!";
                    throw new BusinessException("No se pude eliminar en la tabla colonia!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla colonia!";
                throw new BusinessException("No se pude eliminar en la tabla colonia!");
            }
            return resultado;
        }

        internal static ColoniaDP LoadDetail(DbConnection con, DbTransaction tran, ColoniaDP coloniaAnterior, ColoniaDP colonia) 
        {
            #region FK Municipio
            MunicipioPk municipioPk = new MunicipioPk();
            if (coloniaAnterior != null)
            {
                if (municipioPk.Equals(coloniaAnterior.Pk))
                    colonia.Municipio = coloniaAnterior.Municipio;
                else
                    colonia.Municipio = MunicipioMD.Load(con, tran, municipioPk);
            }
            else
                colonia.Municipio = MunicipioMD.Load(con, tran, municipioPk);
            #endregion
            return colonia;
        }
        public static ColoniaDP Load(DbConnection con, ColoniaPk pk)
        {
            ColoniaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ColoniaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Colonia!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ColoniaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Colonia!", ex);
            }
            return resultado;
        }

        public static ColoniaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            ColoniaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ColoniaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Colonia!", ex);
            }
            return resultado;

        }
    }
}
