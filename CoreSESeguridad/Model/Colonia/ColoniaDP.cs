using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Colonia".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:34:57 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Colonia".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>MunicipioId</term><description>Descripcion MunicipioId</description>
    ///    </item>
    ///    <item>
    ///        <term>ColoniaId</term><description>Descripcion ColoniaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>CodigoPostal</term><description>Descripcion CodigoPostal</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>LocalidadId</term><description>Descripcion LocalidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>Municipio</term><description>Descripcion Municipio</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "ColoniaDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// ColoniaDTO colonia = new ColoniaDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Colonia")]
    public class ColoniaDP
    {
        #region Definicion de campos privados.
        private Int16 paisId;
        private Int16 entidadId;
        private Int16 municipioId;
        private Int16 coloniaId;
        private String nombre;
        private String codigoPostal;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Int16 localidadId;
        private MunicipioDP municipio;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// MunicipioId
        /// </summary> 
        [XmlElement("MunicipioId")]
        public Int16 MunicipioId
        {
            get {
                    return municipioId; 
            }
            set {
                    municipioId = value; 
            }
        }

        /// <summary>
        /// ColoniaId
        /// </summary> 
        [XmlElement("ColoniaId")]
        public Int16 ColoniaId
        {
            get {
                    return coloniaId; 
            }
            set {
                    coloniaId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// CodigoPostal
        /// </summary> 
        [XmlElement("CodigoPostal")]
        public String CodigoPostal
        {
            get {
                    return codigoPostal; 
            }
            set {
                    codigoPostal = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// LocalidadId
        /// </summary> 
        [XmlElement("LocalidadId")]
        public Int16 LocalidadId
        {
            get {
                    return localidadId; 
            }
            set {
                    localidadId = value; 
            }
        }

        /// <summary>
        /// Municipio
        /// </summary> 
        [XmlElement("Municipio")]
        public MunicipioDP Municipio
        {
            get {
                    return municipio; 
            }
            set {
                    municipio = value; 
            }
        }

        /// <summary>
        /// Llave primaria de ColoniaPk
        /// </summary>
        [XmlElement("Pk")]
        public ColoniaPk Pk {
            get {
                    return new ColoniaPk( paisId, entidadId, municipioId, coloniaId );
            }
        }
        #endregion.
    }
}
