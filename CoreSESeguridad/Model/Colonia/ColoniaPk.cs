using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "ColoniaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:34:57 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "ColoniaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>MunicipioId</term><description>Descripcion MunicipioId</description>
    ///    </item>
    ///    <item>
    ///        <term>ColoniaId</term><description>Descripcion ColoniaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "ColoniaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// ColoniaPk coloniaPk = new ColoniaPk(colonia);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("ColoniaPk")]
    public class ColoniaPk
    {
        #region Definicion de campos privados.
        private Int16 paisId;
        private Int16 entidadId;
        private Int16 municipioId;
        private Int16 coloniaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// MunicipioId
        /// </summary> 
        [XmlElement("MunicipioId")]
        public Int16 MunicipioId
        {
            get {
                    return municipioId; 
            }
            set {
                    municipioId = value; 
            }
        }

        /// <summary>
        /// ColoniaId
        /// </summary> 
        [XmlElement("ColoniaId")]
        public Int16 ColoniaId
        {
            get {
                    return coloniaId; 
            }
            set {
                    coloniaId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de ColoniaPk.
        /// </summary>
        /// <param name="paisId">Descripción paisId del tipo Int16.</param>
        /// <param name="entidadId">Descripción entidadId del tipo Int16.</param>
        /// <param name="municipioId">Descripción municipioId del tipo Int16.</param>
        /// <param name="coloniaId">Descripción coloniaId del tipo Int16.</param>
        public ColoniaPk(Int16 paisId, Int16 entidadId, Int16 municipioId, Int16 coloniaId) 
        {
            this.paisId = paisId;
            this.entidadId = entidadId;
            this.municipioId = municipioId;
            this.coloniaId = coloniaId;
        }
        /// <summary>
        /// Constructor normal de ColoniaPk.
        /// </summary>
        public ColoniaPk() 
        {
        }
        #endregion.
    }
}
