using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class ColoniaMD
    {
        private ColoniaDP colonia = null;

        public ColoniaMD(ColoniaDP colonia)
        {
            this.colonia = colonia;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Colonia
            DbCommand com = con.CreateCommand();
            String insertColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Colonia",
                        "Nombre",
                        "Codigo_Postal",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Localidad");
            com.CommandText = insertColonia;
            #endregion
            #region Parametros Insert Colonia
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.MunicipioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Colonia",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.ColoniaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, colonia.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Codigo_Postal",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 5, ParameterDirection.Input, colonia.CodigoPostal);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, colonia.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, colonia.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(colonia.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.LocalidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Colonia
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertColonia = String.Format(Colonia.ColoniaResx.ColoniaInsert, "", 
                    colonia.PaisId.ToString(),
                    colonia.EntidadId.ToString(),
                    colonia.MunicipioId.ToString(),
                    colonia.ColoniaId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,colonia.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,colonia.CodigoPostal),
                    colonia.BitActivo.ToString(),
                    colonia.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(colonia.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    colonia.LocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorInsertColonia);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Colonia
            DbCommand com = con.CreateCommand();
            String findColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Colonia");
            com.CommandText = findColonia;
            #endregion
            #region Parametros Find Colonia
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.MunicipioId);
            Common.CreateParameter(com, String.Format("{0}Id_Colonia",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.ColoniaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Colonia
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaFind, "", 
                    colonia.PaisId.ToString(),
                    colonia.EntidadId.ToString(),
                    colonia.MunicipioId.ToString(),
                    colonia.ColoniaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindColonia);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Colonia
            DbCommand com = con.CreateCommand();
            String deleteColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Colonia");
            com.CommandText = deleteColonia;
            #endregion
            #region Parametros Delete Colonia
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.MunicipioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Colonia",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.ColoniaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Colonia
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaDelete, "", 
                    colonia.PaisId.ToString(),
                    colonia.EntidadId.ToString(),
                    colonia.MunicipioId.ToString(),
                    colonia.ColoniaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteColonia);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Colonia
            DbCommand com = con.CreateCommand();
            String updateColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Codigo_Postal",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Localidad",
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Colonia");
            com.CommandText = updateColonia;
            #endregion
            #region Parametros Update Colonia
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, colonia.Nombre);
            Common.CreateParameter(com, String.Format("{0}Codigo_Postal",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 5, ParameterDirection.Input, colonia.CodigoPostal);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, colonia.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, colonia.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(colonia.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.LocalidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.MunicipioId);
            Common.CreateParameter(com, String.Format("{0}Id_Colonia",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, colonia.ColoniaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Colonia
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateColonia = String.Format(Colonia.ColoniaResx.ColoniaUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,colonia.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,colonia.CodigoPostal),
                    colonia.BitActivo.ToString(),
                    colonia.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(colonia.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    colonia.LocalidadId.ToString(),
                    colonia.PaisId.ToString(),
                    colonia.EntidadId.ToString(),
                    colonia.MunicipioId.ToString(),
                    colonia.ColoniaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateColonia);
                #endregion
            }
            return resultado;
        }
        protected static ColoniaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ColoniaDP colonia = new ColoniaDP();
            colonia.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            colonia.EntidadId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            colonia.MunicipioId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            colonia.ColoniaId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            colonia.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            colonia.CodigoPostal = dr.IsDBNull(5) ? "" : dr.GetString(5);
            colonia.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            colonia.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            colonia.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            colonia.LocalidadId = dr.IsDBNull(9) ? (short)0 : dr.GetInt16(9);
            return colonia;
            #endregion
        }
        public static ColoniaDP Load(DbConnection con, DbTransaction tran, ColoniaPk pk)
        {
            #region SQL Load Colonia
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Colonia");
            com.CommandText = loadColonia;
            #endregion
            #region Parametros Load Colonia
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.MunicipioId);
            Common.CreateParameter(com, String.Format("{0}Id_Colonia",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.ColoniaId);
            #endregion
            ColoniaDP colonia;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Colonia
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        colonia = ReadRow(dr);
                    }
                    else
                        colonia = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Colonia
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaSelect, "", 
                    pk.PaisId.ToString(),
                    pk.EntidadId.ToString(),
                    pk.MunicipioId.ToString(),
                    pk.ColoniaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadColonia);
                colonia = null;
                #endregion
            }
            return colonia;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Colonia
            DbCommand com = con.CreateCommand();
            String countColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaCount,"");
            com.CommandText = countColonia;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Colonia
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountColonia);
                #endregion
            }
            return resultado;
        }
        public static ColoniaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Colonia
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listColonia = String.Format(CultureInfo.CurrentCulture, Colonia.ColoniaResx.ColoniaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listColonia = listColonia.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listColonia, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Colonia
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Colonia
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Colonia
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaColonia = String.Format(CultureInfo.CurrentCulture,Colonia.ColoniaResx.ColoniaSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaColonia);
                #endregion
            }
            return (ColoniaDP[])lista.ToArray(typeof(ColoniaDP));
        }
    }
}
