using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class OpcionMD
    {
        private OpcionDP opcion = null;

        public OpcionMD(OpcionDP opcion)
        {
            this.opcion = opcion;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Opcion
            DbCommand com = con.CreateCommand();
            String insertOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Carpeta_Opcion");
            com.CommandText = insertOpcion;
            #endregion
            #region Parametros Insert Opcion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, opcion.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, opcion.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, opcion.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, opcion.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(opcion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(opcion.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(opcion.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Carpeta_Opcion",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 255, ParameterDirection.Input, opcion.CarpetaOpcion);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Opcion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertOpcion = String.Format(Opcion.OpcionResx.OpcionInsert, "", 
                    opcion.SistemaId.ToString(),
                    opcion.ModuloId.ToString(),
                    opcion.OpcionId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,opcion.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,opcion.Abreviatura),
                    opcion.BitActivo.ToString(),
                    opcion.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(opcion.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(opcion.FechaInicio).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(opcion.FechaFin).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,opcion.CarpetaOpcion));
                System.Diagnostics.Debug.WriteLine(errorInsertOpcion);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Opcion
            DbCommand com = con.CreateCommand();
            String findOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion");
            com.CommandText = findOpcion;
            #endregion
            #region Parametros Find Opcion
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.OpcionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Opcion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionFind, "", 
                    opcion.SistemaId.ToString(),
                    opcion.ModuloId.ToString(),
                    opcion.OpcionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindOpcion);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Opcion
            DbCommand com = con.CreateCommand();
            String deleteOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion");
            com.CommandText = deleteOpcion;
            #endregion
            #region Parametros Delete Opcion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.OpcionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Opcion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionDelete, "", 
                    opcion.SistemaId.ToString(),
                    opcion.ModuloId.ToString(),
                    opcion.OpcionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteOpcion);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Opcion
            DbCommand com = con.CreateCommand();
            String updateOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Carpeta_Opcion",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion");
            com.CommandText = updateOpcion;
            #endregion
            #region Parametros Update Opcion
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, opcion.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, opcion.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, opcion.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, opcion.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(opcion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(opcion.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(opcion.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Carpeta_Opcion",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 255, ParameterDirection.Input, opcion.CarpetaOpcion);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, opcion.OpcionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Opcion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateOpcion = String.Format(Opcion.OpcionResx.OpcionUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,opcion.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,opcion.Abreviatura),
                    opcion.BitActivo.ToString(),
                    opcion.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(opcion.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(opcion.FechaInicio).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(opcion.FechaFin).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteString,opcion.CarpetaOpcion),
                    opcion.SistemaId.ToString(),
                    opcion.ModuloId.ToString(),
                    opcion.OpcionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateOpcion);
                #endregion
            }
            return resultado;
        }
        protected static OpcionDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            OpcionDP opcion = new OpcionDP();
            opcion.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            opcion.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            opcion.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            opcion.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            opcion.Abreviatura = dr.IsDBNull(4) ? "" : dr.GetString(4);
            opcion.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            opcion.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            opcion.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            opcion.FechaInicio = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            opcion.FechaFin = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            opcion.CarpetaOpcion = dr.IsDBNull(10) ? "" : dr.GetString(10);
            return opcion;
            #endregion
        }
        public static OpcionDP Load(DbConnection con, DbTransaction tran, OpcionPk pk)
        {
            #region SQL Load Opcion
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion");
            com.CommandText = loadOpcion;
            #endregion
            #region Parametros Load Opcion
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.OpcionId);
            #endregion
            OpcionDP opcion;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Opcion
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        opcion = ReadRow(dr);
                    }
                    else
                        opcion = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Opcion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionSelect, "", 
                    pk.SistemaId.ToString(),
                    pk.ModuloId.ToString(),
                    pk.OpcionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadOpcion);
                opcion = null;
                #endregion
            }
            return opcion;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Opcion
            DbCommand com = con.CreateCommand();
            String countOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionCount,"");
            com.CommandText = countOpcion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Opcion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcion);
                #endregion
            }
            return resultado;
        }
        public static OpcionDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Opcion
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcion = String.Format(CultureInfo.CurrentCulture, Opcion.OpcionResx.OpcionSelectAll, "", ConstantesGlobales.IncluyeRows);
            listOpcion = listOpcion.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listOpcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Opcion
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Opcion
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Opcion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionResx.OpcionSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcion);
                #endregion
            }
            return (OpcionDP[])lista.ToArray(typeof(OpcionDP));
        }
    }
}
