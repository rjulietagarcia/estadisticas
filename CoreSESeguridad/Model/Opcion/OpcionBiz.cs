using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class OpcionBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, OpcionDP opcion)
        {
            #region SaveDetail
            #region FK Modulo
            ModuloDP modulo = opcion.Modulo;
            if (modulo != null) 
            {
                ModuloMD moduloMd = new ModuloMD(modulo);
                if (!moduloMd.Find(con, tran))
                    moduloMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, OpcionDP opcion)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, opcion);
                        resultado = InternalSave(con, tran, opcion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla opcion!";
                        throw new BusinessException("Error interno al intentar guardar opcion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla opcion!";
                throw new BusinessException("Error interno al intentar guardar opcion!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, OpcionDP opcion)
        {
            OpcionMD opcionMd = new OpcionMD(opcion);
            String resultado = "";
            if (opcionMd.Find(con, tran))
            {
                OpcionDP opcionAnterior = OpcionMD.Load(con, tran, opcion.Pk);
                if (opcionMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla opcion!";
                    throw new BusinessException("No se pude actualizar la tabla opcion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (opcionMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla opcion!";
                    throw new BusinessException("No se pude insertar en la tabla opcion!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, OpcionDP opcion)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, opcion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla opcion!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla opcion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla opcion!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla opcion!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, OpcionDP opcion)
        {
            String resultado = "";
            OpcionMD opcionMd = new OpcionMD(opcion);
            if (opcionMd.Find(con, tran))
            {
                if (opcionMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla opcion!";
                    throw new BusinessException("No se pude eliminar en la tabla opcion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla opcion!";
                throw new BusinessException("No se pude eliminar en la tabla opcion!");
            }
            return resultado;
        }

        internal static OpcionDP LoadDetail(DbConnection con, DbTransaction tran, OpcionDP opcionAnterior, OpcionDP opcion) 
        {
            #region FK Modulo
            ModuloPk moduloPk = new ModuloPk();
            if (opcionAnterior != null)
            {
                if (moduloPk.Equals(opcionAnterior.Pk))
                    opcion.Modulo = opcionAnterior.Modulo;
                else
                    opcion.Modulo = ModuloMD.Load(con, tran, moduloPk);
            }
            else
                opcion.Modulo = ModuloMD.Load(con, tran, moduloPk);
            #endregion
            return opcion;
        }
        public static OpcionDP Load(DbConnection con, OpcionPk pk)
        {
            OpcionDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = OpcionMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Opcion!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = OpcionMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Opcion!", ex);
            }
            return resultado;
        }

        public static OpcionDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            OpcionDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = OpcionMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Opcion!", ex);
            }
            return resultado;

        }
    }
}
