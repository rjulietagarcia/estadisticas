using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Domicilio".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:32:31 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Domicilio".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>DomicilioId</term><description>Descripcion DomicilioId</description>
    ///    </item>
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>MunicipioId</term><description>Descripcion MunicipioId</description>
    ///    </item>
    ///    <item>
    ///        <term>LocalidadId</term><description>Descripcion LocalidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>ColoniaId</term><description>Descripcion ColoniaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Calle</term><description>Descripcion Calle</description>
    ///    </item>
    ///    <item>
    ///        <term>NumeroExterior</term><description>Descripcion NumeroExterior</description>
    ///    </item>
    ///    <item>
    ///        <term>NumeroInterior</term><description>Descripcion NumeroInterior</description>
    ///    </item>
    ///    <item>
    ///        <term>Piso</term><description>Descripcion Piso</description>
    ///    </item>
    ///    <item>
    ///        <term>CodigoPostal</term><description>Descripcion CodigoPostal</description>
    ///    </item>
    ///    <item>
    ///        <term>Entrecalles</term><description>Descripcion Entrecalles</description>
    ///    </item>
    ///    <item>
    ///        <term>Entrecalle</term><description>Descripcion Entrecalle</description>
    ///    </item>
    ///    <item>
    ///        <term>Ycalle</term><description>Descripcion Ycalle</description>
    ///    </item>
    ///    <item>
    ///        <term>Calleposterior</term><description>Descripcion Calleposterior</description>
    ///    </item>
    ///    <item>
    ///        <term>X</term><description>Descripcion X</description>
    ///    </item>
    ///    <item>
    ///        <term>Y</term><description>Descripcion Y</description>
    ///    </item>
    ///    <item>
    ///        <term>Latitud</term><description>Descripcion Latitud</description>
    ///    </item>
    ///    <item>
    ///        <term>Longitud</term><description>Descripcion Longitud</description>
    ///    </item>
    ///    <item>
    ///        <term>Altitud</term><description>Descripcion Altitud</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>Colonia</term><description>Descripcion Colonia</description>
    ///    </item>
    ///    <item>
    ///        <term>Localidad</term><description>Descripcion Localidad</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "DomicilioDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// DomicilioDTO domicilio = new DomicilioDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Domicilio")]
    public class DomicilioDP
    {
        #region Definicion de campos privados.
        private Int32 domicilioId;
        private Int16 paisId;
        private Int16 entidadId;
        private Int16 municipioId;
        private Int16 localidadId;
        private Int16 coloniaId;
        private String calle;
        private Int32 numeroExterior;
        private String numeroInterior;
        private Int16 piso;
        private String codigoPostal;
        private String entrecalles;
        private String entrecalle;
        private String ycalle;
        private String calleposterior;
        private String x;
        private String y;
        private String latitud;
        private String longitud;
        private String altitud;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private ColoniaDP colonia;
        private LocalidadDP localidad;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// DomicilioId
        /// </summary> 
        [XmlElement("DomicilioId")]
        public Int32 DomicilioId
        {
            get {
                    return domicilioId; 
            }
            set {
                    domicilioId = value; 
            }
        }

        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// MunicipioId
        /// </summary> 
        [XmlElement("MunicipioId")]
        public Int16 MunicipioId
        {
            get {
                    return municipioId; 
            }
            set {
                    municipioId = value; 
            }
        }

        /// <summary>
        /// LocalidadId
        /// </summary> 
        [XmlElement("LocalidadId")]
        public Int16 LocalidadId
        {
            get {
                    return localidadId; 
            }
            set {
                    localidadId = value; 
            }
        }

        /// <summary>
        /// ColoniaId
        /// </summary> 
        [XmlElement("ColoniaId")]
        public Int16 ColoniaId
        {
            get {
                    return coloniaId; 
            }
            set {
                    coloniaId = value; 
            }
        }

        /// <summary>
        /// Calle
        /// </summary> 
        [XmlElement("Calle")]
        public String Calle
        {
            get {
                    return calle; 
            }
            set {
                    calle = value; 
            }
        }

        /// <summary>
        /// NumeroExterior
        /// </summary> 
        [XmlElement("NumeroExterior")]
        public Int32 NumeroExterior
        {
            get {
                    return numeroExterior; 
            }
            set {
                    numeroExterior = value; 
            }
        }

        /// <summary>
        /// NumeroInterior
        /// </summary> 
        [XmlElement("NumeroInterior")]
        public String NumeroInterior
        {
            get {
                    return numeroInterior; 
            }
            set {
                    numeroInterior = value; 
            }
        }

        /// <summary>
        /// Piso
        /// </summary> 
        [XmlElement("Piso")]
        public Int16 Piso
        {
            get {
                    return piso; 
            }
            set {
                    piso = value; 
            }
        }

        /// <summary>
        /// CodigoPostal
        /// </summary> 
        [XmlElement("CodigoPostal")]
        public String CodigoPostal
        {
            get {
                    return codigoPostal; 
            }
            set {
                    codigoPostal = value; 
            }
        }

        /// <summary>
        /// Entrecalles
        /// </summary> 
        [XmlElement("Entrecalles")]
        public String Entrecalles
        {
            get {
                    return entrecalles; 
            }
            set {
                    entrecalles = value; 
            }
        }

        /// <summary>
        /// Entrecalle
        /// </summary> 
        [XmlElement("Entrecalle")]
        public String Entrecalle
        {
            get {
                    return entrecalle; 
            }
            set {
                    entrecalle = value; 
            }
        }

        /// <summary>
        /// Ycalle
        /// </summary> 
        [XmlElement("Ycalle")]
        public String Ycalle
        {
            get {
                    return ycalle; 
            }
            set {
                    ycalle = value; 
            }
        }

        /// <summary>
        /// Calleposterior
        /// </summary> 
        [XmlElement("Calleposterior")]
        public String Calleposterior
        {
            get {
                    return calleposterior; 
            }
            set {
                    calleposterior = value; 
            }
        }

        /// <summary>
        /// X
        /// </summary> 
        [XmlElement("X")]
        public String X
        {
            get {
                    return x; 
            }
            set {
                    x = value; 
            }
        }

        /// <summary>
        /// Y
        /// </summary> 
        [XmlElement("Y")]
        public String Y
        {
            get {
                    return y; 
            }
            set {
                    y = value; 
            }
        }

        /// <summary>
        /// Latitud
        /// </summary> 
        [XmlElement("Latitud")]
        public String Latitud
        {
            get {
                    return latitud; 
            }
            set {
                    latitud = value; 
            }
        }

        /// <summary>
        /// Longitud
        /// </summary> 
        [XmlElement("Longitud")]
        public String Longitud
        {
            get {
                    return longitud; 
            }
            set {
                    longitud = value; 
            }
        }

        /// <summary>
        /// Altitud
        /// </summary> 
        [XmlElement("Altitud")]
        public String Altitud
        {
            get {
                    return altitud; 
            }
            set {
                    altitud = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// Colonia
        /// </summary> 
        [XmlElement("Colonia")]
        public ColoniaDP Colonia
        {
            get {
                    return colonia; 
            }
            set {
                    colonia = value; 
            }
        }

        /// <summary>
        /// Localidad
        /// </summary> 
        [XmlElement("Localidad")]
        public LocalidadDP Localidad
        {
            get {
                    return localidad; 
            }
            set {
                    localidad = value; 
            }
        }

        /// <summary>
        /// Llave primaria de DomicilioPk
        /// </summary>
        [XmlElement("Pk")]
        public DomicilioPk Pk {
            get {
                    return new DomicilioPk( domicilioId );
            }
        }
        #endregion.
    }
}
