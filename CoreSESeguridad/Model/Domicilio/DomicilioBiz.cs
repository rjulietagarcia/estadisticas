using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class DomicilioBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, DomicilioDP domicilio)
        {
            #region SaveDetail
            #region FK Colonia
            ColoniaDP colonia = domicilio.Colonia;
            if (colonia != null) 
            {
                ColoniaMD coloniaMd = new ColoniaMD(colonia);
                if (!coloniaMd.Find(con, tran))
                    coloniaMd.Insert(con, tran);
            }
            #endregion
            #region FK Localidad
            LocalidadDP localidad = domicilio.Localidad;
            if (localidad != null) 
            {
                LocalidadMD localidadMd = new LocalidadMD(localidad);
                if (!localidadMd.Find(con, tran))
                    localidadMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, DomicilioDP domicilio)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, domicilio);
                        resultado = InternalSave(con, tran, domicilio);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla domicilio!";
                        throw new BusinessException("Error interno al intentar guardar domicilio!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla domicilio!";
                throw new BusinessException("Error interno al intentar guardar domicilio!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, DomicilioDP domicilio)
        {
            DomicilioMD domicilioMd = new DomicilioMD(domicilio);
            String resultado = "";
            if (domicilioMd.Find(con, tran))
            {
                DomicilioDP domicilioAnterior = DomicilioMD.Load(con, tran, domicilio.Pk);
                if (domicilioMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla domicilio!";
                    throw new BusinessException("No se pude actualizar la tabla domicilio!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (domicilioMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla domicilio!";
                    throw new BusinessException("No se pude insertar en la tabla domicilio!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, DomicilioDP domicilio)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, domicilio);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla domicilio!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla domicilio!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla domicilio!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla domicilio!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, DomicilioDP domicilio)
        {
            String resultado = "";
            DomicilioMD domicilioMd = new DomicilioMD(domicilio);
            if (domicilioMd.Find(con, tran))
            {
                if (domicilioMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla domicilio!";
                    throw new BusinessException("No se pude eliminar en la tabla domicilio!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla domicilio!";
                throw new BusinessException("No se pude eliminar en la tabla domicilio!");
            }
            return resultado;
        }

        internal static DomicilioDP LoadDetail(DbConnection con, DbTransaction tran, DomicilioDP domicilioAnterior, DomicilioDP domicilio) 
        {
            #region FK Colonia
            ColoniaPk coloniaPk = new ColoniaPk();
            if (domicilioAnterior != null)
            {
                if (coloniaPk.Equals(domicilioAnterior.Pk))
                    domicilio.Colonia = domicilioAnterior.Colonia;
                else
                    domicilio.Colonia = ColoniaMD.Load(con, tran, coloniaPk);
            }
            else
                domicilio.Colonia = ColoniaMD.Load(con, tran, coloniaPk);
            #endregion
            #region FK Localidad
            LocalidadPk localidadPk = new LocalidadPk();
            if (domicilioAnterior != null)
            {
                if (localidadPk.Equals(domicilioAnterior.Pk))
                    domicilio.Localidad = domicilioAnterior.Localidad;
                else
                    domicilio.Localidad = LocalidadMD.Load(con, tran, localidadPk);
            }
            else
                domicilio.Localidad = LocalidadMD.Load(con, tran, localidadPk);
            #endregion
            return domicilio;
        }
        public static DomicilioDP Load(DbConnection con, DomicilioPk pk)
        {
            DomicilioDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = DomicilioMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Domicilio!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = DomicilioMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Domicilio!", ex);
            }
            return resultado;
        }

        public static DomicilioDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            DomicilioDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = DomicilioMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Domicilio!", ex);
            }
            return resultado;

        }
    }
}
