using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "DomicilioPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:32:31 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "DomicilioPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>DomicilioId</term><description>Descripcion DomicilioId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "DomicilioPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// DomicilioPk domicilioPk = new DomicilioPk(domicilio);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("DomicilioPk")]
    public class DomicilioPk
    {
        #region Definicion de campos privados.
        private Int32 domicilioId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// DomicilioId
        /// </summary> 
        [XmlElement("DomicilioId")]
        public Int32 DomicilioId
        {
            get {
                    return domicilioId; 
            }
            set {
                    domicilioId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de DomicilioPk.
        /// </summary>
        /// <param name="domicilioId">Descripción domicilioId del tipo Int32.</param>
        public DomicilioPk(Int32 domicilioId) 
        {
            this.domicilioId = domicilioId;
        }
        /// <summary>
        /// Constructor normal de DomicilioPk.
        /// </summary>
        public DomicilioPk() 
        {
        }
        #endregion.
    }
}
