using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class DomicilioMD
    {
        private DomicilioDP domicilio = null;

        public DomicilioMD(DomicilioDP domicilio)
        {
            this.domicilio = domicilio;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Domicilio
            DbCommand com = con.CreateCommand();
            String insertDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Domicilio",
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Localidad",
                        "Id_Colonia",
                        "Calle",
                        "Numero_Exterior",
                        "Numero_Interior",
                        "Piso",
                        "Codigo_Postal",
                        "EntreCalles",
                        "EntreCalle",
                        "YCalle",
                        "CallePosterior",
                        "X",
                        "Y",
                        "Latitud",
                        "Longitud",
                        "Altitud",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertDomicilio;
            #endregion
            #region Parametros Insert Domicilio
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, domicilio.DomicilioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.MunicipioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.LocalidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Colonia",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.ColoniaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Calle",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, domicilio.Calle);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Numero_Exterior",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, domicilio.NumeroExterior);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Numero_Interior",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, domicilio.NumeroInterior);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Piso",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.Piso);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Codigo_Postal",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 5, ParameterDirection.Input, domicilio.CodigoPostal);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}EntreCalles",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, domicilio.Entrecalles);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}EntreCalle",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 40, ParameterDirection.Input, domicilio.Entrecalle);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}YCalle",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 40, ParameterDirection.Input, domicilio.Ycalle);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CallePosterior",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 40, ParameterDirection.Input, domicilio.Calleposterior);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}X",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, domicilio.X);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Y",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, domicilio.Y);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Latitud",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, domicilio.Latitud);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Longitud",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, domicilio.Longitud);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Altitud",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 8, ParameterDirection.Input, domicilio.Altitud);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, domicilio.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, domicilio.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(domicilio.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Domicilio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertDomicilio = String.Format(Domicilio.DomicilioResx.DomicilioInsert, "", 
                    domicilio.DomicilioId.ToString(),
                    domicilio.PaisId.ToString(),
                    domicilio.EntidadId.ToString(),
                    domicilio.MunicipioId.ToString(),
                    domicilio.LocalidadId.ToString(),
                    domicilio.ColoniaId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Calle),
                    domicilio.NumeroExterior.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.NumeroInterior),
                    domicilio.Piso.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.CodigoPostal),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Entrecalles),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Entrecalle),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Ycalle),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Calleposterior),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.X),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Y),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Latitud),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Longitud),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,domicilio.Altitud),
                    domicilio.BitActivo.ToString(),
                    domicilio.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(domicilio.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertDomicilio);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Domicilio
            DbCommand com = con.CreateCommand();
            String findDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Domicilio");
            com.CommandText = findDomicilio;
            #endregion
            #region Parametros Find Domicilio
            Common.CreateParameter(com, String.Format("{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, domicilio.DomicilioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Domicilio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioFind, "", 
                    domicilio.DomicilioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindDomicilio);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Domicilio
            DbCommand com = con.CreateCommand();
            String deleteDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Domicilio");
            com.CommandText = deleteDomicilio;
            #endregion
            #region Parametros Delete Domicilio
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, domicilio.DomicilioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Domicilio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioDelete, "", 
                    domicilio.DomicilioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteDomicilio);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Domicilio
            DbCommand com = con.CreateCommand();
            String updateDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Localidad",
                        "Id_Colonia",
                        "Calle",
                        "Numero_Exterior",
                        "Numero_Interior",
                        "Piso",
                        "Codigo_Postal",
                        "EntreCalles",
                        "EntreCalle",
                        "YCalle",
                        "CallePosterior",
                        "X",
                        "Y",
                        "Latitud",
                        "Longitud",
                        "Altitud",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Domicilio");
            com.CommandText = updateDomicilio;
            #endregion
            #region Parametros Update Domicilio
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.MunicipioId);
            Common.CreateParameter(com, String.Format("{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.LocalidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Colonia",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.ColoniaId);
            Common.CreateParameter(com, String.Format("{0}Calle",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, domicilio.Calle);
            Common.CreateParameter(com, String.Format("{0}Numero_Exterior",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, domicilio.NumeroExterior);
            Common.CreateParameter(com, String.Format("{0}Numero_Interior",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, domicilio.NumeroInterior);
            Common.CreateParameter(com, String.Format("{0}Piso",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, domicilio.Piso);
            Common.CreateParameter(com, String.Format("{0}Codigo_Postal",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 5, ParameterDirection.Input, domicilio.CodigoPostal);
            Common.CreateParameter(com, String.Format("{0}EntreCalles",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, domicilio.Entrecalles);
            Common.CreateParameter(com, String.Format("{0}EntreCalle",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 40, ParameterDirection.Input, domicilio.Entrecalle);
            Common.CreateParameter(com, String.Format("{0}YCalle",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 40, ParameterDirection.Input, domicilio.Ycalle);
            Common.CreateParameter(com, String.Format("{0}CallePosterior",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 40, ParameterDirection.Input, domicilio.Calleposterior);
            Common.CreateParameter(com, String.Format("{0}X",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, domicilio.X);
            Common.CreateParameter(com, String.Format("{0}Y",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, domicilio.Y);
            Common.CreateParameter(com, String.Format("{0}Latitud",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, domicilio.Latitud);
            Common.CreateParameter(com, String.Format("{0}Longitud",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, domicilio.Longitud);
            Common.CreateParameter(com, String.Format("{0}Altitud",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 8, ParameterDirection.Input, domicilio.Altitud);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, domicilio.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, domicilio.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(domicilio.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, domicilio.DomicilioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Domicilio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateDomicilio = String.Format(Domicilio.DomicilioResx.DomicilioUpdate, "", 
                    domicilio.PaisId.ToString(),
                    domicilio.EntidadId.ToString(),
                    domicilio.MunicipioId.ToString(),
                    domicilio.LocalidadId.ToString(),
                    domicilio.ColoniaId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Calle),
                    domicilio.NumeroExterior.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.NumeroInterior),
                    domicilio.Piso.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.CodigoPostal),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Entrecalles),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Entrecalle),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Ycalle),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Calleposterior),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.X),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Y),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Latitud),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Longitud),
                    String.Format(ConstantesGlobales.ConvierteString,domicilio.Altitud),
                    domicilio.BitActivo.ToString(),
                    domicilio.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(domicilio.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    domicilio.DomicilioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateDomicilio);
                #endregion
            }
            return resultado;
        }
        protected static DomicilioDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            DomicilioDP domicilio = new DomicilioDP();
            domicilio.DomicilioId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            domicilio.PaisId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            domicilio.EntidadId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            domicilio.MunicipioId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            domicilio.LocalidadId = dr.IsDBNull(4) ? (short)0 : dr.GetInt16(4);
            domicilio.ColoniaId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            domicilio.Calle = dr.IsDBNull(6) ? "" : dr.GetString(6);
            domicilio.NumeroExterior = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            domicilio.NumeroInterior = dr.IsDBNull(8) ? "" : dr.GetString(8);
            domicilio.Piso = dr.IsDBNull(9) ? (short)0 : dr.GetInt16(9);
            domicilio.CodigoPostal = dr.IsDBNull(10) ? "" : dr.GetString(10);
            domicilio.Entrecalles = dr.IsDBNull(11) ? "" : dr.GetString(11);
            domicilio.Entrecalle = dr.IsDBNull(12) ? "" : dr.GetString(12);
            domicilio.Ycalle = dr.IsDBNull(13) ? "" : dr.GetString(13);
            domicilio.Calleposterior = dr.IsDBNull(14) ? "" : dr.GetString(14);
            domicilio.X = dr.IsDBNull(15) ? "" : dr.GetString(15);
            domicilio.Y = dr.IsDBNull(16) ? "" : dr.GetString(16);
            domicilio.Latitud = dr.IsDBNull(17) ? "" : dr.GetString(17);
            domicilio.Longitud = dr.IsDBNull(18) ? "" : dr.GetString(18);
            domicilio.Altitud = dr.IsDBNull(19) ? "" : dr.GetString(19);
            domicilio.BitActivo = dr.IsDBNull(20) ? false : dr.GetBoolean(20);
            domicilio.UsuarioId = dr.IsDBNull(21) ? 0 : dr.GetInt32(21);
            domicilio.FechaActualizacion = dr.IsDBNull(22) ? "" : dr.GetDateTime(22).ToShortDateString();;
            return domicilio;
            #endregion
        }
        public static DomicilioDP Load(DbConnection con, DbTransaction tran, DomicilioPk pk)
        {
            #region SQL Load Domicilio
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Domicilio");
            com.CommandText = loadDomicilio;
            #endregion
            #region Parametros Load Domicilio
            Common.CreateParameter(com, String.Format("{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.DomicilioId);
            #endregion
            DomicilioDP domicilio;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Domicilio
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        domicilio = ReadRow(dr);
                    }
                    else
                        domicilio = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Domicilio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioSelect, "", 
                    pk.DomicilioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadDomicilio);
                domicilio = null;
                #endregion
            }
            return domicilio;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Domicilio
            DbCommand com = con.CreateCommand();
            String countDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioCount,"");
            com.CommandText = countDomicilio;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Domicilio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountDomicilio);
                #endregion
            }
            return resultado;
        }
        public static DomicilioDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Domicilio
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listDomicilio = String.Format(CultureInfo.CurrentCulture, Domicilio.DomicilioResx.DomicilioSelectAll, "", ConstantesGlobales.IncluyeRows);
            listDomicilio = listDomicilio.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listDomicilio, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Domicilio
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Domicilio
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Domicilio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaDomicilio = String.Format(CultureInfo.CurrentCulture,Domicilio.DomicilioResx.DomicilioSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaDomicilio);
                #endregion
            }
            return (DomicilioDP[])lista.ToArray(typeof(DomicilioDP));
        }
    }
}
