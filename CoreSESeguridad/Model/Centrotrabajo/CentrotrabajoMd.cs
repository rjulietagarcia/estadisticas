using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class CentrotrabajoMD
    {
        private CentrotrabajoDP centrotrabajo = null;

        public CentrotrabajoMD(CentrotrabajoDP centrotrabajo)
        {
            this.centrotrabajo = centrotrabajo;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Centrotrabajo
            DbCommand com = con.CreateCommand();
            String insertCentrotrabajo = String.Format(CultureInfo.CurrentCulture,Centrotrabajo.CentrotrabajoResx.CentrotrabajoInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CentroTrabajo",
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region",
                        "Id_Zona",
                        "Id_Sostenimiento",
                        "Clave",
                        "Turno3d",
                        "BitProvisional",
                        "Id_Inmueble",
                        "Id_Domicilio",
                        "Fecha_Fundacion",
                        "Fecha_Alta",
                        "Fecha_Clausura",
                        "Id_MotivoBajaCentroTrabajo",
                        "Fecha_Reapertura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Cambio",
                        "Nombre",
                        "id_tipoct",
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel",
                        "id_control",
                        "id_subcontrol",
                        "id_dependencianormativa",
                        "id_dependenciaadministrativa",
                        "id_servicio",
                        "id_sector",
                        "id_educacionfisica",
                        "id_estatus",
                        "id_almacen",
                        "id_puntocardinal",
                        "numero_incorporacion",
                        "folio",
                        "id_dependenciaoperativa",
                        "observaciones",
                        "id_incorporacion",
                        "fechasol",
                        "id_claveinstitucional",
                        "Id_NivelEducacion",
                        "Id_ClaveAgrupador",
                        "id_turno",
                        "Valid_Datos");
            com.CommandText = insertCentrotrabajo;
            #endregion
            #region Parametros Insert Centrotrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.CentrotrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.RegionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.ZonaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, centrotrabajo.SostenimientoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, centrotrabajo.Clave);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Turno3d",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 3, ParameterDirection.Input, centrotrabajo.Turno3d);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitProvisional",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, centrotrabajo.Bitprovisional);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, centrotrabajo.InmuebleId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, centrotrabajo.DomicilioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Fundacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaFundacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Alta",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaAlta,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Clausura",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaClausura,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_MotivoBajaCentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.MotivobajacentrotrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Reapertura",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaReapertura,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, centrotrabajo.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, centrotrabajo.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Cambio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaCambio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, centrotrabajo.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoct",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, centrotrabajo.TipoctId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.NivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.SubnivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_control",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.ControlId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_subcontrol",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.SubcontrolId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_dependencianormativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.DependencianormativaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_dependenciaadministrativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.DependenciaadministrativaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_servicio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.ServicioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.SectorId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_educacionfisica",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.EducacionfisicaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_estatus",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.EstatusId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_almacen",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.AlmacenId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_puntocardinal",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.PuntocardinalId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}numero_incorporacion",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, centrotrabajo.NumeroIncorporacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}folio",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, centrotrabajo.Folio);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_dependenciaoperativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.DependenciaoperativaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}observaciones",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1500, ParameterDirection.Input, centrotrabajo.Observaciones);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_incorporacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.IncorporacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}fechasol",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.Fechasol,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_claveinstitucional",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.ClaveinstitucionalId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelEducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, centrotrabajo.NiveleducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_ClaveAgrupador",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, centrotrabajo.ClaveagrupadorId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.TurnoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Valid_Datos",ConstantesGlobales.ParameterPrefix), DbType.Decimal, 0, ParameterDirection.Input, centrotrabajo.ValidDatos);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Centrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Centrotrabajo
            DbCommand com = con.CreateCommand();
            String findCentrotrabajo = String.Format(CultureInfo.CurrentCulture,Centrotrabajo.CentrotrabajoResx.CentrotrabajoFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CentroTrabajo");
            com.CommandText = findCentrotrabajo;
            #endregion
            #region Parametros Find Centrotrabajo
            Common.CreateParameter(com, String.Format("{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.CentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Centrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Centrotrabajo
            DbCommand com = con.CreateCommand();
            String deleteCentrotrabajo = String.Format(CultureInfo.CurrentCulture,Centrotrabajo.CentrotrabajoResx.CentrotrabajoDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CentroTrabajo");
            com.CommandText = deleteCentrotrabajo;
            #endregion
            #region Parametros Delete Centrotrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.CentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Centrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Centrotrabajo
            DbCommand com = con.CreateCommand();
            String updateCentrotrabajo = String.Format(CultureInfo.CurrentCulture,Centrotrabajo.CentrotrabajoResx.CentrotrabajoUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region",
                        "Id_Zona",
                        "Id_Sostenimiento",
                        "Clave",
                        "Turno3d",
                        "BitProvisional",
                        "Id_Inmueble",
                        "Id_Domicilio",
                        "Fecha_Fundacion",
                        "Fecha_Alta",
                        "Fecha_Clausura",
                        "Id_MotivoBajaCentroTrabajo",
                        "Fecha_Reapertura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Cambio",
                        "Nombre",
                        "id_tipoct",
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel",
                        "id_control",
                        "id_subcontrol",
                        "id_dependencianormativa",
                        "id_dependenciaadministrativa",
                        "id_servicio",
                        "id_sector",
                        "id_educacionfisica",
                        "id_estatus",
                        "id_almacen",
                        "id_puntocardinal",
                        "numero_incorporacion",
                        "folio",
                        "id_dependenciaoperativa",
                        "observaciones",
                        "id_incorporacion",
                        "fechasol",
                        "id_claveinstitucional",
                        "Id_NivelEducacion",
                        "Id_ClaveAgrupador",
                        "id_turno",
                        "Valid_Datos",
                        "Id_CentroTrabajo");
            com.CommandText = updateCentrotrabajo;
            #endregion
            #region Parametros Update Centrotrabajo
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.RegionId);
            Common.CreateParameter(com, String.Format("{0}Id_Zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.ZonaId);
            Common.CreateParameter(com, String.Format("{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, centrotrabajo.SostenimientoId);
            Common.CreateParameter(com, String.Format("{0}Clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, centrotrabajo.Clave);
            Common.CreateParameter(com, String.Format("{0}Turno3d",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 3, ParameterDirection.Input, centrotrabajo.Turno3d);
            Common.CreateParameter(com, String.Format("{0}BitProvisional",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, centrotrabajo.Bitprovisional);
            Common.CreateParameter(com, String.Format("{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, centrotrabajo.InmuebleId);
            Common.CreateParameter(com, String.Format("{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, centrotrabajo.DomicilioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Fundacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaFundacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Alta",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaAlta,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Clausura",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaClausura,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_MotivoBajaCentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.MotivobajacentrotrabajoId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Reapertura",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaReapertura,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, centrotrabajo.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, centrotrabajo.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Cambio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.FechaCambio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, centrotrabajo.Nombre);
            Common.CreateParameter(com, String.Format("{0}id_tipoct",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, centrotrabajo.TipoctId);
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.NivelId);
            Common.CreateParameter(com, String.Format("{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.SubnivelId);
            Common.CreateParameter(com, String.Format("{0}id_control",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.ControlId);
            Common.CreateParameter(com, String.Format("{0}id_subcontrol",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.SubcontrolId);
            Common.CreateParameter(com, String.Format("{0}id_dependencianormativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.DependencianormativaId);
            Common.CreateParameter(com, String.Format("{0}id_dependenciaadministrativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.DependenciaadministrativaId);
            Common.CreateParameter(com, String.Format("{0}id_servicio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.ServicioId);
            Common.CreateParameter(com, String.Format("{0}id_sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.SectorId);
            Common.CreateParameter(com, String.Format("{0}id_educacionfisica",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.EducacionfisicaId);
            Common.CreateParameter(com, String.Format("{0}id_estatus",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.EstatusId);
            Common.CreateParameter(com, String.Format("{0}id_almacen",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.AlmacenId);
            Common.CreateParameter(com, String.Format("{0}id_puntocardinal",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.PuntocardinalId);
            Common.CreateParameter(com, String.Format("{0}numero_incorporacion",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, centrotrabajo.NumeroIncorporacion);
            Common.CreateParameter(com, String.Format("{0}folio",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, centrotrabajo.Folio);
            Common.CreateParameter(com, String.Format("{0}id_dependenciaoperativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.DependenciaoperativaId);
            Common.CreateParameter(com, String.Format("{0}observaciones",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1500, ParameterDirection.Input, centrotrabajo.Observaciones);
            Common.CreateParameter(com, String.Format("{0}id_incorporacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.IncorporacionId);
            Common.CreateParameter(com, String.Format("{0}fechasol",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(centrotrabajo.Fechasol,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}id_claveinstitucional",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.ClaveinstitucionalId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelEducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, centrotrabajo.NiveleducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_ClaveAgrupador",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, centrotrabajo.ClaveagrupadorId);
            Common.CreateParameter(com, String.Format("{0}id_turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.TurnoId);
            Common.CreateParameter(com, String.Format("{0}Valid_Datos",ConstantesGlobales.ParameterPrefix), DbType.Decimal, 0, ParameterDirection.Input, centrotrabajo.ValidDatos);
            Common.CreateParameter(com, String.Format("{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, centrotrabajo.CentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Centrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static CentrotrabajoDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CentrotrabajoDP centrotrabajo = new CentrotrabajoDP();
            centrotrabajo.CentrotrabajoId = dr.IsDBNull(0) ? 0 :int.Parse(dr.GetValue(0).ToString());
            centrotrabajo.PaisId = dr.IsDBNull(1) ? 0 : int.Parse(dr.GetValue(1).ToString());
            centrotrabajo.EntidadId = dr.IsDBNull(2) ? 0 : int.Parse(dr.GetValue(2).ToString());
            centrotrabajo.RegionId = dr.IsDBNull(3) ? 0 : int.Parse(dr.GetValue(3).ToString());
            centrotrabajo.ZonaId = dr.IsDBNull(4) ? 0 : int.Parse(dr.GetValue(4).ToString());
            centrotrabajo.SostenimientoId = dr.IsDBNull(5) ? 0 :int.Parse(dr.GetValue(5).ToString());
            centrotrabajo.Clave = dr.IsDBNull(6) ? "" : dr.GetString(6);
            centrotrabajo.Turno3d = dr.IsDBNull(7) ? "" : dr.GetString(7);
            centrotrabajo.Bitprovisional = dr.IsDBNull(8) ? false : dr.GetBoolean(8);
            centrotrabajo.InmuebleId = dr.IsDBNull(9) ? 0 : dr.GetInt32(9);
            centrotrabajo.DomicilioId = dr.IsDBNull(10) ? 0 : dr.GetInt32(10);
            centrotrabajo.FechaFundacion = dr.IsDBNull(11) ? "" : dr.GetDateTime(11).ToShortDateString();;
            centrotrabajo.FechaAlta = dr.IsDBNull(12) ? "" : dr.GetDateTime(12).ToShortDateString();;
            centrotrabajo.FechaClausura = dr.IsDBNull(13) ? "" : dr.GetDateTime(13).ToShortDateString();;
            centrotrabajo.MotivobajacentrotrabajoId = dr.IsDBNull(14) ? 0 : int.Parse(dr.GetValue(14).ToString());
            centrotrabajo.FechaReapertura = dr.IsDBNull(15) ? "" : dr.GetDateTime(15).ToShortDateString();;
            centrotrabajo.BitActivo = dr.IsDBNull(16) ? false : dr.GetBoolean(16);
            centrotrabajo.UsuarioId = dr.IsDBNull(17) ? 0 : dr.GetInt32(17);
            centrotrabajo.FechaActualizacion = dr.IsDBNull(18) ? "" : dr.GetDateTime(18).ToShortDateString();;
            centrotrabajo.FechaCambio = dr.IsDBNull(19) ? "" : dr.GetDateTime(19).ToShortDateString();;
            centrotrabajo.Nombre = dr.IsDBNull(20) ? "" : dr.GetString(20);
            centrotrabajo.TipoctId = dr.IsDBNull(21) ? "" : dr.GetString(21);
            centrotrabajo.TipoeducacionId = dr.IsDBNull(22) ? 0 : int.Parse(dr.GetValue(22).ToString());
            centrotrabajo.NivelId = dr.IsDBNull(23) ? 0 : int.Parse(dr.GetValue(23).ToString());
            centrotrabajo.SubnivelId = dr.IsDBNull(24) ? 0 : int.Parse(dr.GetValue(24).ToString());
            centrotrabajo.ControlId = dr.IsDBNull(25) ? 0 : int.Parse(dr.GetValue(25).ToString());
            centrotrabajo.SubcontrolId = dr.IsDBNull(26) ? 0 : int.Parse(dr.GetValue(26).ToString());
            centrotrabajo.DependencianormativaId = dr.IsDBNull(27) ? 0 : int.Parse(dr.GetValue(27).ToString());
            centrotrabajo.DependenciaadministrativaId = dr.IsDBNull(28) ? 0 : int.Parse(dr.GetValue(28).ToString());
            centrotrabajo.ServicioId = dr.IsDBNull(29) ? 0 : int.Parse(dr.GetValue(29).ToString());
            centrotrabajo.SectorId = dr.IsDBNull(30) ? 0 : int.Parse(dr.GetValue(30).ToString());
            centrotrabajo.EducacionfisicaId = dr.IsDBNull(31) ? 0 : int.Parse(dr.GetValue(31).ToString());
            centrotrabajo.EstatusId = dr.IsDBNull(32) ? 0 : int.Parse(dr.GetValue(32).ToString());
            centrotrabajo.AlmacenId = dr.IsDBNull(33) ? 0 : int.Parse(dr.GetValue(33).ToString());
            centrotrabajo.PuntocardinalId = dr.IsDBNull(34) ? 0 : int.Parse(dr.GetValue(34).ToString());
            centrotrabajo.NumeroIncorporacion = dr.IsDBNull(35) ? "" : dr.GetString(35);
            centrotrabajo.Folio = dr.IsDBNull(36) ? "" : dr.GetString(36);
            centrotrabajo.DependenciaoperativaId = dr.IsDBNull(37) ? 0 : int.Parse(dr.GetValue(37).ToString());
            centrotrabajo.Observaciones = dr.IsDBNull(38) ? "" : dr.GetString(38);
            centrotrabajo.IncorporacionId = dr.IsDBNull(39) ? 0 : int.Parse(dr.GetValue(39).ToString());
            centrotrabajo.Fechasol = dr.IsDBNull(40) ? "" : dr.GetDateTime(40).ToShortDateString();;
            centrotrabajo.ClaveinstitucionalId = dr.IsDBNull(41) ? 0 : int.Parse(dr.GetValue(41).ToString());
            centrotrabajo.NiveleducacionId = dr.IsDBNull(42) ? 0 :int.Parse( dr.GetValue(42).ToString());
            centrotrabajo.ClaveagrupadorId = dr.IsDBNull(43) ? 0 :int.Parse( dr.GetValue(43).ToString());
            centrotrabajo.TurnoId = dr.IsDBNull(44) ? 0 : int.Parse(dr.GetValue(44).ToString());
            centrotrabajo.ValidDatos = dr.IsDBNull(45) ? 0 :decimal.Parse( dr.GetValue(45).ToString());
            return centrotrabajo;
            #endregion
        }
        public static CentrotrabajoDP Load(DbConnection con, DbTransaction tran, CentrotrabajoPk pk)
        {
            #region SQL Load Centrotrabajo
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadCentrotrabajo = String.Format(CultureInfo.CurrentCulture,Centrotrabajo.CentrotrabajoResx.CentrotrabajoSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CentroTrabajo");
            com.CommandText = loadCentrotrabajo;
            #endregion
            #region Parametros Load Centrotrabajo
            Common.CreateParameter(com, String.Format("{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.CentrotrabajoId);
            #endregion
            CentrotrabajoDP centrotrabajo;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Centrotrabajo
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        centrotrabajo = ReadRow(dr);
                    }
                    else
                        centrotrabajo = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Centrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return centrotrabajo;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Centrotrabajo
            DbCommand com = con.CreateCommand();
            String countCentrotrabajo = String.Format(CultureInfo.CurrentCulture,Centrotrabajo.CentrotrabajoResx.CentrotrabajoCount,"");
            com.CommandText = countCentrotrabajo;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Centrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CentrotrabajoDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Centrotrabajo
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCentrotrabajo = String.Format(CultureInfo.CurrentCulture, Centrotrabajo.CentrotrabajoResx.CentrotrabajoSelectAll, "", ConstantesGlobales.IncluyeRows);
            listCentrotrabajo = listCentrotrabajo.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCentrotrabajo, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Centrotrabajo
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Centrotrabajo
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Centrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CentrotrabajoDP[])lista.ToArray(typeof(CentrotrabajoDP));
        }
    }
}
