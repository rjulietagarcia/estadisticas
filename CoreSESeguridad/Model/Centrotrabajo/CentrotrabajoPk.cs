using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "CentrotrabajoPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: viernes, 26 de junio de 2009.</Para>
    /// <Para>Hora: 01:04:14 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CentrotrabajoPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>CentrotrabajoId</term><description>Descripcion CentrotrabajoId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CentrotrabajoPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// CentrotrabajoPk centrotrabajoPk = new CentrotrabajoPk(centrotrabajo);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("CentrotrabajoPk")]
    public class CentrotrabajoPk
    {
        #region Definicion de campos privados.
        private int centrotrabajoId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// CentrotrabajoId
        /// </summary> 
        [XmlElement("CentrotrabajoId")]
        public int CentrotrabajoId
        {
            get {
                    return centrotrabajoId; 
            }
            set {
                    centrotrabajoId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de CentrotrabajoPk.
        /// </summary>
        /// <param name="centrotrabajoId">Descripción centrotrabajoId del tipo Int16.</param>
        public CentrotrabajoPk(int centrotrabajoId) 
        {
            this.centrotrabajoId = centrotrabajoId;
        }
        /// <summary>
        /// Constructor normal de CentrotrabajoPk.
        /// </summary>
        public CentrotrabajoPk() 
        {
        }
        #endregion.
    }
}
