using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class CentrotrabajoBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, CentrotrabajoDP centrotrabajo)
        {
            #region SaveDetail
            #region FK Region
            RegionDP region = centrotrabajo.Region;
            if (region != null) 
            {
                RegionMD regionMd = new RegionMD(region);
                if (!regionMd.Find(con, tran))
                    regionMd.Insert(con, tran);
            }
            #endregion
            #region FK Zona
            ZonaDP zona = centrotrabajo.Zona;
            if (zona != null) 
            {
                ZonaMD zonaMd = new ZonaMD(zona);
                if (!zonaMd.Find(con, tran))
                    zonaMd.Insert(con, tran);
            }
            #endregion
            #region FK Sostenimiento
            SostenimientoDP sostenimiento = centrotrabajo.Sostenimiento;
            if (sostenimiento != null) 
            {
                SostenimientoMD sostenimientoMd = new SostenimientoMD(sostenimiento);
                if (!sostenimientoMd.Find(con, tran))
                    sostenimientoMd.Insert(con, tran);
            }
            #endregion
            #region FK Motivobajacentrotrabajo
            MotivobajacentrotrabajoDP motivobajacentrotrabajo = centrotrabajo.Motivobajacentrotrabajo;
            if (motivobajacentrotrabajo != null) 
            {
                MotivobajacentrotrabajoMD motivobajacentrotrabajoMd = new MotivobajacentrotrabajoMD(motivobajacentrotrabajo);
                if (!motivobajacentrotrabajoMd.Find(con, tran))
                    motivobajacentrotrabajoMd.Insert(con, tran);
            }
            #endregion
            #region FK Dependenciaadministrativa
            DependenciaadministrativaDP dependenciaadministrativa = centrotrabajo.Dependenciaadministrativa;
            if (dependenciaadministrativa != null) 
            {
                DependenciaadministrativaMD dependenciaadministrativaMd = new DependenciaadministrativaMD(dependenciaadministrativa);
                if (!dependenciaadministrativaMd.Find(con, tran))
                    dependenciaadministrativaMd.Insert(con, tran);
            }
            #endregion
            #region FK Sector
            SectorDP sector = centrotrabajo.Sector;
            if (sector != null) 
            {
                SectorMD sectorMd = new SectorMD(sector);
                if (!sectorMd.Find(con, tran))
                    sectorMd.Insert(con, tran);
            }
            #endregion
            #region FK Niveleducacionbasica
            NiveleducacionbasicaDP niveleducacion = centrotrabajo.Niveleducacion;
            if (niveleducacion != null) 
            {
                NiveleducacionbasicaMD niveleducacionbasicaMd = new NiveleducacionbasicaMD(niveleducacion);
                if (!niveleducacionbasicaMd.Find(con, tran))
                    niveleducacionbasicaMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, CentrotrabajoDP centrotrabajo)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, centrotrabajo);
                        resultado = InternalSave(con, tran, centrotrabajo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla centrotrabajo!";
                        throw new BusinessException("Error interno al intentar guardar centrotrabajo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla centrotrabajo!";
                throw new BusinessException("Error interno al intentar guardar centrotrabajo!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, CentrotrabajoDP centrotrabajo)
        {
            CentrotrabajoMD centrotrabajoMd = new CentrotrabajoMD(centrotrabajo);
            String resultado = "";
            if (centrotrabajoMd.Find(con, tran))
            {
                CentrotrabajoDP centrotrabajoAnterior = CentrotrabajoMD.Load(con, tran, centrotrabajo.Pk);
                if (centrotrabajoMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla centrotrabajo!";
                    throw new BusinessException("No se pude actualizar la tabla centrotrabajo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (centrotrabajoMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla centrotrabajo!";
                    throw new BusinessException("No se pude insertar en la tabla centrotrabajo!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, CentrotrabajoDP centrotrabajo)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, centrotrabajo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla centrotrabajo!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla centrotrabajo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla centrotrabajo!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla centrotrabajo!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, CentrotrabajoDP centrotrabajo)
        {
            String resultado = "";
            CentrotrabajoMD centrotrabajoMd = new CentrotrabajoMD(centrotrabajo);
            if (centrotrabajoMd.Find(con, tran))
            {
                if (centrotrabajoMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla centrotrabajo!";
                    throw new BusinessException("No se pude eliminar en la tabla centrotrabajo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla centrotrabajo!";
                throw new BusinessException("No se pude eliminar en la tabla centrotrabajo!");
            }
            return resultado;
        }

        internal static CentrotrabajoDP LoadDetail(DbConnection con, DbTransaction tran, CentrotrabajoDP centrotrabajoAnterior, CentrotrabajoDP centrotrabajo) 
        {
            #region FK Region
            RegionPk regionPk = new RegionPk();
            if (centrotrabajoAnterior != null)
            {
                if (regionPk.Equals(centrotrabajoAnterior.Pk))
                    centrotrabajo.Region = centrotrabajoAnterior.Region;
                else
                    centrotrabajo.Region = RegionMD.Load(con, tran, regionPk);
            }
            else
                centrotrabajo.Region = RegionMD.Load(con, tran, regionPk);
            #endregion
            #region FK Zona
            ZonaPk zonaPk = new ZonaPk();
            if (centrotrabajoAnterior != null)
            {
                if (zonaPk.Equals(centrotrabajoAnterior.Pk))
                    centrotrabajo.Zona = centrotrabajoAnterior.Zona;
                else
                    centrotrabajo.Zona = ZonaMD.Load(con, tran, zonaPk);
            }
            else
                centrotrabajo.Zona = ZonaMD.Load(con, tran, zonaPk);
            #endregion
            #region FK Sostenimiento
            SostenimientoPk sostenimientoPk = new SostenimientoPk();
            if (centrotrabajoAnterior != null)
            {
                if (sostenimientoPk.Equals(centrotrabajoAnterior.Pk))
                    centrotrabajo.Sostenimiento = centrotrabajoAnterior.Sostenimiento;
                else
                    centrotrabajo.Sostenimiento = SostenimientoMD.Load(con, tran, sostenimientoPk);
            }
            else
                centrotrabajo.Sostenimiento = SostenimientoMD.Load(con, tran, sostenimientoPk);
            #endregion
            #region FK Motivobajacentrotrabajo
            MotivobajacentrotrabajoPk motivobajacentrotrabajoPk = new MotivobajacentrotrabajoPk();
            if (centrotrabajoAnterior != null)
            {
                if (motivobajacentrotrabajoPk.Equals(centrotrabajoAnterior.Pk))
                    centrotrabajo.Motivobajacentrotrabajo = centrotrabajoAnterior.Motivobajacentrotrabajo;
                else
                    centrotrabajo.Motivobajacentrotrabajo = MotivobajacentrotrabajoMD.Load(con, tran, motivobajacentrotrabajoPk);
            }
            else
                centrotrabajo.Motivobajacentrotrabajo = MotivobajacentrotrabajoMD.Load(con, tran, motivobajacentrotrabajoPk);
            #endregion
            #region FK Dependenciaadministrativa
            DependenciaadministrativaPk dependenciaadministrativaPk = new DependenciaadministrativaPk();
            if (centrotrabajoAnterior != null)
            {
                if (dependenciaadministrativaPk.Equals(centrotrabajoAnterior.Pk))
                    centrotrabajo.Dependenciaadministrativa = centrotrabajoAnterior.Dependenciaadministrativa;
                else
                    centrotrabajo.Dependenciaadministrativa = DependenciaadministrativaMD.Load(con, tran, dependenciaadministrativaPk);
            }
            else
                centrotrabajo.Dependenciaadministrativa = DependenciaadministrativaMD.Load(con, tran, dependenciaadministrativaPk);
            #endregion
            #region FK Sector
            SectorPk sectorPk = new SectorPk();
            if (centrotrabajoAnterior != null)
            {
                if (sectorPk.Equals(centrotrabajoAnterior.Pk))
                    centrotrabajo.Sector = centrotrabajoAnterior.Sector;
                else
                    centrotrabajo.Sector = SectorMD.Load(con, tran, sectorPk);
            }
            else
                centrotrabajo.Sector = SectorMD.Load(con, tran, sectorPk);
            #endregion
            #region FK Niveleducacion
            NiveleducacionbasicaPk niveleducacionPk = new NiveleducacionbasicaPk();
            if (centrotrabajoAnterior != null)
            {
                if (niveleducacionPk.Equals(centrotrabajoAnterior.Pk))
                    centrotrabajo.Niveleducacion = centrotrabajoAnterior.Niveleducacion;
                else
                    centrotrabajo.Niveleducacion = NiveleducacionbasicaMD.Load(con, tran, niveleducacionPk);
            }
            else
                centrotrabajo.Niveleducacion = NiveleducacionbasicaMD.Load(con, tran, niveleducacionPk);
            #endregion
            return centrotrabajo;
        }
        public static CentrotrabajoDP Load(DbConnection con, CentrotrabajoPk pk)
        {
            CentrotrabajoDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CentrotrabajoMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Centrotrabajo!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CentrotrabajoMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Centrotrabajo!", ex);
            }
            return resultado;
        }

        public static CentrotrabajoDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CentrotrabajoDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CentrotrabajoMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Centrotrabajo!", ex);
            }
            return resultado;

        }
    }
}
