using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Centrotrabajo".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: viernes, 26 de junio de 2009.</Para>
    /// <Para>Hora: 01:04:14 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Centrotrabajo".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>CentrotrabajoId</term><description>Descripcion CentrotrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    ///    <item>
    ///        <term>ZonaId</term><description>Descripcion ZonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Clave</term><description>Descripcion Clave</description>
    ///    </item>
    ///    <item>
    ///        <term>Turno3d</term><description>Descripcion Turno3d</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitprovisional</term><description>Descripcion Bitprovisional</description>
    ///    </item>
    ///    <item>
    ///        <term>InmuebleId</term><description>Descripcion InmuebleId</description>
    ///    </item>
    ///    <item>
    ///        <term>DomicilioId</term><description>Descripcion DomicilioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaFundacion</term><description>Descripcion FechaFundacion</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaAlta</term><description>Descripcion FechaAlta</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaClausura</term><description>Descripcion FechaClausura</description>
    ///    </item>
    ///    <item>
    ///        <term>MotivobajacentrotrabajoId</term><description>Descripcion MotivobajacentrotrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaReapertura</term><description>Descripcion FechaReapertura</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaCambio</term><description>Descripcion FechaCambio</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>TipoctId</term><description>Descripcion TipoctId</description>
    ///    </item>
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>SubnivelId</term><description>Descripcion SubnivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>ControlId</term><description>Descripcion ControlId</description>
    ///    </item>
    ///    <item>
    ///        <term>SubcontrolId</term><description>Descripcion SubcontrolId</description>
    ///    </item>
    ///    <item>
    ///        <term>DependencianormativaId</term><description>Descripcion DependencianormativaId</description>
    ///    </item>
    ///    <item>
    ///        <term>DependenciaadministrativaId</term><description>Descripcion DependenciaadministrativaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ServicioId</term><description>Descripcion ServicioId</description>
    ///    </item>
    ///    <item>
    ///        <term>SectorId</term><description>Descripcion SectorId</description>
    ///    </item>
    ///    <item>
    ///        <term>EducacionfisicaId</term><description>Descripcion EducacionfisicaId</description>
    ///    </item>
    ///    <item>
    ///        <term>EstatusId</term><description>Descripcion EstatusId</description>
    ///    </item>
    ///    <item>
    ///        <term>AlmacenId</term><description>Descripcion AlmacenId</description>
    ///    </item>
    ///    <item>
    ///        <term>PuntocardinalId</term><description>Descripcion PuntocardinalId</description>
    ///    </item>
    ///    <item>
    ///        <term>NumeroIncorporacion</term><description>Descripcion NumeroIncorporacion</description>
    ///    </item>
    ///    <item>
    ///        <term>Folio</term><description>Descripcion Folio</description>
    ///    </item>
    ///    <item>
    ///        <term>DependenciaoperativaId</term><description>Descripcion DependenciaoperativaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Observaciones</term><description>Descripcion Observaciones</description>
    ///    </item>
    ///    <item>
    ///        <term>IncorporacionId</term><description>Descripcion IncorporacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Fechasol</term><description>Descripcion Fechasol</description>
    ///    </item>
    ///    <item>
    ///        <term>ClaveinstitucionalId</term><description>Descripcion ClaveinstitucionalId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveleducacionId</term><description>Descripcion NiveleducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>ClaveagrupadorId</term><description>Descripcion ClaveagrupadorId</description>
    ///    </item>
    ///    <item>
    ///        <term>TurnoId</term><description>Descripcion TurnoId</description>
    ///    </item>
    ///    <item>
    ///        <term>ValidDatos</term><description>Descripcion ValidDatos</description>
    ///    </item>
    ///    <item>
    ///        <term>Region</term><description>Descripcion Region</description>
    ///    </item>
    ///    <item>
    ///        <term>Zona</term><description>Descripcion Zona</description>
    ///    </item>
    ///    <item>
    ///        <term>Sostenimiento</term><description>Descripcion Sostenimiento</description>
    ///    </item>
    ///    <item>
    ///        <term>Motivobajacentrotrabajo</term><description>Descripcion Motivobajacentrotrabajo</description>
    ///    </item>
    ///    <item>
    ///        <term>Dependenciaadministrativa</term><description>Descripcion Dependenciaadministrativa</description>
    ///    </item>
    ///    <item>
    ///        <term>Sector</term><description>Descripcion Sector</description>
    ///    </item>
    ///    <item>
    ///        <term>Niveleducacion</term><description>Descripcion Niveleducacion</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CentrotrabajoDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// CentrotrabajoDTO centrotrabajo = new CentrotrabajoDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Centrotrabajo")]
    public class CentrotrabajoDP
    {
        #region Definicion de campos privados.
        private int centrotrabajoId;
        private int paisId;
        private int entidadId;
        private int regionId;
        private int zonaId;
        private int sostenimientoId;
        private String clave;
        private String turno3d;
        private Boolean bitprovisional;
        private int inmuebleId;
        private int domicilioId;
        private String fechaFundacion;
        private String fechaAlta;
        private String fechaClausura;
        private int motivobajacentrotrabajoId;
        private String fechaReapertura;
        private Boolean bitActivo;
        private int usuarioId;
        private String fechaActualizacion;
        private String fechaCambio;
        private String nombre;
        private String tipoctId;
        private int tipoeducacionId;
        private int nivelId;
        private int subnivelId;
        private int controlId;
        private int subcontrolId;
        private int dependencianormativaId;
        private int dependenciaadministrativaId;
        private int servicioId;
        private int sectorId;
        private int educacionfisicaId;
        private int estatusId;
        private int almacenId;
        private int puntocardinalId;
        private String numeroIncorporacion;
        private String folio;
        private int dependenciaoperativaId;
        private String observaciones;
        private int incorporacionId;
        private String fechasol;
        private int claveinstitucionalId;
        private int niveleducacionId;
        private int claveagrupadorId;
        private int turnoId;
        private Decimal validDatos;
        private RegionDP region;
        private ZonaDP zona;
        private SostenimientoDP sostenimiento;
        private MotivobajacentrotrabajoDP motivobajacentrotrabajo;
        private DependenciaadministrativaDP dependenciaadministrativa;
        private SectorDP sector;
        private NiveleducacionbasicaDP niveleducacion;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// CentrotrabajoId
        /// </summary> 
        [XmlElement("CentrotrabajoId")]
        public int CentrotrabajoId
        {
            get {
                    return centrotrabajoId; 
            }
            set {
                    centrotrabajoId = value; 
            }
        }

        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public int PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public int EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public int RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        /// <summary>
        /// ZonaId
        /// </summary> 
        [XmlElement("ZonaId")]
        public int ZonaId
        {
            get {
                    return zonaId; 
            }
            set {
                    zonaId = value; 
            }
        }

        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public int SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// Clave
        /// </summary> 
        [XmlElement("Clave")]
        public String Clave
        {
            get {
                    return clave; 
            }
            set {
                    clave = value; 
            }
        }

        /// <summary>
        /// Turno3d
        /// </summary> 
        [XmlElement("Turno3d")]
        public String Turno3d
        {
            get {
                    return turno3d; 
            }
            set {
                    turno3d = value; 
            }
        }

        /// <summary>
        /// Bitprovisional
        /// </summary> 
        [XmlElement("Bitprovisional")]
        public Boolean Bitprovisional
        {
            get {
                    return bitprovisional; 
            }
            set {
                    bitprovisional = value; 
            }
        }

        /// <summary>
        /// InmuebleId
        /// </summary> 
        [XmlElement("InmuebleId")]
        public int InmuebleId
        {
            get {
                    return inmuebleId; 
            }
            set {
                    inmuebleId = value; 
            }
        }

        /// <summary>
        /// DomicilioId
        /// </summary> 
        [XmlElement("DomicilioId")]
        public int DomicilioId
        {
            get {
                    return domicilioId; 
            }
            set {
                    domicilioId = value; 
            }
        }

        /// <summary>
        /// FechaFundacion
        /// </summary> 
        [XmlElement("FechaFundacion")]
        public String FechaFundacion
        {
            get {
                    return fechaFundacion; 
            }
            set {
                    fechaFundacion = value; 
            }
        }

        /// <summary>
        /// FechaAlta
        /// </summary> 
        [XmlElement("FechaAlta")]
        public String FechaAlta
        {
            get {
                    return fechaAlta; 
            }
            set {
                    fechaAlta = value; 
            }
        }

        /// <summary>
        /// FechaClausura
        /// </summary> 
        [XmlElement("FechaClausura")]
        public String FechaClausura
        {
            get {
                    return fechaClausura; 
            }
            set {
                    fechaClausura = value; 
            }
        }

        /// <summary>
        /// MotivobajacentrotrabajoId
        /// </summary> 
        [XmlElement("MotivobajacentrotrabajoId")]
        public int MotivobajacentrotrabajoId
        {
            get {
                    return motivobajacentrotrabajoId; 
            }
            set {
                    motivobajacentrotrabajoId = value; 
            }
        }

        /// <summary>
        /// FechaReapertura
        /// </summary> 
        [XmlElement("FechaReapertura")]
        public String FechaReapertura
        {
            get {
                    return fechaReapertura; 
            }
            set {
                    fechaReapertura = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public int UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// FechaCambio
        /// </summary> 
        [XmlElement("FechaCambio")]
        public String FechaCambio
        {
            get {
                    return fechaCambio; 
            }
            set {
                    fechaCambio = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// TipoctId
        /// </summary> 
        [XmlElement("TipoctId")]
        public String TipoctId
        {
            get {
                    return tipoctId; 
            }
            set {
                    tipoctId = value; 
            }
        }

        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public int TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public int NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        /// <summary>
        /// SubnivelId
        /// </summary> 
        [XmlElement("SubnivelId")]
        public int SubnivelId
        {
            get {
                    return subnivelId; 
            }
            set {
                    subnivelId = value; 
            }
        }

        /// <summary>
        /// ControlId
        /// </summary> 
        [XmlElement("ControlId")]
        public int ControlId
        {
            get {
                    return controlId; 
            }
            set {
                    controlId = value; 
            }
        }

        /// <summary>
        /// SubcontrolId
        /// </summary> 
        [XmlElement("SubcontrolId")]
        public int SubcontrolId
        {
            get {
                    return subcontrolId; 
            }
            set {
                    subcontrolId = value; 
            }
        }

        /// <summary>
        /// DependencianormativaId
        /// </summary> 
        [XmlElement("DependencianormativaId")]
        public int DependencianormativaId
        {
            get {
                    return dependencianormativaId; 
            }
            set {
                    dependencianormativaId = value; 
            }
        }

        /// <summary>
        /// DependenciaadministrativaId
        /// </summary> 
        [XmlElement("DependenciaadministrativaId")]
        public int DependenciaadministrativaId
        {
            get {
                    return dependenciaadministrativaId; 
            }
            set {
                    dependenciaadministrativaId = value; 
            }
        }

        /// <summary>
        /// ServicioId
        /// </summary> 
        [XmlElement("ServicioId")]
        public int ServicioId
        {
            get {
                    return servicioId; 
            }
            set {
                    servicioId = value; 
            }
        }

        /// <summary>
        /// SectorId
        /// </summary> 
        [XmlElement("SectorId")]
        public int SectorId
        {
            get {
                    return sectorId; 
            }
            set {
                    sectorId = value; 
            }
        }

        /// <summary>
        /// EducacionfisicaId
        /// </summary> 
        [XmlElement("EducacionfisicaId")]
        public int EducacionfisicaId
        {
            get {
                    return educacionfisicaId; 
            }
            set {
                    educacionfisicaId = value; 
            }
        }

        /// <summary>
        /// EstatusId
        /// </summary> 
        [XmlElement("EstatusId")]
        public int EstatusId
        {
            get {
                    return estatusId; 
            }
            set {
                    estatusId = value; 
            }
        }

        /// <summary>
        /// AlmacenId
        /// </summary> 
        [XmlElement("AlmacenId")]
        public int AlmacenId
        {
            get {
                    return almacenId; 
            }
            set {
                    almacenId = value; 
            }
        }

        /// <summary>
        /// PuntocardinalId
        /// </summary> 
        [XmlElement("PuntocardinalId")]
        public int PuntocardinalId
        {
            get {
                    return puntocardinalId; 
            }
            set {
                    puntocardinalId = value; 
            }
        }

        /// <summary>
        /// NumeroIncorporacion
        /// </summary> 
        [XmlElement("NumeroIncorporacion")]
        public String NumeroIncorporacion
        {
            get {
                    return numeroIncorporacion; 
            }
            set {
                    numeroIncorporacion = value; 
            }
        }

        /// <summary>
        /// Folio
        /// </summary> 
        [XmlElement("Folio")]
        public String Folio
        {
            get {
                    return folio; 
            }
            set {
                    folio = value; 
            }
        }

        /// <summary>
        /// DependenciaoperativaId
        /// </summary> 
        [XmlElement("DependenciaoperativaId")]
        public int DependenciaoperativaId
        {
            get {
                    return dependenciaoperativaId; 
            }
            set {
                    dependenciaoperativaId = value; 
            }
        }

        /// <summary>
        /// Observaciones
        /// </summary> 
        [XmlElement("Observaciones")]
        public String Observaciones
        {
            get {
                    return observaciones; 
            }
            set {
                    observaciones = value; 
            }
        }

        /// <summary>
        /// IncorporacionId
        /// </summary> 
        [XmlElement("IncorporacionId")]
        public int IncorporacionId
        {
            get {
                    return incorporacionId; 
            }
            set {
                    incorporacionId = value; 
            }
        }

        /// <summary>
        /// Fechasol
        /// </summary> 
        [XmlElement("Fechasol")]
        public String Fechasol
        {
            get {
                    return fechasol; 
            }
            set {
                    fechasol = value; 
            }
        }

        /// <summary>
        /// ClaveinstitucionalId
        /// </summary> 
        [XmlElement("ClaveinstitucionalId")]
        public int ClaveinstitucionalId
        {
            get {
                    return claveinstitucionalId; 
            }
            set {
                    claveinstitucionalId = value; 
            }
        }

        /// <summary>
        /// NiveleducacionId
        /// </summary> 
        [XmlElement("NiveleducacionId")]
        public int NiveleducacionId
        {
            get {
                    return niveleducacionId; 
            }
            set {
                    niveleducacionId = value; 
            }
        }

        /// <summary>
        /// ClaveagrupadorId
        /// </summary> 
        [XmlElement("ClaveagrupadorId")]
        public int ClaveagrupadorId
        {
            get {
                    return claveagrupadorId; 
            }
            set {
                    claveagrupadorId = value; 
            }
        }

        /// <summary>
        /// TurnoId
        /// </summary> 
        [XmlElement("TurnoId")]
        public int TurnoId
        {
            get {
                    return turnoId; 
            }
            set {
                    turnoId = value; 
            }
        }

        /// <summary>
        /// ValidDatos
        /// </summary> 
        [XmlElement("ValidDatos")]
        public Decimal ValidDatos
        {
            get {
                    return validDatos; 
            }
            set {
                    validDatos = value; 
            }
        }

        /// <summary>
        /// Region
        /// </summary> 
        [XmlElement("Region")]
        public RegionDP Region
        {
            get {
                    return region; 
            }
            set {
                    region = value; 
            }
        }

        /// <summary>
        /// Zona
        /// </summary> 
        [XmlElement("Zona")]
        public ZonaDP Zona
        {
            get {
                    return zona; 
            }
            set {
                    zona = value; 
            }
        }

        /// <summary>
        /// Sostenimiento
        /// </summary> 
        [XmlElement("Sostenimiento")]
        public SostenimientoDP Sostenimiento
        {
            get {
                    return sostenimiento; 
            }
            set {
                    sostenimiento = value; 
            }
        }

        /// <summary>
        /// Motivobajacentrotrabajo
        /// </summary> 
        [XmlElement("Motivobajacentrotrabajo")]
        public MotivobajacentrotrabajoDP Motivobajacentrotrabajo
        {
            get {
                    return motivobajacentrotrabajo; 
            }
            set {
                    motivobajacentrotrabajo = value; 
            }
        }

        /// <summary>
        /// Dependenciaadministrativa
        /// </summary> 
        [XmlElement("Dependenciaadministrativa")]
        public DependenciaadministrativaDP Dependenciaadministrativa
        {
            get {
                    return dependenciaadministrativa; 
            }
            set {
                    dependenciaadministrativa = value; 
            }
        }

        /// <summary>
        /// Sector
        /// </summary> 
        [XmlElement("Sector")]
        public SectorDP Sector
        {
            get {
                    return sector; 
            }
            set {
                    sector = value; 
            }
        }

        /// <summary>
        /// Niveleducacion
        /// </summary> 
        [XmlElement("Niveleducacion")]
        public NiveleducacionbasicaDP Niveleducacion
        {
            get {
                    return niveleducacion; 
            }
            set {
                    niveleducacion = value; 
            }
        }

        /// <summary>
        /// Llave primaria de CentrotrabajoPk
        /// </summary>
        [XmlElement("Pk")]
        public CentrotrabajoPk Pk {
            get {
                    return new CentrotrabajoPk( centrotrabajoId );
            }
        }
        #endregion.
    }
}
