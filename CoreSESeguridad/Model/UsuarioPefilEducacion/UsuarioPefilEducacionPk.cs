using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "UsuarioPefilEducacionPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: miércoles, 22 de julio de 2009.</Para>
    /// <Para>Hora: 05:39:48 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "UsuarioPefilEducacionPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuarioId</term><description>Descripcion UsuIdUsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioPefilEducacionPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioPefilEducacionPk usuariopefileducacionPk = new UsuarioPefilEducacionPk(usuariopefileducacion);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("UsuarioPefilEducacionPk")]
    public class UsuarioPefilEducacionPk
    {
        #region Definicion de campos privados.
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Int32 usuIdUsuarioId;
        private Int16 cicloescolarId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuarioId
        /// </summary> 
        [XmlElement("UsuIdUsuarioId")]
        public Int32 UsuIdUsuarioId
        {
            get {
                    return usuIdUsuarioId; 
            }
            set {
                    usuIdUsuarioId = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de UsuarioPefilEducacionPk.
        /// </summary>
        /// <param name="perfilEducacionId">Descripción perfilEducacionId del tipo Int16.</param>
        /// <param name="niveltrabajoId">Descripción niveltrabajoId del tipo Byte.</param>
        /// <param name="usuIdUsuarioId">Descripción usuIdUsuarioId del tipo Int32.</param>
        /// <param name="cicloescolarId">Descripción cicloescolarId del tipo Int16.</param>
        public UsuarioPefilEducacionPk(Int16 perfilEducacionId, Byte niveltrabajoId, Int32 usuIdUsuarioId, Int16 cicloescolarId) 
        {
            this.perfilEducacionId = perfilEducacionId;
            this.niveltrabajoId = niveltrabajoId;
            this.usuIdUsuarioId = usuIdUsuarioId;
            this.cicloescolarId = cicloescolarId;
        }
        /// <summary>
        /// Constructor normal de UsuarioPefilEducacionPk.
        /// </summary>
        public UsuarioPefilEducacionPk() 
        {
        }
        #endregion.
    }
}
