using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class UsuarioPefilEducacionMD
    {
        private UsuarioPefilEducacionDP usuarioPefilEducacion = null;

        public UsuarioPefilEducacionMD(UsuarioPefilEducacionDP usuarioPefilEducacion)
        {
            this.usuarioPefilEducacion = usuarioPefilEducacion;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert UsuarioPefilEducacion
            DbCommand com = con.CreateCommand();
            String insertUsuarioPefilEducacion = String.Format(CultureInfo.CurrentCulture,UsuarioPefilEducacion.UsuarioPefilEducacionResx.UsuarioPefilEducacionInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Usu_Id_Usuario",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_CicloEscolar");
            com.CommandText = insertUsuarioPefilEducacion;
            #endregion
            #region Parametros Insert UsuarioPefilEducacion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioPefilEducacion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioPefilEducacion.NiveltrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioPefilEducacion.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioPefilEducacion.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioPefilEducacion.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuarioPefilEducacion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioPefilEducacion.CicloescolarId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert UsuarioPefilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find UsuarioPefilEducacion
            DbCommand com = con.CreateCommand();
            String findUsuarioPefilEducacion = String.Format(CultureInfo.CurrentCulture,UsuarioPefilEducacion.UsuarioPefilEducacionResx.UsuarioPefilEducacionFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar");
            com.CommandText = findUsuarioPefilEducacion;
            #endregion
            #region Parametros Find UsuarioPefilEducacion
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioPefilEducacion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioPefilEducacion.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioPefilEducacion.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioPefilEducacion.CicloescolarId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find UsuarioPefilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete UsuarioPefilEducacion
            DbCommand com = con.CreateCommand();
            String deleteUsuarioPefilEducacion = String.Format(CultureInfo.CurrentCulture,UsuarioPefilEducacion.UsuarioPefilEducacionResx.UsuarioPefilEducacionDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar");
            com.CommandText = deleteUsuarioPefilEducacion;
            #endregion
            #region Parametros Delete UsuarioPefilEducacion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioPefilEducacion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioPefilEducacion.NiveltrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioPefilEducacion.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioPefilEducacion.CicloescolarId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete UsuarioPefilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update UsuarioPefilEducacion
            DbCommand com = con.CreateCommand();
            String updateUsuarioPefilEducacion = String.Format(CultureInfo.CurrentCulture,UsuarioPefilEducacion.UsuarioPefilEducacionResx.UsuarioPefilEducacionUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar");
            com.CommandText = updateUsuarioPefilEducacion;
            #endregion
            #region Parametros Update UsuarioPefilEducacion
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioPefilEducacion.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioPefilEducacion.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuarioPefilEducacion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioPefilEducacion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioPefilEducacion.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioPefilEducacion.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioPefilEducacion.CicloescolarId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update UsuarioPefilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static UsuarioPefilEducacionDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            UsuarioPefilEducacionDP usuarioPefilEducacion = new UsuarioPefilEducacionDP();
            usuarioPefilEducacion.PerfilEducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            usuarioPefilEducacion.NiveltrabajoId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            usuarioPefilEducacion.UsuIdUsuarioId = dr.IsDBNull(2) ? 0 : dr.GetInt32(2);
            usuarioPefilEducacion.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            usuarioPefilEducacion.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            usuarioPefilEducacion.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            usuarioPefilEducacion.CicloescolarId = dr.IsDBNull(6) ? (short)0 : dr.GetInt16(6);
            return usuarioPefilEducacion;
            #endregion
        }
        public static UsuarioPefilEducacionDP Load(DbConnection con, DbTransaction tran, UsuarioPefilEducacionPk pk)
        {
            #region SQL Load UsuarioPefilEducacion
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadUsuarioPefilEducacion = String.Format(CultureInfo.CurrentCulture,UsuarioPefilEducacion.UsuarioPefilEducacionResx.UsuarioPefilEducacionSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar");
            com.CommandText = loadUsuarioPefilEducacion;
            #endregion
            #region Parametros Load UsuarioPefilEducacion
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.CicloescolarId);
            #endregion
            UsuarioPefilEducacionDP usuarioPefilEducacion;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load UsuarioPefilEducacion
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        usuarioPefilEducacion = ReadRow(dr);
                    }
                    else
                        usuarioPefilEducacion = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load UsuarioPefilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return usuarioPefilEducacion;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count UsuarioPefilEducacion
            DbCommand com = con.CreateCommand();
            String countUsuarioPefilEducacion = String.Format(CultureInfo.CurrentCulture,UsuarioPefilEducacion.UsuarioPefilEducacionResx.UsuarioPefilEducacionCount,"");
            com.CommandText = countUsuarioPefilEducacion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count UsuarioPefilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static UsuarioPefilEducacionDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List UsuarioPefilEducacion
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuarioPefilEducacion = String.Format(CultureInfo.CurrentCulture, UsuarioPefilEducacion.UsuarioPefilEducacionResx.UsuarioPefilEducacionSelectAll, "", ConstantesGlobales.IncluyeRows);
            listUsuarioPefilEducacion = listUsuarioPefilEducacion.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuarioPefilEducacion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load UsuarioPefilEducacion
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista UsuarioPefilEducacion
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista UsuarioPefilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (UsuarioPefilEducacionDP[])lista.ToArray(typeof(UsuarioPefilEducacionDP));
        }
    }
}
