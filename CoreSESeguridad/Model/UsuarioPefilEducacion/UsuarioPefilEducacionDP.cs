using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "UsuarioPefilEducacion".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: miércoles, 22 de julio de 2009.</Para>
    /// <Para>Hora: 05:39:48 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "UsuarioPefilEducacion".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuarioId</term><description>Descripcion UsuIdUsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacion</term><description>Descripcion PerfilEducacion</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuario</term><description>Descripcion UsuIdUsuario</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioPefilEducacionDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioPefilEducacionDTO usuariopefileducacion = new UsuarioPefilEducacionDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("UsuarioPefilEducacion")]
    public class UsuarioPefilEducacionDP
    {
        #region Definicion de campos privados.
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Int32 usuIdUsuarioId;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Int16 cicloescolarId;
        private PerfilEducacionDP perfilEducacion;
        private UsuarioDP usuIdUsuario;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuarioId
        /// </summary> 
        [XmlElement("UsuIdUsuarioId")]
        public Int32 UsuIdUsuarioId
        {
            get {
                    return usuIdUsuarioId; 
            }
            set {
                    usuIdUsuarioId = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// PerfilEducacion
        /// </summary> 
        [XmlElement("PerfilEducacion")]
        public PerfilEducacionDP PerfilEducacion
        {
            get {
                    return perfilEducacion; 
            }
            set {
                    perfilEducacion = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuario
        /// </summary> 
        [XmlElement("UsuIdUsuario")]
        public UsuarioDP UsuIdUsuario
        {
            get {
                    return usuIdUsuario; 
            }
            set {
                    usuIdUsuario = value; 
            }
        }

        /// <summary>
        /// Llave primaria de UsuarioPefilEducacionPk
        /// </summary>
        [XmlElement("Pk")]
        public UsuarioPefilEducacionPk Pk {
            get {
                    return new UsuarioPefilEducacionPk( perfilEducacionId, niveltrabajoId, usuIdUsuarioId, cicloescolarId );
            }
        }
        #endregion.
    }
}
