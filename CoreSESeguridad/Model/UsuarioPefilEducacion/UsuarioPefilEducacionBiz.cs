using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class UsuarioPefilEducacionBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, UsuarioPefilEducacionDP usuarioPefilEducacion)
        {
            #region SaveDetail
            #region FK PerfilEducacion
            PerfilEducacionDP perfilEducacion = usuarioPefilEducacion.PerfilEducacion;
            if (perfilEducacion != null) 
            {
                PerfilEducacionMD perfilEducacionMd = new PerfilEducacionMD(perfilEducacion);
                if (!perfilEducacionMd.Find(con, tran))
                    perfilEducacionMd.Insert(con, tran);
            }
            #endregion
            #region FK Usuario
            UsuarioDP usuIdUsuario = usuarioPefilEducacion.UsuIdUsuario;
            if (usuIdUsuario != null) 
            {
                UsuarioMD usuarioMd = new UsuarioMD(usuIdUsuario);
                if (!usuarioMd.Find(con, tran))
                    usuarioMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, UsuarioPefilEducacionDP usuarioPefilEducacion)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, usuarioPefilEducacion);
                        resultado = InternalSave(con, tran, usuarioPefilEducacion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla usuarioPefilEducacion!";
                        throw new BusinessException("Error interno al intentar guardar usuarioPefilEducacion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla usuarioPefilEducacion!";
                throw new BusinessException("Error interno al intentar guardar usuarioPefilEducacion!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, UsuarioPefilEducacionDP usuarioPefilEducacion)
        {
            UsuarioPefilEducacionMD usuarioPefilEducacionMd = new UsuarioPefilEducacionMD(usuarioPefilEducacion);
            String resultado = "";
            if (usuarioPefilEducacionMd.Find(con, tran))
            {
                UsuarioPefilEducacionDP usuarioPefilEducacionAnterior = UsuarioPefilEducacionMD.Load(con, tran, usuarioPefilEducacion.Pk);
                if (usuarioPefilEducacionMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla usuarioPefilEducacion!";
                    throw new BusinessException("No se pude actualizar la tabla usuarioPefilEducacion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (usuarioPefilEducacionMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla usuarioPefilEducacion!";
                    throw new BusinessException("No se pude insertar en la tabla usuarioPefilEducacion!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, UsuarioPefilEducacionDP usuarioPefilEducacion)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, usuarioPefilEducacion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla usuarioPefilEducacion!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla usuarioPefilEducacion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla usuarioPefilEducacion!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla usuarioPefilEducacion!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, UsuarioPefilEducacionDP usuarioPefilEducacion)
        {
            String resultado = "";
            UsuarioPefilEducacionMD usuarioPefilEducacionMd = new UsuarioPefilEducacionMD(usuarioPefilEducacion);
            if (usuarioPefilEducacionMd.Find(con, tran))
            {
                if (usuarioPefilEducacionMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla usuarioPefilEducacion!";
                    throw new BusinessException("No se pude eliminar en la tabla usuarioPefilEducacion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla usuarioPefilEducacion!";
                throw new BusinessException("No se pude eliminar en la tabla usuarioPefilEducacion!");
            }
            return resultado;
        }

        internal static UsuarioPefilEducacionDP LoadDetail(DbConnection con, DbTransaction tran, UsuarioPefilEducacionDP usuarioPefilEducacionAnterior, UsuarioPefilEducacionDP usuarioPefilEducacion) 
        {
            #region FK PerfilEducacion
            PerfilEducacionPk perfilEducacionPk = new PerfilEducacionPk();
            if (usuarioPefilEducacionAnterior != null)
            {
                if (perfilEducacionPk.Equals(usuarioPefilEducacionAnterior.Pk))
                    usuarioPefilEducacion.PerfilEducacion = usuarioPefilEducacionAnterior.PerfilEducacion;
                else
                    usuarioPefilEducacion.PerfilEducacion = PerfilEducacionMD.Load(con, tran, perfilEducacionPk);
            }
            else
                usuarioPefilEducacion.PerfilEducacion = PerfilEducacionMD.Load(con, tran, perfilEducacionPk);
            #endregion
            #region FK UsuIdUsuario
            UsuarioPk usuIdUsuarioPk = new UsuarioPk(
                usuarioPefilEducacion.UsuIdUsuarioId);
            if (usuarioPefilEducacionAnterior != null)
            {
                if (usuIdUsuarioPk.Equals(usuarioPefilEducacionAnterior.Pk))
                    usuarioPefilEducacion.UsuIdUsuario = usuarioPefilEducacionAnterior.UsuIdUsuario;
                else
                    usuarioPefilEducacion.UsuIdUsuario = UsuarioMD.Load(con, tran, usuIdUsuarioPk);
            }
            else
                usuarioPefilEducacion.UsuIdUsuario = UsuarioMD.Load(con, tran, usuIdUsuarioPk);
            #endregion
            return usuarioPefilEducacion;
        }
        public static UsuarioPefilEducacionDP Load(DbConnection con, UsuarioPefilEducacionPk pk)
        {
            UsuarioPefilEducacionDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioPefilEducacionMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un UsuarioPefilEducacion!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioPefilEducacionMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un UsuarioPefilEducacion!", ex);
            }
            return resultado;
        }

        public static UsuarioPefilEducacionDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            UsuarioPefilEducacionDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioPefilEducacionMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de UsuarioPefilEducacion!", ex);
            }
            return resultado;

        }
    }
}
