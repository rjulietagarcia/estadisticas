using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "InmueblePk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:30:07 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "InmueblePk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>InmuebleId</term><description>Descripcion InmuebleId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "InmueblePkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// InmueblePk inmueblePk = new InmueblePk(inmueble);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("InmueblePk")]
    public class InmueblePk
    {
        #region Definicion de campos privados.
        private Int32 inmuebleId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// InmuebleId
        /// </summary> 
        [XmlElement("InmuebleId")]
        public Int32 InmuebleId
        {
            get {
                    return inmuebleId; 
            }
            set {
                    inmuebleId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de InmueblePk.
        /// </summary>
        /// <param name="inmuebleId">Descripción inmuebleId del tipo Int32.</param>
        public InmueblePk(Int32 inmuebleId) 
        {
            this.inmuebleId = inmuebleId;
        }
        /// <summary>
        /// Constructor normal de InmueblePk.
        /// </summary>
        public InmueblePk() 
        {
        }
        #endregion.
    }
}
