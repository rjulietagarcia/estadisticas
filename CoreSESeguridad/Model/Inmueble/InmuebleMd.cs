using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class InmuebleMD
    {
        private InmuebleDP inmueble = null;

        public InmuebleMD(InmuebleDP inmueble)
        {
            this.inmueble = inmueble;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Inmueble
            DbCommand com = con.CreateCommand();
            String insertInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Inmueble",
                        "Id_Domicilio",
                        "Clave",
                        "Fecha_Alta",
                        "AGEB",
                        "Carta_Topografica",
                        "Clave_Cartografica",
                        "Bit_Provisional",
                        "Nombre",
                        "Longitud",
                        "Latitud",
                        "Altitud",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Id_Ct_Responsable",
                        "Fecha_Actualizacion");
            com.CommandText = insertInmueble;
            #endregion
            #region Parametros Insert Inmueble
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.InmuebleId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.DomicilioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 8, ParameterDirection.Input, inmueble.Clave);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Alta",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(inmueble.FechaAlta,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}AGEB",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, inmueble.Ageb);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Carta_Topografica",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, inmueble.CartaTopografica);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clave_Cartografica",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, inmueble.ClaveCartografica);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Provisional",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, inmueble.BitProvisional);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, inmueble.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Longitud",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.Longitud);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Latitud",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.Latitud);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Altitud",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.Altitud);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, inmueble.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Ct_Responsable",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.CtResponsableId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(inmueble.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Inmueble
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertInmueble = String.Format(Inmueble.InmuebleResx.InmuebleInsert, "", 
                    inmueble.InmuebleId.ToString(),
                    inmueble.DomicilioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,inmueble.Clave),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(inmueble.FechaAlta).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,inmueble.Ageb),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,inmueble.CartaTopografica),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,inmueble.ClaveCartografica),
                    inmueble.BitProvisional.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,inmueble.Nombre),
                    inmueble.Longitud.ToString(),
                    inmueble.Latitud.ToString(),
                    inmueble.Altitud.ToString(),
                    inmueble.BitActivo.ToString(),
                    inmueble.UsuarioId.ToString(),
                    inmueble.CtResponsableId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(inmueble.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertInmueble);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Inmueble
            DbCommand com = con.CreateCommand();
            String findInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Inmueble");
            com.CommandText = findInmueble;
            #endregion
            #region Parametros Find Inmueble
            Common.CreateParameter(com, String.Format("{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.InmuebleId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Inmueble
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleFind, "", 
                    inmueble.InmuebleId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindInmueble);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Inmueble
            DbCommand com = con.CreateCommand();
            String deleteInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Inmueble");
            com.CommandText = deleteInmueble;
            #endregion
            #region Parametros Delete Inmueble
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.InmuebleId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Inmueble
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleDelete, "", 
                    inmueble.InmuebleId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteInmueble);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Inmueble
            DbCommand com = con.CreateCommand();
            String updateInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Domicilio",
                        "Clave",
                        "Fecha_Alta",
                        "AGEB",
                        "Carta_Topografica",
                        "Clave_Cartografica",
                        "Bit_Provisional",
                        "Nombre",
                        "Longitud",
                        "Latitud",
                        "Altitud",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Id_Ct_Responsable",
                        "Fecha_Actualizacion",
                        "Id_Inmueble");
            com.CommandText = updateInmueble;
            #endregion
            #region Parametros Update Inmueble
            Common.CreateParameter(com, String.Format("{0}Id_Domicilio",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.DomicilioId);
            Common.CreateParameter(com, String.Format("{0}Clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 8, ParameterDirection.Input, inmueble.Clave);
            Common.CreateParameter(com, String.Format("{0}Fecha_Alta",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(inmueble.FechaAlta,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}AGEB",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, inmueble.Ageb);
            Common.CreateParameter(com, String.Format("{0}Carta_Topografica",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, inmueble.CartaTopografica);
            Common.CreateParameter(com, String.Format("{0}Clave_Cartografica",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, inmueble.ClaveCartografica);
            Common.CreateParameter(com, String.Format("{0}Bit_Provisional",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, inmueble.BitProvisional);
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, inmueble.Nombre);
            Common.CreateParameter(com, String.Format("{0}Longitud",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.Longitud);
            Common.CreateParameter(com, String.Format("{0}Latitud",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.Latitud);
            Common.CreateParameter(com, String.Format("{0}Altitud",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.Altitud);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, inmueble.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_Ct_Responsable",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.CtResponsableId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(inmueble.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, inmueble.InmuebleId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Inmueble
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateInmueble = String.Format(Inmueble.InmuebleResx.InmuebleUpdate, "", 
                    inmueble.DomicilioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,inmueble.Clave),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(inmueble.FechaAlta).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteString,inmueble.Ageb),
                    String.Format(ConstantesGlobales.ConvierteString,inmueble.CartaTopografica),
                    String.Format(ConstantesGlobales.ConvierteString,inmueble.ClaveCartografica),
                    inmueble.BitProvisional.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,inmueble.Nombre),
                    inmueble.Longitud.ToString(),
                    inmueble.Latitud.ToString(),
                    inmueble.Altitud.ToString(),
                    inmueble.BitActivo.ToString(),
                    inmueble.UsuarioId.ToString(),
                    inmueble.CtResponsableId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(inmueble.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    inmueble.InmuebleId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateInmueble);
                #endregion
            }
            return resultado;
        }
        protected static InmuebleDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            InmuebleDP inmueble = new InmuebleDP();
            inmueble.InmuebleId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            inmueble.DomicilioId = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            inmueble.Clave = dr.IsDBNull(2) ? "" : dr.GetString(2);
            inmueble.FechaAlta = dr.IsDBNull(3) ? "" : dr.GetDateTime(3).ToShortDateString();;
            inmueble.Ageb = dr.IsDBNull(4) ? "" : dr.GetString(4);
            inmueble.CartaTopografica = dr.IsDBNull(5) ? "" : dr.GetString(5);
            inmueble.ClaveCartografica = dr.IsDBNull(6) ? "" : dr.GetString(6);
            inmueble.BitProvisional = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            inmueble.Nombre = dr.IsDBNull(8) ? "" : dr.GetString(8);
            inmueble.Longitud = dr.IsDBNull(9) ? 0 : dr.GetInt32(9);
            inmueble.Latitud = dr.IsDBNull(10) ? 0 : dr.GetInt32(10);
            inmueble.Altitud = dr.IsDBNull(11) ? 0 : dr.GetInt32(11);
            inmueble.BitActivo = dr.IsDBNull(12) ? false : dr.GetBoolean(12);
            inmueble.UsuarioId = dr.IsDBNull(13) ? 0 : dr.GetInt32(13);
            inmueble.CtResponsableId = dr.IsDBNull(14) ? 0 : dr.GetInt32(14);
            inmueble.FechaActualizacion = dr.IsDBNull(15) ? "" : dr.GetDateTime(15).ToShortDateString();;
            return inmueble;
            #endregion
        }
        public static InmuebleDP Load(DbConnection con, DbTransaction tran, InmueblePk pk)
        {
            #region SQL Load Inmueble
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Inmueble");
            com.CommandText = loadInmueble;
            #endregion
            #region Parametros Load Inmueble
            Common.CreateParameter(com, String.Format("{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.InmuebleId);
            #endregion
            InmuebleDP inmueble;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Inmueble
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        inmueble = ReadRow(dr);
                    }
                    else
                        inmueble = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Inmueble
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleSelect, "", 
                    pk.InmuebleId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadInmueble);
                inmueble = null;
                #endregion
            }
            return inmueble;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Inmueble
            DbCommand com = con.CreateCommand();
            String countInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleCount,"");
            com.CommandText = countInmueble;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Inmueble
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountInmueble);
                #endregion
            }
            return resultado;
        }
        public static InmuebleDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Inmueble
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listInmueble = String.Format(CultureInfo.CurrentCulture, Inmueble.InmuebleResx.InmuebleSelectAll, "", ConstantesGlobales.IncluyeRows);
            listInmueble = listInmueble.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listInmueble, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Inmueble
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Inmueble
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Inmueble
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaInmueble = String.Format(CultureInfo.CurrentCulture,Inmueble.InmuebleResx.InmuebleSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaInmueble);
                #endregion
            }
            return (InmuebleDP[])lista.ToArray(typeof(InmuebleDP));
        }
    }
}
