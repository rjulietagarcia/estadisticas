using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Inmueble".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:30:07 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Inmueble".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>InmuebleId</term><description>Descripcion InmuebleId</description>
    ///    </item>
    ///    <item>
    ///        <term>DomicilioId</term><description>Descripcion DomicilioId</description>
    ///    </item>
    ///    <item>
    ///        <term>Clave</term><description>Descripcion Clave</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaAlta</term><description>Descripcion FechaAlta</description>
    ///    </item>
    ///    <item>
    ///        <term>Ageb</term><description>Descripcion Ageb</description>
    ///    </item>
    ///    <item>
    ///        <term>CartaTopografica</term><description>Descripcion CartaTopografica</description>
    ///    </item>
    ///    <item>
    ///        <term>ClaveCartografica</term><description>Descripcion ClaveCartografica</description>
    ///    </item>
    ///    <item>
    ///        <term>BitProvisional</term><description>Descripcion BitProvisional</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>Longitud</term><description>Descripcion Longitud</description>
    ///    </item>
    ///    <item>
    ///        <term>Latitud</term><description>Descripcion Latitud</description>
    ///    </item>
    ///    <item>
    ///        <term>Altitud</term><description>Descripcion Altitud</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>CtResponsableId</term><description>Descripcion CtResponsableId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>Domicilio</term><description>Descripcion Domicilio</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "InmuebleDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// InmuebleDTO inmueble = new InmuebleDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Inmueble")]
    public class InmuebleDP
    {
        #region Definicion de campos privados.
        private Int32 inmuebleId;
        private Int32 domicilioId;
        private String clave;
        private String fechaAlta;
        private String ageb;
        private String cartaTopografica;
        private String claveCartografica;
        private Boolean bitProvisional;
        private String nombre;
        private Int32 longitud;
        private Int32 latitud;
        private Int32 altitud;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private Int32 ctResponsableId;
        private String fechaActualizacion;
        private DomicilioDP domicilio;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// InmuebleId
        /// </summary> 
        [XmlElement("InmuebleId")]
        public Int32 InmuebleId
        {
            get {
                    return inmuebleId; 
            }
            set {
                    inmuebleId = value; 
            }
        }

        /// <summary>
        /// DomicilioId
        /// </summary> 
        [XmlElement("DomicilioId")]
        public Int32 DomicilioId
        {
            get {
                    return domicilioId; 
            }
            set {
                    domicilioId = value; 
            }
        }

        /// <summary>
        /// Clave
        /// </summary> 
        [XmlElement("Clave")]
        public String Clave
        {
            get {
                    return clave; 
            }
            set {
                    clave = value; 
            }
        }

        /// <summary>
        /// FechaAlta
        /// </summary> 
        [XmlElement("FechaAlta")]
        public String FechaAlta
        {
            get {
                    return fechaAlta; 
            }
            set {
                    fechaAlta = value; 
            }
        }

        /// <summary>
        /// Ageb
        /// </summary> 
        [XmlElement("Ageb")]
        public String Ageb
        {
            get {
                    return ageb; 
            }
            set {
                    ageb = value; 
            }
        }

        /// <summary>
        /// CartaTopografica
        /// </summary> 
        [XmlElement("CartaTopografica")]
        public String CartaTopografica
        {
            get {
                    return cartaTopografica; 
            }
            set {
                    cartaTopografica = value; 
            }
        }

        /// <summary>
        /// ClaveCartografica
        /// </summary> 
        [XmlElement("ClaveCartografica")]
        public String ClaveCartografica
        {
            get {
                    return claveCartografica; 
            }
            set {
                    claveCartografica = value; 
            }
        }

        /// <summary>
        /// BitProvisional
        /// </summary> 
        [XmlElement("BitProvisional")]
        public Boolean BitProvisional
        {
            get {
                    return bitProvisional; 
            }
            set {
                    bitProvisional = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// Longitud
        /// </summary> 
        [XmlElement("Longitud")]
        public Int32 Longitud
        {
            get {
                    return longitud; 
            }
            set {
                    longitud = value; 
            }
        }

        /// <summary>
        /// Latitud
        /// </summary> 
        [XmlElement("Latitud")]
        public Int32 Latitud
        {
            get {
                    return latitud; 
            }
            set {
                    latitud = value; 
            }
        }

        /// <summary>
        /// Altitud
        /// </summary> 
        [XmlElement("Altitud")]
        public Int32 Altitud
        {
            get {
                    return altitud; 
            }
            set {
                    altitud = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// CtResponsableId
        /// </summary> 
        [XmlElement("CtResponsableId")]
        public Int32 CtResponsableId
        {
            get {
                    return ctResponsableId; 
            }
            set {
                    ctResponsableId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// Domicilio
        /// </summary> 
        [XmlElement("Domicilio")]
        public DomicilioDP Domicilio
        {
            get {
                    return domicilio; 
            }
            set {
                    domicilio = value; 
            }
        }

        /// <summary>
        /// Llave primaria de InmueblePk
        /// </summary>
        [XmlElement("Pk")]
        public InmueblePk Pk {
            get {
                    return new InmueblePk( inmuebleId );
            }
        }
        #endregion.
    }
}
