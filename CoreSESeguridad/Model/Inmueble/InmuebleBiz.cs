using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class InmuebleBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, InmuebleDP inmueble)
        {
            #region SaveDetail
            #region FK Domicilio
            DomicilioDP domicilio = inmueble.Domicilio;
            if (domicilio != null) 
            {
                DomicilioMD domicilioMd = new DomicilioMD(domicilio);
                if (!domicilioMd.Find(con, tran))
                    domicilioMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, InmuebleDP inmueble)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, inmueble);
                        resultado = InternalSave(con, tran, inmueble);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla inmueble!";
                        throw new BusinessException("Error interno al intentar guardar inmueble!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla inmueble!";
                throw new BusinessException("Error interno al intentar guardar inmueble!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, InmuebleDP inmueble)
        {
            InmuebleMD inmuebleMd = new InmuebleMD(inmueble);
            String resultado = "";
            if (inmuebleMd.Find(con, tran))
            {
                InmuebleDP inmuebleAnterior = InmuebleMD.Load(con, tran, inmueble.Pk);
                if (inmuebleMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla inmueble!";
                    throw new BusinessException("No se pude actualizar la tabla inmueble!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (inmuebleMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla inmueble!";
                    throw new BusinessException("No se pude insertar en la tabla inmueble!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, InmuebleDP inmueble)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, inmueble);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla inmueble!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla inmueble!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla inmueble!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla inmueble!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, InmuebleDP inmueble)
        {
            String resultado = "";
            InmuebleMD inmuebleMd = new InmuebleMD(inmueble);
            if (inmuebleMd.Find(con, tran))
            {
                if (inmuebleMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla inmueble!";
                    throw new BusinessException("No se pude eliminar en la tabla inmueble!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla inmueble!";
                throw new BusinessException("No se pude eliminar en la tabla inmueble!");
            }
            return resultado;
        }

        internal static InmuebleDP LoadDetail(DbConnection con, DbTransaction tran, InmuebleDP inmuebleAnterior, InmuebleDP inmueble) 
        {
            #region FK Domicilio
            DomicilioPk domicilioPk = new DomicilioPk();
            if (inmuebleAnterior != null)
            {
                if (domicilioPk.Equals(inmuebleAnterior.Pk))
                    inmueble.Domicilio = inmuebleAnterior.Domicilio;
                else
                    inmueble.Domicilio = DomicilioMD.Load(con, tran, domicilioPk);
            }
            else
                inmueble.Domicilio = DomicilioMD.Load(con, tran, domicilioPk);
            #endregion
            return inmueble;
        }
        public static InmuebleDP Load(DbConnection con, InmueblePk pk)
        {
            InmuebleDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = InmuebleMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Inmueble!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = InmuebleMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Inmueble!", ex);
            }
            return resultado;
        }

        public static InmuebleDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            InmuebleDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = InmuebleMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Inmueble!", ex);
            }
            return resultado;

        }
    }
}
