using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class NiveltrabajoBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, NiveltrabajoDP niveltrabajo)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, NiveltrabajoDP niveltrabajo)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, niveltrabajo);
                        resultado = InternalSave(con, tran, niveltrabajo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla niveltrabajo!";
                        throw new BusinessException("Error interno al intentar guardar niveltrabajo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla niveltrabajo!";
                throw new BusinessException("Error interno al intentar guardar niveltrabajo!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, NiveltrabajoDP niveltrabajo)
        {
            NiveltrabajoMD niveltrabajoMd = new NiveltrabajoMD(niveltrabajo);
            String resultado = "";
            if (niveltrabajoMd.Find(con, tran))
            {
                NiveltrabajoDP niveltrabajoAnterior = NiveltrabajoMD.Load(con, tran, niveltrabajo.Pk);
                if (niveltrabajoMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla niveltrabajo!";
                    throw new BusinessException("No se pude actualizar la tabla niveltrabajo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (niveltrabajoMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla niveltrabajo!";
                    throw new BusinessException("No se pude insertar en la tabla niveltrabajo!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, NiveltrabajoDP niveltrabajo)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, niveltrabajo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla niveltrabajo!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla niveltrabajo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla niveltrabajo!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla niveltrabajo!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, NiveltrabajoDP niveltrabajo)
        {
            String resultado = "";
            NiveltrabajoMD niveltrabajoMd = new NiveltrabajoMD(niveltrabajo);
            if (niveltrabajoMd.Find(con, tran))
            {
                if (niveltrabajoMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla niveltrabajo!";
                    throw new BusinessException("No se pude eliminar en la tabla niveltrabajo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla niveltrabajo!";
                throw new BusinessException("No se pude eliminar en la tabla niveltrabajo!");
            }
            return resultado;
        }

        internal static NiveltrabajoDP LoadDetail(DbConnection con, DbTransaction tran, NiveltrabajoDP niveltrabajoAnterior, NiveltrabajoDP niveltrabajo) 
        {
            return niveltrabajo;
        }
        public static NiveltrabajoDP Load(DbConnection con, NiveltrabajoPk pk)
        {
            NiveltrabajoDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = NiveltrabajoMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Niveltrabajo!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = NiveltrabajoMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Niveltrabajo!", ex);
            }
            return resultado;
        }

        public static NiveltrabajoDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            NiveltrabajoDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = NiveltrabajoMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Niveltrabajo!", ex);
            }
            return resultado;

        }
    }
}
