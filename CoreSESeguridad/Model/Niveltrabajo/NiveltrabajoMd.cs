using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class NiveltrabajoMD
    {
        private NiveltrabajoDP niveltrabajo = null;

        public NiveltrabajoMD(NiveltrabajoDP niveltrabajo)
        {
            this.niveltrabajo = niveltrabajo;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Niveltrabajo
            DbCommand com = con.CreateCommand();
            String insertNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_NivelTrabajo",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertNiveltrabajo;
            #endregion
            #region Parametros Insert Niveltrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, niveltrabajo.NiveltrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, niveltrabajo.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, niveltrabajo.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, niveltrabajo.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, niveltrabajo.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(niveltrabajo.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Niveltrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertNiveltrabajo = String.Format(Niveltrabajo.NiveltrabajoResx.NiveltrabajoInsert, "", 
                    niveltrabajo.NiveltrabajoId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,niveltrabajo.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,niveltrabajo.Abreviatura),
                    niveltrabajo.BitActivo.ToString(),
                    niveltrabajo.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(niveltrabajo.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertNiveltrabajo);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Niveltrabajo
            DbCommand com = con.CreateCommand();
            String findNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_NivelTrabajo");
            com.CommandText = findNiveltrabajo;
            #endregion
            #region Parametros Find Niveltrabajo
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, niveltrabajo.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Niveltrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoFind, "", 
                    niveltrabajo.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindNiveltrabajo);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Niveltrabajo
            DbCommand com = con.CreateCommand();
            String deleteNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_NivelTrabajo");
            com.CommandText = deleteNiveltrabajo;
            #endregion
            #region Parametros Delete Niveltrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, niveltrabajo.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Niveltrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoDelete, "", 
                    niveltrabajo.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteNiveltrabajo);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Niveltrabajo
            DbCommand com = con.CreateCommand();
            String updateNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_NivelTrabajo");
            com.CommandText = updateNiveltrabajo;
            #endregion
            #region Parametros Update Niveltrabajo
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, niveltrabajo.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, niveltrabajo.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, niveltrabajo.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, niveltrabajo.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(niveltrabajo.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, niveltrabajo.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Niveltrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateNiveltrabajo = String.Format(Niveltrabajo.NiveltrabajoResx.NiveltrabajoUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,niveltrabajo.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,niveltrabajo.Abreviatura),
                    niveltrabajo.BitActivo.ToString(),
                    niveltrabajo.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(niveltrabajo.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    niveltrabajo.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateNiveltrabajo);
                #endregion
            }
            return resultado;
        }
        protected static NiveltrabajoDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            NiveltrabajoDP niveltrabajo = new NiveltrabajoDP();
            niveltrabajo.NiveltrabajoId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            niveltrabajo.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            niveltrabajo.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            niveltrabajo.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            niveltrabajo.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            niveltrabajo.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            return niveltrabajo;
            #endregion
        }
        public static NiveltrabajoDP Load(DbConnection con, DbTransaction tran, NiveltrabajoPk pk)
        {
            #region SQL Load Niveltrabajo
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_NivelTrabajo");
            com.CommandText = loadNiveltrabajo;
            #endregion
            #region Parametros Load Niveltrabajo
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.NiveltrabajoId);
            #endregion
            NiveltrabajoDP niveltrabajo;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Niveltrabajo
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        niveltrabajo = ReadRow(dr);
                    }
                    else
                        niveltrabajo = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Niveltrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoSelect, "", 
                    pk.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadNiveltrabajo);
                niveltrabajo = null;
                #endregion
            }
            return niveltrabajo;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Niveltrabajo
            DbCommand com = con.CreateCommand();
            String countNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoCount,"");
            com.CommandText = countNiveltrabajo;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Niveltrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountNiveltrabajo);
                #endregion
            }
            return resultado;
        }
        public static NiveltrabajoDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Niveltrabajo
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNiveltrabajo = String.Format(CultureInfo.CurrentCulture, Niveltrabajo.NiveltrabajoResx.NiveltrabajoSelectAll, "", ConstantesGlobales.IncluyeRows);
            listNiveltrabajo = listNiveltrabajo.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listNiveltrabajo, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Niveltrabajo
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Niveltrabajo
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Niveltrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaNiveltrabajo = String.Format(CultureInfo.CurrentCulture,Niveltrabajo.NiveltrabajoResx.NiveltrabajoSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaNiveltrabajo);
                #endregion
            }
            return (NiveltrabajoDP[])lista.ToArray(typeof(NiveltrabajoDP));
        }
    }
}
