using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "NiveltrabajoPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "NiveltrabajoPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "NiveltrabajoPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// NiveltrabajoPk niveltrabajoPk = new NiveltrabajoPk(niveltrabajo);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("NiveltrabajoPk")]
    public class NiveltrabajoPk
    {
        #region Definicion de campos privados.
        private Byte niveltrabajoId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de NiveltrabajoPk.
        /// </summary>
        /// <param name="niveltrabajoId">Descripción niveltrabajoId del tipo Byte.</param>
        public NiveltrabajoPk(Byte niveltrabajoId) 
        {
            this.niveltrabajoId = niveltrabajoId;
        }
        /// <summary>
        /// Constructor normal de NiveltrabajoPk.
        /// </summary>
        public NiveltrabajoPk() 
        {
        }
        #endregion.
    }
}
