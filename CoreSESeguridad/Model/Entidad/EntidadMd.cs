using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class EntidadMD
    {
        private EntidadDP entidad = null;

        public EntidadMD(EntidadDP entidad)
        {
            this.entidad = entidad;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Entidad
            DbCommand com = con.CreateCommand();
            String insertEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertEntidad;
            #endregion
            #region Parametros Insert Entidad
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, entidad.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, entidad.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, entidad.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 5, ParameterDirection.Input, entidad.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, entidad.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, entidad.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(entidad.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Entidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertEntidad = String.Format(Entidad.EntidadResx.EntidadInsert, "", 
                    entidad.PaisId.ToString(),
                    entidad.EntidadId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,entidad.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,entidad.Abreviatura),
                    entidad.BitActivo.ToString(),
                    entidad.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(entidad.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertEntidad);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Entidad
            DbCommand com = con.CreateCommand();
            String findEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad");
            com.CommandText = findEntidad;
            #endregion
            #region Parametros Find Entidad
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, entidad.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, entidad.EntidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Entidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadFind, "", 
                    entidad.PaisId.ToString(),
                    entidad.EntidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindEntidad);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Entidad
            DbCommand com = con.CreateCommand();
            String deleteEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad");
            com.CommandText = deleteEntidad;
            #endregion
            #region Parametros Delete Entidad
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, entidad.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, entidad.EntidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Entidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadDelete, "", 
                    entidad.PaisId.ToString(),
                    entidad.EntidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteEntidad);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Entidad
            DbCommand com = con.CreateCommand();
            String updateEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Pais",
                        "Id_Entidad");
            com.CommandText = updateEntidad;
            #endregion
            #region Parametros Update Entidad
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, entidad.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 5, ParameterDirection.Input, entidad.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, entidad.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, entidad.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(entidad.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, entidad.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, entidad.EntidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Entidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateEntidad = String.Format(Entidad.EntidadResx.EntidadUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,entidad.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,entidad.Abreviatura),
                    entidad.BitActivo.ToString(),
                    entidad.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(entidad.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    entidad.PaisId.ToString(),
                    entidad.EntidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateEntidad);
                #endregion
            }
            return resultado;
        }
        protected static EntidadDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            EntidadDP entidad = new EntidadDP();
            entidad.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            entidad.EntidadId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            entidad.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            entidad.Abreviatura = dr.IsDBNull(3) ? "" : dr.GetString(3);
            entidad.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            entidad.UsuarioId = dr.IsDBNull(5) ? 0 : dr.GetInt32(5);
            entidad.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            return entidad;
            #endregion
        }
        public static EntidadDP Load(DbConnection con, DbTransaction tran, EntidadPk pk)
        {
            #region SQL Load Entidad
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad");
            com.CommandText = loadEntidad;
            #endregion
            #region Parametros Load Entidad
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.EntidadId);
            #endregion
            EntidadDP entidad;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Entidad
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        entidad = ReadRow(dr);
                    }
                    else
                        entidad = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Entidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadSelect, "", 
                    pk.PaisId.ToString(),
                    pk.EntidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadEntidad);
                entidad = null;
                #endregion
            }
            return entidad;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Entidad
            DbCommand com = con.CreateCommand();
            String countEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadCount,"");
            com.CommandText = countEntidad;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Entidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountEntidad);
                #endregion
            }
            return resultado;
        }
        public static EntidadDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Entidad
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listEntidad = String.Format(CultureInfo.CurrentCulture, Entidad.EntidadResx.EntidadSelectAll, "", ConstantesGlobales.IncluyeRows);
            listEntidad = listEntidad.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listEntidad, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Entidad
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Entidad
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Entidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaEntidad = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadResx.EntidadSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaEntidad);
                #endregion
            }
            return (EntidadDP[])lista.ToArray(typeof(EntidadDP));
        }
    }
}
