using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class EntidadBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, EntidadDP entidad)
        {
            #region SaveDetail
            #region FK Pais
            PaisDP pais = entidad.Pais;
            if (pais != null) 
            {
                PaisMD paisMd = new PaisMD(pais);
                if (!paisMd.Find(con, tran))
                    paisMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, EntidadDP entidad)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, entidad);
                        resultado = InternalSave(con, tran, entidad);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla entidad!";
                        throw new BusinessException("Error interno al intentar guardar entidad!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla entidad!";
                throw new BusinessException("Error interno al intentar guardar entidad!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, EntidadDP entidad)
        {
            EntidadMD entidadMd = new EntidadMD(entidad);
            String resultado = "";
            if (entidadMd.Find(con, tran))
            {
                EntidadDP entidadAnterior = EntidadMD.Load(con, tran, entidad.Pk);
                if (entidadMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla entidad!";
                    throw new BusinessException("No se pude actualizar la tabla entidad!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (entidadMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla entidad!";
                    throw new BusinessException("No se pude insertar en la tabla entidad!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, EntidadDP entidad)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, entidad);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla entidad!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla entidad!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla entidad!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla entidad!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, EntidadDP entidad)
        {
            String resultado = "";
            EntidadMD entidadMd = new EntidadMD(entidad);
            if (entidadMd.Find(con, tran))
            {
                if (entidadMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla entidad!";
                    throw new BusinessException("No se pude eliminar en la tabla entidad!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla entidad!";
                throw new BusinessException("No se pude eliminar en la tabla entidad!");
            }
            return resultado;
        }

        internal static EntidadDP LoadDetail(DbConnection con, DbTransaction tran, EntidadDP entidadAnterior, EntidadDP entidad) 
        {
            #region FK Pais
            PaisPk paisPk = new PaisPk();
            if (entidadAnterior != null)
            {
                if (paisPk.Equals(entidadAnterior.Pk))
                    entidad.Pais = entidadAnterior.Pais;
                else
                    entidad.Pais = PaisMD.Load(con, tran, paisPk);
            }
            else
                entidad.Pais = PaisMD.Load(con, tran, paisPk);
            #endregion
            return entidad;
        }
        public static EntidadDP Load(DbConnection con, EntidadPk pk)
        {
            EntidadDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = EntidadMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Entidad!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = EntidadMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Entidad!", ex);
            }
            return resultado;
        }

        public static EntidadDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            EntidadDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = EntidadMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Entidad!", ex);
            }
            return resultado;

        }
    }
}
