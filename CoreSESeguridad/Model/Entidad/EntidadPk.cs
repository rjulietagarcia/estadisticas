using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "EntidadPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "EntidadPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "EntidadPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// EntidadPk entidadPk = new EntidadPk(entidad);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("EntidadPk")]
    public class EntidadPk
    {
        #region Definicion de campos privados.
        private Int16 paisId;
        private Int16 entidadId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de EntidadPk.
        /// </summary>
        /// <param name="paisId">Descripción paisId del tipo Int16.</param>
        /// <param name="entidadId">Descripción entidadId del tipo Int16.</param>
        public EntidadPk(Int16 paisId, Int16 entidadId) 
        {
            this.paisId = paisId;
            this.entidadId = entidadId;
        }
        /// <summary>
        /// Constructor normal de EntidadPk.
        /// </summary>
        public EntidadPk() 
        {
        }
        #endregion.
    }
}
