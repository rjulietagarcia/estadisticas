using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class RegionMD
    {
        private RegionDP region = null;

        public RegionMD(RegionDP region)
        {
            this.region = region;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Region
            DbCommand com = con.CreateCommand();
            String insertRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region",
                        "Nombre",
                        "Clave",
                        "Letra_Folio",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertRegion;
            #endregion
            #region Parametros Insert Region
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.RegionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 60, ParameterDirection.Input, region.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, region.Clave);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Letra_Folio",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, region.LetraFolio);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, region.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, region.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(region.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Region
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertRegion = String.Format(Region.RegionResx.RegionInsert, "", 
                    region.PaisId.ToString(),
                    region.EntidadId.ToString(),
                    region.RegionId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,region.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,region.Clave),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,region.LetraFolio),
                    region.BitActivo.ToString(),
                    region.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(region.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertRegion);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Region
            DbCommand com = con.CreateCommand();
            String findRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region");
            com.CommandText = findRegion;
            #endregion
            #region Parametros Find Region
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.RegionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Region
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionFind, "", 
                    region.PaisId.ToString(),
                    region.EntidadId.ToString(),
                    region.RegionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindRegion);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Region
            DbCommand com = con.CreateCommand();
            String deleteRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region");
            com.CommandText = deleteRegion;
            #endregion
            #region Parametros Delete Region
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.RegionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Region
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionDelete, "", 
                    region.PaisId.ToString(),
                    region.EntidadId.ToString(),
                    region.RegionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteRegion);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Region
            DbCommand com = con.CreateCommand();
            String updateRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Clave",
                        "Letra_Folio",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region");
            com.CommandText = updateRegion;
            #endregion
            #region Parametros Update Region
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 60, ParameterDirection.Input, region.Nombre);
            Common.CreateParameter(com, String.Format("{0}Clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, region.Clave);
            Common.CreateParameter(com, String.Format("{0}Letra_Folio",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, region.LetraFolio);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, region.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, region.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(region.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, region.RegionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Region
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateRegion = String.Format(Region.RegionResx.RegionUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,region.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,region.Clave),
                    String.Format(ConstantesGlobales.ConvierteString,region.LetraFolio),
                    region.BitActivo.ToString(),
                    region.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(region.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    region.PaisId.ToString(),
                    region.EntidadId.ToString(),
                    region.RegionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateRegion);
                #endregion
            }
            return resultado;
        }
        protected static RegionDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            RegionDP region = new RegionDP();
            region.PaisId = dr.IsDBNull(0) ? 0 :    int.Parse(dr.GetValue(0).ToString());
            region.EntidadId = dr.IsDBNull(1) ? 0 : int.Parse(dr.GetValue(1).ToString());
            region.RegionId = dr.IsDBNull(2) ? 0 :  int.Parse(dr.GetValue(2).ToString());
            region.Nombre = dr.IsDBNull(3) ? "" :   dr.GetString(3);
            region.Clave = dr.IsDBNull(4) ? "" :    dr.GetString(4);
            region.LetraFolio = dr.IsDBNull(5) ? "" : dr.GetString(5);
            region.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            region.UsuarioId = dr.IsDBNull(7) ? 0 : int.Parse(dr.GetValue(7).ToString());
            region.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            return region;
            #endregion
        }
        public static RegionDP Load(DbConnection con, DbTransaction tran, RegionPk pk)
        {
            #region SQL Load Region
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region");
            com.CommandText = loadRegion;
            #endregion
            #region Parametros Load Region
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.RegionId);
            #endregion
            RegionDP region;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Region
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        region = ReadRow(dr);
                    }
                    else
                        region = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Region
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionSelect, "", 
                    pk.PaisId.ToString(),
                    pk.EntidadId.ToString(),
                    pk.RegionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadRegion);
                region = null;
                #endregion
            }
            return region;
        }



        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Region
            DbCommand com = con.CreateCommand();
            String countRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionCount,"");
            com.CommandText = countRegion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Region
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountRegion);
                #endregion
            }
            return resultado;
        }
        public static RegionDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Region
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listRegion = String.Format(CultureInfo.CurrentCulture, Region.RegionResx.RegionSelectAll, "", ConstantesGlobales.IncluyeRows);
            listRegion = listRegion.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listRegion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Region
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Region
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Region
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaRegion = String.Format(CultureInfo.CurrentCulture,Region.RegionResx.RegionSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaRegion);
                #endregion
            }
            return (RegionDP[])lista.ToArray(typeof(RegionDP));
        }
    }
}
