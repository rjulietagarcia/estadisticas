using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class RegionBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, RegionDP region)
        {
            #region SaveDetail
            #region FK Entidad
            EntidadDP entidad = region.Entidad;
            if (entidad != null) 
            {
                EntidadMD entidadMd = new EntidadMD(entidad);
                if (!entidadMd.Find(con, tran))
                    entidadMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, RegionDP region)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, region);
                        resultado = InternalSave(con, tran, region);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla region!";
                        throw new BusinessException("Error interno al intentar guardar region!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla region!";
                throw new BusinessException("Error interno al intentar guardar region!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, RegionDP region)
        {
            RegionMD regionMd = new RegionMD(region);
            String resultado = "";
            if (regionMd.Find(con, tran))
            {
                RegionDP regionAnterior = RegionMD.Load(con, tran, region.Pk);
                if (regionMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla region!";
                    throw new BusinessException("No se pude actualizar la tabla region!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (regionMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla region!";
                    throw new BusinessException("No se pude insertar en la tabla region!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, RegionDP region)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, region);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla region!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla region!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla region!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla region!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, RegionDP region)
        {
            String resultado = "";
            RegionMD regionMd = new RegionMD(region);
            if (regionMd.Find(con, tran))
            {
                if (regionMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla region!";
                    throw new BusinessException("No se pude eliminar en la tabla region!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla region!";
                throw new BusinessException("No se pude eliminar en la tabla region!");
            }
            return resultado;
        }

        internal static RegionDP LoadDetail(DbConnection con, DbTransaction tran, RegionDP regionAnterior, RegionDP region) 
        {
            #region FK Entidad
            EntidadPk entidadPk = new EntidadPk();
            if (regionAnterior != null)
            {
                if (entidadPk.Equals(regionAnterior.Pk))
                    region.Entidad = regionAnterior.Entidad;
                else
                    region.Entidad = EntidadMD.Load(con, tran, entidadPk);
            }
            else
                region.Entidad = EntidadMD.Load(con, tran, entidadPk);
            #endregion
            return region;
        }
        public static RegionDP Load(DbConnection con, RegionPk pk)
        {
            RegionDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = RegionMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Region!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = RegionMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Region!", ex);
            }
            return resultado;
        }

        public static RegionDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            RegionDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = RegionMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Region!", ex);
            }
            return resultado;

        }
    }
}
