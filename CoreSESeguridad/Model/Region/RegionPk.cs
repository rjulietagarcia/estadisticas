using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "RegionPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 04:32:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "RegionPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "RegionPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// RegionPk regionPk = new RegionPk(region);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("RegionPk")]
    public class RegionPk
    {
        #region Definicion de campos privados.
        private int paisId;
        private int entidadId;
        private int regionId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public int PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public int EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public int RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de RegionPk.
        /// </summary>
        /// <param name="paisId">Descripción paisId del tipo Int16.</param>
        /// <param name="entidadId">Descripción entidadId del tipo Int16.</param>
        /// <param name="regionId">Descripción regionId del tipo Int16.</param>
        public RegionPk(int paisId, int entidadId, int regionId) 
        {
            this.paisId = paisId;
            this.entidadId = entidadId;
            this.regionId = regionId;
        }
        /// <summary>
        /// Constructor normal de RegionPk.
        /// </summary>
        public RegionPk() 
        {
        }
        #endregion.
    }
}
