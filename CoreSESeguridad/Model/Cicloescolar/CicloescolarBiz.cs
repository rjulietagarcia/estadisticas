using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class CicloescolarBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, CicloescolarDP cicloescolar)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, CicloescolarDP cicloescolar)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, cicloescolar);
                        resultado = InternalSave(con, tran, cicloescolar);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla cicloescolar!";
                        throw new BusinessException("Error interno al intentar guardar cicloescolar!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla cicloescolar!";
                throw new BusinessException("Error interno al intentar guardar cicloescolar!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, CicloescolarDP cicloescolar)
        {
            CicloescolarMD cicloescolarMd = new CicloescolarMD(cicloescolar);
            String resultado = "";
            if (cicloescolarMd.Find(con, tran))
            {
                CicloescolarDP cicloescolarAnterior = CicloescolarMD.Load(con, tran, cicloescolar.Pk);
                if (cicloescolarMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla cicloescolar!";
                    throw new BusinessException("No se pude actualizar la tabla cicloescolar!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (cicloescolarMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla cicloescolar!";
                    throw new BusinessException("No se pude insertar en la tabla cicloescolar!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, CicloescolarDP cicloescolar)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, cicloescolar);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla cicloescolar!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla cicloescolar!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla cicloescolar!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla cicloescolar!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, CicloescolarDP cicloescolar)
        {
            String resultado = "";
            CicloescolarMD cicloescolarMd = new CicloescolarMD(cicloescolar);
            if (cicloescolarMd.Find(con, tran))
            {
                if (cicloescolarMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla cicloescolar!";
                    throw new BusinessException("No se pude eliminar en la tabla cicloescolar!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla cicloescolar!";
                throw new BusinessException("No se pude eliminar en la tabla cicloescolar!");
            }
            return resultado;
        }

        internal static CicloescolarDP LoadDetail(DbConnection con, DbTransaction tran, CicloescolarDP cicloescolarAnterior, CicloescolarDP cicloescolar) 
        {
            return cicloescolar;
        }
        public static CicloescolarDP Load(DbConnection con, CicloescolarPk pk)
        {
            CicloescolarDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CicloescolarMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Cicloescolar!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CicloescolarMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Cicloescolar!", ex);
            }
            return resultado;
        }

        public static CicloescolarDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CicloescolarDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CicloescolarMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Cicloescolar!", ex);
            }
            return resultado;

        }
    }
}
