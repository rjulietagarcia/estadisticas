using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Cicloescolar".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 16 de junio de 2009.</Para>
    /// <Para>Hora: 03:39:46 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Cicloescolar".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>TipocicloId</term><description>Descripcion TipocicloId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaInicio</term><description>Descripcion FechaInicio</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaFin</term><description>Descripcion FechaFin</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolaranteriorId</term><description>Descripcion CicloescolaranteriorId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarsiguienteId</term><description>Descripcion CicloescolarsiguienteId</description>
    ///    </item>
    ///    <item>
    ///        <term>BitCicloactual</term><description>Descripcion BitCicloactual</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CicloescolarDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// CicloescolarDTO cicloescolar = new CicloescolarDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Cicloescolar")]
    public class CicloescolarDP
    {
        #region Definicion de campos privados.
        private Int16 cicloescolarId;
        private Int16 tipocicloId;
        private String fechaInicio;
        private String fechaFin;
        private String nombre;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Int16 cicloescolaranteriorId;
        private Int16 cicloescolarsiguienteId;
        private Boolean bitCicloactual;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// TipocicloId
        /// </summary> 
        [XmlElement("TipocicloId")]
        public Int16 TipocicloId
        {
            get {
                    return tipocicloId; 
            }
            set {
                    tipocicloId = value; 
            }
        }

        /// <summary>
        /// FechaInicio
        /// </summary> 
        [XmlElement("FechaInicio")]
        public String FechaInicio
        {
            get {
                    return fechaInicio; 
            }
            set {
                    fechaInicio = value; 
            }
        }

        /// <summary>
        /// FechaFin
        /// </summary> 
        [XmlElement("FechaFin")]
        public String FechaFin
        {
            get {
                    return fechaFin; 
            }
            set {
                    fechaFin = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// CicloescolaranteriorId
        /// </summary> 
        [XmlElement("CicloescolaranteriorId")]
        public Int16 CicloescolaranteriorId
        {
            get {
                    return cicloescolaranteriorId; 
            }
            set {
                    cicloescolaranteriorId = value; 
            }
        }

        /// <summary>
        /// CicloescolarsiguienteId
        /// </summary> 
        [XmlElement("CicloescolarsiguienteId")]
        public Int16 CicloescolarsiguienteId
        {
            get {
                    return cicloescolarsiguienteId; 
            }
            set {
                    cicloescolarsiguienteId = value; 
            }
        }

        /// <summary>
        /// BitCicloactual
        /// </summary> 
        [XmlElement("BitCicloactual")]
        public Boolean BitCicloactual
        {
            get {
                    return bitCicloactual; 
            }
            set {
                    bitCicloactual = value; 
            }
        }

        /// <summary>
        /// Llave primaria de CicloescolarPk
        /// </summary>
        [XmlElement("Pk")]
        public CicloescolarPk Pk {
            get {
                    return new CicloescolarPk( cicloescolarId, tipocicloId );
            }
        }
        #endregion.
    }
}
