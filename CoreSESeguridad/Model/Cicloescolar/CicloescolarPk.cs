using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "CicloescolarPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 16 de junio de 2009.</Para>
    /// <Para>Hora: 03:39:46 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CicloescolarPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>TipocicloId</term><description>Descripcion TipocicloId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CicloescolarPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// CicloescolarPk cicloescolarPk = new CicloescolarPk(cicloescolar);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("CicloescolarPk")]
    public class CicloescolarPk
    {
        #region Definicion de campos privados.
        private Int16 cicloescolarId;
        private Int16 tipocicloId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// TipocicloId
        /// </summary> 
        [XmlElement("TipocicloId")]
        public Int16 TipocicloId
        {
            get {
                    return tipocicloId; 
            }
            set {
                    tipocicloId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de CicloescolarPk.
        /// </summary>
        /// <param name="cicloescolarId">Descripción cicloescolarId del tipo Int16.</param>
        /// <param name="tipocicloId">Descripción tipocicloId del tipo Int16.</param>
        public CicloescolarPk(Int16 cicloescolarId, Int16 tipocicloId) 
        {
            this.cicloescolarId = cicloescolarId;
            this.tipocicloId = tipocicloId;
        }
        /// <summary>
        /// Constructor normal de CicloescolarPk.
        /// </summary>
        public CicloescolarPk() 
        {
        }
        #endregion.
    }
}
