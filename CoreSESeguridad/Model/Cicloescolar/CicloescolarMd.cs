using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class CicloescolarMD
    {
        private CicloescolarDP cicloescolar = null;

        public CicloescolarMD(CicloescolarDP cicloescolar)
        {
            this.cicloescolar = cicloescolar;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Cicloescolar
            DbCommand com = con.CreateCommand();
            String insertCicloescolar = String.Format(CultureInfo.CurrentCulture,Cicloescolar.CicloescolarResx.CicloescolarInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CicloEscolar",
                        "Id_TipoCiclo",
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Nombre",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "id_CicloEscolarAnterior",
                        "id_CicloEscolarSiguiente",
                        "Bit_CicloActual");
            com.CommandText = insertCicloescolar;
            #endregion
            #region Parametros Insert Cicloescolar
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.CicloescolarId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_TipoCiclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.TipocicloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(cicloescolar.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(cicloescolar.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, cicloescolar.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, cicloescolar.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, cicloescolar.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(cicloescolar.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_CicloEscolarAnterior",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.CicloescolaranteriorId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_CicloEscolarSiguiente",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.CicloescolarsiguienteId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_CicloActual",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, cicloescolar.BitCicloactual);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Cicloescolar
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Cicloescolar
            DbCommand com = con.CreateCommand();
            String findCicloescolar = String.Format(CultureInfo.CurrentCulture,Cicloescolar.CicloescolarResx.CicloescolarFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CicloEscolar",
                        "Id_TipoCiclo");
            com.CommandText = findCicloescolar;
            #endregion
            #region Parametros Find Cicloescolar
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.CicloescolarId);
            Common.CreateParameter(com, String.Format("{0}Id_TipoCiclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.TipocicloId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Cicloescolar
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Cicloescolar
            DbCommand com = con.CreateCommand();
            String deleteCicloescolar = String.Format(CultureInfo.CurrentCulture,Cicloescolar.CicloescolarResx.CicloescolarDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CicloEscolar",
                        "Id_TipoCiclo");
            com.CommandText = deleteCicloescolar;
            #endregion
            #region Parametros Delete Cicloescolar
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.CicloescolarId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_TipoCiclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.TipocicloId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Cicloescolar
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Cicloescolar
            DbCommand com = con.CreateCommand();
            String updateCicloescolar = String.Format(CultureInfo.CurrentCulture,Cicloescolar.CicloescolarResx.CicloescolarUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Nombre",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "id_CicloEscolarAnterior",
                        "id_CicloEscolarSiguiente",
                        "Bit_CicloActual",
                        "Id_CicloEscolar",
                        "Id_TipoCiclo");
            com.CommandText = updateCicloescolar;
            #endregion
            #region Parametros Update Cicloescolar
            Common.CreateParameter(com, String.Format("{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(cicloescolar.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(cicloescolar.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, cicloescolar.Nombre);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, cicloescolar.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, cicloescolar.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(cicloescolar.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}id_CicloEscolarAnterior",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.CicloescolaranteriorId);
            Common.CreateParameter(com, String.Format("{0}id_CicloEscolarSiguiente",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.CicloescolarsiguienteId);
            Common.CreateParameter(com, String.Format("{0}Bit_CicloActual",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, cicloescolar.BitCicloactual);
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.CicloescolarId);
            Common.CreateParameter(com, String.Format("{0}Id_TipoCiclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, cicloescolar.TipocicloId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Cicloescolar
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static CicloescolarDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CicloescolarDP cicloescolar = new CicloescolarDP();
            cicloescolar.CicloescolarId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            cicloescolar.TipocicloId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            cicloescolar.FechaInicio = dr.IsDBNull(2) ? "" : dr.GetDateTime(2).ToShortDateString();;
            cicloescolar.FechaFin = dr.IsDBNull(3) ? "" : dr.GetDateTime(3).ToShortDateString();;
            cicloescolar.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            cicloescolar.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            cicloescolar.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            cicloescolar.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            cicloescolar.CicloescolaranteriorId = dr.IsDBNull(8) ? (short)0 : dr.GetInt16(8);
            cicloescolar.CicloescolarsiguienteId = dr.IsDBNull(9) ? (short)0 : dr.GetInt16(9);
            cicloescolar.BitCicloactual = dr.IsDBNull(10) ? false : dr.GetBoolean(10);
            return cicloescolar;
            #endregion
        }
        public static CicloescolarDP Load(DbConnection con, DbTransaction tran, CicloescolarPk pk)
        {
            #region SQL Load Cicloescolar
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadCicloescolar = String.Format(CultureInfo.CurrentCulture,Cicloescolar.CicloescolarResx.CicloescolarSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CicloEscolar",
                        "Id_TipoCiclo");
            com.CommandText = loadCicloescolar;
            #endregion
            #region Parametros Load Cicloescolar
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.CicloescolarId);
            Common.CreateParameter(com, String.Format("{0}Id_TipoCiclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.TipocicloId);
            #endregion
            CicloescolarDP cicloescolar;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Cicloescolar
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        cicloescolar = ReadRow(dr);
                    }
                    else
                        cicloescolar = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Cicloescolar
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return cicloescolar;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Cicloescolar
            DbCommand com = con.CreateCommand();
            String countCicloescolar = String.Format(CultureInfo.CurrentCulture,Cicloescolar.CicloescolarResx.CicloescolarCount,"");
            com.CommandText = countCicloescolar;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Cicloescolar
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CicloescolarDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Cicloescolar
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCicloescolar = String.Format(CultureInfo.CurrentCulture, Cicloescolar.CicloescolarResx.CicloescolarSelectAll, "", ConstantesGlobales.IncluyeRows);
            listCicloescolar = listCicloescolar.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCicloescolar, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Cicloescolar
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Cicloescolar
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Cicloescolar
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CicloescolarDP[])lista.ToArray(typeof(CicloescolarDP));
        }
    }
}
