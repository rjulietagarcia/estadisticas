using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "CategorialocalidadPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:36:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CategorialocalidadPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>CategorialocalidadId</term><description>Descripcion CategorialocalidadId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CategorialocalidadPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// CategorialocalidadPk categorialocalidadPk = new CategorialocalidadPk(categorialocalidad);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("CategorialocalidadPk")]
    public class CategorialocalidadPk
    {
        #region Definicion de campos privados.
        private Int16 categorialocalidadId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// CategorialocalidadId
        /// </summary> 
        [XmlElement("CategorialocalidadId")]
        public Int16 CategorialocalidadId
        {
            get {
                    return categorialocalidadId; 
            }
            set {
                    categorialocalidadId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de CategorialocalidadPk.
        /// </summary>
        /// <param name="categorialocalidadId">Descripción categorialocalidadId del tipo Int16.</param>
        public CategorialocalidadPk(Int16 categorialocalidadId) 
        {
            this.categorialocalidadId = categorialocalidadId;
        }
        /// <summary>
        /// Constructor normal de CategorialocalidadPk.
        /// </summary>
        public CategorialocalidadPk() 
        {
        }
        #endregion.
    }
}
