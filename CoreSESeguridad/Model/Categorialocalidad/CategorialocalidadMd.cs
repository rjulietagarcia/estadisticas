using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class CategorialocalidadMD
    {
        private CategorialocalidadDP categorialocalidad = null;

        public CategorialocalidadMD(CategorialocalidadDP categorialocalidad)
        {
            this.categorialocalidad = categorialocalidad;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Categorialocalidad
            DbCommand com = con.CreateCommand();
            String insertCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CategoriaLocalidad",
                        "Nombre",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertCategorialocalidad;
            #endregion
            #region Parametros Insert Categorialocalidad
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CategoriaLocalidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, categorialocalidad.CategorialocalidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, categorialocalidad.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, categorialocalidad.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, categorialocalidad.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(categorialocalidad.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Categorialocalidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertCategorialocalidad = String.Format(Categorialocalidad.CategorialocalidadResx.CategorialocalidadInsert, "", 
                    categorialocalidad.CategorialocalidadId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,categorialocalidad.Nombre),
                    categorialocalidad.BitActivo.ToString(),
                    categorialocalidad.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(categorialocalidad.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertCategorialocalidad);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Categorialocalidad
            DbCommand com = con.CreateCommand();
            String findCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CategoriaLocalidad");
            com.CommandText = findCategorialocalidad;
            #endregion
            #region Parametros Find Categorialocalidad
            Common.CreateParameter(com, String.Format("{0}Id_CategoriaLocalidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, categorialocalidad.CategorialocalidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Categorialocalidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadFind, "", 
                    categorialocalidad.CategorialocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindCategorialocalidad);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Categorialocalidad
            DbCommand com = con.CreateCommand();
            String deleteCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CategoriaLocalidad");
            com.CommandText = deleteCategorialocalidad;
            #endregion
            #region Parametros Delete Categorialocalidad
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CategoriaLocalidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, categorialocalidad.CategorialocalidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Categorialocalidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadDelete, "", 
                    categorialocalidad.CategorialocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteCategorialocalidad);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Categorialocalidad
            DbCommand com = con.CreateCommand();
            String updateCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_CategoriaLocalidad");
            com.CommandText = updateCategorialocalidad;
            #endregion
            #region Parametros Update Categorialocalidad
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, categorialocalidad.Nombre);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, categorialocalidad.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, categorialocalidad.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(categorialocalidad.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_CategoriaLocalidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, categorialocalidad.CategorialocalidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Categorialocalidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateCategorialocalidad = String.Format(Categorialocalidad.CategorialocalidadResx.CategorialocalidadUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,categorialocalidad.Nombre),
                    categorialocalidad.BitActivo.ToString(),
                    categorialocalidad.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(categorialocalidad.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    categorialocalidad.CategorialocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateCategorialocalidad);
                #endregion
            }
            return resultado;
        }
        protected static CategorialocalidadDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CategorialocalidadDP categorialocalidad = new CategorialocalidadDP();
            categorialocalidad.CategorialocalidadId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            categorialocalidad.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            categorialocalidad.BitActivo = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            categorialocalidad.UsuarioId = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            categorialocalidad.FechaActualizacion = dr.IsDBNull(4) ? "" : dr.GetDateTime(4).ToShortDateString();;
            return categorialocalidad;
            #endregion
        }
        public static CategorialocalidadDP Load(DbConnection con, DbTransaction tran, CategorialocalidadPk pk)
        {
            #region SQL Load Categorialocalidad
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CategoriaLocalidad");
            com.CommandText = loadCategorialocalidad;
            #endregion
            #region Parametros Load Categorialocalidad
            Common.CreateParameter(com, String.Format("{0}Id_CategoriaLocalidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.CategorialocalidadId);
            #endregion
            CategorialocalidadDP categorialocalidad;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Categorialocalidad
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        categorialocalidad = ReadRow(dr);
                    }
                    else
                        categorialocalidad = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Categorialocalidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadSelect, "", 
                    pk.CategorialocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadCategorialocalidad);
                categorialocalidad = null;
                #endregion
            }
            return categorialocalidad;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Categorialocalidad
            DbCommand com = con.CreateCommand();
            String countCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadCount,"");
            com.CommandText = countCategorialocalidad;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Categorialocalidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountCategorialocalidad);
                #endregion
            }
            return resultado;
        }
        public static CategorialocalidadDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Categorialocalidad
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCategorialocalidad = String.Format(CultureInfo.CurrentCulture, Categorialocalidad.CategorialocalidadResx.CategorialocalidadSelectAll, "", ConstantesGlobales.IncluyeRows);
            listCategorialocalidad = listCategorialocalidad.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCategorialocalidad, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Categorialocalidad
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Categorialocalidad
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Categorialocalidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaCategorialocalidad = String.Format(CultureInfo.CurrentCulture,Categorialocalidad.CategorialocalidadResx.CategorialocalidadSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaCategorialocalidad);
                #endregion
            }
            return (CategorialocalidadDP[])lista.ToArray(typeof(CategorialocalidadDP));
        }
    }
}
