using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class CategorialocalidadBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, CategorialocalidadDP categorialocalidad)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, CategorialocalidadDP categorialocalidad)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, categorialocalidad);
                        resultado = InternalSave(con, tran, categorialocalidad);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla categorialocalidad!";
                        throw new BusinessException("Error interno al intentar guardar categorialocalidad!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla categorialocalidad!";
                throw new BusinessException("Error interno al intentar guardar categorialocalidad!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, CategorialocalidadDP categorialocalidad)
        {
            CategorialocalidadMD categorialocalidadMd = new CategorialocalidadMD(categorialocalidad);
            String resultado = "";
            if (categorialocalidadMd.Find(con, tran))
            {
                CategorialocalidadDP categorialocalidadAnterior = CategorialocalidadMD.Load(con, tran, categorialocalidad.Pk);
                if (categorialocalidadMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla categorialocalidad!";
                    throw new BusinessException("No se pude actualizar la tabla categorialocalidad!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (categorialocalidadMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla categorialocalidad!";
                    throw new BusinessException("No se pude insertar en la tabla categorialocalidad!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, CategorialocalidadDP categorialocalidad)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, categorialocalidad);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla categorialocalidad!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla categorialocalidad!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla categorialocalidad!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla categorialocalidad!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, CategorialocalidadDP categorialocalidad)
        {
            String resultado = "";
            CategorialocalidadMD categorialocalidadMd = new CategorialocalidadMD(categorialocalidad);
            if (categorialocalidadMd.Find(con, tran))
            {
                if (categorialocalidadMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla categorialocalidad!";
                    throw new BusinessException("No se pude eliminar en la tabla categorialocalidad!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla categorialocalidad!";
                throw new BusinessException("No se pude eliminar en la tabla categorialocalidad!");
            }
            return resultado;
        }

        internal static CategorialocalidadDP LoadDetail(DbConnection con, DbTransaction tran, CategorialocalidadDP categorialocalidadAnterior, CategorialocalidadDP categorialocalidad) 
        {
            return categorialocalidad;
        }
        public static CategorialocalidadDP Load(DbConnection con, CategorialocalidadPk pk)
        {
            CategorialocalidadDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CategorialocalidadMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Categorialocalidad!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CategorialocalidadMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Categorialocalidad!", ex);
            }
            return resultado;
        }

        public static CategorialocalidadDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CategorialocalidadDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CategorialocalidadMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Categorialocalidad!", ex);
            }
            return resultado;

        }
    }
}
