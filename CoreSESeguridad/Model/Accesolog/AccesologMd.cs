using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class AccesologMD
    {
        private AccesologDP accesolog = null;

        public AccesologMD(AccesologDP accesolog)
        {
            this.accesolog = accesolog;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Accesolog
            DbCommand com = con.CreateCommand();
            String insertAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Log",
                        "Id_Opcion",
                        "Id_Usuario",
                        "Fecha",
                        "IP");
            com.CommandText = insertAccesolog;
            #endregion
            #region Parametros Insert Accesolog
            // Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Log",ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, accesolog.LogId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, accesolog.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, accesolog.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(accesolog.Fecha,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IP",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 15, ParameterDirection.Input, accesolog.Ip);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Accesolog
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertAccesolog = String.Format(Accesolog.AccesologResx.AccesologInsert, "", 
                    accesolog.LogId.ToString(),
                    accesolog.OpcionId.ToString(),
                    accesolog.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(accesolog.Fecha).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,accesolog.Ip));
                System.Diagnostics.Debug.WriteLine(errorInsertAccesolog);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Accesolog
            DbCommand com = con.CreateCommand();
            String findAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Log");
            com.CommandText = findAccesolog;
            #endregion
            #region Parametros Find Accesolog
            Common.CreateParameter(com, String.Format("{0}Id_Log",ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, accesolog.LogId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Accesolog
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologFind, "", 
                    accesolog.LogId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindAccesolog);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Accesolog
            DbCommand com = con.CreateCommand();
            String deleteAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Log");
            com.CommandText = deleteAccesolog;
            #endregion
            #region Parametros Delete Accesolog
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Log",ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, accesolog.LogId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Accesolog
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologDelete, "", 
                    accesolog.LogId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteAccesolog);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Accesolog
            DbCommand com = con.CreateCommand();
            String updateAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Opcion",
                        "Id_Usuario",
                        "Fecha",
                        "IP",
                        "Id_Log");
            com.CommandText = updateAccesolog;
            #endregion
            #region Parametros Update Accesolog
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, accesolog.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, accesolog.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(accesolog.Fecha,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}IP",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 15, ParameterDirection.Input, accesolog.Ip);
            Common.CreateParameter(com, String.Format("{0}Id_Log",ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, accesolog.LogId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Accesolog
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateAccesolog = String.Format(Accesolog.AccesologResx.AccesologUpdate, "", 
                    accesolog.OpcionId.ToString(),
                    accesolog.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(accesolog.Fecha).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteString,accesolog.Ip),
                    accesolog.LogId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateAccesolog);
                #endregion
            }
            return resultado;
        }
        protected static AccesologDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            AccesologDP accesolog = new AccesologDP();
            accesolog.LogId = dr.IsDBNull(0) ? 0 : dr.GetInt64(0);
            accesolog.OpcionId = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            accesolog.UsuarioId = dr.IsDBNull(2) ? 0 : dr.GetInt64(2);
            accesolog.Fecha = dr.IsDBNull(3) ? "" : dr.GetDateTime(3).ToShortDateString();;
            accesolog.Ip = dr.IsDBNull(4) ? "" : dr.GetString(4);
            return accesolog;
            #endregion
        }
        public static AccesologDP Load(DbConnection con, DbTransaction tran, AccesologPk pk)
        {
            #region SQL Load Accesolog
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Log");
            com.CommandText = loadAccesolog;
            #endregion
            #region Parametros Load Accesolog
            Common.CreateParameter(com, String.Format("{0}Id_Log",ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, pk.LogId);
            #endregion
            AccesologDP accesolog;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Accesolog
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        accesolog = ReadRow(dr);
                    }
                    else
                        accesolog = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Accesolog
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologSelect, "", 
                    pk.LogId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadAccesolog);
                accesolog = null;
                #endregion
            }
            return accesolog;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Accesolog
            DbCommand com = con.CreateCommand();
            String countAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologCount,"");
            com.CommandText = countAccesolog;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Accesolog
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountAccesolog);
                #endregion
            }
            return resultado;
        }
        public static AccesologDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Accesolog
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listAccesolog = String.Format(CultureInfo.CurrentCulture, Accesolog.AccesologResx.AccesologSelectAll, "", ConstantesGlobales.IncluyeRows);
            listAccesolog = listAccesolog.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listAccesolog, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Accesolog
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Accesolog
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Accesolog
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaAccesolog = String.Format(CultureInfo.CurrentCulture,Accesolog.AccesologResx.AccesologSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaAccesolog);
                #endregion
            }
            return (AccesologDP[])lista.ToArray(typeof(AccesologDP));
        }
    }
}
