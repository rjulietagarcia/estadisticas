using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "AccesologPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 05:55:17 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "AccesologPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>LogId</term><description>Descripcion LogId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "AccesologPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// AccesologPk accesologPk = new AccesologPk(accesolog);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("AccesologPk")]
    public class AccesologPk
    {
        #region Definicion de campos privados.
        private Int64 logId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// LogId
        /// </summary> 
        [XmlElement("LogId")]
        public Int64 LogId
        {
            get {
                    return logId; 
            }
            set {
                    logId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de AccesologPk.
        /// </summary>
        /// <param name="logId">Descripción logId del tipo Int64.</param>
        public AccesologPk(Int64 logId) 
        {
            this.logId = logId;
        }
        /// <summary>
        /// Constructor normal de AccesologPk.
        /// </summary>
        public AccesologPk() 
        {
        }
        #endregion.
    }
}
