using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class AccesologBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, AccesologDP accesolog)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, AccesologDP accesolog)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, accesolog);
                        resultado = InternalSave(con, tran, accesolog);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla accesolog!";
                        throw new BusinessException("Error interno al intentar guardar accesolog!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla accesolog!";
                throw new BusinessException("Error interno al intentar guardar accesolog!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, AccesologDP accesolog)
        {
            AccesologMD accesologMd = new AccesologMD(accesolog);
            String resultado = "";
            if (accesologMd.Find(con, tran))
            {
                AccesologDP accesologAnterior = AccesologMD.Load(con, tran, accesolog.Pk);
                if (accesologMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla accesolog!";
                    throw new BusinessException("No se pude actualizar la tabla accesolog!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (accesologMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla accesolog!";
                    throw new BusinessException("No se pude insertar en la tabla accesolog!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, AccesologDP accesolog)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, accesolog);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla accesolog!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla accesolog!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla accesolog!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla accesolog!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, AccesologDP accesolog)
        {
            String resultado = "";
            AccesologMD accesologMd = new AccesologMD(accesolog);
            if (accesologMd.Find(con, tran))
            {
                if (accesologMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla accesolog!";
                    throw new BusinessException("No se pude eliminar en la tabla accesolog!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla accesolog!";
                throw new BusinessException("No se pude eliminar en la tabla accesolog!");
            }
            return resultado;
        }

        internal static AccesologDP LoadDetail(DbConnection con, DbTransaction tran, AccesologDP accesologAnterior, AccesologDP accesolog) 
        {
            return accesolog;
        }
        public static AccesologDP Load(DbConnection con, AccesologPk pk)
        {
            AccesologDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = AccesologMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Accesolog!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = AccesologMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Accesolog!", ex);
            }
            return resultado;
        }

        public static AccesologDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            AccesologDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = AccesologMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Accesolog!", ex);
            }
            return resultado;

        }
    }
}
