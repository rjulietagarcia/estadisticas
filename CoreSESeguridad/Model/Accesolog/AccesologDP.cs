using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Accesolog".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 05:55:17 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Accesolog".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>LogId</term><description>Descripcion LogId</description>
    ///    </item>
    ///    <item>
    ///        <term>OpcionId</term><description>Descripcion OpcionId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>Fecha</term><description>Descripcion Fecha</description>
    ///    </item>
    ///    <item>
    ///        <term>Ip</term><description>Descripcion Ip</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "AccesologDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// AccesologDTO accesolog = new AccesologDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Accesolog")]
    public class AccesologDP
    {
        #region Definicion de campos privados.
        private Int64 logId;
        private Int32 opcionId;
        private Int64 usuarioId;
        private String fecha;
        private String ip;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// LogId
        /// </summary> 
        [XmlElement("LogId")]
        public Int64 LogId
        {
            get {
                    return logId; 
            }
            set {
                    logId = value; 
            }
        }

        /// <summary>
        /// OpcionId
        /// </summary> 
        [XmlElement("OpcionId")]
        public Int32 OpcionId
        {
            get {
                    return opcionId; 
            }
            set {
                    opcionId = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int64 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// Fecha
        /// </summary> 
        [XmlElement("Fecha")]
        public String Fecha
        {
            get {
                    return fecha; 
            }
            set {
                    fecha = value; 
            }
        }

        /// <summary>
        /// Ip
        /// </summary> 
        [XmlElement("Ip")]
        public String Ip
        {
            get {
                    return ip; 
            }
            set {
                    ip = value; 
            }
        }

        /// <summary>
        /// Llave primaria de AccesologPk
        /// </summary>
        [XmlElement("Pk")]
        public AccesologPk Pk {
            get {
                    return new AccesologPk( logId );
            }
        }
        #endregion.
    }
}
