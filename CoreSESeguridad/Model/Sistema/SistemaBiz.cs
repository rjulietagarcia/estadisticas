using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class SistemaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, SistemaDP sistema)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, SistemaDP sistema)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, sistema);
                        resultado = InternalSave(con, tran, sistema);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla sistema!";
                        throw new BusinessException("Error interno al intentar guardar sistema!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla sistema!";
                throw new BusinessException("Error interno al intentar guardar sistema!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, SistemaDP sistema)
        {
            SistemaMD sistemaMd = new SistemaMD(sistema);
            String resultado = "";
            if (sistemaMd.Find(con, tran))
            {
                SistemaDP sistemaAnterior = SistemaMD.Load(con, tran, sistema.Pk);
                if (sistemaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla sistema!";
                    throw new BusinessException("No se pude actualizar la tabla sistema!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (sistemaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla sistema!";
                    throw new BusinessException("No se pude insertar en la tabla sistema!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, SistemaDP sistema)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, sistema);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla sistema!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla sistema!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla sistema!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla sistema!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, SistemaDP sistema)
        {
            String resultado = "";
            SistemaMD sistemaMd = new SistemaMD(sistema);
            if (sistemaMd.Find(con, tran))
            {
                if (sistemaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla sistema!";
                    throw new BusinessException("No se pude eliminar en la tabla sistema!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla sistema!";
                throw new BusinessException("No se pude eliminar en la tabla sistema!");
            }
            return resultado;
        }

        internal static SistemaDP LoadDetail(DbConnection con, DbTransaction tran, SistemaDP sistemaAnterior, SistemaDP sistema) 
        {
            return sistema;
        }
        public static SistemaDP Load(DbConnection con, SistemaPk pk)
        {
            SistemaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SistemaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Sistema!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SistemaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Sistema!", ex);
            }
            return resultado;
        }

        public static SistemaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SistemaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SistemaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Sistema!", ex);
            }
            return resultado;

        }
    }
}
