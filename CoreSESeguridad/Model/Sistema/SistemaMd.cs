using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class SistemaMD
    {
        private SistemaDP sistema = null;

        public SistemaMD(SistemaDP sistema)
        {
            this.sistema = sistema;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Sistema
            DbCommand com = con.CreateCommand();
            String insertSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Carpeta_Sistema");
            com.CommandText = insertSistema;
            #endregion
            #region Parametros Insert Sistema
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sistema.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, sistema.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, sistema.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sistema.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, sistema.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sistema.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sistema.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sistema.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Carpeta_Sistema",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 255, ParameterDirection.Input, sistema.CarpetaSistema);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Sistema
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertSistema = String.Format(Sistema.SistemaResx.SistemaInsert, "", 
                    sistema.SistemaId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,sistema.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,sistema.Abreviatura),
                    sistema.BitActivo.ToString(),
                    sistema.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(sistema.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(sistema.FechaInicio).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(sistema.FechaFin).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,sistema.CarpetaSistema));
                System.Diagnostics.Debug.WriteLine(errorInsertSistema);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Sistema
            DbCommand com = con.CreateCommand();
            String findSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema");
            com.CommandText = findSistema;
            #endregion
            #region Parametros Find Sistema
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sistema.SistemaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Sistema
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaFind, "", 
                    sistema.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindSistema);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Sistema
            DbCommand com = con.CreateCommand();
            String deleteSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema");
            com.CommandText = deleteSistema;
            #endregion
            #region Parametros Delete Sistema
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sistema.SistemaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Sistema
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaDelete, "", 
                    sistema.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteSistema);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Sistema
            DbCommand com = con.CreateCommand();
            String updateSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Carpeta_Sistema",
                        "Id_Sistema");
            com.CommandText = updateSistema;
            #endregion
            #region Parametros Update Sistema
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, sistema.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, sistema.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sistema.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, sistema.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sistema.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sistema.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sistema.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Carpeta_Sistema",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 255, ParameterDirection.Input, sistema.CarpetaSistema);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sistema.SistemaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Sistema
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateSistema = String.Format(Sistema.SistemaResx.SistemaUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,sistema.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,sistema.Abreviatura),
                    sistema.BitActivo.ToString(),
                    sistema.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(sistema.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(sistema.FechaInicio).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(sistema.FechaFin).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteString,sistema.CarpetaSistema),
                    sistema.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateSistema);
                #endregion
            }
            return resultado;
        }
        protected static SistemaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SistemaDP sistema = new SistemaDP();
            sistema.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            sistema.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            sistema.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            sistema.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            sistema.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            sistema.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            sistema.FechaInicio = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            sistema.FechaFin = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            sistema.CarpetaSistema = dr.IsDBNull(8) ? "" : dr.GetString(8);
            return sistema;
            #endregion
        }
        public static SistemaDP Load(DbConnection con, DbTransaction tran, SistemaPk pk)
        {
            #region SQL Load Sistema
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema");
            com.CommandText = loadSistema;
            #endregion
            #region Parametros Load Sistema
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            #endregion
            SistemaDP sistema;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Sistema
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        sistema = ReadRow(dr);
                    }
                    else
                        sistema = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Sistema
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaSelect, "", 
                    pk.SistemaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadSistema);
                sistema = null;
                #endregion
            }
            return sistema;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Sistema
            DbCommand com = con.CreateCommand();
            String countSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaCount,"");
            com.CommandText = countSistema;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Sistema
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountSistema);
                #endregion
            }
            return resultado;
        }
        public static SistemaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Sistema
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSistema = String.Format(CultureInfo.CurrentCulture, Sistema.SistemaResx.SistemaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listSistema = listSistema.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSistema, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Sistema
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Sistema
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Sistema
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaResx.SistemaSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaSistema);
                #endregion
            }
            return (SistemaDP[])lista.ToArray(typeof(SistemaDP));
        }
    }
}
