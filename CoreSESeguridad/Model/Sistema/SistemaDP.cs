using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Sistema".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: viernes, 29 de mayo de 2009.</Para>
    /// <Para>Hora: 10:02:31 a.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Sistema".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>Abreviatura</term><description>Descripcion Abreviatura</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaInicio</term><description>Descripcion FechaInicio</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaFin</term><description>Descripcion FechaFin</description>
    ///    </item>
    ///    <item>
    ///        <term>CarpetaSistema</term><description>Descripcion CarpetaSistema</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SistemaDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// SistemaDTO sistema = new SistemaDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Sistema")]
    public class SistemaDP
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private String nombre;
        private String abreviatura;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private String fechaInicio;
        private String fechaFin;
        private String carpetaSistema;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// Abreviatura
        /// </summary> 
        [XmlElement("Abreviatura")]
        public String Abreviatura
        {
            get {
                    return abreviatura; 
            }
            set {
                    abreviatura = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// FechaInicio
        /// </summary> 
        [XmlElement("FechaInicio")]
        public String FechaInicio
        {
            get {
                    return fechaInicio; 
            }
            set {
                    fechaInicio = value; 
            }
        }

        /// <summary>
        /// FechaFin
        /// </summary> 
        [XmlElement("FechaFin")]
        public String FechaFin
        {
            get {
                    return fechaFin; 
            }
            set {
                    fechaFin = value; 
            }
        }

        /// <summary>
        /// CarpetaSistema
        /// </summary> 
        [XmlElement("CarpetaSistema")]
        public String CarpetaSistema
        {
            get {
                    return carpetaSistema; 
            }
            set {
                    carpetaSistema = value; 
            }
        }

        /// <summary>
        /// Llave primaria de SistemaPk
        /// </summary>
        [XmlElement("Pk")]
        public SistemaPk Pk {
            get {
                    return new SistemaPk( sistemaId );
            }
        }
        #endregion.
    }
}
