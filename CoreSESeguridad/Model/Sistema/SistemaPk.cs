using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "SistemaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: viernes, 29 de mayo de 2009.</Para>
    /// <Para>Hora: 10:02:31 a.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "SistemaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SistemaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// SistemaPk sistemaPk = new SistemaPk(sistema);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("SistemaPk")]
    public class SistemaPk
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de SistemaPk.
        /// </summary>
        /// <param name="sistemaId">Descripción sistemaId del tipo Byte.</param>
        public SistemaPk(Byte sistemaId) 
        {
            this.sistemaId = sistemaId;
        }
        /// <summary>
        /// Constructor normal de SistemaPk.
        /// </summary>
        public SistemaPk() 
        {
        }
        #endregion.
    }
}
