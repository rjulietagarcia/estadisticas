using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class SectorMD
    {
        private SectorDP sector = null;

        public SectorMD(SectorDP sector)
        {
            this.sector = sector;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Sector
            DbCommand com = con.CreateCommand();
            String insertSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "id_sector",
                        "id_tipoeducacion",
                        "id_nivel",
                        "numerosector",
                        "nombre",
                        "clave",
                        "id_sostenimiento",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion");
            com.CommandText = insertSector;
            #endregion
            #region Parametros Insert Sector
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.SectorId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.NivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}numerosector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.Numerosector);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, sector.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, sector.Clave);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sector.SostenimientoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sector.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, sector.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sector.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Sector
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertSector = String.Format(Sector.SectorResx.SectorInsert, "", 
                    sector.SectorId.ToString(),
                    sector.TipoeducacionId.ToString(),
                    sector.NivelId.ToString(),
                    sector.Numerosector.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,sector.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,sector.Clave),
                    sector.SostenimientoId.ToString(),
                    sector.BitActivo.ToString(),
                    sector.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(sector.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertSector);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Sector
            DbCommand com = con.CreateCommand();
            String findSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorFind,
                        ConstantesGlobales.ParameterPrefix,
                        "id_sector");
            com.CommandText = findSector;
            #endregion
            #region Parametros Find Sector
            Common.CreateParameter(com, String.Format("{0}id_sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.SectorId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Sector
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorFind, "", 
                    sector.SectorId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindSector);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Sector
            DbCommand com = con.CreateCommand();
            String deleteSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "id_sector");
            com.CommandText = deleteSector;
            #endregion
            #region Parametros Delete Sector
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.SectorId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Sector
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorDelete, "", 
                    sector.SectorId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteSector);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Sector
            DbCommand com = con.CreateCommand();
            String updateSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel",
                        "numerosector",
                        "nombre",
                        "clave",
                        "id_sostenimiento",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion",
                        "id_sector");
            com.CommandText = updateSector;
            #endregion
            #region Parametros Update Sector
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.NivelId);
            Common.CreateParameter(com, String.Format("{0}numerosector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.Numerosector);
            Common.CreateParameter(com, String.Format("{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, sector.Nombre);
            Common.CreateParameter(com, String.Format("{0}clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, sector.Clave);
            Common.CreateParameter(com, String.Format("{0}id_sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sector.SostenimientoId);
            Common.CreateParameter(com, String.Format("{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sector.BitActivo);
            Common.CreateParameter(com, String.Format("{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, sector.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sector.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}id_sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, sector.SectorId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Sector
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateSector = String.Format(Sector.SectorResx.SectorUpdate, "", 
                    sector.TipoeducacionId.ToString(),
                    sector.NivelId.ToString(),
                    sector.Numerosector.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,sector.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,sector.Clave),
                    sector.SostenimientoId.ToString(),
                    sector.BitActivo.ToString(),
                    sector.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(sector.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    sector.SectorId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateSector);
                #endregion
            }
            return resultado;
        }
        protected static SectorDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SectorDP sector = new SectorDP();
            sector.SectorId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            sector.TipoeducacionId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            sector.NivelId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            sector.Numerosector = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            sector.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            sector.Clave = dr.IsDBNull(5) ? "" : dr.GetString(5);
            sector.SostenimientoId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            sector.BitActivo = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            sector.UsuarioId = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            sector.FechaActualizacion = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            return sector;
            #endregion
        }
        public static SectorDP Load(DbConnection con, DbTransaction tran, SectorPk pk)
        {
            #region SQL Load Sector
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "id_sector");
            com.CommandText = loadSector;
            #endregion
            #region Parametros Load Sector
            Common.CreateParameter(com, String.Format("{0}id_sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.SectorId);
            #endregion
            SectorDP sector;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Sector
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        sector = ReadRow(dr);
                    }
                    else
                        sector = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Sector
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorSelect, "", 
                    pk.SectorId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadSector);
                sector = null;
                #endregion
            }
            return sector;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Sector
            DbCommand com = con.CreateCommand();
            String countSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorCount,"");
            com.CommandText = countSector;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Sector
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountSector);
                #endregion
            }
            return resultado;
        }
        public static SectorDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Sector
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSector = String.Format(CultureInfo.CurrentCulture, Sector.SectorResx.SectorSelectAll, "", ConstantesGlobales.IncluyeRows);
            listSector = listSector.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSector, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Sector
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Sector
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Sector
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaSector = String.Format(CultureInfo.CurrentCulture,Sector.SectorResx.SectorSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaSector);
                #endregion
            }
            return (SectorDP[])lista.ToArray(typeof(SectorDP));
        }
    }
}
