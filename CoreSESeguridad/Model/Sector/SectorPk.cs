using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "SectorPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 04:32:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "SectorPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SectorId</term><description>Descripcion SectorId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SectorPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// SectorPk sectorPk = new SectorPk(sector);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("SectorPk")]
    public class SectorPk
    {
        #region Definicion de campos privados.
        private Int16 sectorId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SectorId
        /// </summary> 
        [XmlElement("SectorId")]
        public Int16 SectorId
        {
            get {
                    return sectorId; 
            }
            set {
                    sectorId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de SectorPk.
        /// </summary>
        /// <param name="sectorId">Descripción sectorId del tipo Int16.</param>
        public SectorPk(Int16 sectorId) 
        {
            this.sectorId = sectorId;
        }
        /// <summary>
        /// Constructor normal de SectorPk.
        /// </summary>
        public SectorPk() 
        {
        }
        #endregion.
    }
}
