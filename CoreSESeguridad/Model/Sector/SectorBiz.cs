using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class SectorBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, SectorDP sector)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, SectorDP sector)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, sector);
                        resultado = InternalSave(con, tran, sector);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla sector!";
                        throw new BusinessException("Error interno al intentar guardar sector!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla sector!";
                throw new BusinessException("Error interno al intentar guardar sector!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, SectorDP sector)
        {
            SectorMD sectorMd = new SectorMD(sector);
            String resultado = "";
            if (sectorMd.Find(con, tran))
            {
                SectorDP sectorAnterior = SectorMD.Load(con, tran, sector.Pk);
                if (sectorMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla sector!";
                    throw new BusinessException("No se pude actualizar la tabla sector!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (sectorMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla sector!";
                    throw new BusinessException("No se pude insertar en la tabla sector!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, SectorDP sector)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, sector);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla sector!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla sector!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla sector!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla sector!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, SectorDP sector)
        {
            String resultado = "";
            SectorMD sectorMd = new SectorMD(sector);
            if (sectorMd.Find(con, tran))
            {
                if (sectorMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla sector!";
                    throw new BusinessException("No se pude eliminar en la tabla sector!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla sector!";
                throw new BusinessException("No se pude eliminar en la tabla sector!");
            }
            return resultado;
        }

        internal static SectorDP LoadDetail(DbConnection con, DbTransaction tran, SectorDP sectorAnterior, SectorDP sector) 
        {
            return sector;
        }
        public static SectorDP Load(DbConnection con, SectorPk pk)
        {
            SectorDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SectorMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Sector!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SectorMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Sector!", ex);
            }
            return resultado;
        }

        public static SectorDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SectorDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SectorMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Sector!", ex);
            }
            return resultado;

        }
    }
}
