using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Turnocriterio".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 16 de junio de 2009.</Para>
    /// <Para>Hora: 03:44:48 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Turnocriterio".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TurnocriterioId</term><description>Descripcion TurnocriterioId</description>
    ///    </item>
    ///    <item>
    ///        <term>TurnoId</term><description>Descripcion TurnoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TurnocriterioDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// TurnocriterioDTO turnocriterio = new TurnocriterioDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Turnocriterio")]
    public class TurnocriterioDP
    {
        #region Definicion de campos privados.
        private Int16 turnocriterioId;
        private Int16 turnoId;
        private String nombre;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TurnocriterioId
        /// </summary> 
        [XmlElement("TurnocriterioId")]
        public Int16 TurnocriterioId
        {
            get {
                    return turnocriterioId; 
            }
            set {
                    turnocriterioId = value; 
            }
        }

        /// <summary>
        /// TurnoId
        /// </summary> 
        [XmlElement("TurnoId")]
        public Int16 TurnoId
        {
            get {
                    return turnoId; 
            }
            set {
                    turnoId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// Llave primaria de TurnocriterioPk
        /// </summary>
        [XmlElement("Pk")]
        public TurnocriterioPk Pk {
            get {
                    return new TurnocriterioPk( turnocriterioId, turnoId );
            }
        }
        #endregion.
    }
}
