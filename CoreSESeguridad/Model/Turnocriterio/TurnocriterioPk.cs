using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "TurnocriterioPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 16 de junio de 2009.</Para>
    /// <Para>Hora: 03:44:48 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "TurnocriterioPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TurnocriterioId</term><description>Descripcion TurnocriterioId</description>
    ///    </item>
    ///    <item>
    ///        <term>TurnoId</term><description>Descripcion TurnoId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TurnocriterioPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// TurnocriterioPk turnocriterioPk = new TurnocriterioPk(turnocriterio);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("TurnocriterioPk")]
    public class TurnocriterioPk
    {
        #region Definicion de campos privados.
        private Int16 turnocriterioId;
        private Int16 turnoId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TurnocriterioId
        /// </summary> 
        [XmlElement("TurnocriterioId")]
        public Int16 TurnocriterioId
        {
            get {
                    return turnocriterioId; 
            }
            set {
                    turnocriterioId = value; 
            }
        }

        /// <summary>
        /// TurnoId
        /// </summary> 
        [XmlElement("TurnoId")]
        public Int16 TurnoId
        {
            get {
                    return turnoId; 
            }
            set {
                    turnoId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de TurnocriterioPk.
        /// </summary>
        /// <param name="turnocriterioId">Descripción turnocriterioId del tipo Int16.</param>
        /// <param name="turnoId">Descripción turnoId del tipo Int16.</param>
        public TurnocriterioPk(Int16 turnocriterioId, Int16 turnoId) 
        {
            this.turnocriterioId = turnocriterioId;
            this.turnoId = turnoId;
        }
        /// <summary>
        /// Constructor normal de TurnocriterioPk.
        /// </summary>
        public TurnocriterioPk() 
        {
        }
        #endregion.
    }
}
