using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class TurnocriterioBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, TurnocriterioDP turnocriterio)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, TurnocriterioDP turnocriterio)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, turnocriterio);
                        resultado = InternalSave(con, tran, turnocriterio);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla turnocriterio!";
                        throw new BusinessException("Error interno al intentar guardar turnocriterio!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla turnocriterio!";
                throw new BusinessException("Error interno al intentar guardar turnocriterio!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, TurnocriterioDP turnocriterio)
        {
            TurnocriterioMD turnocriterioMd = new TurnocriterioMD(turnocriterio);
            String resultado = "";
            if (turnocriterioMd.Find(con, tran))
            {
                TurnocriterioDP turnocriterioAnterior = TurnocriterioMD.Load(con, tran, turnocriterio.Pk);
                if (turnocriterioMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla turnocriterio!";
                    throw new BusinessException("No se pude actualizar la tabla turnocriterio!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (turnocriterioMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla turnocriterio!";
                    throw new BusinessException("No se pude insertar en la tabla turnocriterio!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, TurnocriterioDP turnocriterio)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, turnocriterio);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla turnocriterio!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla turnocriterio!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla turnocriterio!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla turnocriterio!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, TurnocriterioDP turnocriterio)
        {
            String resultado = "";
            TurnocriterioMD turnocriterioMd = new TurnocriterioMD(turnocriterio);
            if (turnocriterioMd.Find(con, tran))
            {
                if (turnocriterioMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla turnocriterio!";
                    throw new BusinessException("No se pude eliminar en la tabla turnocriterio!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla turnocriterio!";
                throw new BusinessException("No se pude eliminar en la tabla turnocriterio!");
            }
            return resultado;
        }

        internal static TurnocriterioDP LoadDetail(DbConnection con, DbTransaction tran, TurnocriterioDP turnocriterioAnterior, TurnocriterioDP turnocriterio) 
        {
            return turnocriterio;
        }
        public static TurnocriterioDP Load(DbConnection con, TurnocriterioPk pk)
        {
            TurnocriterioDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TurnocriterioMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Turnocriterio!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TurnocriterioMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Turnocriterio!", ex);
            }
            return resultado;
        }

        public static TurnocriterioDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TurnocriterioDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TurnocriterioMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Turnocriterio!", ex);
            }
            return resultado;

        }
    }
}
