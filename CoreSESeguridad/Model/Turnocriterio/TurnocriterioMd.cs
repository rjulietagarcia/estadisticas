using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class TurnocriterioMD
    {
        private TurnocriterioDP turnocriterio = null;

        public TurnocriterioMD(TurnocriterioDP turnocriterio)
        {
            this.turnocriterio = turnocriterio;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Turnocriterio
            DbCommand com = con.CreateCommand();
            String insertTurnocriterio = String.Format(CultureInfo.CurrentCulture,Turnocriterio.TurnocriterioResx.TurnocriterioInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "id_turnocriterio",
                        "id_turno",
                        "nombre");
            com.CommandText = insertTurnocriterio;
            #endregion
            #region Parametros Insert Turnocriterio
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_turnocriterio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turnocriterio.TurnocriterioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turnocriterio.TurnoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, turnocriterio.Nombre);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Turnocriterio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Turnocriterio
            DbCommand com = con.CreateCommand();
            String findTurnocriterio = String.Format(CultureInfo.CurrentCulture,Turnocriterio.TurnocriterioResx.TurnocriterioFind,
                        ConstantesGlobales.ParameterPrefix,
                        "id_turnocriterio",
                        "id_turno");
            com.CommandText = findTurnocriterio;
            #endregion
            #region Parametros Find Turnocriterio
            Common.CreateParameter(com, String.Format("{0}id_turnocriterio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turnocriterio.TurnocriterioId);
            Common.CreateParameter(com, String.Format("{0}id_turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turnocriterio.TurnoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Turnocriterio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Turnocriterio
            DbCommand com = con.CreateCommand();
            String deleteTurnocriterio = String.Format(CultureInfo.CurrentCulture,Turnocriterio.TurnocriterioResx.TurnocriterioDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "id_turnocriterio",
                        "id_turno");
            com.CommandText = deleteTurnocriterio;
            #endregion
            #region Parametros Delete Turnocriterio
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_turnocriterio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turnocriterio.TurnocriterioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turnocriterio.TurnoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Turnocriterio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Turnocriterio
            DbCommand com = con.CreateCommand();
            String updateTurnocriterio = String.Format(CultureInfo.CurrentCulture,Turnocriterio.TurnocriterioResx.TurnocriterioUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "nombre",
                        "id_turnocriterio",
                        "id_turno");
            com.CommandText = updateTurnocriterio;
            #endregion
            #region Parametros Update Turnocriterio
            Common.CreateParameter(com, String.Format("{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, turnocriterio.Nombre);
            Common.CreateParameter(com, String.Format("{0}id_turnocriterio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turnocriterio.TurnocriterioId);
            Common.CreateParameter(com, String.Format("{0}id_turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turnocriterio.TurnoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Turnocriterio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static TurnocriterioDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TurnocriterioDP turnocriterio = new TurnocriterioDP();
            turnocriterio.TurnocriterioId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            turnocriterio.TurnoId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            turnocriterio.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            return turnocriterio;
            #endregion
        }
        public static TurnocriterioDP Load(DbConnection con, DbTransaction tran, TurnocriterioPk pk)
        {
            #region SQL Load Turnocriterio
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadTurnocriterio = String.Format(CultureInfo.CurrentCulture,Turnocriterio.TurnocriterioResx.TurnocriterioSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "id_turnocriterio",
                        "id_turno");
            com.CommandText = loadTurnocriterio;
            #endregion
            #region Parametros Load Turnocriterio
            Common.CreateParameter(com, String.Format("{0}id_turnocriterio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.TurnocriterioId);
            Common.CreateParameter(com, String.Format("{0}id_turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.TurnoId);
            #endregion
            TurnocriterioDP turnocriterio;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Turnocriterio
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        turnocriterio = ReadRow(dr);
                    }
                    else
                        turnocriterio = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Turnocriterio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return turnocriterio;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Turnocriterio
            DbCommand com = con.CreateCommand();
            String countTurnocriterio = String.Format(CultureInfo.CurrentCulture,Turnocriterio.TurnocriterioResx.TurnocriterioCount,"");
            com.CommandText = countTurnocriterio;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Turnocriterio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TurnocriterioDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Turnocriterio
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTurnocriterio = String.Format(CultureInfo.CurrentCulture, Turnocriterio.TurnocriterioResx.TurnocriterioSelectAll, "", ConstantesGlobales.IncluyeRows);
            listTurnocriterio = listTurnocriterio.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTurnocriterio, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Turnocriterio
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Turnocriterio
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Turnocriterio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TurnocriterioDP[])lista.ToArray(typeof(TurnocriterioDP));
        }
    }
}
