using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class UsuarioAccionMD
    {
        private UsuarioAccionDP usuarioAccion = null;

        public UsuarioAccionMD(UsuarioAccionDP usuarioAccion)
        {
            this.usuarioAccion = usuarioAccion;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert UsuarioAccion
            DbCommand com = con.CreateCommand();
            String insertUsuarioAccion = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionResx.UsuarioAccionInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Bit_Autorizacion",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertUsuarioAccion;
            #endregion
            #region Parametros Insert UsuarioAccion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccion.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioAccion.CicloescolarId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.AccionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Autorizacion",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioAccion.BitAutorizacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioAccion.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccion.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuarioAccion.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert UsuarioAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find UsuarioAccion
            DbCommand com = con.CreateCommand();
            String findUsuarioAccion = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionResx.UsuarioAccionFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion");
            com.CommandText = findUsuarioAccion;
            #endregion
            #region Parametros Find UsuarioAccion
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccion.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioAccion.CicloescolarId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.AccionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find UsuarioAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete UsuarioAccion
            DbCommand com = con.CreateCommand();
            String deleteUsuarioAccion = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionResx.UsuarioAccionDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion");
            com.CommandText = deleteUsuarioAccion;
            #endregion
            #region Parametros Delete UsuarioAccion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccion.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioAccion.CicloescolarId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.AccionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete UsuarioAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update UsuarioAccion
            DbCommand com = con.CreateCommand();
            String updateUsuarioAccion = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionResx.UsuarioAccionUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Bit_Autorizacion",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion");
            com.CommandText = updateUsuarioAccion;
            #endregion
            #region Parametros Update UsuarioAccion
            Common.CreateParameter(com, String.Format("{0}Bit_Autorizacion",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioAccion.BitAutorizacion);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioAccion.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccion.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuarioAccion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccion.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioAccion.CicloescolarId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccion.AccionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update UsuarioAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static UsuarioAccionDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            UsuarioAccionDP usuarioAccion = new UsuarioAccionDP();
            usuarioAccion.UsuIdUsuarioId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            usuarioAccion.CicloescolarId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            usuarioAccion.SistemaId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            usuarioAccion.ModuloId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            usuarioAccion.OpcionId = dr.IsDBNull(4) ? (Byte)0 : dr.GetByte(4);
            usuarioAccion.AccionId = dr.IsDBNull(5) ? (Byte)0 : dr.GetByte(5);
            usuarioAccion.BitAutorizacion = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            usuarioAccion.BitActivo = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            usuarioAccion.UsuarioId = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            usuarioAccion.FechaActualizacion = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            return usuarioAccion;
            #endregion
        }
        public static UsuarioAccionDP Load(DbConnection con, DbTransaction tran, UsuarioAccionPk pk)
        {
            #region SQL Load UsuarioAccion
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadUsuarioAccion = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionResx.UsuarioAccionSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion");
            com.CommandText = loadUsuarioAccion;
            #endregion
            #region Parametros Load UsuarioAccion
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.UsuIdUsuarioId);
            // Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.CicloescolarId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.AccionId);
            #endregion
            UsuarioAccionDP usuarioAccion;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load UsuarioAccion
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        usuarioAccion = ReadRow(dr);
                    }
                    else
                        usuarioAccion = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load UsuarioAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return usuarioAccion;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count UsuarioAccion
            DbCommand com = con.CreateCommand();
            String countUsuarioAccion = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionResx.UsuarioAccionCount,"");
            com.CommandText = countUsuarioAccion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count UsuarioAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static UsuarioAccionDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List UsuarioAccion
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuarioAccion = String.Format(CultureInfo.CurrentCulture, UsuarioAccion.UsuarioAccionResx.UsuarioAccionSelectAll, "", ConstantesGlobales.IncluyeRows);
            listUsuarioAccion = listUsuarioAccion.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuarioAccion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load UsuarioAccion
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista UsuarioAccion
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista UsuarioAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (UsuarioAccionDP[])lista.ToArray(typeof(UsuarioAccionDP));
        }
    }
}
