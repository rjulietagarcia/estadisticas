using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "UsuarioAccionPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: jueves, 23 de julio de 2009.</Para>
    /// <Para>Hora: 04:26:20 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "UsuarioAccionPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>UsuIdUsuarioId</term><description>Descripcion UsuIdUsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    ///    <item>
    ///        <term>OpcionId</term><description>Descripcion OpcionId</description>
    ///    </item>
    ///    <item>
    ///        <term>AccionId</term><description>Descripcion AccionId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioAccionPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioAccionPk usuarioaccionPk = new UsuarioAccionPk(usuarioaccion);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("UsuarioAccionPk")]
    public class UsuarioAccionPk
    {
        #region Definicion de campos privados.
        private Int32 usuIdUsuarioId;
        private Int16 cicloescolarId;
        private Byte sistemaId;
        private Byte moduloId;
        private Byte opcionId;
        private Byte accionId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// UsuIdUsuarioId
        /// </summary> 
        [XmlElement("UsuIdUsuarioId")]
        public Int32 UsuIdUsuarioId
        {
            get {
                    return usuIdUsuarioId; 
            }
            set {
                    usuIdUsuarioId = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        /// <summary>
        /// OpcionId
        /// </summary> 
        [XmlElement("OpcionId")]
        public Byte OpcionId
        {
            get {
                    return opcionId; 
            }
            set {
                    opcionId = value; 
            }
        }

        /// <summary>
        /// AccionId
        /// </summary> 
        [XmlElement("AccionId")]
        public Byte AccionId
        {
            get {
                    return accionId; 
            }
            set {
                    accionId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de UsuarioAccionPk.
        /// </summary>
        /// <param name="usuIdUsuarioId">Descripción usuIdUsuarioId del tipo Int32.</param>
        /// <param name="cicloescolarId">Descripción cicloescolarId del tipo Int16.</param>
        /// <param name="sistemaId">Descripción sistemaId del tipo Byte.</param>
        /// <param name="moduloId">Descripción moduloId del tipo Byte.</param>
        /// <param name="opcionId">Descripción opcionId del tipo Byte.</param>
        /// <param name="accionId">Descripción accionId del tipo Byte.</param>
        public UsuarioAccionPk(Int32 usuIdUsuarioId, Int16 cicloescolarId, Byte sistemaId, Byte moduloId, Byte opcionId, Byte accionId) 
        {
            this.usuIdUsuarioId = usuIdUsuarioId;
            this.cicloescolarId = cicloescolarId;
            this.sistemaId = sistemaId;
            this.moduloId = moduloId;
            this.opcionId = opcionId;
            this.accionId = accionId;
        }
        /// <summary>
        /// Constructor normal de UsuarioAccionPk.
        /// </summary>
        public UsuarioAccionPk() 
        {
        }
        #endregion.
    }
}
