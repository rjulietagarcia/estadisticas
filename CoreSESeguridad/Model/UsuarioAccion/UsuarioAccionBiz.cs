using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class UsuarioAccionBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, UsuarioAccionDP usuarioAccion)
        {
            #region SaveDetail
            #region FK Usuario
            UsuarioDP usuIdUsuario = usuarioAccion.UsuIdUsuario;
            if (usuIdUsuario != null) 
            {
                UsuarioMD usuarioMd = new UsuarioMD(usuIdUsuario);
                if (!usuarioMd.Find(con, tran))
                    usuarioMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, UsuarioAccionDP usuarioAccion)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, usuarioAccion);
                        resultado = InternalSave(con, tran, usuarioAccion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla usuarioAccion!";
                        throw new BusinessException("Error interno al intentar guardar usuarioAccion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla usuarioAccion!";
                throw new BusinessException("Error interno al intentar guardar usuarioAccion!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, UsuarioAccionDP usuarioAccion)
        {
            UsuarioAccionMD usuarioAccionMd = new UsuarioAccionMD(usuarioAccion);
            String resultado = "";
            if (usuarioAccionMd.Find(con, tran))
            {
                UsuarioAccionDP usuarioAccionAnterior = UsuarioAccionMD.Load(con, tran, usuarioAccion.Pk);
                if (usuarioAccionMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla usuarioAccion!";
                    throw new BusinessException("No se pude actualizar la tabla usuarioAccion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (usuarioAccionMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla usuarioAccion!";
                    throw new BusinessException("No se pude insertar en la tabla usuarioAccion!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, UsuarioAccionDP usuarioAccion)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, usuarioAccion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla usuarioAccion!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla usuarioAccion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla usuarioAccion!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla usuarioAccion!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, UsuarioAccionDP usuarioAccion)
        {
            String resultado = "";
            UsuarioAccionMD usuarioAccionMd = new UsuarioAccionMD(usuarioAccion);
            if (usuarioAccionMd.Find(con, tran))
            {
                if (usuarioAccionMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla usuarioAccion!";
                    throw new BusinessException("No se pude eliminar en la tabla usuarioAccion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla usuarioAccion!";
                throw new BusinessException("No se pude eliminar en la tabla usuarioAccion!");
            }
            return resultado;
        }

        internal static UsuarioAccionDP LoadDetail(DbConnection con, DbTransaction tran, UsuarioAccionDP usuarioAccionAnterior, UsuarioAccionDP usuarioAccion) 
        {
            #region FK UsuIdUsuario
            UsuarioPk usuIdUsuarioPk = new UsuarioPk(
                usuarioAccion.UsuIdUsuarioId);
            if (usuarioAccionAnterior != null)
            {
                if (usuIdUsuarioPk.Equals(usuarioAccionAnterior.Pk))
                    usuarioAccion.UsuIdUsuario = usuarioAccionAnterior.UsuIdUsuario;
                else
                    usuarioAccion.UsuIdUsuario = UsuarioMD.Load(con, tran, usuIdUsuarioPk);
            }
            else
                usuarioAccion.UsuIdUsuario = UsuarioMD.Load(con, tran, usuIdUsuarioPk);
            #endregion
            return usuarioAccion;
        }
        public static UsuarioAccionDP Load(DbConnection con, UsuarioAccionPk pk)
        {
            UsuarioAccionDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioAccionMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un UsuarioAccion!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioAccionMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un UsuarioAccion!", ex);
            }
            return resultado;
        }

        public static UsuarioAccionDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            UsuarioAccionDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioAccionMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de UsuarioAccion!", ex);
            }
            return resultado;

        }
    }
}
