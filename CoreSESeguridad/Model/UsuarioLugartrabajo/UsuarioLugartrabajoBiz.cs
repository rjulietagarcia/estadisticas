using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class UsuarioLugartrabajoBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, UsuarioLugartrabajoDP usuarioLugartrabajo)
        {
            #region SaveDetail
            #region FK Usuario
            UsuarioDP usuarioLt = usuarioLugartrabajo.UsuarioLt;
            if (usuarioLt != null) 
            {
                UsuarioMD usuarioMd = new UsuarioMD(usuarioLt);
                if (!usuarioMd.Find(con, tran))
                    usuarioMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, UsuarioLugartrabajoDP usuarioLugartrabajo)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, usuarioLugartrabajo);
                        resultado = InternalSave(con, tran, usuarioLugartrabajo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla usuarioLugartrabajo!";
                        throw new BusinessException("Error interno al intentar guardar usuarioLugartrabajo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla usuarioLugartrabajo!";
                throw new BusinessException("Error interno al intentar guardar usuarioLugartrabajo!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, UsuarioLugartrabajoDP usuarioLugartrabajo)
        {
            UsuarioLugartrabajoMD usuarioLugartrabajoMd = new UsuarioLugartrabajoMD(usuarioLugartrabajo);
            String resultado = "";
            if (usuarioLugartrabajoMd.Find(con, tran))
            {
                UsuarioLugartrabajoDP usuarioLugartrabajoAnterior = UsuarioLugartrabajoMD.Load(con, tran, usuarioLugartrabajo.Pk);
                if (usuarioLugartrabajoMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla usuarioLugartrabajo!";
                    throw new BusinessException("No se pude actualizar la tabla usuarioLugartrabajo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (usuarioLugartrabajoMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla usuarioLugartrabajo!";
                    throw new BusinessException("No se pude insertar en la tabla usuarioLugartrabajo!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, UsuarioLugartrabajoDP usuarioLugartrabajo)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, usuarioLugartrabajo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla usuarioLugartrabajo!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla usuarioLugartrabajo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla usuarioLugartrabajo!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla usuarioLugartrabajo!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, UsuarioLugartrabajoDP usuarioLugartrabajo)
        {
            String resultado = "";
            UsuarioLugartrabajoMD usuarioLugartrabajoMd = new UsuarioLugartrabajoMD(usuarioLugartrabajo);
            if (usuarioLugartrabajoMd.Find(con, tran))
            {
                if (usuarioLugartrabajoMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla usuarioLugartrabajo!";
                    throw new BusinessException("No se pude eliminar en la tabla usuarioLugartrabajo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla usuarioLugartrabajo!";
                throw new BusinessException("No se pude eliminar en la tabla usuarioLugartrabajo!");
            }
            return resultado;
        }

        internal static UsuarioLugartrabajoDP LoadDetail(DbConnection con, DbTransaction tran, UsuarioLugartrabajoDP usuarioLugartrabajoAnterior, UsuarioLugartrabajoDP usuarioLugartrabajo) 
        {
            #region FK UsuarioLt
            UsuarioPk usuarioLtPk = new UsuarioPk();
            if (usuarioLugartrabajoAnterior != null)
            {
                if (usuarioLtPk.Equals(usuarioLugartrabajoAnterior.Pk))
                    usuarioLugartrabajo.UsuarioLt = usuarioLugartrabajoAnterior.UsuarioLt;
                else
                    usuarioLugartrabajo.UsuarioLt = UsuarioMD.Load(con, tran, usuarioLtPk);
            }
            else
                usuarioLugartrabajo.UsuarioLt = UsuarioMD.Load(con, tran, usuarioLtPk);
            #endregion
            return usuarioLugartrabajo;
        }
        public static UsuarioLugartrabajoDP Load(DbConnection con, UsuarioLugartrabajoPk pk)
        {
            UsuarioLugartrabajoDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioLugartrabajoMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un UsuarioLugartrabajo!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioLugartrabajoMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un UsuarioLugartrabajo!", ex);
            }
            return resultado;
        }

        public static UsuarioLugartrabajoDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            UsuarioLugartrabajoDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioLugartrabajoMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de UsuarioLugartrabajo!", ex);
            }
            return resultado;

        }

        public static int GetNextId(DbConnection con, int centroTrabajoId)
        {
            int resultado = 0;
            
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    
                    //resultado = UsuarioLugartrabajoMD.(con, tran, centroTrabajoId);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un el siguiente id de centro de trabajo!", ex);
            }
            return resultado;
        }
    }
}
