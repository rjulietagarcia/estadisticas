using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "UsuarioLugartrabajoPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 15 de junio de 2009.</Para>
    /// <Para>Hora: 01:31:14 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "UsuarioLugartrabajoPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>ConsecutivoId</term><description>Descripcion ConsecutivoId</description>
    ///    </item>
    ///    <item>
    ///        <term>CentrotrabajoId</term><description>Descripcion CentrotrabajoId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioLugartrabajoPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioLugartrabajoPk usuariolugartrabajoPk = new UsuarioLugartrabajoPk(usuariolugartrabajo);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("UsuarioLugartrabajoPk")]
    public class UsuarioLugartrabajoPk
    {
        #region Definicion de campos privados.
        private Int16 consecutivoId;
        private Int16 centrotrabajoId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// ConsecutivoId
        /// </summary> 
        [XmlElement("ConsecutivoId")]
        public Int16 ConsecutivoId
        {
            get {
                    return consecutivoId; 
            }
            set {
                    consecutivoId = value; 
            }
        }

        /// <summary>
        /// CentrotrabajoId
        /// </summary> 
        [XmlElement("CentrotrabajoId")]
        public Int16 CentrotrabajoId
        {
            get {
                    return centrotrabajoId; 
            }
            set {
                    centrotrabajoId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de UsuarioLugartrabajoPk.
        /// </summary>
        /// <param name="consecutivoId">Descripción consecutivoId del tipo Int16.</param>
        /// <param name="centrotrabajoId">Descripción centrotrabajoId del tipo Int16.</param>
        public UsuarioLugartrabajoPk(Int16 consecutivoId, Int16 centrotrabajoId) 
        {
            this.consecutivoId = consecutivoId;
            this.centrotrabajoId = centrotrabajoId;
        }
        /// <summary>
        /// Constructor normal de UsuarioLugartrabajoPk.
        /// </summary>
        public UsuarioLugartrabajoPk() 
        {
        }
        #endregion.
    }
}
