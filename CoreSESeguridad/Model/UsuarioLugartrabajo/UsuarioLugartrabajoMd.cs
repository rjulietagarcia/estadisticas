using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class UsuarioLugartrabajoMD
    {
        private UsuarioLugartrabajoDP usuarioLugartrabajo = null;

        public UsuarioLugartrabajoMD(UsuarioLugartrabajoDP usuarioLugartrabajo)
        {
            this.usuarioLugartrabajo = usuarioLugartrabajo;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert UsuarioLugartrabajo
            DbCommand com = con.CreateCommand();
            String insertUsuarioLugartrabajo = String.Format(CultureInfo.CurrentCulture,UsuarioLugartrabajo.UsuarioLugartrabajoResx.UsuarioLugartrabajoInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Usuario_LT",
                        "Id_Consecutivo",
                        "Cnsc_UsuarioLugarTrabajo",
                        "Id_NivelTrabajo",
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region",
                        "Id_Zona",
                        "Id_CentroTrabajo",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Sector");
            com.CommandText = insertUsuarioLugartrabajo;
            #endregion
            #region Parametros Insert UsuarioLugartrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario_LT",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioLugartrabajo.UsuarioLtId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Consecutivo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.ConsecutivoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Cnsc_UsuarioLugarTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioLugartrabajo.CnscUsuariolugartrabajo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioLugartrabajo.NiveltrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.RegionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.ZonaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.CentrotrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioLugartrabajo.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioLugartrabajo.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuarioLugartrabajo.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.SectorId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert UsuarioLugartrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find UsuarioLugartrabajo
            DbCommand com = con.CreateCommand();
            String findUsuarioLugartrabajo = String.Format(CultureInfo.CurrentCulture,UsuarioLugartrabajo.UsuarioLugartrabajoResx.UsuarioLugartrabajoFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Consecutivo",
                        "Id_CentroTrabajo");
            com.CommandText = findUsuarioLugartrabajo;
            #endregion
            #region Parametros Find UsuarioLugartrabajo
            Common.CreateParameter(com, String.Format("{0}Id_Consecutivo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.ConsecutivoId);
            Common.CreateParameter(com, String.Format("{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.CentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find UsuarioLugartrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete UsuarioLugartrabajo
            DbCommand com = con.CreateCommand();
            String deleteUsuarioLugartrabajo = String.Format(CultureInfo.CurrentCulture,UsuarioLugartrabajo.UsuarioLugartrabajoResx.UsuarioLugartrabajoDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Consecutivo",
                        "Id_CentroTrabajo");
            com.CommandText = deleteUsuarioLugartrabajo;
            #endregion
            #region Parametros Delete UsuarioLugartrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Consecutivo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.ConsecutivoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.CentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete UsuarioLugartrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update UsuarioLugartrabajo
            DbCommand com = con.CreateCommand();
            String updateUsuarioLugartrabajo = String.Format(CultureInfo.CurrentCulture,UsuarioLugartrabajo.UsuarioLugartrabajoResx.UsuarioLugartrabajoUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Usuario_LT",
                        "Cnsc_UsuarioLugarTrabajo",
                        "Id_NivelTrabajo",
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Region",
                        "Id_Zona",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Sector",
                        "Id_Consecutivo",
                        "Id_CentroTrabajo");
            com.CommandText = updateUsuarioLugartrabajo;
            #endregion
            #region Parametros Update UsuarioLugartrabajo
            Common.CreateParameter(com, String.Format("{0}Id_Usuario_LT",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioLugartrabajo.UsuarioLtId);
            Common.CreateParameter(com, String.Format("{0}Cnsc_UsuarioLugarTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioLugartrabajo.CnscUsuariolugartrabajo);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioLugartrabajo.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Region",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.RegionId);
            Common.CreateParameter(com, String.Format("{0}Id_Zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.ZonaId);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioLugartrabajo.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioLugartrabajo.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuarioLugartrabajo.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Sector",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.SectorId);
            Common.CreateParameter(com, String.Format("{0}Id_Consecutivo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.ConsecutivoId);
            Common.CreateParameter(com, String.Format("{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioLugartrabajo.CentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update UsuarioLugartrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static UsuarioLugartrabajoDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            UsuarioLugartrabajoDP usuarioLugartrabajo = new UsuarioLugartrabajoDP();
            usuarioLugartrabajo.UsuarioLtId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            usuarioLugartrabajo.ConsecutivoId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            usuarioLugartrabajo.CnscUsuariolugartrabajo = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            usuarioLugartrabajo.NiveltrabajoId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            usuarioLugartrabajo.PaisId = dr.IsDBNull(4) ? (short)0 : dr.GetInt16(4);
            usuarioLugartrabajo.EntidadId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            usuarioLugartrabajo.RegionId = dr.IsDBNull(6) ? (short)0 : dr.GetInt16(6);
            usuarioLugartrabajo.ZonaId = dr.IsDBNull(7) ? (short)0 : dr.GetInt16(7);
            usuarioLugartrabajo.CentrotrabajoId = dr.IsDBNull(8) ? (short)0 : dr.GetInt16(8);
            usuarioLugartrabajo.BitActivo = dr.IsDBNull(9) ? false : dr.GetBoolean(9);
            usuarioLugartrabajo.UsuarioId = dr.IsDBNull(10) ? 0 : dr.GetInt32(10);
            usuarioLugartrabajo.FechaActualizacion = dr.IsDBNull(11) ? "" : dr.GetDateTime(11).ToShortDateString();;
            usuarioLugartrabajo.SectorId = dr.IsDBNull(12) ? (short)0 : dr.GetInt16(12);
            return usuarioLugartrabajo;
            #endregion
        }
        public static UsuarioLugartrabajoDP Load(DbConnection con, DbTransaction tran, UsuarioLugartrabajoPk pk)
        {
            #region SQL Load UsuarioLugartrabajo
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadUsuarioLugartrabajo = String.Format(CultureInfo.CurrentCulture,UsuarioLugartrabajo.UsuarioLugartrabajoResx.UsuarioLugartrabajoSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Consecutivo",
                        "Id_CentroTrabajo");
            com.CommandText = loadUsuarioLugartrabajo;
            #endregion
            #region Parametros Load UsuarioLugartrabajo
            Common.CreateParameter(com, String.Format("{0}Id_Consecutivo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.ConsecutivoId);
            Common.CreateParameter(com, String.Format("{0}Id_CentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.CentrotrabajoId);
            #endregion
            UsuarioLugartrabajoDP usuarioLugartrabajo;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load UsuarioLugartrabajo
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        usuarioLugartrabajo = ReadRow(dr);
                    }
                    else
                        usuarioLugartrabajo = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load UsuarioLugartrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return usuarioLugartrabajo;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count UsuarioLugartrabajo
            DbCommand com = con.CreateCommand();
            String countUsuarioLugartrabajo = String.Format(CultureInfo.CurrentCulture,UsuarioLugartrabajo.UsuarioLugartrabajoResx.UsuarioLugartrabajoCount,"");
            com.CommandText = countUsuarioLugartrabajo;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count UsuarioLugartrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static UsuarioLugartrabajoDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List UsuarioLugartrabajo
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuarioLugartrabajo = String.Format(CultureInfo.CurrentCulture, UsuarioLugartrabajo.UsuarioLugartrabajoResx.UsuarioLugartrabajoSelectAll, "", ConstantesGlobales.IncluyeRows);
            listUsuarioLugartrabajo = listUsuarioLugartrabajo.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuarioLugartrabajo, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load UsuarioLugartrabajo
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista UsuarioLugartrabajo
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista UsuarioLugartrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (UsuarioLugartrabajoDP[])lista.ToArray(typeof(UsuarioLugartrabajoDP));
        }
    }
}
