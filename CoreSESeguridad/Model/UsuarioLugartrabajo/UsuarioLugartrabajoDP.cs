using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "UsuarioLugartrabajo".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 15 de junio de 2009.</Para>
    /// <Para>Hora: 01:31:14 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "UsuarioLugartrabajo".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>UsuarioLtId</term><description>Descripcion UsuarioLtId</description>
    ///    </item>
    ///    <item>
    ///        <term>ConsecutivoId</term><description>Descripcion ConsecutivoId</description>
    ///    </item>
    ///    <item>
    ///        <term>CnscUsuariolugartrabajo</term><description>Descripcion CnscUsuariolugartrabajo</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    ///    <item>
    ///        <term>ZonaId</term><description>Descripcion ZonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>CentrotrabajoId</term><description>Descripcion CentrotrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>SectorId</term><description>Descripcion SectorId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioLt</term><description>Descripcion UsuarioLt</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioLugartrabajoDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioLugartrabajoDTO usuariolugartrabajo = new UsuarioLugartrabajoDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("UsuarioLugartrabajo")]
    public class UsuarioLugartrabajoDP
    {
        #region Definicion de campos privados.
        private Int32 usuarioLtId;
        private Int16 consecutivoId;
        private Byte cnscUsuariolugartrabajo;
        private Byte niveltrabajoId;
        private Int16 paisId;
        private Int16 entidadId;
        private Int16 regionId;
        private Int16 zonaId;
        private Int16 centrotrabajoId;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Int16 sectorId;
        private UsuarioDP usuarioLt;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// UsuarioLtId
        /// </summary> 
        [XmlElement("UsuarioLtId")]
        public Int32 UsuarioLtId
        {
            get {
                    return usuarioLtId; 
            }
            set {
                    usuarioLtId = value; 
            }
        }

        /// <summary>
        /// ConsecutivoId
        /// </summary> 
        [XmlElement("ConsecutivoId")]
        public Int16 ConsecutivoId
        {
            get {
                    return consecutivoId; 
            }
            set {
                    consecutivoId = value; 
            }
        }

        /// <summary>
        /// CnscUsuariolugartrabajo
        /// </summary> 
        [XmlElement("CnscUsuariolugartrabajo")]
        public Byte CnscUsuariolugartrabajo
        {
            get {
                    return cnscUsuariolugartrabajo; 
            }
            set {
                    cnscUsuariolugartrabajo = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public Int16 RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        /// <summary>
        /// ZonaId
        /// </summary> 
        [XmlElement("ZonaId")]
        public Int16 ZonaId
        {
            get {
                    return zonaId; 
            }
            set {
                    zonaId = value; 
            }
        }

        /// <summary>
        /// CentrotrabajoId
        /// </summary> 
        [XmlElement("CentrotrabajoId")]
        public Int16 CentrotrabajoId
        {
            get {
                    return centrotrabajoId; 
            }
            set {
                    centrotrabajoId = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// SectorId
        /// </summary> 
        [XmlElement("SectorId")]
        public Int16 SectorId
        {
            get {
                    return sectorId; 
            }
            set {
                    sectorId = value; 
            }
        }

        /// <summary>
        /// UsuarioLt
        /// </summary> 
        [XmlElement("UsuarioLt")]
        public UsuarioDP UsuarioLt
        {
            get {
                    return usuarioLt; 
            }
            set {
                    usuarioLt = value; 
            }
        }

        /// <summary>
        /// Llave primaria de UsuarioLugartrabajoPk
        /// </summary>
        [XmlElement("Pk")]
        public UsuarioLugartrabajoPk Pk {
            get {
                    return new UsuarioLugartrabajoPk( consecutivoId, centrotrabajoId );
            }
        }
        #endregion.
    }
}
