using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class TipoaulaMD
    {
        private TipoaulaDP tipoaula = null;

        public TipoaulaMD(TipoaulaDP tipoaula)
        {
            this.tipoaula = tipoaula;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Tipoaula
            DbCommand com = con.CreateCommand();
            String insertTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_TipoAula",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertTipoaula;
            #endregion
            #region Parametros Insert Tipoaula
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_TipoAula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, tipoaula.TipoaulaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, tipoaula.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, tipoaula.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, tipoaula.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, tipoaula.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(tipoaula.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Tipoaula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertTipoaula = String.Format(Tipoaula.TipoaulaResx.TipoaulaInsert, "", 
                    tipoaula.TipoaulaId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,tipoaula.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,tipoaula.Abreviatura),
                    tipoaula.BitActivo.ToString(),
                    tipoaula.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(tipoaula.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertTipoaula);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Tipoaula
            DbCommand com = con.CreateCommand();
            String findTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_TipoAula");
            com.CommandText = findTipoaula;
            #endregion
            #region Parametros Find Tipoaula
            Common.CreateParameter(com, String.Format("{0}Id_TipoAula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, tipoaula.TipoaulaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Tipoaula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaFind, "", 
                    tipoaula.TipoaulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindTipoaula);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Tipoaula
            DbCommand com = con.CreateCommand();
            String deleteTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_TipoAula");
            com.CommandText = deleteTipoaula;
            #endregion
            #region Parametros Delete Tipoaula
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_TipoAula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, tipoaula.TipoaulaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Tipoaula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaDelete, "", 
                    tipoaula.TipoaulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteTipoaula);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Tipoaula
            DbCommand com = con.CreateCommand();
            String updateTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_TipoAula");
            com.CommandText = updateTipoaula;
            #endregion
            #region Parametros Update Tipoaula
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, tipoaula.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, tipoaula.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, tipoaula.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, tipoaula.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(tipoaula.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_TipoAula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, tipoaula.TipoaulaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Tipoaula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateTipoaula = String.Format(Tipoaula.TipoaulaResx.TipoaulaUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,tipoaula.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,tipoaula.Abreviatura),
                    tipoaula.BitActivo.ToString(),
                    tipoaula.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(tipoaula.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    tipoaula.TipoaulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateTipoaula);
                #endregion
            }
            return resultado;
        }
        protected static TipoaulaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TipoaulaDP tipoaula = new TipoaulaDP();
            tipoaula.TipoaulaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            tipoaula.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            tipoaula.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            tipoaula.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            tipoaula.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            tipoaula.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            return tipoaula;
            #endregion
        }
        public static TipoaulaDP Load(DbConnection con, DbTransaction tran, TipoaulaPk pk)
        {
            #region SQL Load Tipoaula
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_TipoAula");
            com.CommandText = loadTipoaula;
            #endregion
            #region Parametros Load Tipoaula
            Common.CreateParameter(com, String.Format("{0}Id_TipoAula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.TipoaulaId);
            #endregion
            TipoaulaDP tipoaula;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Tipoaula
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        tipoaula = ReadRow(dr);
                    }
                    else
                        tipoaula = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Tipoaula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaSelect, "", 
                    pk.TipoaulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadTipoaula);
                tipoaula = null;
                #endregion
            }
            return tipoaula;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Tipoaula
            DbCommand com = con.CreateCommand();
            String countTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaCount,"");
            com.CommandText = countTipoaula;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Tipoaula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountTipoaula);
                #endregion
            }
            return resultado;
        }
        public static TipoaulaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Tipoaula
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTipoaula = String.Format(CultureInfo.CurrentCulture, Tipoaula.TipoaulaResx.TipoaulaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listTipoaula = listTipoaula.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTipoaula, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Tipoaula
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Tipoaula
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Tipoaula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaTipoaula = String.Format(CultureInfo.CurrentCulture,Tipoaula.TipoaulaResx.TipoaulaSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaTipoaula);
                #endregion
            }
            return (TipoaulaDP[])lista.ToArray(typeof(TipoaulaDP));
        }
    }
}
