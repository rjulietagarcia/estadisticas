using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Tipoaula".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:30:07 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Tipoaula".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TipoaulaId</term><description>Descripcion TipoaulaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>Abreviatura</term><description>Descripcion Abreviatura</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TipoaulaDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// TipoaulaDTO tipoaula = new TipoaulaDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Tipoaula")]
    public class TipoaulaDP
    {
        #region Definicion de campos privados.
        private Byte tipoaulaId;
        private String nombre;
        private String abreviatura;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TipoaulaId
        /// </summary> 
        [XmlElement("TipoaulaId")]
        public Byte TipoaulaId
        {
            get {
                    return tipoaulaId; 
            }
            set {
                    tipoaulaId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// Abreviatura
        /// </summary> 
        [XmlElement("Abreviatura")]
        public String Abreviatura
        {
            get {
                    return abreviatura; 
            }
            set {
                    abreviatura = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// Llave primaria de TipoaulaPk
        /// </summary>
        [XmlElement("Pk")]
        public TipoaulaPk Pk {
            get {
                    return new TipoaulaPk( tipoaulaId );
            }
        }
        #endregion.
    }
}
