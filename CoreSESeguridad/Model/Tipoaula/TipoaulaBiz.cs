using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class TipoaulaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, TipoaulaDP tipoaula)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, TipoaulaDP tipoaula)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, tipoaula);
                        resultado = InternalSave(con, tran, tipoaula);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla tipoaula!";
                        throw new BusinessException("Error interno al intentar guardar tipoaula!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla tipoaula!";
                throw new BusinessException("Error interno al intentar guardar tipoaula!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, TipoaulaDP tipoaula)
        {
            TipoaulaMD tipoaulaMd = new TipoaulaMD(tipoaula);
            String resultado = "";
            if (tipoaulaMd.Find(con, tran))
            {
                TipoaulaDP tipoaulaAnterior = TipoaulaMD.Load(con, tran, tipoaula.Pk);
                if (tipoaulaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla tipoaula!";
                    throw new BusinessException("No se pude actualizar la tabla tipoaula!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (tipoaulaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla tipoaula!";
                    throw new BusinessException("No se pude insertar en la tabla tipoaula!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, TipoaulaDP tipoaula)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, tipoaula);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla tipoaula!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla tipoaula!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla tipoaula!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla tipoaula!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, TipoaulaDP tipoaula)
        {
            String resultado = "";
            TipoaulaMD tipoaulaMd = new TipoaulaMD(tipoaula);
            if (tipoaulaMd.Find(con, tran))
            {
                if (tipoaulaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla tipoaula!";
                    throw new BusinessException("No se pude eliminar en la tabla tipoaula!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla tipoaula!";
                throw new BusinessException("No se pude eliminar en la tabla tipoaula!");
            }
            return resultado;
        }

        internal static TipoaulaDP LoadDetail(DbConnection con, DbTransaction tran, TipoaulaDP tipoaulaAnterior, TipoaulaDP tipoaula) 
        {
            return tipoaula;
        }
        public static TipoaulaDP Load(DbConnection con, TipoaulaPk pk)
        {
            TipoaulaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TipoaulaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Tipoaula!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TipoaulaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Tipoaula!", ex);
            }
            return resultado;
        }

        public static TipoaulaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TipoaulaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TipoaulaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Tipoaula!", ex);
            }
            return resultado;

        }
    }
}
