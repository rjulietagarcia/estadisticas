using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "TipoaulaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:30:07 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "TipoaulaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TipoaulaId</term><description>Descripcion TipoaulaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TipoaulaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// TipoaulaPk tipoaulaPk = new TipoaulaPk(tipoaula);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("TipoaulaPk")]
    public class TipoaulaPk
    {
        #region Definicion de campos privados.
        private Byte tipoaulaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TipoaulaId
        /// </summary> 
        [XmlElement("TipoaulaId")]
        public Byte TipoaulaId
        {
            get {
                    return tipoaulaId; 
            }
            set {
                    tipoaulaId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de TipoaulaPk.
        /// </summary>
        /// <param name="tipoaulaId">Descripción tipoaulaId del tipo Byte.</param>
        public TipoaulaPk(Byte tipoaulaId) 
        {
            this.tipoaulaId = tipoaulaId;
        }
        /// <summary>
        /// Constructor normal de TipoaulaPk.
        /// </summary>
        public TipoaulaPk() 
        {
        }
        #endregion.
    }
}
