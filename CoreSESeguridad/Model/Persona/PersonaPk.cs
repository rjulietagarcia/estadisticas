using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "PersonaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PersonaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PersonaId</term><description>Descripcion PersonaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PersonaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// PersonaPk personaPk = new PersonaPk(persona);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("PersonaPk")]
    public class PersonaPk
    {
        #region Definicion de campos privados.
        private Int32 personaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PersonaId
        /// </summary> 
        [XmlElement("PersonaId")]
        public Int32 PersonaId
        {
            get {
                    return personaId; 
            }
            set {
                    personaId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de PersonaPk.
        /// </summary>
        /// <param name="personaId">Descripción personaId del tipo Int32.</param>
        public PersonaPk(Int32 personaId) 
        {
            this.personaId = personaId;
        }
        /// <summary>
        /// Constructor normal de PersonaPk.
        /// </summary>
        public PersonaPk() 
        {
        }
        #endregion.
    }
}
