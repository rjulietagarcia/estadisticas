using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Persona".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Persona".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PersonaId</term><description>Descripcion PersonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>Curp</term><description>Descripcion Curp</description>
    ///    </item>
    ///    <item>
    ///        <term>Crip</term><description>Descripcion Crip</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>ApellidoPaterno</term><description>Descripcion ApellidoPaterno</description>
    ///    </item>
    ///    <item>
    ///        <term>ApellidoMaterno</term><description>Descripcion ApellidoMaterno</description>
    ///    </item>
    ///    <item>
    ///        <term>Sexo</term><description>Descripcion Sexo</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaNacimiento</term><description>Descripcion FechaNacimiento</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>PadrePersonaId</term><description>Descripcion PadrePersonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>MadrePersonaId</term><description>Descripcion MadrePersonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>BitBuscado</term><description>Descripcion BitBuscado</description>
    ///    </item>
    ///    <item>
    ///        <term>BitEncontrado</term><description>Descripcion BitEncontrado</description>
    ///    </item>
    ///    <item>
    ///        <term>Entidad</term><description>Descripcion Entidad</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PersonaDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// PersonaDTO persona = new PersonaDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Persona")]
    public class PersonaDP
    {
        #region Definicion de campos privados.
        private Int32 personaId;
        private Int16 paisId;
        private Int16 entidadId;
        private String curp;
        private String crip;
        private String nombre;
        private String apellidoPaterno;
        private String apellidoMaterno;
        private String sexo;
        private String fechaNacimiento;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Int32 padrePersonaId;
        private Int32 madrePersonaId;
        private Boolean bitBuscado;
        private Boolean bitEncontrado;
        private EntidadDP entidad;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PersonaId
        /// </summary> 
        [XmlElement("PersonaId")]
        public Int32 PersonaId
        {
            get {
                    return personaId; 
            }
            set {
                    personaId = value; 
            }
        }

        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// Curp
        /// </summary> 
        [XmlElement("Curp")]
        public String Curp
        {
            get {
                    return curp; 
            }
            set {
                    curp = value; 
            }
        }

        /// <summary>
        /// Crip
        /// </summary> 
        [XmlElement("Crip")]
        public String Crip
        {
            get {
                    return crip; 
            }
            set {
                    crip = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// ApellidoPaterno
        /// </summary> 
        [XmlElement("ApellidoPaterno")]
        public String ApellidoPaterno
        {
            get {
                    return apellidoPaterno; 
            }
            set {
                    apellidoPaterno = value; 
            }
        }

        /// <summary>
        /// ApellidoMaterno
        /// </summary> 
        [XmlElement("ApellidoMaterno")]
        public String ApellidoMaterno
        {
            get {
                    return apellidoMaterno; 
            }
            set {
                    apellidoMaterno = value; 
            }
        }

        /// <summary>
        /// Sexo
        /// </summary> 
        [XmlElement("Sexo")]
        public String Sexo
        {
            get {
                    return sexo; 
            }
            set {
                    sexo = value; 
            }
        }

        /// <summary>
        /// FechaNacimiento
        /// </summary> 
        [XmlElement("FechaNacimiento")]
        public String FechaNacimiento
        {
            get {
                    return fechaNacimiento; 
            }
            set {
                    fechaNacimiento = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// PadrePersonaId
        /// </summary> 
        [XmlElement("PadrePersonaId")]
        public Int32 PadrePersonaId
        {
            get {
                    return padrePersonaId; 
            }
            set {
                    padrePersonaId = value; 
            }
        }

        /// <summary>
        /// MadrePersonaId
        /// </summary> 
        [XmlElement("MadrePersonaId")]
        public Int32 MadrePersonaId
        {
            get {
                    return madrePersonaId; 
            }
            set {
                    madrePersonaId = value; 
            }
        }

        /// <summary>
        /// BitBuscado
        /// </summary> 
        [XmlElement("BitBuscado")]
        public Boolean BitBuscado
        {
            get {
                    return bitBuscado; 
            }
            set {
                    bitBuscado = value; 
            }
        }

        /// <summary>
        /// BitEncontrado
        /// </summary> 
        [XmlElement("BitEncontrado")]
        public Boolean BitEncontrado
        {
            get {
                    return bitEncontrado; 
            }
            set {
                    bitEncontrado = value; 
            }
        }

        /// <summary>
        /// Entidad
        /// </summary> 
        [XmlElement("Entidad")]
        public EntidadDP Entidad
        {
            get {
                    return entidad; 
            }
            set {
                    entidad = value; 
            }
        }

        /// <summary>
        /// Llave primaria de PersonaPk
        /// </summary>
        [XmlElement("Pk")]
        public PersonaPk Pk {
            get {
                    return new PersonaPk( personaId );
            }
        }
        #endregion.
    }
}
