using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class PersonaMD
    {
        private PersonaDP persona = null;

        public PersonaMD(PersonaDP persona)
        {
            this.persona = persona;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Persona
            DbCommand com = con.CreateCommand();
            String insertPersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Persona",
                        "Id_Pais",
                        "Id_Entidad",
                        "Curp",
                        "Crip",
                        "Nombre",
                        "Apellido_Paterno",
                        "Apellido_Materno",
                        "Sexo",
                        "Fecha_Nacimiento",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Padre_Persona",
                        "Id_Madre_Persona",
                        "Bit_Buscado",
                        "Bit_Encontrado");
            com.CommandText = insertPersona;
            #endregion
            #region Parametros Insert Persona
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.PersonaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, persona.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, persona.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Curp",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, persona.Curp);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Crip",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 24, ParameterDirection.Input, persona.Crip);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, persona.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Apellido_Paterno",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, persona.ApellidoPaterno);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Apellido_Materno",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, persona.ApellidoMaterno);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Sexo",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, persona.Sexo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Nacimiento",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(persona.FechaNacimiento,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, persona.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(persona.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Padre_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.PadrePersonaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Madre_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.MadrePersonaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Buscado",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, persona.BitBuscado);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Encontrado",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, persona.BitEncontrado);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Persona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertPersona = String.Format(Persona.PersonaResx.PersonaInsert, "", 
                    persona.PersonaId.ToString(),
                    persona.PaisId.ToString(),
                    persona.EntidadId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,persona.Curp),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,persona.Crip),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,persona.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,persona.ApellidoPaterno),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,persona.ApellidoMaterno),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,persona.Sexo),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(persona.FechaNacimiento).ToString(ConstantesGlobales.FormatoFecha)),
                    persona.BitActivo.ToString(),
                    persona.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(persona.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    persona.PadrePersonaId.ToString(),
                    persona.MadrePersonaId.ToString(),
                    persona.BitBuscado.ToString(),
                    persona.BitEncontrado.ToString());
                System.Diagnostics.Debug.WriteLine(errorInsertPersona);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Persona
            DbCommand com = con.CreateCommand();
            String findPersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Persona");
            com.CommandText = findPersona;
            #endregion
            #region Parametros Find Persona
            Common.CreateParameter(com, String.Format("{0}Id_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.PersonaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Persona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindPersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaFind, "", 
                    persona.PersonaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindPersona);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Persona
            DbCommand com = con.CreateCommand();
            String deletePersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Persona");
            com.CommandText = deletePersona;
            #endregion
            #region Parametros Delete Persona
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.PersonaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Persona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeletePersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaDelete, "", 
                    persona.PersonaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeletePersona);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Persona
            DbCommand com = con.CreateCommand();
            String updatePersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Curp",
                        "Crip",
                        "Nombre",
                        "Apellido_Paterno",
                        "Apellido_Materno",
                        "Sexo",
                        "Fecha_Nacimiento",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Padre_Persona",
                        "Id_Madre_Persona",
                        "Bit_Buscado",
                        "Bit_Encontrado",
                        "Id_Persona");
            com.CommandText = updatePersona;
            #endregion
            #region Parametros Update Persona
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, persona.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, persona.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Curp",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 20, ParameterDirection.Input, persona.Curp);
            Common.CreateParameter(com, String.Format("{0}Crip",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 24, ParameterDirection.Input, persona.Crip);
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, persona.Nombre);
            Common.CreateParameter(com, String.Format("{0}Apellido_Paterno",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, persona.ApellidoPaterno);
            Common.CreateParameter(com, String.Format("{0}Apellido_Materno",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, persona.ApellidoMaterno);
            Common.CreateParameter(com, String.Format("{0}Sexo",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, persona.Sexo);
            Common.CreateParameter(com, String.Format("{0}Fecha_Nacimiento",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(persona.FechaNacimiento,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, persona.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(persona.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Padre_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.PadrePersonaId);
            Common.CreateParameter(com, String.Format("{0}Id_Madre_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.MadrePersonaId);
            Common.CreateParameter(com, String.Format("{0}Bit_Buscado",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, persona.BitBuscado);
            Common.CreateParameter(com, String.Format("{0}Bit_Encontrado",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, persona.BitEncontrado);
            Common.CreateParameter(com, String.Format("{0}Id_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, persona.PersonaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Persona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdatePersona = String.Format(Persona.PersonaResx.PersonaUpdate, "", 
                    persona.PaisId.ToString(),
                    persona.EntidadId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,persona.Curp),
                    String.Format(ConstantesGlobales.ConvierteString,persona.Crip),
                    String.Format(ConstantesGlobales.ConvierteString,persona.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,persona.ApellidoPaterno),
                    String.Format(ConstantesGlobales.ConvierteString,persona.ApellidoMaterno),
                    String.Format(ConstantesGlobales.ConvierteString,persona.Sexo),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(persona.FechaNacimiento).ToString(ConstantesGlobales.FormatoFecha)),
                    persona.BitActivo.ToString(),
                    persona.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(persona.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    persona.PadrePersonaId.ToString(),
                    persona.MadrePersonaId.ToString(),
                    persona.BitBuscado.ToString(),
                    persona.BitEncontrado.ToString(),
                    persona.PersonaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdatePersona);
                #endregion
            }
            return resultado;
        }
        protected static PersonaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PersonaDP persona = new PersonaDP();
            persona.PersonaId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            persona.PaisId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            persona.EntidadId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            persona.Curp = dr.IsDBNull(3) ? "" : dr.GetString(3);
            persona.Crip = dr.IsDBNull(4) ? "" : dr.GetString(4);
            persona.Nombre = dr.IsDBNull(5) ? "" : dr.GetString(5);
            persona.ApellidoPaterno = dr.IsDBNull(6) ? "" : dr.GetString(6);
            persona.ApellidoMaterno = dr.IsDBNull(7) ? "" : dr.GetString(7);
            persona.Sexo = dr.IsDBNull(8) ? "" : dr.GetString(8);
            persona.FechaNacimiento = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            persona.BitActivo = dr.IsDBNull(10) ? false : dr.GetBoolean(10);
            persona.UsuarioId = dr.IsDBNull(11) ? 0 : dr.GetInt32(11);
            persona.FechaActualizacion = dr.IsDBNull(12) ? "" : dr.GetDateTime(12).ToShortDateString();;
            persona.PadrePersonaId = dr.IsDBNull(13) ? 0 : dr.GetInt32(13);
            persona.MadrePersonaId = dr.IsDBNull(14) ? 0 : dr.GetInt32(14);
            persona.BitBuscado = dr.IsDBNull(15) ? false : dr.GetBoolean(15);
            persona.BitEncontrado = dr.IsDBNull(16) ? false : dr.GetBoolean(16);
            return persona;
            #endregion
        }
        public static PersonaDP Load(DbConnection con, DbTransaction tran, PersonaPk pk)
        {
            #region SQL Load Persona
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Persona");
            com.CommandText = loadPersona;
            #endregion
            #region Parametros Load Persona
            Common.CreateParameter(com, String.Format("{0}Id_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.PersonaId);
            #endregion
            PersonaDP persona;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Persona
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        persona = ReadRow(dr);
                    }
                    else
                        persona = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Persona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadPersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaSelect, "", 
                    pk.PersonaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadPersona);
                persona = null;
                #endregion
            }
            return persona;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Persona
            DbCommand com = con.CreateCommand();
            String countPersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaCount,"");
            com.CommandText = countPersona;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Persona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPersona);
                #endregion
            }
            return resultado;
        }
        public static PersonaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Persona
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPersona = String.Format(CultureInfo.CurrentCulture, Persona.PersonaResx.PersonaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listPersona = listPersona.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPersona, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Persona
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Persona
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Persona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPersona = String.Format(CultureInfo.CurrentCulture,Persona.PersonaResx.PersonaSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPersona);
                #endregion
            }
            return (PersonaDP[])lista.ToArray(typeof(PersonaDP));
        }
    }
}
