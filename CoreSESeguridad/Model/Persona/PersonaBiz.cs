using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class PersonaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, PersonaDP persona)
        {
            #region SaveDetail
            #region FK Entidad
            EntidadDP entidad = persona.Entidad;
            if (entidad != null) 
            {
                EntidadMD entidadMd = new EntidadMD(entidad);
                if (!entidadMd.Find(con, tran))
                    entidadMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, PersonaDP persona)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, persona);
                        resultado = InternalSave(con, tran, persona);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla persona!";
                        throw new BusinessException("Error interno al intentar guardar persona!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla persona!";
                throw new BusinessException("Error interno al intentar guardar persona!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, PersonaDP persona)
        {
            PersonaMD personaMd = new PersonaMD(persona);
            String resultado = "";
            if (personaMd.Find(con, tran))
            {
                PersonaDP personaAnterior = PersonaMD.Load(con, tran, persona.Pk);
                if (personaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla persona!";
                    throw new BusinessException("No se pude actualizar la tabla persona!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (personaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla persona!";
                    throw new BusinessException("No se pude insertar en la tabla persona!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, PersonaDP persona)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, persona);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla persona!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla persona!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla persona!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla persona!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, PersonaDP persona)
        {
            String resultado = "";
            PersonaMD personaMd = new PersonaMD(persona);
            if (personaMd.Find(con, tran))
            {
                if (personaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla persona!";
                    throw new BusinessException("No se pude eliminar en la tabla persona!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla persona!";
                throw new BusinessException("No se pude eliminar en la tabla persona!");
            }
            return resultado;
        }

        internal static PersonaDP LoadDetail(DbConnection con, DbTransaction tran, PersonaDP personaAnterior, PersonaDP persona) 
        {
            #region FK Entidad
            EntidadPk entidadPk = new EntidadPk();
            if (personaAnterior != null)
            {
                if (entidadPk.Equals(personaAnterior.Pk))
                    persona.Entidad = personaAnterior.Entidad;
                else
                    persona.Entidad = EntidadMD.Load(con, tran, entidadPk);
            }
            else
                persona.Entidad = EntidadMD.Load(con, tran, entidadPk);
            #endregion
            return persona;
        }
        public static PersonaDP Load(DbConnection con, PersonaPk pk)
        {
            PersonaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PersonaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Persona!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PersonaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Persona!", ex);
            }
            return resultado;
        }

        public static PersonaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PersonaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PersonaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Persona!", ex);
            }
            return resultado;

        }
    }
}
