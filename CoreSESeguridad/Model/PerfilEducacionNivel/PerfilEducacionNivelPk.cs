using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilEducacionNivelPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: miércoles, 29 de julio de 2009.</Para>
    /// <Para>Hora: 12:41:09 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilEducacionNivelPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>SubnivelId</term><description>Descripcion SubnivelId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilEducacionNivelPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// PerfilEducacionNivelPk perfileducacionnivelPk = new PerfilEducacionNivelPk(perfileducacionnivel);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("PerfilEducacionNivelPk")]
    public class PerfilEducacionNivelPk
    {
        #region Definicion de campos privados.
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Byte sistemaId;
        private Int16 tipoeducacionId;
        private Int16 nivelId;
        private Int16 subnivelId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public Int16 TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public Int16 NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        /// <summary>
        /// SubnivelId
        /// </summary> 
        [XmlElement("SubnivelId")]
        public Int16 SubnivelId
        {
            get {
                    return subnivelId; 
            }
            set {
                    subnivelId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de PerfilEducacionNivelPk.
        /// </summary>
        /// <param name="perfilEducacionId">Descripción perfilEducacionId del tipo Int16.</param>
        /// <param name="niveltrabajoId">Descripción niveltrabajoId del tipo Byte.</param>
        /// <param name="sistemaId">Descripción sistemaId del tipo Byte.</param>
        /// <param name="tipoeducacionId">Descripción tipoeducacionId del tipo Int16.</param>
        /// <param name="nivelId">Descripción nivelId del tipo Int16.</param>
        /// <param name="subnivelId">Descripción subnivelId del tipo Int16.</param>
        public PerfilEducacionNivelPk(Int16 perfilEducacionId, Byte niveltrabajoId, Byte sistemaId, Int16 tipoeducacionId, Int16 nivelId, Int16 subnivelId) 
        {
            this.perfilEducacionId = perfilEducacionId;
            this.niveltrabajoId = niveltrabajoId;
            this.sistemaId = sistemaId;
            this.tipoeducacionId = tipoeducacionId;
            this.nivelId = nivelId;
            this.subnivelId = subnivelId;
        }
        /// <summary>
        /// Constructor normal de PerfilEducacionNivelPk.
        /// </summary>
        public PerfilEducacionNivelPk() 
        {
        }
        #endregion.
    }
}
