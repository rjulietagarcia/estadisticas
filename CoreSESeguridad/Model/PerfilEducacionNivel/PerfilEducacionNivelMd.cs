using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class PerfilEducacionNivelMD
    {
        private PerfilEducacionNivelDP perfilEducacionNivel = null;

        public PerfilEducacionNivelMD(PerfilEducacionNivelDP perfilEducacionNivel)
        {
            this.perfilEducacionNivel = perfilEducacionNivel;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert PerfilEducacionNivel
            DbCommand com = con.CreateCommand();
            String insertPerfilEducacionNivel = String.Format(CultureInfo.CurrentCulture,PerfilEducacionNivel.PerfilEducacionNivelResx.PerfilEducacionNivelInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Sistema",
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = insertPerfilEducacionNivel;
            #endregion
            #region Parametros Insert PerfilEducacionNivel
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionNivel.NiveltrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacionNivel.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfilEducacionNivel.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfilEducacionNivel.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionNivel.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.NivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.SubnivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert PerfilEducacionNivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find PerfilEducacionNivel
            DbCommand com = con.CreateCommand();
            String findPerfilEducacionNivel = String.Format(CultureInfo.CurrentCulture,PerfilEducacionNivel.PerfilEducacionNivelResx.PerfilEducacionNivelFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Id_Sistema",
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = findPerfilEducacionNivel;
            #endregion
            #region Parametros Find PerfilEducacionNivel
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionNivel.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionNivel.SistemaId);
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.NivelId);
            Common.CreateParameter(com, String.Format("{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.SubnivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find PerfilEducacionNivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete PerfilEducacionNivel
            DbCommand com = con.CreateCommand();
            String deletePerfilEducacionNivel = String.Format(CultureInfo.CurrentCulture,PerfilEducacionNivel.PerfilEducacionNivelResx.PerfilEducacionNivelDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Id_Sistema",
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = deletePerfilEducacionNivel;
            #endregion
            #region Parametros Delete PerfilEducacionNivel
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionNivel.NiveltrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionNivel.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.NivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.SubnivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete PerfilEducacionNivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update PerfilEducacionNivel
            DbCommand com = con.CreateCommand();
            String updatePerfilEducacionNivel = String.Format(CultureInfo.CurrentCulture,PerfilEducacionNivel.PerfilEducacionNivelResx.PerfilEducacionNivelUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Id_Sistema",
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = updatePerfilEducacionNivel;
            #endregion
            #region Parametros Update PerfilEducacionNivel
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacionNivel.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfilEducacionNivel.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfilEducacionNivel.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionNivel.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionNivel.SistemaId);
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.NivelId);
            Common.CreateParameter(com, String.Format("{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionNivel.SubnivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update PerfilEducacionNivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static PerfilEducacionNivelDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilEducacionNivelDP perfilEducacionNivel = new PerfilEducacionNivelDP();
            perfilEducacionNivel.PerfilEducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            perfilEducacionNivel.NiveltrabajoId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            perfilEducacionNivel.BitActivo = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            perfilEducacionNivel.UsuarioId = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            perfilEducacionNivel.FechaActualizacion = dr.IsDBNull(4) ? "" : dr.GetDateTime(4).ToShortDateString();;
            perfilEducacionNivel.SistemaId = dr.IsDBNull(5) ? (Byte)0 : dr.GetByte(5);
            perfilEducacionNivel.TipoeducacionId = dr.IsDBNull(6) ? (short)0 : dr.GetInt16(6);
            perfilEducacionNivel.NivelId = dr.IsDBNull(7) ? (short)0 : dr.GetInt16(7);
            perfilEducacionNivel.SubnivelId = dr.IsDBNull(8) ? (short)0 : dr.GetInt16(8);
            return perfilEducacionNivel;
            #endregion
        }
        public static PerfilEducacionNivelDP Load(DbConnection con, DbTransaction tran, PerfilEducacionNivelPk pk)
        {
            #region SQL Load PerfilEducacionNivel
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPerfilEducacionNivel = String.Format(CultureInfo.CurrentCulture,PerfilEducacionNivel.PerfilEducacionNivelResx.PerfilEducacionNivelSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo",
                        "Id_Sistema",
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = loadPerfilEducacionNivel;
            #endregion
            #region Parametros Load PerfilEducacionNivel
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.NiveltrabajoId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.NivelId);
            Common.CreateParameter(com, String.Format("{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.SubnivelId);
            #endregion
            PerfilEducacionNivelDP perfilEducacionNivel;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load PerfilEducacionNivel
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        perfilEducacionNivel = ReadRow(dr);
                    }
                    else
                        perfilEducacionNivel = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load PerfilEducacionNivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return perfilEducacionNivel;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilEducacionNivel
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionNivel = String.Format(CultureInfo.CurrentCulture,PerfilEducacionNivel.PerfilEducacionNivelResx.PerfilEducacionNivelCount,"");
            com.CommandText = countPerfilEducacionNivel;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacionNivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionNivelDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilEducacionNivel
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionNivel = String.Format(CultureInfo.CurrentCulture, PerfilEducacionNivel.PerfilEducacionNivelResx.PerfilEducacionNivelSelectAll, "", ConstantesGlobales.IncluyeRows);
            listPerfilEducacionNivel = listPerfilEducacionNivel.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPerfilEducacionNivel, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionNivel
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionNivel
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionNivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PerfilEducacionNivelDP[])lista.ToArray(typeof(PerfilEducacionNivelDP));
        }
    }
}
