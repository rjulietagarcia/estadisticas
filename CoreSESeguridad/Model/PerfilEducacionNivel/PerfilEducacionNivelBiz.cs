using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class PerfilEducacionNivelBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, PerfilEducacionNivelDP perfilEducacionNivel)
        {
            #region SaveDetail
            #region FK PerfilEducacion
            PerfilEducacionDP perfilEducacion = perfilEducacionNivel.PerfilEducacion;
            if (perfilEducacion != null) 
            {
                PerfilEducacionMD perfilEducacionMd = new PerfilEducacionMD(perfilEducacion);
                if (!perfilEducacionMd.Find(con, tran))
                    perfilEducacionMd.Insert(con, tran);
            }
            #endregion
            #region FK Sistema
            SistemaDP sistema = perfilEducacionNivel.Sistema;
            if (sistema != null) 
            {
                SistemaMD sistemaMd = new SistemaMD(sistema);
                if (!sistemaMd.Find(con, tran))
                    sistemaMd.Insert(con, tran);
            }
            #endregion
            #region FK Subnivel
            SubnivelDP subnivel = perfilEducacionNivel.Subnivel;
            if (subnivel != null) 
            {
                SubnivelMD subnivelMd = new SubnivelMD(subnivel);
                if (!subnivelMd.Find(con, tran))
                    subnivelMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, PerfilEducacionNivelDP perfilEducacionNivel)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, perfilEducacionNivel);
                        resultado = InternalSave(con, tran, perfilEducacionNivel);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla perfilEducacionNivel!";
                        throw new BusinessException("Error interno al intentar guardar perfilEducacionNivel!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla perfilEducacionNivel!";
                throw new BusinessException("Error interno al intentar guardar perfilEducacionNivel!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, PerfilEducacionNivelDP perfilEducacionNivel)
        {
            PerfilEducacionNivelMD perfilEducacionNivelMd = new PerfilEducacionNivelMD(perfilEducacionNivel);
            String resultado = "";
            if (perfilEducacionNivelMd.Find(con, tran))
            {
                PerfilEducacionNivelDP perfilEducacionNivelAnterior = PerfilEducacionNivelMD.Load(con, tran, perfilEducacionNivel.Pk);
                if (perfilEducacionNivelMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla perfilEducacionNivel!";
                    throw new BusinessException("No se pude actualizar la tabla perfilEducacionNivel!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (perfilEducacionNivelMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla perfilEducacionNivel!";
                    throw new BusinessException("No se pude insertar en la tabla perfilEducacionNivel!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, PerfilEducacionNivelDP perfilEducacionNivel)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, perfilEducacionNivel);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla perfilEducacionNivel!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla perfilEducacionNivel!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla perfilEducacionNivel!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla perfilEducacionNivel!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, PerfilEducacionNivelDP perfilEducacionNivel)
        {
            String resultado = "";
            PerfilEducacionNivelMD perfilEducacionNivelMd = new PerfilEducacionNivelMD(perfilEducacionNivel);
            if (perfilEducacionNivelMd.Find(con, tran))
            {
                if (perfilEducacionNivelMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla perfilEducacionNivel!";
                    throw new BusinessException("No se pude eliminar en la tabla perfilEducacionNivel!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla perfilEducacionNivel!";
                throw new BusinessException("No se pude eliminar en la tabla perfilEducacionNivel!");
            }
            return resultado;
        }

        internal static PerfilEducacionNivelDP LoadDetail(DbConnection con, DbTransaction tran, PerfilEducacionNivelDP perfilEducacionNivelAnterior, PerfilEducacionNivelDP perfilEducacionNivel) 
        {
            #region FK PerfilEducacion
            PerfilEducacionPk perfilEducacionPk = new PerfilEducacionPk();
            if (perfilEducacionNivelAnterior != null)
            {
                if (perfilEducacionPk.Equals(perfilEducacionNivelAnterior.Pk))
                    perfilEducacionNivel.PerfilEducacion = perfilEducacionNivelAnterior.PerfilEducacion;
                else
                    perfilEducacionNivel.PerfilEducacion = PerfilEducacionMD.Load(con, tran, perfilEducacionPk);
            }
            else
                perfilEducacionNivel.PerfilEducacion = PerfilEducacionMD.Load(con, tran, perfilEducacionPk);
            #endregion
            #region FK Sistema
            SistemaPk sistemaPk = new SistemaPk();
            if (perfilEducacionNivelAnterior != null)
            {
                if (sistemaPk.Equals(perfilEducacionNivelAnterior.Pk))
                    perfilEducacionNivel.Sistema = perfilEducacionNivelAnterior.Sistema;
                else
                    perfilEducacionNivel.Sistema = SistemaMD.Load(con, tran, sistemaPk);
            }
            else
                perfilEducacionNivel.Sistema = SistemaMD.Load(con, tran, sistemaPk);
            #endregion
            #region FK Subnivel
            SubnivelPk subnivelPk = new SubnivelPk();
            if (perfilEducacionNivelAnterior != null)
            {
                if (subnivelPk.Equals(perfilEducacionNivelAnterior.Pk))
                    perfilEducacionNivel.Subnivel = perfilEducacionNivelAnterior.Subnivel;
                else
                    perfilEducacionNivel.Subnivel = SubnivelMD.Load(con, tran, subnivelPk);
            }
            else
                perfilEducacionNivel.Subnivel = SubnivelMD.Load(con, tran, subnivelPk);
            #endregion
            return perfilEducacionNivel;
        }
        public static PerfilEducacionNivelDP Load(DbConnection con, PerfilEducacionNivelPk pk)
        {
            PerfilEducacionNivelDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionNivelMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un PerfilEducacionNivel!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilEducacionNivelMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilEducacionNivel!", ex);
            }
            return resultado;
        }

        public static PerfilEducacionNivelDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PerfilEducacionNivelDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionNivelMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilEducacionNivel!", ex);
            }
            return resultado;

        }
    }
}
