using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class PerfilEducacionAccionBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, PerfilEducacionAccionDP perfilEducacionAccion)
        {
            #region SaveDetail
            #region FK Accion
            AccionDP accion = perfilEducacionAccion.Accion;
            if (accion != null) 
            {
                AccionMD accionMd = new AccionMD(accion);
                if (!accionMd.Find(con, tran))
                    accionMd.Insert(con, tran);
            }
            #endregion
            #region FK PerfilEducacion
            PerfilEducacionDP perfilEducacion = perfilEducacionAccion.PerfilEducacion;
            if (perfilEducacion != null) 
            {
                PerfilEducacionMD perfilEducacionMd = new PerfilEducacionMD(perfilEducacion);
                if (!perfilEducacionMd.Find(con, tran))
                    perfilEducacionMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, PerfilEducacionAccionDP perfilEducacionAccion)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, perfilEducacionAccion);
                        resultado = InternalSave(con, tran, perfilEducacionAccion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla perfilEducacionAccion!";
                        throw new BusinessException("Error interno al intentar guardar perfilEducacionAccion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla perfilEducacionAccion!";
                throw new BusinessException("Error interno al intentar guardar perfilEducacionAccion!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, PerfilEducacionAccionDP perfilEducacionAccion)
        {
            PerfilEducacionAccionMD perfilEducacionAccionMd = new PerfilEducacionAccionMD(perfilEducacionAccion);
            String resultado = "";
            if (perfilEducacionAccionMd.Find(con, tran))
            {
                PerfilEducacionAccionDP perfilEducacionAccionAnterior = PerfilEducacionAccionMD.Load(con, tran, perfilEducacionAccion.Pk);
                if (perfilEducacionAccionMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla perfilEducacionAccion!";
                    throw new BusinessException("No se pude actualizar la tabla perfilEducacionAccion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (perfilEducacionAccionMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla perfilEducacionAccion!";
                    throw new BusinessException("No se pude insertar en la tabla perfilEducacionAccion!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, PerfilEducacionAccionDP perfilEducacionAccion)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, perfilEducacionAccion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla perfilEducacionAccion!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla perfilEducacionAccion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla perfilEducacionAccion!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla perfilEducacionAccion!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, PerfilEducacionAccionDP perfilEducacionAccion)
        {
            String resultado = "";
            PerfilEducacionAccionMD perfilEducacionAccionMd = new PerfilEducacionAccionMD(perfilEducacionAccion);
            if (perfilEducacionAccionMd.Find(con, tran))
            {
                if (perfilEducacionAccionMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla perfilEducacionAccion!";
                    throw new BusinessException("No se pude eliminar en la tabla perfilEducacionAccion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla perfilEducacionAccion!";
                throw new BusinessException("No se pude eliminar en la tabla perfilEducacionAccion!");
            }
            return resultado;
        }

        internal static PerfilEducacionAccionDP LoadDetail(DbConnection con, DbTransaction tran, PerfilEducacionAccionDP perfilEducacionAccionAnterior, PerfilEducacionAccionDP perfilEducacionAccion) 
        {
            #region FK Accion
            AccionPk accionPk = new AccionPk();
            if (perfilEducacionAccionAnterior != null)
            {
                if (accionPk.Equals(perfilEducacionAccionAnterior.Pk))
                    perfilEducacionAccion.Accion = perfilEducacionAccionAnterior.Accion;
                else
                    perfilEducacionAccion.Accion = AccionMD.Load(con, tran, accionPk);
            }
            else
                perfilEducacionAccion.Accion = AccionMD.Load(con, tran, accionPk);
            #endregion
            #region FK PerfilEducacion
            PerfilEducacionPk perfilEducacionPk = new PerfilEducacionPk();
            if (perfilEducacionAccionAnterior != null)
            {
                if (perfilEducacionPk.Equals(perfilEducacionAccionAnterior.Pk))
                    perfilEducacionAccion.PerfilEducacion = perfilEducacionAccionAnterior.PerfilEducacion;
                else
                    perfilEducacionAccion.PerfilEducacion = PerfilEducacionMD.Load(con, tran, perfilEducacionPk);
            }
            else
                perfilEducacionAccion.PerfilEducacion = PerfilEducacionMD.Load(con, tran, perfilEducacionPk);
            #endregion
            return perfilEducacionAccion;
        }
        public static PerfilEducacionAccionDP Load(DbConnection con, PerfilEducacionAccionPk pk)
        {
            PerfilEducacionAccionDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionAccionMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un PerfilEducacionAccion!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilEducacionAccionMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilEducacionAccion!", ex);
            }
            return resultado;
        }

        public static PerfilEducacionAccionDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PerfilEducacionAccionDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionAccionMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilEducacionAccion!", ex);
            }
            return resultado;

        }
    }
}
