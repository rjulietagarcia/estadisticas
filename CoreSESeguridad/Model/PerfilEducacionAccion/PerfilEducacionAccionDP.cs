using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilEducacionAccion".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilEducacionAccion".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    ///    <item>
    ///        <term>OpcionId</term><description>Descripcion OpcionId</description>
    ///    </item>
    ///    <item>
    ///        <term>AccionId</term><description>Descripcion AccionId</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>BitAutorizacion</term><description>Descripcion BitAutorizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Accion</term><description>Descripcion Accion</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacion</term><description>Descripcion PerfilEducacion</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilEducacionAccionDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// PerfilEducacionAccionDTO perfileducacionaccion = new PerfilEducacionAccionDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("PerfilEducacionAccion")]
    public class PerfilEducacionAccionDP
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Byte moduloId;
        private Byte opcionId;
        private Byte accionId;
        private Int16 perfilEducacionId;
        private Boolean bitAutorizacion;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Byte niveltrabajoId;
        private AccionDP accion;
        private PerfilEducacionDP perfilEducacion;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        /// <summary>
        /// OpcionId
        /// </summary> 
        [XmlElement("OpcionId")]
        public Byte OpcionId
        {
            get {
                    return opcionId; 
            }
            set {
                    opcionId = value; 
            }
        }

        /// <summary>
        /// AccionId
        /// </summary> 
        [XmlElement("AccionId")]
        public Byte AccionId
        {
            get {
                    return accionId; 
            }
            set {
                    accionId = value; 
            }
        }

        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// BitAutorizacion
        /// </summary> 
        [XmlElement("BitAutorizacion")]
        public Boolean BitAutorizacion
        {
            get {
                    return bitAutorizacion; 
            }
            set {
                    bitAutorizacion = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// Accion
        /// </summary> 
        [XmlElement("Accion")]
        public AccionDP Accion
        {
            get {
                    return accion; 
            }
            set {
                    accion = value; 
            }
        }

        /// <summary>
        /// PerfilEducacion
        /// </summary> 
        [XmlElement("PerfilEducacion")]
        public PerfilEducacionDP PerfilEducacion
        {
            get {
                    return perfilEducacion; 
            }
            set {
                    perfilEducacion = value; 
            }
        }

        /// <summary>
        /// Llave primaria de PerfilEducacionAccionPk
        /// </summary>
        [XmlElement("Pk")]
        public PerfilEducacionAccionPk Pk {
            get {
                    return new PerfilEducacionAccionPk( sistemaId, moduloId, opcionId, accionId, perfilEducacionId, niveltrabajoId );
            }
        }
        #endregion.
    }
}
