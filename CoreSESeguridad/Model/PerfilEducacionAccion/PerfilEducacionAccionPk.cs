using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilEducacionAccionPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilEducacionAccionPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    ///    <item>
    ///        <term>OpcionId</term><description>Descripcion OpcionId</description>
    ///    </item>
    ///    <item>
    ///        <term>AccionId</term><description>Descripcion AccionId</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilEducacionAccionPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// PerfilEducacionAccionPk perfileducacionaccionPk = new PerfilEducacionAccionPk(perfileducacionaccion);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("PerfilEducacionAccionPk")]
    public class PerfilEducacionAccionPk
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Byte moduloId;
        private Byte opcionId;
        private Byte accionId;
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        /// <summary>
        /// OpcionId
        /// </summary> 
        [XmlElement("OpcionId")]
        public Byte OpcionId
        {
            get {
                    return opcionId; 
            }
            set {
                    opcionId = value; 
            }
        }

        /// <summary>
        /// AccionId
        /// </summary> 
        [XmlElement("AccionId")]
        public Byte AccionId
        {
            get {
                    return accionId; 
            }
            set {
                    accionId = value; 
            }
        }

        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de PerfilEducacionAccionPk.
        /// </summary>
        /// <param name="sistemaId">Descripción sistemaId del tipo Byte.</param>
        /// <param name="moduloId">Descripción moduloId del tipo Byte.</param>
        /// <param name="opcionId">Descripción opcionId del tipo Byte.</param>
        /// <param name="accionId">Descripción accionId del tipo Byte.</param>
        /// <param name="perfilEducacionId">Descripción perfilEducacionId del tipo Int16.</param>
        /// <param name="niveltrabajoId">Descripción niveltrabajoId del tipo Byte.</param>
        public PerfilEducacionAccionPk(Byte sistemaId, Byte moduloId, Byte opcionId, Byte accionId, Int16 perfilEducacionId, Byte niveltrabajoId) 
        {
            this.sistemaId = sistemaId;
            this.moduloId = moduloId;
            this.opcionId = opcionId;
            this.accionId = accionId;
            this.perfilEducacionId = perfilEducacionId;
            this.niveltrabajoId = niveltrabajoId;
        }
        /// <summary>
        /// Constructor normal de PerfilEducacionAccionPk.
        /// </summary>
        public PerfilEducacionAccionPk() 
        {
        }
        #endregion.
    }
}
