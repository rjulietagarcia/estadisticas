using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class PerfilEducacionAccionMD
    {
        private PerfilEducacionAccionDP perfilEducacionAccion = null;

        public PerfilEducacionAccionMD(PerfilEducacionAccionDP perfilEducacionAccion)
        {
            this.perfilEducacionAccion = perfilEducacionAccion;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert PerfilEducacionAccion
            DbCommand com = con.CreateCommand();
            String insertPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Id_Perfil_Educacion",
                        "Bit_Autorizacion",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_NivelTrabajo");
            com.CommandText = insertPerfilEducacionAccion;
            #endregion
            #region Parametros Insert PerfilEducacionAccion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.AccionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionAccion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Autorizacion",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacionAccion.BitAutorizacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacionAccion.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfilEducacionAccion.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfilEducacionAccion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert PerfilEducacionAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertPerfilEducacionAccion = String.Format(PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionInsert, "", 
                    perfilEducacionAccion.SistemaId.ToString(),
                    perfilEducacionAccion.ModuloId.ToString(),
                    perfilEducacionAccion.OpcionId.ToString(),
                    perfilEducacionAccion.AccionId.ToString(),
                    perfilEducacionAccion.PerfilEducacionId.ToString(),
                    perfilEducacionAccion.BitAutorizacion.ToString(),
                    perfilEducacionAccion.BitActivo.ToString(),
                    perfilEducacionAccion.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(perfilEducacionAccion.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    perfilEducacionAccion.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorInsertPerfilEducacionAccion);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find PerfilEducacionAccion
            DbCommand com = con.CreateCommand();
            String findPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo");
            com.CommandText = findPerfilEducacionAccion;
            #endregion
            #region Parametros Find PerfilEducacionAccion
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.AccionId);
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionAccion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find PerfilEducacionAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionFind, "", 
                    perfilEducacionAccion.SistemaId.ToString(),
                    perfilEducacionAccion.ModuloId.ToString(),
                    perfilEducacionAccion.OpcionId.ToString(),
                    perfilEducacionAccion.AccionId.ToString(),
                    perfilEducacionAccion.PerfilEducacionId.ToString(),
                    perfilEducacionAccion.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindPerfilEducacionAccion);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete PerfilEducacionAccion
            DbCommand com = con.CreateCommand();
            String deletePerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo");
            com.CommandText = deletePerfilEducacionAccion;
            #endregion
            #region Parametros Delete PerfilEducacionAccion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.AccionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionAccion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete PerfilEducacionAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeletePerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionDelete, "", 
                    perfilEducacionAccion.SistemaId.ToString(),
                    perfilEducacionAccion.ModuloId.ToString(),
                    perfilEducacionAccion.OpcionId.ToString(),
                    perfilEducacionAccion.AccionId.ToString(),
                    perfilEducacionAccion.PerfilEducacionId.ToString(),
                    perfilEducacionAccion.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeletePerfilEducacionAccion);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update PerfilEducacionAccion
            DbCommand com = con.CreateCommand();
            String updatePerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Bit_Autorizacion",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo");
            com.CommandText = updatePerfilEducacionAccion;
            #endregion
            #region Parametros Update PerfilEducacionAccion
            Common.CreateParameter(com, String.Format("{0}Bit_Autorizacion",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacionAccion.BitAutorizacion);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacionAccion.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfilEducacionAccion.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfilEducacionAccion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.AccionId);
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacionAccion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacionAccion.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update PerfilEducacionAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdatePerfilEducacionAccion = String.Format(PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionUpdate, "", 
                    perfilEducacionAccion.BitAutorizacion.ToString(),
                    perfilEducacionAccion.BitActivo.ToString(),
                    perfilEducacionAccion.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(perfilEducacionAccion.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    perfilEducacionAccion.SistemaId.ToString(),
                    perfilEducacionAccion.ModuloId.ToString(),
                    perfilEducacionAccion.OpcionId.ToString(),
                    perfilEducacionAccion.AccionId.ToString(),
                    perfilEducacionAccion.PerfilEducacionId.ToString(),
                    perfilEducacionAccion.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdatePerfilEducacionAccion);
                #endregion
            }
            return resultado;
        }
        protected static PerfilEducacionAccionDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilEducacionAccionDP perfilEducacionAccion = new PerfilEducacionAccionDP();
            perfilEducacionAccion.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            perfilEducacionAccion.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            perfilEducacionAccion.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            perfilEducacionAccion.AccionId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            perfilEducacionAccion.PerfilEducacionId = dr.IsDBNull(4) ? (short)0 : dr.GetInt16(4);
            perfilEducacionAccion.BitAutorizacion = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            perfilEducacionAccion.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            perfilEducacionAccion.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            perfilEducacionAccion.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            perfilEducacionAccion.NiveltrabajoId = dr.IsDBNull(9) ? (Byte)0 : dr.GetByte(9);
            return perfilEducacionAccion;
            #endregion
        }
        public static PerfilEducacionAccionDP Load(DbConnection con, DbTransaction tran, PerfilEducacionAccionPk pk)
        {
            #region SQL Load PerfilEducacionAccion
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo");
            com.CommandText = loadPerfilEducacionAccion;
            #endregion
            #region Parametros Load PerfilEducacionAccion
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.AccionId);
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.NiveltrabajoId);
            #endregion
            PerfilEducacionAccionDP perfilEducacionAccion;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load PerfilEducacionAccion
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        perfilEducacionAccion = ReadRow(dr);
                    }
                    else
                        perfilEducacionAccion = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load PerfilEducacionAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionSelect, "", 
                    pk.SistemaId.ToString(),
                    pk.ModuloId.ToString(),
                    pk.OpcionId.ToString(),
                    pk.AccionId.ToString(),
                    pk.PerfilEducacionId.ToString(),
                    pk.NiveltrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadPerfilEducacionAccion);
                perfilEducacionAccion = null;
                #endregion
            }
            return perfilEducacionAccion;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilEducacionAccion
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionCount,"");
            com.CommandText = countPerfilEducacionAccion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacionAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPerfilEducacionAccion);
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionAccionDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilEducacionAccion
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture, PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionSelectAll, "", ConstantesGlobales.IncluyeRows);
            listPerfilEducacionAccion = listPerfilEducacionAccion.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPerfilEducacionAccion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionAccion
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionAccion
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionAccion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPerfilEducacionAccion = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionResx.PerfilEducacionAccionSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPerfilEducacionAccion);
                #endregion
            }
            return (PerfilEducacionAccionDP[])lista.ToArray(typeof(PerfilEducacionAccionDP));
        }
    }
}
