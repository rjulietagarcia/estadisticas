using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class NiveleducacionbasicaMD
    {
        private NiveleducacionbasicaDP niveleducacionbasica = null;

        public NiveleducacionbasicaMD(NiveleducacionbasicaDP niveleducacionbasica)
        {
            this.niveleducacionbasica = niveleducacionbasica;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Niveleducacionbasica
            DbCommand com = con.CreateCommand();
            String insertNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_NivelEducacion",
                        "Nombre",
                        "Letra_Folio",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertNiveleducacionbasica;
            #endregion
            #region Parametros Insert Niveleducacionbasica
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelEducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, niveleducacionbasica.NiveleducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, niveleducacionbasica.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Letra_Folio",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, niveleducacionbasica.LetraFolio);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, niveleducacionbasica.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, niveleducacionbasica.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(niveleducacionbasica.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Niveleducacionbasica
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertNiveleducacionbasica = String.Format(Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaInsert, "", 
                    niveleducacionbasica.NiveleducacionId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,niveleducacionbasica.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,niveleducacionbasica.LetraFolio),
                    niveleducacionbasica.BitActivo.ToString(),
                    niveleducacionbasica.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(niveleducacionbasica.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertNiveleducacionbasica);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Niveleducacionbasica
            DbCommand com = con.CreateCommand();
            String findNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_NivelEducacion");
            com.CommandText = findNiveleducacionbasica;
            #endregion
            #region Parametros Find Niveleducacionbasica
            Common.CreateParameter(com, String.Format("{0}Id_NivelEducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, niveleducacionbasica.NiveleducacionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Niveleducacionbasica
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaFind, "", 
                    niveleducacionbasica.NiveleducacionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindNiveleducacionbasica);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Niveleducacionbasica
            DbCommand com = con.CreateCommand();
            String deleteNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_NivelEducacion");
            com.CommandText = deleteNiveleducacionbasica;
            #endregion
            #region Parametros Delete Niveleducacionbasica
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelEducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, niveleducacionbasica.NiveleducacionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Niveleducacionbasica
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaDelete, "", 
                    niveleducacionbasica.NiveleducacionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteNiveleducacionbasica);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Niveleducacionbasica
            DbCommand com = con.CreateCommand();
            String updateNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Letra_Folio",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_NivelEducacion");
            com.CommandText = updateNiveleducacionbasica;
            #endregion
            #region Parametros Update Niveleducacionbasica
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, niveleducacionbasica.Nombre);
            Common.CreateParameter(com, String.Format("{0}Letra_Folio",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, niveleducacionbasica.LetraFolio);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, niveleducacionbasica.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, niveleducacionbasica.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(niveleducacionbasica.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_NivelEducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, niveleducacionbasica.NiveleducacionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Niveleducacionbasica
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateNiveleducacionbasica = String.Format(Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,niveleducacionbasica.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,niveleducacionbasica.LetraFolio),
                    niveleducacionbasica.BitActivo.ToString(),
                    niveleducacionbasica.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(niveleducacionbasica.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    niveleducacionbasica.NiveleducacionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateNiveleducacionbasica);
                #endregion
            }
            return resultado;
        }
        protected static NiveleducacionbasicaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            NiveleducacionbasicaDP niveleducacionbasica = new NiveleducacionbasicaDP();
            niveleducacionbasica.NiveleducacionId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            niveleducacionbasica.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            niveleducacionbasica.LetraFolio = dr.IsDBNull(2) ? "" : dr.GetString(2);
            niveleducacionbasica.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            niveleducacionbasica.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            niveleducacionbasica.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            return niveleducacionbasica;
            #endregion
        }
        public static NiveleducacionbasicaDP Load(DbConnection con, DbTransaction tran, NiveleducacionbasicaPk pk)
        {
            #region SQL Load Niveleducacionbasica
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_NivelEducacion");
            com.CommandText = loadNiveleducacionbasica;
            #endregion
            #region Parametros Load Niveleducacionbasica
            Common.CreateParameter(com, String.Format("{0}Id_NivelEducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.NiveleducacionId);
            #endregion
            NiveleducacionbasicaDP niveleducacionbasica;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Niveleducacionbasica
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        niveleducacionbasica = ReadRow(dr);
                    }
                    else
                        niveleducacionbasica = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Niveleducacionbasica
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaSelect, "", 
                    pk.NiveleducacionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadNiveleducacionbasica);
                niveleducacionbasica = null;
                #endregion
            }
            return niveleducacionbasica;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Niveleducacionbasica
            DbCommand com = con.CreateCommand();
            String countNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaCount,"");
            com.CommandText = countNiveleducacionbasica;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Niveleducacionbasica
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountNiveleducacionbasica);
                #endregion
            }
            return resultado;
        }
        public static NiveleducacionbasicaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Niveleducacionbasica
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture, Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listNiveleducacionbasica = listNiveleducacionbasica.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listNiveleducacionbasica, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Niveleducacionbasica
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Niveleducacionbasica
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Niveleducacionbasica
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaNiveleducacionbasica = String.Format(CultureInfo.CurrentCulture,Niveleducacionbasica.NiveleducacionbasicaResx.NiveleducacionbasicaSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaNiveleducacionbasica);
                #endregion
            }
            return (NiveleducacionbasicaDP[])lista.ToArray(typeof(NiveleducacionbasicaDP));
        }
    }
}
