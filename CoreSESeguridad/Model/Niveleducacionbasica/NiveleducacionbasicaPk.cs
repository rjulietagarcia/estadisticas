using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "NiveleducacionbasicaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 04:32:07 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "NiveleducacionbasicaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>NiveleducacionId</term><description>Descripcion NiveleducacionId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "NiveleducacionbasicaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// NiveleducacionbasicaPk niveleducacionbasicaPk = new NiveleducacionbasicaPk(niveleducacionbasica);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("NiveleducacionbasicaPk")]
    public class NiveleducacionbasicaPk
    {
        #region Definicion de campos privados.
        private Byte niveleducacionId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// NiveleducacionId
        /// </summary> 
        [XmlElement("NiveleducacionId")]
        public Byte NiveleducacionId
        {
            get {
                    return niveleducacionId; 
            }
            set {
                    niveleducacionId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de NiveleducacionbasicaPk.
        /// </summary>
        /// <param name="niveleducacionId">Descripción niveleducacionId del tipo Byte.</param>
        public NiveleducacionbasicaPk(Byte niveleducacionId) 
        {
            this.niveleducacionId = niveleducacionId;
        }
        /// <summary>
        /// Constructor normal de NiveleducacionbasicaPk.
        /// </summary>
        public NiveleducacionbasicaPk() 
        {
        }
        #endregion.
    }
}
