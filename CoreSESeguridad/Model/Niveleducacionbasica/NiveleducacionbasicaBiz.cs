using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class NiveleducacionbasicaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, NiveleducacionbasicaDP niveleducacionbasica)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, NiveleducacionbasicaDP niveleducacionbasica)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, niveleducacionbasica);
                        resultado = InternalSave(con, tran, niveleducacionbasica);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla niveleducacionbasica!";
                        throw new BusinessException("Error interno al intentar guardar niveleducacionbasica!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla niveleducacionbasica!";
                throw new BusinessException("Error interno al intentar guardar niveleducacionbasica!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, NiveleducacionbasicaDP niveleducacionbasica)
        {
            NiveleducacionbasicaMD niveleducacionbasicaMd = new NiveleducacionbasicaMD(niveleducacionbasica);
            String resultado = "";
            if (niveleducacionbasicaMd.Find(con, tran))
            {
                NiveleducacionbasicaDP niveleducacionbasicaAnterior = NiveleducacionbasicaMD.Load(con, tran, niveleducacionbasica.Pk);
                if (niveleducacionbasicaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla niveleducacionbasica!";
                    throw new BusinessException("No se pude actualizar la tabla niveleducacionbasica!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (niveleducacionbasicaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla niveleducacionbasica!";
                    throw new BusinessException("No se pude insertar en la tabla niveleducacionbasica!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, NiveleducacionbasicaDP niveleducacionbasica)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, niveleducacionbasica);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla niveleducacionbasica!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla niveleducacionbasica!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla niveleducacionbasica!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla niveleducacionbasica!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, NiveleducacionbasicaDP niveleducacionbasica)
        {
            String resultado = "";
            NiveleducacionbasicaMD niveleducacionbasicaMd = new NiveleducacionbasicaMD(niveleducacionbasica);
            if (niveleducacionbasicaMd.Find(con, tran))
            {
                if (niveleducacionbasicaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla niveleducacionbasica!";
                    throw new BusinessException("No se pude eliminar en la tabla niveleducacionbasica!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla niveleducacionbasica!";
                throw new BusinessException("No se pude eliminar en la tabla niveleducacionbasica!");
            }
            return resultado;
        }

        internal static NiveleducacionbasicaDP LoadDetail(DbConnection con, DbTransaction tran, NiveleducacionbasicaDP niveleducacionbasicaAnterior, NiveleducacionbasicaDP niveleducacionbasica) 
        {
            return niveleducacionbasica;
        }
        public static NiveleducacionbasicaDP Load(DbConnection con, NiveleducacionbasicaPk pk)
        {
            NiveleducacionbasicaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = NiveleducacionbasicaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Niveleducacionbasica!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = NiveleducacionbasicaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Niveleducacionbasica!", ex);
            }
            return resultado;
        }

        public static NiveleducacionbasicaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            NiveleducacionbasicaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = NiveleducacionbasicaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Niveleducacionbasica!", ex);
            }
            return resultado;

        }
    }
}
