using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class PerfilEducacionMD
    {
        private PerfilEducacionDP perfilEducacion = null;

        public PerfilEducacionMD(PerfilEducacionDP perfilEducacion)
        {
            this.perfilEducacion = perfilEducacion;
        }

        public Int16 Identity(DbConnection con, DbTransaction tran)
        {
            Int16 resultado = 0;
            #region SQL Count PerfilEducacion
            DbCommand com = con.CreateCommand();
            String countPerfilEducacion = String.Format(CultureInfo.CurrentCulture, PerfilEducacion.PerfilEducacionResx.PerfilEducacionIdentity, "");
            com.CommandText = countPerfilEducacion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt16(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert PerfilEducacion
            DbCommand com = con.CreateCommand();
            String insertPerfilEducacion = String.Format(CultureInfo.CurrentCulture,PerfilEducacion.PerfilEducacionResx.PerfilEducacionInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_NivelTrabajo");
            com.CommandText = insertPerfilEducacion;
            #endregion

            if (perfilEducacion.PerfilEducacionId == 0) {
                perfilEducacion.PerfilEducacionId = this.Identity(con, tran);
            }

            #region Parametros Insert PerfilEducacion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, perfilEducacion.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, perfilEducacion.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacion.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfilEducacion.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfilEducacion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacion.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find PerfilEducacion
            DbCommand com = con.CreateCommand();
            String findPerfilEducacion = String.Format(CultureInfo.CurrentCulture,PerfilEducacion.PerfilEducacionResx.PerfilEducacionFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo");
            com.CommandText = findPerfilEducacion;
            #endregion
            #region Parametros Find PerfilEducacion
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacion.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete PerfilEducacion
            DbCommand com = con.CreateCommand();
            String deletePerfilEducacion = String.Format(CultureInfo.CurrentCulture,PerfilEducacion.PerfilEducacionResx.PerfilEducacionDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo");
            com.CommandText = deletePerfilEducacion;
            #endregion
            #region Parametros Delete PerfilEducacion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacion.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update PerfilEducacion
            DbCommand com = con.CreateCommand();
            String updatePerfilEducacion = String.Format(CultureInfo.CurrentCulture,PerfilEducacion.PerfilEducacionResx.PerfilEducacionUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo");
            com.CommandText = updatePerfilEducacion;
            #endregion
            #region Parametros Update PerfilEducacion
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, perfilEducacion.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, perfilEducacion.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfilEducacion.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfilEducacion.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfilEducacion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, perfilEducacion.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, perfilEducacion.NiveltrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static PerfilEducacionDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilEducacionDP perfilEducacion = new PerfilEducacionDP();
            perfilEducacion.PerfilEducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            perfilEducacion.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            perfilEducacion.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            perfilEducacion.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            perfilEducacion.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            perfilEducacion.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            perfilEducacion.NiveltrabajoId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            return perfilEducacion;
            #endregion
        }
        public static PerfilEducacionDP Load(DbConnection con, DbTransaction tran, PerfilEducacionPk pk)
        {
            #region SQL Load PerfilEducacion
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPerfilEducacion = String.Format(CultureInfo.CurrentCulture,PerfilEducacion.PerfilEducacionResx.PerfilEducacionSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil_Educacion",
                        "Id_NivelTrabajo");
            com.CommandText = loadPerfilEducacion;
            #endregion
            #region Parametros Load PerfilEducacion
            Common.CreateParameter(com, String.Format("{0}Id_Perfil_Educacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PerfilEducacionId);
            Common.CreateParameter(com, String.Format("{0}Id_NivelTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.NiveltrabajoId);
            #endregion
            PerfilEducacionDP perfilEducacion;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load PerfilEducacion
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        perfilEducacion = ReadRow(dr);
                    }
                    else
                        perfilEducacion = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return perfilEducacion;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilEducacion
            DbCommand com = con.CreateCommand();
            String countPerfilEducacion = String.Format(CultureInfo.CurrentCulture,PerfilEducacion.PerfilEducacionResx.PerfilEducacionCount,"");
            com.CommandText = countPerfilEducacion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilEducacion
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacion = String.Format(CultureInfo.CurrentCulture, PerfilEducacion.PerfilEducacionResx.PerfilEducacionSelectAll, "", ConstantesGlobales.IncluyeRows);
            listPerfilEducacion = listPerfilEducacion.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPerfilEducacion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacion
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacion
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PerfilEducacionDP[])lista.ToArray(typeof(PerfilEducacionDP));
        }

        public Boolean CopiaPerfil(DbConnection con, DbTransaction tran, Byte aNivelTrabajoOrigen, Int32 aPerfilEducacionOrigen, Byte aNivelTrabajoDestino, Int32 aPerfilEducacionDestino)
        {
            Boolean resultado = false;
            #region SQL Copia Perfil
            DbCommand com = con.CreateCommand();
            String copiaPerfilEducacion = String.Format(CultureInfo.CurrentCulture, String.Format(CultureInfo.CurrentCulture,PerfilEducacion.PerfilEducacionResx.PerfilEducacionCopia,
                        ConstantesGlobales.ParameterPrefix,
                        "NivelTrabajoOrigen",
                        "PerfilEducacionOrigen",
                        "NivelTrabajoDestino",
                        "PerfilEducacionDestino"));
            com.CommandText = copiaPerfilEducacion;
            #endregion
            #region Parametros Insert PerfilEducacion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}NivelTrabajoOrigen",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aNivelTrabajoOrigen);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}PerfilEducacionOrigen",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aPerfilEducacionOrigen);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}NivelTrabajoDestino",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aNivelTrabajoDestino);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}PerfilEducacionDestino",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aPerfilEducacionDestino);
            #endregion

            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                com.ExecuteScalar();
                resultado = true;
                // resultado = Convert.ToInt16();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
    }
}
