﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.225
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mx.Gob.Nl.Educacion.Model.PerfilEducacion {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso con establecimiento inflexible de tipos, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o vuelva a generar su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class PerfilEducacionResx {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PerfilEducacionResx() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Mx.Gob.Nl.Educacion.Model.PerfilEducacion.PerfilEducacionResx", typeof(PerfilEducacionResx).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso con establecimiento inflexible de tipos.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Exec sp_gp_CopiaPerfil {0}{1},{0}{2}, {0}{3},{0}{4}.
        /// </summary>
        internal static string PerfilEducacionCopia {
            get {
                return ResourceManager.GetString("PerfilEducacionCopia", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Select Count(*) as cnt From dbo.Perfil_Educacion.
        /// </summary>
        internal static string PerfilEducacionCount {
            get {
                return ResourceManager.GetString("PerfilEducacionCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Delete From dbo.Perfil_Educacion
        ///Where   Id_Perfil_Educacion = {0}{1} 
        ///    AND  Id_NivelTrabajo = {0}{2} 
        ///.
        /// </summary>
        internal static string PerfilEducacionDelete {
            get {
                return ResourceManager.GetString("PerfilEducacionDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Select Count(*) as cnt
        ///   From dbo.Perfil_Educacion
        ///   Where   Id_Perfil_Educacion = {0}{1} 
        ///    AND  Id_NivelTrabajo = {0}{2} 
        ///.
        /// </summary>
        internal static string PerfilEducacionFind {
            get {
                return ResourceManager.GetString("PerfilEducacionFind", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Select Max(IsNull(Id_Perfil_Educacion,0))+ 1 as sig From dbo.Perfil_Educacion.
        /// </summary>
        internal static string PerfilEducacionIdentity {
            get {
                return ResourceManager.GetString("PerfilEducacionIdentity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Insert into dbo.Perfil_Educacion
        ///  (     Id_Perfil_Educacion,
        ///     Nombre,
        ///     Abreviatura,
        ///     Bit_Activo,
        ///     Id_Usuario,
        ///     Fecha_Actualizacion,
        ///     Id_NivelTrabajo
        ///)
        ///Values
        ///  (     {0}{1},
        ///     {0}{2},
        ///     {0}{3},
        ///     {0}{4},
        ///     {0}{5},
        ///     {0}{6},
        ///     {0}{7}
        ///).
        /// </summary>
        internal static string PerfilEducacionInsert {
            get {
                return ResourceManager.GetString("PerfilEducacionInsert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Select 
        ///     Id_Perfil_Educacion,
        ///     Nombre,
        ///     Abreviatura,
        ///     Bit_Activo,
        ///     Id_Usuario,
        ///     Fecha_Actualizacion,
        ///     Id_NivelTrabajo
        ///
        ///   From dbo.Perfil_Educacion
        ///   Where   Id_Perfil_Educacion = {0}{1} 
        ///    AND  Id_NivelTrabajo = {0}{2} 
        ///.
        /// </summary>
        internal static string PerfilEducacionSelect {
            get {
                return ResourceManager.GetString("PerfilEducacionSelect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Select 
        ///     Id_Perfil_Educacion,
        ///     Nombre,
        ///     Abreviatura,
        ///     Bit_Activo,
        ///     Id_Usuario,
        ///     Fecha_Actualizacion,
        ///     Id_NivelTrabajo
        ///
        ///   {1} From dbo.Perfil_Educacion
        ///    .
        /// </summary>
        internal static string PerfilEducacionSelectAll {
            get {
                return ResourceManager.GetString("PerfilEducacionSelectAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Update dbo.Perfil_Educacion
        ///    Set      Nombre = {0}{1}, 
        ///     Abreviatura = {0}{2}, 
        ///     Bit_Activo = {0}{3}, 
        ///     Id_Usuario = {0}{4}, 
        ///     Fecha_Actualizacion = {0}{5}
        ///
        ///    Where   Id_Perfil_Educacion = {0}{6} 
        ///    AND  Id_NivelTrabajo = {0}{7} 
        ///.
        /// </summary>
        internal static string PerfilEducacionUpdate {
            get {
                return ResourceManager.GetString("PerfilEducacionUpdate", resourceCulture);
            }
        }
    }
}
