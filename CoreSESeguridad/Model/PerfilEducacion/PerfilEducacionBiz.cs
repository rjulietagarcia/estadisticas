using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class PerfilEducacionBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, PerfilEducacionDP perfilEducacion)
        {
            #region SaveDetail
            #region FK Niveltrabajo
            NiveltrabajoDP niveltrabajo = perfilEducacion.Niveltrabajo;
            if (niveltrabajo != null) 
            {
                NiveltrabajoMD niveltrabajoMd = new NiveltrabajoMD(niveltrabajo);
                if (!niveltrabajoMd.Find(con, tran))
                    niveltrabajoMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, PerfilEducacionDP perfilEducacion)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, perfilEducacion);
                        resultado = InternalSave(con, tran, perfilEducacion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla perfilEducacion!";
                        throw new BusinessException("Error interno al intentar guardar perfilEducacion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla perfilEducacion!";
                throw new BusinessException("Error interno al intentar guardar perfilEducacion!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, PerfilEducacionDP perfilEducacion)
        {
            PerfilEducacionMD perfilEducacionMd = new PerfilEducacionMD(perfilEducacion);
            String resultado = "";
            if (perfilEducacionMd.Find(con, tran))
            {
                PerfilEducacionDP perfilEducacionAnterior = PerfilEducacionMD.Load(con, tran, perfilEducacion.Pk);
                if (perfilEducacionMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla perfilEducacion!";
                    throw new BusinessException("No se pude actualizar la tabla perfilEducacion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (perfilEducacionMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla perfilEducacion!";
                    throw new BusinessException("No se pude insertar en la tabla perfilEducacion!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, PerfilEducacionDP perfilEducacion)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, perfilEducacion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla perfilEducacion!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla perfilEducacion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla perfilEducacion!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla perfilEducacion!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, PerfilEducacionDP perfilEducacion)
        {
            String resultado = "";
            PerfilEducacionMD perfilEducacionMd = new PerfilEducacionMD(perfilEducacion);
            if (perfilEducacionMd.Find(con, tran))
            {
                if (perfilEducacionMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla perfilEducacion!";
                    throw new BusinessException("No se pude eliminar en la tabla perfilEducacion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla perfilEducacion!";
                throw new BusinessException("No se pude eliminar en la tabla perfilEducacion!");
            }
            return resultado;
        }

        internal static PerfilEducacionDP LoadDetail(DbConnection con, DbTransaction tran, PerfilEducacionDP perfilEducacionAnterior, PerfilEducacionDP perfilEducacion) 
        {
            #region FK Niveltrabajo
            NiveltrabajoPk niveltrabajoPk = new NiveltrabajoPk();
            if (perfilEducacionAnterior != null)
            {
                if (niveltrabajoPk.Equals(perfilEducacionAnterior.Pk))
                    perfilEducacion.Niveltrabajo = perfilEducacionAnterior.Niveltrabajo;
                else
                    perfilEducacion.Niveltrabajo = NiveltrabajoMD.Load(con, tran, niveltrabajoPk);
            }
            else
                perfilEducacion.Niveltrabajo = NiveltrabajoMD.Load(con, tran, niveltrabajoPk);
            #endregion
            return perfilEducacion;
        }
        public static PerfilEducacionDP Load(DbConnection con, PerfilEducacionPk pk)
        {
            PerfilEducacionDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un PerfilEducacion!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilEducacionMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilEducacion!", ex);
            }
            return resultado;
        }

        public static PerfilEducacionDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PerfilEducacionDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilEducacion!", ex);
            }
            return resultado;

        }

        public static String CopiaPerfil(DbConnection con,
            Byte aNivelTrabajoOrigen, Int32 aPerfilEducacionOrigen, Byte aNivelTrabajoDestino, Int32 aPerfilEducacionDestino)
        {
            #region Copia
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        PerfilEducacionMD perfilEducacionMd = new PerfilEducacionMD(null);
                        resultado = perfilEducacionMd.CopiaPerfil(con, tran, aNivelTrabajoOrigen, aPerfilEducacionOrigen, aNivelTrabajoDestino, aPerfilEducacionDestino) ? "1" : "0";
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al copiar un perfilEducacion!";
                        throw new BusinessException("Error interno al copiar un perfilEducacion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al copiar un perfilEducacion!";
                throw new BusinessException("Error interno al copiar un perfilEducacion!", ex);
            }
            return resultado;
            #endregion
        }

    }
}
