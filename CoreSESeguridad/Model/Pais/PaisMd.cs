using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class PaisMD
    {
        private PaisDP pais = null;

        public PaisMD(PaisDP pais)
        {
            this.pais = pais;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Pais
            DbCommand com = con.CreateCommand();
            String insertPais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Nombre",
                        "Nacionalidad",
                        "Abreviatura",
                        "Id_RegionContinente",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertPais;
            #endregion
            #region Parametros Insert Pais
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pais.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, pais.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nacionalidad",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, pais.Nacionalidad);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 3, ParameterDirection.Input, pais.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_RegionContinente",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pais.RegioncontinenteId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, pais.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pais.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(pais.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Pais
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertPais = String.Format(Pais.PaisResx.PaisInsert, "", 
                    pais.PaisId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,pais.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,pais.Nacionalidad),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,pais.Abreviatura),
                    pais.RegioncontinenteId.ToString(),
                    pais.BitActivo.ToString(),
                    pais.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(pais.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertPais);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Pais
            DbCommand com = con.CreateCommand();
            String findPais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais");
            com.CommandText = findPais;
            #endregion
            #region Parametros Find Pais
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pais.PaisId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Pais
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindPais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisFind, "", 
                    pais.PaisId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindPais);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Pais
            DbCommand com = con.CreateCommand();
            String deletePais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais");
            com.CommandText = deletePais;
            #endregion
            #region Parametros Delete Pais
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pais.PaisId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Pais
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeletePais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisDelete, "", 
                    pais.PaisId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeletePais);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Pais
            DbCommand com = con.CreateCommand();
            String updatePais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Nacionalidad",
                        "Abreviatura",
                        "Id_RegionContinente",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Pais");
            com.CommandText = updatePais;
            #endregion
            #region Parametros Update Pais
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, pais.Nombre);
            Common.CreateParameter(com, String.Format("{0}Nacionalidad",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, pais.Nacionalidad);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 3, ParameterDirection.Input, pais.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Id_RegionContinente",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pais.RegioncontinenteId);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, pais.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pais.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(pais.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pais.PaisId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Pais
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdatePais = String.Format(Pais.PaisResx.PaisUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,pais.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,pais.Nacionalidad),
                    String.Format(ConstantesGlobales.ConvierteString,pais.Abreviatura),
                    pais.RegioncontinenteId.ToString(),
                    pais.BitActivo.ToString(),
                    pais.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(pais.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    pais.PaisId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdatePais);
                #endregion
            }
            return resultado;
        }
        protected static PaisDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PaisDP pais = new PaisDP();
            pais.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            pais.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            pais.Nacionalidad = dr.IsDBNull(2) ? "" : dr.GetString(2);
            pais.Abreviatura = dr.IsDBNull(3) ? "" : dr.GetString(3);
            pais.RegioncontinenteId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            pais.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            pais.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            pais.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            return pais;
            #endregion
        }
        public static PaisDP Load(DbConnection con, DbTransaction tran, PaisPk pk)
        {
            #region SQL Load Pais
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais");
            com.CommandText = loadPais;
            #endregion
            #region Parametros Load Pais
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PaisId);
            #endregion
            PaisDP pais;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Pais
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        pais = ReadRow(dr);
                    }
                    else
                        pais = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Pais
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadPais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisSelect, "", 
                    pk.PaisId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadPais);
                pais = null;
                #endregion
            }
            return pais;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Pais
            DbCommand com = con.CreateCommand();
            String countPais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisCount,"");
            com.CommandText = countPais;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Pais
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPais);
                #endregion
            }
            return resultado;
        }
        public static PaisDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Pais
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPais = String.Format(CultureInfo.CurrentCulture, Pais.PaisResx.PaisSelectAll, "", ConstantesGlobales.IncluyeRows);
            listPais = listPais.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPais, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Pais
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Pais
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Pais
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPais = String.Format(CultureInfo.CurrentCulture,Pais.PaisResx.PaisSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPais);
                #endregion
            }
            return (PaisDP[])lista.ToArray(typeof(PaisDP));
        }
    }
}
