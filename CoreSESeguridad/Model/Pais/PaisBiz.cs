using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class PaisBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, PaisDP pais)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, PaisDP pais)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, pais);
                        resultado = InternalSave(con, tran, pais);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla pais!";
                        throw new BusinessException("Error interno al intentar guardar pais!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla pais!";
                throw new BusinessException("Error interno al intentar guardar pais!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, PaisDP pais)
        {
            PaisMD paisMd = new PaisMD(pais);
            String resultado = "";
            if (paisMd.Find(con, tran))
            {
                PaisDP paisAnterior = PaisMD.Load(con, tran, pais.Pk);
                if (paisMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla pais!";
                    throw new BusinessException("No se pude actualizar la tabla pais!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (paisMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla pais!";
                    throw new BusinessException("No se pude insertar en la tabla pais!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, PaisDP pais)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, pais);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla pais!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla pais!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla pais!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla pais!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, PaisDP pais)
        {
            String resultado = "";
            PaisMD paisMd = new PaisMD(pais);
            if (paisMd.Find(con, tran))
            {
                if (paisMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla pais!";
                    throw new BusinessException("No se pude eliminar en la tabla pais!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla pais!";
                throw new BusinessException("No se pude eliminar en la tabla pais!");
            }
            return resultado;
        }

        internal static PaisDP LoadDetail(DbConnection con, DbTransaction tran, PaisDP paisAnterior, PaisDP pais) 
        {
            return pais;
        }
        public static PaisDP Load(DbConnection con, PaisPk pk)
        {
            PaisDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PaisMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Pais!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PaisMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Pais!", ex);
            }
            return resultado;
        }

        public static PaisDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PaisDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PaisMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Pais!", ex);
            }
            return resultado;

        }
    }
}
