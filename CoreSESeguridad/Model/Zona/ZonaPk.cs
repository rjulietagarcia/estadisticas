using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "ZonaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 04:32:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "ZonaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>ZonaId</term><description>Descripcion ZonaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "ZonaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// ZonaPk zonaPk = new ZonaPk(zona);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("ZonaPk")]
    public class ZonaPk           
    {  


        #region Definicion de campos privados.
        private int zonaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// ZonaId
        /// </summary> 
        [XmlElement("ZonaId")]
        public int ZonaId
        {
            get {
                    return zonaId; 
            }
            set {
                    zonaId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de ZonaPk.
        /// </summary>
        /// <param name="zonaId">Descripción zonaId del tipo Int16.</param>
        public ZonaPk(int zonaId) 
        {
            this.zonaId = zonaId;
        }
        /// <summary>
        /// Constructor normal de ZonaPk.
        /// </summary>
        public ZonaPk() 
        {
        }
        #endregion.
    }
}
