using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class ZonaMD
    {
        private ZonaDP zona = null;

        public ZonaMD(ZonaDP zona)
        {
            this.zona = zona;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Zona
            DbCommand com = con.CreateCommand();
            String insertZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "id_zona",
                        "id_tipoeducacion",
                        "id_nivel",
                        "numerozona",
                        "nombre",
                        "clave",
                        "id_sostenimiento",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion");
            com.CommandText = insertZona;
            #endregion
            #region Parametros Insert Zona
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.ZonaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.NivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}numerozona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.Numerozona);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, zona.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, zona.Clave);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, zona.SostenimientoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, zona.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, zona.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(zona.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Zona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertZona = String.Format(Zona.ZonaResx.ZonaInsert, "", 
                    zona.ZonaId.ToString(),
                    zona.TipoeducacionId.ToString(),
                    zona.NivelId.ToString(),
                    zona.Numerozona.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,zona.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,zona.Clave),
                    zona.SostenimientoId.ToString(),
                    zona.BitActivo.ToString(),
                    zona.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(zona.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertZona);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Zona
            DbCommand com = con.CreateCommand();
            String findZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "id_zona");
            com.CommandText = findZona;
            #endregion
            #region Parametros Find Zona
            Common.CreateParameter(com, String.Format("{0}id_zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.ZonaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Zona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaFind, "", 
                    zona.ZonaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindZona);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Zona
            DbCommand com = con.CreateCommand();
            String deleteZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "id_zona");
            com.CommandText = deleteZona;
            #endregion
            #region Parametros Delete Zona
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.ZonaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Zona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaDelete, "", 
                    zona.ZonaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteZona);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Zona
            DbCommand com = con.CreateCommand();
            String updateZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel",
                        "numerozona",
                        "nombre",
                        "clave",
                        "id_sostenimiento",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion",
                        "id_zona");
            com.CommandText = updateZona;
            #endregion
            #region Parametros Update Zona
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.NivelId);
            Common.CreateParameter(com, String.Format("{0}numerozona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.Numerozona);
            Common.CreateParameter(com, String.Format("{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, zona.Nombre);
            Common.CreateParameter(com, String.Format("{0}clave",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, zona.Clave);
            Common.CreateParameter(com, String.Format("{0}id_sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, zona.SostenimientoId);
            Common.CreateParameter(com, String.Format("{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, zona.BitActivo);
            Common.CreateParameter(com, String.Format("{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, zona.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(zona.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}id_zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, zona.ZonaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Zona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateZona = String.Format(Zona.ZonaResx.ZonaUpdate, "", 
                    zona.TipoeducacionId.ToString(),
                    zona.NivelId.ToString(),
                    zona.Numerozona.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,zona.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,zona.Clave),
                    zona.SostenimientoId.ToString(),
                    zona.BitActivo.ToString(),
                    zona.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(zona.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    zona.ZonaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateZona);
                #endregion
            }
            return resultado;
        }
        protected static ZonaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ZonaDP zona = new ZonaDP();
            zona.ZonaId = dr.IsDBNull(0) ? (short)0 :Int16.Parse( dr.GetValue(0).ToString());
            zona.TipoeducacionId = dr.IsDBNull(1) ? (short)0 : Int16.Parse(dr.GetValue(1).ToString());
            zona.NivelId = dr.IsDBNull(2) ? (short)0 : Int16.Parse(dr.GetValue(2).ToString());
            zona.Numerozona = dr.IsDBNull(3) ? (short)0 : Int16.Parse(dr.GetValue(3).ToString());
            zona.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            zona.Clave = dr.IsDBNull(5) ? "" : dr.GetString(5);
            zona.SostenimientoId = dr.IsDBNull(6) ? (Byte)0 :byte.Parse(dr.GetValue(6).ToString());
            zona.BitActivo = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            zona.UsuarioId = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            zona.FechaActualizacion = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            return zona;
            #endregion
        }
        public static ZonaDP Load(DbConnection con, DbTransaction tran, ZonaPk pk)
        {
            #region SQL Load Zona
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "id_zona");
            com.CommandText = loadZona;
            #endregion
            #region Parametros Load Zona
            Common.CreateParameter(com, String.Format("{0}id_zona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.ZonaId);
            #endregion
            ZonaDP zona;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Zona
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        zona = ReadRow(dr);
                    }
                    else
                        zona = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Zona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaSelect, "", 
                    pk.ZonaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadZona);
                zona = null;
                #endregion
            }
            return zona;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Zona
            DbCommand com = con.CreateCommand();
            String countZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaCount,"");
            com.CommandText = countZona;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Zona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountZona);
                #endregion
            }
            return resultado;
        }
        public static ZonaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Zona
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listZona = String.Format(CultureInfo.CurrentCulture, Zona.ZonaResx.ZonaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listZona = listZona.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listZona, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Zona
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Zona
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Zona
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaZona = String.Format(CultureInfo.CurrentCulture,Zona.ZonaResx.ZonaSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaZona);
                #endregion
            }
            return (ZonaDP[])lista.ToArray(typeof(ZonaDP));
        }
    }
}
