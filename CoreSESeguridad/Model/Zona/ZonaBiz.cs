using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class ZonaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, ZonaDP zona)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, ZonaDP zona)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, zona);
                        resultado = InternalSave(con, tran, zona);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla zona!";
                        throw new BusinessException("Error interno al intentar guardar zona!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla zona!";
                throw new BusinessException("Error interno al intentar guardar zona!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, ZonaDP zona)
        {
            ZonaMD zonaMd = new ZonaMD(zona);
            String resultado = "";
            if (zonaMd.Find(con, tran))
            {
                ZonaDP zonaAnterior = ZonaMD.Load(con, tran, zona.Pk);
                if (zonaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla zona!";
                    throw new BusinessException("No se pude actualizar la tabla zona!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (zonaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla zona!";
                    throw new BusinessException("No se pude insertar en la tabla zona!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, ZonaDP zona)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, zona);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla zona!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla zona!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla zona!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla zona!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, ZonaDP zona)
        {
            String resultado = "";
            ZonaMD zonaMd = new ZonaMD(zona);
            if (zonaMd.Find(con, tran))
            {
                if (zonaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla zona!";
                    throw new BusinessException("No se pude eliminar en la tabla zona!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla zona!";
                throw new BusinessException("No se pude eliminar en la tabla zona!");
            }
            return resultado;
        }

        internal static ZonaDP LoadDetail(DbConnection con, DbTransaction tran, ZonaDP zonaAnterior, ZonaDP zona) 
        {
            return zona;
        }
        public static ZonaDP Load(DbConnection con, ZonaPk pk)
        {
            ZonaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ZonaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Zona!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ZonaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Zona!", ex);
            }
            return resultado;
        }

        public static ZonaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            ZonaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ZonaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Zona!", ex);
            }
            return resultado;

        }
    }
}
