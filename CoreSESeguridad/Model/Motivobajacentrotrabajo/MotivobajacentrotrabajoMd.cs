using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class MotivobajacentrotrabajoMD
    {
        private MotivobajacentrotrabajoDP motivobajacentrotrabajo = null;

        public MotivobajacentrotrabajoMD(MotivobajacentrotrabajoDP motivobajacentrotrabajo)
        {
            this.motivobajacentrotrabajo = motivobajacentrotrabajo;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Motivobajacentrotrabajo
            DbCommand com = con.CreateCommand();
            String insertMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_MotivoBajaCentroTrabajo",
                        "Nombre",
                        "Abrevitura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertMotivobajacentrotrabajo;
            #endregion
            #region Parametros Insert Motivobajacentrotrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_MotivoBajaCentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, motivobajacentrotrabajo.MotivobajacentrotrabajoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, motivobajacentrotrabajo.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abrevitura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, motivobajacentrotrabajo.Abrevitura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, motivobajacentrotrabajo.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, motivobajacentrotrabajo.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(motivobajacentrotrabajo.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Motivobajacentrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertMotivobajacentrotrabajo = String.Format(Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoInsert, "", 
                    motivobajacentrotrabajo.MotivobajacentrotrabajoId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,motivobajacentrotrabajo.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,motivobajacentrotrabajo.Abrevitura),
                    motivobajacentrotrabajo.BitActivo.ToString(),
                    motivobajacentrotrabajo.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(motivobajacentrotrabajo.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertMotivobajacentrotrabajo);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Motivobajacentrotrabajo
            DbCommand com = con.CreateCommand();
            String findMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_MotivoBajaCentroTrabajo");
            com.CommandText = findMotivobajacentrotrabajo;
            #endregion
            #region Parametros Find Motivobajacentrotrabajo
            Common.CreateParameter(com, String.Format("{0}Id_MotivoBajaCentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, motivobajacentrotrabajo.MotivobajacentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Motivobajacentrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoFind, "", 
                    motivobajacentrotrabajo.MotivobajacentrotrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindMotivobajacentrotrabajo);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Motivobajacentrotrabajo
            DbCommand com = con.CreateCommand();
            String deleteMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_MotivoBajaCentroTrabajo");
            com.CommandText = deleteMotivobajacentrotrabajo;
            #endregion
            #region Parametros Delete Motivobajacentrotrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_MotivoBajaCentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, motivobajacentrotrabajo.MotivobajacentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Motivobajacentrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoDelete, "", 
                    motivobajacentrotrabajo.MotivobajacentrotrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteMotivobajacentrotrabajo);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Motivobajacentrotrabajo
            DbCommand com = con.CreateCommand();
            String updateMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abrevitura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_MotivoBajaCentroTrabajo");
            com.CommandText = updateMotivobajacentrotrabajo;
            #endregion
            #region Parametros Update Motivobajacentrotrabajo
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, motivobajacentrotrabajo.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abrevitura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, motivobajacentrotrabajo.Abrevitura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, motivobajacentrotrabajo.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, motivobajacentrotrabajo.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(motivobajacentrotrabajo.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_MotivoBajaCentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, motivobajacentrotrabajo.MotivobajacentrotrabajoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Motivobajacentrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateMotivobajacentrotrabajo = String.Format(Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,motivobajacentrotrabajo.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,motivobajacentrotrabajo.Abrevitura),
                    motivobajacentrotrabajo.BitActivo.ToString(),
                    motivobajacentrotrabajo.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(motivobajacentrotrabajo.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    motivobajacentrotrabajo.MotivobajacentrotrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateMotivobajacentrotrabajo);
                #endregion
            }
            return resultado;
        }
        protected static MotivobajacentrotrabajoDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            MotivobajacentrotrabajoDP motivobajacentrotrabajo = new MotivobajacentrotrabajoDP();
            motivobajacentrotrabajo.MotivobajacentrotrabajoId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            motivobajacentrotrabajo.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            motivobajacentrotrabajo.Abrevitura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            motivobajacentrotrabajo.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            motivobajacentrotrabajo.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            motivobajacentrotrabajo.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            return motivobajacentrotrabajo;
            #endregion
        }
        public static MotivobajacentrotrabajoDP Load(DbConnection con, DbTransaction tran, MotivobajacentrotrabajoPk pk)
        {
            #region SQL Load Motivobajacentrotrabajo
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_MotivoBajaCentroTrabajo");
            com.CommandText = loadMotivobajacentrotrabajo;
            #endregion
            #region Parametros Load Motivobajacentrotrabajo
            Common.CreateParameter(com, String.Format("{0}Id_MotivoBajaCentroTrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.MotivobajacentrotrabajoId);
            #endregion
            MotivobajacentrotrabajoDP motivobajacentrotrabajo;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Motivobajacentrotrabajo
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        motivobajacentrotrabajo = ReadRow(dr);
                    }
                    else
                        motivobajacentrotrabajo = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Motivobajacentrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoSelect, "", 
                    pk.MotivobajacentrotrabajoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadMotivobajacentrotrabajo);
                motivobajacentrotrabajo = null;
                #endregion
            }
            return motivobajacentrotrabajo;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Motivobajacentrotrabajo
            DbCommand com = con.CreateCommand();
            String countMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoCount,"");
            com.CommandText = countMotivobajacentrotrabajo;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Motivobajacentrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountMotivobajacentrotrabajo);
                #endregion
            }
            return resultado;
        }
        public static MotivobajacentrotrabajoDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Motivobajacentrotrabajo
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture, Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoSelectAll, "", ConstantesGlobales.IncluyeRows);
            listMotivobajacentrotrabajo = listMotivobajacentrotrabajo.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listMotivobajacentrotrabajo, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Motivobajacentrotrabajo
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Motivobajacentrotrabajo
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Motivobajacentrotrabajo
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaMotivobajacentrotrabajo = String.Format(CultureInfo.CurrentCulture,Motivobajacentrotrabajo.MotivobajacentrotrabajoResx.MotivobajacentrotrabajoSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaMotivobajacentrotrabajo);
                #endregion
            }
            return (MotivobajacentrotrabajoDP[])lista.ToArray(typeof(MotivobajacentrotrabajoDP));
        }
    }
}
