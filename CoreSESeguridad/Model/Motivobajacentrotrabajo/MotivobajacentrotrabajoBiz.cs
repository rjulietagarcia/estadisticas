using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class MotivobajacentrotrabajoBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, MotivobajacentrotrabajoDP motivobajacentrotrabajo)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, MotivobajacentrotrabajoDP motivobajacentrotrabajo)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, motivobajacentrotrabajo);
                        resultado = InternalSave(con, tran, motivobajacentrotrabajo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla motivobajacentrotrabajo!";
                        throw new BusinessException("Error interno al intentar guardar motivobajacentrotrabajo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla motivobajacentrotrabajo!";
                throw new BusinessException("Error interno al intentar guardar motivobajacentrotrabajo!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, MotivobajacentrotrabajoDP motivobajacentrotrabajo)
        {
            MotivobajacentrotrabajoMD motivobajacentrotrabajoMd = new MotivobajacentrotrabajoMD(motivobajacentrotrabajo);
            String resultado = "";
            if (motivobajacentrotrabajoMd.Find(con, tran))
            {
                MotivobajacentrotrabajoDP motivobajacentrotrabajoAnterior = MotivobajacentrotrabajoMD.Load(con, tran, motivobajacentrotrabajo.Pk);
                if (motivobajacentrotrabajoMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla motivobajacentrotrabajo!";
                    throw new BusinessException("No se pude actualizar la tabla motivobajacentrotrabajo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (motivobajacentrotrabajoMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla motivobajacentrotrabajo!";
                    throw new BusinessException("No se pude insertar en la tabla motivobajacentrotrabajo!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, MotivobajacentrotrabajoDP motivobajacentrotrabajo)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, motivobajacentrotrabajo);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla motivobajacentrotrabajo!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla motivobajacentrotrabajo!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla motivobajacentrotrabajo!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla motivobajacentrotrabajo!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, MotivobajacentrotrabajoDP motivobajacentrotrabajo)
        {
            String resultado = "";
            MotivobajacentrotrabajoMD motivobajacentrotrabajoMd = new MotivobajacentrotrabajoMD(motivobajacentrotrabajo);
            if (motivobajacentrotrabajoMd.Find(con, tran))
            {
                if (motivobajacentrotrabajoMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla motivobajacentrotrabajo!";
                    throw new BusinessException("No se pude eliminar en la tabla motivobajacentrotrabajo!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla motivobajacentrotrabajo!";
                throw new BusinessException("No se pude eliminar en la tabla motivobajacentrotrabajo!");
            }
            return resultado;
        }

        internal static MotivobajacentrotrabajoDP LoadDetail(DbConnection con, DbTransaction tran, MotivobajacentrotrabajoDP motivobajacentrotrabajoAnterior, MotivobajacentrotrabajoDP motivobajacentrotrabajo) 
        {
            return motivobajacentrotrabajo;
        }
        public static MotivobajacentrotrabajoDP Load(DbConnection con, MotivobajacentrotrabajoPk pk)
        {
            MotivobajacentrotrabajoDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MotivobajacentrotrabajoMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Motivobajacentrotrabajo!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = MotivobajacentrotrabajoMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Motivobajacentrotrabajo!", ex);
            }
            return resultado;
        }

        public static MotivobajacentrotrabajoDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            MotivobajacentrotrabajoDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MotivobajacentrotrabajoMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Motivobajacentrotrabajo!", ex);
            }
            return resultado;

        }
    }
}
