using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "MotivobajacentrotrabajoPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 04:32:07 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "MotivobajacentrotrabajoPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>MotivobajacentrotrabajoId</term><description>Descripcion MotivobajacentrotrabajoId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "MotivobajacentrotrabajoPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// MotivobajacentrotrabajoPk motivobajacentrotrabajoPk = new MotivobajacentrotrabajoPk(motivobajacentrotrabajo);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("MotivobajacentrotrabajoPk")]
    public class MotivobajacentrotrabajoPk
    {
        #region Definicion de campos privados.
        private Int16 motivobajacentrotrabajoId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// MotivobajacentrotrabajoId
        /// </summary> 
        [XmlElement("MotivobajacentrotrabajoId")]
        public Int16 MotivobajacentrotrabajoId
        {
            get {
                    return motivobajacentrotrabajoId; 
            }
            set {
                    motivobajacentrotrabajoId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de MotivobajacentrotrabajoPk.
        /// </summary>
        /// <param name="motivobajacentrotrabajoId">Descripción motivobajacentrotrabajoId del tipo Int16.</param>
        public MotivobajacentrotrabajoPk(Int16 motivobajacentrotrabajoId) 
        {
            this.motivobajacentrotrabajoId = motivobajacentrotrabajoId;
        }
        /// <summary>
        /// Constructor normal de MotivobajacentrotrabajoPk.
        /// </summary>
        public MotivobajacentrotrabajoPk() 
        {
        }
        #endregion.
    }
}
