using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Sessiones".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 18 de agosto de 2009.</Para>
    /// <Para>Hora: 06:46:41 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Sessiones".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SessionId</term><description>Descripcion SessionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Fecha</term><description>Descripcion Fecha</description>
    ///    </item>
    ///    <item>
    ///        <term>Datos</term><description>Descripcion Datos</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SessionesDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// SessionesDTO sessiones = new SessionesDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Sessiones")]
    public class SessionesDP
    {
        #region Definicion de campos privados.
        private String sessionId;
        private String fecha;
        private Byte[] datos;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SessionId
        /// </summary> 
        [XmlElement("SessionId")]
        public String SessionId
        {
            get {
                    return sessionId; 
            }
            set {
                    sessionId = value; 
            }
        }

        /// <summary>
        /// Fecha
        /// </summary> 
        [XmlElement("Fecha")]
        public String Fecha
        {
            get {
                    return fecha; 
            }
            set {
                    fecha = value; 
            }
        }

        /// <summary>
        /// Datos
        /// </summary> 
        [XmlElement("Datos")]
        public Byte[] Datos
        {
            get {
                    return datos; 
            }
            set {
                    datos = value; 
            }
        }

        /// <summary>
        /// Llave primaria de SessionesPk
        /// </summary>
        [XmlElement("Pk")]
        public SessionesPk Pk {
            get {
                    return new SessionesPk( sessionId );
            }
        }
        #endregion.
    }
}
