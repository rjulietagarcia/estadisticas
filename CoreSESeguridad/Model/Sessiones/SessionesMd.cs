using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class SessionesMD
    {
        private SessionesDP sessiones = null;

        public SessionesMD(SessionesDP sessiones)
        {
            this.sessiones = sessiones;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Sessiones
            DbCommand com = con.CreateCommand();
            String insertSessiones = String.Format(CultureInfo.CurrentCulture,Sessiones.SessionesResx.SessionesInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "ID_SESSION",
                        "FECHA",
                        "DATOS");
            com.CommandText = insertSessiones;
            #endregion
            #region Parametros Insert Sessiones
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}ID_SESSION",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 90, ParameterDirection.Input, sessiones.SessionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}FECHA",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sessiones.Fecha,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}DATOS", ConstantesGlobales.ParameterPrefix), DbType.Binary, sessiones.Datos.Length, ParameterDirection.Input, sessiones.Datos);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Sessiones
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Sessiones
            DbCommand com = con.CreateCommand();
            String findSessiones = String.Format(CultureInfo.CurrentCulture,Sessiones.SessionesResx.SessionesFind,
                        ConstantesGlobales.ParameterPrefix,
                        "ID_SESSION");
            com.CommandText = findSessiones;
            #endregion
            #region Parametros Find Sessiones
            Common.CreateParameter(com, String.Format("{0}ID_SESSION",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 90, ParameterDirection.Input, sessiones.SessionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Sessiones
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Sessiones
            DbCommand com = con.CreateCommand();
            String deleteSessiones = String.Format(CultureInfo.CurrentCulture,Sessiones.SessionesResx.SessionesDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "ID_SESSION");
            com.CommandText = deleteSessiones;
            #endregion
            #region Parametros Delete Sessiones
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}ID_SESSION",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 90, ParameterDirection.Input, sessiones.SessionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Sessiones
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Sessiones
            DbCommand com = con.CreateCommand();
            String updateSessiones = String.Format(CultureInfo.CurrentCulture,Sessiones.SessionesResx.SessionesUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "FECHA",
                        "DATOS",
                        "ID_SESSION");
            com.CommandText = updateSessiones;
            #endregion
            #region Parametros Update Sessiones
            Common.CreateParameter(com, String.Format("{0}FECHA",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sessiones.Fecha,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}DATOS", ConstantesGlobales.ParameterPrefix), DbType.Binary, sessiones.Datos.Length, ParameterDirection.Input, sessiones.Datos);
            Common.CreateParameter(com, String.Format("{0}ID_SESSION",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 90, ParameterDirection.Input, sessiones.SessionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Sessiones
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static SessionesDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SessionesDP sessiones = new SessionesDP();
            sessiones.SessionId = dr.IsDBNull(0) ? "" : dr.GetString(0);
            sessiones.Fecha = dr.IsDBNull(1) ? "" : dr.GetDateTime(1).ToShortDateString();;
            sessiones.Datos = dr.IsDBNull(2) ? new byte[0] : (byte[])dr[2];
            return sessiones;
            #endregion
        }
        public static SessionesDP Load(DbConnection con, DbTransaction tran, SessionesPk pk)
        {
            #region SQL Load Sessiones
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadSessiones = String.Format(CultureInfo.CurrentCulture,Sessiones.SessionesResx.SessionesSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "ID_SESSION");
            com.CommandText = loadSessiones;
            #endregion
            #region Parametros Load Sessiones
            Common.CreateParameter(com, String.Format("{0}ID_SESSION",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 90, ParameterDirection.Input, pk.SessionId);
            #endregion
            SessionesDP sessiones;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Sessiones
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        sessiones = ReadRow(dr);
                    }
                    else
                        sessiones = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Sessiones
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return sessiones;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Sessiones
            DbCommand com = con.CreateCommand();
            String countSessiones = String.Format(CultureInfo.CurrentCulture,Sessiones.SessionesResx.SessionesCount,"");
            com.CommandText = countSessiones;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Sessiones
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SessionesDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Sessiones
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSessiones = String.Format(CultureInfo.CurrentCulture, Sessiones.SessionesResx.SessionesSelectAll, "", ConstantesGlobales.IncluyeRows);
            listSessiones = listSessiones.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSessiones, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Sessiones
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Sessiones
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Sessiones
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SessionesDP[])lista.ToArray(typeof(SessionesDP));
        }
    }
}
