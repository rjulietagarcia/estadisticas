using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "SessionesPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 18 de agosto de 2009.</Para>
    /// <Para>Hora: 06:46:41 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "SessionesPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SessionId</term><description>Descripcion SessionId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SessionesPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// SessionesPk sessionesPk = new SessionesPk(sessiones);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("SessionesPk")]
    public class SessionesPk
    {
        #region Definicion de campos privados.
        private String sessionId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SessionId
        /// </summary> 
        [XmlElement("SessionId")]
        public String SessionId
        {
            get {
                    return sessionId; 
            }
            set {
                    sessionId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de SessionesPk.
        /// </summary>
        /// <param name="sessionId">Descripción sessionId del tipo String.</param>
        public SessionesPk(String sessionId) 
        {
            this.sessionId = sessionId;
        }
        /// <summary>
        /// Constructor normal de SessionesPk.
        /// </summary>
        public SessionesPk() 
        {
        }
        #endregion.
    }
}
