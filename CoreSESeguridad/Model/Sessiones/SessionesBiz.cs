using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class SessionesBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, SessionesDP sessiones)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, SessionesDP sessiones)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, sessiones);
                        resultado = InternalSave(con, tran, sessiones);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla sessiones!";
                        throw new BusinessException("Error interno al intentar guardar sessiones!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla sessiones!";
                throw new BusinessException("Error interno al intentar guardar sessiones!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, SessionesDP sessiones)
        {
            SessionesMD sessionesMd = new SessionesMD(sessiones);
            String resultado = "";
            if (sessionesMd.Find(con, tran))
            {
                SessionesDP sessionesAnterior = SessionesMD.Load(con, tran, sessiones.Pk);
                if (sessionesMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla sessiones!";
                    throw new BusinessException("No se pude actualizar la tabla sessiones!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (sessionesMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla sessiones!";
                    throw new BusinessException("No se pude insertar en la tabla sessiones!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, SessionesDP sessiones)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, sessiones);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla sessiones!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla sessiones!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla sessiones!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla sessiones!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, SessionesDP sessiones)
        {
            String resultado = "";
            SessionesMD sessionesMd = new SessionesMD(sessiones);
            if (sessionesMd.Find(con, tran))
            {
                if (sessionesMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla sessiones!";
                    throw new BusinessException("No se pude eliminar en la tabla sessiones!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla sessiones!";
                throw new BusinessException("No se pude eliminar en la tabla sessiones!");
            }
            return resultado;
        }

        internal static SessionesDP LoadDetail(DbConnection con, DbTransaction tran, SessionesDP sessionesAnterior, SessionesDP sessiones) 
        {
            return sessiones;
        }
        public static SessionesDP Load(DbConnection con, SessionesPk pk)
        {
            SessionesDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SessionesMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Sessiones!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SessionesMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Sessiones!", ex);
            }
            return resultado;
        }

        public static SessionesDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SessionesDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SessionesMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Sessiones!", ex);
            }
            return resultado;

        }
    }
}
