using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "MunicipioPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:36:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "MunicipioPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>MunicipioId</term><description>Descripcion MunicipioId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "MunicipioPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// MunicipioPk municipioPk = new MunicipioPk(municipio);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("MunicipioPk")]
    public class MunicipioPk
    {
        #region Definicion de campos privados.
        private Int16 paisId;
        private Int16 entidadId;
        private Int16 municipioId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// MunicipioId
        /// </summary> 
        [XmlElement("MunicipioId")]
        public Int16 MunicipioId
        {
            get {
                    return municipioId; 
            }
            set {
                    municipioId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de MunicipioPk.
        /// </summary>
        /// <param name="paisId">Descripción paisId del tipo Int16.</param>
        /// <param name="entidadId">Descripción entidadId del tipo Int16.</param>
        /// <param name="municipioId">Descripción municipioId del tipo Int16.</param>
        public MunicipioPk(Int16 paisId, Int16 entidadId, Int16 municipioId) 
        {
            this.paisId = paisId;
            this.entidadId = entidadId;
            this.municipioId = municipioId;
        }
        /// <summary>
        /// Constructor normal de MunicipioPk.
        /// </summary>
        public MunicipioPk() 
        {
        }
        #endregion.
    }
}
