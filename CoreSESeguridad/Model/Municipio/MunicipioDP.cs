using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Municipio".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:36:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Municipio".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>MunicipioId</term><description>Descripcion MunicipioId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>ZonaEconomica</term><description>Descripcion ZonaEconomica</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>Entidad</term><description>Descripcion Entidad</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "MunicipioDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// MunicipioDTO municipio = new MunicipioDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Municipio")]
    public class MunicipioDP
    {
        #region Definicion de campos privados.
        private Int16 paisId;
        private Int16 entidadId;
        private Int16 municipioId;
        private String nombre;
        private String zonaEconomica;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private EntidadDP entidad;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// MunicipioId
        /// </summary> 
        [XmlElement("MunicipioId")]
        public Int16 MunicipioId
        {
            get {
                    return municipioId; 
            }
            set {
                    municipioId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// ZonaEconomica
        /// </summary> 
        [XmlElement("ZonaEconomica")]
        public String ZonaEconomica
        {
            get {
                    return zonaEconomica; 
            }
            set {
                    zonaEconomica = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// Entidad
        /// </summary> 
        [XmlElement("Entidad")]
        public EntidadDP Entidad
        {
            get {
                    return entidad; 
            }
            set {
                    entidad = value; 
            }
        }

        /// <summary>
        /// Llave primaria de MunicipioPk
        /// </summary>
        [XmlElement("Pk")]
        public MunicipioPk Pk {
            get {
                    return new MunicipioPk( paisId, entidadId, municipioId );
            }
        }
        #endregion.
    }
}
