using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class MunicipioMD
    {
        private MunicipioDP municipio = null;

        public MunicipioMD(MunicipioDP municipio)
        {
            this.municipio = municipio;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Municipio
            DbCommand com = con.CreateCommand();
            String insertMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Nombre",
                        "Zona_Economica",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertMunicipio;
            #endregion
            #region Parametros Insert Municipio
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.MunicipioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, municipio.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Zona_Economica",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 3, ParameterDirection.Input, municipio.ZonaEconomica);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, municipio.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, municipio.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(municipio.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Municipio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertMunicipio = String.Format(Municipio.MunicipioResx.MunicipioInsert, "", 
                    municipio.PaisId.ToString(),
                    municipio.EntidadId.ToString(),
                    municipio.MunicipioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,municipio.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,municipio.ZonaEconomica),
                    municipio.BitActivo.ToString(),
                    municipio.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(municipio.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertMunicipio);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Municipio
            DbCommand com = con.CreateCommand();
            String findMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio");
            com.CommandText = findMunicipio;
            #endregion
            #region Parametros Find Municipio
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.MunicipioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Municipio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioFind, "", 
                    municipio.PaisId.ToString(),
                    municipio.EntidadId.ToString(),
                    municipio.MunicipioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindMunicipio);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Municipio
            DbCommand com = con.CreateCommand();
            String deleteMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio");
            com.CommandText = deleteMunicipio;
            #endregion
            #region Parametros Delete Municipio
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.MunicipioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Municipio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioDelete, "", 
                    municipio.PaisId.ToString(),
                    municipio.EntidadId.ToString(),
                    municipio.MunicipioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteMunicipio);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Municipio
            DbCommand com = con.CreateCommand();
            String updateMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Zona_Economica",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio");
            com.CommandText = updateMunicipio;
            #endregion
            #region Parametros Update Municipio
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, municipio.Nombre);
            Common.CreateParameter(com, String.Format("{0}Zona_Economica",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 3, ParameterDirection.Input, municipio.ZonaEconomica);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, municipio.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, municipio.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(municipio.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, municipio.MunicipioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Municipio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateMunicipio = String.Format(Municipio.MunicipioResx.MunicipioUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,municipio.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,municipio.ZonaEconomica),
                    municipio.BitActivo.ToString(),
                    municipio.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(municipio.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    municipio.PaisId.ToString(),
                    municipio.EntidadId.ToString(),
                    municipio.MunicipioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateMunicipio);
                #endregion
            }
            return resultado;
        }
        protected static MunicipioDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            MunicipioDP municipio = new MunicipioDP();
            municipio.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            municipio.EntidadId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            municipio.MunicipioId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            municipio.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            municipio.ZonaEconomica = dr.IsDBNull(4) ? "" : dr.GetString(4);
            municipio.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            municipio.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            municipio.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            return municipio;
            #endregion
        }
        public static MunicipioDP Load(DbConnection con, DbTransaction tran, MunicipioPk pk)
        {
            #region SQL Load Municipio
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio");
            com.CommandText = loadMunicipio;
            #endregion
            #region Parametros Load Municipio
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.MunicipioId);
            #endregion
            MunicipioDP municipio;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Municipio
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        municipio = ReadRow(dr);
                    }
                    else
                        municipio = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Municipio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioSelect, "", 
                    pk.PaisId.ToString(),
                    pk.EntidadId.ToString(),
                    pk.MunicipioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadMunicipio);
                municipio = null;
                #endregion
            }
            return municipio;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Municipio
            DbCommand com = con.CreateCommand();
            String countMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioCount,"");
            com.CommandText = countMunicipio;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Municipio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountMunicipio);
                #endregion
            }
            return resultado;
        }
        public static MunicipioDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Municipio
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listMunicipio = String.Format(CultureInfo.CurrentCulture, Municipio.MunicipioResx.MunicipioSelectAll, "", ConstantesGlobales.IncluyeRows);
            listMunicipio = listMunicipio.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listMunicipio, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Municipio
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Municipio
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Municipio
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaMunicipio = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioResx.MunicipioSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaMunicipio);
                #endregion
            }
            return (MunicipioDP[])lista.ToArray(typeof(MunicipioDP));
        }
    }
}
