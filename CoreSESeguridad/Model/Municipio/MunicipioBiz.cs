using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class MunicipioBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, MunicipioDP municipio)
        {
            #region SaveDetail
            #region FK Entidad
            EntidadDP entidad = municipio.Entidad;
            if (entidad != null) 
            {
                EntidadMD entidadMd = new EntidadMD(entidad);
                if (!entidadMd.Find(con, tran))
                    entidadMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, MunicipioDP municipio)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, municipio);
                        resultado = InternalSave(con, tran, municipio);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla municipio!";
                        throw new BusinessException("Error interno al intentar guardar municipio!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla municipio!";
                throw new BusinessException("Error interno al intentar guardar municipio!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, MunicipioDP municipio)
        {
            MunicipioMD municipioMd = new MunicipioMD(municipio);
            String resultado = "";
            if (municipioMd.Find(con, tran))
            {
                MunicipioDP municipioAnterior = MunicipioMD.Load(con, tran, municipio.Pk);
                if (municipioMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla municipio!";
                    throw new BusinessException("No se pude actualizar la tabla municipio!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (municipioMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla municipio!";
                    throw new BusinessException("No se pude insertar en la tabla municipio!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, MunicipioDP municipio)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, municipio);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla municipio!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla municipio!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla municipio!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla municipio!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, MunicipioDP municipio)
        {
            String resultado = "";
            MunicipioMD municipioMd = new MunicipioMD(municipio);
            if (municipioMd.Find(con, tran))
            {
                if (municipioMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla municipio!";
                    throw new BusinessException("No se pude eliminar en la tabla municipio!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla municipio!";
                throw new BusinessException("No se pude eliminar en la tabla municipio!");
            }
            return resultado;
        }

        internal static MunicipioDP LoadDetail(DbConnection con, DbTransaction tran, MunicipioDP municipioAnterior, MunicipioDP municipio) 
        {
            #region FK Entidad
            EntidadPk entidadPk = new EntidadPk();
            if (municipioAnterior != null)
            {
                if (entidadPk.Equals(municipioAnterior.Pk))
                    municipio.Entidad = municipioAnterior.Entidad;
                else
                    municipio.Entidad = EntidadMD.Load(con, tran, entidadPk);
            }
            else
                municipio.Entidad = EntidadMD.Load(con, tran, entidadPk);
            #endregion
            return municipio;
        }
        public static MunicipioDP Load(DbConnection con, MunicipioPk pk)
        {
            MunicipioDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MunicipioMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Municipio!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = MunicipioMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Municipio!", ex);
            }
            return resultado;
        }

        public static MunicipioDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            MunicipioDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MunicipioMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Municipio!", ex);
            }
            return resultado;

        }
    }
}
