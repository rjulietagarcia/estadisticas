using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class AulaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, AulaDP aula)
        {
            #region SaveDetail
            #region FK Inmueble
            InmuebleDP inmueble = aula.Inmueble;
            if (inmueble != null) 
            {
                InmuebleMD inmuebleMd = new InmuebleMD(inmueble);
                if (!inmuebleMd.Find(con, tran))
                    inmuebleMd.Insert(con, tran);
            }
            #endregion
            #region FK Tipoaula
            TipoaulaDP tipoaula = aula.Tipoaula;
            if (tipoaula != null) 
            {
                TipoaulaMD tipoaulaMd = new TipoaulaMD(tipoaula);
                if (!tipoaulaMd.Find(con, tran))
                    tipoaulaMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, AulaDP aula)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, aula);
                        resultado = InternalSave(con, tran, aula);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla aula!";
                        throw new BusinessException("Error interno al intentar guardar aula!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla aula!";
                throw new BusinessException("Error interno al intentar guardar aula!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, AulaDP aula)
        {
            AulaMD aulaMd = new AulaMD(aula);
            String resultado = "";
            if (aulaMd.Find(con, tran))
            {
                AulaDP aulaAnterior = AulaMD.Load(con, tran, aula.Pk);
                if (aulaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla aula!";
                    throw new BusinessException("No se pude actualizar la tabla aula!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (aulaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla aula!";
                    throw new BusinessException("No se pude insertar en la tabla aula!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, AulaDP aula)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, aula);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla aula!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla aula!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla aula!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla aula!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, AulaDP aula)
        {
            String resultado = "";
            AulaMD aulaMd = new AulaMD(aula);
            if (aulaMd.Find(con, tran))
            {
                if (aulaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla aula!";
                    throw new BusinessException("No se pude eliminar en la tabla aula!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla aula!";
                throw new BusinessException("No se pude eliminar en la tabla aula!");
            }
            return resultado;
        }

        internal static AulaDP LoadDetail(DbConnection con, DbTransaction tran, AulaDP aulaAnterior, AulaDP aula) 
        {
            #region FK Inmueble
            InmueblePk inmueblePk = new InmueblePk();
            if (aulaAnterior != null)
            {
                if (inmueblePk.Equals(aulaAnterior.Pk))
                    aula.Inmueble = aulaAnterior.Inmueble;
                else
                    aula.Inmueble = InmuebleMD.Load(con, tran, inmueblePk);
            }
            else
                aula.Inmueble = InmuebleMD.Load(con, tran, inmueblePk);
            #endregion
            #region FK Tipoaula
            TipoaulaPk tipoaulaPk = new TipoaulaPk();
            if (aulaAnterior != null)
            {
                if (tipoaulaPk.Equals(aulaAnterior.Pk))
                    aula.Tipoaula = aulaAnterior.Tipoaula;
                else
                    aula.Tipoaula = TipoaulaMD.Load(con, tran, tipoaulaPk);
            }
            else
                aula.Tipoaula = TipoaulaMD.Load(con, tran, tipoaulaPk);
            #endregion
            return aula;
        }
        public static AulaDP Load(DbConnection con, AulaPk pk)
        {
            AulaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = AulaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Aula!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = AulaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Aula!", ex);
            }
            return resultado;
        }

        public static AulaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            AulaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = AulaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Aula!", ex);
            }
            return resultado;

        }
    }
}
