using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Aula".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:30:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Aula".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>InmuebleId</term><description>Descripcion InmuebleId</description>
    ///    </item>
    ///    <item>
    ///        <term>AulaId</term><description>Descripcion AulaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>TipoaulaId</term><description>Descripcion TipoaulaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Inmueble</term><description>Descripcion Inmueble</description>
    ///    </item>
    ///    <item>
    ///        <term>Tipoaula</term><description>Descripcion Tipoaula</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "AulaDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// AulaDTO aula = new AulaDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Aula")]
    public class AulaDP
    {
        #region Definicion de campos privados.
        private Int32 inmuebleId;
        private Byte aulaId;
        private String nombre;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Byte tipoaulaId;
        private InmuebleDP inmueble;
        private TipoaulaDP tipoaula;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// InmuebleId
        /// </summary> 
        [XmlElement("InmuebleId")]
        public Int32 InmuebleId
        {
            get {
                    return inmuebleId; 
            }
            set {
                    inmuebleId = value; 
            }
        }

        /// <summary>
        /// AulaId
        /// </summary> 
        [XmlElement("AulaId")]
        public Byte AulaId
        {
            get {
                    return aulaId; 
            }
            set {
                    aulaId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// TipoaulaId
        /// </summary> 
        [XmlElement("TipoaulaId")]
        public Byte TipoaulaId
        {
            get {
                    return tipoaulaId; 
            }
            set {
                    tipoaulaId = value; 
            }
        }

        /// <summary>
        /// Inmueble
        /// </summary> 
        [XmlElement("Inmueble")]
        public InmuebleDP Inmueble
        {
            get {
                    return inmueble; 
            }
            set {
                    inmueble = value; 
            }
        }

        /// <summary>
        /// Tipoaula
        /// </summary> 
        [XmlElement("Tipoaula")]
        public TipoaulaDP Tipoaula
        {
            get {
                    return tipoaula; 
            }
            set {
                    tipoaula = value; 
            }
        }

        /// <summary>
        /// Llave primaria de AulaPk
        /// </summary>
        [XmlElement("Pk")]
        public AulaPk Pk {
            get {
                    return new AulaPk( inmuebleId, aulaId );
            }
        }
        #endregion.
    }
}
