using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "AulaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:30:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "AulaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>InmuebleId</term><description>Descripcion InmuebleId</description>
    ///    </item>
    ///    <item>
    ///        <term>AulaId</term><description>Descripcion AulaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "AulaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// AulaPk aulaPk = new AulaPk(aula);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("AulaPk")]
    public class AulaPk
    {
        #region Definicion de campos privados.
        private Int32 inmuebleId;
        private Byte aulaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// InmuebleId
        /// </summary> 
        [XmlElement("InmuebleId")]
        public Int32 InmuebleId
        {
            get {
                    return inmuebleId; 
            }
            set {
                    inmuebleId = value; 
            }
        }

        /// <summary>
        /// AulaId
        /// </summary> 
        [XmlElement("AulaId")]
        public Byte AulaId
        {
            get {
                    return aulaId; 
            }
            set {
                    aulaId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de AulaPk.
        /// </summary>
        /// <param name="inmuebleId">Descripción inmuebleId del tipo Int32.</param>
        /// <param name="aulaId">Descripción aulaId del tipo Byte.</param>
        public AulaPk(Int32 inmuebleId, Byte aulaId) 
        {
            this.inmuebleId = inmuebleId;
            this.aulaId = aulaId;
        }
        /// <summary>
        /// Constructor normal de AulaPk.
        /// </summary>
        public AulaPk() 
        {
        }
        #endregion.
    }
}
