using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class AulaMD
    {
        private AulaDP aula = null;

        public AulaMD(AulaDP aula)
        {
            this.aula = aula;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Aula
            DbCommand com = con.CreateCommand();
            String insertAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Inmueble",
                        "Id_Aula",
                        "Nombre",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_TipoAula");
            com.CommandText = insertAula;
            #endregion
            #region Parametros Insert Aula
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aula.InmuebleId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Aula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aula.AulaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, aula.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aula.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aula.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(aula.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_TipoAula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aula.TipoaulaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Aula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertAula = String.Format(Aula.AulaResx.AulaInsert, "", 
                    aula.InmuebleId.ToString(),
                    aula.AulaId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,aula.Nombre),
                    aula.BitActivo.ToString(),
                    aula.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(aula.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    aula.TipoaulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorInsertAula);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Aula
            DbCommand com = con.CreateCommand();
            String findAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Inmueble",
                        "Id_Aula");
            com.CommandText = findAula;
            #endregion
            #region Parametros Find Aula
            Common.CreateParameter(com, String.Format("{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aula.InmuebleId);
            Common.CreateParameter(com, String.Format("{0}Id_Aula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aula.AulaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Aula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaFind, "", 
                    aula.InmuebleId.ToString(),
                    aula.AulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindAula);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Aula
            DbCommand com = con.CreateCommand();
            String deleteAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Inmueble",
                        "Id_Aula");
            com.CommandText = deleteAula;
            #endregion
            #region Parametros Delete Aula
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aula.InmuebleId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Aula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aula.AulaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Aula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaDelete, "", 
                    aula.InmuebleId.ToString(),
                    aula.AulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteAula);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Aula
            DbCommand com = con.CreateCommand();
            String updateAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_TipoAula",
                        "Id_Inmueble",
                        "Id_Aula");
            com.CommandText = updateAula;
            #endregion
            #region Parametros Update Aula
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, aula.Nombre);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aula.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aula.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(aula.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_TipoAula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aula.TipoaulaId);
            Common.CreateParameter(com, String.Format("{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aula.InmuebleId);
            Common.CreateParameter(com, String.Format("{0}Id_Aula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aula.AulaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Aula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateAula = String.Format(Aula.AulaResx.AulaUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,aula.Nombre),
                    aula.BitActivo.ToString(),
                    aula.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(aula.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    aula.TipoaulaId.ToString(),
                    aula.InmuebleId.ToString(),
                    aula.AulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateAula);
                #endregion
            }
            return resultado;
        }
        protected static AulaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            AulaDP aula = new AulaDP();
            aula.InmuebleId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            aula.AulaId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            aula.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            aula.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            aula.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            aula.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            aula.TipoaulaId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            return aula;
            #endregion
        }
        public static AulaDP Load(DbConnection con, DbTransaction tran, AulaPk pk)
        {
            #region SQL Load Aula
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Inmueble",
                        "Id_Aula");
            com.CommandText = loadAula;
            #endregion
            #region Parametros Load Aula
            Common.CreateParameter(com, String.Format("{0}Id_Inmueble",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.InmuebleId);
            Common.CreateParameter(com, String.Format("{0}Id_Aula",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.AulaId);
            #endregion
            AulaDP aula;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Aula
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        aula = ReadRow(dr);
                    }
                    else
                        aula = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Aula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaSelect, "", 
                    pk.InmuebleId.ToString(),
                    pk.AulaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadAula);
                aula = null;
                #endregion
            }
            return aula;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Aula
            DbCommand com = con.CreateCommand();
            String countAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaCount,"");
            com.CommandText = countAula;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Aula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountAula);
                #endregion
            }
            return resultado;
        }
        public static AulaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Aula
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listAula = String.Format(CultureInfo.CurrentCulture, Aula.AulaResx.AulaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listAula = listAula.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listAula, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Aula
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Aula
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Aula
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaAula = String.Format(CultureInfo.CurrentCulture,Aula.AulaResx.AulaSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaAula);
                #endregion
            }
            return (AulaDP[])lista.ToArray(typeof(AulaDP));
        }
    }
}
