using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class UsuarioAccionDenegadaMD
    {
        private UsuarioAccionDenegadaDP usuarioAccionDenegada = null;

        public UsuarioAccionDenegadaMD(UsuarioAccionDenegadaDP usuarioAccionDenegada)
        {
            this.usuarioAccionDenegada = usuarioAccionDenegada;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert UsuarioAccionDenegada
            DbCommand com = con.CreateCommand();
            String insertUsuarioAccionDenegada = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaResx.UsuarioAccionDenegadaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar",
                        "Bit_Autorizacion",
                        "Bit_Activo",
                        "Fecha_Actualizacion",
                        "Id_Usuario");
            com.CommandText = insertUsuarioAccionDenegada;
            #endregion
            #region Parametros Insert UsuarioAccionDenegada
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.AccionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccionDenegada.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioAccionDenegada.CicloescolarId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Autorizacion",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioAccionDenegada.BitAutorizacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioAccionDenegada.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuarioAccionDenegada.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccionDenegada.UsuarioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert UsuarioAccionDenegada
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find UsuarioAccionDenegada
            DbCommand com = con.CreateCommand();
            String findUsuarioAccionDenegada = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaResx.UsuarioAccionDenegadaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar");
            com.CommandText = findUsuarioAccionDenegada;
            #endregion
            #region Parametros Find UsuarioAccionDenegada
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.AccionId);
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccionDenegada.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioAccionDenegada.CicloescolarId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find UsuarioAccionDenegada
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete UsuarioAccionDenegada
            DbCommand com = con.CreateCommand();
            String deleteUsuarioAccionDenegada = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaResx.UsuarioAccionDenegadaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar");
            com.CommandText = deleteUsuarioAccionDenegada;
            #endregion
            #region Parametros Delete UsuarioAccionDenegada
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.AccionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccionDenegada.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioAccionDenegada.CicloescolarId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete UsuarioAccionDenegada
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update UsuarioAccionDenegada
            DbCommand com = con.CreateCommand();
            String updateUsuarioAccionDenegada = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaResx.UsuarioAccionDenegadaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Bit_Autorizacion",
                        "Bit_Activo",
                        "Fecha_Actualizacion",
                        "Id_Usuario",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar");
            com.CommandText = updateUsuarioAccionDenegada;
            #endregion
            #region Parametros Update UsuarioAccionDenegada
            Common.CreateParameter(com, String.Format("{0}Bit_Autorizacion",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioAccionDenegada.BitAutorizacion);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuarioAccionDenegada.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuarioAccionDenegada.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccionDenegada.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, usuarioAccionDenegada.AccionId);
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuarioAccionDenegada.UsuIdUsuarioId);
            Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, usuarioAccionDenegada.CicloescolarId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update UsuarioAccionDenegada
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static UsuarioAccionDenegadaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            UsuarioAccionDenegadaDP usuarioAccionDenegada = new UsuarioAccionDenegadaDP();
            usuarioAccionDenegada.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            usuarioAccionDenegada.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            usuarioAccionDenegada.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            usuarioAccionDenegada.AccionId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            usuarioAccionDenegada.UsuIdUsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            usuarioAccionDenegada.CicloescolarId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            usuarioAccionDenegada.BitAutorizacion = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            usuarioAccionDenegada.BitActivo = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            usuarioAccionDenegada.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            usuarioAccionDenegada.UsuarioId = dr.IsDBNull(9) ? 0 : dr.GetInt32(9);
            return usuarioAccionDenegada;
            #endregion
        }
        public static UsuarioAccionDenegadaDP Load(DbConnection con, DbTransaction tran, UsuarioAccionDenegadaPk pk)
        {
            #region SQL Load UsuarioAccionDenegada
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadUsuarioAccionDenegada = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaResx.UsuarioAccionDenegadaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Usu_Id_Usuario",
                        "Id_CicloEscolar");
            com.CommandText = loadUsuarioAccionDenegada;
            #endregion
            #region Parametros Load UsuarioAccionDenegada
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.AccionId);
            Common.CreateParameter(com, String.Format("{0}Usu_Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.UsuIdUsuarioId);
            //Common.CreateParameter(com, String.Format("{0}Id_CicloEscolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.CicloescolarId);
            #endregion
            UsuarioAccionDenegadaDP usuarioAccionDenegada;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load UsuarioAccionDenegada
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        usuarioAccionDenegada = ReadRow(dr);
                    }
                    else
                        usuarioAccionDenegada = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load UsuarioAccionDenegada
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return usuarioAccionDenegada;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count UsuarioAccionDenegada
            DbCommand com = con.CreateCommand();
            String countUsuarioAccionDenegada = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaResx.UsuarioAccionDenegadaCount,"");
            com.CommandText = countUsuarioAccionDenegada;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count UsuarioAccionDenegada
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static UsuarioAccionDenegadaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List UsuarioAccionDenegada
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuarioAccionDenegada = String.Format(CultureInfo.CurrentCulture, UsuarioAccionDenegada.UsuarioAccionDenegadaResx.UsuarioAccionDenegadaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listUsuarioAccionDenegada = listUsuarioAccionDenegada.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuarioAccionDenegada, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load UsuarioAccionDenegada
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista UsuarioAccionDenegada
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista UsuarioAccionDenegada
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (UsuarioAccionDenegadaDP[])lista.ToArray(typeof(UsuarioAccionDenegadaDP));
        }
    }
}
