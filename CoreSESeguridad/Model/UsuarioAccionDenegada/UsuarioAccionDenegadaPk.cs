using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "UsuarioAccionDenegadaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: jueves, 23 de julio de 2009.</Para>
    /// <Para>Hora: 04:26:21 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "UsuarioAccionDenegadaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    ///    <item>
    ///        <term>OpcionId</term><description>Descripcion OpcionId</description>
    ///    </item>
    ///    <item>
    ///        <term>AccionId</term><description>Descripcion AccionId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuarioId</term><description>Descripcion UsuIdUsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioAccionDenegadaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioAccionDenegadaPk usuarioacciondenegadaPk = new UsuarioAccionDenegadaPk(usuarioacciondenegada);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("UsuarioAccionDenegadaPk")]
    public class UsuarioAccionDenegadaPk
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Byte moduloId;
        private Byte opcionId;
        private Byte accionId;
        private Int32 usuIdUsuarioId;
        private Int16 cicloescolarId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        /// <summary>
        /// OpcionId
        /// </summary> 
        [XmlElement("OpcionId")]
        public Byte OpcionId
        {
            get {
                    return opcionId; 
            }
            set {
                    opcionId = value; 
            }
        }

        /// <summary>
        /// AccionId
        /// </summary> 
        [XmlElement("AccionId")]
        public Byte AccionId
        {
            get {
                    return accionId; 
            }
            set {
                    accionId = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuarioId
        /// </summary> 
        [XmlElement("UsuIdUsuarioId")]
        public Int32 UsuIdUsuarioId
        {
            get {
                    return usuIdUsuarioId; 
            }
            set {
                    usuIdUsuarioId = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de UsuarioAccionDenegadaPk.
        /// </summary>
        /// <param name="sistemaId">Descripción sistemaId del tipo Byte.</param>
        /// <param name="moduloId">Descripción moduloId del tipo Byte.</param>
        /// <param name="opcionId">Descripción opcionId del tipo Byte.</param>
        /// <param name="accionId">Descripción accionId del tipo Byte.</param>
        /// <param name="usuIdUsuarioId">Descripción usuIdUsuarioId del tipo Int32.</param>
        /// <param name="cicloescolarId">Descripción cicloescolarId del tipo Int16.</param>
        public UsuarioAccionDenegadaPk(Byte sistemaId, Byte moduloId, Byte opcionId, Byte accionId, Int32 usuIdUsuarioId, Int16 cicloescolarId) 
        {
            this.sistemaId = sistemaId;
            this.moduloId = moduloId;
            this.opcionId = opcionId;
            this.accionId = accionId;
            this.usuIdUsuarioId = usuIdUsuarioId;
            this.cicloescolarId = cicloescolarId;
        }
        /// <summary>
        /// Constructor normal de UsuarioAccionDenegadaPk.
        /// </summary>
        public UsuarioAccionDenegadaPk() 
        {
        }
        #endregion.
    }
}
