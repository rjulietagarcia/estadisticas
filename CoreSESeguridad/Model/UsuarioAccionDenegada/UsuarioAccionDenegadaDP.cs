using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "UsuarioAccionDenegada".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: jueves, 23 de julio de 2009.</Para>
    /// <Para>Hora: 04:26:21 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "UsuarioAccionDenegada".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    ///    <item>
    ///        <term>OpcionId</term><description>Descripcion OpcionId</description>
    ///    </item>
    ///    <item>
    ///        <term>AccionId</term><description>Descripcion AccionId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuarioId</term><description>Descripcion UsuIdUsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>BitAutorizacion</term><description>Descripcion BitAutorizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>Accion</term><description>Descripcion Accion</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuario</term><description>Descripcion UsuIdUsuario</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioAccionDenegadaDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioAccionDenegadaDTO usuarioacciondenegada = new UsuarioAccionDenegadaDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("UsuarioAccionDenegada")]
    public class UsuarioAccionDenegadaDP
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Byte moduloId;
        private Byte opcionId;
        private Byte accionId;
        private Int32 usuIdUsuarioId;
        private Int16 cicloescolarId;
        private Boolean bitAutorizacion;
        private Boolean bitActivo;
        private String fechaActualizacion;
        private Int32 usuarioId;
        private AccionDP accion;
        private UsuarioDP usuIdUsuario;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        /// <summary>
        /// OpcionId
        /// </summary> 
        [XmlElement("OpcionId")]
        public Byte OpcionId
        {
            get {
                    return opcionId; 
            }
            set {
                    opcionId = value; 
            }
        }

        /// <summary>
        /// AccionId
        /// </summary> 
        [XmlElement("AccionId")]
        public Byte AccionId
        {
            get {
                    return accionId; 
            }
            set {
                    accionId = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuarioId
        /// </summary> 
        [XmlElement("UsuIdUsuarioId")]
        public Int32 UsuIdUsuarioId
        {
            get {
                    return usuIdUsuarioId; 
            }
            set {
                    usuIdUsuarioId = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// BitAutorizacion
        /// </summary> 
        [XmlElement("BitAutorizacion")]
        public Boolean BitAutorizacion
        {
            get {
                    return bitAutorizacion; 
            }
            set {
                    bitAutorizacion = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// Accion
        /// </summary> 
        [XmlElement("Accion")]
        public AccionDP Accion
        {
            get {
                    return accion; 
            }
            set {
                    accion = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuario
        /// </summary> 
        [XmlElement("UsuIdUsuario")]
        public UsuarioDP UsuIdUsuario
        {
            get {
                    return usuIdUsuario; 
            }
            set {
                    usuIdUsuario = value; 
            }
        }

        /// <summary>
        /// Llave primaria de UsuarioAccionDenegadaPk
        /// </summary>
        [XmlElement("Pk")]
        public UsuarioAccionDenegadaPk Pk {
            get {
                    return new UsuarioAccionDenegadaPk( sistemaId, moduloId, opcionId, accionId, usuIdUsuarioId, cicloescolarId );
            }
        }
        #endregion.
    }
}
