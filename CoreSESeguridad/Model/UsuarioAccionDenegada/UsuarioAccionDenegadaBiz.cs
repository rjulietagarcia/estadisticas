using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class UsuarioAccionDenegadaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, UsuarioAccionDenegadaDP usuarioAccionDenegada)
        {
            #region SaveDetail
            #region FK Accion
            AccionDP accion = usuarioAccionDenegada.Accion;
            if (accion != null) 
            {
                AccionMD accionMd = new AccionMD(accion);
                if (!accionMd.Find(con, tran))
                    accionMd.Insert(con, tran);
            }
            #endregion
            #region FK Usuario
            UsuarioDP usuIdUsuario = usuarioAccionDenegada.UsuIdUsuario;
            if (usuIdUsuario != null) 
            {
                UsuarioMD usuarioMd = new UsuarioMD(usuIdUsuario);
                if (!usuarioMd.Find(con, tran))
                    usuarioMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, UsuarioAccionDenegadaDP usuarioAccionDenegada)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, usuarioAccionDenegada);
                        resultado = InternalSave(con, tran, usuarioAccionDenegada);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla usuarioAccionDenegada!";
                        throw new BusinessException("Error interno al intentar guardar usuarioAccionDenegada!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla usuarioAccionDenegada!";
                throw new BusinessException("Error interno al intentar guardar usuarioAccionDenegada!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, UsuarioAccionDenegadaDP usuarioAccionDenegada)
        {
            UsuarioAccionDenegadaMD usuarioAccionDenegadaMd = new UsuarioAccionDenegadaMD(usuarioAccionDenegada);
            String resultado = "";
            if (usuarioAccionDenegadaMd.Find(con, tran))
            {
                UsuarioAccionDenegadaDP usuarioAccionDenegadaAnterior = UsuarioAccionDenegadaMD.Load(con, tran, usuarioAccionDenegada.Pk);
                if (usuarioAccionDenegadaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla usuarioAccionDenegada!";
                    throw new BusinessException("No se pude actualizar la tabla usuarioAccionDenegada!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (usuarioAccionDenegadaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla usuarioAccionDenegada!";
                    throw new BusinessException("No se pude insertar en la tabla usuarioAccionDenegada!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, UsuarioAccionDenegadaDP usuarioAccionDenegada)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, usuarioAccionDenegada);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla usuarioAccionDenegada!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla usuarioAccionDenegada!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla usuarioAccionDenegada!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla usuarioAccionDenegada!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, UsuarioAccionDenegadaDP usuarioAccionDenegada)
        {
            String resultado = "";
            UsuarioAccionDenegadaMD usuarioAccionDenegadaMd = new UsuarioAccionDenegadaMD(usuarioAccionDenegada);
            if (usuarioAccionDenegadaMd.Find(con, tran))
            {
                if (usuarioAccionDenegadaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla usuarioAccionDenegada!";
                    throw new BusinessException("No se pude eliminar en la tabla usuarioAccionDenegada!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla usuarioAccionDenegada!";
                throw new BusinessException("No se pude eliminar en la tabla usuarioAccionDenegada!");
            }
            return resultado;
        }

        internal static UsuarioAccionDenegadaDP LoadDetail(DbConnection con, DbTransaction tran, UsuarioAccionDenegadaDP usuarioAccionDenegadaAnterior, UsuarioAccionDenegadaDP usuarioAccionDenegada) 
        {
            #region FK Accion
            AccionPk accionPk = new AccionPk();
            if (usuarioAccionDenegadaAnterior != null)
            {
                if (accionPk.Equals(usuarioAccionDenegadaAnterior.Pk))
                    usuarioAccionDenegada.Accion = usuarioAccionDenegadaAnterior.Accion;
                else
                    usuarioAccionDenegada.Accion = AccionMD.Load(con, tran, accionPk);
            }
            else
                usuarioAccionDenegada.Accion = AccionMD.Load(con, tran, accionPk);
            #endregion
            #region FK UsuIdUsuario
            UsuarioPk usuIdUsuarioPk = new UsuarioPk(
                usuarioAccionDenegada.UsuIdUsuarioId);
            if (usuarioAccionDenegadaAnterior != null)
            {
                if (usuIdUsuarioPk.Equals(usuarioAccionDenegadaAnterior.Pk))
                    usuarioAccionDenegada.UsuIdUsuario = usuarioAccionDenegadaAnterior.UsuIdUsuario;
                else
                    usuarioAccionDenegada.UsuIdUsuario = UsuarioMD.Load(con, tran, usuIdUsuarioPk);
            }
            else
                usuarioAccionDenegada.UsuIdUsuario = UsuarioMD.Load(con, tran, usuIdUsuarioPk);
            #endregion
            return usuarioAccionDenegada;
        }
        public static UsuarioAccionDenegadaDP Load(DbConnection con, UsuarioAccionDenegadaPk pk)
        {
            UsuarioAccionDenegadaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioAccionDenegadaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un UsuarioAccionDenegada!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioAccionDenegadaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un UsuarioAccionDenegada!", ex);
            }
            return resultado;
        }

        public static UsuarioAccionDenegadaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            UsuarioAccionDenegadaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioAccionDenegadaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de UsuarioAccionDenegada!", ex);
            }
            return resultado;

        }
    }
}
