using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "SubnivelPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: jueves, 11 de junio de 2009.</Para>
    /// <Para>Hora: 10:40:14 a.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "SubnivelPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>SubnivelId</term><description>Descripcion SubnivelId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SubnivelPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// SubnivelPk subnivelPk = new SubnivelPk(subnivel);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("SubnivelPk")]
    public class SubnivelPk
    {
        #region Definicion de campos privados.
        private Int16 tipoeducacionId;
        private Int16 nivelId;
        private Int16 subnivelId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public Int16 TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public Int16 NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        /// <summary>
        /// SubnivelId
        /// </summary> 
        [XmlElement("SubnivelId")]
        public Int16 SubnivelId
        {
            get {
                    return subnivelId; 
            }
            set {
                    subnivelId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de SubnivelPk.
        /// </summary>
        /// <param name="tipoeducacionId">Descripción tipoeducacionId del tipo Int16.</param>
        /// <param name="nivelId">Descripción nivelId del tipo Int16.</param>
        /// <param name="subnivelId">Descripción subnivelId del tipo Int16.</param>
        public SubnivelPk(Int16 tipoeducacionId, Int16 nivelId, Int16 subnivelId) 
        {
            this.tipoeducacionId = tipoeducacionId;
            this.nivelId = nivelId;
            this.subnivelId = subnivelId;
        }
        /// <summary>
        /// Constructor normal de SubnivelPk.
        /// </summary>
        public SubnivelPk() 
        {
        }
        #endregion.
    }
}
