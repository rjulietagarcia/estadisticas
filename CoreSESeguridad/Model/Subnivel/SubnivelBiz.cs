using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class SubnivelBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, SubnivelDP subnivel)
        {
            #region SaveDetail
            #region FK Nivel
            NivelDP nivel = subnivel.Nivel;
            if (nivel != null) 
            {
                NivelMD nivelMd = new NivelMD(nivel);
                if (!nivelMd.Find(con, tran))
                    nivelMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, SubnivelDP subnivel)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, subnivel);
                        resultado = InternalSave(con, tran, subnivel);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla subnivel!";
                        throw new BusinessException("Error interno al intentar guardar subnivel!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla subnivel!";
                throw new BusinessException("Error interno al intentar guardar subnivel!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, SubnivelDP subnivel)
        {
            SubnivelMD subnivelMd = new SubnivelMD(subnivel);
            String resultado = "";
            if (subnivelMd.Find(con, tran))
            {
                SubnivelDP subnivelAnterior = SubnivelMD.Load(con, tran, subnivel.Pk);
                if (subnivelMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla subnivel!";
                    throw new BusinessException("No se pude actualizar la tabla subnivel!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (subnivelMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla subnivel!";
                    throw new BusinessException("No se pude insertar en la tabla subnivel!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, SubnivelDP subnivel)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, subnivel);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla subnivel!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla subnivel!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla subnivel!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla subnivel!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, SubnivelDP subnivel)
        {
            String resultado = "";
            SubnivelMD subnivelMd = new SubnivelMD(subnivel);
            if (subnivelMd.Find(con, tran))
            {
                if (subnivelMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla subnivel!";
                    throw new BusinessException("No se pude eliminar en la tabla subnivel!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla subnivel!";
                throw new BusinessException("No se pude eliminar en la tabla subnivel!");
            }
            return resultado;
        }

        internal static SubnivelDP LoadDetail(DbConnection con, DbTransaction tran, SubnivelDP subnivelAnterior, SubnivelDP subnivel) 
        {
            #region FK Nivel
            NivelPk nivelPk = new NivelPk();
            if (subnivelAnterior != null)
            {
                if (nivelPk.Equals(subnivelAnterior.Pk))
                    subnivel.Nivel = subnivelAnterior.Nivel;
                else
                    subnivel.Nivel = NivelMD.Load(con, tran, nivelPk);
            }
            else
                subnivel.Nivel = NivelMD.Load(con, tran, nivelPk);
            #endregion
            return subnivel;
        }
        public static SubnivelDP Load(DbConnection con, SubnivelPk pk)
        {
            SubnivelDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SubnivelMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Subnivel!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SubnivelMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Subnivel!", ex);
            }
            return resultado;
        }

        public static SubnivelDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SubnivelDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SubnivelMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Subnivel!", ex);
            }
            return resultado;

        }
    }
}
