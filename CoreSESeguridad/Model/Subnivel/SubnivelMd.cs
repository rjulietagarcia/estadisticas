using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class SubnivelMD
    {
        private SubnivelDP subnivel = null;

        public SubnivelMD(SubnivelDP subnivel)
        {
            this.subnivel = subnivel;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Subnivel
            DbCommand com = con.CreateCommand();
            String insertSubnivel = String.Format(CultureInfo.CurrentCulture,Subnivel.SubnivelResx.SubnivelInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel",
                        "nombre",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion");
            com.CommandText = insertSubnivel;
            #endregion
            #region Parametros Insert Subnivel
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.NivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.SubnivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, subnivel.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, subnivel.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(subnivel.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Subnivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Subnivel
            DbCommand com = con.CreateCommand();
            String findSubnivel = String.Format(CultureInfo.CurrentCulture,Subnivel.SubnivelResx.SubnivelFind,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = findSubnivel;
            #endregion
            #region Parametros Find Subnivel
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.NivelId);
            Common.CreateParameter(com, String.Format("{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.SubnivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Subnivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Subnivel
            DbCommand com = con.CreateCommand();
            String deleteSubnivel = String.Format(CultureInfo.CurrentCulture,Subnivel.SubnivelResx.SubnivelDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = deleteSubnivel;
            #endregion
            #region Parametros Delete Subnivel
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.NivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.SubnivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Subnivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Subnivel
            DbCommand com = con.CreateCommand();
            String updateSubnivel = String.Format(CultureInfo.CurrentCulture,Subnivel.SubnivelResx.SubnivelUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "nombre",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion",
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = updateSubnivel;
            #endregion
            #region Parametros Update Subnivel
            Common.CreateParameter(com, String.Format("{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, subnivel.Nombre);
            Common.CreateParameter(com, String.Format("{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, subnivel.BitActivo);
            Common.CreateParameter(com, String.Format("{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(subnivel.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.NivelId);
            Common.CreateParameter(com, String.Format("{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, subnivel.SubnivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Subnivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static SubnivelDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SubnivelDP subnivel = new SubnivelDP();
            subnivel.TipoeducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            subnivel.NivelId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            subnivel.SubnivelId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            subnivel.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            subnivel.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            subnivel.UsuarioId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            subnivel.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            return subnivel;
            #endregion
        }
        public static SubnivelDP Load(DbConnection con, DbTransaction tran, SubnivelPk pk)
        {
            #region SQL Load Subnivel
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadSubnivel = String.Format(CultureInfo.CurrentCulture,Subnivel.SubnivelResx.SubnivelSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel",
                        "id_subnivel");
            com.CommandText = loadSubnivel;
            #endregion
            #region Parametros Load Subnivel
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.NivelId);
            Common.CreateParameter(com, String.Format("{0}id_subnivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.SubnivelId);
            #endregion
            SubnivelDP subnivel;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Subnivel
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        subnivel = ReadRow(dr);
                    }
                    else
                        subnivel = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Subnivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return subnivel;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Subnivel
            DbCommand com = con.CreateCommand();
            String countSubnivel = String.Format(CultureInfo.CurrentCulture,Subnivel.SubnivelResx.SubnivelCount,"");
            com.CommandText = countSubnivel;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Subnivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SubnivelDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Subnivel
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSubnivel = String.Format(CultureInfo.CurrentCulture, Subnivel.SubnivelResx.SubnivelSelectAll, "", ConstantesGlobales.IncluyeRows);
            listSubnivel = listSubnivel.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSubnivel, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Subnivel
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Subnivel
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Subnivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SubnivelDP[])lista.ToArray(typeof(SubnivelDP));
        }
    }
}
