using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class LocalidadBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, LocalidadDP localidad)
        {
            #region SaveDetail
            #region FK Municipio
            MunicipioDP municipio = localidad.Municipio;
            if (municipio != null) 
            {
                MunicipioMD municipioMd = new MunicipioMD(municipio);
                if (!municipioMd.Find(con, tran))
                    municipioMd.Insert(con, tran);
            }
            #endregion
            #region FK Categorialocalidad
            CategorialocalidadDP categorialocalidad = localidad.Categorialocalidad;
            if (categorialocalidad != null) 
            {
                CategorialocalidadMD categorialocalidadMd = new CategorialocalidadMD(categorialocalidad);
                if (!categorialocalidadMd.Find(con, tran))
                    categorialocalidadMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, LocalidadDP localidad)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, localidad);
                        resultado = InternalSave(con, tran, localidad);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla localidad!";
                        throw new BusinessException("Error interno al intentar guardar localidad!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla localidad!";
                throw new BusinessException("Error interno al intentar guardar localidad!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, LocalidadDP localidad)
        {
            LocalidadMD localidadMd = new LocalidadMD(localidad);
            String resultado = "";
            if (localidadMd.Find(con, tran))
            {
                LocalidadDP localidadAnterior = LocalidadMD.Load(con, tran, localidad.Pk);
                if (localidadMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla localidad!";
                    throw new BusinessException("No se pude actualizar la tabla localidad!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (localidadMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla localidad!";
                    throw new BusinessException("No se pude insertar en la tabla localidad!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, LocalidadDP localidad)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, localidad);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla localidad!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla localidad!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla localidad!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla localidad!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, LocalidadDP localidad)
        {
            String resultado = "";
            LocalidadMD localidadMd = new LocalidadMD(localidad);
            if (localidadMd.Find(con, tran))
            {
                if (localidadMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla localidad!";
                    throw new BusinessException("No se pude eliminar en la tabla localidad!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla localidad!";
                throw new BusinessException("No se pude eliminar en la tabla localidad!");
            }
            return resultado;
        }

        internal static LocalidadDP LoadDetail(DbConnection con, DbTransaction tran, LocalidadDP localidadAnterior, LocalidadDP localidad) 
        {
            #region FK Municipio
            MunicipioPk municipioPk = new MunicipioPk();
            if (localidadAnterior != null)
            {
                if (municipioPk.Equals(localidadAnterior.Pk))
                    localidad.Municipio = localidadAnterior.Municipio;
                else
                    localidad.Municipio = MunicipioMD.Load(con, tran, municipioPk);
            }
            else
                localidad.Municipio = MunicipioMD.Load(con, tran, municipioPk);
            #endregion
            #region FK Categorialocalidad
            CategorialocalidadPk categorialocalidadPk = new CategorialocalidadPk();
            if (localidadAnterior != null)
            {
                if (categorialocalidadPk.Equals(localidadAnterior.Pk))
                    localidad.Categorialocalidad = localidadAnterior.Categorialocalidad;
                else
                    localidad.Categorialocalidad = CategorialocalidadMD.Load(con, tran, categorialocalidadPk);
            }
            else
                localidad.Categorialocalidad = CategorialocalidadMD.Load(con, tran, categorialocalidadPk);
            #endregion
            return localidad;
        }
        public static LocalidadDP Load(DbConnection con, LocalidadPk pk)
        {
            LocalidadDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = LocalidadMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Localidad!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = LocalidadMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Localidad!", ex);
            }
            return resultado;
        }

        public static LocalidadDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            LocalidadDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = LocalidadMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Localidad!", ex);
            }
            return resultado;

        }
    }
}
