using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class LocalidadMD
    {
        private LocalidadDP localidad = null;

        public LocalidadMD(LocalidadDP localidad)
        {
            this.localidad = localidad;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Localidad
            DbCommand com = con.CreateCommand();
            String insertLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Localidad",
                        "Id_CategoriaLocalidad",
                        "Nombre",
                        "Codigo_Postal",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertLocalidad;
            #endregion
            #region Parametros Insert Localidad
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.MunicipioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.LocalidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_CategoriaLocalidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.CategorialocalidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 60, ParameterDirection.Input, localidad.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Codigo_Postal",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 5, ParameterDirection.Input, localidad.CodigoPostal);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, localidad.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, localidad.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(localidad.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Localidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertLocalidad = String.Format(Localidad.LocalidadResx.LocalidadInsert, "", 
                    localidad.PaisId.ToString(),
                    localidad.EntidadId.ToString(),
                    localidad.MunicipioId.ToString(),
                    localidad.LocalidadId.ToString(),
                    localidad.CategorialocalidadId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,localidad.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,localidad.CodigoPostal),
                    localidad.BitActivo.ToString(),
                    localidad.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(localidad.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertLocalidad);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Localidad
            DbCommand com = con.CreateCommand();
            String findLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Localidad");
            com.CommandText = findLocalidad;
            #endregion
            #region Parametros Find Localidad
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.MunicipioId);
            Common.CreateParameter(com, String.Format("{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.LocalidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Localidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadFind, "", 
                    localidad.PaisId.ToString(),
                    localidad.EntidadId.ToString(),
                    localidad.MunicipioId.ToString(),
                    localidad.LocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindLocalidad);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Localidad
            DbCommand com = con.CreateCommand();
            String deleteLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Localidad");
            com.CommandText = deleteLocalidad;
            #endregion
            #region Parametros Delete Localidad
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.PaisId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.EntidadId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.MunicipioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.LocalidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Localidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadDelete, "", 
                    localidad.PaisId.ToString(),
                    localidad.EntidadId.ToString(),
                    localidad.MunicipioId.ToString(),
                    localidad.LocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteLocalidad);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Localidad
            DbCommand com = con.CreateCommand();
            String updateLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_CategoriaLocalidad",
                        "Nombre",
                        "Codigo_Postal",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Localidad");
            com.CommandText = updateLocalidad;
            #endregion
            #region Parametros Update Localidad
            Common.CreateParameter(com, String.Format("{0}Id_CategoriaLocalidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.CategorialocalidadId);
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 60, ParameterDirection.Input, localidad.Nombre);
            Common.CreateParameter(com, String.Format("{0}Codigo_Postal",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 5, ParameterDirection.Input, localidad.CodigoPostal);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, localidad.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, localidad.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(localidad.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.MunicipioId);
            Common.CreateParameter(com, String.Format("{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, localidad.LocalidadId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Localidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateLocalidad = String.Format(Localidad.LocalidadResx.LocalidadUpdate, "", 
                    localidad.CategorialocalidadId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,localidad.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,localidad.CodigoPostal),
                    localidad.BitActivo.ToString(),
                    localidad.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(localidad.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    localidad.PaisId.ToString(),
                    localidad.EntidadId.ToString(),
                    localidad.MunicipioId.ToString(),
                    localidad.LocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateLocalidad);
                #endregion
            }
            return resultado;
        }
        protected static LocalidadDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            LocalidadDP localidad = new LocalidadDP();
            localidad.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            localidad.EntidadId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            localidad.MunicipioId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            localidad.LocalidadId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            localidad.CategorialocalidadId = dr.IsDBNull(4) ? (short)0 : dr.GetInt16(4);
            localidad.Nombre = dr.IsDBNull(5) ? "" : dr.GetString(5);
            localidad.CodigoPostal = dr.IsDBNull(6) ? "" : dr.GetString(6);
            localidad.BitActivo = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            localidad.UsuarioId = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            localidad.FechaActualizacion = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            return localidad;
            #endregion
        }
        public static LocalidadDP Load(DbConnection con, DbTransaction tran, LocalidadPk pk)
        {
            #region SQL Load Localidad
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Pais",
                        "Id_Entidad",
                        "Id_Municipio",
                        "Id_Localidad");
            com.CommandText = loadLocalidad;
            #endregion
            #region Parametros Load Localidad
            Common.CreateParameter(com, String.Format("{0}Id_Pais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PaisId);
            Common.CreateParameter(com, String.Format("{0}Id_Entidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.EntidadId);
            Common.CreateParameter(com, String.Format("{0}Id_Municipio",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.MunicipioId);
            Common.CreateParameter(com, String.Format("{0}Id_Localidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.LocalidadId);
            #endregion
            LocalidadDP localidad;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Localidad
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        localidad = ReadRow(dr);
                    }
                    else
                        localidad = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Localidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadSelect, "", 
                    pk.PaisId.ToString(),
                    pk.EntidadId.ToString(),
                    pk.MunicipioId.ToString(),
                    pk.LocalidadId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadLocalidad);
                localidad = null;
                #endregion
            }
            return localidad;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Localidad
            DbCommand com = con.CreateCommand();
            String countLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadCount,"");
            com.CommandText = countLocalidad;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Localidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountLocalidad);
                #endregion
            }
            return resultado;
        }
        public static LocalidadDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Localidad
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listLocalidad = String.Format(CultureInfo.CurrentCulture, Localidad.LocalidadResx.LocalidadSelectAll, "", ConstantesGlobales.IncluyeRows);
            listLocalidad = listLocalidad.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listLocalidad, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Localidad
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Localidad
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Localidad
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaLocalidad = String.Format(CultureInfo.CurrentCulture,Localidad.LocalidadResx.LocalidadSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaLocalidad);
                #endregion
            }
            return (LocalidadDP[])lista.ToArray(typeof(LocalidadDP));
        }
    }
}
