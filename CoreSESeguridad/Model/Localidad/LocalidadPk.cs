using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "LocalidadPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 09 de junio de 2009.</Para>
    /// <Para>Hora: 05:34:57 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "LocalidadPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>MunicipioId</term><description>Descripcion MunicipioId</description>
    ///    </item>
    ///    <item>
    ///        <term>LocalidadId</term><description>Descripcion LocalidadId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "LocalidadPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// LocalidadPk localidadPk = new LocalidadPk(localidad);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("LocalidadPk")]
    public class LocalidadPk
    {
        #region Definicion de campos privados.
        private Int16 paisId;
        private Int16 entidadId;
        private Int16 municipioId;
        private Int16 localidadId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// MunicipioId
        /// </summary> 
        [XmlElement("MunicipioId")]
        public Int16 MunicipioId
        {
            get {
                    return municipioId; 
            }
            set {
                    municipioId = value; 
            }
        }

        /// <summary>
        /// LocalidadId
        /// </summary> 
        [XmlElement("LocalidadId")]
        public Int16 LocalidadId
        {
            get {
                    return localidadId; 
            }
            set {
                    localidadId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de LocalidadPk.
        /// </summary>
        /// <param name="paisId">Descripción paisId del tipo Int16.</param>
        /// <param name="entidadId">Descripción entidadId del tipo Int16.</param>
        /// <param name="municipioId">Descripción municipioId del tipo Int16.</param>
        /// <param name="localidadId">Descripción localidadId del tipo Int16.</param>
        public LocalidadPk(Int16 paisId, Int16 entidadId, Int16 municipioId, Int16 localidadId) 
        {
            this.paisId = paisId;
            this.entidadId = entidadId;
            this.municipioId = municipioId;
            this.localidadId = localidadId;
        }
        /// <summary>
        /// Constructor normal de LocalidadPk.
        /// </summary>
        public LocalidadPk() 
        {
        }
        #endregion.
    }
}
