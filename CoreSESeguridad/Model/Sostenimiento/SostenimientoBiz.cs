using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class SostenimientoBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, SostenimientoDP sostenimiento)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, SostenimientoDP sostenimiento)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, sostenimiento);
                        resultado = InternalSave(con, tran, sostenimiento);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla sostenimiento!";
                        throw new BusinessException("Error interno al intentar guardar sostenimiento!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla sostenimiento!";
                throw new BusinessException("Error interno al intentar guardar sostenimiento!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, SostenimientoDP sostenimiento)
        {
            SostenimientoMD sostenimientoMd = new SostenimientoMD(sostenimiento);
            String resultado = "";
            if (sostenimientoMd.Find(con, tran))
            {
                SostenimientoDP sostenimientoAnterior = SostenimientoMD.Load(con, tran, sostenimiento.Pk);
                if (sostenimientoMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla sostenimiento!";
                    throw new BusinessException("No se pude actualizar la tabla sostenimiento!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (sostenimientoMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla sostenimiento!";
                    throw new BusinessException("No se pude insertar en la tabla sostenimiento!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, SostenimientoDP sostenimiento)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, sostenimiento);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla sostenimiento!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla sostenimiento!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla sostenimiento!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla sostenimiento!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, SostenimientoDP sostenimiento)
        {
            String resultado = "";
            SostenimientoMD sostenimientoMd = new SostenimientoMD(sostenimiento);
            if (sostenimientoMd.Find(con, tran))
            {
                if (sostenimientoMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla sostenimiento!";
                    throw new BusinessException("No se pude eliminar en la tabla sostenimiento!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla sostenimiento!";
                throw new BusinessException("No se pude eliminar en la tabla sostenimiento!");
            }
            return resultado;
        }

        internal static SostenimientoDP LoadDetail(DbConnection con, DbTransaction tran, SostenimientoDP sostenimientoAnterior, SostenimientoDP sostenimiento) 
        {
            return sostenimiento;
        }
        public static SostenimientoDP Load(DbConnection con, SostenimientoPk pk)
        {
            SostenimientoDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SostenimientoMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Sostenimiento!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SostenimientoMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Sostenimiento!", ex);
            }
            return resultado;
        }

        public static SostenimientoDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SostenimientoDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SostenimientoMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Sostenimiento!", ex);
            }
            return resultado;

        }
    }
}
