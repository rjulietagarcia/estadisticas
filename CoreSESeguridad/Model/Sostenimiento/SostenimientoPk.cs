using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "SostenimientoPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "SostenimientoPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SostenimientoPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// SostenimientoPk sostenimientoPk = new SostenimientoPk(sostenimiento);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("SostenimientoPk")]
    public class SostenimientoPk
    {
        #region Definicion de campos privados.
        private Byte sostenimientoId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de SostenimientoPk.
        /// </summary>
        /// <param name="sostenimientoId">Descripción sostenimientoId del tipo Byte.</param>
        public SostenimientoPk(Byte sostenimientoId) 
        {
            this.sostenimientoId = sostenimientoId;
        }
        /// <summary>
        /// Constructor normal de SostenimientoPk.
        /// </summary>
        public SostenimientoPk() 
        {
        }
        #endregion.
    }
}
