using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Sostenimiento".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Sostenimiento".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitfederal</term><description>Descripcion Bitfederal</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitestatal</term><description>Descripcion Bitestatal</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitparticular</term><description>Descripcion Bitparticular</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SostenimientoDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// SostenimientoDTO sostenimiento = new SostenimientoDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Sostenimiento")]
    public class SostenimientoDP
    {
        #region Definicion de campos privados.
        private Byte sostenimientoId;
        private String nombre;
        private Boolean bitfederal;
        private Boolean bitestatal;
        private Boolean bitparticular;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// Bitfederal
        /// </summary> 
        [XmlElement("Bitfederal")]
        public Boolean Bitfederal
        {
            get {
                    return bitfederal; 
            }
            set {
                    bitfederal = value; 
            }
        }

        /// <summary>
        /// Bitestatal
        /// </summary> 
        [XmlElement("Bitestatal")]
        public Boolean Bitestatal
        {
            get {
                    return bitestatal; 
            }
            set {
                    bitestatal = value; 
            }
        }

        /// <summary>
        /// Bitparticular
        /// </summary> 
        [XmlElement("Bitparticular")]
        public Boolean Bitparticular
        {
            get {
                    return bitparticular; 
            }
            set {
                    bitparticular = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// Llave primaria de SostenimientoPk
        /// </summary>
        [XmlElement("Pk")]
        public SostenimientoPk Pk {
            get {
                    return new SostenimientoPk( sostenimientoId );
            }
        }
        #endregion.
    }
}
