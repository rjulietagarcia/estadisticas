using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class SostenimientoMD
    {
        private SostenimientoDP sostenimiento = null;

        public SostenimientoMD(SostenimientoDP sostenimiento)
        {
            this.sostenimiento = sostenimiento;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Sostenimiento
            DbCommand com = con.CreateCommand();
            String insertSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sostenimiento",
                        "Nombre",
                        "BitFederal",
                        "BitEstatal",
                        "BitParticular",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertSostenimiento;
            #endregion
            #region Parametros Insert Sostenimiento
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sostenimiento.SostenimientoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, sostenimiento.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitFederal",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sostenimiento.Bitfederal);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitEstatal",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sostenimiento.Bitestatal);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitParticular",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sostenimiento.Bitparticular);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sostenimiento.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, sostenimiento.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sostenimiento.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Sostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertSostenimiento = String.Format(Sostenimiento.SostenimientoResx.SostenimientoInsert, "", 
                    sostenimiento.SostenimientoId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,sostenimiento.Nombre),
                    sostenimiento.Bitfederal.ToString(),
                    sostenimiento.Bitestatal.ToString(),
                    sostenimiento.Bitparticular.ToString(),
                    sostenimiento.BitActivo.ToString(),
                    sostenimiento.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(sostenimiento.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertSostenimiento);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Sostenimiento
            DbCommand com = con.CreateCommand();
            String findSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sostenimiento");
            com.CommandText = findSostenimiento;
            #endregion
            #region Parametros Find Sostenimiento
            Common.CreateParameter(com, String.Format("{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sostenimiento.SostenimientoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Sostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoFind, "", 
                    sostenimiento.SostenimientoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindSostenimiento);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Sostenimiento
            DbCommand com = con.CreateCommand();
            String deleteSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sostenimiento");
            com.CommandText = deleteSostenimiento;
            #endregion
            #region Parametros Delete Sostenimiento
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sostenimiento.SostenimientoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Sostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoDelete, "", 
                    sostenimiento.SostenimientoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteSostenimiento);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Sostenimiento
            DbCommand com = con.CreateCommand();
            String updateSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "BitFederal",
                        "BitEstatal",
                        "BitParticular",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Sostenimiento");
            com.CommandText = updateSostenimiento;
            #endregion
            #region Parametros Update Sostenimiento
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, sostenimiento.Nombre);
            Common.CreateParameter(com, String.Format("{0}BitFederal",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sostenimiento.Bitfederal);
            Common.CreateParameter(com, String.Format("{0}BitEstatal",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sostenimiento.Bitestatal);
            Common.CreateParameter(com, String.Format("{0}BitParticular",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sostenimiento.Bitparticular);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, sostenimiento.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, sostenimiento.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(sostenimiento.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, sostenimiento.SostenimientoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Sostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateSostenimiento = String.Format(Sostenimiento.SostenimientoResx.SostenimientoUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,sostenimiento.Nombre),
                    sostenimiento.Bitfederal.ToString(),
                    sostenimiento.Bitestatal.ToString(),
                    sostenimiento.Bitparticular.ToString(),
                    sostenimiento.BitActivo.ToString(),
                    sostenimiento.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(sostenimiento.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    sostenimiento.SostenimientoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateSostenimiento);
                #endregion
            }
            return resultado;
        }
        protected static SostenimientoDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SostenimientoDP sostenimiento = new SostenimientoDP();
            sostenimiento.SostenimientoId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            sostenimiento.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            sostenimiento.Bitfederal = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            sostenimiento.Bitestatal = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            sostenimiento.Bitparticular = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            sostenimiento.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            sostenimiento.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            sostenimiento.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            return sostenimiento;
            #endregion
        }
        public static SostenimientoDP Load(DbConnection con, DbTransaction tran, SostenimientoPk pk)
        {
            #region SQL Load Sostenimiento
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sostenimiento");
            com.CommandText = loadSostenimiento;
            #endregion
            #region Parametros Load Sostenimiento
            Common.CreateParameter(com, String.Format("{0}Id_Sostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SostenimientoId);
            #endregion
            SostenimientoDP sostenimiento;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Sostenimiento
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        sostenimiento = ReadRow(dr);
                    }
                    else
                        sostenimiento = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Sostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoSelect, "", 
                    pk.SostenimientoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadSostenimiento);
                sostenimiento = null;
                #endregion
            }
            return sostenimiento;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Sostenimiento
            DbCommand com = con.CreateCommand();
            String countSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoCount,"");
            com.CommandText = countSostenimiento;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Sostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountSostenimiento);
                #endregion
            }
            return resultado;
        }
        public static SostenimientoDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Sostenimiento
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSostenimiento = String.Format(CultureInfo.CurrentCulture, Sostenimiento.SostenimientoResx.SostenimientoSelectAll, "", ConstantesGlobales.IncluyeRows);
            listSostenimiento = listSostenimiento.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSostenimiento, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Sostenimiento
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Sostenimiento
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Sostenimiento
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaSostenimiento = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoResx.SostenimientoSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaSostenimiento);
                #endregion
            }
            return (SostenimientoDP[])lista.ToArray(typeof(SostenimientoDP));
        }
    }
}
