using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class UsuarioBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, UsuarioDP usuario)
        {
            #region SaveDetail
            #region FK Persona
            PersonaDP persona = usuario.Persona;
            if (persona != null) 
            {
                PersonaMD personaMd = new PersonaMD(persona);
                if (!personaMd.Find(con, tran))
                    personaMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, UsuarioDP usuario)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, usuario);
                        resultado = InternalSave(con, tran, usuario);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla usuario!";
                        throw new BusinessException("Error interno al intentar guardar usuario!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla usuario!";
                throw new BusinessException("Error interno al intentar guardar usuario!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, UsuarioDP usuario)
        {
            UsuarioMD usuarioMd = new UsuarioMD(usuario);
            String resultado = "";
            if (usuarioMd.Find(con, tran))
            {
                UsuarioDP usuarioAnterior = UsuarioMD.Load(con, tran, usuario.Pk);
                if (usuarioMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla usuario!";
                    throw new BusinessException("No se pude actualizar la tabla usuario!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (usuarioMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla usuario!";
                    throw new BusinessException("No se pude insertar en la tabla usuario!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, UsuarioDP usuario)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, usuario);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla usuario!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla usuario!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla usuario!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla usuario!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, UsuarioDP usuario)
        {
            String resultado = "";
            UsuarioMD usuarioMd = new UsuarioMD(usuario);
            if (usuarioMd.Find(con, tran))
            {
                if (usuarioMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla usuario!";
                    throw new BusinessException("No se pude eliminar en la tabla usuario!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla usuario!";
                throw new BusinessException("No se pude eliminar en la tabla usuario!");
            }
            return resultado;
        }

        internal static UsuarioDP LoadDetail(DbConnection con, DbTransaction tran, UsuarioDP usuarioAnterior, UsuarioDP usuario) 
        {
            #region FK Persona
            PersonaPk personaPk = new PersonaPk();
            if (usuarioAnterior != null)
            {
                if (personaPk.Equals(usuarioAnterior.Pk))
                    usuario.Persona = usuarioAnterior.Persona;
                else
                    usuario.Persona = PersonaMD.Load(con, tran, personaPk);
            }
            else
                usuario.Persona = PersonaMD.Load(con, tran, personaPk);
            #endregion
            return usuario;
        }
        public static UsuarioDP Load(DbConnection con, UsuarioPk pk)
        {
            UsuarioDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Usuario!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Usuario!", ex);
            }
            return resultado;
        }

        public static UsuarioDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            UsuarioDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Usuario!", ex);
            }
            return resultado;

        }
    }
}
