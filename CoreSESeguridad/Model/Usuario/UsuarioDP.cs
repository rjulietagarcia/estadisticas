using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "Usuario".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Usuario".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>PersonaId</term><description>Descripcion PersonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Login</term><description>Descripcion Login</description>
    ///    </item>
    ///    <item>
    ///        <term>Password</term><description>Descripcion Password</description>
    ///    </item>
    ///    <item>
    ///        <term>BitBloqueado</term><description>Descripcion BitBloqueado</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo2</term><description>Descripcion BitActivo2</description>
    ///    </item>
    ///    <item>
    ///        <term>Usuario2Id</term><description>Descripcion Usuario2Id</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>Persona</term><description>Descripcion Persona</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioDTO usuario = new UsuarioDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("Usuario")]
    public class UsuarioDP
    {
        #region Definicion de campos privados.
        private Int32 usuarioId;
        private Int32 personaId;
        private String login;
        private String password;
        private Boolean bitBloqueado;
        private Boolean bitActivo;
        private Boolean bitActivo2;
        private Int32 usuario2Id;
        private String fechaActualizacion;
        private PersonaDP persona;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// PersonaId
        /// </summary> 
        [XmlElement("PersonaId")]
        public Int32 PersonaId
        {
            get {
                    return personaId; 
            }
            set {
                    personaId = value; 
            }
        }

        /// <summary>
        /// Login
        /// </summary> 
        [XmlElement("Login")]
        public String Login
        {
            get {
                    return login; 
            }
            set {
                    login = value; 
            }
        }

        /// <summary>
        /// Password
        /// </summary> 
        [XmlElement("Password")]
        public String Password
        {
            get {
                    return password; 
            }
            set {
                    password = value; 
            }
        }

        /// <summary>
        /// BitBloqueado
        /// </summary> 
        [XmlElement("BitBloqueado")]
        public Boolean BitBloqueado
        {
            get {
                    return bitBloqueado; 
            }
            set {
                    bitBloqueado = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// BitActivo2
        /// </summary> 
        [XmlElement("BitActivo2")]
        public Boolean BitActivo2
        {
            get {
                    return bitActivo2; 
            }
            set {
                    bitActivo2 = value; 
            }
        }

        /// <summary>
        /// Usuario2Id
        /// </summary> 
        [XmlElement("Usuario2Id")]
        public Int32 Usuario2Id
        {
            get {
                    return usuario2Id; 
            }
            set {
                    usuario2Id = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// Persona
        /// </summary> 
        [XmlElement("Persona")]
        public PersonaDP Persona
        {
            get {
                    return persona; 
            }
            set {
                    persona = value; 
            }
        }

        /// <summary>
        /// Llave primaria de UsuarioPk
        /// </summary>
        [XmlElement("Pk")]
        public UsuarioPk Pk {
            get {
                    return new UsuarioPk( usuarioId );
            }
        }
        #endregion.
    }
}
