using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class UsuarioMD
    {
        private UsuarioDP usuario = null;

        public UsuarioMD(UsuarioDP usuario)
        {
            this.usuario = usuario;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Usuario
            DbCommand com = con.CreateCommand();
            String insertUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Usuario",
                        "Id_Persona",
                        "Login",
                        "Password",
                        "Bit_Bloqueado",
                        "Bit_Activo",
                        "Bit_Activo2",
                        "Id_Usuario2",
                        "Fecha_Actualizacion");
            com.CommandText = insertUsuario;
            #endregion
            #region Parametros Insert Usuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuario.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuario.PersonaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 30, ParameterDirection.Input, usuario.Login);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Password",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 30, ParameterDirection.Input, usuario.Password);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Bloqueado",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuario.BitBloqueado);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuario.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo2",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuario.BitActivo2);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario2",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuario.Usuario2Id);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuario.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Usuario
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertUsuario = String.Format(Usuario.UsuarioResx.UsuarioInsert, "", 
                    usuario.UsuarioId.ToString(),
                    usuario.PersonaId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,usuario.Login),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,usuario.Password),
                    usuario.BitBloqueado.ToString(),
                    usuario.BitActivo.ToString(),
                    usuario.BitActivo2.ToString(),
                    usuario.Usuario2Id.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(usuario.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertUsuario);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Usuario
            DbCommand com = con.CreateCommand();
            String findUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Usuario");
            com.CommandText = findUsuario;
            #endregion
            #region Parametros Find Usuario
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuario.UsuarioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Usuario
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioFind, "", 
                    usuario.UsuarioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindUsuario);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Usuario
            DbCommand com = con.CreateCommand();
            String deleteUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Usuario");
            com.CommandText = deleteUsuario;
            #endregion
            #region Parametros Delete Usuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuario.UsuarioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Usuario
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioDelete, "", 
                    usuario.UsuarioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteUsuario);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Usuario
            DbCommand com = con.CreateCommand();
            String updateUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Persona",
                        "Login",
                        "Password",
                        "Bit_Bloqueado",
                        "Bit_Activo",
                        "Bit_Activo2",
                        "Id_Usuario2",
                        "Fecha_Actualizacion",
                        "Id_Usuario");
            com.CommandText = updateUsuario;
            #endregion
            #region Parametros Update Usuario
            Common.CreateParameter(com, String.Format("{0}Id_Persona",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuario.PersonaId);
            Common.CreateParameter(com, String.Format("{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 30, ParameterDirection.Input, usuario.Login);
            Common.CreateParameter(com, String.Format("{0}Password",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 30, ParameterDirection.Input, usuario.Password);
            Common.CreateParameter(com, String.Format("{0}Bit_Bloqueado",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuario.BitBloqueado);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuario.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo2",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, usuario.BitActivo2);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario2",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuario.Usuario2Id);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(usuario.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, usuario.UsuarioId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Usuario
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateUsuario = String.Format(Usuario.UsuarioResx.UsuarioUpdate, "", 
                    usuario.PersonaId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteString,usuario.Login),
                    String.Format(ConstantesGlobales.ConvierteString,usuario.Password),
                    usuario.BitBloqueado.ToString(),
                    usuario.BitActivo.ToString(),
                    usuario.BitActivo2.ToString(),
                    usuario.Usuario2Id.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(usuario.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    usuario.UsuarioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateUsuario);
                #endregion
            }
            return resultado;
        }
        protected static UsuarioDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            UsuarioDP usuario = new UsuarioDP();
            usuario.UsuarioId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            usuario.PersonaId = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            usuario.Login = dr.IsDBNull(2) ? "" : dr.GetString(2);
            usuario.Password = dr.IsDBNull(3) ? "" : dr.GetString(3);
            usuario.BitBloqueado = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            usuario.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            usuario.BitActivo2 = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            usuario.Usuario2Id = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            usuario.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            return usuario;
            #endregion
        }
        public static UsuarioDP Load(DbConnection con, DbTransaction tran, UsuarioPk pk)
        {
            #region SQL Load Usuario
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Usuario");
            com.CommandText = loadUsuario;
            #endregion
            #region Parametros Load Usuario
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.UsuarioId);
            #endregion
            UsuarioDP usuario;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Usuario
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        usuario = ReadRow(dr);
                    }
                    else
                        usuario = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Usuario
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioSelect, "", 
                    pk.UsuarioId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadUsuario);
                usuario = null;
                #endregion
            }
            return usuario;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Usuario
            DbCommand com = con.CreateCommand();
            String countUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioCount,"");
            com.CommandText = countUsuario;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Usuario
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountUsuario);
                #endregion
            }
            return resultado;
        }
        public static UsuarioDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Usuario
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuario = String.Format(CultureInfo.CurrentCulture, Usuario.UsuarioResx.UsuarioSelectAll, "", ConstantesGlobales.IncluyeRows);
            listUsuario = listUsuario.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuario, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Usuario
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Usuario
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Usuario
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaUsuario = String.Format(CultureInfo.CurrentCulture,Usuario.UsuarioResx.UsuarioSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaUsuario);
                #endregion
            }
            return (UsuarioDP[])lista.ToArray(typeof(UsuarioDP));
        }
    }
}
