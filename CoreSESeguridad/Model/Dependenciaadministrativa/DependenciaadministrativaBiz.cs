using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class DependenciaadministrativaBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, DependenciaadministrativaDP dependenciaadministrativa)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, DependenciaadministrativaDP dependenciaadministrativa)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, dependenciaadministrativa);
                        resultado = InternalSave(con, tran, dependenciaadministrativa);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla dependenciaadministrativa!";
                        throw new BusinessException("Error interno al intentar guardar dependenciaadministrativa!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla dependenciaadministrativa!";
                throw new BusinessException("Error interno al intentar guardar dependenciaadministrativa!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, DependenciaadministrativaDP dependenciaadministrativa)
        {
            DependenciaadministrativaMD dependenciaadministrativaMd = new DependenciaadministrativaMD(dependenciaadministrativa);
            String resultado = "";
            if (dependenciaadministrativaMd.Find(con, tran))
            {
                DependenciaadministrativaDP dependenciaadministrativaAnterior = DependenciaadministrativaMD.Load(con, tran, dependenciaadministrativa.Pk);
                if (dependenciaadministrativaMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla dependenciaadministrativa!";
                    throw new BusinessException("No se pude actualizar la tabla dependenciaadministrativa!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (dependenciaadministrativaMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla dependenciaadministrativa!";
                    throw new BusinessException("No se pude insertar en la tabla dependenciaadministrativa!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, DependenciaadministrativaDP dependenciaadministrativa)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, dependenciaadministrativa);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla dependenciaadministrativa!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla dependenciaadministrativa!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla dependenciaadministrativa!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla dependenciaadministrativa!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, DependenciaadministrativaDP dependenciaadministrativa)
        {
            String resultado = "";
            DependenciaadministrativaMD dependenciaadministrativaMd = new DependenciaadministrativaMD(dependenciaadministrativa);
            if (dependenciaadministrativaMd.Find(con, tran))
            {
                if (dependenciaadministrativaMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla dependenciaadministrativa!";
                    throw new BusinessException("No se pude eliminar en la tabla dependenciaadministrativa!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla dependenciaadministrativa!";
                throw new BusinessException("No se pude eliminar en la tabla dependenciaadministrativa!");
            }
            return resultado;
        }

        internal static DependenciaadministrativaDP LoadDetail(DbConnection con, DbTransaction tran, DependenciaadministrativaDP dependenciaadministrativaAnterior, DependenciaadministrativaDP dependenciaadministrativa) 
        {
            return dependenciaadministrativa;
        }
        public static DependenciaadministrativaDP Load(DbConnection con, DependenciaadministrativaPk pk)
        {
            DependenciaadministrativaDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = DependenciaadministrativaMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Dependenciaadministrativa!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = DependenciaadministrativaMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Dependenciaadministrativa!", ex);
            }
            return resultado;
        }

        public static DependenciaadministrativaDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            DependenciaadministrativaDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = DependenciaadministrativaMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Dependenciaadministrativa!", ex);
            }
            return resultado;

        }
    }
}
