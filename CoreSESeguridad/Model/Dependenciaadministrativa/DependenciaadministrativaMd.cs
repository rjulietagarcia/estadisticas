using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class DependenciaadministrativaMD
    {
        private DependenciaadministrativaDP dependenciaadministrativa = null;

        public DependenciaadministrativaMD(DependenciaadministrativaDP dependenciaadministrativa)
        {
            this.dependenciaadministrativa = dependenciaadministrativa;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Dependenciaadministrativa
            DbCommand com = con.CreateCommand();
            String insertDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "id_dependenciaadministrativa",
                        "nombre",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion");
            com.CommandText = insertDependenciaadministrativa;
            #endregion
            #region Parametros Insert Dependenciaadministrativa
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_dependenciaadministrativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, dependenciaadministrativa.DependenciaadministrativaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, dependenciaadministrativa.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, dependenciaadministrativa.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, dependenciaadministrativa.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(dependenciaadministrativa.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Dependenciaadministrativa
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertDependenciaadministrativa = String.Format(Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaInsert, "", 
                    dependenciaadministrativa.DependenciaadministrativaId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,dependenciaadministrativa.Nombre),
                    dependenciaadministrativa.BitActivo.ToString(),
                    dependenciaadministrativa.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(dependenciaadministrativa.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertDependenciaadministrativa);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Dependenciaadministrativa
            DbCommand com = con.CreateCommand();
            String findDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaFind,
                        ConstantesGlobales.ParameterPrefix,
                        "id_dependenciaadministrativa");
            com.CommandText = findDependenciaadministrativa;
            #endregion
            #region Parametros Find Dependenciaadministrativa
            Common.CreateParameter(com, String.Format("{0}id_dependenciaadministrativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, dependenciaadministrativa.DependenciaadministrativaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Dependenciaadministrativa
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaFind, "", 
                    dependenciaadministrativa.DependenciaadministrativaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindDependenciaadministrativa);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Dependenciaadministrativa
            DbCommand com = con.CreateCommand();
            String deleteDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "id_dependenciaadministrativa");
            com.CommandText = deleteDependenciaadministrativa;
            #endregion
            #region Parametros Delete Dependenciaadministrativa
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_dependenciaadministrativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, dependenciaadministrativa.DependenciaadministrativaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Dependenciaadministrativa
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaDelete, "", 
                    dependenciaadministrativa.DependenciaadministrativaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteDependenciaadministrativa);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Dependenciaadministrativa
            DbCommand com = con.CreateCommand();
            String updateDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "nombre",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion",
                        "id_dependenciaadministrativa");
            com.CommandText = updateDependenciaadministrativa;
            #endregion
            #region Parametros Update Dependenciaadministrativa
            Common.CreateParameter(com, String.Format("{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 100, ParameterDirection.Input, dependenciaadministrativa.Nombre);
            Common.CreateParameter(com, String.Format("{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, dependenciaadministrativa.BitActivo);
            Common.CreateParameter(com, String.Format("{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, dependenciaadministrativa.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(dependenciaadministrativa.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}id_dependenciaadministrativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, dependenciaadministrativa.DependenciaadministrativaId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Dependenciaadministrativa
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateDependenciaadministrativa = String.Format(Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,dependenciaadministrativa.Nombre),
                    dependenciaadministrativa.BitActivo.ToString(),
                    dependenciaadministrativa.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(dependenciaadministrativa.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    dependenciaadministrativa.DependenciaadministrativaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateDependenciaadministrativa);
                #endregion
            }
            return resultado;
        }
        protected static DependenciaadministrativaDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            DependenciaadministrativaDP dependenciaadministrativa = new DependenciaadministrativaDP();
            dependenciaadministrativa.DependenciaadministrativaId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            dependenciaadministrativa.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            dependenciaadministrativa.BitActivo = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            dependenciaadministrativa.UsuarioId = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            dependenciaadministrativa.FechaActualizacion = dr.IsDBNull(4) ? "" : dr.GetDateTime(4).ToShortDateString();;
            return dependenciaadministrativa;
            #endregion
        }
        public static DependenciaadministrativaDP Load(DbConnection con, DbTransaction tran, DependenciaadministrativaPk pk)
        {
            #region SQL Load Dependenciaadministrativa
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "id_dependenciaadministrativa");
            com.CommandText = loadDependenciaadministrativa;
            #endregion
            #region Parametros Load Dependenciaadministrativa
            Common.CreateParameter(com, String.Format("{0}id_dependenciaadministrativa",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.DependenciaadministrativaId);
            #endregion
            DependenciaadministrativaDP dependenciaadministrativa;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Dependenciaadministrativa
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        dependenciaadministrativa = ReadRow(dr);
                    }
                    else
                        dependenciaadministrativa = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Dependenciaadministrativa
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaSelect, "", 
                    pk.DependenciaadministrativaId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadDependenciaadministrativa);
                dependenciaadministrativa = null;
                #endregion
            }
            return dependenciaadministrativa;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Dependenciaadministrativa
            DbCommand com = con.CreateCommand();
            String countDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaCount,"");
            com.CommandText = countDependenciaadministrativa;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Dependenciaadministrativa
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountDependenciaadministrativa);
                #endregion
            }
            return resultado;
        }
        public static DependenciaadministrativaDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Dependenciaadministrativa
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture, Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaSelectAll, "", ConstantesGlobales.IncluyeRows);
            listDependenciaadministrativa = listDependenciaadministrativa.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listDependenciaadministrativa, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Dependenciaadministrativa
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Dependenciaadministrativa
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Dependenciaadministrativa
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaDependenciaadministrativa = String.Format(CultureInfo.CurrentCulture,Dependenciaadministrativa.DependenciaadministrativaResx.DependenciaadministrativaSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaDependenciaadministrativa);
                #endregion
            }
            return (DependenciaadministrativaDP[])lista.ToArray(typeof(DependenciaadministrativaDP));
        }
    }
}
