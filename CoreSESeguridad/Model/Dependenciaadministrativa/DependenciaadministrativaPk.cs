using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "DependenciaadministrativaPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 04:32:07 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "DependenciaadministrativaPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>DependenciaadministrativaId</term><description>Descripcion DependenciaadministrativaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "DependenciaadministrativaPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// DependenciaadministrativaPk dependenciaadministrativaPk = new DependenciaadministrativaPk(dependenciaadministrativa);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("DependenciaadministrativaPk")]
    public class DependenciaadministrativaPk
    {
        #region Definicion de campos privados.
        private Int16 dependenciaadministrativaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// DependenciaadministrativaId
        /// </summary> 
        [XmlElement("DependenciaadministrativaId")]
        public Int16 DependenciaadministrativaId
        {
            get {
                    return dependenciaadministrativaId; 
            }
            set {
                    dependenciaadministrativaId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de DependenciaadministrativaPk.
        /// </summary>
        /// <param name="dependenciaadministrativaId">Descripción dependenciaadministrativaId del tipo Int16.</param>
        public DependenciaadministrativaPk(Int16 dependenciaadministrativaId) 
        {
            this.dependenciaadministrativaId = dependenciaadministrativaId;
        }
        /// <summary>
        /// Constructor normal de DependenciaadministrativaPk.
        /// </summary>
        public DependenciaadministrativaPk() 
        {
        }
        #endregion.
    }
}
