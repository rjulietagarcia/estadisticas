using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "TurnoPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: miércoles, 10 de junio de 2009.</Para>
    /// <Para>Hora: 04:54:35 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "TurnoPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TurnoId</term><description>Descripcion TurnoId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TurnoPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// TurnoPk turnoPk = new TurnoPk(turno);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("TurnoPk")]
    public class TurnoPk
    {
        #region Definicion de campos privados.
        private Int16 turnoId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TurnoId
        /// </summary> 
        [XmlElement("TurnoId")]
        public Int16 TurnoId
        {
            get {
                    return turnoId; 
            }
            set {
                    turnoId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de TurnoPk.
        /// </summary>
        /// <param name="turnoId">Descripción turnoId del tipo Int16.</param>
        public TurnoPk(Int16 turnoId) 
        {
            this.turnoId = turnoId;
        }
        /// <summary>
        /// Constructor normal de TurnoPk.
        /// </summary>
        public TurnoPk() 
        {
        }
        #endregion.
    }
}
