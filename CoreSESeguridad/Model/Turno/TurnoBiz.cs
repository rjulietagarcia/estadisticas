using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class TurnoBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, TurnoDP turno)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, TurnoDP turno)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, turno);
                        resultado = InternalSave(con, tran, turno);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla turno!";
                        throw new BusinessException("Error interno al intentar guardar turno!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla turno!";
                throw new BusinessException("Error interno al intentar guardar turno!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, TurnoDP turno)
        {
            TurnoMD turnoMd = new TurnoMD(turno);
            String resultado = "";
            if (turnoMd.Find(con, tran))
            {
                TurnoDP turnoAnterior = TurnoMD.Load(con, tran, turno.Pk);
                if (turnoMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla turno!";
                    throw new BusinessException("No se pude actualizar la tabla turno!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (turnoMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla turno!";
                    throw new BusinessException("No se pude insertar en la tabla turno!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, TurnoDP turno)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, turno);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla turno!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla turno!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla turno!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla turno!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, TurnoDP turno)
        {
            String resultado = "";
            TurnoMD turnoMd = new TurnoMD(turno);
            if (turnoMd.Find(con, tran))
            {
                if (turnoMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla turno!";
                    throw new BusinessException("No se pude eliminar en la tabla turno!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla turno!";
                throw new BusinessException("No se pude eliminar en la tabla turno!");
            }
            return resultado;
        }

        internal static TurnoDP LoadDetail(DbConnection con, DbTransaction tran, TurnoDP turnoAnterior, TurnoDP turno) 
        {
            return turno;
        }
        public static TurnoDP Load(DbConnection con, TurnoPk pk)
        {
            TurnoDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TurnoMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Turno!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TurnoMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Turno!", ex);
            }
            return resultado;
        }

        public static TurnoDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TurnoDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TurnoMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Turno!", ex);
            }
            return resultado;

        }
    }
}
