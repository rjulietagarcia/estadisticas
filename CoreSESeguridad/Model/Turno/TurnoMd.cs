using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class TurnoMD
    {
        private TurnoDP turno = null;

        public TurnoMD(TurnoDP turno)
        {
            this.turno = turno;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Turno
            DbCommand com = con.CreateCommand();
            String insertTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Turno",
                        "Nombre",
                        "Abreviatura",
                        "Hora_Inicio",
                        "Hora_Fin",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertTurno;
            #endregion
            #region Parametros Insert Turno
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turno.TurnoId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, turno.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, turno.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Hora_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(turno.HoraInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Hora_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(turno.HoraFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, turno.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, turno.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(turno.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Turno
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertTurno = String.Format(Turno.TurnoResx.TurnoInsert, "", 
                    turno.TurnoId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,turno.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,turno.Abreviatura),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(turno.HoraInicio).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(turno.HoraFin).ToString(ConstantesGlobales.FormatoFecha)),
                    turno.BitActivo.ToString(),
                    turno.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(turno.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertTurno);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Turno
            DbCommand com = con.CreateCommand();
            String findTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Turno");
            com.CommandText = findTurno;
            #endregion
            #region Parametros Find Turno
            Common.CreateParameter(com, String.Format("{0}Id_Turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turno.TurnoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Turno
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoFind, "", 
                    turno.TurnoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindTurno);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Turno
            DbCommand com = con.CreateCommand();
            String deleteTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Turno");
            com.CommandText = deleteTurno;
            #endregion
            #region Parametros Delete Turno
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turno.TurnoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Turno
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoDelete, "", 
                    turno.TurnoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteTurno);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Turno
            DbCommand com = con.CreateCommand();
            String updateTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Hora_Inicio",
                        "Hora_Fin",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Turno");
            com.CommandText = updateTurno;
            #endregion
            #region Parametros Update Turno
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, turno.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, turno.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Hora_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(turno.HoraInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Hora_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(turno.HoraFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, turno.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, turno.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(turno.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, turno.TurnoId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Turno
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateTurno = String.Format(Turno.TurnoResx.TurnoUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,turno.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,turno.Abreviatura),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(turno.HoraInicio).ToString(ConstantesGlobales.FormatoFecha)),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(turno.HoraFin).ToString(ConstantesGlobales.FormatoFecha)),
                    turno.BitActivo.ToString(),
                    turno.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(turno.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    turno.TurnoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateTurno);
                #endregion
            }
            return resultado;
        }
        protected static TurnoDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TurnoDP turno = new TurnoDP();
            turno.TurnoId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            turno.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            turno.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            turno.HoraInicio = dr.IsDBNull(3) ? "" : dr.GetDateTime(3).ToShortDateString();;
            turno.HoraFin = dr.IsDBNull(4) ? "" : dr.GetDateTime(4).ToShortDateString();;
            turno.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            turno.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            turno.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            return turno;
            #endregion
        }
        public static TurnoDP Load(DbConnection con, DbTransaction tran, TurnoPk pk)
        {
            #region SQL Load Turno
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Turno");
            com.CommandText = loadTurno;
            #endregion
            #region Parametros Load Turno
            Common.CreateParameter(com, String.Format("{0}Id_Turno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.TurnoId);
            #endregion
            TurnoDP turno;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Turno
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        turno = ReadRow(dr);
                    }
                    else
                        turno = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Turno
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoSelect, "", 
                    pk.TurnoId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadTurno);
                turno = null;
                #endregion
            }
            return turno;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Turno
            DbCommand com = con.CreateCommand();
            String countTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoCount,"");
            com.CommandText = countTurno;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Turno
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountTurno);
                #endregion
            }
            return resultado;
        }
        public static TurnoDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Turno
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTurno = String.Format(CultureInfo.CurrentCulture, Turno.TurnoResx.TurnoSelectAll, "", ConstantesGlobales.IncluyeRows);
            listTurno = listTurno.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTurno, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Turno
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Turno
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Turno
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaTurno = String.Format(CultureInfo.CurrentCulture,Turno.TurnoResx.TurnoSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaTurno);
                #endregion
            }
            return (TurnoDP[])lista.ToArray(typeof(TurnoDP));
        }
    }
}
