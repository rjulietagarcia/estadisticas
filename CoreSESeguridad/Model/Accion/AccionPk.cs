using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "AccionPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 29 de junio de 2009.</Para>
    /// <Para>Hora: 03:59:55 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "AccionPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    ///    <item>
    ///        <term>OpcionId</term><description>Descripcion OpcionId</description>
    ///    </item>
    ///    <item>
    ///        <term>AccionId</term><description>Descripcion AccionId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "AccionPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// AccionPk accionPk = new AccionPk(accion);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("AccionPk")]
    public class AccionPk
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Byte moduloId;
        private Byte opcionId;
        private Byte accionId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        /// <summary>
        /// OpcionId
        /// </summary> 
        [XmlElement("OpcionId")]
        public Byte OpcionId
        {
            get {
                    return opcionId; 
            }
            set {
                    opcionId = value; 
            }
        }

        /// <summary>
        /// AccionId
        /// </summary> 
        [XmlElement("AccionId")]
        public Byte AccionId
        {
            get {
                    return accionId; 
            }
            set {
                    accionId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de AccionPk.
        /// </summary>
        /// <param name="sistemaId">Descripción sistemaId del tipo Byte.</param>
        /// <param name="moduloId">Descripción moduloId del tipo Byte.</param>
        /// <param name="opcionId">Descripción opcionId del tipo Byte.</param>
        /// <param name="accionId">Descripción accionId del tipo Byte.</param>
        public AccionPk(Byte sistemaId, Byte moduloId, Byte opcionId, Byte accionId) 
        {
            this.sistemaId = sistemaId;
            this.moduloId = moduloId;
            this.opcionId = opcionId;
            this.accionId = accionId;
        }
        /// <summary>
        /// Constructor normal de AccionPk.
        /// </summary>
        public AccionPk() 
        {
        }
        #endregion.
    }
}
