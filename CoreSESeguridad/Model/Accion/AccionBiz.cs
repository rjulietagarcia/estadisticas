using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class AccionBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, AccionDP accion)
        {
            #region SaveDetail
            #region FK Opcion
            OpcionDP opcion = accion.Opcion;
            if (opcion != null) 
            {
                OpcionMD opcionMd = new OpcionMD(opcion);
                if (!opcionMd.Find(con, tran))
                    opcionMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, AccionDP accion)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, accion);
                        resultado = InternalSave(con, tran, accion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla accion!";
                        throw new BusinessException("Error interno al intentar guardar accion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla accion!";
                throw new BusinessException("Error interno al intentar guardar accion!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, AccionDP accion)
        {
            AccionMD accionMd = new AccionMD(accion);
            String resultado = "";
            if (accionMd.Find(con, tran))
            {
                AccionDP accionAnterior = AccionMD.Load(con, tran, accion.Pk);
                if (accionMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla accion!";
                    throw new BusinessException("No se pude actualizar la tabla accion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (accionMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla accion!";
                    throw new BusinessException("No se pude insertar en la tabla accion!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, AccionDP accion)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, accion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla accion!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla accion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla accion!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla accion!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, AccionDP accion)
        {
            String resultado = "";
            AccionMD accionMd = new AccionMD(accion);
            if (accionMd.Find(con, tran))
            {
                if (accionMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla accion!";
                    throw new BusinessException("No se pude eliminar en la tabla accion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla accion!";
                throw new BusinessException("No se pude eliminar en la tabla accion!");
            }
            return resultado;
        }

        internal static AccionDP LoadDetail(DbConnection con, DbTransaction tran, AccionDP accionAnterior, AccionDP accion) 
        {
            #region FK Opcion
            OpcionPk opcionPk = new OpcionPk();
            if (accionAnterior != null)
            {
                if (opcionPk.Equals(accionAnterior.Pk))
                    accion.Opcion = accionAnterior.Opcion;
                else
                    accion.Opcion = OpcionMD.Load(con, tran, opcionPk);
            }
            else
                accion.Opcion = OpcionMD.Load(con, tran, opcionPk);
            #endregion
            return accion;
        }
        public static AccionDP Load(DbConnection con, AccionPk pk)
        {
            AccionDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = AccionMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Accion!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = AccionMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Accion!", ex);
            }
            return resultado;
        }

        public static AccionDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            AccionDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = AccionMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Accion!", ex);
            }
            return resultado;

        }
    }
}
