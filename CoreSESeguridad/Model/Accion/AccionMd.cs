using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class AccionMD
    {
        private AccionDP accion = null;

        public AccionMD(AccionDP accion)
        {
            this.accion = accion;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Accion
            DbCommand com = con.CreateCommand();
            String insertAccion = String.Format(CultureInfo.CurrentCulture,Accion.AccionResx.AccionInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Inicio",
                        "Fecha_Fin");
            com.CommandText = insertAccion;
            #endregion
            #region Parametros Insert Accion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.AccionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, accion.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, accion.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, accion.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, accion.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(accion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(accion.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(accion.FechaFin,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Accion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Accion
            DbCommand com = con.CreateCommand();
            String findAccion = String.Format(CultureInfo.CurrentCulture,Accion.AccionResx.AccionFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion");
            com.CommandText = findAccion;
            #endregion
            #region Parametros Find Accion
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.AccionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Accion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Accion
            DbCommand com = con.CreateCommand();
            String deleteAccion = String.Format(CultureInfo.CurrentCulture,Accion.AccionResx.AccionDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion");
            com.CommandText = deleteAccion;
            #endregion
            #region Parametros Delete Accion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.SistemaId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.ModuloId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.OpcionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.AccionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Accion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Accion
            DbCommand com = con.CreateCommand();
            String updateAccion = String.Format(CultureInfo.CurrentCulture,Accion.AccionResx.AccionUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Fecha_Inicio",
                        "Fecha_Fin",
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion");
            com.CommandText = updateAccion;
            #endregion
            #region Parametros Update Accion
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, accion.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, accion.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, accion.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, accion.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(accion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Inicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(accion.FechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Fecha_Fin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(accion.FechaFin,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, accion.AccionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Accion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        protected static AccionDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            AccionDP accion = new AccionDP();
            accion.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            accion.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            accion.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            accion.AccionId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            accion.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            accion.Abreviatura = dr.IsDBNull(5) ? "" : dr.GetString(5);
            accion.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            accion.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            accion.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            accion.FechaInicio = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            accion.FechaFin = dr.IsDBNull(10) ? "" : dr.GetDateTime(10).ToShortDateString();;
            return accion;
            #endregion
        }
        public static AccionDP Load(DbConnection con, DbTransaction tran, AccionPk pk)
        {
            #region SQL Load Accion
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadAccion = String.Format(CultureInfo.CurrentCulture,Accion.AccionResx.AccionSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Sistema",
                        "Id_Modulo",
                        "Id_Opcion",
                        "Id_Accion");
            com.CommandText = loadAccion;
            #endregion
            #region Parametros Load Accion
            Common.CreateParameter(com, String.Format("{0}Id_Sistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.SistemaId);
            Common.CreateParameter(com, String.Format("{0}Id_Modulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.ModuloId);
            Common.CreateParameter(com, String.Format("{0}Id_Opcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.OpcionId);
            Common.CreateParameter(com, String.Format("{0}Id_Accion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, pk.AccionId);
            #endregion
            AccionDP accion;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Accion
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        accion = ReadRow(dr);
                    }
                    else
                        accion = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Accion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return accion;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Accion
            DbCommand com = con.CreateCommand();
            String countAccion = String.Format(CultureInfo.CurrentCulture,Accion.AccionResx.AccionCount,"");
            com.CommandText = countAccion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Accion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static AccionDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Accion
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listAccion = String.Format(CultureInfo.CurrentCulture, Accion.AccionResx.AccionSelectAll, "", ConstantesGlobales.IncluyeRows);
            listAccion = listAccion.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listAccion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Accion
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Accion
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Accion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (AccionDP[])lista.ToArray(typeof(AccionDP));
        }
    }
}
