using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "TipoeducacionPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "TipoeducacionPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TipoeducacionPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// TipoeducacionPk tipoeducacionPk = new TipoeducacionPk(tipoeducacion);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("TipoeducacionPk")]
    public class TipoeducacionPk
    {
        #region Definicion de campos privados.
        private Int16 tipoeducacionId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public Int16 TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de TipoeducacionPk.
        /// </summary>
        /// <param name="tipoeducacionId">Descripción tipoeducacionId del tipo Int16.</param>
        public TipoeducacionPk(Int16 tipoeducacionId) 
        {
            this.tipoeducacionId = tipoeducacionId;
        }
        /// <summary>
        /// Constructor normal de TipoeducacionPk.
        /// </summary>
        public TipoeducacionPk() 
        {
        }
        #endregion.
    }
}
