using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class TipoeducacionMD
    {
        private TipoeducacionDP tipoeducacion = null;

        public TipoeducacionMD(TipoeducacionDP tipoeducacion)
        {
            this.tipoeducacion = tipoeducacion;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Tipoeducacion
            DbCommand com = con.CreateCommand();
            String insertTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "nombre",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion");
            com.CommandText = insertTipoeducacion;
            #endregion
            #region Parametros Insert Tipoeducacion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, tipoeducacion.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, tipoeducacion.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, tipoeducacion.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, tipoeducacion.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(tipoeducacion.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Tipoeducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertTipoeducacion = String.Format(Tipoeducacion.TipoeducacionResx.TipoeducacionInsert, "", 
                    tipoeducacion.TipoeducacionId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,tipoeducacion.Nombre),
                    tipoeducacion.BitActivo.ToString(),
                    tipoeducacion.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(tipoeducacion.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertTipoeducacion);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Tipoeducacion
            DbCommand com = con.CreateCommand();
            String findTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionFind,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion");
            com.CommandText = findTipoeducacion;
            #endregion
            #region Parametros Find Tipoeducacion
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, tipoeducacion.TipoeducacionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Tipoeducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionFind, "", 
                    tipoeducacion.TipoeducacionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindTipoeducacion);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Tipoeducacion
            DbCommand com = con.CreateCommand();
            String deleteTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion");
            com.CommandText = deleteTipoeducacion;
            #endregion
            #region Parametros Delete Tipoeducacion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, tipoeducacion.TipoeducacionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Tipoeducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionDelete, "", 
                    tipoeducacion.TipoeducacionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteTipoeducacion);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Tipoeducacion
            DbCommand com = con.CreateCommand();
            String updateTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "nombre",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion",
                        "id_tipoeducacion");
            com.CommandText = updateTipoeducacion;
            #endregion
            #region Parametros Update Tipoeducacion
            Common.CreateParameter(com, String.Format("{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, tipoeducacion.Nombre);
            Common.CreateParameter(com, String.Format("{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, tipoeducacion.BitActivo);
            Common.CreateParameter(com, String.Format("{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, tipoeducacion.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(tipoeducacion.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, tipoeducacion.TipoeducacionId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Tipoeducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateTipoeducacion = String.Format(Tipoeducacion.TipoeducacionResx.TipoeducacionUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,tipoeducacion.Nombre),
                    tipoeducacion.BitActivo.ToString(),
                    tipoeducacion.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(tipoeducacion.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    tipoeducacion.TipoeducacionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateTipoeducacion);
                #endregion
            }
            return resultado;
        }
        protected static TipoeducacionDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TipoeducacionDP tipoeducacion = new TipoeducacionDP();
            tipoeducacion.TipoeducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            tipoeducacion.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            tipoeducacion.BitActivo = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            tipoeducacion.UsuarioId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            tipoeducacion.FechaActualizacion = dr.IsDBNull(4) ? "" : dr.GetDateTime(4).ToShortDateString();;
            return tipoeducacion;
            #endregion
        }
        public static TipoeducacionDP Load(DbConnection con, DbTransaction tran, TipoeducacionPk pk)
        {
            #region SQL Load Tipoeducacion
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion");
            com.CommandText = loadTipoeducacion;
            #endregion
            #region Parametros Load Tipoeducacion
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.TipoeducacionId);
            #endregion
            TipoeducacionDP tipoeducacion;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Tipoeducacion
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        tipoeducacion = ReadRow(dr);
                    }
                    else
                        tipoeducacion = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Tipoeducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionSelect, "", 
                    pk.TipoeducacionId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadTipoeducacion);
                tipoeducacion = null;
                #endregion
            }
            return tipoeducacion;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Tipoeducacion
            DbCommand com = con.CreateCommand();
            String countTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionCount,"");
            com.CommandText = countTipoeducacion;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Tipoeducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountTipoeducacion);
                #endregion
            }
            return resultado;
        }
        public static TipoeducacionDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Tipoeducacion
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTipoeducacion = String.Format(CultureInfo.CurrentCulture, Tipoeducacion.TipoeducacionResx.TipoeducacionSelectAll, "", ConstantesGlobales.IncluyeRows);
            listTipoeducacion = listTipoeducacion.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTipoeducacion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Tipoeducacion
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Tipoeducacion
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Tipoeducacion
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaTipoeducacion = String.Format(CultureInfo.CurrentCulture,Tipoeducacion.TipoeducacionResx.TipoeducacionSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaTipoeducacion);
                #endregion
            }
            return (TipoeducacionDP[])lista.ToArray(typeof(TipoeducacionDP));
        }
    }
}
