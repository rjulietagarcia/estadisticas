using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class TipoeducacionBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, TipoeducacionDP tipoeducacion)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, TipoeducacionDP tipoeducacion)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, tipoeducacion);
                        resultado = InternalSave(con, tran, tipoeducacion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla tipoeducacion!";
                        throw new BusinessException("Error interno al intentar guardar tipoeducacion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla tipoeducacion!";
                throw new BusinessException("Error interno al intentar guardar tipoeducacion!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, TipoeducacionDP tipoeducacion)
        {
            TipoeducacionMD tipoeducacionMd = new TipoeducacionMD(tipoeducacion);
            String resultado = "";
            if (tipoeducacionMd.Find(con, tran))
            {
                TipoeducacionDP tipoeducacionAnterior = TipoeducacionMD.Load(con, tran, tipoeducacion.Pk);
                if (tipoeducacionMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla tipoeducacion!";
                    throw new BusinessException("No se pude actualizar la tabla tipoeducacion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (tipoeducacionMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla tipoeducacion!";
                    throw new BusinessException("No se pude insertar en la tabla tipoeducacion!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, TipoeducacionDP tipoeducacion)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, tipoeducacion);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla tipoeducacion!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla tipoeducacion!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla tipoeducacion!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla tipoeducacion!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, TipoeducacionDP tipoeducacion)
        {
            String resultado = "";
            TipoeducacionMD tipoeducacionMd = new TipoeducacionMD(tipoeducacion);
            if (tipoeducacionMd.Find(con, tran))
            {
                if (tipoeducacionMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla tipoeducacion!";
                    throw new BusinessException("No se pude eliminar en la tabla tipoeducacion!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla tipoeducacion!";
                throw new BusinessException("No se pude eliminar en la tabla tipoeducacion!");
            }
            return resultado;
        }

        internal static TipoeducacionDP LoadDetail(DbConnection con, DbTransaction tran, TipoeducacionDP tipoeducacionAnterior, TipoeducacionDP tipoeducacion) 
        {
            return tipoeducacion;
        }
        public static TipoeducacionDP Load(DbConnection con, TipoeducacionPk pk)
        {
            TipoeducacionDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TipoeducacionMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Tipoeducacion!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TipoeducacionMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Tipoeducacion!", ex);
            }
            return resultado;
        }

        public static TipoeducacionDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TipoeducacionDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TipoeducacionMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Tipoeducacion!", ex);
            }
            return resultado;

        }
    }
}
