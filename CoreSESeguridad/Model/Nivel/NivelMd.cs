using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class NivelMD
    {
        private NivelDP nivel = null;

        public NivelMD(NivelDP nivel)
        {
            this.nivel = nivel;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Nivel
            DbCommand com = con.CreateCommand();
            String insertNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel",
                        "nombre",
                        "letra_folio",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion");
            com.CommandText = insertNivel;
            #endregion
            #region Parametros Insert Nivel
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.NivelId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, nivel.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}letra_folio",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, nivel.LetraFolio);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, nivel.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(nivel.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Nivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertNivel = String.Format(Nivel.NivelResx.NivelInsert, "", 
                    nivel.TipoeducacionId.ToString(),
                    nivel.NivelId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,nivel.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,nivel.LetraFolio),
                    nivel.BitActivo.ToString(),
                    nivel.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(nivel.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertNivel);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Nivel
            DbCommand com = con.CreateCommand();
            String findNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelFind,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel");
            com.CommandText = findNivel;
            #endregion
            #region Parametros Find Nivel
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.NivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Nivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelFind, "", 
                    nivel.TipoeducacionId.ToString(),
                    nivel.NivelId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindNivel);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Nivel
            DbCommand com = con.CreateCommand();
            String deleteNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel");
            com.CommandText = deleteNivel;
            #endregion
            #region Parametros Delete Nivel
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.NivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Nivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeleteNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelDelete, "", 
                    nivel.TipoeducacionId.ToString(),
                    nivel.NivelId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeleteNivel);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Nivel
            DbCommand com = con.CreateCommand();
            String updateNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "nombre",
                        "letra_folio",
                        "bit_activo",
                        "id_usuario",
                        "fecha_actualizacion",
                        "id_tipoeducacion",
                        "id_nivel");
            com.CommandText = updateNivel;
            #endregion
            #region Parametros Update Nivel
            Common.CreateParameter(com, String.Format("{0}nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, nivel.Nombre);
            Common.CreateParameter(com, String.Format("{0}letra_folio",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, nivel.LetraFolio);
            Common.CreateParameter(com, String.Format("{0}bit_activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, nivel.BitActivo);
            Common.CreateParameter(com, String.Format("{0}id_usuario",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}fecha_actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(nivel.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, nivel.NivelId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Nivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdateNivel = String.Format(Nivel.NivelResx.NivelUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,nivel.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,nivel.LetraFolio),
                    nivel.BitActivo.ToString(),
                    nivel.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(nivel.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    nivel.TipoeducacionId.ToString(),
                    nivel.NivelId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdateNivel);
                #endregion
            }
            return resultado;
        }
        protected static NivelDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            NivelDP nivel = new NivelDP();
            nivel.TipoeducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            nivel.NivelId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            nivel.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            nivel.LetraFolio = dr.IsDBNull(3) ? "" : dr.GetString(3);
            nivel.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            nivel.UsuarioId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            nivel.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            return nivel;
            #endregion
        }
        public static NivelDP Load(DbConnection con, DbTransaction tran, NivelPk pk)
        {
            #region SQL Load Nivel
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "id_tipoeducacion",
                        "id_nivel");
            com.CommandText = loadNivel;
            #endregion
            #region Parametros Load Nivel
            Common.CreateParameter(com, String.Format("{0}id_tipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.TipoeducacionId);
            Common.CreateParameter(com, String.Format("{0}id_nivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.NivelId);
            #endregion
            NivelDP nivel;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Nivel
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        nivel = ReadRow(dr);
                    }
                    else
                        nivel = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Nivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelSelect, "", 
                    pk.TipoeducacionId.ToString(),
                    pk.NivelId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadNivel);
                nivel = null;
                #endregion
            }
            return nivel;
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Nivel
            DbCommand com = con.CreateCommand();
            String countNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelCount,"");
            com.CommandText = countNivel;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Nivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountNivel);
                #endregion
            }
            return resultado;
        }
        public static NivelDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Nivel
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNivel = String.Format(CultureInfo.CurrentCulture, Nivel.NivelResx.NivelSelectAll, "", ConstantesGlobales.IncluyeRows);
            listNivel = listNivel.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listNivel, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Nivel
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Nivel
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Nivel
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaNivel = String.Format(CultureInfo.CurrentCulture,Nivel.NivelResx.NivelSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaNivel);
                #endregion
            }
            return (NivelDP[])lista.ToArray(typeof(NivelDP));
        }
    }
}
