using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class NivelBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, NivelDP nivel)
        {
            #region SaveDetail
            #region FK Tipoeducacion
            TipoeducacionDP tipoeducacion = nivel.Tipoeducacion;
            if (tipoeducacion != null) 
            {
                TipoeducacionMD tipoeducacionMd = new TipoeducacionMD(tipoeducacion);
                if (!tipoeducacionMd.Find(con, tran))
                    tipoeducacionMd.Insert(con, tran);
            }
            #endregion
            #endregion
        }
        public static String Save(DbConnection con, NivelDP nivel)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, nivel);
                        resultado = InternalSave(con, tran, nivel);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla nivel!";
                        throw new BusinessException("Error interno al intentar guardar nivel!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla nivel!";
                throw new BusinessException("Error interno al intentar guardar nivel!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, NivelDP nivel)
        {
            NivelMD nivelMd = new NivelMD(nivel);
            String resultado = "";
            if (nivelMd.Find(con, tran))
            {
                NivelDP nivelAnterior = NivelMD.Load(con, tran, nivel.Pk);
                if (nivelMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla nivel!";
                    throw new BusinessException("No se pude actualizar la tabla nivel!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (nivelMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla nivel!";
                    throw new BusinessException("No se pude insertar en la tabla nivel!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, NivelDP nivel)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, nivel);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla nivel!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla nivel!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla nivel!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla nivel!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, NivelDP nivel)
        {
            String resultado = "";
            NivelMD nivelMd = new NivelMD(nivel);
            if (nivelMd.Find(con, tran))
            {
                if (nivelMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla nivel!";
                    throw new BusinessException("No se pude eliminar en la tabla nivel!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla nivel!";
                throw new BusinessException("No se pude eliminar en la tabla nivel!");
            }
            return resultado;
        }

        internal static NivelDP LoadDetail(DbConnection con, DbTransaction tran, NivelDP nivelAnterior, NivelDP nivel) 
        {
            #region FK Tipoeducacion
            TipoeducacionPk tipoeducacionPk = new TipoeducacionPk();
            if (nivelAnterior != null)
            {
                if (tipoeducacionPk.Equals(nivelAnterior.Pk))
                    nivel.Tipoeducacion = nivelAnterior.Tipoeducacion;
                else
                    nivel.Tipoeducacion = TipoeducacionMD.Load(con, tran, tipoeducacionPk);
            }
            else
                nivel.Tipoeducacion = TipoeducacionMD.Load(con, tran, tipoeducacionPk);
            #endregion
            return nivel;
        }
        public static NivelDP Load(DbConnection con, NivelPk pk)
        {
            NivelDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = NivelMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Nivel!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = NivelMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Nivel!", ex);
            }
            return resultado;
        }

        public static NivelDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            NivelDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = NivelMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Nivel!", ex);
            }
            return resultado;

        }
    }
}
