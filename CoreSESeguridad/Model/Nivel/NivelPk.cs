using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "NivelPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "NivelPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "NivelPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// NivelPk nivelPk = new NivelPk(nivel);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("NivelPk")]
    public class NivelPk
    {
        #region Definicion de campos privados.
        private Int16 tipoeducacionId;
        private Int16 nivelId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public Int16 TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public Int16 NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de NivelPk.
        /// </summary>
        /// <param name="tipoeducacionId">Descripción tipoeducacionId del tipo Int16.</param>
        /// <param name="nivelId">Descripción nivelId del tipo Int16.</param>
        public NivelPk(Int16 tipoeducacionId, Int16 nivelId) 
        {
            this.tipoeducacionId = tipoeducacionId;
            this.nivelId = nivelId;
        }
        /// <summary>
        /// Constructor normal de NivelPk.
        /// </summary>
        public NivelPk() 
        {
        }
        #endregion.
    }
}
