using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    public class PerfilBiz
    {
        internal static void SaveDetail(DbConnection con, DbTransaction tran, PerfilDP perfil)
        {
            #region SaveDetail
            #endregion
        }
        public static String Save(DbConnection con, PerfilDP perfil)
        {
            #region Save
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        SaveDetail(con, tran, perfil);
                        resultado = InternalSave(con, tran, perfil);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar guardar la tabla perfil!";
                        throw new BusinessException("Error interno al intentar guardar perfil!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar guardar la tabla perfil!";
                throw new BusinessException("Error interno al intentar guardar perfil!", ex);
            }
            return resultado;
            #endregion
        }

        internal static String InternalSave(DbConnection con, DbTransaction tran, PerfilDP perfil)
        {
            PerfilMD perfilMd = new PerfilMD(perfil);
            String resultado = "";
            if (perfilMd.Find(con, tran))
            {
                PerfilDP perfilAnterior = PerfilMD.Load(con, tran, perfil.Pk);
                if (perfilMd.Update(con, tran) != 1)
                {
                    resultado = "No se pude actualizar la tabla perfil!";
                    throw new BusinessException("No se pude actualizar la tabla perfil!");
                }
                else
                    resultado = "1";
            }
            else
            {
                if (perfilMd.Insert(con, tran) != 1)
                {
                    resultado = "No se pude insertar en la tabla perfil!";
                    throw new BusinessException("No se pude insertar en la tabla perfil!");
                }
            }
            return resultado;
        }

        public static String Delete(DbConnection con, PerfilDP perfil)
        {
            String resultado = "";
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    try
                    {
                        resultado = InternalDelete(con, tran, perfil);
                        tran.Commit();
                    }
                    catch (DbException ex)
                    {
                        tran.Rollback();
                        resultado = "Error interno al intentar eliminar en la tabla perfil!";
                        throw new BusinessException("Error interno al intentar eliminar en la tabla perfil!", ex);
                    }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                resultado = "Error interno al intentar eliminar en la tabla perfil!";
                throw new BusinessException("Error interno al intentar eliminar en la tabla perfil!", ex);
            }
            return resultado;
        }

        internal static String InternalDelete(DbConnection con, DbTransaction tran, PerfilDP perfil)
        {
            String resultado = "";
            PerfilMD perfilMd = new PerfilMD(perfil);
            if (perfilMd.Find(con, tran))
            {
                if (perfilMd.Delete(con, tran) != 1)
                {
                    resultado = "No se pude eliminar en la tabla perfil!";
                    throw new BusinessException("No se pude eliminar en la tabla perfil!");
                }
                else
                    resultado = "1";
            }
            else
            {
                resultado = "No se pude eliminar en la tabla perfil!";
                throw new BusinessException("No se pude eliminar en la tabla perfil!");
            }
            return resultado;
        }

        internal static PerfilDP LoadDetail(DbConnection con, DbTransaction tran, PerfilDP perfilAnterior, PerfilDP perfil) 
        {
            return perfil;
        }
        public static PerfilDP Load(DbConnection con, PerfilPk pk)
        {
            PerfilDP resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilMD.Load(con, tran, pk);
                    if (resultado != null)
                        resultado = LoadDetail(con, tran, null, resultado);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener un Perfil!", ex);
            }
            return resultado;
        }

/*        internal static PerfilDP LoadDetail(DbConnection con, DbTransaction tran, PerfilDP perfilAnterior, PerfilDP perfil)
        {
            return perfil;
        } */
        public static PerfilDP[] LoadLista(DbConnection con, int idUsuario)
        {
            PerfilDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilMD.LoadLista(con, tran, idUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la lista de Perfiles!", ex);
            }
            return resultado;
        }

        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Perfil!", ex);
            }
            return resultado;
        }

        public static PerfilDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PerfilDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilMD.List(con, tran, startRowIndex, maximumRows);
                    if (resultado != null)
                        for (int numResultado = 0; numResultado < resultado.Length - 1; numResultado++)
                        {
                            if (numResultado == 0)
                                resultado[numResultado] = LoadDetail(con, tran, null, resultado[numResultado]);
                            else
                                resultado[numResultado] = LoadDetail(con, tran, resultado[numResultado - 1], resultado[numResultado]);
                        }
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Perfil!", ex);
            }
            return resultado;

        }
    }
}
