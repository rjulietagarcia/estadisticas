using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Model
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilPk".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 04:32:06 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilPk".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PerfilId</term><description>Descripcion PerfilId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilPkDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// PerfilPk perfilPk = new PerfilPk(perfil);
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [XmlRoot("PerfilPk")]
    public class PerfilPk
    {
        #region Definicion de campos privados.
        private int perfilId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PerfilId
        /// </summary> 
        [XmlElement("PerfilId")]
        public int PerfilId 
        {
            get {
                    return perfilId; 
            }
            set {
                    perfilId = value; 
            }
        }

        #endregion.

        #region Constructor interno.
        /// <summary>
        /// Constructor extendido de PerfilPk.
        /// </summary>
        /// <param name="perfilId">Descripción perfilId del tipo Int16.</param>
        public PerfilPk(int perfilId) 
        {
            this.perfilId = perfilId;
        }
        /// <summary>
        /// Constructor normal de PerfilPk.
        /// </summary>
        public PerfilPk() 
        {
        }
        #endregion.
    }
}
