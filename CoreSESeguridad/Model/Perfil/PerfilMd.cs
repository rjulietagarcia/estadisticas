using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Model
{
    internal class PerfilMD
    {
        private PerfilDP perfil = null;

        public PerfilMD(PerfilDP perfil)
        {
            this.perfil = perfil;
        }

        public Int32 Insert(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Insert Perfil
            DbCommand com = con.CreateCommand();
            String insertPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilInsert,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil",
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion");
            com.CommandText = insertPerfil;
            #endregion
            #region Parametros Insert Perfil
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}Id_Perfil", ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfil.PerfilId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, perfil.Nombre);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, perfil.Abreviatura);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfil.BitActivo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfil.UsuarioId);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfil.FechaActualizacion,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Insert Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorInsertPerfil = String.Format(Perfil.PerfilResx.PerfilInsert, "", 
                    perfil.PerfilId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,perfil.Nombre),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteString,perfil.Abreviatura),
                    perfil.BitActivo.ToString(),
                    perfil.UsuarioId.ToString(),
                    String.Format(CultureInfo.CurrentCulture,ConstantesGlobales.ConvierteFecha,DateTime.Parse(perfil.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)));
                System.Diagnostics.Debug.WriteLine(errorInsertPerfil);
                #endregion
            }
            return resultado;
        }
        public Boolean Find(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Find Perfil
            DbCommand com = con.CreateCommand();
            String findPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilFind,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil");
            com.CommandText = findPerfil;
            #endregion
            #region Parametros Find Perfil
            Common.CreateParameter(com, String.Format("{0}Id_Perfil",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfil.PerfilId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt32(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Find Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorFindPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilFind, "", 
                    perfil.PerfilId.ToString());
                System.Diagnostics.Debug.WriteLine(errorFindPerfil);
                #endregion
            }
            return resultado > 0;
        }
        public Int32 Delete(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Delete Perfil
            DbCommand com = con.CreateCommand();
            String deletePerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilDelete,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil");
            com.CommandText = deletePerfil;
            #endregion
            #region Parametros Delete Perfil
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Id_Perfil",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfil.PerfilId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Delete Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorDeletePerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilDelete, "", 
                    perfil.PerfilId.ToString());
                System.Diagnostics.Debug.WriteLine(errorDeletePerfil);
                #endregion
            }
            return resultado;
        }
        public Int32 Update(DbConnection con, DbTransaction tran)
        {
            Int32 resultado = 0;
            #region SQL Update Perfil
            DbCommand com = con.CreateCommand();
            String updatePerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilUpdate,
                        ConstantesGlobales.ParameterPrefix,
                        "Nombre",
                        "Abreviatura",
                        "Bit_Activo",
                        "Id_Usuario",
                        "Fecha_Actualizacion",
                        "Id_Perfil");
            com.CommandText = updatePerfil;
            #endregion
            #region Parametros Update Perfil
            Common.CreateParameter(com, String.Format("{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 50, ParameterDirection.Input, perfil.Nombre);
            Common.CreateParameter(com, String.Format("{0}Abreviatura",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 10, ParameterDirection.Input, perfil.Abreviatura);
            Common.CreateParameter(com, String.Format("{0}Bit_Activo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, perfil.BitActivo);
            Common.CreateParameter(com, String.Format("{0}Id_Usuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfil.UsuarioId);
            Common.CreateParameter(com, String.Format("{0}Fecha_Actualizacion",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(perfil.FechaActualizacion,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format("{0}Id_Perfil",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, perfil.PerfilId);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = com.ExecuteNonQuery();
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Update Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorUpdatePerfil = String.Format(Perfil.PerfilResx.PerfilUpdate, "", 
                    String.Format(ConstantesGlobales.ConvierteString,perfil.Nombre),
                    String.Format(ConstantesGlobales.ConvierteString,perfil.Abreviatura),
                    perfil.BitActivo.ToString(),
                    perfil.UsuarioId.ToString(),
                    String.Format(ConstantesGlobales.ConvierteFecha,DateTime.Parse(perfil.FechaActualizacion).ToString(ConstantesGlobales.FormatoFecha)),
                    perfil.PerfilId.ToString());
                System.Diagnostics.Debug.WriteLine(errorUpdatePerfil);
                #endregion
            }
            return resultado;
        }
        protected static PerfilDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilDP perfil = new PerfilDP();
            perfil.PerfilId = dr.IsDBNull(0) ? 0 : int.Parse(dr.GetValue(0).ToString());
            perfil.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            perfil.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            perfil.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            perfil.UsuarioId = dr.IsDBNull(4) ? 0 : int.Parse(dr.GetValue(4).ToString());
            perfil.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            return perfil;
            #endregion
        }
      /*  public static PerfilDP Load(DbConnection con, DbTransaction tran, PerfilPk pk)
        {
            #region SQL Load Perfil
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil");
            com.CommandText = loadPerfil;
            #endregion
            #region Parametros Load Perfil
            Common.CreateParameter(com, String.Format("{0}Id_Perfil",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, pk.PerfilId);
            #endregion
            PerfilDP perfil;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Perfil
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        perfil = ReadRow(dr);
                    }
                    else
                        perfil = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilSelect, "", 
                    pk.PerfilId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadPerfil);
                perfil = null;
                #endregion
            }
            return perfil;
        } */
        public static PerfilDP Load(DbConnection con, DbTransaction tran, PerfilPk pk)
        {
            #region SQL Load Perfil
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilSelect,
                        ConstantesGlobales.ParameterPrefix,
                        "Id_Perfil");
            com.CommandText = loadPerfil;
            #endregion
            #region Parametros Load Perfil
            Common.CreateParameter(com, String.Format("{0}Id_Perfil",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, pk.PerfilId);
            #endregion
            PerfilDP perfil;
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Read Load Perfil
                dr = com.ExecuteReader();
                try {
                    if (dr.Read())
                    {
                        perfil = ReadRow(dr);
                    }
                    else
                        perfil = null;
                } finally {  dr.Close(); }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilSelect, "", 
                    pk.PerfilId.ToString());
                System.Diagnostics.Debug.WriteLine(errorLoadPerfil);
                perfil = null;
                #endregion
            }
            return perfil;
        }

        public static PerfilDP[] LoadLista(DbConnection con, DbTransaction tran, int idusuario)
        {
            #region SQL Load Perfil
            DbCommand com = con.CreateCommand();
            DbDataReader dr;
            String loadPerfil = String.Format(CultureInfo.CurrentCulture, Perfil.PerfilResx.ListaPerfilSelect,
                        ConstantesGlobales.ParameterPrefix,  "Id_Usuario");
            com.CommandText = loadPerfil;
            #endregion
            #region Parametros Load Perfil
            Common.CreateParameter(com, String.Format("{0}Id_Usuario", ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, idusuario);
            if (tran != null)
            {
                com.Transaction = tran;
            }

            #endregion
            ArrayList lista = new ArrayList();
            try
            {

                dr = com.ExecuteReader();
                try
                {
                    while (dr.Read())
                    {
                        PerfilDP tmp = new PerfilDP();
                        tmp.PerfilId = dr.IsDBNull(0) ? 0 : int.Parse(dr.GetValue(0).ToString());
                        tmp.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
                        tmp.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
                        lista.Add(tmp);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    dr.Close();
                }
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadPerfil = String.Format(CultureInfo.CurrentCulture, Perfil.PerfilResx.PerfilSelect, "",
                    idusuario);
                System.Diagnostics.Debug.WriteLine(errorLoadPerfil);
                lista = null;
                #endregion
            }
            return (PerfilDP[])lista.ToArray(typeof(PerfilDP));

        }

        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Perfil
            DbCommand com = con.CreateCommand();
            String countPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilCount,"");
            com.CommandText = countPerfil;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPerfil);
                #endregion
            }
            return resultado;
        }
        public static PerfilDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Perfil
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfil = String.Format(CultureInfo.CurrentCulture, Perfil.PerfilResx.PerfilSelectAll, "", ConstantesGlobales.IncluyeRows);
            listPerfil = listPerfil.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPerfil, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Perfil
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Perfil
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Perfil
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPerfil = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilResx.PerfilSelectAll, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPerfil);
                #endregion
            }
            return (PerfilDP[])lista.ToArray(typeof(PerfilDP));
        }
    }
}
