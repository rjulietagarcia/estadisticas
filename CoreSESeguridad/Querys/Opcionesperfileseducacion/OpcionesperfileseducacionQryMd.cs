using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class OpcionesperfileseducacionQryMD
    {
        private OpcionesperfileseducacionQryDP opcionesperfileseducacionQry = null;

        public OpcionesperfileseducacionQryMD(OpcionesperfileseducacionQryDP opcionesperfileseducacionQry)
        {
            this.opcionesperfileseducacionQry = opcionesperfileseducacionQry;
        }

        protected static OpcionesperfileseducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            OpcionesperfileseducacionQryDP opcionesperfileseducacionQry = new OpcionesperfileseducacionQryDP();
            opcionesperfileseducacionQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            opcionesperfileseducacionQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            opcionesperfileseducacionQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            opcionesperfileseducacionQry.UsuIdUsuario = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            opcionesperfileseducacionQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            opcionesperfileseducacionQry.CicloescolarId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            return opcionesperfileseducacionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count OpcionesperfileseducacionQry
            DbCommand com = con.CreateCommand();
            String countOpcionesperfileseducacionQry = String.Format(CultureInfo.CurrentCulture,Opcionesperfileseducacion.OpcionesperfileseducacionQry.OpcionesperfileseducacionQryCount,"");
            com.CommandText = countOpcionesperfileseducacionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count OpcionesperfileseducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcionesperfileseducacionQry = String.Format(CultureInfo.CurrentCulture,Opcionesperfileseducacion.OpcionesperfileseducacionQry.OpcionesperfileseducacionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcionesperfileseducacionQry);
                #endregion
            }
            return resultado;
        }
        public static OpcionesperfileseducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List OpcionesperfileseducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcionesperfileseducacionQry = String.Format(CultureInfo.CurrentCulture, Opcionesperfileseducacion.OpcionesperfileseducacionQry.OpcionesperfileseducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listOpcionesperfileseducacionQry = listOpcionesperfileseducacionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listOpcionesperfileseducacionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load OpcionesperfileseducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista OpcionesperfileseducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista OpcionesperfileseducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcionesperfileseducacionQry = String.Format(CultureInfo.CurrentCulture,Opcionesperfileseducacion.OpcionesperfileseducacionQry.OpcionesperfileseducacionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcionesperfileseducacionQry);
                #endregion
            }
            return (OpcionesperfileseducacionQryDP[])lista.ToArray(typeof(OpcionesperfileseducacionQryDP));
        }
        public static Int64 CountForOpcionesAsignadas(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aUsuIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar)
        {
            Int64 resultado = 0;
            #region SQL CountForOpcionesAsignadas OpcionesperfileseducacionQry
            DbCommand com = con.CreateCommand();
            String countOpcionesperfileseducacionQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture,Opcionesperfileseducacion.OpcionesperfileseducacionQry.OpcionesperfileseducacionQryCount,"");
            countOpcionesperfileseducacionQryForOpcionesAsignadas += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aUsuIdUsuario != -1)
            {
                countOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} peu.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} pa.Bit_Activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdCicloescolar != -1)
            {
                countOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} peu.Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countOpcionesperfileseducacionQryForOpcionesAsignadas;
            #endregion
            #region Parametros countOpcionesperfileseducacionQryForOpcionesAsignadas
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForOpcionesAsignadas OpcionesperfileseducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcionesperfileseducacionQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture,Opcionesperfileseducacion.OpcionesperfileseducacionQry.OpcionesperfileseducacionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcionesperfileseducacionQryForOpcionesAsignadas);
                #endregion
            }
            return resultado;
        }
        public static OpcionesperfileseducacionQryDP[] ListForOpcionesAsignadas(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aUsuIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar)
        {
            #region SQL List OpcionesperfileseducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcionesperfileseducacionQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture, Opcionesperfileseducacion.OpcionesperfileseducacionQry.OpcionesperfileseducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listOpcionesperfileseducacionQryForOpcionesAsignadas = listOpcionesperfileseducacionQryForOpcionesAsignadas.Substring(6);
            listOpcionesperfileseducacionQryForOpcionesAsignadas += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aUsuIdUsuario != -1)
            {
                listOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} peu.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} pa.Bit_Activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdCicloescolar != -1)
            {
                listOpcionesperfileseducacionQryForOpcionesAsignadas += String.Format("    {1} peu.Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listOpcionesperfileseducacionQryForOpcionesAsignadas, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load OpcionesperfileseducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista OpcionesperfileseducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista OpcionesperfileseducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcionesperfileseducacionQry = String.Format(CultureInfo.CurrentCulture,Opcionesperfileseducacion.OpcionesperfileseducacionQry.OpcionesperfileseducacionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcionesperfileseducacionQry);
                #endregion
            }
            return (OpcionesperfileseducacionQryDP[])lista.ToArray(typeof(OpcionesperfileseducacionQryDP));
        }
    }
}
