using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class OpcionesperfileseducacionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = OpcionesperfileseducacionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un OpcionesperfileseducacionQry!", ex);
            }
            return resultado;
        }

        public static OpcionesperfileseducacionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            OpcionesperfileseducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = OpcionesperfileseducacionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de OpcionesperfileseducacionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForOpcionesAsignadas(DbConnection con, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aUsuIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = OpcionesperfileseducacionQryMD.CountForOpcionesAsignadas(con, tran, aIdSistema, aIdModulo, aIdOpcion, aUsuIdUsuario, aBitActivo, aIdCicloescolar);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un OpcionesperfileseducacionQry!", ex);
            }
            return resultado;
        }

        public static OpcionesperfileseducacionQryDP[] LoadListForOpcionesAsignadas(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aUsuIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar) 
        {
            OpcionesperfileseducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = OpcionesperfileseducacionQryMD.ListForOpcionesAsignadas(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion, aUsuIdUsuario, aBitActivo, aIdCicloescolar);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de OpcionesperfileseducacionQry!", ex);
            }
            return resultado;

        }
    }
}
