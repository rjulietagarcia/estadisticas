using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CtntQryMD
    {
        private CtntQryDP ctntQry = null;

        public CtntQryMD(CtntQryDP ctntQry)
        {
            this.ctntQry = ctntQry;
        }

        protected static CtntQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CtntQryDP ctntQry = new CtntQryDP();
            ctntQry.CctntId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            ctntQry.NiveleducacionId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            ctntQry.Niveleducacion = dr.IsDBNull(2) ? "" : dr.GetString(2);
            ctntQry.TurnoId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            ctntQry.Truno = dr.IsDBNull(4) ? "" : dr.GetString(4);
            ctntQry.CentrotrabajoId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            ctntQry.Nombrecct = dr.IsDBNull(6) ? "" : dr.GetString(6);
            ctntQry.Clavecct = dr.IsDBNull(7) ? "" : dr.GetString(7);
            ctntQry.SostenimientoId = dr.IsDBNull(8) ? (Byte)0 : dr.GetByte(8);
            ctntQry.Sostenimiento = dr.IsDBNull(9) ? "" : dr.GetString(9);
            ctntQry.SubnivelId = dr.IsDBNull(10) ? (short)0 : dr.GetInt16(10);
            ctntQry.Subnivel = dr.IsDBNull(11) ? "" : dr.GetString(11);
            ctntQry.TipoeducacionId = dr.IsDBNull(12) ? (short)0 : dr.GetInt16(12);
            ctntQry.Tipoeducacion = dr.IsDBNull(13) ? "" : dr.GetString(13);
            ctntQry.NivelId = dr.IsDBNull(14) ? (short)0 : dr.GetInt16(14);
            ctntQry.Nivel = dr.IsDBNull(15) ? "" : dr.GetString(15);
            ctntQry.RegionId = dr.IsDBNull(16) ? (short)0 : dr.GetInt16(16);
            ctntQry.Region = dr.IsDBNull(17) ? "" : dr.GetString(17);
            ctntQry.ZonaId = dr.IsDBNull(18) ? (short)0 : dr.GetInt16(18);
            ctntQry.Numerozona = dr.IsDBNull(19) ? (short)0 : dr.GetInt16(19);
            return ctntQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CtntQry
            DbCommand com = con.CreateCommand();
            String countCtntQry = String.Format(CultureInfo.CurrentCulture,Ctnt.CtntQry.CtntQryCount,"");
            com.CommandText = countCtntQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CtntQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CtntQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CtntQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCtntQry = String.Format(CultureInfo.CurrentCulture, Ctnt.CtntQry.CtntQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCtntQry = listCtntQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCtntQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CtntQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CtntQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CtntQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CtntQryDP[])lista.ToArray(typeof(CtntQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Int16 aIdCctnt)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave CtntQry
            DbCommand com = con.CreateCommand();
            String countCtntQryForLlave = String.Format(CultureInfo.CurrentCulture,Ctnt.CtntQry.CtntQryCount,"");
            countCtntQryForLlave += " AND \n";
            String delimitador = "";
            if (aIdCctnt != -1)
            {
                countCtntQryForLlave += String.Format("    {1} ctnt.id_CCTNT = {0}IdCctnt\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCtntQryForLlave;
            #endregion
            #region Parametros countCtntQryForLlave
            if (aIdCctnt != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCctnt",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCctnt);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave CtntQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CtntQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdCctnt)
        {
            #region SQL List CtntQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCtntQryForLlave = String.Format(CultureInfo.CurrentCulture, Ctnt.CtntQry.CtntQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCtntQryForLlave = listCtntQryForLlave.Substring(6);
            listCtntQryForLlave += " AND \n";
            String delimitador = "";
            if (aIdCctnt != -1)
            {
                listCtntQryForLlave += String.Format("    {1} ctnt.id_CCTNT = {0}IdCctnt\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCtntQryForLlave, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CtntQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdCctnt != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCctnt",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCctnt);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CtntQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CtntQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CtntQryDP[])lista.ToArray(typeof(CtntQryDP));
        }
        public static Int64 CountForCentroTrabajo(DbConnection con, DbTransaction tran, Int16 aIdCentrotrabajo, Int16 aIdTurno, Byte aIdNiveleducacion)
        {
            Int64 resultado = 0;
            #region SQL CountForCentroTrabajo CtntQry
            DbCommand com = con.CreateCommand();
            String countCtntQryForCentroTrabajo = String.Format(CultureInfo.CurrentCulture,Ctnt.CtntQry.CtntQryCount,"");
            String delimitador = " AND ";
            countCtntQryForCentroTrabajo += String.Format("    {1} ctnt.id_CentroTrabajo = {0}IdCentrotrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdTurno != -1)
            {
                countCtntQryForCentroTrabajo += String.Format("    {1} ctnt.id_turno = {0}IdTurno\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdNiveleducacion != 0)
            {
                countCtntQryForCentroTrabajo += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCtntQryForCentroTrabajo;
            #endregion
            #region Parametros countCtntQryForCentroTrabajo
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCentrotrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCentrotrabajo);
            if (aIdTurno != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTurno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTurno);
            }
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForCentroTrabajo CtntQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CtntQryDP[] ListForCentroTrabajo(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdCentrotrabajo, Int16 aIdTurno, Byte aIdNiveleducacion)
        {
            #region SQL List CtntQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCtntQryForCentroTrabajo = String.Format(CultureInfo.CurrentCulture, Ctnt.CtntQry.CtntQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCtntQryForCentroTrabajo = listCtntQryForCentroTrabajo.Substring(6);
            String delimitador = " AND ";
            listCtntQryForCentroTrabajo += String.Format("    {1} ctnt.id_CentroTrabajo = {0}IdCentrotrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdTurno != -1)
            {
                listCtntQryForCentroTrabajo += String.Format("    {1} ctnt.id_turno = {0}IdTurno\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdNiveleducacion != 0)
            {
                listCtntQryForCentroTrabajo += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCtntQryForCentroTrabajo, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CtntQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCentrotrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCentrotrabajo);
            if (aIdTurno != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTurno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTurno);
            }
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CtntQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CtntQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CtntQryDP[])lista.ToArray(typeof(CtntQryDP));
        }
        public static Int64 CountForClaveCentroTrabajo(DbConnection con, DbTransaction tran, String aClavecct, Int16 aIdTurno, Byte aIdNiveleducacion)
        {
            Int64 resultado = 0;
            #region SQL CountForClaveCentroTrabajo CtntQry
            DbCommand com = con.CreateCommand();
            String countCtntQryForClaveCentroTrabajo = String.Format(CultureInfo.CurrentCulture,Ctnt.CtntQry.CtntQryCount,"");
            countCtntQryForClaveCentroTrabajo += " AND \n";
            String delimitador = "";
            if (aClavecct != "")
            {
                countCtntQryForClaveCentroTrabajo += String.Format("    {1} ct.Clave = {0}Clavecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdTurno != -1)
            {
                countCtntQryForClaveCentroTrabajo += String.Format("    {1} ctnt.id_turno = {0}IdTurno\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdNiveleducacion != 0)
            {
                countCtntQryForClaveCentroTrabajo += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCtntQryForClaveCentroTrabajo;
            #endregion
            #region Parametros countCtntQryForClaveCentroTrabajo
            if (aClavecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clavecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 10, ParameterDirection.Input, aClavecct);
            }
            if (aIdTurno != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTurno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTurno);
            }
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForClaveCentroTrabajo CtntQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CtntQryDP[] ListForClaveCentroTrabajo(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aClavecct, Int16 aIdTurno, Byte aIdNiveleducacion)
        {
            #region SQL List CtntQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCtntQryForClaveCentroTrabajo = String.Format(CultureInfo.CurrentCulture, Ctnt.CtntQry.CtntQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCtntQryForClaveCentroTrabajo = listCtntQryForClaveCentroTrabajo.Substring(6);
            listCtntQryForClaveCentroTrabajo += " AND \n";
            String delimitador = "";
            if (aClavecct != "")
            {
                listCtntQryForClaveCentroTrabajo += String.Format("    {1} ct.Clave = {0}Clavecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdTurno != -1)
            {
                listCtntQryForClaveCentroTrabajo += String.Format("    {1} ctnt.id_turno = {0}IdTurno\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdNiveleducacion != 0)
            {
                listCtntQryForClaveCentroTrabajo += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCtntQryForClaveCentroTrabajo, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CtntQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aClavecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clavecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 10, ParameterDirection.Input, aClavecct);
            }
            if (aIdTurno != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTurno",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTurno);
            }
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CtntQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CtntQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CtntQryDP[])lista.ToArray(typeof(CtntQryDP));
        }
    }
}
