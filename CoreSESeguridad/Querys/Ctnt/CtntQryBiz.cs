using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CtntQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CtntQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CtntQry!", ex);
            }
            return resultado;
        }

        public static CtntQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CtntQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CtntQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CtntQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLlave(DbConnection con, Int16 aIdCctnt) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CtntQryMD.CountForLlave(con, tran, aIdCctnt);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CtntQry!", ex);
            }
            return resultado;
        }

        public static CtntQryDP[] LoadListForLlave(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdCctnt) 
        {
            CtntQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CtntQryMD.ListForLlave(con, tran, startRowIndex, maximumRows, aIdCctnt);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CtntQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForCentroTrabajo(DbConnection con, Int16 aIdCentrotrabajo, Int16 aIdTurno, Byte aIdNiveleducacion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CtntQryMD.CountForCentroTrabajo(con, tran, aIdCentrotrabajo, aIdTurno, aIdNiveleducacion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CtntQry!", ex);
            }
            return resultado;
        }

        public static CtntQryDP[] LoadListForCentroTrabajo(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdCentrotrabajo, Int16 aIdTurno, Byte aIdNiveleducacion) 
        {
            CtntQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CtntQryMD.ListForCentroTrabajo(con, tran, startRowIndex, maximumRows, aIdCentrotrabajo, aIdTurno, aIdNiveleducacion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CtntQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForClaveCentroTrabajo(DbConnection con, String aClavecct, Int16 aIdTurno, Byte aIdNiveleducacion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CtntQryMD.CountForClaveCentroTrabajo(con, tran, aClavecct, aIdTurno, aIdNiveleducacion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CtntQry!", ex);
            }
            return resultado;
        }

        public static CtntQryDP[] LoadListForClaveCentroTrabajo(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aClavecct, Int16 aIdTurno, Byte aIdNiveleducacion) 
        {
            CtntQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CtntQryMD.ListForClaveCentroTrabajo(con, tran, startRowIndex, maximumRows, aClavecct, aIdTurno, aIdNiveleducacion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CtntQry!", ex);
            }
            return resultado;

        }
    }
}
