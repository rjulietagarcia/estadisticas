using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "SistemaPuertoQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 16 de junio de 2009.</Para>
    /// <Para>Hora: 10:04:16 a.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "SistemaPuertoQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Puerto</term><description>Descripcion Puerto</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>CarpetaSistema</term><description>Descripcion CarpetaSistema</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SistemaPuertoQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// SistemaPuertoQryDTO sistemapuertoqry = new SistemaPuertoQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("SistemaPuertoQry")]
    public class SistemaPuertoQryDP
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private String puerto;
        private String nombre;
        private String carpetaSistema;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// Puerto
        /// </summary> 
        [XmlElement("Puerto")]
        public String Puerto
        {
            get {
                    return puerto; 
            }
            set {
                    puerto = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// CarpetaSistema
        /// </summary> 
        [XmlElement("CarpetaSistema")]
        public String CarpetaSistema
        {
            get {
                    return carpetaSistema; 
            }
            set {
                    carpetaSistema = value; 
            }
        }

        #endregion.
    }
}
