using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class SistemaPuertoQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SistemaPuertoQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un SistemaPuertoQry!", ex);
            }
            return resultado;
        }

        public static SistemaPuertoQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SistemaPuertoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SistemaPuertoQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de SistemaPuertoQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForPuerto(DbConnection con, String aPuerto) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SistemaPuertoQryMD.CountForPuerto(con, tran, aPuerto);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un SistemaPuertoQry!", ex);
            }
            return resultado;
        }

        public static SistemaPuertoQryDP[] LoadListForPuerto(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aPuerto) 
        {
            SistemaPuertoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SistemaPuertoQryMD.ListForPuerto(con, tran, startRowIndex, maximumRows, aPuerto);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de SistemaPuertoQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForCarpeta(DbConnection con, String aCarpetaSistema) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SistemaPuertoQryMD.CountForCarpeta(con, tran, aCarpetaSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un SistemaPuertoQry!", ex);
            }
            return resultado;
        }

        public static SistemaPuertoQryDP[] LoadListForCarpeta(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aCarpetaSistema) 
        {
            SistemaPuertoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SistemaPuertoQryMD.ListForCarpeta(con, tran, startRowIndex, maximumRows, aCarpetaSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de SistemaPuertoQry!", ex);
            }
            return resultado;

        }
    }
}
