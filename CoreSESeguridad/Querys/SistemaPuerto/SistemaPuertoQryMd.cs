using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class SistemaPuertoQryMD
    {
        private SistemaPuertoQryDP sistemaPuertoQry = null;

        public SistemaPuertoQryMD(SistemaPuertoQryDP sistemaPuertoQry)
        {
            this.sistemaPuertoQry = sistemaPuertoQry;
        }

        protected static SistemaPuertoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SistemaPuertoQryDP sistemaPuertoQry = new SistemaPuertoQryDP();
            sistemaPuertoQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            sistemaPuertoQry.Puerto = dr.IsDBNull(1) ? "" : dr.GetString(1);
            sistemaPuertoQry.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            sistemaPuertoQry.CarpetaSistema = dr.IsDBNull(3) ? "" : dr.GetString(3);
            return sistemaPuertoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count SistemaPuertoQry
            DbCommand com = con.CreateCommand();
            String countSistemaPuertoQry = String.Format(CultureInfo.CurrentCulture,SistemaPuerto.SistemaPuertoQry.SistemaPuertoQryCount,"");
            com.CommandText = countSistemaPuertoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count SistemaPuertoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SistemaPuertoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List SistemaPuertoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSistemaPuertoQry = String.Format(CultureInfo.CurrentCulture, SistemaPuerto.SistemaPuertoQry.SistemaPuertoQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listSistemaPuertoQry = listSistemaPuertoQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listSistemaPuertoQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listSistemaPuertoQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load SistemaPuertoQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SistemaPuertoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SistemaPuertoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SistemaPuertoQryDP[])lista.ToArray(typeof(SistemaPuertoQryDP));
        }
        public static Int64 CountForPuerto(DbConnection con, DbTransaction tran, String aPuerto)
        {
            Int64 resultado = 0;
            #region SQL CountForPuerto SistemaPuertoQry
            DbCommand com = con.CreateCommand();
            String countSistemaPuertoQryForPuerto = String.Format(CultureInfo.CurrentCulture,SistemaPuerto.SistemaPuertoQry.SistemaPuertoQryCount,"");
            countSistemaPuertoQryForPuerto += " WHERE \n";
            String delimitador = "";
            countSistemaPuertoQryForPuerto += String.Format("    {1} Puerto = {0}Puerto\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countSistemaPuertoQryForPuerto;
            #endregion
            #region Parametros countSistemaPuertoQryForPuerto
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Puerto",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 20, ParameterDirection.Input, aPuerto);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForPuerto SistemaPuertoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SistemaPuertoQryDP[] ListForPuerto(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aPuerto)
        {
            #region SQL List SistemaPuertoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSistemaPuertoQryForPuerto = String.Format(CultureInfo.CurrentCulture, SistemaPuerto.SistemaPuertoQry.SistemaPuertoQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listSistemaPuertoQryForPuerto = listSistemaPuertoQryForPuerto.Substring(6);
            listSistemaPuertoQryForPuerto += " WHERE \n";
            String delimitador = "";
            listSistemaPuertoQryForPuerto += String.Format("    {1} Puerto = {0}Puerto\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listSistemaPuertoQryForPuerto, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listSistemaPuertoQryForPuerto;
            DbDataReader dr;
            #endregion
            #region Parametros Load SistemaPuertoQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Puerto",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 20, ParameterDirection.Input, aPuerto);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SistemaPuertoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SistemaPuertoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SistemaPuertoQryDP[])lista.ToArray(typeof(SistemaPuertoQryDP));
        }
        public static Int64 CountForCarpeta(DbConnection con, DbTransaction tran, String aCarpetaSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForCarpeta SistemaPuertoQry
            DbCommand com = con.CreateCommand();
            String countSistemaPuertoQryForCarpeta = String.Format(CultureInfo.CurrentCulture,SistemaPuerto.SistemaPuertoQry.SistemaPuertoQryCount,"");
            countSistemaPuertoQryForCarpeta += " WHERE \n";
            String delimitador = "";
            countSistemaPuertoQryForCarpeta += String.Format("    {1} Carpeta_Sistema = {0}CarpetaSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countSistemaPuertoQryForCarpeta;
            #endregion
            #region Parametros countSistemaPuertoQryForCarpeta
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CarpetaSistema",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 255, ParameterDirection.Input, aCarpetaSistema);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForCarpeta SistemaPuertoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SistemaPuertoQryDP[] ListForCarpeta(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aCarpetaSistema)
        {
            #region SQL List SistemaPuertoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSistemaPuertoQryForCarpeta = String.Format(CultureInfo.CurrentCulture, SistemaPuerto.SistemaPuertoQry.SistemaPuertoQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listSistemaPuertoQryForCarpeta = listSistemaPuertoQryForCarpeta.Substring(6);
            listSistemaPuertoQryForCarpeta += " WHERE \n";
            String delimitador = "";
            listSistemaPuertoQryForCarpeta += String.Format("    {1} Carpeta_Sistema = {0}CarpetaSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listSistemaPuertoQryForCarpeta, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listSistemaPuertoQryForCarpeta;
            DbDataReader dr;
            #endregion
            #region Parametros Load SistemaPuertoQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CarpetaSistema",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 255, ParameterDirection.Input, aCarpetaSistema);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SistemaPuertoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SistemaPuertoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SistemaPuertoQryDP[])lista.ToArray(typeof(SistemaPuertoQryDP));
        }
    }
}
