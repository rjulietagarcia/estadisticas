using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class SistemaQryMD
    {
        private SistemaQryDP sistemaQry = null;

        public SistemaQryMD(SistemaQryDP sistemaQry)
        {
            this.sistemaQry = sistemaQry;
        }

        protected static SistemaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SistemaQryDP sistemaQry = new SistemaQryDP();
            sistemaQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            sistemaQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            sistemaQry.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            sistemaQry.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            sistemaQry.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            sistemaQry.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            sistemaQry.FechaInicio = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            sistemaQry.FechaFin = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            sistemaQry.CarpetaSistema = dr.IsDBNull(8) ? "" : dr.GetString(8);
            return sistemaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count SistemaQry
            DbCommand com = con.CreateCommand();
            String countSistemaQry = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaQry.SistemaQryCount,"");
            com.CommandText = countSistemaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count SistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountSistemaQry = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaQry.SistemaQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountSistemaQry);
                #endregion
            }
            return resultado;
        }
        public static SistemaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List SistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSistemaQry = String.Format(CultureInfo.CurrentCulture, Sistema.SistemaQry.SistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listSistemaQry = listSistemaQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listSistemaQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listSistemaQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load SistemaQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaSistemaQry = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaQry.SistemaQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaSistemaQry);
                #endregion
            }
            return (SistemaQryDP[])lista.ToArray(typeof(SistemaQryDP));
        }
        public static Int64 CountForSistema(DbConnection con, DbTransaction tran, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForSistema SistemaQry
            DbCommand com = con.CreateCommand();
            String countSistemaQryForSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaQry.SistemaQryCount,"");
            countSistemaQryForSistema += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countSistemaQryForSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countSistemaQryForSistema;
            #endregion
            #region Parametros countSistemaQryForSistema
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForSistema SistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountSistemaQryForSistema = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaQry.SistemaQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountSistemaQryForSistema);
                #endregion
            }
            return resultado;
        }
        public static SistemaQryDP[] ListForSistema(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema)
        {
            #region SQL List SistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSistemaQryForSistema = String.Format(CultureInfo.CurrentCulture, Sistema.SistemaQry.SistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listSistemaQryForSistema = listSistemaQryForSistema.Substring(6);
            if (aIdSistema != 0)
            {
                listSistemaQryForSistema += " AND \n";
                String delimitador = "";
                listSistemaQryForSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listSistemaQryForSistema, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listSistemaQryForSistema;
            DbDataReader dr;
            #endregion
            #region Parametros Load SistemaQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaSistemaQry = String.Format(CultureInfo.CurrentCulture,Sistema.SistemaQry.SistemaQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaSistemaQry);
                #endregion
            }
            return (SistemaQryDP[])lista.ToArray(typeof(SistemaQryDP));
        }
    }
}
