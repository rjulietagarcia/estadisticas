using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TreeAccionesQryMD
    {
        private TreeAccionesQryDP treeAccionesQry = null;

        public TreeAccionesQryMD(TreeAccionesQryDP treeAccionesQry)
        {
            this.treeAccionesQry = treeAccionesQry;
        }

        protected static TreeAccionesQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TreeAccionesQryDP treeAccionesQry = new TreeAccionesQryDP();
            treeAccionesQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            treeAccionesQry.ModuloId = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            treeAccionesQry.OpcionId = dr.IsDBNull(2) ? 0 : dr.GetInt32(2);
            treeAccionesQry.AccionId = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            treeAccionesQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            return treeAccionesQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TreeAccionesQry
            DbCommand com = con.CreateCommand();
            String countTreeAccionesQry = String.Format(CultureInfo.CurrentCulture,TreeAcciones.TreeAccionesQry.TreeAccionesQryCount,"");
            com.CommandText = countTreeAccionesQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TreeAccionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeAccionesQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TreeAccionesQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeAccionesQry = String.Format(CultureInfo.CurrentCulture, TreeAcciones.TreeAccionesQry.TreeAccionesQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreeAccionesQry = listTreeAccionesQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreeAccionesQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeAccionesQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeAccionesQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeAccionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeAccionesQryDP[])lista.ToArray(typeof(TreeAccionesQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Byte aIdSistema, Int32 aIdModulo, Int32 aIdOpcion, Int32 aIdAccion)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave TreeAccionesQry
            DbCommand com = con.CreateCommand();
            String countTreeAccionesQryForLlave = String.Format(CultureInfo.CurrentCulture,TreeAcciones.TreeAccionesQry.TreeAccionesQryCount,"");
            countTreeAccionesQryForLlave += " WHERE \n";
            String delimitador = "";
            countTreeAccionesQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdModulo != 0)
            {
                countTreeAccionesQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countTreeAccionesQryForLlave += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdAccion != 0)
            {
                countTreeAccionesQryForLlave += String.Format("    {1} Id_Accion = {0}IdAccion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTreeAccionesQryForLlave;
            #endregion
            #region Parametros countTreeAccionesQryForLlave
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aIdAccion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdAccion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdAccion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave TreeAccionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeAccionesQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Int32 aIdModulo, Int32 aIdOpcion, Int32 aIdAccion)
        {
            #region SQL List TreeAccionesQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeAccionesQryForLlave = String.Format(CultureInfo.CurrentCulture, TreeAcciones.TreeAccionesQry.TreeAccionesQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreeAccionesQryForLlave = listTreeAccionesQryForLlave.Substring(6);
            listTreeAccionesQryForLlave += " WHERE \n";
            String delimitador = "";
            listTreeAccionesQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdModulo != 0)
            {
                listTreeAccionesQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listTreeAccionesQryForLlave += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdAccion != 0)
            {
                listTreeAccionesQryForLlave += String.Format("    {1} Id_Accion = {0}IdAccion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listTreeAccionesQryForLlave += "    Order By Id_Sistema, Id_Modulo, Id_Opcion, Id_Accion ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreeAccionesQryForLlave, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeAccionesQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aIdAccion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdAccion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdAccion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeAccionesQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeAccionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeAccionesQryDP[])lista.ToArray(typeof(TreeAccionesQryDP));
        }
    }
}
