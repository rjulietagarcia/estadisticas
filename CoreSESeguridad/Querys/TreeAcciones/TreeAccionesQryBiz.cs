using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class TreeAccionesQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreeAccionesQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreeAccionesQry!", ex);
            }
            return resultado;
        }

        public static TreeAccionesQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TreeAccionesQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreeAccionesQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreeAccionesQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLlave(DbConnection con, Byte aIdSistema, Int32 aIdModulo, Int32 aIdOpcion, Int32 aIdAccion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreeAccionesQryMD.CountForLlave(con, tran, aIdSistema, aIdModulo, aIdOpcion, aIdAccion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreeAccionesQry!", ex);
            }
            return resultado;
        }

        public static TreeAccionesQryDP[] LoadListForLlave(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Int32 aIdModulo, Int32 aIdOpcion, Int32 aIdAccion) 
        {
            TreeAccionesQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreeAccionesQryMD.ListForLlave(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion, aIdAccion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreeAccionesQry!", ex);
            }
            return resultado;

        }
    }
}
