using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TipoEducacionQryMD
    {
        private TipoEducacionQryDP tipoEducacionQry = null;

        public TipoEducacionQryMD(TipoEducacionQryDP tipoEducacionQry)
        {
            this.tipoEducacionQry = tipoEducacionQry;
        }

        protected static TipoEducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TipoEducacionQryDP tipoEducacionQry = new TipoEducacionQryDP();
            tipoEducacionQry.TipoeducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            tipoEducacionQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            tipoEducacionQry.BitActivo = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            tipoEducacionQry.UsuarioId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            tipoEducacionQry.FechaActualizacion = dr.IsDBNull(4) ? "" : dr.GetDateTime(4).ToShortDateString();;
            return tipoEducacionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TipoEducacionQry
            DbCommand com = con.CreateCommand();
            String countTipoEducacionQry = String.Format(CultureInfo.CurrentCulture,TipoEducacion.TipoEducacionQry.TipoEducacionQryCount,"");
            com.CommandText = countTipoEducacionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TipoEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountTipoEducacionQry = String.Format(CultureInfo.CurrentCulture,TipoEducacion.TipoEducacionQry.TipoEducacionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountTipoEducacionQry);
                #endregion
            }
            return resultado;
        }
        public static TipoEducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TipoEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTipoEducacionQry = String.Format(CultureInfo.CurrentCulture, TipoEducacion.TipoEducacionQry.TipoEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTipoEducacionQry = listTipoEducacionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTipoEducacionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TipoEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TipoEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TipoEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaTipoEducacionQry = String.Format(CultureInfo.CurrentCulture,TipoEducacion.TipoEducacionQry.TipoEducacionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaTipoEducacionQry);
                #endregion
            }
            return (TipoEducacionQryDP[])lista.ToArray(typeof(TipoEducacionQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion TipoEducacionQry
            DbCommand com = con.CreateCommand();
            String countTipoEducacionQryForDescripcion = String.Format(CultureInfo.CurrentCulture,TipoEducacion.TipoEducacionQry.TipoEducacionQryCount,"");
            countTipoEducacionQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countTipoEducacionQryForDescripcion += String.Format("    {1} nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTipoEducacionQryForDescripcion;
            #endregion
            #region Parametros countTipoEducacionQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion TipoEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountTipoEducacionQryForDescripcion = String.Format(CultureInfo.CurrentCulture,TipoEducacion.TipoEducacionQry.TipoEducacionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountTipoEducacionQryForDescripcion);
                #endregion
            }
            return resultado;
        }
        public static TipoEducacionQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre)
        {
            #region SQL List TipoEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTipoEducacionQryForDescripcion = String.Format(CultureInfo.CurrentCulture, TipoEducacion.TipoEducacionQry.TipoEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTipoEducacionQryForDescripcion = listTipoEducacionQryForDescripcion.Substring(6);
            listTipoEducacionQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listTipoEducacionQryForDescripcion += String.Format("    {1} nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTipoEducacionQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TipoEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TipoEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TipoEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaTipoEducacionQry = String.Format(CultureInfo.CurrentCulture,TipoEducacion.TipoEducacionQry.TipoEducacionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaTipoEducacionQry);
                #endregion
            }
            return (TipoEducacionQryDP[])lista.ToArray(typeof(TipoEducacionQryDP));
        }
    }
}
