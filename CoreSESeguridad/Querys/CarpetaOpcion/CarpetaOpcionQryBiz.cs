using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CarpetaOpcionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CarpetaOpcionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CarpetaOpcionQry!", ex);
            }
            return resultado;
        }

        public static CarpetaOpcionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CarpetaOpcionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CarpetaOpcionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CarpetaOpcionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForCarpeta(DbConnection con, Byte aIdSistema, Byte aIdModulo, String aCarpetaOpcion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CarpetaOpcionQryMD.CountForCarpeta(con, tran, aIdSistema, aIdModulo, aCarpetaOpcion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CarpetaOpcionQry!", ex);
            }
            return resultado;
        }

        public static CarpetaOpcionQryDP[] LoadListForCarpeta(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, String aCarpetaOpcion) 
        {
            CarpetaOpcionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CarpetaOpcionQryMD.ListForCarpeta(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aCarpetaOpcion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CarpetaOpcionQry!", ex);
            }
            return resultado;

        }
    }
}
