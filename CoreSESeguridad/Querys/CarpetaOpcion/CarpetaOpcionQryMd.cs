using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CarpetaOpcionQryMD
    {
        private CarpetaOpcionQryDP carpetaOpcionQry = null;

        public CarpetaOpcionQryMD(CarpetaOpcionQryDP carpetaOpcionQry)
        {
            this.carpetaOpcionQry = carpetaOpcionQry;
        }

        protected static CarpetaOpcionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CarpetaOpcionQryDP carpetaOpcionQry = new CarpetaOpcionQryDP();
            carpetaOpcionQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            carpetaOpcionQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            carpetaOpcionQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            carpetaOpcionQry.CarpetaOpcion = dr.IsDBNull(3) ? "" : dr.GetString(3);
            carpetaOpcionQry.Abreviatura = dr.IsDBNull(4) ? "" : dr.GetString(4);
            return carpetaOpcionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CarpetaOpcionQry
            DbCommand com = con.CreateCommand();
            String countCarpetaOpcionQry = String.Format(CultureInfo.CurrentCulture,CarpetaOpcion.CarpetaOpcionQry.CarpetaOpcionQryCount,"");
            com.CommandText = countCarpetaOpcionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CarpetaOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CarpetaOpcionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CarpetaOpcionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCarpetaOpcionQry = String.Format(CultureInfo.CurrentCulture, CarpetaOpcion.CarpetaOpcionQry.CarpetaOpcionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCarpetaOpcionQry = listCarpetaOpcionQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCarpetaOpcionQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCarpetaOpcionQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load CarpetaOpcionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CarpetaOpcionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CarpetaOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CarpetaOpcionQryDP[])lista.ToArray(typeof(CarpetaOpcionQryDP));
        }
        public static Int64 CountForCarpeta(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, String aCarpetaOpcion)
        {
            Int64 resultado = 0;
            #region SQL CountForCarpeta CarpetaOpcionQry
            DbCommand com = con.CreateCommand();
            String countCarpetaOpcionQryForCarpeta = String.Format(CultureInfo.CurrentCulture,CarpetaOpcion.CarpetaOpcionQry.CarpetaOpcionQryCount,"");
            countCarpetaOpcionQryForCarpeta += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countCarpetaOpcionQryForCarpeta += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countCarpetaOpcionQryForCarpeta += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aCarpetaOpcion.Length != 0)
            {
                countCarpetaOpcionQryForCarpeta += String.Format("    {1} Carpeta_Opcion LIKE {0}CarpetaOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCarpetaOpcionQryForCarpeta;
            #endregion
            #region Parametros countCarpetaOpcionQryForCarpeta
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aCarpetaOpcion.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CarpetaOpcion",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 255, ParameterDirection.Input, aCarpetaOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForCarpeta CarpetaOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CarpetaOpcionQryDP[] ListForCarpeta(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, String aCarpetaOpcion)
        {
            #region SQL List CarpetaOpcionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCarpetaOpcionQryForCarpeta = String.Format(CultureInfo.CurrentCulture, CarpetaOpcion.CarpetaOpcionQry.CarpetaOpcionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCarpetaOpcionQryForCarpeta = listCarpetaOpcionQryForCarpeta.Substring(6);
            listCarpetaOpcionQryForCarpeta += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listCarpetaOpcionQryForCarpeta += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listCarpetaOpcionQryForCarpeta += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aCarpetaOpcion.Length != 0)
            {
                listCarpetaOpcionQryForCarpeta += String.Format("    {1} Carpeta_Opcion LIKE {0}CarpetaOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCarpetaOpcionQryForCarpeta, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCarpetaOpcionQryForCarpeta;
            DbDataReader dr;
            #endregion
            #region Parametros Load CarpetaOpcionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aCarpetaOpcion.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CarpetaOpcion",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 255, ParameterDirection.Input, aCarpetaOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CarpetaOpcionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CarpetaOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CarpetaOpcionQryDP[])lista.ToArray(typeof(CarpetaOpcionQryDP));
        }
    }
}
