using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TreeEfectivosQryMD
    {
        private TreeEfectivosQryDP treeEfectivosQry = null;

        public TreeEfectivosQryMD(TreeEfectivosQryDP treeEfectivosQry)
        {
            this.treeEfectivosQry = treeEfectivosQry;
        }

        protected static TreeEfectivosQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TreeEfectivosQryDP treeEfectivosQry = new TreeEfectivosQryDP();
            treeEfectivosQry.Llave = dr.IsDBNull(0) ? "" : dr.GetString(0);
            treeEfectivosQry.UsuIdUsuario = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            treeEfectivosQry.SistemaId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            return treeEfectivosQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TreeEfectivosQry
            DbCommand com = con.CreateCommand();
            String countTreeEfectivosQry = String.Format(CultureInfo.CurrentCulture,TreeEfectivos.TreeEfectivosQry.TreeEfectivosQryCount,"");
            com.CommandText = countTreeEfectivosQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TreeEfectivosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeEfectivosQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TreeEfectivosQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeEfectivosQry = String.Format(CultureInfo.CurrentCulture, TreeEfectivos.TreeEfectivosQry.TreeEfectivosQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreeEfectivosQry = listTreeEfectivosQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreeEfectivosQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeEfectivosQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeEfectivosQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeEfectivosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeEfectivosQryDP[])lista.ToArray(typeof(TreeEfectivosQryDP));
        }
        public static Int64 CountForUsuarioSistema(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuarioSistema TreeEfectivosQry
            DbCommand com = con.CreateCommand();
            String countTreeEfectivosQryForUsuarioSistema = String.Format(CultureInfo.CurrentCulture,TreeEfectivos.TreeEfectivosQry.TreeEfectivosQryCount,"");
            countTreeEfectivosQryForUsuarioSistema += " WHERE \n";
            String delimitador = "";
            countTreeEfectivosQryForUsuarioSistema += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdSistema != 0)
            {
                countTreeEfectivosQryForUsuarioSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTreeEfectivosQryForUsuarioSistema;
            #endregion
            #region Parametros countTreeEfectivosQryForUsuarioSistema
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuarioSistema TreeEfectivosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeEfectivosQryDP[] ListForUsuarioSistema(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Byte aIdSistema)
        {
            #region SQL List TreeEfectivosQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeEfectivosQryForUsuarioSistema = String.Format(CultureInfo.CurrentCulture, TreeEfectivos.TreeEfectivosQry.TreeEfectivosQrySelect, "",
                String.Format("{0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix)
                );
            //listTreeEfectivosQryForUsuarioSistema = listTreeEfectivosQryForUsuarioSistema.Substring(6);
            //listTreeEfectivosQryForUsuarioSistema += " WHERE \n";
            //String delimitador = "";
            //listTreeEfectivosQryForUsuarioSistema += 

            //delimitador = " AND ";
            //if (aIdSistema != 0)
            //{
            //    listTreeEfectivosQryForUsuarioSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            String filtraList = listTreeEfectivosQryForUsuarioSistema;
                //String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                //ConstantesGlobales.ParameterPrefix,
                //listTreeEfectivosQryForUsuarioSistema, "", 
                //(startRowIndex + maximumRows).ToString(),
                //startRowIndex,
                //maximumRows,
                //"" // DISTINCT si es que aplica.
                //);
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeEfectivosQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            else
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}IdSistema", ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, 1);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeEfectivosQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeEfectivosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeEfectivosQryDP[])lista.ToArray(typeof(TreeEfectivosQryDP));
        }
    }
}
