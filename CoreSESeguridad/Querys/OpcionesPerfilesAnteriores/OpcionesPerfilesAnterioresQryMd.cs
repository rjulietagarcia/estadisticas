using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class OpcionesPerfilesAnterioresQryMD
    {
        private OpcionesPerfilesAnterioresQryDP opcionesPerfilesAnterioresQry = null;

        public OpcionesPerfilesAnterioresQryMD(OpcionesPerfilesAnterioresQryDP opcionesPerfilesAnterioresQry)
        {
            this.opcionesPerfilesAnterioresQry = opcionesPerfilesAnterioresQry;
        }

        protected static OpcionesPerfilesAnterioresQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            OpcionesPerfilesAnterioresQryDP opcionesPerfilesAnterioresQry = new OpcionesPerfilesAnterioresQryDP();
            opcionesPerfilesAnterioresQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            opcionesPerfilesAnterioresQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            opcionesPerfilesAnterioresQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            opcionesPerfilesAnterioresQry.UsuIdUsuario = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            opcionesPerfilesAnterioresQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            opcionesPerfilesAnterioresQry.CicloescolarId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            return opcionesPerfilesAnterioresQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count OpcionesPerfilesAnterioresQry
            DbCommand com = con.CreateCommand();
            String countOpcionesPerfilesAnterioresQry = String.Format(CultureInfo.CurrentCulture,OpcionesPerfilesAnteriores.OpcionesPerfilesAnterioresQry.OpcionesPerfilesAnterioresQryCount,"");
            com.CommandText = countOpcionesPerfilesAnterioresQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count OpcionesPerfilesAnterioresQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcionesPerfilesAnterioresQry = String.Format(CultureInfo.CurrentCulture,OpcionesPerfilesAnteriores.OpcionesPerfilesAnterioresQry.OpcionesPerfilesAnterioresQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcionesPerfilesAnterioresQry);
                #endregion
            }
            return resultado;
        }
        public static OpcionesPerfilesAnterioresQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List OpcionesPerfilesAnterioresQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcionesPerfilesAnterioresQry = String.Format(CultureInfo.CurrentCulture, OpcionesPerfilesAnteriores.OpcionesPerfilesAnterioresQry.OpcionesPerfilesAnterioresQrySelect, "", ConstantesGlobales.IncluyeRows);
            listOpcionesPerfilesAnterioresQry = listOpcionesPerfilesAnterioresQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listOpcionesPerfilesAnterioresQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load OpcionesPerfilesAnterioresQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista OpcionesPerfilesAnterioresQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista OpcionesPerfilesAnterioresQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcionesPerfilesAnterioresQry = String.Format(CultureInfo.CurrentCulture,OpcionesPerfilesAnteriores.OpcionesPerfilesAnterioresQry.OpcionesPerfilesAnterioresQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcionesPerfilesAnterioresQry);
                #endregion
            }
            return (OpcionesPerfilesAnterioresQryDP[])lista.ToArray(typeof(OpcionesPerfilesAnterioresQryDP));
        }
        public static Int64 CountForOpcionesAsignadas(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aUsuIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar)
        {
            Int64 resultado = 0;
            #region SQL CountForOpcionesAsignadas OpcionesPerfilesAnterioresQry
            DbCommand com = con.CreateCommand();
            String countOpcionesPerfilesAnterioresQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture,OpcionesPerfilesAnteriores.OpcionesPerfilesAnterioresQry.OpcionesPerfilesAnterioresQryCount,"");
            countOpcionesPerfilesAnterioresQryForOpcionesAsignadas += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aUsuIdUsuario != -1)
            {
                countOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} up.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} pa.Bit_activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdCicloescolar != -1)
            {
                countOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} up.Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countOpcionesPerfilesAnterioresQryForOpcionesAsignadas;
            #endregion
            #region Parametros countOpcionesPerfilesAnterioresQryForOpcionesAsignadas
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForOpcionesAsignadas OpcionesPerfilesAnterioresQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcionesPerfilesAnterioresQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture,OpcionesPerfilesAnteriores.OpcionesPerfilesAnterioresQry.OpcionesPerfilesAnterioresQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcionesPerfilesAnterioresQryForOpcionesAsignadas);
                #endregion
            }
            return resultado;
        }
        public static OpcionesPerfilesAnterioresQryDP[] ListForOpcionesAsignadas(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aUsuIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar)
        {
            #region SQL List OpcionesPerfilesAnterioresQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcionesPerfilesAnterioresQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture, OpcionesPerfilesAnteriores.OpcionesPerfilesAnterioresQry.OpcionesPerfilesAnterioresQrySelect, "", ConstantesGlobales.IncluyeRows);
            listOpcionesPerfilesAnterioresQryForOpcionesAsignadas = listOpcionesPerfilesAnterioresQryForOpcionesAsignadas.Substring(6);
            listOpcionesPerfilesAnterioresQryForOpcionesAsignadas += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} pa.Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aUsuIdUsuario != -1)
            {
                listOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} up.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} pa.Bit_activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdCicloescolar != -1)
            {
                listOpcionesPerfilesAnterioresQryForOpcionesAsignadas += String.Format("    {1} up.Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listOpcionesPerfilesAnterioresQryForOpcionesAsignadas, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load OpcionesPerfilesAnterioresQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista OpcionesPerfilesAnterioresQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista OpcionesPerfilesAnterioresQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcionesPerfilesAnterioresQry = String.Format(CultureInfo.CurrentCulture,OpcionesPerfilesAnteriores.OpcionesPerfilesAnterioresQry.OpcionesPerfilesAnterioresQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcionesPerfilesAnterioresQry);
                #endregion
            }
            return (OpcionesPerfilesAnterioresQryDP[])lista.ToArray(typeof(OpcionesPerfilesAnterioresQryDP));
        }
    }
}
