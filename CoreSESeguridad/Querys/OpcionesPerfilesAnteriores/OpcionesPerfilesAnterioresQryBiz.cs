using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class OpcionesPerfilesAnterioresQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = OpcionesPerfilesAnterioresQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un OpcionesPerfilesAnterioresQry!", ex);
            }
            return resultado;
        }

        public static OpcionesPerfilesAnterioresQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            OpcionesPerfilesAnterioresQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = OpcionesPerfilesAnterioresQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de OpcionesPerfilesAnterioresQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForOpcionesAsignadas(DbConnection con, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aUsuIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = OpcionesPerfilesAnterioresQryMD.CountForOpcionesAsignadas(con, tran, aIdSistema, aIdModulo, aIdOpcion, aUsuIdUsuario, aBitActivo, aIdCicloescolar);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un OpcionesPerfilesAnterioresQry!", ex);
            }
            return resultado;
        }

        public static OpcionesPerfilesAnterioresQryDP[] LoadListForOpcionesAsignadas(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aUsuIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar) 
        {
            OpcionesPerfilesAnterioresQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = OpcionesPerfilesAnterioresQryMD.ListForOpcionesAsignadas(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion, aUsuIdUsuario, aBitActivo, aIdCicloescolar);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de OpcionesPerfilesAnterioresQry!", ex);
            }
            return resultado;

        }
    }
}
