using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class NivelTrabajoQryMD
    {
        private NivelTrabajoQryDP nivelTrabajoQry = null;

        public NivelTrabajoQryMD(NivelTrabajoQryDP nivelTrabajoQry)
        {
            this.nivelTrabajoQry = nivelTrabajoQry;
        }

        protected static NivelTrabajoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            NivelTrabajoQryDP nivelTrabajoQry = new NivelTrabajoQryDP();
            nivelTrabajoQry.NiveltrabajoId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            nivelTrabajoQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            nivelTrabajoQry.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            nivelTrabajoQry.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            nivelTrabajoQry.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            nivelTrabajoQry.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            return nivelTrabajoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count NivelTrabajoQry
            DbCommand com = con.CreateCommand();
            String countNivelTrabajoQry = String.Format(CultureInfo.CurrentCulture,NivelTrabajo.NivelTrabajoQry.NivelTrabajoQryCount,"");
            com.CommandText = countNivelTrabajoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count NivelTrabajoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountNivelTrabajoQry = String.Format(CultureInfo.CurrentCulture,NivelTrabajo.NivelTrabajoQry.NivelTrabajoQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountNivelTrabajoQry);
                #endregion
            }
            return resultado;
        }
        public static NivelTrabajoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List NivelTrabajoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNivelTrabajoQry = String.Format(CultureInfo.CurrentCulture, NivelTrabajo.NivelTrabajoQry.NivelTrabajoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listNivelTrabajoQry = listNivelTrabajoQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listNivelTrabajoQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load NivelTrabajoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista NivelTrabajoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista NivelTrabajoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaNivelTrabajoQry = String.Format(CultureInfo.CurrentCulture,NivelTrabajo.NivelTrabajoQry.NivelTrabajoQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaNivelTrabajoQry);
                #endregion
            }
            return (NivelTrabajoQryDP[])lista.ToArray(typeof(NivelTrabajoQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion NivelTrabajoQry
            DbCommand com = con.CreateCommand();
            String countNivelTrabajoQryForDescripcion = String.Format(CultureInfo.CurrentCulture,NivelTrabajo.NivelTrabajoQry.NivelTrabajoQryCount,"");
            countNivelTrabajoQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countNivelTrabajoQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countNivelTrabajoQryForDescripcion;
            #endregion
            #region Parametros countNivelTrabajoQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion NivelTrabajoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountNivelTrabajoQryForDescripcion = String.Format(CultureInfo.CurrentCulture,NivelTrabajo.NivelTrabajoQry.NivelTrabajoQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountNivelTrabajoQryForDescripcion);
                #endregion
            }
            return resultado;
        }
        public static NivelTrabajoQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre)
        {
            #region SQL List NivelTrabajoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNivelTrabajoQryForDescripcion = String.Format(CultureInfo.CurrentCulture, NivelTrabajo.NivelTrabajoQry.NivelTrabajoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listNivelTrabajoQryForDescripcion = listNivelTrabajoQryForDescripcion.Substring(6);
            listNivelTrabajoQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listNivelTrabajoQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listNivelTrabajoQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load NivelTrabajoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista NivelTrabajoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista NivelTrabajoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaNivelTrabajoQry = String.Format(CultureInfo.CurrentCulture,NivelTrabajo.NivelTrabajoQry.NivelTrabajoQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaNivelTrabajoQry);
                #endregion
            }
            return (NivelTrabajoQryDP[])lista.ToArray(typeof(NivelTrabajoQryDP));
        }
    }
}
