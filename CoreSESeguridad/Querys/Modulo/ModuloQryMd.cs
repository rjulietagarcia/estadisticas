using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class ModuloQryMD
    {
        private ModuloQryDP moduloQry = null;

        public ModuloQryMD(ModuloQryDP moduloQry)
        {
            this.moduloQry = moduloQry;
        }

        protected static ModuloQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ModuloQryDP moduloQry = new ModuloQryDP();
            moduloQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            moduloQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            moduloQry.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            moduloQry.Abreviatura = dr.IsDBNull(3) ? "" : dr.GetString(3);
            moduloQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            moduloQry.UsuarioId = dr.IsDBNull(5) ? 0 : dr.GetInt32(5);
            moduloQry.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            moduloQry.FechaInicio = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            moduloQry.FechaFin = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            moduloQry.CarpetaModulo = dr.IsDBNull(9) ? "" : dr.GetString(9);
            return moduloQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count ModuloQry
            DbCommand com = con.CreateCommand();
            String countModuloQry = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloQry.ModuloQryCount,"");
            com.CommandText = countModuloQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count ModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountModuloQry = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloQry.ModuloQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountModuloQry);
                #endregion
            }
            return resultado;
        }
        public static ModuloQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List ModuloQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listModuloQry = String.Format(CultureInfo.CurrentCulture, Modulo.ModuloQry.ModuloQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listModuloQry = listModuloQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listModuloQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listModuloQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load ModuloQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ModuloQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaModuloQry = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloQry.ModuloQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaModuloQry);
                #endregion
            }
            return (ModuloQryDP[])lista.ToArray(typeof(ModuloQryDP));
        }
        public static Int64 CountForModulo(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo)
        {
            Int64 resultado = 0;
            #region SQL CountForModulo ModuloQry
            DbCommand com = con.CreateCommand();
            String countModuloQryForModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloQry.ModuloQryCount,"");
            countModuloQryForModulo += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countModuloQryForModulo += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countModuloQryForModulo += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countModuloQryForModulo;
            #endregion
            #region Parametros countModuloQryForModulo
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForModulo ModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountModuloQryForModulo = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloQry.ModuloQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountModuloQryForModulo);
                #endregion
            }
            return resultado;
        }
        public static ModuloQryDP[] ListForModulo(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo)
        {
            #region SQL List ModuloQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listModuloQryForModulo = String.Format(CultureInfo.CurrentCulture, Modulo.ModuloQry.ModuloQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listModuloQryForModulo = listModuloQryForModulo.Substring(6);
            listModuloQryForModulo += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listModuloQryForModulo += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listModuloQryForModulo += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listModuloQryForModulo, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listModuloQryForModulo;
            DbDataReader dr;
            #endregion
            #region Parametros Load ModuloQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ModuloQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaModuloQry = String.Format(CultureInfo.CurrentCulture,Modulo.ModuloQry.ModuloQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaModuloQry);
                #endregion
            }
            return (ModuloQryDP[])lista.ToArray(typeof(ModuloQryDP));
        }
    }
}
