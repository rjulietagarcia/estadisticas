using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "TreeDenegadoQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 13 de julio de 2009.</Para>
    /// <Para>Hora: 05:07:41 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "TreeDenegadoQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>Llave</term><description>Descripcion Llave</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuario</term><description>Descripcion UsuIdUsuario</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TreeDenegadoQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// TreeDenegadoQryDTO treedenegadoqry = new TreeDenegadoQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("TreeDenegadoQry")]
    public class TreeDenegadoQryDP
    {
        #region Definicion de campos privados.
        private String llave;
        private Int32 usuIdUsuario;
        private Byte sistemaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// Llave
        /// </summary> 
        [XmlElement("Llave")]
        public String Llave
        {
            get {
                    return llave; 
            }
            set {
                    llave = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuario
        /// </summary> 
        [XmlElement("UsuIdUsuario")]
        public Int32 UsuIdUsuario
        {
            get {
                    return usuIdUsuario; 
            }
            set {
                    usuIdUsuario = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        #endregion.
    }
}
