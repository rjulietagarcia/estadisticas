using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TreeDenegadoQryMD
    {
        private TreeDenegadoQryDP treeDenegadoQry = null;

        public TreeDenegadoQryMD(TreeDenegadoQryDP treeDenegadoQry)
        {
            this.treeDenegadoQry = treeDenegadoQry;
        }

        protected static TreeDenegadoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TreeDenegadoQryDP treeDenegadoQry = new TreeDenegadoQryDP();
            treeDenegadoQry.Llave = dr.IsDBNull(0) ? "" : dr.GetString(0);
            treeDenegadoQry.UsuIdUsuario = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            treeDenegadoQry.SistemaId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            return treeDenegadoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TreeDenegadoQry
            DbCommand com = con.CreateCommand();
            String countTreeDenegadoQry = String.Format(CultureInfo.CurrentCulture,TreeDenegado.TreeDenegadoQry.TreeDenegadoQryCount,"");
            com.CommandText = countTreeDenegadoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TreeDenegadoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeDenegadoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TreeDenegadoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeDenegadoQry = String.Format(CultureInfo.CurrentCulture, TreeDenegado.TreeDenegadoQry.TreeDenegadoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreeDenegadoQry = listTreeDenegadoQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreeDenegadoQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeDenegadoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeDenegadoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeDenegadoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeDenegadoQryDP[])lista.ToArray(typeof(TreeDenegadoQryDP));
        }
        public static Int64 CountForUsuarioSistema(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuarioSistema TreeDenegadoQry
            DbCommand com = con.CreateCommand();
            String countTreeDenegadoQryForUsuarioSistema = String.Format(CultureInfo.CurrentCulture,TreeDenegado.TreeDenegadoQry.TreeDenegadoQryCount,"");
            countTreeDenegadoQryForUsuarioSistema += " WHERE \n";
            String delimitador = "";
            countTreeDenegadoQryForUsuarioSistema += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdSistema != 0)
            {
                countTreeDenegadoQryForUsuarioSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTreeDenegadoQryForUsuarioSistema;
            #endregion
            #region Parametros countTreeDenegadoQryForUsuarioSistema
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuarioSistema TreeDenegadoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeDenegadoQryDP[] ListForUsuarioSistema(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Byte aIdSistema)
        {
            #region SQL List TreeDenegadoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeDenegadoQryForUsuarioSistema = String.Format(CultureInfo.CurrentCulture, TreeDenegado.TreeDenegadoQry.TreeDenegadoQrySelect, "",
                String.Format("{0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix)
                );

            //listTreeDenegadoQryForUsuarioSistema = listTreeDenegadoQryForUsuarioSistema.Substring(6);
            //listTreeDenegadoQryForUsuarioSistema += " WHERE \n";
            //String delimitador = "";
            //listTreeDenegadoQryForUsuarioSistema += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
            //    delimitador);
            //delimitador = " AND ";
            //if (aIdSistema != 0)
            //{
            //    listTreeDenegadoQryForUsuarioSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            String filtraList = listTreeDenegadoQryForUsuarioSistema;
            //String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //     ConstantesGlobales.ParameterPrefix,
            //     listTreeDenegadoQryForUsuarioSistema, "",
            //     (startRowIndex + maximumRows).ToString(),
            //     startRowIndex,
            //     maximumRows,
            //     "" // DISTINCT si es que aplica.
            //     );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeDenegadoQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            else
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}IdSistema", ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, 1);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeDenegadoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeDenegadoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeDenegadoQryDP[])lista.ToArray(typeof(TreeDenegadoQryDP));
        }
    }
}
