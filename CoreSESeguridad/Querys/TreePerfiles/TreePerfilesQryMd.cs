using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TreePerfilesQryMD
    {
        private TreePerfilesQryDP treePerfilesQry = null;

        public TreePerfilesQryMD(TreePerfilesQryDP treePerfilesQry)
        {
            this.treePerfilesQry = treePerfilesQry;
        }

        protected static TreePerfilesQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TreePerfilesQryDP treePerfilesQry = new TreePerfilesQryDP();
            treePerfilesQry.Llave = dr.IsDBNull(0) ? "" : dr.GetString(0);
            treePerfilesQry.UsuIdUsuario = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            treePerfilesQry.SistemaId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            return treePerfilesQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TreePerfilesQry
            DbCommand com = con.CreateCommand();
            String countTreePerfilesQry = String.Format(CultureInfo.CurrentCulture,TreePerfiles.TreePerfilesQry.TreePerfilesQryCount,"");
            com.CommandText = countTreePerfilesQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TreePerfilesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreePerfilesQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TreePerfilesQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreePerfilesQry = String.Format(CultureInfo.CurrentCulture, TreePerfiles.TreePerfilesQry.TreePerfilesQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreePerfilesQry = listTreePerfilesQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreePerfilesQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreePerfilesQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreePerfilesQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreePerfilesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreePerfilesQryDP[])lista.ToArray(typeof(TreePerfilesQryDP));
        }
        public static Int64 CountForUsuarioSistema(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuarioSistema TreePerfilesQry
            DbCommand com = con.CreateCommand();
            String countTreePerfilesQryForUsuarioSistema = String.Format(CultureInfo.CurrentCulture,TreePerfiles.TreePerfilesQry.TreePerfilesQryCount,"");
            countTreePerfilesQryForUsuarioSistema += " WHERE \n";
            String delimitador = "";
            countTreePerfilesQryForUsuarioSistema += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdSistema != 0)
            {
                countTreePerfilesQryForUsuarioSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTreePerfilesQryForUsuarioSistema;
            #endregion
            #region Parametros countTreePerfilesQryForUsuarioSistema
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuarioSistema TreePerfilesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreePerfilesQryDP[] ListForUsuarioSistema(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Byte aIdSistema)
        {
            #region SQL List TreePerfilesQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreePerfilesQryForUsuarioSistema = String.Format(CultureInfo.CurrentCulture, TreePerfiles.TreePerfilesQry.TreePerfilesQrySelect, "",
                String.Format("{0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix)
                );

            //listTreePerfilesQryForUsuarioSistema = listTreePerfilesQryForUsuarioSistema.Substring(6);
            //listTreePerfilesQryForUsuarioSistema += " WHERE \n";
            //String delimitador = "";
            //listTreePerfilesQryForUsuarioSistema += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
            //    delimitador);
            //delimitador = " AND ";
            //if (aIdSistema != 0)
            //{
            //    listTreePerfilesQryForUsuarioSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            String filtraList = listTreePerfilesQryForUsuarioSistema;
            //String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //     ConstantesGlobales.ParameterPrefix,
            //     listTreePerfilesQryForUsuarioSistema, "",
            //     (startRowIndex + maximumRows).ToString(),
            //     startRowIndex,
            //     maximumRows,
            //     "" // DISTINCT si es que aplica.
            //     );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreePerfilesQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            else
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}IdSistema", ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, 1);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreePerfilesQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreePerfilesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreePerfilesQryDP[])lista.ToArray(typeof(TreePerfilesQryDP));
        }
    }
}
