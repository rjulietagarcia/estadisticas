using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class PermisoLlaveCarpetaQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PermisoLlaveCarpetaQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PermisoLlaveCarpetaQry!", ex);
            }
            return resultado;
        }

        public static PermisoLlaveCarpetaQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PermisoLlaveCarpetaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PermisoLlaveCarpetaQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PermisoLlaveCarpetaQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLlaves(DbConnection con, Int32 aUsuIdUsuario, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PermisoLlaveCarpetaQryMD.CountForLlaves(con, tran, aUsuIdUsuario, aIdSistema, aIdModulo, aIdOpcion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PermisoLlaveCarpetaQry!", ex);
            }
            return resultado;
        }

        public static PermisoLlaveCarpetaQryDP[] LoadListForLlaves(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion) 
        {
            PermisoLlaveCarpetaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PermisoLlaveCarpetaQryMD.ListForLlaves(con, tran, startRowIndex, maximumRows, aUsuIdUsuario, aIdSistema, aIdModulo, aIdOpcion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PermisoLlaveCarpetaQry!", ex);
            }
            return resultado;

        }
    }
}
