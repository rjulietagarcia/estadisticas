using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class PermisoLlaveCarpetaQryMD
    {
        private PermisoLlaveCarpetaQryDP permisoLlaveCarpetaQry = null;

        public PermisoLlaveCarpetaQryMD(PermisoLlaveCarpetaQryDP permisoLlaveCarpetaQry)
        {
            this.permisoLlaveCarpetaQry = permisoLlaveCarpetaQry;
        }

        protected static PermisoLlaveCarpetaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PermisoLlaveCarpetaQryDP permisoLlaveCarpetaQry = new PermisoLlaveCarpetaQryDP();
            permisoLlaveCarpetaQry.Llave = dr.IsDBNull(0) ? "" : dr.GetString(0);
            permisoLlaveCarpetaQry.UsuIdUsuario = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            permisoLlaveCarpetaQry.SistemaId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            permisoLlaveCarpetaQry.ModuloId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            permisoLlaveCarpetaQry.OpcionId = dr.IsDBNull(4) ? (Byte)0 : dr.GetByte(4);
            return permisoLlaveCarpetaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PermisoLlaveCarpetaQry
            DbCommand com = con.CreateCommand();
            String countPermisoLlaveCarpetaQry = String.Format(CultureInfo.CurrentCulture,PermisoLlaveCarpeta.PermisoLlaveCarpetaQry.PermisoLlaveCarpetaQryCount,"");
            com.CommandText = countPermisoLlaveCarpetaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PermisoLlaveCarpetaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PermisoLlaveCarpetaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PermisoLlaveCarpetaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPermisoLlaveCarpetaQry = String.Format(CultureInfo.CurrentCulture, PermisoLlaveCarpeta.PermisoLlaveCarpetaQry.PermisoLlaveCarpetaQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listPermisoLlaveCarpetaQry = listPermisoLlaveCarpetaQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPermisoLlaveCarpetaQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPermisoLlaveCarpetaQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load PermisoLlaveCarpetaQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PermisoLlaveCarpetaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PermisoLlaveCarpetaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PermisoLlaveCarpetaQryDP[])lista.ToArray(typeof(PermisoLlaveCarpetaQryDP));
        }
        public static Int64 CountForLlaves(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion)
        {
            Int64 resultado = 0;
            #region SQL CountForLlaves PermisoLlaveCarpetaQry
            DbCommand com = con.CreateCommand();
            String countPermisoLlaveCarpetaQryForLlaves = String.Format(CultureInfo.CurrentCulture,PermisoLlaveCarpeta.PermisoLlaveCarpetaQry.PermisoLlaveCarpetaQryCount,"");
            countPermisoLlaveCarpetaQryForLlaves += " WHERE \n";
            String delimitador = "";
            if (aUsuIdUsuario != -1)
            {
                countPermisoLlaveCarpetaQryForLlaves += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSistema != 0)
            {
                countPermisoLlaveCarpetaQryForLlaves += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countPermisoLlaveCarpetaQryForLlaves += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countPermisoLlaveCarpetaQryForLlaves += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countPermisoLlaveCarpetaQryForLlaves;
            #endregion
            #region Parametros countPermisoLlaveCarpetaQryForLlaves
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlaves PermisoLlaveCarpetaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PermisoLlaveCarpetaQryDP[] ListForLlaves(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion)
        {
            #region SQL List PermisoLlaveCarpetaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPermisoLlaveCarpetaQryForLlaves =
                String.Format(CultureInfo.CurrentCulture, PermisoLlaveCarpeta.PermisoLlaveCarpetaQry.PermisoLlaveCarpetaQrySelect, "", 
                String.Format("{0}UsuIdUsuario", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdSistema", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdModulo", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdOpcion", Model.ConstantesGlobales.ParameterPrefix)
                );
            //listPermisoLlaveCarpetaQryForLlaves = listPermisoLlaveCarpetaQryForLlaves.Substring(6);
            //listPermisoLlaveCarpetaQryForLlaves += " WHERE \n";
            
            //String filtraList = String.Format(CultureInfo.CurrentCulture, "{1}",
            //    ConstantesGlobales.ParameterPrefix,
            //    listPermisoLlaveCarpetaQryForLlaves, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );

            com.CommandText = listPermisoLlaveCarpetaQryForLlaves;
            DbDataReader dr;
            #endregion
            #region Parametros Load PermisoLlaveCarpetaQry
            // Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            // Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PermisoLlaveCarpetaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PermisoLlaveCarpetaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PermisoLlaveCarpetaQryDP[])lista.ToArray(typeof(PermisoLlaveCarpetaQryDP));
        }
    }
}
