using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class PaisQryMD
    {
        private PaisQryDP paisQry = null;

        public PaisQryMD(PaisQryDP paisQry)
        {
            this.paisQry = paisQry;
        }

        protected static PaisQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PaisQryDP paisQry = new PaisQryDP();
            paisQry.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            paisQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            paisQry.Nacionalidad = dr.IsDBNull(2) ? "" : dr.GetString(2);
            paisQry.Abreviatura = dr.IsDBNull(3) ? "" : dr.GetString(3);
            paisQry.RegioncontinenteId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            paisQry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            paisQry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            paisQry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            return paisQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PaisQry
            DbCommand com = con.CreateCommand();
            String countPaisQry = String.Format(CultureInfo.CurrentCulture,Pais.PaisQry.PaisQryCount,"");
            com.CommandText = countPaisQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PaisQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPaisQry = String.Format(CultureInfo.CurrentCulture,Pais.PaisQry.PaisQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPaisQry);
                #endregion
            }
            return resultado;
        }
        public static PaisQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PaisQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPaisQry = String.Format(CultureInfo.CurrentCulture, Pais.PaisQry.PaisQrySelect, "", ConstantesGlobales.IncluyeRows);
            listPaisQry = listPaisQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPaisQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load PaisQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PaisQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PaisQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPaisQry = String.Format(CultureInfo.CurrentCulture,Pais.PaisQry.PaisQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPaisQry);
                #endregion
            }
            return (PaisQryDP[])lista.ToArray(typeof(PaisQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion PaisQry
            DbCommand com = con.CreateCommand();
            String countPaisQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Pais.PaisQry.PaisQryCount,"");
            countPaisQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countPaisQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countPaisQryForDescripcion;
            #endregion
            #region Parametros countPaisQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion PaisQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPaisQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Pais.PaisQry.PaisQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPaisQryForDescripcion);
                #endregion
            }
            return resultado;
        }
        public static PaisQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre)
        {
            #region SQL List PaisQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPaisQryForDescripcion = String.Format(CultureInfo.CurrentCulture, Pais.PaisQry.PaisQrySelect, "", ConstantesGlobales.IncluyeRows);
            listPaisQryForDescripcion = listPaisQryForDescripcion.Substring(6);
            listPaisQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listPaisQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPaisQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load PaisQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PaisQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PaisQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPaisQry = String.Format(CultureInfo.CurrentCulture,Pais.PaisQry.PaisQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPaisQry);
                #endregion
            }
            return (PaisQryDP[])lista.ToArray(typeof(PaisQryDP));
        }
    }
}
