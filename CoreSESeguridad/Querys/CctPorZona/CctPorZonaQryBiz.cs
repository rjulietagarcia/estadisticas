using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CctPorZonaQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CctPorZonaQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CctPorZonaQry!", ex);
            }
            return resultado;
        }

        public static CctPorZonaQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CctPorZonaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CctPorZonaQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CctPorZonaQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, String aNombre, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CctPorZonaQryMD.CountForDescripcion(con, tran, aNombre, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CctPorZonaQry!", ex);
            }
            return resultado;
        }

        public static CctPorZonaQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona) 
        {
            CctPorZonaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CctPorZonaQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aNombre, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CctPorZonaQry!", ex);
            }
            return resultado;

        }
    }
}
