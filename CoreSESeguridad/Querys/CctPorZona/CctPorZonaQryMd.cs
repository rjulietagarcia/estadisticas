using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CctPorZonaQryMD
    {
        private CctPorZonaQryDP cctPorZonaQry = null;

        public CctPorZonaQryMD(CctPorZonaQryDP cctPorZonaQry)
        {
            this.cctPorZonaQry = cctPorZonaQry;
        }

        protected static CctPorZonaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CctPorZonaQryDP cctPorZonaQry = new CctPorZonaQryDP();
            cctPorZonaQry.CentrotrabajoId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            cctPorZonaQry.PaisId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            cctPorZonaQry.EntidadId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            cctPorZonaQry.RegionId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            cctPorZonaQry.ZonaId = dr.IsDBNull(4) ? (short)0 : dr.GetInt16(4);
            cctPorZonaQry.SostenimientoId = dr.IsDBNull(5) ? (Byte)0 : dr.GetByte(5);
            cctPorZonaQry.Clave = dr.IsDBNull(6) ? "" : dr.GetString(6);
            cctPorZonaQry.Turno3d = dr.IsDBNull(7) ? "" : dr.GetString(7);
            cctPorZonaQry.Bitprovisional = dr.IsDBNull(8) ? false : dr.GetBoolean(8);
            cctPorZonaQry.InmuebleId = dr.IsDBNull(9) ? 0 : dr.GetInt32(9);
            cctPorZonaQry.DomicilioId = dr.IsDBNull(10) ? 0 : dr.GetInt32(10);
            cctPorZonaQry.FechaFundacion = dr.IsDBNull(11) ? "" : dr.GetDateTime(11).ToShortDateString();;
            cctPorZonaQry.FechaAlta = dr.IsDBNull(12) ? "" : dr.GetDateTime(12).ToShortDateString();;
            cctPorZonaQry.FechaClausura = dr.IsDBNull(13) ? "" : dr.GetDateTime(13).ToShortDateString();;
            cctPorZonaQry.MotivobajacentrotrabajoId = dr.IsDBNull(14) ? (short)0 : dr.GetInt16(14);
            cctPorZonaQry.FechaReapertura = dr.IsDBNull(15) ? "" : dr.GetDateTime(15).ToShortDateString();;
            cctPorZonaQry.BitActivo = dr.IsDBNull(16) ? false : dr.GetBoolean(16);
            cctPorZonaQry.UsuarioId = dr.IsDBNull(17) ? 0 : dr.GetInt32(17);
            cctPorZonaQry.FechaActualizacion = dr.IsDBNull(18) ? "" : dr.GetDateTime(18).ToShortDateString();;
            cctPorZonaQry.FechaCambio = dr.IsDBNull(19) ? "" : dr.GetDateTime(19).ToShortDateString();;
            cctPorZonaQry.Nombre = dr.IsDBNull(20) ? "" : dr.GetString(20);
            cctPorZonaQry.TipoctId = dr.IsDBNull(21) ? "" : dr.GetString(21);
            cctPorZonaQry.TipoeducacionId = dr.IsDBNull(22) ? (short)0 : dr.GetInt16(22);
            cctPorZonaQry.NivelId = dr.IsDBNull(23) ? (short)0 : dr.GetInt16(23);
            cctPorZonaQry.SubnivelId = dr.IsDBNull(24) ? (short)0 : dr.GetInt16(24);
            cctPorZonaQry.ControlId = dr.IsDBNull(25) ? (short)0 : dr.GetInt16(25);
            cctPorZonaQry.SubcontrolId = dr.IsDBNull(26) ? (short)0 : dr.GetInt16(26);
            cctPorZonaQry.DependencianormativaId = dr.IsDBNull(27) ? (short)0 : dr.GetInt16(27);
            cctPorZonaQry.DependenciaadministrativaId = dr.IsDBNull(28) ? (short)0 : dr.GetInt16(28);
            cctPorZonaQry.ServicioId = dr.IsDBNull(29) ? (short)0 : dr.GetInt16(29);
            cctPorZonaQry.SectorId = dr.IsDBNull(30) ? (short)0 : dr.GetInt16(30);
            cctPorZonaQry.EducacionfisicaId = dr.IsDBNull(31) ? (short)0 : dr.GetInt16(31);
            cctPorZonaQry.EstatusId = dr.IsDBNull(32) ? (short)0 : dr.GetInt16(32);
            cctPorZonaQry.AlmacenId = dr.IsDBNull(33) ? (short)0 : dr.GetInt16(33);
            cctPorZonaQry.PuntocardinalId = dr.IsDBNull(34) ? (short)0 : dr.GetInt16(34);
            cctPorZonaQry.NumeroIncorporacion = dr.IsDBNull(35) ? "" : dr.GetString(35);
            cctPorZonaQry.Folio = dr.IsDBNull(36) ? "" : dr.GetString(36);
            cctPorZonaQry.DependenciaoperativaId = dr.IsDBNull(37) ? (short)0 : dr.GetInt16(37);
            cctPorZonaQry.Observaciones = dr.IsDBNull(38) ? "" : dr.GetString(38);
            cctPorZonaQry.IncorporacionId = dr.IsDBNull(39) ? (short)0 : dr.GetInt16(39);
            cctPorZonaQry.Fechasol = dr.IsDBNull(40) ? "" : dr.GetDateTime(40).ToShortDateString();;
            cctPorZonaQry.ClaveinstitucionalId = dr.IsDBNull(41) ? (short)0 : dr.GetInt16(41);
            cctPorZonaQry.ClaveagrupadorId = dr.IsDBNull(42) ? (Byte)0 : dr.GetByte(42);
            cctPorZonaQry.TurnoId = dr.IsDBNull(43) ? (short)0 : dr.GetInt16(43);
            cctPorZonaQry.ValidDatos = dr.IsDBNull(44) ? 0 : dr.GetDecimal(44);
            cctPorZonaQry.NiveleducacionId = dr.IsDBNull(45) ? (Byte)0 : dr.GetByte(45);
            cctPorZonaQry.NombreTurno = dr.IsDBNull(46) ? "" : dr.GetString(46);
            return cctPorZonaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CctPorZonaQry
            DbCommand com = con.CreateCommand();
            String countCctPorZonaQry = String.Format(CultureInfo.CurrentCulture,CctPorZona.CctPorZonaQry.CctPorZonaQryCount,"");
            com.CommandText = countCctPorZonaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CctPorZonaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CctPorZonaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CctPorZonaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCctPorZonaQry = String.Format(CultureInfo.CurrentCulture, CctPorZona.CctPorZonaQry.CctPorZonaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCctPorZonaQry = listCctPorZonaQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCctPorZonaQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CctPorZonaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CctPorZonaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CctPorZonaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CctPorZonaQryDP[])lista.ToArray(typeof(CctPorZonaQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion CctPorZonaQry
            DbCommand com = con.CreateCommand();
            String countCctPorZonaQryForDescripcion = String.Format(CultureInfo.CurrentCulture,CctPorZona.CctPorZonaQry.CctPorZonaQryCount,"");
            countCctPorZonaQryForDescripcion += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countCctPorZonaQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countCctPorZonaQryForDescripcion += String.Format("    {1} cnt.id_niveleducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countCctPorZonaQryForDescripcion += String.Format("    {1} Id_Region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countCctPorZonaQryForDescripcion += String.Format("    {1} Id_Sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countCctPorZonaQryForDescripcion += String.Format("    {1} Id_Zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countCctPorZonaQryForDescripcion;
            #endregion
            #region Parametros countCctPorZonaQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion CctPorZonaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CctPorZonaQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona)
        {
            #region SQL List CctPorZonaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCctPorZonaQryForDescripcion = String.Format(CultureInfo.CurrentCulture, CctPorZona.CctPorZonaQry.CctPorZonaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCctPorZonaQryForDescripcion = listCctPorZonaQryForDescripcion.Substring(6);
            listCctPorZonaQryForDescripcion += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listCctPorZonaQryForDescripcion += String.Format("    {1} cct.Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listCctPorZonaQryForDescripcion += String.Format("    {1} cnt.id_niveleducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listCctPorZonaQryForDescripcion += String.Format("    {1} Id_Region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listCctPorZonaQryForDescripcion += String.Format("    {1} Id_Sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listCctPorZonaQryForDescripcion += String.Format("    {1} Id_Zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCctPorZonaQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CctPorZonaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CctPorZonaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CctPorZonaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CctPorZonaQryDP[])lista.ToArray(typeof(CctPorZonaQryDP));
        }
    }
}
