using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilEducacionUsuarioQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 02 de junio de 2009.</Para>
    /// <Para>Hora: 06:35:08 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilEducacionUsuarioQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuario</term><description>Descripcion UsuIdUsuario</description>
    ///    </item>
    ///    <item>
    ///        <term>NombrePerfil</term><description>Descripcion NombrePerfil</description>
    ///    </item>
    ///    <item>
    ///        <term>AbreviaturaPerfil</term><description>Descripcion AbreviaturaPerfil</description>
    ///    </item>
    ///    <item>
    ///        <term>NombreNivelTrabajo</term><description>Descripcion NombreNivelTrabajo</description>
    ///    </item>
    ///    <item>
    ///        <term>AbreviaturaNivelTrabajo</term><description>Descripcion AbreviaturaNivelTrabajo</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilEducacionUsuarioQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// PerfilEducacionUsuarioQryDTO perfileducacionusuarioqry = new PerfilEducacionUsuarioQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("PerfilEducacionUsuarioQry")]
    public class PerfilEducacionUsuarioQryDP
    {
        #region Definicion de campos privados.
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Int16 cicloescolarId;
        private Int32 usuIdUsuario;
        private String nombrePerfil;
        private String abreviaturaPerfil;
        private String nombreNivelTrabajo;
        private String abreviaturaNivelTrabajo;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuario
        /// </summary> 
        [XmlElement("UsuIdUsuario")]
        public Int32 UsuIdUsuario
        {
            get {
                    return usuIdUsuario; 
            }
            set {
                    usuIdUsuario = value; 
            }
        }

        /// <summary>
        /// NombrePerfil
        /// </summary> 
        [XmlElement("NombrePerfil")]
        public String NombrePerfil
        {
            get {
                    return nombrePerfil; 
            }
            set {
                    nombrePerfil = value; 
            }
        }

        /// <summary>
        /// AbreviaturaPerfil
        /// </summary> 
        [XmlElement("AbreviaturaPerfil")]
        public String AbreviaturaPerfil
        {
            get {
                    return abreviaturaPerfil; 
            }
            set {
                    abreviaturaPerfil = value; 
            }
        }

        /// <summary>
        /// NombreNivelTrabajo
        /// </summary> 
        [XmlElement("NombreNivelTrabajo")]
        public String NombreNivelTrabajo
        {
            get {
                    return nombreNivelTrabajo; 
            }
            set {
                    nombreNivelTrabajo = value; 
            }
        }

        /// <summary>
        /// AbreviaturaNivelTrabajo
        /// </summary> 
        [XmlElement("AbreviaturaNivelTrabajo")]
        public String AbreviaturaNivelTrabajo
        {
            get {
                    return abreviaturaNivelTrabajo; 
            }
            set {
                    abreviaturaNivelTrabajo = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        #endregion.
    }
}
