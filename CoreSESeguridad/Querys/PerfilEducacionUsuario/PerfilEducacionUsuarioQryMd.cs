using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class PerfilEducacionUsuarioQryMD
    {
        private PerfilEducacionUsuarioQryDP perfilEducacionUsuarioQry = null;

        public PerfilEducacionUsuarioQryMD(PerfilEducacionUsuarioQryDP perfilEducacionUsuarioQry)
        {
            this.perfilEducacionUsuarioQry = perfilEducacionUsuarioQry;
        }

        protected static PerfilEducacionUsuarioQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilEducacionUsuarioQryDP perfilEducacionUsuarioQry = new PerfilEducacionUsuarioQryDP();
            perfilEducacionUsuarioQry.PerfilEducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            perfilEducacionUsuarioQry.NiveltrabajoId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            perfilEducacionUsuarioQry.CicloescolarId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            perfilEducacionUsuarioQry.UsuIdUsuario = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            perfilEducacionUsuarioQry.NombrePerfil = dr.IsDBNull(4) ? "" : dr.GetString(4);
            perfilEducacionUsuarioQry.AbreviaturaPerfil = dr.IsDBNull(5) ? "" : dr.GetString(5);
            perfilEducacionUsuarioQry.NombreNivelTrabajo = dr.IsDBNull(6) ? "" : dr.GetString(6);
            perfilEducacionUsuarioQry.AbreviaturaNivelTrabajo = dr.IsDBNull(7) ? "" : dr.GetString(7);
            perfilEducacionUsuarioQry.BitActivo = dr.IsDBNull(8) ? false : dr.GetBoolean(8);
            perfilEducacionUsuarioQry.UsuarioId = dr.IsDBNull(9) ? 0 : dr.GetInt32(9);
            perfilEducacionUsuarioQry.FechaActualizacion = dr.IsDBNull(10) ? "" : dr.GetDateTime(10).ToShortDateString();;
            return perfilEducacionUsuarioQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilEducacionUsuarioQry
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionUsuarioQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionUsuario.PerfilEducacionUsuarioQry.PerfilEducacionUsuarioQryCount,"");
            com.CommandText = countPerfilEducacionUsuarioQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacionUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPerfilEducacionUsuarioQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionUsuario.PerfilEducacionUsuarioQry.PerfilEducacionUsuarioQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPerfilEducacionUsuarioQry);
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionUsuarioQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilEducacionUsuarioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionUsuarioQry = String.Format(CultureInfo.CurrentCulture, PerfilEducacionUsuario.PerfilEducacionUsuarioQry.PerfilEducacionUsuarioQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listPerfilEducacionUsuarioQry = listPerfilEducacionUsuarioQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPerfilEducacionUsuarioQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPerfilEducacionUsuarioQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionUsuarioQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionUsuarioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPerfilEducacionUsuarioQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionUsuario.PerfilEducacionUsuarioQry.PerfilEducacionUsuarioQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPerfilEducacionUsuarioQry);
                #endregion
            }
            return (PerfilEducacionUsuarioQryDP[])lista.ToArray(typeof(PerfilEducacionUsuarioQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario, Int16 aIdCicloescolar)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario PerfilEducacionUsuarioQry
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionUsuarioQryForUsuario = String.Format(CultureInfo.CurrentCulture,PerfilEducacionUsuario.PerfilEducacionUsuarioQry.PerfilEducacionUsuarioQryCount,"");
            countPerfilEducacionUsuarioQryForUsuario += " WHERE \n";
            String delimitador = "";
            if (aUsuIdUsuario != -1)
            {
                countPerfilEducacionUsuarioQryForUsuario += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdCicloescolar != -1)
            {
                countPerfilEducacionUsuarioQryForUsuario += String.Format("    {1} Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countPerfilEducacionUsuarioQryForUsuario;
            #endregion
            #region Parametros countPerfilEducacionUsuarioQryForUsuario
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario PerfilEducacionUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPerfilEducacionUsuarioQryForUsuario = String.Format(CultureInfo.CurrentCulture,PerfilEducacionUsuario.PerfilEducacionUsuarioQry.PerfilEducacionUsuarioQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPerfilEducacionUsuarioQryForUsuario);
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionUsuarioQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Int16 aIdCicloescolar)
        {
            #region SQL List PerfilEducacionUsuarioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionUsuarioQryForUsuario = String.Format(CultureInfo.CurrentCulture, PerfilEducacionUsuario.PerfilEducacionUsuarioQry.PerfilEducacionUsuarioQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listPerfilEducacionUsuarioQryForUsuario = listPerfilEducacionUsuarioQryForUsuario.Substring(6);
            listPerfilEducacionUsuarioQryForUsuario += " WHERE \n";
            String delimitador = "";
            if (aUsuIdUsuario != -1)
            {
                listPerfilEducacionUsuarioQryForUsuario += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdCicloescolar != -1)
            {
                listPerfilEducacionUsuarioQryForUsuario += String.Format("    {1} Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPerfilEducacionUsuarioQryForUsuario, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPerfilEducacionUsuarioQryForUsuario;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionUsuarioQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionUsuarioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                // String errorLoadListaPerfilEducacionUsuarioQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionUsuario.PerfilEducacionUsuarioQry.PerfilEducacionUsuarioQrySelect, "" );
                // System.Diagnostics.Debug.WriteLine(errorLoadListaPerfilEducacionUsuarioQry);
                #endregion
            }
            return (PerfilEducacionUsuarioQryDP[])lista.ToArray(typeof(PerfilEducacionUsuarioQryDP));
        }
    }
}
