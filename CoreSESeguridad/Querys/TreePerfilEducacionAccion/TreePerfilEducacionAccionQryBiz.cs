using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class TreePerfilEducacionAccionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreePerfilEducacionAccionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreePerfilEducacionAccionQry!", ex);
            }
            return resultado;
        }

        public static TreePerfilEducacionAccionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TreePerfilEducacionAccionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreePerfilEducacionAccionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreePerfilEducacionAccionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForNivelTrabajoSistema(DbConnection con, Byte aIdNiveltrabajo, Byte aIdSistema) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreePerfilEducacionAccionQryMD.CountForNivelTrabajoSistema(con, tran, aIdNiveltrabajo, aIdSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreePerfilEducacionAccionQry!", ex);
            }
            return resultado;
        }

        public static TreePerfilEducacionAccionQryDP[] LoadListForNivelTrabajoSistema(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveltrabajo, Byte aIdSistema, Byte aIdPerfil) 
        {
            TreePerfilEducacionAccionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreePerfilEducacionAccionQryMD.ListForNivelTrabajoSistema(con, tran, startRowIndex, maximumRows, aIdNiveltrabajo, aIdSistema, aIdPerfil);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreePerfilEducacionAccionQry!", ex);
            }
            return resultado;

        }
    }
}
