using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TreePerfilEducacionAccionQryMD
    {
        private TreePerfilEducacionAccionQryDP treePerfilEducacionAccionQry = null;

        public TreePerfilEducacionAccionQryMD(TreePerfilEducacionAccionQryDP treePerfilEducacionAccionQry)
        {
            this.treePerfilEducacionAccionQry = treePerfilEducacionAccionQry;
        }

        protected static TreePerfilEducacionAccionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TreePerfilEducacionAccionQryDP treePerfilEducacionAccionQry = new TreePerfilEducacionAccionQryDP();
            treePerfilEducacionAccionQry.Llave = dr.IsDBNull(0) ? "" : dr.GetString(0);
            treePerfilEducacionAccionQry.NiveltrabajoId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            treePerfilEducacionAccionQry.SistemaId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            return treePerfilEducacionAccionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TreePerfilEducacionAccionQry
            DbCommand com = con.CreateCommand();
            String countTreePerfilEducacionAccionQry = String.Format(CultureInfo.CurrentCulture,TreePerfilEducacionAccion.TreePerfilEducacionAccionQry.TreePerfilEducacionAccionQryCount,"");
            com.CommandText = countTreePerfilEducacionAccionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TreePerfilEducacionAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreePerfilEducacionAccionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TreePerfilEducacionAccionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreePerfilEducacionAccionQry = String.Format(CultureInfo.CurrentCulture, TreePerfilEducacionAccion.TreePerfilEducacionAccionQry.TreePerfilEducacionAccionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreePerfilEducacionAccionQry = listTreePerfilEducacionAccionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreePerfilEducacionAccionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreePerfilEducacionAccionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreePerfilEducacionAccionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreePerfilEducacionAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreePerfilEducacionAccionQryDP[])lista.ToArray(typeof(TreePerfilEducacionAccionQryDP));
        }
        public static Int64 CountForNivelTrabajoSistema(DbConnection con, DbTransaction tran, Byte aIdNiveltrabajo, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForNivelTrabajoSistema TreePerfilEducacionAccionQry
            DbCommand com = con.CreateCommand();
            String countTreePerfilEducacionAccionQryForNivelTrabajoSistema = String.Format(CultureInfo.CurrentCulture,TreePerfilEducacionAccion.TreePerfilEducacionAccionQry.TreePerfilEducacionAccionQryCount,"");
            countTreePerfilEducacionAccionQryForNivelTrabajoSistema += " WHERE \n";
            String delimitador = "";
            if (aIdNiveltrabajo != 0)
            {
                countTreePerfilEducacionAccionQryForNivelTrabajoSistema += String.Format("    {1} Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSistema != 0)
            {
                countTreePerfilEducacionAccionQryForNivelTrabajoSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTreePerfilEducacionAccionQryForNivelTrabajoSistema;
            #endregion
            #region Parametros countTreePerfilEducacionAccionQryForNivelTrabajoSistema
            if (aIdNiveltrabajo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            }
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForNivelTrabajoSistema TreePerfilEducacionAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreePerfilEducacionAccionQryDP[] ListForNivelTrabajoSistema(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveltrabajo, Byte aIdSistema, Byte aIdPerfil)
        {
            #region SQL List TreePerfilEducacionAccionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            //String listTreePerfilEducacionAccionQryForNivelTrabajoSistema = String.Format(CultureInfo.CurrentCulture, TreePerfilEducacionAccion.TreePerfilEducacionAccionQry.TreePerfilEducacionAccionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listTreePerfilEducacionAccionQryForNivelTrabajoSistema = listTreePerfilEducacionAccionQryForNivelTrabajoSistema.Substring(6);
            //listTreePerfilEducacionAccionQryForNivelTrabajoSistema += " WHERE \n";

            String listTreePerfilEducacionAccionQryForNivelTrabajoSistema = String.Format(CultureInfo.CurrentCulture, TreePerfilEducacionAccion.TreePerfilEducacionAccionQry.TreePerfilEducacionAccionQrySelect, "",
                String.Format("{0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdPerfil\n", Model.ConstantesGlobales.ParameterPrefix)
                );

            //String delimitador = "";
            //if (aIdNiveltrabajo != 0)
            //{
            //    listTreePerfilEducacionAccionQryForNivelTrabajoSistema += String.Format("    {1} Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            //if (aIdSistema != 0)
            //{
            //    listTreePerfilEducacionAccionQryForNivelTrabajoSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listTreePerfilEducacionAccionQryForNivelTrabajoSistema, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            String filtraList = listTreePerfilEducacionAccionQryForNivelTrabajoSistema;
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreePerfilEducacionAccionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdNiveltrabajo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            }
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdPerfil != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPerfil",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdPerfil);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreePerfilEducacionAccionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreePerfilEducacionAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreePerfilEducacionAccionQryDP[])lista.ToArray(typeof(TreePerfilEducacionAccionQryDP));
        }
    }
}
