using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class FltPerfilEducacionSostenimientoQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltPerfilEducacionSostenimientoQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltPerfilEducacionSostenimientoQry!", ex);
            }
            return resultado;
        }

        public static FltPerfilEducacionSostenimientoQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            FltPerfilEducacionSostenimientoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltPerfilEducacionSostenimientoQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltPerfilEducacionSostenimientoQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForPerfilNivelSistema(DbConnection con, Int16 aIdPerfilEducacion, Byte aIdNiveltrabajo, Byte aIdSistema) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltPerfilEducacionSostenimientoQryMD.CountForPerfilNivelSistema(con, tran, aIdPerfilEducacion, aIdNiveltrabajo, aIdSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltPerfilEducacionSostenimientoQry!", ex);
            }
            return resultado;
        }

        public static FltPerfilEducacionSostenimientoQryDP[] LoadListForPerfilNivelSistema(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdPerfilEducacion, Byte aIdNiveltrabajo, Byte aIdSistema) 
        {
            FltPerfilEducacionSostenimientoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltPerfilEducacionSostenimientoQryMD.ListForPerfilNivelSistema(con, tran, startRowIndex, maximumRows, aIdPerfilEducacion, aIdNiveltrabajo, aIdSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltPerfilEducacionSostenimientoQry!", ex);
            }
            return resultado;

        }
    }
}
