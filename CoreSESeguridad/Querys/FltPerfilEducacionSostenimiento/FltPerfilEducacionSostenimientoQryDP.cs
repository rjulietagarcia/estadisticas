using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "FltPerfilEducacionSostenimientoQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 27 de julio de 2009.</Para>
    /// <Para>Hora: 04:08:18 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "FltPerfilEducacionSostenimientoQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitfederal</term><description>Descripcion Bitfederal</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitestatal</term><description>Descripcion Bitestatal</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitparticular</term><description>Descripcion Bitparticular</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Selected</term><description>Descripcion Selected</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "FltPerfilEducacionSostenimientoQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// FltPerfilEducacionSostenimientoQryDTO fltperfileducacionsostenimientoqry = new FltPerfilEducacionSostenimientoQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("FltPerfilEducacionSostenimientoQry")]
    public class FltPerfilEducacionSostenimientoQryDP
    {
        #region Definicion de campos privados.
        private Byte sostenimientoId;
        private String nombre;
        private Boolean bitfederal;
        private Boolean bitestatal;
        private Boolean bitparticular;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Byte sistemaId;
        private Boolean selected;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// Bitfederal
        /// </summary> 
        [XmlElement("Bitfederal")]
        public Boolean Bitfederal
        {
            get {
                    return bitfederal; 
            }
            set {
                    bitfederal = value; 
            }
        }

        /// <summary>
        /// Bitestatal
        /// </summary> 
        [XmlElement("Bitestatal")]
        public Boolean Bitestatal
        {
            get {
                    return bitestatal; 
            }
            set {
                    bitestatal = value; 
            }
        }

        /// <summary>
        /// Bitparticular
        /// </summary> 
        [XmlElement("Bitparticular")]
        public Boolean Bitparticular
        {
            get {
                    return bitparticular; 
            }
            set {
                    bitparticular = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// Selected
        /// </summary> 
        [XmlElement("Selected")]
        public Boolean Selected
        {
            get {
                    return selected; 
            }
            set {
                    selected = value; 
            }
        }

        #endregion.
    }
}
