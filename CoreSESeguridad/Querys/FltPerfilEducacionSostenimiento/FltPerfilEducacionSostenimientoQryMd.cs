using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class FltPerfilEducacionSostenimientoQryMD
    {
        private FltPerfilEducacionSostenimientoQryDP fltPerfilEducacionSostenimientoQry = null;

        public FltPerfilEducacionSostenimientoQryMD(FltPerfilEducacionSostenimientoQryDP fltPerfilEducacionSostenimientoQry)
        {
            this.fltPerfilEducacionSostenimientoQry = fltPerfilEducacionSostenimientoQry;
        }

        protected static FltPerfilEducacionSostenimientoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            FltPerfilEducacionSostenimientoQryDP fltPerfilEducacionSostenimientoQry = new FltPerfilEducacionSostenimientoQryDP();
            fltPerfilEducacionSostenimientoQry.SostenimientoId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            fltPerfilEducacionSostenimientoQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            fltPerfilEducacionSostenimientoQry.Bitfederal = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            fltPerfilEducacionSostenimientoQry.Bitestatal = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            fltPerfilEducacionSostenimientoQry.Bitparticular = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            fltPerfilEducacionSostenimientoQry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            fltPerfilEducacionSostenimientoQry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            fltPerfilEducacionSostenimientoQry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            fltPerfilEducacionSostenimientoQry.PerfilEducacionId = dr.IsDBNull(8) ? (short)0 : dr.GetInt16(8);
            fltPerfilEducacionSostenimientoQry.NiveltrabajoId = dr.IsDBNull(9) ? (Byte)0 : dr.GetByte(9);
            fltPerfilEducacionSostenimientoQry.SistemaId = dr.IsDBNull(10) ? (Byte)0 : dr.GetByte(10);
            fltPerfilEducacionSostenimientoQry.Selected = dr.IsDBNull(11) ? false : dr.GetBoolean(11);
            return fltPerfilEducacionSostenimientoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count FltPerfilEducacionSostenimientoQry
            DbCommand com = con.CreateCommand();
            String countFltPerfilEducacionSostenimientoQry = String.Format(CultureInfo.CurrentCulture,FltPerfilEducacionSostenimiento.FltPerfilEducacionSostenimientoQry.FltPerfilEducacionSostenimientoQryCount,"");
            com.CommandText = countFltPerfilEducacionSostenimientoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count FltPerfilEducacionSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltPerfilEducacionSostenimientoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List FltPerfilEducacionSostenimientoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltPerfilEducacionSostenimientoQry = String.Format(CultureInfo.CurrentCulture, FltPerfilEducacionSostenimiento.FltPerfilEducacionSostenimientoQry.FltPerfilEducacionSostenimientoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltPerfilEducacionSostenimientoQry = listFltPerfilEducacionSostenimientoQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltPerfilEducacionSostenimientoQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltPerfilEducacionSostenimientoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltPerfilEducacionSostenimientoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltPerfilEducacionSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltPerfilEducacionSostenimientoQryDP[])lista.ToArray(typeof(FltPerfilEducacionSostenimientoQryDP));
        }
        public static Int64 CountForPerfilNivelSistema(DbConnection con, DbTransaction tran, Int16 aIdPerfilEducacion, Byte aIdNiveltrabajo, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForPerfilNivelSistema FltPerfilEducacionSostenimientoQry
            DbCommand com = con.CreateCommand();
            String countFltPerfilEducacionSostenimientoQryForPerfilNivelSistema = String.Format(CultureInfo.CurrentCulture,FltPerfilEducacionSostenimiento.FltPerfilEducacionSostenimientoQry.FltPerfilEducacionSostenimientoQryCount,"");
            countFltPerfilEducacionSostenimientoQryForPerfilNivelSistema += " WHERE \n";
            String delimitador = "";
            countFltPerfilEducacionSostenimientoQryForPerfilNivelSistema += String.Format("    {1} Id_Perfil_Educacion = {0}IdPerfilEducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countFltPerfilEducacionSostenimientoQryForPerfilNivelSistema += String.Format("    {1} Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countFltPerfilEducacionSostenimientoQryForPerfilNivelSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countFltPerfilEducacionSostenimientoQryForPerfilNivelSistema;
            #endregion
            #region Parametros countFltPerfilEducacionSostenimientoQryForPerfilNivelSistema
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPerfilEducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPerfilEducacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForPerfilNivelSistema FltPerfilEducacionSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltPerfilEducacionSostenimientoQryDP[] ListForPerfilNivelSistema(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdPerfilEducacion, Byte aIdNiveltrabajo, Byte aIdSistema)
        {
            #region SQL List FltPerfilEducacionSostenimientoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();

            String condicion = " AND \n";
            String delimitador = "";
            condicion += String.Format("    {1} Id_Perfil_Educacion = {0}IdPerfilEducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            condicion += String.Format("    {1} Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            condicion += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String listFltPerfilEducacionSostenimientoQryForPerfilNivelSistema = String.Format(CultureInfo.CurrentCulture, FltPerfilEducacionSostenimiento.FltPerfilEducacionSostenimientoQry.FltPerfilEducacionSostenimientoQrySelect, ConstantesGlobales.ParameterPrefix, condicion);
//            listFltPerfilEducacionSostenimientoQryForPerfilNivelSistema = listFltPerfilEducacionSostenimientoQryForPerfilNivelSistema.Substring(6);

            String filtraList = listFltPerfilEducacionSostenimientoQryForPerfilNivelSistema;
                //String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                //ConstantesGlobales.ParameterPrefix,
                //listFltPerfilEducacionSostenimientoQryForPerfilNivelSistema, "", 
                //(startRowIndex + maximumRows).ToString(),
                //startRowIndex,
                //maximumRows,
                //"" // DISTINCT si es que aplica.
                //);
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltPerfilEducacionSostenimientoQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPerfilEducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPerfilEducacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltPerfilEducacionSostenimientoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltPerfilEducacionSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltPerfilEducacionSostenimientoQryDP[])lista.ToArray(typeof(FltPerfilEducacionSostenimientoQryDP));
        }
    }
}
