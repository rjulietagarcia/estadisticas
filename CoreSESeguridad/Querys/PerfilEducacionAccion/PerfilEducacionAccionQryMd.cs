using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class PerfilEducacionAccionQryMD
    {
        private PerfilEducacionAccionQryDP perfilEducacionAccionQry = null;

        public PerfilEducacionAccionQryMD(PerfilEducacionAccionQryDP perfilEducacionAccionQry)
        {
            this.perfilEducacionAccionQry = perfilEducacionAccionQry;
        }

        protected static PerfilEducacionAccionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilEducacionAccionQryDP perfilEducacionAccionQry = new PerfilEducacionAccionQryDP();
            perfilEducacionAccionQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            perfilEducacionAccionQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            perfilEducacionAccionQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            perfilEducacionAccionQry.AccionId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            perfilEducacionAccionQry.PerfilEducacionId = dr.IsDBNull(4) ? (short)0 : dr.GetInt16(4);
            perfilEducacionAccionQry.BitAutorizacion = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            perfilEducacionAccionQry.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            perfilEducacionAccionQry.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            perfilEducacionAccionQry.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            perfilEducacionAccionQry.NiveltrabajoId = dr.IsDBNull(9) ? (Byte)0 : dr.GetByte(9);
            return perfilEducacionAccionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilEducacionAccionQry
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionAccionQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionQry.PerfilEducacionAccionQryCount,"");
            com.CommandText = countPerfilEducacionAccionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacionAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionAccionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilEducacionAccionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionAccionQry = String.Format(CultureInfo.CurrentCulture, PerfilEducacionAccion.PerfilEducacionAccionQry.PerfilEducacionAccionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listPerfilEducacionAccionQry = listPerfilEducacionAccionQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPerfilEducacionAccionQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPerfilEducacionAccionQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionAccionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionAccionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PerfilEducacionAccionQryDP[])lista.ToArray(typeof(PerfilEducacionAccionQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Byte aIdNiveltrabajo, Int16 aIdPerfilEducacion)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave PerfilEducacionAccionQry
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionAccionQryForLlave = String.Format(CultureInfo.CurrentCulture,PerfilEducacionAccion.PerfilEducacionAccionQry.PerfilEducacionAccionQryCount,"");
            //countPerfilEducacionAccionQryForLlave += " WHERE \n";
            String delimitador = " AND ";
            if (aIdNiveltrabajo != 0)
            {
                countPerfilEducacionAccionQryForLlave += String.Format("   {1} Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdPerfilEducacion != -1)
            {
                countPerfilEducacionAccionQryForLlave += String.Format("    {1} Id_Perfil_Educacion = {0}IdPerfilEducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countPerfilEducacionAccionQryForLlave;
            #endregion
            #region Parametros countPerfilEducacionAccionQryForLlave
            if (aIdNiveltrabajo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            }
            if (aIdPerfilEducacion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPerfilEducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPerfilEducacion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave PerfilEducacionAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionAccionQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveltrabajo, Int16 aIdPerfilEducacion)
        {
            #region SQL List PerfilEducacionAccionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionAccionQryForLlave = String.Format(CultureInfo.CurrentCulture, PerfilEducacionAccion.PerfilEducacionAccionQry.PerfilEducacionAccionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listPerfilEducacionAccionQryForLlave = listPerfilEducacionAccionQryForLlave.Substring(6);
            //listPerfilEducacionAccionQryForLlave += " WHERE \n";
            String delimitador = " AND ";
            if (aIdNiveltrabajo != 0)
            {
                listPerfilEducacionAccionQryForLlave += String.Format("  {1} Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdPerfilEducacion != -1)
            {
                listPerfilEducacionAccionQryForLlave += String.Format("    {1} Id_Perfil_Educacion = {0}IdPerfilEducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPerfilEducacionAccionQryForLlave, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPerfilEducacionAccionQryForLlave;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionAccionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdNiveltrabajo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            }
            if (aIdPerfilEducacion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPerfilEducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPerfilEducacion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionAccionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PerfilEducacionAccionQryDP[])lista.ToArray(typeof(PerfilEducacionAccionQryDP));
        }
    }
}
