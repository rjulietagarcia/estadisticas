using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CicloPorTipoEducacionQryMD
    {
        private CicloPorTipoEducacionQryDP cicloPorTipoEducacionQry = null;

        public CicloPorTipoEducacionQryMD(CicloPorTipoEducacionQryDP cicloPorTipoEducacionQry)
        {
            this.cicloPorTipoEducacionQry = cicloPorTipoEducacionQry;
        }

        protected static CicloPorTipoEducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CicloPorTipoEducacionQryDP cicloPorTipoEducacionQry = new CicloPorTipoEducacionQryDP();
            cicloPorTipoEducacionQry.CicloescolarId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            cicloPorTipoEducacionQry.TipocicloId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            cicloPorTipoEducacionQry.FechaInicio = dr.IsDBNull(2) ? "" : dr.GetDateTime(2).ToShortDateString();;
            cicloPorTipoEducacionQry.FechaFin = dr.IsDBNull(3) ? "" : dr.GetDateTime(3).ToShortDateString();;
            cicloPorTipoEducacionQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            cicloPorTipoEducacionQry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            cicloPorTipoEducacionQry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            cicloPorTipoEducacionQry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            cicloPorTipoEducacionQry.CicloescolaranteriorId = dr.IsDBNull(8) ? (short)0 : dr.GetInt16(8);
            cicloPorTipoEducacionQry.CicloescolarsiguienteId = dr.IsDBNull(9) ? (short)0 : dr.GetInt16(9);
            cicloPorTipoEducacionQry.BitCicloactual = dr.IsDBNull(10) ? false : dr.GetBoolean(10);
            return cicloPorTipoEducacionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CicloPorTipoEducacionQry
            DbCommand com = con.CreateCommand();
            String countCicloPorTipoEducacionQry = String.Format(CultureInfo.CurrentCulture,CicloPorTipoEducacion.CicloPorTipoEducacionQry.CicloPorTipoEducacionQryCount,"");
            com.CommandText = countCicloPorTipoEducacionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CicloPorTipoEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CicloPorTipoEducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CicloPorTipoEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCicloPorTipoEducacionQry = String.Format(CultureInfo.CurrentCulture, CicloPorTipoEducacion.CicloPorTipoEducacionQry.CicloPorTipoEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCicloPorTipoEducacionQry = listCicloPorTipoEducacionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCicloPorTipoEducacionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CicloPorTipoEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CicloPorTipoEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CicloPorTipoEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CicloPorTipoEducacionQryDP[])lista.ToArray(typeof(CicloPorTipoEducacionQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre, Int16 aIdTipociclo)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion CicloPorTipoEducacionQry
            DbCommand com = con.CreateCommand();
            String countCicloPorTipoEducacionQryForDescripcion = String.Format(CultureInfo.CurrentCulture,CicloPorTipoEducacion.CicloPorTipoEducacionQry.CicloPorTipoEducacionQryCount,"");
            countCicloPorTipoEducacionQryForDescripcion += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countCicloPorTipoEducacionQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countCicloPorTipoEducacionQryForDescripcion += String.Format("    {1} Id_TipoCiclo = {0}IdTipociclo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countCicloPorTipoEducacionQryForDescripcion;
            #endregion
            #region Parametros countCicloPorTipoEducacionQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTipociclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTipociclo);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion CicloPorTipoEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CicloPorTipoEducacionQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdTipociclo)
        {
            #region SQL List CicloPorTipoEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCicloPorTipoEducacionQryForDescripcion = String.Format(CultureInfo.CurrentCulture, CicloPorTipoEducacion.CicloPorTipoEducacionQry.CicloPorTipoEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCicloPorTipoEducacionQryForDescripcion = listCicloPorTipoEducacionQryForDescripcion.Substring(6);
            listCicloPorTipoEducacionQryForDescripcion += " AND ";
            String delimitador = "";
            if (aNombre != "")
            {
                listCicloPorTipoEducacionQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listCicloPorTipoEducacionQryForDescripcion += String.Format("    {1} Id_TipoCiclo = {0}IdTipociclo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCicloPorTipoEducacionQryForDescripcion, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCicloPorTipoEducacionQryForDescripcion;
            DbDataReader dr;
            #endregion
            #region Parametros Load CicloPorTipoEducacionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTipociclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTipociclo);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CicloPorTipoEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CicloPorTipoEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CicloPorTipoEducacionQryDP[])lista.ToArray(typeof(CicloPorTipoEducacionQryDP));
        }
    }
}
