using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CicloPorTipoEducacionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CicloPorTipoEducacionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CicloPorTipoEducacionQry!", ex);
            }
            return resultado;
        }

        public static CicloPorTipoEducacionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CicloPorTipoEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CicloPorTipoEducacionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CicloPorTipoEducacionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, String aNombre, Int16 aIdTipociclo) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CicloPorTipoEducacionQryMD.CountForDescripcion(con, tran, aNombre, aIdTipociclo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CicloPorTipoEducacionQry!", ex);
            }
            return resultado;
        }

        public static CicloPorTipoEducacionQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdTipociclo) 
        {
            CicloPorTipoEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CicloPorTipoEducacionQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aNombre, aIdTipociclo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CicloPorTipoEducacionQry!", ex);
            }
            return resultado;

        }
    }
}
