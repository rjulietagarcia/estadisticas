using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class RegionQryMD
    {
        private RegionQryDP regionQry = null;

        public RegionQryMD(RegionQryDP regionQry)
        {
            this.regionQry = regionQry;
        }

        protected static RegionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            RegionQryDP regionQry = new RegionQryDP();
            regionQry.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            regionQry.EntidadId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            regionQry.RegionId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            regionQry.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            regionQry.Clave = dr.IsDBNull(4) ? "" : dr.GetString(4);
            regionQry.LetraFolio = dr.IsDBNull(5) ? "" : dr.GetString(5);
            regionQry.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            regionQry.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            regionQry.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            return regionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count RegionQry
            DbCommand com = con.CreateCommand();
            String countRegionQry = String.Format(CultureInfo.CurrentCulture,Region.RegionQry.RegionQryCount,"");
            com.CommandText = countRegionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count RegionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountRegionQry = String.Format(CultureInfo.CurrentCulture,Region.RegionQry.RegionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountRegionQry);
                #endregion
            }
            return resultado;
        }
        public static RegionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List RegionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listRegionQry = String.Format(CultureInfo.CurrentCulture, Region.RegionQry.RegionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listRegionQry = listRegionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listRegionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load RegionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista RegionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista RegionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaRegionQry = String.Format(CultureInfo.CurrentCulture,Region.RegionQry.RegionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaRegionQry);
                #endregion
            }
            return (RegionQryDP[])lista.ToArray(typeof(RegionQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion RegionQry
            DbCommand com = con.CreateCommand();
            String countRegionQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Region.RegionQry.RegionQryCount,"");
            countRegionQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countRegionQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countRegionQryForDescripcion;
            #endregion
            #region Parametros countRegionQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 60, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion RegionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountRegionQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Region.RegionQry.RegionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountRegionQryForDescripcion);
                #endregion
            }
            return resultado;
        }
        public static RegionQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre)
        {
            #region SQL List RegionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listRegionQryForDescripcion = String.Format(CultureInfo.CurrentCulture, Region.RegionQry.RegionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listRegionQryForDescripcion = listRegionQryForDescripcion.Substring(6);
            listRegionQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listRegionQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listRegionQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load RegionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 60, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista RegionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista RegionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaRegionQry = String.Format(CultureInfo.CurrentCulture,Region.RegionQry.RegionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaRegionQry);
                #endregion
            }
            return (RegionQryDP[])lista.ToArray(typeof(RegionQryDP));
        }
    }
}
