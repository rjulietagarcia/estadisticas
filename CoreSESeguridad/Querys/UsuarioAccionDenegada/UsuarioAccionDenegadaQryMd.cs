using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class UsuarioAccionDenegadaQryMD
    {
        private UsuarioAccionDenegadaQryDP usuarioAccionDenegadaQry = null;

        public UsuarioAccionDenegadaQryMD(UsuarioAccionDenegadaQryDP usuarioAccionDenegadaQry)
        {
            this.usuarioAccionDenegadaQry = usuarioAccionDenegadaQry;
        }

        protected static UsuarioAccionDenegadaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            UsuarioAccionDenegadaQryDP usuarioAccionDenegadaQry = new UsuarioAccionDenegadaQryDP();
            usuarioAccionDenegadaQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            usuarioAccionDenegadaQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            usuarioAccionDenegadaQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            usuarioAccionDenegadaQry.UsuarioId = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            usuarioAccionDenegadaQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            usuarioAccionDenegadaQry.CicloescolarId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            return usuarioAccionDenegadaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count UsuarioAccionDenegadaQry
            DbCommand com = con.CreateCommand();
            String countUsuarioAccionDenegadaQry = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaQry.UsuarioAccionDenegadaQryCount,"");
            com.CommandText = countUsuarioAccionDenegadaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count UsuarioAccionDenegadaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountUsuarioAccionDenegadaQry = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaQry.UsuarioAccionDenegadaQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountUsuarioAccionDenegadaQry);
                #endregion
            }
            return resultado;
        }
        public static UsuarioAccionDenegadaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List UsuarioAccionDenegadaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuarioAccionDenegadaQry = String.Format(CultureInfo.CurrentCulture, UsuarioAccionDenegada.UsuarioAccionDenegadaQry.UsuarioAccionDenegadaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listUsuarioAccionDenegadaQry = listUsuarioAccionDenegadaQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuarioAccionDenegadaQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load UsuarioAccionDenegadaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista UsuarioAccionDenegadaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista UsuarioAccionDenegadaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaUsuarioAccionDenegadaQry = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaQry.UsuarioAccionDenegadaQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaUsuarioAccionDenegadaQry);
                #endregion
            }
            return (UsuarioAccionDenegadaQryDP[])lista.ToArray(typeof(UsuarioAccionDenegadaQryDP));
        }
        public static Int64 CountForOpcionesDenegadas(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar)
        {
            Int64 resultado = 0;
            #region SQL CountForOpcionesDenegadas UsuarioAccionDenegadaQry
            DbCommand com = con.CreateCommand();
            String countUsuarioAccionDenegadaQryForOpcionesDenegadas = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaQry.UsuarioAccionDenegadaQryCount,"");
            countUsuarioAccionDenegadaQryForOpcionesDenegadas += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdUsuario != -1)
            {
                countUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_Usuario = {0}IdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Bit_activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdCicloescolar != -1)
            {
                countUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countUsuarioAccionDenegadaQryForOpcionesDenegadas;
            #endregion
            #region Parametros countUsuarioAccionDenegadaQryForOpcionesDenegadas
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuario);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForOpcionesDenegadas UsuarioAccionDenegadaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountUsuarioAccionDenegadaQryForOpcionesDenegadas = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaQry.UsuarioAccionDenegadaQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountUsuarioAccionDenegadaQryForOpcionesDenegadas);
                #endregion
            }
            return resultado;
        }
        public static UsuarioAccionDenegadaQryDP[] ListForOpcionesDenegadas(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar)
        {
            #region SQL List UsuarioAccionDenegadaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuarioAccionDenegadaQryForOpcionesDenegadas = String.Format(CultureInfo.CurrentCulture, UsuarioAccionDenegada.UsuarioAccionDenegadaQry.UsuarioAccionDenegadaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listUsuarioAccionDenegadaQryForOpcionesDenegadas = listUsuarioAccionDenegadaQryForOpcionesDenegadas.Substring(6);
            listUsuarioAccionDenegadaQryForOpcionesDenegadas += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdUsuario != -1)
            {
                listUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_Usuario = {0}IdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Bit_activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdCicloescolar != -1)
            {
                listUsuarioAccionDenegadaQryForOpcionesDenegadas += String.Format("    {1} Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuarioAccionDenegadaQryForOpcionesDenegadas, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load UsuarioAccionDenegadaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuario);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista UsuarioAccionDenegadaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista UsuarioAccionDenegadaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaUsuarioAccionDenegadaQry = String.Format(CultureInfo.CurrentCulture,UsuarioAccionDenegada.UsuarioAccionDenegadaQry.UsuarioAccionDenegadaQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaUsuarioAccionDenegadaQry);
                #endregion
            }
            return (UsuarioAccionDenegadaQryDP[])lista.ToArray(typeof(UsuarioAccionDenegadaQryDP));
        }
    }
}
