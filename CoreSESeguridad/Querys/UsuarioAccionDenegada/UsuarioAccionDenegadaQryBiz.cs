using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class UsuarioAccionDenegadaQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioAccionDenegadaQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un UsuarioAccionDenegadaQry!", ex);
            }
            return resultado;
        }

        public static UsuarioAccionDenegadaQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            UsuarioAccionDenegadaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioAccionDenegadaQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de UsuarioAccionDenegadaQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForOpcionesDenegadas(DbConnection con, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioAccionDenegadaQryMD.CountForOpcionesDenegadas(con, tran, aIdSistema, aIdModulo, aIdOpcion, aIdUsuario, aBitActivo, aIdCicloescolar);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un UsuarioAccionDenegadaQry!", ex);
            }
            return resultado;
        }

        public static UsuarioAccionDenegadaQryDP[] LoadListForOpcionesDenegadas(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar) 
        {
            UsuarioAccionDenegadaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioAccionDenegadaQryMD.ListForOpcionesDenegadas(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion, aIdUsuario, aBitActivo, aIdCicloescolar);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de UsuarioAccionDenegadaQry!", ex);
            }
            return resultado;

        }
    }
}
