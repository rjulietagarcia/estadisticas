using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class ValidateUserQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ValidateUserQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ValidateUserQry!", ex);
            }
            return resultado;
        }

        public static ValidateUserQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            ValidateUserQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ValidateUserQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ValidateUserQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuarioPassword(DbConnection con, String aLogin, String aPassword) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ValidateUserQryMD.CountForUsuarioPassword(con, tran, aLogin, aPassword);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ValidateUserQry!", ex);
            }
            return resultado;
        }

        public static ValidateUserQryDP[] LoadListForUsuarioPassword(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aLogin, String aPassword) 
        {
            ValidateUserQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ValidateUserQryMD.ListForUsuarioPassword(con, tran, startRowIndex, maximumRows, aLogin, aPassword);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ValidateUserQry!", ex);
            }
            return resultado;

        }
    }
}
