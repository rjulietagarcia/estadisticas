using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "ValidateUserQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 10:53:05 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "ValidateUserQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>Login</term><description>Descripcion Login</description>
    ///    </item>
    ///    <item>
    ///        <term>Password</term><description>Descripcion Password</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "ValidateUserQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// ValidateUserQryDTO validateuserqry = new ValidateUserQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("ValidateUserQry")]
    public class ValidateUserQryDP
    {
        #region Definicion de campos privados.
        private String login;
        private String password;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// Login
        /// </summary> 
        [XmlElement("Login")]
        public String Login
        {
            get {
                    return login; 
            }
            set {
                    login = value; 
            }
        }

        /// <summary>
        /// Password
        /// </summary> 
        [XmlElement("Password")]
        public String Password
        {
            get {
                    return password; 
            }
            set {
                    password = value; 
            }
        }

        #endregion.
    }
}
