using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class ValidateUserQryMD
    {
        private ValidateUserQryDP validateUserQry = null;

        public ValidateUserQryMD(ValidateUserQryDP validateUserQry)
        {
            this.validateUserQry = validateUserQry;
        }

        protected static ValidateUserQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ValidateUserQryDP validateUserQry = new ValidateUserQryDP();
            validateUserQry.Login = dr.IsDBNull(0) ? "" : dr.GetString(0);
            validateUserQry.Password = dr.IsDBNull(1) ? "" : dr.GetString(1);
            return validateUserQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count ValidateUserQry
            DbCommand com = con.CreateCommand();
            String countValidateUserQry = String.Format(CultureInfo.CurrentCulture,ValidateUser.ValidateUserQry.ValidateUserQryCount,"");
            com.CommandText = countValidateUserQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count ValidateUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountValidateUserQry = String.Format(CultureInfo.CurrentCulture,ValidateUser.ValidateUserQry.ValidateUserQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountValidateUserQry);
                #endregion
            }
            return resultado;
        }
        public static ValidateUserQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List ValidateUserQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listValidateUserQry = String.Format(CultureInfo.CurrentCulture, ValidateUser.ValidateUserQry.ValidateUserQrySelect, "", ConstantesGlobales.IncluyeRows);
            listValidateUserQry = listValidateUserQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listValidateUserQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load ValidateUserQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ValidateUserQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ValidateUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaValidateUserQry = String.Format(CultureInfo.CurrentCulture,ValidateUser.ValidateUserQry.ValidateUserQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaValidateUserQry);
                #endregion
            }
            return (ValidateUserQryDP[])lista.ToArray(typeof(ValidateUserQryDP));
        }
        public static Int64 CountForUsuarioPassword(DbConnection con, DbTransaction tran, String aLogin, String aPassword)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuarioPassword ValidateUserQry
            DbCommand com = con.CreateCommand();
            String countValidateUserQryForUsuarioPassword = String.Format(CultureInfo.CurrentCulture,ValidateUser.ValidateUserQry.ValidateUserQryCount,"");
            countValidateUserQryForUsuarioPassword += " WHERE \n";
            String delimitador = "";
            countValidateUserQryForUsuarioPassword += String.Format("    {1} Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countValidateUserQryForUsuarioPassword += String.Format("    {1} Password = {0}Password\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countValidateUserQryForUsuarioPassword;
            #endregion
            #region Parametros countValidateUserQryForUsuarioPassword
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Password",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aPassword);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuarioPassword ValidateUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountValidateUserQryForUsuarioPassword = String.Format(CultureInfo.CurrentCulture,ValidateUser.ValidateUserQry.ValidateUserQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountValidateUserQryForUsuarioPassword);
                #endregion
            }
            return resultado;
        }
        public static ValidateUserQryDP[] ListForUsuarioPassword(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aLogin, String aPassword)
        {
            #region SQL List ValidateUserQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listValidateUserQryForUsuarioPassword = String.Format(CultureInfo.CurrentCulture, ValidateUser.ValidateUserQry.ValidateUserQrySelect, "", ConstantesGlobales.IncluyeRows);
            listValidateUserQryForUsuarioPassword = listValidateUserQryForUsuarioPassword.Substring(6);
            listValidateUserQryForUsuarioPassword += " WHERE \n";
            String delimitador = "";
            listValidateUserQryForUsuarioPassword += String.Format("    {1} Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listValidateUserQryForUsuarioPassword += String.Format("    {1} Password = {0}Password\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listValidateUserQryForUsuarioPassword, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load ValidateUserQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Password",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aPassword);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ValidateUserQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ValidateUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaValidateUserQry = String.Format(CultureInfo.CurrentCulture,ValidateUser.ValidateUserQry.ValidateUserQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaValidateUserQry);
                #endregion
            }
            return (ValidateUserQryDP[])lista.ToArray(typeof(ValidateUserQryDP));
        }
    }
}
