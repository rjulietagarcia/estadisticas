using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class Sostenimmiento2QryMD
    {
        private SostenimientoQryDP sostenimmiento2Qry = null;

        public Sostenimmiento2QryMD(SostenimientoQryDP sostenimmiento2Qry)
        {
            this.sostenimmiento2Qry = sostenimmiento2Qry;
        }

        protected static SostenimientoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SostenimientoQryDP sostenimmiento2Qry = new SostenimientoQryDP();
            sostenimmiento2Qry.SostenimientoId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            sostenimmiento2Qry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            sostenimmiento2Qry.Bitfederal = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            sostenimmiento2Qry.Bitestatal = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            sostenimmiento2Qry.Bitparticular = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            sostenimmiento2Qry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            sostenimmiento2Qry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            sostenimmiento2Qry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            //sostenimmiento2Qry.UsuIdUsuario = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            return sostenimmiento2Qry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count Sostenimmiento2Qry
            DbCommand com = con.CreateCommand();
            String countSostenimmiento2Qry = String.Format(CultureInfo.CurrentCulture,Sostenimmiento2.Sostenimmiento2Qry.Sostenimmiento2QryCount,"");
            com.CommandText = countSostenimmiento2Qry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count Sostenimmiento2Qry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SostenimientoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List Sostenimmiento2Qry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSostenimmiento2Qry = String.Format(CultureInfo.CurrentCulture, Sostenimmiento2.Sostenimmiento2Qry.Sostenimmiento2QrySelect, "", ConstantesGlobales.IncluyeRows);
            listSostenimmiento2Qry = listSostenimmiento2Qry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSostenimmiento2Qry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Sostenimmiento2Qry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Sostenimmiento2Qry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Sostenimmiento2Qry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SostenimientoQryDP[])lista.ToArray(typeof(SostenimientoQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario Sostenimmiento2Qry
            DbCommand com = con.CreateCommand();
            String countSostenimmiento2QryForUsuario = String.Format(CultureInfo.CurrentCulture,Sostenimmiento2.Sostenimmiento2Qry.Sostenimmiento2QryCount,"");
            countSostenimmiento2QryForUsuario += " AND \n";
            String delimitador = "";
            countSostenimmiento2QryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countSostenimmiento2QryForUsuario;
            #endregion
            #region Parametros countSostenimmiento2QryForUsuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario Sostenimmiento2Qry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SostenimientoQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario)
        {
            #region SQL List Sostenimmiento2Qry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSostenimmiento2QryForUsuario = String.Format(CultureInfo.CurrentCulture, Sostenimmiento2.Sostenimmiento2Qry.Sostenimmiento2QrySelect, "", ConstantesGlobales.IncluyeRows);
            //listSostenimmiento2QryForUsuario = listSostenimmiento2QryForUsuario.Substring(6);
            listSostenimmiento2QryForUsuario += " AND \n";
            String delimitador = "";
            listSostenimmiento2QryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, //ConstantesGlobales.TraeUnosRegistros,
               // ConstantesGlobales.ParameterPrefix,
                listSostenimmiento2QryForUsuario //"", 
                //(startRowIndex + maximumRows).ToString(),
               // startRowIndex,
                //maximumRows,
                //"" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load Sostenimmiento2Qry
           // Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista Sostenimmiento2Qry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista Sostenimmiento2Qry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SostenimientoQryDP[])lista.ToArray(typeof(SostenimientoQryDP));
        }
    }
}
