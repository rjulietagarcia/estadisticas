using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class Sostenimmiento2QryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = Sostenimmiento2QryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Sostenimmiento2Qry!", ex);
            }
            return resultado;
        }

        public static SostenimientoQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SostenimientoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = Sostenimmiento2QryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Sostenimmiento2Qry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuario(DbConnection con, Int32 aUsuIdUsuario) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = Sostenimmiento2QryMD.CountForUsuario(con, tran, aUsuIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un Sostenimmiento2Qry!", ex);
            }
            return resultado;
        }

        public static SostenimientoQryDP[] LoadListForUsuario(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario) 
        {
            SostenimientoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = Sostenimmiento2QryMD.ListForUsuario(con, tran, startRowIndex, maximumRows, aUsuIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de Sostenimmiento2Qry!", ex);
            }
            return resultado;

        }
    }
}
