using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class UsuarioAccionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioAccionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un UsuarioAccionQry!", ex);
            }
            return resultado;
        }

        public static UsuarioAccionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            UsuarioAccionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioAccionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de UsuarioAccionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForOpcionesAsignadas(DbConnection con, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = UsuarioAccionQryMD.CountForOpcionesAsignadas(con, tran, aIdSistema, aIdModulo, aIdOpcion, aIdUsuario, aBitActivo, aIdCicloescolar);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un UsuarioAccionQry!", ex);
            }
            return resultado;
        }

        public static UsuarioAccionQryDP[] LoadListForOpcionesAsignadas(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar) 
        {
            UsuarioAccionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = UsuarioAccionQryMD.ListForOpcionesAsignadas(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion, aIdUsuario, aBitActivo, aIdCicloescolar);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de UsuarioAccionQry!", ex);
            }
            return resultado;

        }
    }
}
