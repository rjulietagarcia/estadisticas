using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class UsuarioAccionQryMD
    {
        private UsuarioAccionQryDP usuarioAccionQry = null;

        public UsuarioAccionQryMD(UsuarioAccionQryDP usuarioAccionQry)
        {
            this.usuarioAccionQry = usuarioAccionQry;
        }

        protected static UsuarioAccionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            UsuarioAccionQryDP usuarioAccionQry = new UsuarioAccionQryDP();
            usuarioAccionQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            usuarioAccionQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            usuarioAccionQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            usuarioAccionQry.UsuarioId = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            usuarioAccionQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            usuarioAccionQry.CicloescolarId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            return usuarioAccionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count UsuarioAccionQry
            DbCommand com = con.CreateCommand();
            String countUsuarioAccionQry = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionQry.UsuarioAccionQryCount,"");
            com.CommandText = countUsuarioAccionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count UsuarioAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountUsuarioAccionQry = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionQry.UsuarioAccionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountUsuarioAccionQry);
                #endregion
            }
            return resultado;
        }
        public static UsuarioAccionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List UsuarioAccionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuarioAccionQry = String.Format(CultureInfo.CurrentCulture, UsuarioAccion.UsuarioAccionQry.UsuarioAccionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listUsuarioAccionQry = listUsuarioAccionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuarioAccionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load UsuarioAccionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista UsuarioAccionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista UsuarioAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaUsuarioAccionQry = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionQry.UsuarioAccionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaUsuarioAccionQry);
                #endregion
            }
            return (UsuarioAccionQryDP[])lista.ToArray(typeof(UsuarioAccionQryDP));
        }
        public static Int64 CountForOpcionesAsignadas(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar)
        {
            Int64 resultado = 0;
            #region SQL CountForOpcionesAsignadas UsuarioAccionQry
            DbCommand com = con.CreateCommand();
            String countUsuarioAccionQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionQry.UsuarioAccionQryCount,"");
            countUsuarioAccionQryForOpcionesAsignadas += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdUsuario != -1)
            {
                countUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_Usuario = {0}IdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Bit_activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdCicloescolar != -1)
            {
                countUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countUsuarioAccionQryForOpcionesAsignadas;
            #endregion
            #region Parametros countUsuarioAccionQryForOpcionesAsignadas
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuario);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForOpcionesAsignadas UsuarioAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountUsuarioAccionQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionQry.UsuarioAccionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountUsuarioAccionQryForOpcionesAsignadas);
                #endregion
            }
            return resultado;
        }
        public static UsuarioAccionQryDP[] ListForOpcionesAsignadas(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Int32 aIdUsuario, Boolean aBitActivo, Int16 aIdCicloescolar)
        {
            #region SQL List UsuarioAccionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listUsuarioAccionQryForOpcionesAsignadas = String.Format(CultureInfo.CurrentCulture, UsuarioAccion.UsuarioAccionQry.UsuarioAccionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listUsuarioAccionQryForOpcionesAsignadas = listUsuarioAccionQryForOpcionesAsignadas.Substring(6);
            listUsuarioAccionQryForOpcionesAsignadas += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdUsuario != -1)
            {
                listUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_Usuario = {0}IdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Bit_activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdCicloescolar != -1)
            {
                listUsuarioAccionQryForOpcionesAsignadas += String.Format("    {1} Id_CicloEscolar = {0}IdCicloescolar\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listUsuarioAccionQryForOpcionesAsignadas, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load UsuarioAccionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuario);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            if (aIdCicloescolar != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCicloescolar",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCicloescolar);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista UsuarioAccionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista UsuarioAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaUsuarioAccionQry = String.Format(CultureInfo.CurrentCulture,UsuarioAccion.UsuarioAccionQry.UsuarioAccionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaUsuarioAccionQry);
                #endregion
            }
            return (UsuarioAccionQryDP[])lista.ToArray(typeof(UsuarioAccionQryDP));
        }
    }
}
