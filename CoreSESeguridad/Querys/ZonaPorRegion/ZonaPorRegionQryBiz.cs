using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class ZonaPorRegionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ZonaPorRegionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ZonaPorRegionQry!", ex);
            }
            return resultado;
        }

        public static ZonaPorRegionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            ZonaPorRegionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ZonaPorRegionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ZonaPorRegionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, String aNombre, Int16 aIdRegion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ZonaPorRegionQryMD.CountForDescripcion(con, tran, aNombre, aIdRegion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ZonaPorRegionQry!", ex);
            }
            return resultado;
        }

        public static ZonaPorRegionQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdRegion) 
        {
            ZonaPorRegionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ZonaPorRegionQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aNombre, aIdRegion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ZonaPorRegionQry!", ex);
            }
            return resultado;

        }
    }
}
