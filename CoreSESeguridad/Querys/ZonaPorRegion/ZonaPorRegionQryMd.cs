using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class ZonaPorRegionQryMD
    {
        private ZonaPorRegionQryDP zonaPorRegionQry = null;

        public ZonaPorRegionQryMD(ZonaPorRegionQryDP zonaPorRegionQry)
        {
            this.zonaPorRegionQry = zonaPorRegionQry;
        }

        protected static ZonaPorRegionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ZonaPorRegionQryDP zonaPorRegionQry = new ZonaPorRegionQryDP();
            zonaPorRegionQry.ZonaId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            zonaPorRegionQry.TipoeducacionId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            zonaPorRegionQry.NivelId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            zonaPorRegionQry.Numerozona = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            zonaPorRegionQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            zonaPorRegionQry.Clave = dr.IsDBNull(5) ? "" : dr.GetString(5);
            zonaPorRegionQry.SostenimientoId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            zonaPorRegionQry.BitActivo = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            zonaPorRegionQry.UsuarioId = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            zonaPorRegionQry.FechaActualizacion = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            zonaPorRegionQry.RegionId = dr.IsDBNull(10) ? (short)0 : dr.GetInt16(10);
            return zonaPorRegionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count ZonaPorRegionQry
            DbCommand com = con.CreateCommand();
            String countZonaPorRegionQry = String.Format(CultureInfo.CurrentCulture,ZonaPorRegion.ZonaPorRegionQry.ZonaPorRegionQryCount,"");
            com.CommandText = countZonaPorRegionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count ZonaPorRegionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static ZonaPorRegionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List ZonaPorRegionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listZonaPorRegionQry = String.Format(CultureInfo.CurrentCulture, ZonaPorRegion.ZonaPorRegionQry.ZonaPorRegionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listZonaPorRegionQry = listZonaPorRegionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listZonaPorRegionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load ZonaPorRegionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ZonaPorRegionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ZonaPorRegionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (ZonaPorRegionQryDP[])lista.ToArray(typeof(ZonaPorRegionQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre, Int16 aIdRegion)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion ZonaPorRegionQry
            DbCommand com = con.CreateCommand();
            String countZonaPorRegionQryForDescripcion = String.Format(CultureInfo.CurrentCulture,ZonaPorRegion.ZonaPorRegionQry.ZonaPorRegionQryCount,"");
            countZonaPorRegionQryForDescripcion += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countZonaPorRegionQryForDescripcion += String.Format("    {1} z.nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countZonaPorRegionQryForDescripcion += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countZonaPorRegionQryForDescripcion;
            #endregion
            #region Parametros countZonaPorRegionQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion ZonaPorRegionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static ZonaPorRegionQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdRegion)
        {
            #region SQL List ZonaPorRegionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listZonaPorRegionQryForDescripcion = String.Format(CultureInfo.CurrentCulture, ZonaPorRegion.ZonaPorRegionQry.ZonaPorRegionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listZonaPorRegionQryForDescripcion = listZonaPorRegionQryForDescripcion.Substring(6);
            listZonaPorRegionQryForDescripcion += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listZonaPorRegionQryForDescripcion += String.Format("    {1} z.nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listZonaPorRegionQryForDescripcion += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listZonaPorRegionQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList.Replace("top","distinct top");
            DbDataReader dr;
            #endregion
            #region Parametros Load ZonaPorRegionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ZonaPorRegionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ZonaPorRegionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (ZonaPorRegionQryDP[])lista.ToArray(typeof(ZonaPorRegionQryDP));
        }
    }
}
