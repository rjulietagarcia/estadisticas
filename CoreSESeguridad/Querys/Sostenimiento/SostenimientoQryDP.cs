using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "SostenimientoQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: viernes, 12 de junio de 2009.</Para>
    /// <Para>Hora: 12:14:47 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "SostenimientoQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitfederal</term><description>Descripcion Bitfederal</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitestatal</term><description>Descripcion Bitestatal</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitparticular</term><description>Descripcion Bitparticular</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "SostenimientoQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// SostenimientoQryDTO sostenimientoqry = new SostenimientoQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("SostenimientoQry")]
    public class SostenimientoQryDP
    {
        #region Definicion de campos privados.
        private Byte sostenimientoId;
        private String nombre;
        private Boolean bitfederal;
        private Boolean bitestatal;
        private Boolean bitparticular;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private Int16 regionId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// Bitfederal
        /// </summary> 
        [XmlElement("Bitfederal")]
        public Boolean Bitfederal
        {
            get {
                    return bitfederal; 
            }
            set {
                    bitfederal = value; 
            }
        }

        /// <summary>
        /// Bitestatal
        /// </summary> 
        [XmlElement("Bitestatal")]
        public Boolean Bitestatal
        {
            get {
                    return bitestatal; 
            }
            set {
                    bitestatal = value; 
            }
        }

        /// <summary>
        /// Bitparticular
        /// </summary> 
        [XmlElement("Bitparticular")]
        public Boolean Bitparticular
        {
            get {
                    return bitparticular; 
            }
            set {
                    bitparticular = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public Int16 RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        #endregion.
    }
}
