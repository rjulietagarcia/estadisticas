using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class SostenimientoQryMD
    {
        private SostenimientoQryDP sostenimientoQry = null;

        public SostenimientoQryMD(SostenimientoQryDP sostenimientoQry)
        {
            this.sostenimientoQry = sostenimientoQry;
        }

        protected static SostenimientoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SostenimientoQryDP sostenimientoQry = new SostenimientoQryDP();
            sostenimientoQry.SostenimientoId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            sostenimientoQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            sostenimientoQry.Bitfederal = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            sostenimientoQry.Bitestatal = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            sostenimientoQry.Bitparticular = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            sostenimientoQry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            sostenimientoQry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            sostenimientoQry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            sostenimientoQry.RegionId = dr.IsDBNull(8) ? (short)0 : dr.GetInt16(8);
            return sostenimientoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count SostenimientoQry
            DbCommand com = con.CreateCommand();
            String countSostenimientoQry = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoQry.SostenimientoQryCount,"");
            com.CommandText = countSostenimientoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count SostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SostenimientoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List SostenimientoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSostenimientoQry = String.Format(CultureInfo.CurrentCulture, Sostenimiento.SostenimientoQry.SostenimientoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listSostenimientoQry = listSostenimientoQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSostenimientoQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load SostenimientoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SostenimientoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SostenimientoQryDP[])lista.ToArray(typeof(SostenimientoQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre, Int16 aIdRegion)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion SostenimientoQry
            DbCommand com = con.CreateCommand();
            String countSostenimientoQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Sostenimiento.SostenimientoQry.SostenimientoQryCount,"");
            countSostenimientoQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countSostenimientoQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countSostenimientoQryForDescripcion += String.Format("    {1} id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countSostenimientoQryForDescripcion;
            #endregion
            #region Parametros countSostenimientoQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion SostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SostenimientoQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdRegion)
        {
            #region SQL List SostenimientoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSostenimientoQryForDescripcion = String.Format(CultureInfo.CurrentCulture, Sostenimiento.SostenimientoQry.SostenimientoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listSostenimientoQryForDescripcion = listSostenimientoQryForDescripcion.Substring(6);
            listSostenimientoQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listSostenimientoQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listSostenimientoQryForDescripcion += String.Format("    {1} id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSostenimientoQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load SostenimientoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SostenimientoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SostenimientoQryDP[])lista.ToArray(typeof(SostenimientoQryDP));
        }
    }
}
