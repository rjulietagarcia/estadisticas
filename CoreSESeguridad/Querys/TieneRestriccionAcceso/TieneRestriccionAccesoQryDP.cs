using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "TieneRestriccionAccesoQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: miércoles, 27 de mayo de 2009.</Para>
    /// <Para>Hora: 01:06:29 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "TieneRestriccionAccesoQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>NiveleducacionId</term><description>Descripcion NiveleducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Niveltrabajo</term><description>Descripcion Niveltrabajo</description>
    ///    </item>
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaInicio</term><description>Descripcion FechaInicio</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaFin</term><description>Descripcion FechaFin</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TieneRestriccionAccesoQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// TieneRestriccionAccesoQryDTO tienerestriccionaccesoqry = new TieneRestriccionAccesoQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("TieneRestriccionAccesoQry")]
    public class TieneRestriccionAccesoQryDP
    {
        #region Definicion de campos privados.
        private Int32 niveleducacionId;
        private Int32 niveltrabajo;
        private Int32 sostenimientoId;
        private String fechaInicio;
        private String fechaFin;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// NiveleducacionId
        /// </summary> 
        [XmlElement("NiveleducacionId")]
        public Int32 NiveleducacionId
        {
            get {
                    return niveleducacionId; 
            }
            set {
                    niveleducacionId = value; 
            }
        }

        /// <summary>
        /// Niveltrabajo
        /// </summary> 
        [XmlElement("Niveltrabajo")]
        public Int32 Niveltrabajo
        {
            get {
                    return niveltrabajo; 
            }
            set {
                    niveltrabajo = value; 
            }
        }

        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Int32 SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// FechaInicio
        /// </summary> 
        [XmlElement("FechaInicio")]
        public String FechaInicio
        {
            get {
                    return fechaInicio; 
            }
            set {
                    fechaInicio = value; 
            }
        }

        /// <summary>
        /// FechaFin
        /// </summary> 
        [XmlElement("FechaFin")]
        public String FechaFin
        {
            get {
                    return fechaFin; 
            }
            set {
                    fechaFin = value; 
            }
        }

        #endregion.
    }
}
