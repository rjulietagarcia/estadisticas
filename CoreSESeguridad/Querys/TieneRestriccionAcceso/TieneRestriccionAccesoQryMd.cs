using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TieneRestriccionAccesoQryMD
    {
        private TieneRestriccionAccesoQryDP tieneRestriccionAccesoQry = null;

        public TieneRestriccionAccesoQryMD(TieneRestriccionAccesoQryDP tieneRestriccionAccesoQry)
        {
            this.tieneRestriccionAccesoQry = tieneRestriccionAccesoQry;
        }

        protected static TieneRestriccionAccesoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TieneRestriccionAccesoQryDP tieneRestriccionAccesoQry = new TieneRestriccionAccesoQryDP();
            tieneRestriccionAccesoQry.NiveleducacionId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            tieneRestriccionAccesoQry.Niveltrabajo = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            tieneRestriccionAccesoQry.SostenimientoId = dr.IsDBNull(2) ? 0 : dr.GetInt32(2);
            tieneRestriccionAccesoQry.FechaInicio = dr.IsDBNull(3) ? "" : dr.GetDateTime(3).ToShortDateString();;
            tieneRestriccionAccesoQry.FechaFin = dr.IsDBNull(4) ? "" : dr.GetDateTime(4).ToShortDateString();;
            return tieneRestriccionAccesoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TieneRestriccionAccesoQry
            DbCommand com = con.CreateCommand();
            String countTieneRestriccionAccesoQry = String.Format(CultureInfo.CurrentCulture,TieneRestriccionAcceso.TieneRestriccionAccesoQry.TieneRestriccionAccesoQryCount,"");
            com.CommandText = countTieneRestriccionAccesoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TieneRestriccionAccesoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountTieneRestriccionAccesoQry = String.Format(CultureInfo.CurrentCulture,TieneRestriccionAcceso.TieneRestriccionAccesoQry.TieneRestriccionAccesoQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountTieneRestriccionAccesoQry);
                #endregion
            }
            return resultado;
        }
        public static TieneRestriccionAccesoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TieneRestriccionAccesoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTieneRestriccionAccesoQry = String.Format(CultureInfo.CurrentCulture, TieneRestriccionAcceso.TieneRestriccionAccesoQry.TieneRestriccionAccesoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTieneRestriccionAccesoQry = listTieneRestriccionAccesoQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTieneRestriccionAccesoQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TieneRestriccionAccesoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TieneRestriccionAccesoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TieneRestriccionAccesoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaTieneRestriccionAccesoQry = String.Format(CultureInfo.CurrentCulture,TieneRestriccionAcceso.TieneRestriccionAccesoQry.TieneRestriccionAccesoQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaTieneRestriccionAccesoQry);
                #endregion
            }
            return (TieneRestriccionAccesoQryDP[])lista.ToArray(typeof(TieneRestriccionAccesoQryDP));
        }
        public static Int64 CountForRestriccion(DbConnection con, DbTransaction tran, String aIdNiveleducacion, Int32 aNiveltrabajo, String aIdSostenimiento, String aFechaInicio, String aFechaFin)
        {
            Int64 resultado = 0;
            #region SQL CountForRestriccion TieneRestriccionAccesoQry
            DbCommand com = con.CreateCommand();
            String countTieneRestriccionAccesoQryForRestriccion = String.Format(CultureInfo.CurrentCulture,TieneRestriccionAcceso.TieneRestriccionAccesoQry.TieneRestriccionAccesoQryCount,"");
            countTieneRestriccionAccesoQryForRestriccion += " WHERE \n";
            String delimitador = "";
            countTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} Id_NivelEducacion in ("+aIdNiveleducacion+ ")\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} NivelTrabajo = {0}Niveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} id_Sostenimiento in ("+aIdSostenimiento+")\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} Fecha_Inicio >= {0}FechaInicio\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} Fecha_Fin <= {0}FechaFin\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countTieneRestriccionAccesoQryForRestriccion;
            #endregion
            #region Parametros countTieneRestriccionAccesoQryForRestriccion
            // Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdNiveleducacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Niveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aNiveltrabajo);
            // Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdSostenimiento);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}FechaInicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}FechaFin", ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForRestriccion TieneRestriccionAccesoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountTieneRestriccionAccesoQryForRestriccion = String.Format(CultureInfo.CurrentCulture,TieneRestriccionAcceso.TieneRestriccionAccesoQry.TieneRestriccionAccesoQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountTieneRestriccionAccesoQryForRestriccion);
                #endregion
            }
            return resultado;
        }
        public static TieneRestriccionAccesoQryDP[] ListForRestriccion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aIdNiveleducacion, Int32 aNiveltrabajo, Int32 aIdSostenimiento, String aFechaInicio, String aFechaFin)
        {
            #region SQL List TieneRestriccionAccesoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTieneRestriccionAccesoQryForRestriccion = String.Format(CultureInfo.CurrentCulture, TieneRestriccionAcceso.TieneRestriccionAccesoQry.TieneRestriccionAccesoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTieneRestriccionAccesoQryForRestriccion = listTieneRestriccionAccesoQryForRestriccion.Substring(6);
            listTieneRestriccionAccesoQryForRestriccion += " WHERE \n";
            String delimitador = "";
            listTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} NivelTrabajo = {0}Niveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} id_Sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} Fecha_Inicio = {0}FechaInicio\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listTieneRestriccionAccesoQryForRestriccion += String.Format("    {1} Fecha_Fin = {0}FechaFin\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTieneRestriccionAccesoQryForRestriccion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TieneRestriccionAccesoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdNiveleducacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Niveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aNiveltrabajo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdSostenimiento);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}FechaInicio",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(aFechaInicio,CultureInfo.CurrentCulture));
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}FechaFin",ConstantesGlobales.ParameterPrefix), DbType.DateTime, 0, ParameterDirection.Input, DateTime.Parse(aFechaFin,CultureInfo.CurrentCulture));
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TieneRestriccionAccesoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TieneRestriccionAccesoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaTieneRestriccionAccesoQry = String.Format(CultureInfo.CurrentCulture,TieneRestriccionAcceso.TieneRestriccionAccesoQry.TieneRestriccionAccesoQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaTieneRestriccionAccesoQry);
                #endregion
            }
            return (TieneRestriccionAccesoQryDP[])lista.ToArray(typeof(TieneRestriccionAccesoQryDP));
        }
    }
}
