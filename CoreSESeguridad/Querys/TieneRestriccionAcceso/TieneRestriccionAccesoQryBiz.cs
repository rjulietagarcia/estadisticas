using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class TieneRestriccionAccesoQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TieneRestriccionAccesoQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TieneRestriccionAccesoQry!", ex);
            }
            return resultado;
        }

        public static TieneRestriccionAccesoQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TieneRestriccionAccesoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TieneRestriccionAccesoQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TieneRestriccionAccesoQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForRestriccion(DbConnection con, String aIdNiveleducacion, Int32 aNiveltrabajo, String aIdSostenimiento, String aFechaInicio, String aFechaFin) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TieneRestriccionAccesoQryMD.CountForRestriccion(con, tran, aIdNiveleducacion, aNiveltrabajo, aIdSostenimiento, aFechaInicio, aFechaFin);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TieneRestriccionAccesoQry!", ex);
            }
            return resultado;
        }

        public static TieneRestriccionAccesoQryDP[] LoadListForRestriccion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aIdNiveleducacion, Int32 aNiveltrabajo, Int32 aIdSostenimiento, String aFechaInicio, String aFechaFin) 
        {
            TieneRestriccionAccesoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TieneRestriccionAccesoQryMD.ListForRestriccion(con, tran, startRowIndex, maximumRows, aIdNiveleducacion, aNiveltrabajo, aIdSostenimiento, aFechaInicio, aFechaFin);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TieneRestriccionAccesoQry!", ex);
            }
            return resultado;

        }
    }
}
