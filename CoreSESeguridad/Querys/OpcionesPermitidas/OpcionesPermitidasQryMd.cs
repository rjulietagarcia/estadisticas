using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class OpcionesPermitidasQryMD
    {
        private OpcionesPermitidasQryDP opcionesPermitidasQry = null;

        public OpcionesPermitidasQryMD(OpcionesPermitidasQryDP opcionesPermitidasQry)
        {
            this.opcionesPermitidasQry = opcionesPermitidasQry;
        }

        protected static OpcionesPermitidasQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            OpcionesPermitidasQryDP opcionesPermitidasQry = new OpcionesPermitidasQryDP();
            opcionesPermitidasQry.Llave = dr.IsDBNull(0) ? "" : dr.GetString(0);
            opcionesPermitidasQry.UsuIdUsuario = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            return opcionesPermitidasQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count OpcionesPermitidasQry
            DbCommand com = con.CreateCommand();
            String countOpcionesPermitidasQry = String.Format(CultureInfo.CurrentCulture,OpcionesPermitidas.OpcionesPermitidasQry.OpcionesPermitidasQryCount,"");
            com.CommandText = countOpcionesPermitidasQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count OpcionesPermitidasQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcionesPermitidasQry = String.Format(CultureInfo.CurrentCulture,OpcionesPermitidas.OpcionesPermitidasQry.OpcionesPermitidasQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcionesPermitidasQry);
                #endregion
            }
            return resultado;
        }
        public static OpcionesPermitidasQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List OpcionesPermitidasQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcionesPermitidasQry = String.Format(CultureInfo.CurrentCulture, OpcionesPermitidas.OpcionesPermitidasQry.OpcionesPermitidasQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listOpcionesPermitidasQry = listOpcionesPermitidasQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listOpcionesPermitidasQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listOpcionesPermitidasQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load OpcionesPermitidasQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista OpcionesPermitidasQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista OpcionesPermitidasQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcionesPermitidasQry = String.Format(CultureInfo.CurrentCulture,OpcionesPermitidas.OpcionesPermitidasQry.OpcionesPermitidasQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcionesPermitidasQry);
                #endregion
            }
            return (OpcionesPermitidasQryDP[])lista.ToArray(typeof(OpcionesPermitidasQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario OpcionesPermitidasQry
            DbCommand com = con.CreateCommand();
            String countOpcionesPermitidasQryForUsuario = String.Format(CultureInfo.CurrentCulture,OpcionesPermitidas.OpcionesPermitidasQry.OpcionesPermitidasQryCount,"");
            countOpcionesPermitidasQryForUsuario += " WHERE \n";
            String delimitador = "";
            countOpcionesPermitidasQryForUsuario += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countOpcionesPermitidasQryForUsuario;
            #endregion
            #region Parametros countOpcionesPermitidasQryForUsuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario OpcionesPermitidasQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcionesPermitidasQryForUsuario = String.Format(CultureInfo.CurrentCulture,OpcionesPermitidas.OpcionesPermitidasQry.OpcionesPermitidasQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcionesPermitidasQryForUsuario);
                #endregion
            }
            return resultado;
        }
        public static OpcionesPermitidasQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario)
        {
            #region SQL List OpcionesPermitidasQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcionesPermitidasQryForUsuario = String.Format(CultureInfo.CurrentCulture, OpcionesPermitidas.OpcionesPermitidasQry.OpcionesPermitidasQrySelect, "", String.Format("{0}UsuIdUsuario",Model.ConstantesGlobales.ParameterPrefix));
            // listOpcionesPermitidasQryForUsuario = listOpcionesPermitidasQryForUsuario.Substring(6);
            // String delimitador = "";
            //delimitador = " AND ";
            //String filtraList = String.Format(CultureInfo.CurrentCulture, "{1}",
            //    ConstantesGlobales.ParameterPrefix,
            //    listOpcionesPermitidasQryForUsuario, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listOpcionesPermitidasQryForUsuario;
            DbDataReader dr;
            #endregion
            #region Parametros Load OpcionesPermitidasQry
            // Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            // Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista OpcionesPermitidasQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista OpcionesPermitidasQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcionesPermitidasQry = String.Format(CultureInfo.CurrentCulture,OpcionesPermitidas.OpcionesPermitidasQry.OpcionesPermitidasQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcionesPermitidasQry);
                #endregion
            }
            return (OpcionesPermitidasQryDP[])lista.ToArray(typeof(OpcionesPermitidasQryDP));
        }
    }
}
