using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CctntUsrQryMD
    {
        private CctntUsrQryDP cctntUsrQry = null;

        public CctntUsrQryMD(CctntUsrQryDP cctntUsrQry)
        {
            this.cctntUsrQry = cctntUsrQry;
        }

        protected static CctntUsrQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CctntUsrQryDP cctntUsrQry = new CctntUsrQryDP();










            cctntUsrQry.CctntId = dr.IsDBNull(0) ? 0 : int.Parse(dr.GetValue(0).ToString());
            cctntUsrQry.NiveleducacionId = dr.IsDBNull(1) ? (Byte)0 :byte.Parse(dr.GetValue(1).ToString());
            cctntUsrQry.Niveleducacion = dr.IsDBNull(2) ? "" : dr.GetString(2);
            cctntUsrQry.TurnoId = dr.IsDBNull(3) ? 0 : int.Parse(dr.GetValue(3).ToString());
            cctntUsrQry.Truno = dr.IsDBNull(4) ? "" : dr.GetString(4);
            cctntUsrQry.CentrotrabajoId = dr.IsDBNull(5) ? 0 : int.Parse(dr.GetValue(5).ToString());
            cctntUsrQry.Nombrecct = dr.IsDBNull(6) ? "" : dr.GetString(6);
            cctntUsrQry.Clavecct = dr.IsDBNull(7) ? "" : dr.GetString(7);
            cctntUsrQry.SostenimientoId = dr.IsDBNull(8) ? (Byte)0 : byte.Parse(dr.GetValue(8).ToString());
            cctntUsrQry.Sostenimiento = dr.IsDBNull(9) ? "" : dr.GetString(9);
            cctntUsrQry.SubnivelId = dr.IsDBNull(10) ? 0 : int.Parse(dr.GetValue(10).ToString());
            cctntUsrQry.Subnivel = dr.IsDBNull(11) ? "" : dr.GetString(11);
            cctntUsrQry.TipoeducacionId = dr.IsDBNull(12) ? 0 : int.Parse(dr.GetValue(12).ToString());
            cctntUsrQry.Tipoeducacion = dr.IsDBNull(13) ? "" : dr.GetString(13);
            cctntUsrQry.NivelId = dr.IsDBNull(14) ? 0 : int.Parse(dr.GetValue(14).ToString());
            cctntUsrQry.Nivel = dr.IsDBNull(15) ? "" : dr.GetString(15);
            cctntUsrQry.RegionId = dr.IsDBNull(16) ? 0 : int.Parse(dr.GetValue(16).ToString());
            cctntUsrQry.Region = dr.IsDBNull(17) ? "" : dr.GetString(17);
            cctntUsrQry.ZonaId = dr.IsDBNull(18) ? 0 : int.Parse(dr.GetValue(18).ToString());
            cctntUsrQry.Numerozona = dr.IsDBNull(19) ? 0 : int.Parse(dr.GetValue(19).ToString());
            cctntUsrQry.UsuarioLtId = dr.IsDBNull(20) ? 0 : dr.GetInt32(20);
            cctntUsrQry.InmuebleId = dr.IsDBNull(21) ? 0 : dr.GetInt32(21);
            return cctntUsrQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CctntUsrQry
            DbCommand com = con.CreateCommand();
            String countCctntUsrQry = String.Format(CultureInfo.CurrentCulture,CctntUsr.CctntUsrQry.CctntUsrQryCount,"");
            com.CommandText = countCctntUsrQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CctntUsrQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CctntUsrQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CctntUsrQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCctntUsrQry = String.Format(CultureInfo.CurrentCulture, CctntUsr.CctntUsrQry.CctntUsrQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCctntUsrQry = listCctntUsrQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCctntUsrQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CctntUsrQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CctntUsrQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CctntUsrQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CctntUsrQryDP[])lista.ToArray(typeof(CctntUsrQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aIdUsuarioLt)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario CctntUsrQry
            DbCommand com = con.CreateCommand();
            String countCctntUsrQryForUsuario = String.Format(CultureInfo.CurrentCulture,CctntUsr.CctntUsrQry.CctntUsrQryCount,"");
            String delimitador = " AND ";
            if (aIdUsuarioLt != -1)
            {
                countCctntUsrQryForUsuario += String.Format("    {1} ult.id_usuario_Lt = {0}IdUsuarioLt\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCctntUsrQryForUsuario;
            #endregion
            #region Parametros countCctntUsrQryForUsuario
            if (aIdUsuarioLt != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuarioLt",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuarioLt);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario CctntUsrQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CctntUsrQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aIdUsuarioLt)
        {
            #region SQL List CctntUsrQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCctntUsrQryForUsuario = String.Format(CultureInfo.CurrentCulture, CctntUsr.CctntUsrQry.CctntUsrQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCctntUsrQryForUsuario = listCctntUsrQryForUsuario.Substring(6);
            String delimitador = " AND ";
            if (aIdUsuarioLt != -1)
            {
                listCctntUsrQryForUsuario += String.Format("    {1} ult.id_usuario_Lt = {0}IdUsuarioLt\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCctntUsrQryForUsuario, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CctntUsrQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdUsuarioLt != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuarioLt",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuarioLt);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CctntUsrQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CctntUsrQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CctntUsrQryDP[])lista.ToArray(typeof(CctntUsrQryDP));
        }
    }
}
