using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CctntUsrQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CctntUsrQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CctntUsrQry!", ex);
            }
            return resultado;
        }

        public static CctntUsrQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CctntUsrQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CctntUsrQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CctntUsrQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuario(DbConnection con, Int32 aIdUsuarioLt) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CctntUsrQryMD.CountForUsuario(con, tran, aIdUsuarioLt);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CctntUsrQry!", ex);
            }
            return resultado;
        }

        public static CctntUsrQryDP[] LoadListForUsuario(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aIdUsuarioLt) 
        {
            CctntUsrQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CctntUsrQryMD.ListForUsuario(con, tran, startRowIndex, maximumRows, aIdUsuarioLt);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CctntUsrQry!", ex);
            }
            return resultado;

        }
    }
}
