using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CctPorSostenimientoQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CctPorSostenimientoQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CctPorSostenimientoQry!", ex);
            }
            return resultado;
        }

        public static CctPorSostenimientoQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CctPorSostenimientoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CctPorSostenimientoQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CctPorSostenimientoQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, String aNombre, Int16 aIdNivel, Int16 aIdRegion, Int16 aIdZona, Byte aIdSostenimiento) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CctPorSostenimientoQryMD.CountForDescripcion(con, tran, aNombre, aIdNivel, aIdRegion, aIdZona, aIdSostenimiento);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CctPorSostenimientoQry!", ex);
            }
            return resultado;
        }

        public static CctPorSostenimientoQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdNivel, Int16 aIdRegion, Int16 aIdZona, Byte aIdSostenimiento) 
        {
            CctPorSostenimientoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CctPorSostenimientoQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aNombre, aIdNivel, aIdRegion, aIdZona, aIdSostenimiento);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CctPorSostenimientoQry!", ex);
            }
            return resultado;

        }
    }
}
