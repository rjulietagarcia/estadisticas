using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CctPorSostenimientoQryMD
    {
        private CctPorSostenimientoQryDP cctPorSostenimientoQry = null;

        public CctPorSostenimientoQryMD(CctPorSostenimientoQryDP cctPorSostenimientoQry)
        {
            this.cctPorSostenimientoQry = cctPorSostenimientoQry;
        }

        protected static CctPorSostenimientoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CctPorSostenimientoQryDP cctPorSostenimientoQry = new CctPorSostenimientoQryDP();
            cctPorSostenimientoQry.CentrotrabajoId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            cctPorSostenimientoQry.PaisId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            cctPorSostenimientoQry.EntidadId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            cctPorSostenimientoQry.RegionId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            cctPorSostenimientoQry.ZonaId = dr.IsDBNull(4) ? (short)0 : dr.GetInt16(4);
            cctPorSostenimientoQry.SostenimientoId = dr.IsDBNull(5) ? (Byte)0 : dr.GetByte(5);
            cctPorSostenimientoQry.Clave = dr.IsDBNull(6) ? "" : dr.GetString(6);
            cctPorSostenimientoQry.Turno3d = dr.IsDBNull(7) ? "" : dr.GetString(7);
            cctPorSostenimientoQry.Bitprovisional = dr.IsDBNull(8) ? false : dr.GetBoolean(8);
            cctPorSostenimientoQry.InmuebleId = dr.IsDBNull(9) ? 0 : dr.GetInt32(9);
            cctPorSostenimientoQry.DomicilioId = dr.IsDBNull(10) ? 0 : dr.GetInt32(10);
            cctPorSostenimientoQry.FechaFundacion = dr.IsDBNull(11) ? "" : dr.GetDateTime(11).ToShortDateString();;
            cctPorSostenimientoQry.FechaAlta = dr.IsDBNull(12) ? "" : dr.GetDateTime(12).ToShortDateString();;
            cctPorSostenimientoQry.FechaClausura = dr.IsDBNull(13) ? "" : dr.GetDateTime(13).ToShortDateString();;
            cctPorSostenimientoQry.MotivobajacentrotrabajoId = dr.IsDBNull(14) ? (short)0 : dr.GetInt16(14);
            cctPorSostenimientoQry.FechaReapertura = dr.IsDBNull(15) ? "" : dr.GetDateTime(15).ToShortDateString();;
            cctPorSostenimientoQry.BitActivo = dr.IsDBNull(16) ? false : dr.GetBoolean(16);
            cctPorSostenimientoQry.UsuarioId = dr.IsDBNull(17) ? 0 : dr.GetInt32(17);
            cctPorSostenimientoQry.FechaActualizacion = dr.IsDBNull(18) ? "" : dr.GetDateTime(18).ToShortDateString();;
            cctPorSostenimientoQry.FechaCambio = dr.IsDBNull(19) ? "" : dr.GetDateTime(19).ToShortDateString();;
            cctPorSostenimientoQry.Nombre = dr.IsDBNull(20) ? "" : dr.GetString(20);
            cctPorSostenimientoQry.TipoctId = dr.IsDBNull(21) ? "" : dr.GetString(21);
            cctPorSostenimientoQry.TipoeducacionId = dr.IsDBNull(22) ? (short)0 : dr.GetInt16(22);
            cctPorSostenimientoQry.NivelId = dr.IsDBNull(23) ? (short)0 : dr.GetInt16(23);
            cctPorSostenimientoQry.SubnivelId = dr.IsDBNull(24) ? (short)0 : dr.GetInt16(24);
            cctPorSostenimientoQry.ControlId = dr.IsDBNull(25) ? (short)0 : dr.GetInt16(25);
            cctPorSostenimientoQry.SubcontrolId = dr.IsDBNull(26) ? (short)0 : dr.GetInt16(26);
            cctPorSostenimientoQry.DependencianormativaId = dr.IsDBNull(27) ? (short)0 : dr.GetInt16(27);
            cctPorSostenimientoQry.DependenciaadministrativaId = dr.IsDBNull(28) ? (short)0 : dr.GetInt16(28);
            cctPorSostenimientoQry.ServicioId = dr.IsDBNull(29) ? (short)0 : dr.GetInt16(29);
            cctPorSostenimientoQry.SectorId = dr.IsDBNull(30) ? (short)0 : dr.GetInt16(30);
            cctPorSostenimientoQry.EducacionfisicaId = dr.IsDBNull(31) ? (short)0 : dr.GetInt16(31);
            cctPorSostenimientoQry.EstatusId = dr.IsDBNull(32) ? (short)0 : dr.GetInt16(32);
            cctPorSostenimientoQry.AlmacenId = dr.IsDBNull(33) ? (short)0 : dr.GetInt16(33);
            cctPorSostenimientoQry.Domicilio = dr.IsDBNull(34) ? "" : dr.GetString(34);
            cctPorSostenimientoQry.Entrecalle = dr.IsDBNull(35) ? "" : dr.GetString(35);
            cctPorSostenimientoQry.Ycalle = dr.IsDBNull(36) ? "" : dr.GetString(36);
            cctPorSostenimientoQry.Municipio = dr.IsDBNull(37) ? "" : dr.GetString(37);
            cctPorSostenimientoQry.Localidad = dr.IsDBNull(38) ? "" : dr.GetString(38);
            cctPorSostenimientoQry.Colonia = dr.IsDBNull(39) ? "" : dr.GetString(39);
            cctPorSostenimientoQry.Cp = dr.IsDBNull(40) ? "" : dr.GetString(40);
            cctPorSostenimientoQry.Telefono = dr.IsDBNull(41) ? "" : dr.GetString(41);
            cctPorSostenimientoQry.Telexten = dr.IsDBNull(42) ? "" : dr.GetString(42);
            cctPorSostenimientoQry.Fax = dr.IsDBNull(43) ? "" : dr.GetString(43);
            cctPorSostenimientoQry.Faxexten = dr.IsDBNull(44) ? "" : dr.GetString(44);
            cctPorSostenimientoQry.Ageb = dr.IsDBNull(45) ? "" : dr.GetString(45);
            cctPorSostenimientoQry.Clavecart = dr.IsDBNull(46) ? "" : dr.GetString(46);
            cctPorSostenimientoQry.Longitud = dr.IsDBNull(47) ? 0 : dr.GetInt32(47);
            cctPorSostenimientoQry.Latitud = dr.IsDBNull(48) ? 0 : dr.GetInt32(48);
            cctPorSostenimientoQry.Altitud = dr.IsDBNull(49) ? 0 : dr.GetInt32(49);
            cctPorSostenimientoQry.PuntocardinalId = dr.IsDBNull(50) ? (short)0 : dr.GetInt16(50);
            cctPorSostenimientoQry.Director = dr.IsDBNull(51) ? "" : dr.GetString(51);
            cctPorSostenimientoQry.Cartatopografica = dr.IsDBNull(52) ? "" : dr.GetString(52);
            cctPorSostenimientoQry.NumeroIncorporacion = dr.IsDBNull(53) ? "" : dr.GetString(53);
            cctPorSostenimientoQry.Folio = dr.IsDBNull(54) ? "" : dr.GetString(54);
            cctPorSostenimientoQry.DependenciaoperativaId = dr.IsDBNull(55) ? (short)0 : dr.GetInt16(55);
            cctPorSostenimientoQry.Observaciones = dr.IsDBNull(56) ? "" : dr.GetString(56);
            cctPorSostenimientoQry.IncorporacionId = dr.IsDBNull(57) ? (short)0 : dr.GetInt16(57);
            cctPorSostenimientoQry.Fechasol = dr.IsDBNull(58) ? "" : dr.GetDateTime(58).ToShortDateString();;
            cctPorSostenimientoQry.ClaveinstitucionalId = dr.IsDBNull(59) ? (short)0 : dr.GetInt16(59);
            cctPorSostenimientoQry.NiveleducacionId = dr.IsDBNull(60) ? (Byte)0 : dr.GetByte(60);
            cctPorSostenimientoQry.ClaveagrupadorId = dr.IsDBNull(61) ? (Byte)0 : dr.GetByte(61);
            cctPorSostenimientoQry.TurnoId = dr.IsDBNull(62) ? (short)0 : dr.GetInt16(62);
            cctPorSostenimientoQry.Mark = dr.IsDBNull(63) ? 0 : dr.GetInt32(63);
            cctPorSostenimientoQry.ValidDatos = dr.IsDBNull(64) ? 0 : dr.GetDecimal(64);
            return cctPorSostenimientoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CctPorSostenimientoQry
            DbCommand com = con.CreateCommand();
            String countCctPorSostenimientoQry = String.Format(CultureInfo.CurrentCulture,CctPorSostenimiento.CctPorSostenimientoQry.CctPorSostenimientoQryCount,"");
            com.CommandText = countCctPorSostenimientoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CctPorSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CctPorSostenimientoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CctPorSostenimientoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCctPorSostenimientoQry = String.Format(CultureInfo.CurrentCulture, CctPorSostenimiento.CctPorSostenimientoQry.CctPorSostenimientoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCctPorSostenimientoQry = listCctPorSostenimientoQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCctPorSostenimientoQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CctPorSostenimientoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CctPorSostenimientoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CctPorSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CctPorSostenimientoQryDP[])lista.ToArray(typeof(CctPorSostenimientoQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre, Int16 aIdNivel, Int16 aIdRegion, Int16 aIdZona, Byte aIdSostenimiento)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion CctPorSostenimientoQry
            DbCommand com = con.CreateCommand();
            String countCctPorSostenimientoQryForDescripcion = String.Format(CultureInfo.CurrentCulture,CctPorSostenimiento.CctPorSostenimientoQry.CctPorSostenimientoQryCount,"");
            countCctPorSostenimientoQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countCctPorSostenimientoQryForDescripcion += String.Format("    {1} Nombre like {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countCctPorSostenimientoQryForDescripcion += String.Format("    {1} id_nivel = {0}IdNivel\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countCctPorSostenimientoQryForDescripcion += String.Format("    {1} Id_Region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countCctPorSostenimientoQryForDescripcion += String.Format("    {1} Id_Zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countCctPorSostenimientoQryForDescripcion += String.Format("    {1} Id_Sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countCctPorSostenimientoQryForDescripcion;
            #endregion
            #region Parametros countCctPorSostenimientoQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdNivel);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion CctPorSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CctPorSostenimientoQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdNivel, Int16 aIdRegion, Int16 aIdZona, Byte aIdSostenimiento)
        {
            #region SQL List CctPorSostenimientoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCctPorSostenimientoQryForDescripcion = String.Format(CultureInfo.CurrentCulture, CctPorSostenimiento.CctPorSostenimientoQry.CctPorSostenimientoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCctPorSostenimientoQryForDescripcion = listCctPorSostenimientoQryForDescripcion.Substring(6);
            listCctPorSostenimientoQryForDescripcion += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listCctPorSostenimientoQryForDescripcion += String.Format("    {1} Nombre like {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listCctPorSostenimientoQryForDescripcion += String.Format("    {1} id_nivel = {0}IdNivel\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listCctPorSostenimientoQryForDescripcion += String.Format("    {1} Id_Region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listCctPorSostenimientoQryForDescripcion += String.Format("    {1} Id_Zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listCctPorSostenimientoQryForDescripcion += String.Format("    {1} Id_Sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCctPorSostenimientoQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CctPorSostenimientoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdNivel);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CctPorSostenimientoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CctPorSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CctPorSostenimientoQryDP[])lista.ToArray(typeof(CctPorSostenimientoQryDP));
        }
    }
}
