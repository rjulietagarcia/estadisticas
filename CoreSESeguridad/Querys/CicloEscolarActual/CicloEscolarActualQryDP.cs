using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "CicloEscolarActualQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 01 de junio de 2009.</Para>
    /// <Para>Hora: 03:56:24 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CicloEscolarActualQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TipocicloId</term><description>Descripcion TipocicloId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolaranteriorId</term><description>Descripcion CicloescolaranteriorId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarsiguienteId</term><description>Descripcion CicloescolarsiguienteId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CicloEscolarActualQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// CicloEscolarActualQryDTO cicloescolaractualqry = new CicloEscolarActualQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("CicloEscolarActualQry")]
    public class CicloEscolarActualQryDP
    {
        #region Definicion de campos privados.
        private Int16 tipocicloId;
        private Int16 cicloescolaranteriorId;
        private Int16 cicloescolarId;
        private Int16 cicloescolarsiguienteId;
        private Int32 usuarioId;
        private String fechaActualizacion;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TipocicloId
        /// </summary> 
        [XmlElement("TipocicloId")]
        public Int16 TipocicloId
        {
            get {
                    return tipocicloId; 
            }
            set {
                    tipocicloId = value; 
            }
        }

        /// <summary>
        /// CicloescolaranteriorId
        /// </summary> 
        [XmlElement("CicloescolaranteriorId")]
        public Int16 CicloescolaranteriorId
        {
            get {
                    return cicloescolaranteriorId; 
            }
            set {
                    cicloescolaranteriorId = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// CicloescolarsiguienteId
        /// </summary> 
        [XmlElement("CicloescolarsiguienteId")]
        public Int16 CicloescolarsiguienteId
        {
            get {
                    return cicloescolarsiguienteId; 
            }
            set {
                    cicloescolarsiguienteId = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        #endregion.
    }
}
