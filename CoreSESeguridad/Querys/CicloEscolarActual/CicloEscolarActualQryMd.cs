using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CicloEscolarActualQryMD
    {
        private CicloEscolarActualQryDP cicloEscolarActualQry = null;

        public CicloEscolarActualQryMD(CicloEscolarActualQryDP cicloEscolarActualQry)
        {
            this.cicloEscolarActualQry = cicloEscolarActualQry;
        }

        protected static CicloEscolarActualQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CicloEscolarActualQryDP cicloEscolarActualQry = new CicloEscolarActualQryDP();
            cicloEscolarActualQry.TipocicloId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            cicloEscolarActualQry.CicloescolaranteriorId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            cicloEscolarActualQry.CicloescolarId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            cicloEscolarActualQry.CicloescolarsiguienteId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            cicloEscolarActualQry.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            cicloEscolarActualQry.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            return cicloEscolarActualQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CicloEscolarActualQry
            DbCommand com = con.CreateCommand();
            String countCicloEscolarActualQry = String.Format(CultureInfo.CurrentCulture,CicloEscolarActual.CicloEscolarActualQry.CicloEscolarActualQryCount,"");
            com.CommandText = countCicloEscolarActualQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CicloEscolarActualQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountCicloEscolarActualQry = String.Format(CultureInfo.CurrentCulture,CicloEscolarActual.CicloEscolarActualQry.CicloEscolarActualQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountCicloEscolarActualQry);
                #endregion
            }
            return resultado;
        }
        public static CicloEscolarActualQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CicloEscolarActualQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCicloEscolarActualQry = String.Format(CultureInfo.CurrentCulture, CicloEscolarActual.CicloEscolarActualQry.CicloEscolarActualQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCicloEscolarActualQry = listCicloEscolarActualQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCicloEscolarActualQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCicloEscolarActualQry ;
            DbDataReader dr;
            #endregion
            #region Parametros Load CicloEscolarActualQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CicloEscolarActualQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CicloEscolarActualQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaCicloEscolarActualQry = String.Format(CultureInfo.CurrentCulture,CicloEscolarActual.CicloEscolarActualQry.CicloEscolarActualQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaCicloEscolarActualQry);
                #endregion
            }
            return (CicloEscolarActualQryDP[])lista.ToArray(typeof(CicloEscolarActualQryDP));
        }
        public static Int64 CountForTipoCiclo(DbConnection con, DbTransaction tran, Int16 aIdTipociclo)
        {
            Int64 resultado = 0;
            #region SQL CountForTipoCiclo CicloEscolarActualQry
            DbCommand com = con.CreateCommand();
            String countCicloEscolarActualQryForTipoCiclo = String.Format(CultureInfo.CurrentCulture,CicloEscolarActual.CicloEscolarActualQry.CicloEscolarActualQryCount,"");
            countCicloEscolarActualQryForTipoCiclo += " AND \n";
            String delimitador = "";
            if (aIdTipociclo != -1)
            {
                countCicloEscolarActualQryForTipoCiclo += String.Format("    {1} id_tipoCiclo = {0}IdTipociclo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCicloEscolarActualQryForTipoCiclo;
            #endregion
            #region Parametros countCicloEscolarActualQryForTipoCiclo
            if (aIdTipociclo != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTipociclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTipociclo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForTipoCiclo CicloEscolarActualQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountCicloEscolarActualQryForTipoCiclo = String.Format(CultureInfo.CurrentCulture,CicloEscolarActual.CicloEscolarActualQry.CicloEscolarActualQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountCicloEscolarActualQryForTipoCiclo);
                #endregion
            }
            return resultado;
        }
        public static CicloEscolarActualQryDP[] ListForTipoCiclo(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdTipociclo)
        {
            #region SQL List CicloEscolarActualQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCicloEscolarActualQryForTipoCiclo = String.Format(CultureInfo.CurrentCulture, CicloEscolarActual.CicloEscolarActualQry.CicloEscolarActualQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCicloEscolarActualQryForTipoCiclo = listCicloEscolarActualQryForTipoCiclo.Substring(6);
            listCicloEscolarActualQryForTipoCiclo += " AND \n";
            String delimitador = "";
            if (aIdTipociclo != -1)
            {
                listCicloEscolarActualQryForTipoCiclo += String.Format("    {1} id_tipoCiclo = {0}IdTipociclo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCicloEscolarActualQryForTipoCiclo, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCicloEscolarActualQryForTipoCiclo;
            DbDataReader dr;
            #endregion
            #region Parametros Load CicloEscolarActualQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdTipociclo != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTipociclo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTipociclo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CicloEscolarActualQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CicloEscolarActualQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaCicloEscolarActualQry = String.Format(CultureInfo.CurrentCulture,CicloEscolarActual.CicloEscolarActualQry.CicloEscolarActualQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaCicloEscolarActualQry);
                #endregion
            }
            return (CicloEscolarActualQryDP[])lista.ToArray(typeof(CicloEscolarActualQryDP));
        }
    }
}
