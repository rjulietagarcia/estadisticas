using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CicloEscolarActualQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CicloEscolarActualQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CicloEscolarActualQry!", ex);
            }
            return resultado;
        }

        public static CicloEscolarActualQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CicloEscolarActualQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CicloEscolarActualQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CicloEscolarActualQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForTipoCiclo(DbConnection con, Int16 aIdTipociclo) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CicloEscolarActualQryMD.CountForTipoCiclo(con, tran, aIdTipociclo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CicloEscolarActualQry!", ex);
            }
            return resultado;
        }

        public static CicloEscolarActualQryDP[] LoadListForTipoCiclo(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdTipociclo) 
        {
            CicloEscolarActualQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CicloEscolarActualQryMD.ListForTipoCiclo(con, tran, startRowIndex, maximumRows, aIdTipociclo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CicloEscolarActualQry!", ex);
            }
            return resultado;

        }
    }
}
