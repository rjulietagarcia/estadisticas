using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class RegionNivelEducacionQryMD
    {
        private RegionNivelEducacionQryDP regionNivelEducacionQry = null;

        public RegionNivelEducacionQryMD(RegionNivelEducacionQryDP regionNivelEducacionQry)
        {
            this.regionNivelEducacionQry = regionNivelEducacionQry;
        }

        protected static RegionNivelEducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            RegionNivelEducacionQryDP regionNivelEducacionQry = new RegionNivelEducacionQryDP();
            regionNivelEducacionQry.RegionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            regionNivelEducacionQry.PaisId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            regionNivelEducacionQry.EntidadId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            regionNivelEducacionQry.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            regionNivelEducacionQry.Clave = dr.IsDBNull(4) ? "" : dr.GetString(4);
            regionNivelEducacionQry.LetraFolio = dr.IsDBNull(5) ? "" : dr.GetString(5);
            regionNivelEducacionQry.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            regionNivelEducacionQry.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            regionNivelEducacionQry.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            regionNivelEducacionQry.NiveleducacionId = dr.IsDBNull(9) ? (Byte)0 : dr.GetByte(9);
            return regionNivelEducacionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count RegionNivelEducacionQry
            DbCommand com = con.CreateCommand();
            String countRegionNivelEducacionQry = String.Format(CultureInfo.CurrentCulture,RegionNivelEducacion.RegionNivelEducacionQry.RegionNivelEducacionQryCount,"");
            com.CommandText = countRegionNivelEducacionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count RegionNivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static RegionNivelEducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List RegionNivelEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listRegionNivelEducacionQry = String.Format(CultureInfo.CurrentCulture, RegionNivelEducacion.RegionNivelEducacionQry.RegionNivelEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listRegionNivelEducacionQry = listRegionNivelEducacionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listRegionNivelEducacionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load RegionNivelEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista RegionNivelEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista RegionNivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (RegionNivelEducacionQryDP[])lista.ToArray(typeof(RegionNivelEducacionQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre, Byte aIdNiveleducacion)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion RegionNivelEducacionQry
            DbCommand com = con.CreateCommand();
            String countRegionNivelEducacionQryForDescripcion = String.Format(CultureInfo.CurrentCulture,RegionNivelEducacion.RegionNivelEducacionQry.RegionNivelEducacionQryCount,"");
            countRegionNivelEducacionQryForDescripcion += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countRegionNivelEducacionQryForDescripcion += String.Format("    {1} r.Nombre = {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countRegionNivelEducacionQryForDescripcion += String.Format("    {1} ct.id_niveleducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countRegionNivelEducacionQryForDescripcion;
            #endregion
            #region Parametros countRegionNivelEducacionQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 60, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion RegionNivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static RegionNivelEducacionQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Byte aIdNiveleducacion)
        {
            #region SQL List RegionNivelEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listRegionNivelEducacionQryForDescripcion = String.Format(CultureInfo.CurrentCulture, RegionNivelEducacion.RegionNivelEducacionQry.RegionNivelEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listRegionNivelEducacionQryForDescripcion = listRegionNivelEducacionQryForDescripcion.Substring(6);
            listRegionNivelEducacionQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listRegionNivelEducacionQryForDescripcion += String.Format("    {1} Nombre = {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listRegionNivelEducacionQryForDescripcion += String.Format("    {1} id_niveleducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listRegionNivelEducacionQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load RegionNivelEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 60, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista RegionNivelEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista RegionNivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (RegionNivelEducacionQryDP[])lista.ToArray(typeof(RegionNivelEducacionQryDP));
        }
    }
}
