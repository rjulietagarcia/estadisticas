using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class RegionNivelEducacionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = RegionNivelEducacionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un RegionNivelEducacionQry!", ex);
            }
            return resultado;
        }

        public static RegionNivelEducacionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            RegionNivelEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = RegionNivelEducacionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de RegionNivelEducacionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, String aNombre, Byte aIdNiveleducacion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = RegionNivelEducacionQryMD.CountForDescripcion(con, tran, aNombre, aIdNiveleducacion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un RegionNivelEducacionQry!", ex);
            }
            return resultado;
        }

        public static RegionNivelEducacionQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Byte aIdNiveleducacion) 
        {
            RegionNivelEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = RegionNivelEducacionQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aNombre, aIdNiveleducacion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de RegionNivelEducacionQry!", ex);
            }
            return resultado;

        }
    }
}
