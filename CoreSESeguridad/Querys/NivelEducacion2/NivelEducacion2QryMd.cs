using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class NivelEducacion2QryMD
    {
        private NivelEducacionQryDP nivelEducacion2Qry = null;

        public NivelEducacion2QryMD(NivelEducacionQryDP nivelEducacion2Qry)
        {
            this.nivelEducacion2Qry = nivelEducacion2Qry;
        }

        protected static NivelEducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            NivelEducacionQryDP nivelEducacion2Qry = new NivelEducacionQryDP();
            nivelEducacion2Qry.TipoeducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            nivelEducacion2Qry.NivelId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            nivelEducacion2Qry.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            nivelEducacion2Qry.LetraFolio = dr.IsDBNull(3) ? "" : dr.GetString(3);
            nivelEducacion2Qry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            nivelEducacion2Qry.UsuarioId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            nivelEducacion2Qry.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            //nivelEducacion2Qry.UsuIdUsuario = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            return nivelEducacion2Qry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count NivelEducacion2Qry
            DbCommand com = con.CreateCommand();
            String countNivelEducacion2Qry = String.Format(CultureInfo.CurrentCulture,NivelEducacion2.NivelEducacion2Qry.NivelEducacion2QryCount,"");
            com.CommandText = countNivelEducacion2Qry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count NivelEducacion2Qry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static NivelEducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List NivelEducacion2Qry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNivelEducacion2Qry = String.Format(CultureInfo.CurrentCulture, NivelEducacion2.NivelEducacion2Qry.NivelEducacion2QrySelect, "", ConstantesGlobales.IncluyeRows);
            listNivelEducacion2Qry = listNivelEducacion2Qry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listNivelEducacion2Qry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load NivelEducacion2Qry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista NivelEducacion2Qry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista NivelEducacion2Qry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (NivelEducacionQryDP[])lista.ToArray(typeof(NivelEducacionQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario NivelEducacion2Qry
            DbCommand com = con.CreateCommand();
            String countNivelEducacion2QryForUsuario = String.Format(CultureInfo.CurrentCulture,NivelEducacion2.NivelEducacion2Qry.NivelEducacion2QryCount,"");
            countNivelEducacion2QryForUsuario += " AND \n";
            String delimitador = "";
            countNivelEducacion2QryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n group by n.id_nivel) contador", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countNivelEducacion2QryForUsuario;
            #endregion
            #region Parametros countNivelEducacion2QryForUsuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario NivelEducacion2Qry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static NivelEducacionQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario)
        {
            #region SQL List NivelEducacion2Qry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNivelEducacion2QryForUsuario = String.Format(CultureInfo.CurrentCulture, NivelEducacion2.NivelEducacion2Qry.NivelEducacion2QrySelect, "", ConstantesGlobales.IncluyeRows);
            //listNivelEducacion2QryForUsuario = listNivelEducacion2QryForUsuario.Substring(6);
            listNivelEducacion2QryForUsuario += " AND \n";
            String delimitador = "";
            listNivelEducacion2QryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, //ConstantesGlobales.TraeUnosRegistros,
               // ConstantesGlobales.ParameterPrefix,
                listNivelEducacion2QryForUsuario// ""
                //(startRowIndex + maximumRows).ToString(),
               // startRowIndex,
               // maximumRows,
                //"DISTINCT" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load NivelEducacion2Qry
           // Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista NivelEducacion2Qry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista NivelEducacion2Qry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (NivelEducacionQryDP[])lista.ToArray(typeof(NivelEducacionQryDP));
        }
    }
}
