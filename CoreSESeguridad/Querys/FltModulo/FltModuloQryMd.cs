using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class FltModuloQryMD
    {
        private FltModuloQryDP fltModuloQry = null;

        public FltModuloQryMD(FltModuloQryDP fltModuloQry)
        {
            this.fltModuloQry = fltModuloQry;
        }

        protected static FltModuloQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            FltModuloQryDP fltModuloQry = new FltModuloQryDP();
            fltModuloQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            fltModuloQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            fltModuloQry.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            fltModuloQry.Abreviatura = dr.IsDBNull(3) ? "" : dr.GetString(3);
            fltModuloQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            fltModuloQry.UsuarioId = dr.IsDBNull(5) ? 0 : dr.GetInt32(5);
            fltModuloQry.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            fltModuloQry.FechaInicio = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            fltModuloQry.FechaFin = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            fltModuloQry.CarpetaModulo = dr.IsDBNull(9) ? "" : dr.GetString(9);
            return fltModuloQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count FltModuloQry
            DbCommand com = con.CreateCommand();
            String countFltModuloQry = String.Format(CultureInfo.CurrentCulture,FltModulo.FltModuloQry.FltModuloQryCount,"");
            com.CommandText = countFltModuloQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count FltModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltModuloQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List FltModuloQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltModuloQry = String.Format(CultureInfo.CurrentCulture, FltModulo.FltModuloQry.FltModuloQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltModuloQry = listFltModuloQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltModuloQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltModuloQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltModuloQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltModuloQryDP[])lista.ToArray(typeof(FltModuloQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave FltModuloQry
            DbCommand com = con.CreateCommand();
            String countFltModuloQryForLlave = String.Format(CultureInfo.CurrentCulture,FltModulo.FltModuloQry.FltModuloQryCount,"");
            countFltModuloQryForLlave += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countFltModuloQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countFltModuloQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countFltModuloQryForLlave;
            #endregion
            #region Parametros countFltModuloQryForLlave
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave FltModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltModuloQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo)
        {
            #region SQL List FltModuloQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltModuloQryForLlave = String.Format(CultureInfo.CurrentCulture, FltModulo.FltModuloQry.FltModuloQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltModuloQryForLlave = listFltModuloQryForLlave.Substring(6);
            listFltModuloQryForLlave += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listFltModuloQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listFltModuloQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltModuloQryForLlave, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltModuloQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltModuloQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltModuloQryDP[])lista.ToArray(typeof(FltModuloQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, Byte aIdSistema, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion FltModuloQry
            DbCommand com = con.CreateCommand();
            String countFltModuloQryForDescripcion = String.Format(CultureInfo.CurrentCulture,FltModulo.FltModuloQry.FltModuloQryCount,"");
            countFltModuloQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countFltModuloQryForDescripcion += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre != "")
            {
                countFltModuloQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countFltModuloQryForDescripcion;
            #endregion
            #region Parametros countFltModuloQryForDescripcion
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion FltModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltModuloQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, String aNombre)
        {
            #region SQL List FltModuloQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltModuloQryForDescripcion = String.Format(CultureInfo.CurrentCulture, FltModulo.FltModuloQry.FltModuloQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltModuloQryForDescripcion = listFltModuloQryForDescripcion.Substring(6);
            listFltModuloQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listFltModuloQryForDescripcion += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre != "")
            {
                listFltModuloQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltModuloQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltModuloQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltModuloQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltModuloQryDP[])lista.ToArray(typeof(FltModuloQryDP));
        }
    }
}
