using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class FltModuloQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltModuloQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltModuloQry!", ex);
            }
            return resultado;
        }

        public static FltModuloQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            FltModuloQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltModuloQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltModuloQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLlave(DbConnection con, Byte aIdSistema, Byte aIdModulo) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltModuloQryMD.CountForLlave(con, tran, aIdSistema, aIdModulo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltModuloQry!", ex);
            }
            return resultado;
        }

        public static FltModuloQryDP[] LoadListForLlave(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo) 
        {
            FltModuloQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltModuloQryMD.ListForLlave(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltModuloQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, Byte aIdSistema, String aNombre) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltModuloQryMD.CountForDescripcion(con, tran, aIdSistema, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltModuloQry!", ex);
            }
            return resultado;
        }

        public static FltModuloQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, String aNombre) 
        {
            FltModuloQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltModuloQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aIdSistema, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltModuloQry!", ex);
            }
            return resultado;

        }
    }
}
