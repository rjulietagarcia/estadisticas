using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class TreeOpcionesQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreeOpcionesQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreeOpcionesQry!", ex);
            }
            return resultado;
        }

        public static TreeOpcionesQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TreeOpcionesQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreeOpcionesQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreeOpcionesQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLlave(DbConnection con, Byte aIdSistema, Int32 aIdModulo, Int32 aIdOpcion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreeOpcionesQryMD.CountForLlave(con, tran, aIdSistema, aIdModulo, aIdOpcion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreeOpcionesQry!", ex);
            }
            return resultado;
        }

        public static TreeOpcionesQryDP[] LoadListForLlave(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Int32 aIdModulo, Int32 aIdOpcion) 
        {
            TreeOpcionesQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreeOpcionesQryMD.ListForLlave(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreeOpcionesQry!", ex);
            }
            return resultado;

        }
    }
}
