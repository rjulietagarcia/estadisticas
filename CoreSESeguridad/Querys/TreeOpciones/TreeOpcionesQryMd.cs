using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TreeOpcionesQryMD
    {
        private TreeOpcionesQryDP treeOpcionesQry = null;

        public TreeOpcionesQryMD(TreeOpcionesQryDP treeOpcionesQry)
        {
            this.treeOpcionesQry = treeOpcionesQry;
        }

        protected static TreeOpcionesQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TreeOpcionesQryDP treeOpcionesQry = new TreeOpcionesQryDP();
            treeOpcionesQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            treeOpcionesQry.ModuloId = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            treeOpcionesQry.OpcionId = dr.IsDBNull(2) ? 0 : dr.GetInt32(2);
            treeOpcionesQry.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            return treeOpcionesQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TreeOpcionesQry
            DbCommand com = con.CreateCommand();
            String countTreeOpcionesQry = String.Format(CultureInfo.CurrentCulture,TreeOpciones.TreeOpcionesQry.TreeOpcionesQryCount,"");
            com.CommandText = countTreeOpcionesQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TreeOpcionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeOpcionesQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TreeOpcionesQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeOpcionesQry = String.Format(CultureInfo.CurrentCulture, TreeOpciones.TreeOpcionesQry.TreeOpcionesQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreeOpcionesQry = listTreeOpcionesQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreeOpcionesQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeOpcionesQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeOpcionesQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeOpcionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeOpcionesQryDP[])lista.ToArray(typeof(TreeOpcionesQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Byte aIdSistema, Int32 aIdModulo, Int32 aIdOpcion)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave TreeOpcionesQry
            DbCommand com = con.CreateCommand();
            String countTreeOpcionesQryForLlave = String.Format(CultureInfo.CurrentCulture,TreeOpciones.TreeOpcionesQry.TreeOpcionesQryCount,"");
            countTreeOpcionesQryForLlave += " AND  \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countTreeOpcionesQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countTreeOpcionesQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countTreeOpcionesQryForLlave += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTreeOpcionesQryForLlave;
            #endregion
            #region Parametros countTreeOpcionesQryForLlave
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave TreeOpcionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeOpcionesQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Int32 aIdModulo, Int32 aIdOpcion)
        {
            #region SQL List TreeOpcionesQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeOpcionesQryForLlave = String.Format(CultureInfo.CurrentCulture, TreeOpciones.TreeOpcionesQry.TreeOpcionesQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreeOpcionesQryForLlave = listTreeOpcionesQryForLlave.Substring(6);
            listTreeOpcionesQryForLlave += " AND  \n";
            String delimitador = "";
            if (aIdSistema == 0)
                aIdSistema = 1;
            if (aIdSistema != 0)
            {
                listTreeOpcionesQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listTreeOpcionesQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listTreeOpcionesQryForLlave += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }

            listTreeOpcionesQryForLlave += String.Format("    Order by Id_Sistema, Id_Modulo, Id_Opcion\n");

            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreeOpcionesQryForLlave, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeOpcionesQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeOpcionesQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeOpcionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeOpcionesQryDP[])lista.ToArray(typeof(TreeOpcionesQryDP));
        }
    }
}
