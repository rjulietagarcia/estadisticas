using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CategoriaLocalidadQryMD
    {
        private CategoriaLocalidadQryDP categoriaLocalidadQry = null;

        public CategoriaLocalidadQryMD(CategoriaLocalidadQryDP categoriaLocalidadQry)
        {
            this.categoriaLocalidadQry = categoriaLocalidadQry;
        }

        protected static CategoriaLocalidadQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CategoriaLocalidadQryDP categoriaLocalidadQry = new CategoriaLocalidadQryDP();
            categoriaLocalidadQry.CategorialocalidadId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            categoriaLocalidadQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            categoriaLocalidadQry.BitActivo = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            categoriaLocalidadQry.UsuarioId = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            categoriaLocalidadQry.FechaActualizacion = dr.IsDBNull(4) ? "" : dr.GetDateTime(4).ToShortDateString();;
            return categoriaLocalidadQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CategoriaLocalidadQry
            DbCommand com = con.CreateCommand();
            String countCategoriaLocalidadQry = String.Format(CultureInfo.CurrentCulture,CategoriaLocalidad.CategoriaLocalidadQry.CategoriaLocalidadQryCount,"");
            com.CommandText = countCategoriaLocalidadQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CategoriaLocalidadQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountCategoriaLocalidadQry = String.Format(CultureInfo.CurrentCulture,CategoriaLocalidad.CategoriaLocalidadQry.CategoriaLocalidadQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountCategoriaLocalidadQry);
                #endregion
            }
            return resultado;
        }
        public static CategoriaLocalidadQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CategoriaLocalidadQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCategoriaLocalidadQry = String.Format(CultureInfo.CurrentCulture, CategoriaLocalidad.CategoriaLocalidadQry.CategoriaLocalidadQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCategoriaLocalidadQry = listCategoriaLocalidadQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCategoriaLocalidadQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CategoriaLocalidadQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CategoriaLocalidadQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CategoriaLocalidadQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaCategoriaLocalidadQry = String.Format(CultureInfo.CurrentCulture,CategoriaLocalidad.CategoriaLocalidadQry.CategoriaLocalidadQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaCategoriaLocalidadQry);
                #endregion
            }
            return (CategoriaLocalidadQryDP[])lista.ToArray(typeof(CategoriaLocalidadQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion CategoriaLocalidadQry
            DbCommand com = con.CreateCommand();
            String countCategoriaLocalidadQryForDescripcion = String.Format(CultureInfo.CurrentCulture,CategoriaLocalidad.CategoriaLocalidadQry.CategoriaLocalidadQryCount,"");
            countCategoriaLocalidadQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countCategoriaLocalidadQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCategoriaLocalidadQryForDescripcion;
            #endregion
            #region Parametros countCategoriaLocalidadQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion CategoriaLocalidadQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountCategoriaLocalidadQryForDescripcion = String.Format(CultureInfo.CurrentCulture,CategoriaLocalidad.CategoriaLocalidadQry.CategoriaLocalidadQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountCategoriaLocalidadQryForDescripcion);
                #endregion
            }
            return resultado;
        }
        public static CategoriaLocalidadQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre)
        {
            #region SQL List CategoriaLocalidadQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCategoriaLocalidadQryForDescripcion = String.Format(CultureInfo.CurrentCulture, CategoriaLocalidad.CategoriaLocalidadQry.CategoriaLocalidadQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCategoriaLocalidadQryForDescripcion = listCategoriaLocalidadQryForDescripcion.Substring(6);
            listCategoriaLocalidadQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listCategoriaLocalidadQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCategoriaLocalidadQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CategoriaLocalidadQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CategoriaLocalidadQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CategoriaLocalidadQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaCategoriaLocalidadQry = String.Format(CultureInfo.CurrentCulture,CategoriaLocalidad.CategoriaLocalidadQry.CategoriaLocalidadQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaCategoriaLocalidadQry);
                #endregion
            }
            return (CategoriaLocalidadQryDP[])lista.ToArray(typeof(CategoriaLocalidadQryDP));
        }
    }
}
