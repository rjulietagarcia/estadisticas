using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class FltSistemaQryMD
    {
        private FltSistemaQryDP fltSistemaQry = null;

        public FltSistemaQryMD(FltSistemaQryDP fltSistemaQry)
        {
            this.fltSistemaQry = fltSistemaQry;
        }

        protected static FltSistemaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            FltSistemaQryDP fltSistemaQry = new FltSistemaQryDP();
            fltSistemaQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            fltSistemaQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            fltSistemaQry.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            fltSistemaQry.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            fltSistemaQry.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            fltSistemaQry.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            fltSistemaQry.FechaInicio = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            fltSistemaQry.FechaFin = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            fltSistemaQry.CarpetaSistema = dr.IsDBNull(8) ? "" : dr.GetString(8);
            return fltSistemaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count FltSistemaQry
            DbCommand com = con.CreateCommand();
            String countFltSistemaQry = String.Format(CultureInfo.CurrentCulture,FltSistema.FltSistemaQry.FltSistemaQryCount,"");
            com.CommandText = countFltSistemaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count FltSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltSistemaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List FltSistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltSistemaQry = String.Format(CultureInfo.CurrentCulture, FltSistema.FltSistemaQry.FltSistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltSistemaQry = listFltSistemaQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltSistemaQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltSistemaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltSistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltSistemaQryDP[])lista.ToArray(typeof(FltSistemaQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave FltSistemaQry
            DbCommand com = con.CreateCommand();
            String countFltSistemaQryForLlave = String.Format(CultureInfo.CurrentCulture,FltSistema.FltSistemaQry.FltSistemaQryCount,"");
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countFltSistemaQryForLlave += " WHERE \n";
                countFltSistemaQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countFltSistemaQryForLlave;
            #endregion
            #region Parametros countFltSistemaQryForLlave
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave FltSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltSistemaQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema)
        {
            #region SQL List FltSistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltSistemaQryForLlave = String.Format(CultureInfo.CurrentCulture, FltSistema.FltSistemaQry.FltSistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltSistemaQryForLlave = listFltSistemaQryForLlave.Substring(6);
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listFltSistemaQryForLlave += " WHERE \n";
                listFltSistemaQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltSistemaQryForLlave, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltSistemaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltSistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltSistemaQryDP[])lista.ToArray(typeof(FltSistemaQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion FltSistemaQry
            DbCommand com = con.CreateCommand();
            String countFltSistemaQryForDescripcion = String.Format(CultureInfo.CurrentCulture,FltSistema.FltSistemaQry.FltSistemaQryCount,"");
            countFltSistemaQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countFltSistemaQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countFltSistemaQryForDescripcion;
            #endregion
            #region Parametros countFltSistemaQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion FltSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltSistemaQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre)
        {
            #region SQL List FltSistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltSistemaQryForDescripcion = String.Format(CultureInfo.CurrentCulture, FltSistema.FltSistemaQry.FltSistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltSistemaQryForDescripcion = listFltSistemaQryForDescripcion.Substring(6);
            listFltSistemaQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listFltSistemaQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltSistemaQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltSistemaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltSistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltSistemaQryDP[])lista.ToArray(typeof(FltSistemaQryDP));
        }
    }
}
