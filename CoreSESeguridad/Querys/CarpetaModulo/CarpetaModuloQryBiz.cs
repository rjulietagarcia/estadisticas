using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CarpetaModuloQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CarpetaModuloQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CarpetaModuloQry!", ex);
            }
            return resultado;
        }

        public static CarpetaModuloQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CarpetaModuloQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CarpetaModuloQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CarpetaModuloQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForCarpeta(DbConnection con, Byte aIdSistema, String aCarpetaModulo) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CarpetaModuloQryMD.CountForCarpeta(con, tran, aIdSistema, aCarpetaModulo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CarpetaModuloQry!", ex);
            }
            return resultado;
        }

        public static CarpetaModuloQryDP[] LoadListForCarpeta(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, String aCarpetaModulo) 
        {
            CarpetaModuloQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CarpetaModuloQryMD.ListForCarpeta(con, tran, startRowIndex, maximumRows, aIdSistema, aCarpetaModulo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CarpetaModuloQry!", ex);
            }
            return resultado;

        }
    }
}
