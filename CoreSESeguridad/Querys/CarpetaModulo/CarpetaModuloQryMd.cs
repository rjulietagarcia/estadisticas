using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CarpetaModuloQryMD
    {
        private CarpetaModuloQryDP carpetaModuloQry = null;

        public CarpetaModuloQryMD(CarpetaModuloQryDP carpetaModuloQry)
        {
            this.carpetaModuloQry = carpetaModuloQry;
        }

        protected static CarpetaModuloQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CarpetaModuloQryDP carpetaModuloQry = new CarpetaModuloQryDP();
            carpetaModuloQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            carpetaModuloQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            carpetaModuloQry.CarpetaModulo = dr.IsDBNull(2) ? "" : dr.GetString(2);
            return carpetaModuloQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CarpetaModuloQry
            DbCommand com = con.CreateCommand();
            String countCarpetaModuloQry = String.Format(CultureInfo.CurrentCulture,CarpetaModulo.CarpetaModuloQry.CarpetaModuloQryCount,"");
            com.CommandText = countCarpetaModuloQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CarpetaModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CarpetaModuloQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CarpetaModuloQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCarpetaModuloQry = String.Format(CultureInfo.CurrentCulture, CarpetaModulo.CarpetaModuloQry.CarpetaModuloQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCarpetaModuloQry = listCarpetaModuloQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCarpetaModuloQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCarpetaModuloQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load CarpetaModuloQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CarpetaModuloQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CarpetaModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CarpetaModuloQryDP[])lista.ToArray(typeof(CarpetaModuloQryDP));
        }
        public static Int64 CountForCarpeta(DbConnection con, DbTransaction tran, Byte aIdSistema, String aCarpetaModulo)
        {
            Int64 resultado = 0;
            #region SQL CountForCarpeta CarpetaModuloQry
            DbCommand com = con.CreateCommand();
            String countCarpetaModuloQryForCarpeta = String.Format(CultureInfo.CurrentCulture,CarpetaModulo.CarpetaModuloQry.CarpetaModuloQryCount,"");
            countCarpetaModuloQryForCarpeta += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countCarpetaModuloQryForCarpeta += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aCarpetaModulo.Length != 0)
            {
                countCarpetaModuloQryForCarpeta += String.Format("    {1} Carpeta_Modulo = {0}CarpetaModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCarpetaModuloQryForCarpeta;
            #endregion
            #region Parametros countCarpetaModuloQryForCarpeta
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aCarpetaModulo.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CarpetaModulo",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 255, ParameterDirection.Input, aCarpetaModulo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForCarpeta CarpetaModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CarpetaModuloQryDP[] ListForCarpeta(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, String aCarpetaModulo)
        {
            #region SQL List CarpetaModuloQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCarpetaModuloQryForCarpeta = String.Format(CultureInfo.CurrentCulture, CarpetaModulo.CarpetaModuloQry.CarpetaModuloQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCarpetaModuloQryForCarpeta = listCarpetaModuloQryForCarpeta.Substring(6);
//            listCarpetaModuloQryForCarpeta += " WHERE \n";
            String delimitador = " And ";
            if (aIdSistema != 0)
            {
                listCarpetaModuloQryForCarpeta += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aCarpetaModulo.Length != 0)
            {
                listCarpetaModuloQryForCarpeta += String.Format("    {1} Carpeta_Modulo = {0}CarpetaModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCarpetaModuloQryForCarpeta, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCarpetaModuloQryForCarpeta;
            DbDataReader dr;
            #endregion
            #region Parametros Load CarpetaModuloQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aCarpetaModulo.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CarpetaModulo",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 255, ParameterDirection.Input, aCarpetaModulo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CarpetaModuloQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CarpetaModuloQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CarpetaModuloQryDP[])lista.ToArray(typeof(CarpetaModuloQryDP));
        }
    }
}
