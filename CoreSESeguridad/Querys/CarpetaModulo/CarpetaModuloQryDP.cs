using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "CarpetaModuloQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 17 de agosto de 2009.</Para>
    /// <Para>Hora: 10:59:22 a.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CarpetaModuloQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ModuloId</term><description>Descripcion ModuloId</description>
    ///    </item>
    ///    <item>
    ///        <term>CarpetaModulo</term><description>Descripcion CarpetaModulo</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CarpetaModuloQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// CarpetaModuloQryDTO carpetamoduloqry = new CarpetaModuloQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("CarpetaModuloQry")]
    public class CarpetaModuloQryDP
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Byte moduloId;
        private String carpetaModulo;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// ModuloId
        /// </summary> 
        [XmlElement("ModuloId")]
        public Byte ModuloId
        {
            get {
                    return moduloId; 
            }
            set {
                    moduloId = value; 
            }
        }

        /// <summary>
        /// CarpetaModulo
        /// </summary> 
        [XmlElement("CarpetaModulo")]
        public String CarpetaModulo
        {
            get {
                    return carpetaModulo; 
            }
            set {
                    carpetaModulo = value; 
            }
        }

        #endregion.
    }
}
