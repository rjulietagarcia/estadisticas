using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class PerfilEducacionSostenimientoQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilEducacionSostenimientoQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilEducacionSostenimientoQry!", ex);
            }
            return resultado;
        }

        public static PerfilEducacionSostenimientoQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PerfilEducacionSostenimientoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionSostenimientoQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilEducacionSostenimientoQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuario(DbConnection con, Int32 aUsuIdUsuario) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilEducacionSostenimientoQryMD.CountForUsuario(con, tran, aUsuIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilEducacionSostenimientoQry!", ex);
            }
            return resultado;
        }

        public static PerfilEducacionSostenimientoQryDP[] LoadListForUsuario(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario) 
        {
            PerfilEducacionSostenimientoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionSostenimientoQryMD.ListForUsuario(con, tran, startRowIndex, maximumRows, aUsuIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilEducacionSostenimientoQry!", ex);
            }
            return resultado;

        }
    }
}
