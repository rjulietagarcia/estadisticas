using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class PerfilEducacionSostenimientoQryMD
    {
        private PerfilEducacionSostenimientoQryDP perfilEducacionSostenimientoQry = null;

        public PerfilEducacionSostenimientoQryMD(PerfilEducacionSostenimientoQryDP perfilEducacionSostenimientoQry)
        {
            this.perfilEducacionSostenimientoQry = perfilEducacionSostenimientoQry;
        }

        protected static PerfilEducacionSostenimientoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilEducacionSostenimientoQryDP perfilEducacionSostenimientoQry = new PerfilEducacionSostenimientoQryDP();
            perfilEducacionSostenimientoQry.PerfilEducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            perfilEducacionSostenimientoQry.NiveltrabajoId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            perfilEducacionSostenimientoQry.CicloescolarId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            perfilEducacionSostenimientoQry.UsuIdUsuario = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            perfilEducacionSostenimientoQry.NombrePerfil = dr.IsDBNull(4) ? "" : dr.GetString(4);
            perfilEducacionSostenimientoQry.AbreviaturaPerfil = dr.IsDBNull(5) ? "" : dr.GetString(5);
            perfilEducacionSostenimientoQry.SostenimientoId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            perfilEducacionSostenimientoQry.NombreSostenimiento = dr.IsDBNull(7) ? "" : dr.GetString(7);
            perfilEducacionSostenimientoQry.Bitfederal = dr.IsDBNull(8) ? false : dr.GetBoolean(8);
            perfilEducacionSostenimientoQry.Bitestatal = dr.IsDBNull(9) ? false : dr.GetBoolean(9);
            perfilEducacionSostenimientoQry.Bitparticular = dr.IsDBNull(10) ? false : dr.GetBoolean(10);
            perfilEducacionSostenimientoQry.SistemaId = dr.IsDBNull(11) ? (Byte)0 : dr.GetByte(11);
            perfilEducacionSostenimientoQry.NombreSistema = dr.IsDBNull(12) ? "" : dr.GetString(12);
            perfilEducacionSostenimientoQry.AbreviaturaSistema = dr.IsDBNull(13) ? "" : dr.GetString(13);
            perfilEducacionSostenimientoQry.CarpetaSistema = dr.IsDBNull(14) ? "" : dr.GetString(14);
            perfilEducacionSostenimientoQry.BitActivo = dr.IsDBNull(15) ? false : dr.GetBoolean(15);
            return perfilEducacionSostenimientoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilEducacionSostenimientoQry
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionSostenimientoQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoQry.PerfilEducacionSostenimientoQryCount,"");
            com.CommandText = countPerfilEducacionSostenimientoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacionSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPerfilEducacionSostenimientoQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoQry.PerfilEducacionSostenimientoQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPerfilEducacionSostenimientoQry);
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionSostenimientoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilEducacionSostenimientoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionSostenimientoQry = String.Format(CultureInfo.CurrentCulture, PerfilEducacionSostenimiento.PerfilEducacionSostenimientoQry.PerfilEducacionSostenimientoQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listPerfilEducacionSostenimientoQry = listPerfilEducacionSostenimientoQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPerfilEducacionSostenimientoQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPerfilEducacionSostenimientoQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionSostenimientoQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionSostenimientoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPerfilEducacionSostenimientoQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoQry.PerfilEducacionSostenimientoQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPerfilEducacionSostenimientoQry);
                #endregion
            }
            return (PerfilEducacionSostenimientoQryDP[])lista.ToArray(typeof(PerfilEducacionSostenimientoQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario PerfilEducacionSostenimientoQry
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionSostenimientoQryForUsuario = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoQry.PerfilEducacionSostenimientoQryCount,"");
            //countPerfilEducacionSostenimientoQryForUsuario += " WHERE \n";
            String delimitador = " and ";
            if (aUsuIdUsuario != -1)
            {
                countPerfilEducacionSostenimientoQryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countPerfilEducacionSostenimientoQryForUsuario;
            #endregion
            #region Parametros countPerfilEducacionSostenimientoQryForUsuario
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario PerfilEducacionSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountPerfilEducacionSostenimientoQryForUsuario = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoQry.PerfilEducacionSostenimientoQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountPerfilEducacionSostenimientoQryForUsuario);
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionSostenimientoQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario)
        {
            #region SQL List PerfilEducacionSostenimientoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionSostenimientoQryForUsuario = String.Format(CultureInfo.CurrentCulture, PerfilEducacionSostenimiento.PerfilEducacionSostenimientoQry.PerfilEducacionSostenimientoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listPerfilEducacionSostenimientoQryForUsuario = listPerfilEducacionSostenimientoQryForUsuario.Substring(6);
            //listPerfilEducacionSostenimientoQryForUsuario += " WHERE \n";
            String delimitador = " and ";
            if (aUsuIdUsuario != -1)
            {
                listPerfilEducacionSostenimientoQryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPerfilEducacionSostenimientoQryForUsuario, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPerfilEducacionSostenimientoQryForUsuario;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionSostenimientoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionSostenimientoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionSostenimientoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaPerfilEducacionSostenimientoQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionSostenimiento.PerfilEducacionSostenimientoQry.PerfilEducacionSostenimientoQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaPerfilEducacionSostenimientoQry);
                #endregion
            }
            return (PerfilEducacionSostenimientoQryDP[])lista.ToArray(typeof(PerfilEducacionSostenimientoQryDP));
        }
    }
}
