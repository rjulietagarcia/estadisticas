using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilEducacionSostenimientoQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 02 de junio de 2009.</Para>
    /// <Para>Hora: 04:52:54 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilEducacionSostenimientoQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuario</term><description>Descripcion UsuIdUsuario</description>
    ///    </item>
    ///    <item>
    ///        <term>NombrePerfil</term><description>Descripcion NombrePerfil</description>
    ///    </item>
    ///    <item>
    ///        <term>AbreviaturaPerfil</term><description>Descripcion AbreviaturaPerfil</description>
    ///    </item>
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>NombreSostenimiento</term><description>Descripcion NombreSostenimiento</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitfederal</term><description>Descripcion Bitfederal</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitestatal</term><description>Descripcion Bitestatal</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitparticular</term><description>Descripcion Bitparticular</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>NombreSistema</term><description>Descripcion NombreSistema</description>
    ///    </item>
    ///    <item>
    ///        <term>AbreviaturaSistema</term><description>Descripcion AbreviaturaSistema</description>
    ///    </item>
    ///    <item>
    ///        <term>CarpetaSistema</term><description>Descripcion CarpetaSistema</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilEducacionSostenimientoQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// PerfilEducacionSostenimientoQryDTO perfileducacionsostenimientoqry = new PerfilEducacionSostenimientoQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("PerfilEducacionSostenimientoQry")]
    public class PerfilEducacionSostenimientoQryDP
    {
        #region Definicion de campos privados.
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Int16 cicloescolarId;
        private Int32 usuIdUsuario;
        private String nombrePerfil;
        private String abreviaturaPerfil;
        private Byte sostenimientoId;
        private String nombreSostenimiento;
        private Boolean bitfederal;
        private Boolean bitestatal;
        private Boolean bitparticular;
        private Byte sistemaId;
        private String nombreSistema;
        private String abreviaturaSistema;
        private String carpetaSistema;
        private Boolean bitActivo;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuario
        /// </summary> 
        [XmlElement("UsuIdUsuario")]
        public Int32 UsuIdUsuario
        {
            get {
                    return usuIdUsuario; 
            }
            set {
                    usuIdUsuario = value; 
            }
        }

        /// <summary>
        /// NombrePerfil
        /// </summary> 
        [XmlElement("NombrePerfil")]
        public String NombrePerfil
        {
            get {
                    return nombrePerfil; 
            }
            set {
                    nombrePerfil = value; 
            }
        }

        /// <summary>
        /// AbreviaturaPerfil
        /// </summary> 
        [XmlElement("AbreviaturaPerfil")]
        public String AbreviaturaPerfil
        {
            get {
                    return abreviaturaPerfil; 
            }
            set {
                    abreviaturaPerfil = value; 
            }
        }

        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// NombreSostenimiento
        /// </summary> 
        [XmlElement("NombreSostenimiento")]
        public String NombreSostenimiento
        {
            get {
                    return nombreSostenimiento; 
            }
            set {
                    nombreSostenimiento = value; 
            }
        }

        /// <summary>
        /// Bitfederal
        /// </summary> 
        [XmlElement("Bitfederal")]
        public Boolean Bitfederal
        {
            get {
                    return bitfederal; 
            }
            set {
                    bitfederal = value; 
            }
        }

        /// <summary>
        /// Bitestatal
        /// </summary> 
        [XmlElement("Bitestatal")]
        public Boolean Bitestatal
        {
            get {
                    return bitestatal; 
            }
            set {
                    bitestatal = value; 
            }
        }

        /// <summary>
        /// Bitparticular
        /// </summary> 
        [XmlElement("Bitparticular")]
        public Boolean Bitparticular
        {
            get {
                    return bitparticular; 
            }
            set {
                    bitparticular = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// NombreSistema
        /// </summary> 
        [XmlElement("NombreSistema")]
        public String NombreSistema
        {
            get {
                    return nombreSistema; 
            }
            set {
                    nombreSistema = value; 
            }
        }

        /// <summary>
        /// AbreviaturaSistema
        /// </summary> 
        [XmlElement("AbreviaturaSistema")]
        public String AbreviaturaSistema
        {
            get {
                    return abreviaturaSistema; 
            }
            set {
                    abreviaturaSistema = value; 
            }
        }

        /// <summary>
        /// CarpetaSistema
        /// </summary> 
        [XmlElement("CarpetaSistema")]
        public String CarpetaSistema
        {
            get {
                    return carpetaSistema; 
            }
            set {
                    carpetaSistema = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        #endregion.
    }
}
