using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CttEscolarQryMD
    {
        private CttEscolarQryDP cttEscolarQry = null;

        public CttEscolarQryMD(CttEscolarQryDP cttEscolarQry)
        {
            this.cttEscolarQry = cttEscolarQry;
        }

        protected static CttEscolarQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CttEscolarQryDP cttEscolarQry = new CttEscolarQryDP();
            cttEscolarQry.ZonaId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            cttEscolarQry.TipoeducacionId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            cttEscolarQry.NivelId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            cttEscolarQry.Numerozona = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            cttEscolarQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            cttEscolarQry.Clave = dr.IsDBNull(5) ? "" : dr.GetString(5);
            cttEscolarQry.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            cttEscolarQry.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            cttEscolarQry.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            cttEscolarQry.NiveleducacionId = dr.IsDBNull(9) ? (Byte)0 : dr.GetByte(9);
            cttEscolarQry.RegionId = dr.IsDBNull(10) ? (short)0 : dr.GetInt16(10);
            cttEscolarQry.SostenimientoId = dr.IsDBNull(11) ? (Byte)0 : dr.GetByte(11);
            cttEscolarQry.TipoctId = dr.IsDBNull(12) ? "" : dr.GetString(12);
            return cttEscolarQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CttEscolarQry
            DbCommand com = con.CreateCommand();
            String countCttEscolarQry = String.Format(CultureInfo.CurrentCulture,CttEscolar.CttEscolarQry.CttEscolarQryCount,"");
            com.CommandText = countCttEscolarQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CttEscolarQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CttEscolarQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CttEscolarQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCttEscolarQry = String.Format(CultureInfo.CurrentCulture, CttEscolar.CttEscolarQry.CttEscolarQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCttEscolarQry = listCttEscolarQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCttEscolarQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CttEscolarQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CttEscolarQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CttEscolarQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CttEscolarQryDP[])lista.ToArray(typeof(CttEscolarQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Int16 aIdZona)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave CttEscolarQry
            DbCommand com = con.CreateCommand();
            String countCttEscolarQryForLlave = String.Format(CultureInfo.CurrentCulture,CttEscolar.CttEscolarQry.CttEscolarQryCount,"");
            countCttEscolarQryForLlave += " AND \n";
            String delimitador = "";
            if (aIdZona != -1)
            {
                countCttEscolarQryForLlave += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCttEscolarQryForLlave;
            #endregion
            #region Parametros countCttEscolarQryForLlave
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave CttEscolarQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CttEscolarQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdZona)
        {
            #region SQL List CttEscolarQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCttEscolarQryForLlave = String.Format(CultureInfo.CurrentCulture, CttEscolar.CttEscolarQry.CttEscolarQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCttEscolarQryForLlave = listCttEscolarQryForLlave.Substring(6);
            listCttEscolarQryForLlave += " AND \n";
            String delimitador = "";
            if (aIdZona != -1)
            {
                listCttEscolarQryForLlave += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCttEscolarQryForLlave, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CttEscolarQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CttEscolarQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CttEscolarQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CttEscolarQryDP[])lista.ToArray(typeof(CttEscolarQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, String aIdTipoct, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion CttEscolarQry
            DbCommand com = con.CreateCommand();
            String countCttEscolarQryForDescripcion = String.Format(CultureInfo.CurrentCulture,CttEscolar.CttEscolarQry.CttEscolarQryCount,"");
            String delimitador = " AND ";
            if (aIdNiveleducacion != 0)
            {
                countCttEscolarQryForDescripcion += String.Format("    {1} ct.Id_Nivel = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != 0)
            {
                countCttEscolarQryForDescripcion += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento!=0)
            {
                countCttEscolarQryForDescripcion += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countCttEscolarQryForDescripcion += String.Format("    {1} ct.Id_tipoct = {0}IdTipoct\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aNombre != "")
            {
                countCttEscolarQryForDescripcion += String.Format("    {1} z.nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCttEscolarQryForDescripcion;
            #endregion
            #region Parametros countCttEscolarQryForDescripcion
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTipoct",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, aIdTipoct);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (aIdRegion != 0 || aIdSostenimiento != 0)
            {
                if (tran != null)
                {
                    com.Transaction = tran;
                }
                try
                {
                    resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
                }
                catch (DbException ex)
                {
                    LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                    #region Error CountForDescripcion CttEscolarQry
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    throw ex;
                    #endregion
                }
            }
            return resultado;
        }
        public static CttEscolarQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, String aIdTipoct, String aNombre)
        {
            #region SQL List CttEscolarQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCttEscolarQryForDescripcion = String.Format(CultureInfo.CurrentCulture, CttEscolar.CttEscolarQry.CttEscolarQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCttEscolarQryForDescripcion = listCttEscolarQryForDescripcion.Substring(6);
            String delimitador = " AND ";
            if (aIdNiveleducacion != 0)
            {
                listCttEscolarQryForDescripcion += String.Format("    {1} ct.Id_Nivel = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != 0)
            {
                listCttEscolarQryForDescripcion += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                listCttEscolarQryForDescripcion += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listCttEscolarQryForDescripcion += String.Format("    {1} ct.Id_tipoct = {0}IdTipoct\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aNombre != "")
            {
                listCttEscolarQryForDescripcion += String.Format("    {1} z.nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCttEscolarQryForDescripcion + " GROUP BY z.id_zona,z.id_tipoeducacion,z.id_nivel,z.numerozona,z.nombre,z.clave,z.id_sostenimiento,	z.bit_activo,	z.id_usuario,	z.fecha_actualizacion,cnt.Id_NivelEducacion,ct.id_region,ct.id_sostenimiento,ct.Id_tipoct", "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CttEscolarQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTipoct",ConstantesGlobales.ParameterPrefix), DbType.AnsiStringFixedLength, 1, ParameterDirection.Input, aIdTipoct);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (aIdRegion != 0 || aIdSostenimiento != 0)
            {
                if (tran != null)
                {
                    com.Transaction = tran;
                }
                try
                {
                    #region Lee la lista CttEscolarQry
                    dr = com.ExecuteReader(CommandBehavior.Default);
                    try
                    {
                        while (dr.Read())
                        {
                            lista.Add(ReadRow(dr));
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                    #endregion
                }
                catch (DbException ex)
                {
                    LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                    #region Error Load Lista CttEscolarQry
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    throw ex;
                    #endregion
                }
            }
            return (CttEscolarQryDP[])lista.ToArray(typeof(CttEscolarQryDP));
        }
    }
}
