using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CttEscolarQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CttEscolarQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CttEscolarQry!", ex);
            }
            return resultado;
        }

        public static CttEscolarQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CttEscolarQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CttEscolarQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CttEscolarQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLlave(DbConnection con, Int16 aIdZona) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CttEscolarQryMD.CountForLlave(con, tran, aIdZona);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CttEscolarQry!", ex);
            }
            return resultado;
        }

        public static CttEscolarQryDP[] LoadListForLlave(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdZona) 
        {
            CttEscolarQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CttEscolarQryMD.ListForLlave(con, tran, startRowIndex, maximumRows, aIdZona);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CttEscolarQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, String aIdTipoct, String aNombre) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CttEscolarQryMD.CountForDescripcion(con, tran, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdTipoct, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CttEscolarQry!", ex);
            }
            return resultado;
        }

        public static CttEscolarQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, String aIdTipoct, String aNombre) 
        {
            CttEscolarQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CttEscolarQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdTipoct, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CttEscolarQry!", ex);
            }
            return resultado;

        }
    }
}
