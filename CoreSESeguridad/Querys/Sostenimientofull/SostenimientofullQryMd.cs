using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class SostenimientofullQryMD
    {
        private SostenimientofullQryDP sostenimientofullQry = null;

        public SostenimientofullQryMD(SostenimientofullQryDP sostenimientofullQry)
        {
            this.sostenimientofullQry = sostenimientofullQry;
        }

        protected static SostenimientofullQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SostenimientofullQryDP sostenimientofullQry = new SostenimientofullQryDP();
            sostenimientofullQry.SostenimientoId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            sostenimientofullQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            sostenimientofullQry.Bitfederal = dr.IsDBNull(2) ? false : dr.GetBoolean(2);
            sostenimientofullQry.Bitestatal = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            sostenimientofullQry.Bitparticular = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            sostenimientofullQry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            sostenimientofullQry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            sostenimientofullQry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            return sostenimientofullQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count SostenimientofullQry
            DbCommand com = con.CreateCommand();
            String countSostenimientofullQry = String.Format(CultureInfo.CurrentCulture,Sostenimientofull.SostenimientofullQry.SostenimientofullQryCount,"");
            com.CommandText = countSostenimientofullQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count SostenimientofullQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SostenimientofullQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List SostenimientofullQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSostenimientofullQry = String.Format(CultureInfo.CurrentCulture, Sostenimientofull.SostenimientofullQry.SostenimientofullQrySelect, "", ConstantesGlobales.IncluyeRows);
            listSostenimientofullQry = listSostenimientofullQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSostenimientofullQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load SostenimientofullQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SostenimientofullQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SostenimientofullQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SostenimientofullQryDP[])lista.ToArray(typeof(SostenimientofullQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion SostenimientofullQry
            DbCommand com = con.CreateCommand();
            String countSostenimientofullQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Sostenimientofull.SostenimientofullQry.SostenimientofullQryCount,"");
            countSostenimientofullQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countSostenimientofullQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countSostenimientofullQryForDescripcion;
            #endregion
            #region Parametros countSostenimientofullQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion SostenimientofullQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SostenimientofullQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre)
        {
            #region SQL List SostenimientofullQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSostenimientofullQryForDescripcion = String.Format(CultureInfo.CurrentCulture, Sostenimientofull.SostenimientofullQry.SostenimientofullQrySelect, "", ConstantesGlobales.IncluyeRows);
            listSostenimientofullQryForDescripcion = listSostenimientofullQryForDescripcion.Substring(6);
            listSostenimientofullQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listSostenimientofullQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSostenimientofullQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load SostenimientofullQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SostenimientofullQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SostenimientofullQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SostenimientofullQryDP[])lista.ToArray(typeof(SostenimientofullQryDP));
        }
    }
}
