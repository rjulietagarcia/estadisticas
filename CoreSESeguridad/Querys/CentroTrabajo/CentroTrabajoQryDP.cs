using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "CentroTrabajoQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: viernes, 12 de junio de 2009.</Para>
    /// <Para>Hora: 01:18:04 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CentroTrabajoQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>CentrotrabajoId</term><description>Descripcion CentrotrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>PaisId</term><description>Descripcion PaisId</description>
    ///    </item>
    ///    <item>
    ///        <term>EntidadId</term><description>Descripcion EntidadId</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    ///    <item>
    ///        <term>ZonaId</term><description>Descripcion ZonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Clave</term><description>Descripcion Clave</description>
    ///    </item>
    ///    <item>
    ///        <term>Turno3d</term><description>Descripcion Turno3d</description>
    ///    </item>
    ///    <item>
    ///        <term>Bitprovisional</term><description>Descripcion Bitprovisional</description>
    ///    </item>
    ///    <item>
    ///        <term>InmuebleId</term><description>Descripcion InmuebleId</description>
    ///    </item>
    ///    <item>
    ///        <term>DomicilioId</term><description>Descripcion DomicilioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaFundacion</term><description>Descripcion FechaFundacion</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaAlta</term><description>Descripcion FechaAlta</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaClausura</term><description>Descripcion FechaClausura</description>
    ///    </item>
    ///    <item>
    ///        <term>MotivobajacentrotrabajoId</term><description>Descripcion MotivobajacentrotrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaReapertura</term><description>Descripcion FechaReapertura</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaCambio</term><description>Descripcion FechaCambio</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>TipoctId</term><description>Descripcion TipoctId</description>
    ///    </item>
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>SubnivelId</term><description>Descripcion SubnivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>ControlId</term><description>Descripcion ControlId</description>
    ///    </item>
    ///    <item>
    ///        <term>SubcontrolId</term><description>Descripcion SubcontrolId</description>
    ///    </item>
    ///    <item>
    ///        <term>DependencianormativaId</term><description>Descripcion DependencianormativaId</description>
    ///    </item>
    ///    <item>
    ///        <term>DependenciaadministrativaId</term><description>Descripcion DependenciaadministrativaId</description>
    ///    </item>
    ///    <item>
    ///        <term>ServicioId</term><description>Descripcion ServicioId</description>
    ///    </item>
    ///    <item>
    ///        <term>SectorId</term><description>Descripcion SectorId</description>
    ///    </item>
    ///    <item>
    ///        <term>EducacionfisicaId</term><description>Descripcion EducacionfisicaId</description>
    ///    </item>
    ///    <item>
    ///        <term>EstatusId</term><description>Descripcion EstatusId</description>
    ///    </item>
    ///    <item>
    ///        <term>AlmacenId</term><description>Descripcion AlmacenId</description>
    ///    </item>
    ///    <item>
    ///        <term>Domicilio</term><description>Descripcion Domicilio</description>
    ///    </item>
    ///    <item>
    ///        <term>Entrecalle</term><description>Descripcion Entrecalle</description>
    ///    </item>
    ///    <item>
    ///        <term>Ycalle</term><description>Descripcion Ycalle</description>
    ///    </item>
    ///    <item>
    ///        <term>Municipio</term><description>Descripcion Municipio</description>
    ///    </item>
    ///    <item>
    ///        <term>Localidad</term><description>Descripcion Localidad</description>
    ///    </item>
    ///    <item>
    ///        <term>Colonia</term><description>Descripcion Colonia</description>
    ///    </item>
    ///    <item>
    ///        <term>Cp</term><description>Descripcion Cp</description>
    ///    </item>
    ///    <item>
    ///        <term>Telefono</term><description>Descripcion Telefono</description>
    ///    </item>
    ///    <item>
    ///        <term>Telexten</term><description>Descripcion Telexten</description>
    ///    </item>
    ///    <item>
    ///        <term>Fax</term><description>Descripcion Fax</description>
    ///    </item>
    ///    <item>
    ///        <term>Faxexten</term><description>Descripcion Faxexten</description>
    ///    </item>
    ///    <item>
    ///        <term>Ageb</term><description>Descripcion Ageb</description>
    ///    </item>
    ///    <item>
    ///        <term>Clavecart</term><description>Descripcion Clavecart</description>
    ///    </item>
    ///    <item>
    ///        <term>Longitud</term><description>Descripcion Longitud</description>
    ///    </item>
    ///    <item>
    ///        <term>Latitud</term><description>Descripcion Latitud</description>
    ///    </item>
    ///    <item>
    ///        <term>Altitud</term><description>Descripcion Altitud</description>
    ///    </item>
    ///    <item>
    ///        <term>PuntocardinalId</term><description>Descripcion PuntocardinalId</description>
    ///    </item>
    ///    <item>
    ///        <term>Director</term><description>Descripcion Director</description>
    ///    </item>
    ///    <item>
    ///        <term>Cartatopografica</term><description>Descripcion Cartatopografica</description>
    ///    </item>
    ///    <item>
    ///        <term>NumeroIncorporacion</term><description>Descripcion NumeroIncorporacion</description>
    ///    </item>
    ///    <item>
    ///        <term>Folio</term><description>Descripcion Folio</description>
    ///    </item>
    ///    <item>
    ///        <term>DependenciaoperativaId</term><description>Descripcion DependenciaoperativaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Observaciones</term><description>Descripcion Observaciones</description>
    ///    </item>
    ///    <item>
    ///        <term>Fechasol</term><description>Descripcion Fechasol</description>
    ///    </item>
    ///    <item>
    ///        <term>ClaveinstitucionalId</term><description>Descripcion ClaveinstitucionalId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveleducacionId</term><description>Descripcion NiveleducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>ClaveagrupadorId</term><description>Descripcion ClaveagrupadorId</description>
    ///    </item>
    ///    <item>
    ///        <term>TurnoId</term><description>Descripcion TurnoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Mark</term><description>Descripcion Mark</description>
    ///    </item>
    ///    <item>
    ///        <term>ValidDatos</term><description>Descripcion ValidDatos</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CentroTrabajoQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// CentroTrabajoQryDTO centrotrabajoqry = new CentroTrabajoQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("CentroTrabajoQry")]
    public class CentroTrabajoQryDP
    {
        #region Definicion de campos privados.
        private Int16 centrotrabajoId;
        private Int16 paisId;
        private Int16 entidadId;
        private Int16 regionId;
        private Int16 zonaId;
        private Byte sostenimientoId;
        private String clave;
        private String turno3d;
        private Boolean bitprovisional;
        private Int32 inmuebleId;
        private Int32 domicilioId;
        private String fechaFundacion;
        private String fechaAlta;
        private String fechaClausura;
        private Int16 motivobajacentrotrabajoId;
        private String fechaReapertura;
        private Boolean usuarioId;
        private String fechaActualizacion;
        private String fechaCambio;
        private String nombre;
        private String tipoctId;
        private Int16 tipoeducacionId;
        private Int16 nivelId;
        private Int16 subnivelId;
        private Int16 controlId;
        private Int16 subcontrolId;
        private Int16 dependencianormativaId;
        private Int16 dependenciaadministrativaId;
        private Int16 servicioId;
        private Int16 sectorId;
        private Int16 educacionfisicaId;
        private Int16 estatusId;
        private Int16 almacenId;
        private String domicilio;
        private String entrecalle;
        private String ycalle;
        private String municipio;
        private String localidad;
        private String colonia;
        private String cp;
        private String telefono;
        private String telexten;
        private String fax;
        private String faxexten;
        private String ageb;
        private String clavecart;
        private Int32 longitud;
        private Int32 latitud;
        private Int32 altitud;
        private Int16 puntocardinalId;
        private String director;
        private String cartatopografica;
        private String numeroIncorporacion;
        private String folio;
        private Int16 dependenciaoperativaId;
        private String observaciones;
        private Int16 fechasol;
        private Int16 claveinstitucionalId;
        private Byte niveleducacionId;
        private Byte claveagrupadorId;
        private Int16 turnoId;
        private Int32 mark;
        private Decimal validDatos;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// CentrotrabajoId
        /// </summary> 
        [XmlElement("CentrotrabajoId")]
        public Int16 CentrotrabajoId
        {
            get {
                    return centrotrabajoId; 
            }
            set {
                    centrotrabajoId = value; 
            }
        }

        /// <summary>
        /// PaisId
        /// </summary> 
        [XmlElement("PaisId")]
        public Int16 PaisId
        {
            get {
                    return paisId; 
            }
            set {
                    paisId = value; 
            }
        }

        /// <summary>
        /// EntidadId
        /// </summary> 
        [XmlElement("EntidadId")]
        public Int16 EntidadId
        {
            get {
                    return entidadId; 
            }
            set {
                    entidadId = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public Int16 RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        /// <summary>
        /// ZonaId
        /// </summary> 
        [XmlElement("ZonaId")]
        public Int16 ZonaId
        {
            get {
                    return zonaId; 
            }
            set {
                    zonaId = value; 
            }
        }

        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// Clave
        /// </summary> 
        [XmlElement("Clave")]
        public String Clave
        {
            get {
                    return clave; 
            }
            set {
                    clave = value; 
            }
        }

        /// <summary>
        /// Turno3d
        /// </summary> 
        [XmlElement("Turno3d")]
        public String Turno3d
        {
            get {
                    return turno3d; 
            }
            set {
                    turno3d = value; 
            }
        }

        /// <summary>
        /// Bitprovisional
        /// </summary> 
        [XmlElement("Bitprovisional")]
        public Boolean Bitprovisional
        {
            get {
                    return bitprovisional; 
            }
            set {
                    bitprovisional = value; 
            }
        }

        /// <summary>
        /// InmuebleId
        /// </summary> 
        [XmlElement("InmuebleId")]
        public Int32 InmuebleId
        {
            get {
                    return inmuebleId; 
            }
            set {
                    inmuebleId = value; 
            }
        }

        /// <summary>
        /// DomicilioId
        /// </summary> 
        [XmlElement("DomicilioId")]
        public Int32 DomicilioId
        {
            get {
                    return domicilioId; 
            }
            set {
                    domicilioId = value; 
            }
        }

        /// <summary>
        /// FechaFundacion
        /// </summary> 
        [XmlElement("FechaFundacion")]
        public String FechaFundacion
        {
            get {
                    return fechaFundacion; 
            }
            set {
                    fechaFundacion = value; 
            }
        }

        /// <summary>
        /// FechaAlta
        /// </summary> 
        [XmlElement("FechaAlta")]
        public String FechaAlta
        {
            get {
                    return fechaAlta; 
            }
            set {
                    fechaAlta = value; 
            }
        }

        /// <summary>
        /// FechaClausura
        /// </summary> 
        [XmlElement("FechaClausura")]
        public String FechaClausura
        {
            get {
                    return fechaClausura; 
            }
            set {
                    fechaClausura = value; 
            }
        }

        /// <summary>
        /// MotivobajacentrotrabajoId
        /// </summary> 
        [XmlElement("MotivobajacentrotrabajoId")]
        public Int16 MotivobajacentrotrabajoId
        {
            get {
                    return motivobajacentrotrabajoId; 
            }
            set {
                    motivobajacentrotrabajoId = value; 
            }
        }

        /// <summary>
        /// FechaReapertura
        /// </summary> 
        [XmlElement("FechaReapertura")]
        public String FechaReapertura
        {
            get {
                    return fechaReapertura; 
            }
            set {
                    fechaReapertura = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Boolean UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// FechaCambio
        /// </summary> 
        [XmlElement("FechaCambio")]
        public String FechaCambio
        {
            get {
                    return fechaCambio; 
            }
            set {
                    fechaCambio = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// TipoctId
        /// </summary> 
        [XmlElement("TipoctId")]
        public String TipoctId
        {
            get {
                    return tipoctId; 
            }
            set {
                    tipoctId = value; 
            }
        }

        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public Int16 TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public Int16 NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        /// <summary>
        /// SubnivelId
        /// </summary> 
        [XmlElement("SubnivelId")]
        public Int16 SubnivelId
        {
            get {
                    return subnivelId; 
            }
            set {
                    subnivelId = value; 
            }
        }

        /// <summary>
        /// ControlId
        /// </summary> 
        [XmlElement("ControlId")]
        public Int16 ControlId
        {
            get {
                    return controlId; 
            }
            set {
                    controlId = value; 
            }
        }

        /// <summary>
        /// SubcontrolId
        /// </summary> 
        [XmlElement("SubcontrolId")]
        public Int16 SubcontrolId
        {
            get {
                    return subcontrolId; 
            }
            set {
                    subcontrolId = value; 
            }
        }

        /// <summary>
        /// DependencianormativaId
        /// </summary> 
        [XmlElement("DependencianormativaId")]
        public Int16 DependencianormativaId
        {
            get {
                    return dependencianormativaId; 
            }
            set {
                    dependencianormativaId = value; 
            }
        }

        /// <summary>
        /// DependenciaadministrativaId
        /// </summary> 
        [XmlElement("DependenciaadministrativaId")]
        public Int16 DependenciaadministrativaId
        {
            get {
                    return dependenciaadministrativaId; 
            }
            set {
                    dependenciaadministrativaId = value; 
            }
        }

        /// <summary>
        /// ServicioId
        /// </summary> 
        [XmlElement("ServicioId")]
        public Int16 ServicioId
        {
            get {
                    return servicioId; 
            }
            set {
                    servicioId = value; 
            }
        }

        /// <summary>
        /// SectorId
        /// </summary> 
        [XmlElement("SectorId")]
        public Int16 SectorId
        {
            get {
                    return sectorId; 
            }
            set {
                    sectorId = value; 
            }
        }

        /// <summary>
        /// EducacionfisicaId
        /// </summary> 
        [XmlElement("EducacionfisicaId")]
        public Int16 EducacionfisicaId
        {
            get {
                    return educacionfisicaId; 
            }
            set {
                    educacionfisicaId = value; 
            }
        }

        /// <summary>
        /// EstatusId
        /// </summary> 
        [XmlElement("EstatusId")]
        public Int16 EstatusId
        {
            get {
                    return estatusId; 
            }
            set {
                    estatusId = value; 
            }
        }

        /// <summary>
        /// AlmacenId
        /// </summary> 
        [XmlElement("AlmacenId")]
        public Int16 AlmacenId
        {
            get {
                    return almacenId; 
            }
            set {
                    almacenId = value; 
            }
        }

        /// <summary>
        /// Domicilio
        /// </summary> 
        [XmlElement("Domicilio")]
        public String Domicilio
        {
            get {
                    return domicilio; 
            }
            set {
                    domicilio = value; 
            }
        }

        /// <summary>
        /// Entrecalle
        /// </summary> 
        [XmlElement("Entrecalle")]
        public String Entrecalle
        {
            get {
                    return entrecalle; 
            }
            set {
                    entrecalle = value; 
            }
        }

        /// <summary>
        /// Ycalle
        /// </summary> 
        [XmlElement("Ycalle")]
        public String Ycalle
        {
            get {
                    return ycalle; 
            }
            set {
                    ycalle = value; 
            }
        }

        /// <summary>
        /// Municipio
        /// </summary> 
        [XmlElement("Municipio")]
        public String Municipio
        {
            get {
                    return municipio; 
            }
            set {
                    municipio = value; 
            }
        }

        /// <summary>
        /// Localidad
        /// </summary> 
        [XmlElement("Localidad")]
        public String Localidad
        {
            get {
                    return localidad; 
            }
            set {
                    localidad = value; 
            }
        }

        /// <summary>
        /// Colonia
        /// </summary> 
        [XmlElement("Colonia")]
        public String Colonia
        {
            get {
                    return colonia; 
            }
            set {
                    colonia = value; 
            }
        }

        /// <summary>
        /// Cp
        /// </summary> 
        [XmlElement("Cp")]
        public String Cp
        {
            get {
                    return cp; 
            }
            set {
                    cp = value; 
            }
        }

        /// <summary>
        /// Telefono
        /// </summary> 
        [XmlElement("Telefono")]
        public String Telefono
        {
            get {
                    return telefono; 
            }
            set {
                    telefono = value; 
            }
        }

        /// <summary>
        /// Telexten
        /// </summary> 
        [XmlElement("Telexten")]
        public String Telexten
        {
            get {
                    return telexten; 
            }
            set {
                    telexten = value; 
            }
        }

        /// <summary>
        /// Fax
        /// </summary> 
        [XmlElement("Fax")]
        public String Fax
        {
            get {
                    return fax; 
            }
            set {
                    fax = value; 
            }
        }

        /// <summary>
        /// Faxexten
        /// </summary> 
        [XmlElement("Faxexten")]
        public String Faxexten
        {
            get {
                    return faxexten; 
            }
            set {
                    faxexten = value; 
            }
        }

        /// <summary>
        /// Ageb
        /// </summary> 
        [XmlElement("Ageb")]
        public String Ageb
        {
            get {
                    return ageb; 
            }
            set {
                    ageb = value; 
            }
        }

        /// <summary>
        /// Clavecart
        /// </summary> 
        [XmlElement("Clavecart")]
        public String Clavecart
        {
            get {
                    return clavecart; 
            }
            set {
                    clavecart = value; 
            }
        }

        /// <summary>
        /// Longitud
        /// </summary> 
        [XmlElement("Longitud")]
        public Int32 Longitud
        {
            get {
                    return longitud; 
            }
            set {
                    longitud = value; 
            }
        }

        /// <summary>
        /// Latitud
        /// </summary> 
        [XmlElement("Latitud")]
        public Int32 Latitud
        {
            get {
                    return latitud; 
            }
            set {
                    latitud = value; 
            }
        }

        /// <summary>
        /// Altitud
        /// </summary> 
        [XmlElement("Altitud")]
        public Int32 Altitud
        {
            get {
                    return altitud; 
            }
            set {
                    altitud = value; 
            }
        }

        /// <summary>
        /// PuntocardinalId
        /// </summary> 
        [XmlElement("PuntocardinalId")]
        public Int16 PuntocardinalId
        {
            get {
                    return puntocardinalId; 
            }
            set {
                    puntocardinalId = value; 
            }
        }

        /// <summary>
        /// Director
        /// </summary> 
        [XmlElement("Director")]
        public String Director
        {
            get {
                    return director; 
            }
            set {
                    director = value; 
            }
        }

        /// <summary>
        /// Cartatopografica
        /// </summary> 
        [XmlElement("Cartatopografica")]
        public String Cartatopografica
        {
            get {
                    return cartatopografica; 
            }
            set {
                    cartatopografica = value; 
            }
        }

        /// <summary>
        /// NumeroIncorporacion
        /// </summary> 
        [XmlElement("NumeroIncorporacion")]
        public String NumeroIncorporacion
        {
            get {
                    return numeroIncorporacion; 
            }
            set {
                    numeroIncorporacion = value; 
            }
        }

        /// <summary>
        /// Folio
        /// </summary> 
        [XmlElement("Folio")]
        public String Folio
        {
            get {
                    return folio; 
            }
            set {
                    folio = value; 
            }
        }

        /// <summary>
        /// DependenciaoperativaId
        /// </summary> 
        [XmlElement("DependenciaoperativaId")]
        public Int16 DependenciaoperativaId
        {
            get {
                    return dependenciaoperativaId; 
            }
            set {
                    dependenciaoperativaId = value; 
            }
        }

        /// <summary>
        /// Observaciones
        /// </summary> 
        [XmlElement("Observaciones")]
        public String Observaciones
        {
            get {
                    return observaciones; 
            }
            set {
                    observaciones = value; 
            }
        }

        /// <summary>
        /// Fechasol
        /// </summary> 
        [XmlElement("Fechasol")]
        public Int16 Fechasol
        {
            get {
                    return fechasol; 
            }
            set {
                    fechasol = value; 
            }
        }

        /// <summary>
        /// ClaveinstitucionalId
        /// </summary> 
        [XmlElement("ClaveinstitucionalId")]
        public Int16 ClaveinstitucionalId
        {
            get {
                    return claveinstitucionalId; 
            }
            set {
                    claveinstitucionalId = value; 
            }
        }

        /// <summary>
        /// NiveleducacionId
        /// </summary> 
        [XmlElement("NiveleducacionId")]
        public Byte NiveleducacionId
        {
            get {
                    return niveleducacionId; 
            }
            set {
                    niveleducacionId = value; 
            }
        }

        /// <summary>
        /// ClaveagrupadorId
        /// </summary> 
        [XmlElement("ClaveagrupadorId")]
        public Byte ClaveagrupadorId
        {
            get {
                    return claveagrupadorId; 
            }
            set {
                    claveagrupadorId = value; 
            }
        }

        /// <summary>
        /// TurnoId
        /// </summary> 
        [XmlElement("TurnoId")]
        public Int16 TurnoId
        {
            get {
                    return turnoId; 
            }
            set {
                    turnoId = value; 
            }
        }

        /// <summary>
        /// Mark
        /// </summary> 
        [XmlElement("Mark")]
        public Int32 Mark
        {
            get {
                    return mark; 
            }
            set {
                    mark = value; 
            }
        }

        /// <summary>
        /// ValidDatos
        /// </summary> 
        [XmlElement("ValidDatos")]
        public Decimal ValidDatos
        {
            get {
                    return validDatos; 
            }
            set {
                    validDatos = value; 
            }
        }

        #endregion.
    }
}
