using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CentroTrabajoQryMD
    {
        private CentroTrabajoQryDP centroTrabajoQry = null;

        public CentroTrabajoQryMD(CentroTrabajoQryDP centroTrabajoQry)
        {
            this.centroTrabajoQry = centroTrabajoQry;
        }

        protected static CentroTrabajoQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CentroTrabajoQryDP centroTrabajoQry = new CentroTrabajoQryDP();
            centroTrabajoQry.CentrotrabajoId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            centroTrabajoQry.PaisId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            centroTrabajoQry.EntidadId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            centroTrabajoQry.RegionId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            centroTrabajoQry.ZonaId = dr.IsDBNull(4) ? (short)0 : dr.GetInt16(4);
            centroTrabajoQry.SostenimientoId = dr.IsDBNull(5) ? (Byte)0 : dr.GetByte(5);
            centroTrabajoQry.Clave = dr.IsDBNull(6) ? "" : dr.GetString(6);
            centroTrabajoQry.Turno3d = dr.IsDBNull(7) ? "" : dr.GetString(7);
            centroTrabajoQry.Bitprovisional = dr.IsDBNull(8) ? false : dr.GetBoolean(8);
            centroTrabajoQry.InmuebleId = dr.IsDBNull(9) ? 0 : dr.GetInt32(9);
            centroTrabajoQry.DomicilioId = dr.IsDBNull(10) ? 0 : dr.GetInt32(10);
            centroTrabajoQry.FechaFundacion = dr.IsDBNull(11) ? "" : dr.GetDateTime(11).ToShortDateString();;
            centroTrabajoQry.FechaAlta = dr.IsDBNull(12) ? "" : dr.GetDateTime(12).ToShortDateString();;
            centroTrabajoQry.FechaClausura = dr.IsDBNull(13) ? "" : dr.GetDateTime(13).ToShortDateString();;
            centroTrabajoQry.MotivobajacentrotrabajoId = dr.IsDBNull(14) ? (short)0 : dr.GetInt16(14);
            centroTrabajoQry.FechaReapertura = dr.IsDBNull(15) ? "" : dr.GetDateTime(15).ToShortDateString();;
            centroTrabajoQry.UsuarioId = dr.IsDBNull(16) ? false : dr.GetBoolean(16);
            centroTrabajoQry.FechaActualizacion = dr.IsDBNull(17) ? "" : dr.GetDateTime(17).ToShortDateString();;
            centroTrabajoQry.FechaCambio = dr.IsDBNull(18) ? "" : dr.GetDateTime(18).ToShortDateString();;
            centroTrabajoQry.Nombre = dr.IsDBNull(19) ? "" : dr.GetString(19);
            centroTrabajoQry.TipoctId = dr.IsDBNull(20) ? "" : dr.GetString(20);
            centroTrabajoQry.TipoeducacionId = dr.IsDBNull(21) ? (short)0 : dr.GetInt16(21);
            centroTrabajoQry.NivelId = dr.IsDBNull(22) ? (short)0 : dr.GetInt16(22);
            centroTrabajoQry.SubnivelId = dr.IsDBNull(23) ? (short)0 : dr.GetInt16(23);
            centroTrabajoQry.ControlId = dr.IsDBNull(24) ? (short)0 : dr.GetInt16(24);
            centroTrabajoQry.SubcontrolId = dr.IsDBNull(25) ? (short)0 : dr.GetInt16(25);
            centroTrabajoQry.DependencianormativaId = dr.IsDBNull(26) ? (short)0 : dr.GetInt16(26);
            centroTrabajoQry.DependenciaadministrativaId = dr.IsDBNull(27) ? (short)0 : dr.GetInt16(27);
            centroTrabajoQry.ServicioId = dr.IsDBNull(28) ? (short)0 : dr.GetInt16(28);
            centroTrabajoQry.SectorId = dr.IsDBNull(29) ? (short)0 : dr.GetInt16(29);
            centroTrabajoQry.EducacionfisicaId = dr.IsDBNull(30) ? (short)0 : dr.GetInt16(30);
            centroTrabajoQry.EstatusId = dr.IsDBNull(31) ? (short)0 : dr.GetInt16(31);
            centroTrabajoQry.AlmacenId = dr.IsDBNull(32) ? (short)0 : dr.GetInt16(32);
            centroTrabajoQry.Domicilio = dr.IsDBNull(33) ? "" : dr.GetString(33);
            centroTrabajoQry.Entrecalle = dr.IsDBNull(34) ? "" : dr.GetString(34);
            centroTrabajoQry.Ycalle = dr.IsDBNull(35) ? "" : dr.GetString(35);
            centroTrabajoQry.Municipio = dr.IsDBNull(36) ? "" : dr.GetString(36);
            centroTrabajoQry.Localidad = dr.IsDBNull(37) ? "" : dr.GetString(37);
            centroTrabajoQry.Colonia = dr.IsDBNull(38) ? "" : dr.GetString(38);
            centroTrabajoQry.Cp = dr.IsDBNull(39) ? "" : dr.GetString(39);
            centroTrabajoQry.Telefono = dr.IsDBNull(40) ? "" : dr.GetString(40);
            centroTrabajoQry.Telexten = dr.IsDBNull(41) ? "" : dr.GetString(41);
            centroTrabajoQry.Fax = dr.IsDBNull(42) ? "" : dr.GetString(42);
            centroTrabajoQry.Faxexten = dr.IsDBNull(43) ? "" : dr.GetString(43);
            centroTrabajoQry.Ageb = dr.IsDBNull(44) ? "" : dr.GetString(44);
            centroTrabajoQry.Clavecart = dr.IsDBNull(45) ? "" : dr.GetString(45);
            centroTrabajoQry.Longitud = dr.IsDBNull(46) ? 0 : dr.GetInt32(46);
            centroTrabajoQry.Latitud = dr.IsDBNull(47) ? 0 : dr.GetInt32(47);
            centroTrabajoQry.Altitud = dr.IsDBNull(48) ? 0 : dr.GetInt32(48);
            centroTrabajoQry.PuntocardinalId = dr.IsDBNull(49) ? (short)0 : dr.GetInt16(49);
            centroTrabajoQry.Director = dr.IsDBNull(50) ? "" : dr.GetString(50);
            centroTrabajoQry.Cartatopografica = dr.IsDBNull(51) ? "" : dr.GetString(51);
            centroTrabajoQry.NumeroIncorporacion = dr.IsDBNull(52) ? "" : dr.GetString(52);
            centroTrabajoQry.Folio = dr.IsDBNull(53) ? "" : dr.GetString(53);
            centroTrabajoQry.DependenciaoperativaId = dr.IsDBNull(54) ? (short)0 : dr.GetInt16(54);
            centroTrabajoQry.Observaciones = dr.IsDBNull(55) ? "" : dr.GetString(55);
            centroTrabajoQry.Fechasol = dr.IsDBNull(56) ? (short)0 : dr.GetInt16(56);
            centroTrabajoQry.ClaveinstitucionalId = dr.IsDBNull(57) ? (short)0 : dr.GetInt16(57);
            centroTrabajoQry.NiveleducacionId = dr.IsDBNull(58) ? (Byte)0 : dr.GetByte(58);
            centroTrabajoQry.ClaveagrupadorId = dr.IsDBNull(59) ? (Byte)0 : dr.GetByte(59);
            centroTrabajoQry.TurnoId = dr.IsDBNull(60) ? (short)0 : dr.GetInt16(60);
            centroTrabajoQry.Mark = dr.IsDBNull(61) ? 0 : dr.GetInt32(61);
            centroTrabajoQry.ValidDatos = dr.IsDBNull(62) ? 0 : dr.GetDecimal(62);
            return centroTrabajoQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CentroTrabajoQry
            DbCommand com = con.CreateCommand();
            String countCentroTrabajoQry = String.Format(CultureInfo.CurrentCulture,CentroTrabajo.CentroTrabajoQry.CentroTrabajoQryCount,"");
            com.CommandText = countCentroTrabajoQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CentroTrabajoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CentroTrabajoQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CentroTrabajoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCentroTrabajoQry = String.Format(CultureInfo.CurrentCulture, CentroTrabajo.CentroTrabajoQry.CentroTrabajoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCentroTrabajoQry = listCentroTrabajoQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCentroTrabajoQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CentroTrabajoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CentroTrabajoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CentroTrabajoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CentroTrabajoQryDP[])lista.ToArray(typeof(CentroTrabajoQryDP));
        }
        public static Int64 CountForDescripcionIdZonayIdSostenimiento(DbConnection con, DbTransaction tran, String aNombre, Int16 aIdZona, Byte aIdSostenimiento)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcionIdZonayIdSostenimiento CentroTrabajoQry
            DbCommand com = con.CreateCommand();
            String countCentroTrabajoQryForDescripcionIdZonayIdSostenimiento = String.Format(CultureInfo.CurrentCulture,CentroTrabajo.CentroTrabajoQry.CentroTrabajoQryCount,"");
            countCentroTrabajoQryForDescripcionIdZonayIdSostenimiento += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countCentroTrabajoQryForDescripcionIdZonayIdSostenimiento += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countCentroTrabajoQryForDescripcionIdZonayIdSostenimiento += String.Format("    {1} Id_Zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countCentroTrabajoQryForDescripcionIdZonayIdSostenimiento += String.Format("    {1} Id_Sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countCentroTrabajoQryForDescripcionIdZonayIdSostenimiento;
            #endregion
            #region Parametros countCentroTrabajoQryForDescripcionIdZonayIdSostenimiento
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcionIdZonayIdSostenimiento CentroTrabajoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CentroTrabajoQryDP[] ListForDescripcionIdZonayIdSostenimiento(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdZona, Byte aIdSostenimiento)
        {
            #region SQL List CentroTrabajoQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCentroTrabajoQryForDescripcionIdZonayIdSostenimiento = String.Format(CultureInfo.CurrentCulture, CentroTrabajo.CentroTrabajoQry.CentroTrabajoQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCentroTrabajoQryForDescripcionIdZonayIdSostenimiento = listCentroTrabajoQryForDescripcionIdZonayIdSostenimiento.Substring(6);
            listCentroTrabajoQryForDescripcionIdZonayIdSostenimiento += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listCentroTrabajoQryForDescripcionIdZonayIdSostenimiento += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listCentroTrabajoQryForDescripcionIdZonayIdSostenimiento += String.Format("    {1} Id_Zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listCentroTrabajoQryForDescripcionIdZonayIdSostenimiento += String.Format("    {1} Id_Sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCentroTrabajoQryForDescripcionIdZonayIdSostenimiento, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CentroTrabajoQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CentroTrabajoQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CentroTrabajoQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CentroTrabajoQryDP[])lista.ToArray(typeof(CentroTrabajoQryDP));
        }
    }
}
