using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CentroTrabajoQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CentroTrabajoQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CentroTrabajoQry!", ex);
            }
            return resultado;
        }

        public static CentroTrabajoQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CentroTrabajoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CentroTrabajoQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CentroTrabajoQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcionIdZonayIdSostenimiento(DbConnection con, String aNombre, Int16 aIdZona, Byte aIdSostenimiento) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CentroTrabajoQryMD.CountForDescripcionIdZonayIdSostenimiento(con, tran, aNombre, aIdZona, aIdSostenimiento);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CentroTrabajoQry!", ex);
            }
            return resultado;
        }

        public static CentroTrabajoQryDP[] LoadListForDescripcionIdZonayIdSostenimiento(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdZona, Byte aIdSostenimiento) 
        {
            CentroTrabajoQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CentroTrabajoQryMD.ListForDescripcionIdZonayIdSostenimiento(con, tran, startRowIndex, maximumRows, aNombre, aIdZona, aIdSostenimiento);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CentroTrabajoQry!", ex);
            }
            return resultado;

        }
    }
}
