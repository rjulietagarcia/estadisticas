using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class SubnivelEducacionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SubnivelEducacionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un SubnivelEducacionQry!", ex);
            }
            return resultado;
        }

        public static SubnivelEducacionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SubnivelEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SubnivelEducacionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de SubnivelEducacionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuario(DbConnection con, Int32 aUsuIdUsuario, Int16 aIdNivel) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SubnivelEducacionQryMD.CountForUsuario(con, tran, aUsuIdUsuario, aIdNivel);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un SubnivelEducacionQry!", ex);
            }
            return resultado;
        }

        public static SubnivelEducacionQryDP[] LoadListForUsuario(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Int16 aIdNivel) 
        {
            SubnivelEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SubnivelEducacionQryMD.ListForUsuario(con, tran, startRowIndex, maximumRows, aUsuIdUsuario, aIdNivel);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de SubnivelEducacionQry!", ex);
            }
            return resultado;

        }
    }
}
