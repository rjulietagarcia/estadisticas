using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class SubnivelEducacionQryMD
    {
        private SubnivelEducacionQryDP subnivelEducacionQry = null;

        public SubnivelEducacionQryMD(SubnivelEducacionQryDP subnivelEducacionQry)
        {
            this.subnivelEducacionQry = subnivelEducacionQry;
        }

        protected static SubnivelEducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SubnivelEducacionQryDP subnivelEducacionQry = new SubnivelEducacionQryDP();
            subnivelEducacionQry.TipoeducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            subnivelEducacionQry.NivelId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            subnivelEducacionQry.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            subnivelEducacionQry.LetraFolio = dr.IsDBNull(3) ? "" : dr.GetString(3);
            subnivelEducacionQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            subnivelEducacionQry.UsuarioId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            subnivelEducacionQry.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            subnivelEducacionQry.UsuIdUsuario = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            subnivelEducacionQry.SubnivelId = dr.IsDBNull(8) ? (short)0 : dr.GetInt16(8);
            subnivelEducacionQry.Nombresubnivel = dr.IsDBNull(9) ? "" : dr.GetString(9);
            return subnivelEducacionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count SubnivelEducacionQry
            DbCommand com = con.CreateCommand();
            String countSubnivelEducacionQry = String.Format(CultureInfo.CurrentCulture,SubnivelEducacion.SubnivelEducacionQry.SubnivelEducacionQryCount,"");
            com.CommandText = countSubnivelEducacionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count SubnivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SubnivelEducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List SubnivelEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSubnivelEducacionQry = String.Format(CultureInfo.CurrentCulture, SubnivelEducacion.SubnivelEducacionQry.SubnivelEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listSubnivelEducacionQry = listSubnivelEducacionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSubnivelEducacionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load SubnivelEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SubnivelEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SubnivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SubnivelEducacionQryDP[])lista.ToArray(typeof(SubnivelEducacionQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario, Int16 aIdNivel)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario SubnivelEducacionQry
            DbCommand com = con.CreateCommand();
            String countSubnivelEducacionQryForUsuario = String.Format(CultureInfo.CurrentCulture,SubnivelEducacion.SubnivelEducacionQry.SubnivelEducacionQryCount,"");
            countSubnivelEducacionQryForUsuario += " AND \n";
            String delimitador = "";
            countSubnivelEducacionQryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdNivel != -1)
            {
                countSubnivelEducacionQryForUsuario += String.Format("    {1} pen.id_nivel = {0}IdNivel\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countSubnivelEducacionQryForUsuario;
            #endregion
            #region Parametros countSubnivelEducacionQryForUsuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdNivel != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdNivel);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario SubnivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SubnivelEducacionQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Int16 aIdNivel)
        {
            #region SQL List SubnivelEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSubnivelEducacionQryForUsuario = String.Format(CultureInfo.CurrentCulture, SubnivelEducacion.SubnivelEducacionQry.SubnivelEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listSubnivelEducacionQryForUsuario = listSubnivelEducacionQryForUsuario.Substring(6);
            listSubnivelEducacionQryForUsuario += " AND \n";
            String delimitador = "";
            listSubnivelEducacionQryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdNivel != -1)
            {
                listSubnivelEducacionQryForUsuario += String.Format("    {1} pen.id_nivel = {0}IdNivel\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listSubnivelEducacionQryForUsuario, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );


            //Pendiente de quitar columna id_sistema!!!!!
            com.CommandText = listSubnivelEducacionQryForUsuario;
            DbDataReader dr;
            #endregion
            #region Parametros Load SubnivelEducacionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdNivel != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdNivel);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SubnivelEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SubnivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SubnivelEducacionQryDP[])lista.ToArray(typeof(SubnivelEducacionQryDP));
        }
    }
}
