using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class FltOpcionQryMD
    {
        private FltOpcionQryDP fltOpcionQry = null;

        public FltOpcionQryMD(FltOpcionQryDP fltOpcionQry)
        {
            this.fltOpcionQry = fltOpcionQry;
        }

        protected static FltOpcionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            FltOpcionQryDP fltOpcionQry = new FltOpcionQryDP();
            fltOpcionQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            fltOpcionQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            fltOpcionQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            fltOpcionQry.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            fltOpcionQry.Abreviatura = dr.IsDBNull(4) ? "" : dr.GetString(4);
            fltOpcionQry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            fltOpcionQry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            fltOpcionQry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            fltOpcionQry.FechaInicio = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            fltOpcionQry.FechaFin = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            fltOpcionQry.CarpetaOpcion = dr.IsDBNull(10) ? "" : dr.GetString(10);
            return fltOpcionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count FltOpcionQry
            DbCommand com = con.CreateCommand();
            String countFltOpcionQry = String.Format(CultureInfo.CurrentCulture,FltOpcion.FltOpcionQry.FltOpcionQryCount,"");
            com.CommandText = countFltOpcionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count FltOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltOpcionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List FltOpcionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltOpcionQry = String.Format(CultureInfo.CurrentCulture, FltOpcion.FltOpcionQry.FltOpcionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltOpcionQry = listFltOpcionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltOpcionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltOpcionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltOpcionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltOpcionQryDP[])lista.ToArray(typeof(FltOpcionQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave FltOpcionQry
            DbCommand com = con.CreateCommand();
            String countFltOpcionQryForLlave = String.Format(CultureInfo.CurrentCulture,FltOpcion.FltOpcionQry.FltOpcionQryCount,"");
            countFltOpcionQryForLlave += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countFltOpcionQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countFltOpcionQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countFltOpcionQryForLlave += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countFltOpcionQryForLlave;
            #endregion
            #region Parametros countFltOpcionQryForLlave
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave FltOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltOpcionQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion)
        {
            #region SQL List FltOpcionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltOpcionQryForLlave = String.Format(CultureInfo.CurrentCulture, FltOpcion.FltOpcionQry.FltOpcionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltOpcionQryForLlave = listFltOpcionQryForLlave.Substring(6);
            listFltOpcionQryForLlave += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listFltOpcionQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listFltOpcionQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listFltOpcionQryForLlave += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltOpcionQryForLlave, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltOpcionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltOpcionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltOpcionQryDP[])lista.ToArray(typeof(FltOpcionQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion FltOpcionQry
            DbCommand com = con.CreateCommand();
            String countFltOpcionQryForDescripcion = String.Format(CultureInfo.CurrentCulture,FltOpcion.FltOpcionQry.FltOpcionQryCount,"");
            countFltOpcionQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countFltOpcionQryForDescripcion += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countFltOpcionQryForDescripcion += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre != "")
            {
                countFltOpcionQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countFltOpcionQryForDescripcion;
            #endregion
            #region Parametros countFltOpcionQryForDescripcion
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion FltOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltOpcionQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, String aNombre)
        {
            #region SQL List FltOpcionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltOpcionQryForDescripcion = String.Format(CultureInfo.CurrentCulture, FltOpcion.FltOpcionQry.FltOpcionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltOpcionQryForDescripcion = listFltOpcionQryForDescripcion.Substring(6);
            listFltOpcionQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listFltOpcionQryForDescripcion += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listFltOpcionQryForDescripcion += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre != "")
            {
                listFltOpcionQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltOpcionQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltOpcionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltOpcionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltOpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltOpcionQryDP[])lista.ToArray(typeof(FltOpcionQryDP));
        }
    }
}
