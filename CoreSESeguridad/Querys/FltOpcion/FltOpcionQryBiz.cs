using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class FltOpcionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltOpcionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltOpcionQry!", ex);
            }
            return resultado;
        }

        public static FltOpcionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            FltOpcionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltOpcionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltOpcionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLlave(DbConnection con, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltOpcionQryMD.CountForLlave(con, tran, aIdSistema, aIdModulo, aIdOpcion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltOpcionQry!", ex);
            }
            return resultado;
        }

        public static FltOpcionQryDP[] LoadListForLlave(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion) 
        {
            FltOpcionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltOpcionQryMD.ListForLlave(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltOpcionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, Byte aIdSistema, Byte aIdModulo, String aNombre) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltOpcionQryMD.CountForDescripcion(con, tran, aIdSistema, aIdModulo, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltOpcionQry!", ex);
            }
            return resultado;
        }

        public static FltOpcionQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, String aNombre) 
        {
            FltOpcionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltOpcionQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltOpcionQry!", ex);
            }
            return resultado;

        }
    }
}
