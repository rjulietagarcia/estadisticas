using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CcntFiltrosQryMD
    {
        private CcntFiltrosQryDP ccntFiltrosQry = null;

        public CcntFiltrosQryMD(CcntFiltrosQryDP ccntFiltrosQry)
        {
            this.ccntFiltrosQry = ccntFiltrosQry;
        }

        protected static CcntFiltrosQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CcntFiltrosQryDP ccntFiltrosQry = new CcntFiltrosQryDP();
            ccntFiltrosQry.CctntId              = dr.IsDBNull(0) ? 0 : int.Parse(dr.GetValue(0).ToString());
            ccntFiltrosQry.NiveleducacionId     = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            ccntFiltrosQry.Niveleducacion       = dr.IsDBNull(2) ? "" : dr.GetString(2);
            ccntFiltrosQry.TurnoId              = dr.IsDBNull(3) ? 0 : int.Parse(dr.GetValue(3).ToString());
            ccntFiltrosQry.Truno                = dr.IsDBNull(4) ? "" : dr.GetString(4);
            ccntFiltrosQry.CentrotrabajoId      = dr.IsDBNull(5) ? 0 : int.Parse(dr.GetValue(5).ToString());
            ccntFiltrosQry.Nombrecct            = dr.IsDBNull(6) ? "" : dr.GetString(6);
            ccntFiltrosQry.Clavecct             = dr.IsDBNull(7) ? "" : dr.GetString(7);
            ccntFiltrosQry.SostenimientoId      = dr.IsDBNull(8) ? 0 : int.Parse(dr.GetValue(8).ToString());
            ccntFiltrosQry.Sostenimiento        = dr.IsDBNull(9) ? "" : dr.GetString(9);
            ccntFiltrosQry.SubnivelId           = dr.IsDBNull(10) ? 0 : int.Parse(dr.GetValue(10).ToString());
            ccntFiltrosQry.Subnivel             = dr.IsDBNull(11) ? "" : dr.GetString(11);
            ccntFiltrosQry.TipoeducacionId      = dr.IsDBNull(12) ? 0 : int.Parse(dr.GetValue(12).ToString());
            ccntFiltrosQry.Tipoeducacion        = dr.IsDBNull(13) ? "" : dr.GetString(13);
            ccntFiltrosQry.NivelId              = dr.IsDBNull(14) ? 0 : int.Parse(dr.GetValue(14).ToString());
            ccntFiltrosQry.Nivel                = dr.IsDBNull(15) ? "" : dr.GetString(15);
            ccntFiltrosQry.RegionId             = dr.IsDBNull(16) ? 0 : int.Parse(dr.GetValue(16).ToString());
            ccntFiltrosQry.Region               = dr.IsDBNull(17) ? "" : dr.GetString(17);
            ccntFiltrosQry.ZonaId               = dr.IsDBNull(18) ? 0 : int.Parse(dr.GetValue(18).ToString());
            ccntFiltrosQry.Numerozona           = dr.IsDBNull(19) ? 0 : int.Parse(dr.GetValue(19).ToString());
            ccntFiltrosQry.InmuebleID           = dr.IsDBNull(20) ? 0 : dr.GetInt32(20);
            ccntFiltrosQry.Id_entidad           = dr.IsDBNull(21) ? 0 : dr.GetInt32(21);
            return ccntFiltrosQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CcntFiltrosQry
            DbCommand com = con.CreateCommand();
            String countCcntFiltrosQry = String.Format(CultureInfo.CurrentCulture,CcntFiltros.CcntFiltrosQry.CcntFiltrosQryCount,"");
            com.CommandText = countCcntFiltrosQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CcntFiltrosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CcntFiltrosQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CcntFiltrosQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCcntFiltrosQry = String.Format(CultureInfo.CurrentCulture, CcntFiltros.CcntFiltrosQry.CcntFiltrosQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCcntFiltrosQry = listCcntFiltrosQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCcntFiltrosQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CcntFiltrosQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CcntFiltrosQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CcntFiltrosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CcntFiltrosQryDP[])lista.ToArray(typeof(CcntFiltrosQryDP));
        }
        public static Int64 CountForFiltros(DbConnection con, DbTransaction tran, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, Int16 aIdCentrotrabajo)
        {
            Int64 resultado = 0;
            #region SQL CountForFiltros CcntFiltrosQry
            DbCommand com = con.CreateCommand();
            String countCcntFiltrosQryForFiltros = String.Format(CultureInfo.CurrentCulture,CcntFiltros.CcntFiltrosQry.CcntFiltrosQryCount,"");
            String delimitador = " AND ";
            if (aIdNiveleducacion != 0)
            {
                countCcntFiltrosQryForFiltros += String.Format("    {1}  ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != -1)
            {
                countCcntFiltrosQryForFiltros += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                countCcntFiltrosQryForFiltros += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                countCcntFiltrosQryForFiltros += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdCentrotrabajo != -1)
            {
                countCcntFiltrosQryForFiltros += String.Format("    {1} ctnt.id_cctnt = {0}IdCentrotrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCcntFiltrosQryForFiltros;
            #endregion
            #region Parametros countCcntFiltrosQryForFiltros
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            if (aIdRegion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            }
            if (aIdSostenimiento != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            }
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            if (aIdCentrotrabajo != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCentrotrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCentrotrabajo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForFiltros CcntFiltrosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CcntFiltrosQryDP[] ListForFiltros(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, int aIdCentrotrabajo)
        {
            #region SQL List CcntFiltrosQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCcntFiltrosQryForFiltros = String.Format(CultureInfo.CurrentCulture, CcntFiltros.CcntFiltrosQry.CcntFiltrosQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCcntFiltrosQryForFiltros = listCcntFiltrosQryForFiltros.Substring(6);
            String delimitador = " AND ";
            if (aIdNiveleducacion != 0)
            {
                listCcntFiltrosQryForFiltros += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != -1)
            {
                listCcntFiltrosQryForFiltros += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                listCcntFiltrosQryForFiltros += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                listCcntFiltrosQryForFiltros += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdCentrotrabajo != -1)
            {
                /*LE MODIFIQUE EL CTNT.ID_CENTROTRABAJO POR CCTNT.ID_CCTNT PARA ADECUAR EL QUERY A LA BUSQUEDA NECESARIA AL CARGAR
                 EL CENTRO DE TRABAJO EN EL USR*/
                listCcntFiltrosQryForFiltros += String.Format("    {1} ctnt.id_cctnt = {0}IdCentrotrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCcntFiltrosQryForFiltros, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CcntFiltrosQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            if (aIdRegion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            }
            if (aIdSostenimiento != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            }
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            if (aIdCentrotrabajo != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCentrotrabajo",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdCentrotrabajo);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CcntFiltrosQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CcntFiltrosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CcntFiltrosQryDP[])lista.ToArray(typeof(CcntFiltrosQryDP));
        }
        public static Int64 CountForClave(DbConnection con, DbTransaction tran, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, Int16 aIdCentrotrabajo, String aClavecct)
        {
            Int64 resultado = 0;
            #region SQL CountForClave CcntFiltrosQry
            DbCommand com = con.CreateCommand();
            String countCcntFiltrosQryForClave = String.Format(CultureInfo.CurrentCulture,CcntFiltros.CcntFiltrosQry.CcntFiltrosQryCount,"");
            countCcntFiltrosQryForClave += " AND \n";
            String delimitador = "";
            if (aIdNiveleducacion != 0)
            {
                countCcntFiltrosQryForClave += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != -1)
            {
                countCcntFiltrosQryForClave += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                countCcntFiltrosQryForClave += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                countCcntFiltrosQryForClave += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aClavecct != "")
            {
                countCcntFiltrosQryForClave += String.Format("    {1} ct.clave LIKE {0}Clavecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCcntFiltrosQryForClave;
            #endregion
            #region Parametros countCcntFiltrosQryForClave
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            if (aIdRegion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            }
            if (aIdSostenimiento != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            }
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            if (aClavecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clavecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 10, ParameterDirection.Input, aClavecct);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForClave CcntFiltrosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CcntFiltrosQryDP[] ListForClave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, Int16 aIdCentrotrabajo, String aClavecct)
        {
            #region SQL List CcntFiltrosQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCcntFiltrosQryForClave = String.Format(CultureInfo.CurrentCulture, CcntFiltros.CcntFiltrosQry.CcntFiltrosQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCcntFiltrosQryForClave = listCcntFiltrosQryForClave.Substring(6);
            listCcntFiltrosQryForClave += " AND \n";
            String delimitador = "";
            if (aIdNiveleducacion != 0)
            {
                listCcntFiltrosQryForClave += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != -1)
            {
                listCcntFiltrosQryForClave += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                listCcntFiltrosQryForClave += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                listCcntFiltrosQryForClave += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aClavecct != "")
            {
                listCcntFiltrosQryForClave += String.Format("    {1} ct.clave LIKE {0}Clavecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCcntFiltrosQryForClave, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CcntFiltrosQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            if (aIdRegion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            }
            if (aIdSostenimiento != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            }
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            if (aClavecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clavecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 10, ParameterDirection.Input, aClavecct);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CcntFiltrosQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CcntFiltrosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CcntFiltrosQryDP[])lista.ToArray(typeof(CcntFiltrosQryDP));
        }
        public static Int64 CountForNombre(DbConnection con, DbTransaction tran, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, String aNombrecct)
        {
            Int64 resultado = 0;
            #region SQL CountForNombre CcntFiltrosQry
            DbCommand com = con.CreateCommand();
            String countCcntFiltrosQryForNombre = String.Format(CultureInfo.CurrentCulture,CcntFiltros.CcntFiltrosQry.CcntFiltrosQryCount,"");
            String delimitador = " AND ";
            if (aIdNiveleducacion != 0)
            {
                countCcntFiltrosQryForNombre += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != -1)
            {
                countCcntFiltrosQryForNombre += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                countCcntFiltrosQryForNombre += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                countCcntFiltrosQryForNombre += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombrecct != "")
            {
                countCcntFiltrosQryForNombre += String.Format("    {1} ct.Nombre LIKE {0}Nombrecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCcntFiltrosQryForNombre;
            #endregion
            #region Parametros countCcntFiltrosQryForNombre
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            if (aIdRegion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            }
            if (aIdSostenimiento != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            }
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            if (aNombrecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombrecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombrecct);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForNombre CcntFiltrosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CcntFiltrosQryDP[] ListForNombre(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, String aNombrecct)
        {
            #region SQL List CcntFiltrosQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCcntFiltrosQryForNombre = String.Format(CultureInfo.CurrentCulture, CcntFiltros.CcntFiltrosQry.CcntFiltrosQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCcntFiltrosQryForNombre = listCcntFiltrosQryForNombre.Substring(6);
            String delimitador = " AND ";
            if (aIdNiveleducacion != 0)
            {
                listCcntFiltrosQryForNombre += String.Format("    {1} ctnt.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != -1)
            {
                listCcntFiltrosQryForNombre += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                listCcntFiltrosQryForNombre += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                listCcntFiltrosQryForNombre += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombrecct != "")
            {
                listCcntFiltrosQryForNombre += String.Format("    {1} ct.Nombre LIKE {0}Nombrecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCcntFiltrosQryForNombre, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CcntFiltrosQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            if (aIdRegion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            }
            if (aIdSostenimiento != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            }
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            if (aNombrecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombrecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombrecct);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CcntFiltrosQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CcntFiltrosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CcntFiltrosQryDP[])lista.ToArray(typeof(CcntFiltrosQryDP));
        }
    }
}
