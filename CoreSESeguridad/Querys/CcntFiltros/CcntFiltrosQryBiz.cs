using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CcntFiltrosQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CcntFiltrosQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CcntFiltrosQry!", ex);
            }
            return resultado;
        }

        public static CcntFiltrosQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CcntFiltrosQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CcntFiltrosQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CcntFiltrosQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForFiltros(DbConnection con, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, Int16 aIdCentrotrabajo) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CcntFiltrosQryMD.CountForFiltros(con, tran, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona, aIdCentrotrabajo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CcntFiltrosQry!", ex);
            }
            return resultado;
        }

        public static CcntFiltrosQryDP[] LoadListForFiltros(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, int aIdCentrotrabajo) 
        {
            CcntFiltrosQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CcntFiltrosQryMD.ListForFiltros(con, tran, startRowIndex, maximumRows, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona, aIdCentrotrabajo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CcntFiltrosQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForClave(DbConnection con, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, Int16 aIdCentrotrabajo, String aClavecct) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CcntFiltrosQryMD.CountForClave(con, tran, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona, aIdCentrotrabajo, aClavecct);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CcntFiltrosQry!", ex);
            }
            return resultado;
        }

        public static CcntFiltrosQryDP[] LoadListForClave(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, Int16 aIdCentrotrabajo, String aClavecct) 
        {
            CcntFiltrosQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CcntFiltrosQryMD.ListForClave(con, tran, startRowIndex, maximumRows, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona, aIdCentrotrabajo, aClavecct);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CcntFiltrosQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForNombre(DbConnection con, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, String aNombrecct) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CcntFiltrosQryMD.CountForNombre(con, tran, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona, aNombrecct);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CcntFiltrosQry!", ex);
            }
            return resultado;
        }

        public static CcntFiltrosQryDP[] LoadListForNombre(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, String aNombrecct) 
        {
            CcntFiltrosQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CcntFiltrosQryMD.ListForNombre(con, tran, startRowIndex, maximumRows, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona, aNombrecct);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CcntFiltrosQry!", ex);
            }
            return resultado;

        }
    }
}
