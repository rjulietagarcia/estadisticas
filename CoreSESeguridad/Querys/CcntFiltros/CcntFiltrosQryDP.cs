using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "CcntFiltrosQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 29 de junio de 2009.</Para>
    /// <Para>Hora: 03:26:00 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CcntFiltrosQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>CctntId</term><description>Descripcion CctntId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveleducacionId</term><description>Descripcion NiveleducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Niveleducacion</term><description>Descripcion Niveleducacion</description>
    ///    </item>
    ///    <item>
    ///        <term>TurnoId</term><description>Descripcion TurnoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Truno</term><description>Descripcion Truno</description>
    ///    </item>
    ///    <item>
    ///        <term>CentrotrabajoId</term><description>Descripcion CentrotrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombrecct</term><description>Descripcion Nombrecct</description>
    ///    </item>
    ///    <item>
    ///        <term>Clavecct</term><description>Descripcion Clavecct</description>
    ///    </item>
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Sostenimiento</term><description>Descripcion Sostenimiento</description>
    ///    </item>
    ///    <item>
    ///        <term>SubnivelId</term><description>Descripcion SubnivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>Subnivel</term><description>Descripcion Subnivel</description>
    ///    </item>
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Tipoeducacion</term><description>Descripcion Tipoeducacion</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nivel</term><description>Descripcion Nivel</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Region</term><description>Descripcion Region</description>
    ///    </item>
    ///    <item>
    ///        <term>ZonaId</term><description>Descripcion ZonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Numerozona</term><description>Descripcion Numerozona</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CcntFiltrosQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// CcntFiltrosQryDTO ccntfiltrosqry = new CcntFiltrosQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("CcntFiltrosQry")]
    public class CcntFiltrosQryDP
    {
        #region Definicion de campos privados.
        private int cctntId;
        private Byte niveleducacionId;
        private String niveleducacion;
        private int turnoId;
        private String truno;
        private int centrotrabajoId;
        private String nombrecct;
        private String clavecct;
        private int sostenimientoId;
        private String sostenimiento;
        private int subnivelId;
        private String subnivel;
        private int tipoeducacionId;
        private String tipoeducacion;
        private int nivelId;
        private String nivel;
        private int regionId;
        private String region;
        private int zonaId;
        private int numerozona;
        private Int32 inmuebleId;
        private int id_entidad;

        


        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// CctntId
        /// </summary> 
        [XmlElement("CctntId")]
        public int CctntId
        {
            get {
                    return cctntId; 
            }
            set {
                    cctntId = value; 
            }
        }
      
        /// <summary>
        /// NiveleducacionId
        /// </summary> 
        [XmlElement("NiveleducacionId")]
        public Byte NiveleducacionId
        {
            get {
                    return niveleducacionId; 
            }
            set {
                    niveleducacionId = value; 
            }
        }

        /// <summary>
        /// Niveleducacion
        /// </summary> 
        [XmlElement("Niveleducacion")]
        public String Niveleducacion
        {
            get {
                    return niveleducacion; 
            }
            set {
                    niveleducacion = value; 
            }
        }

        /// <summary>
        /// TurnoId
        /// </summary> 
        [XmlElement("TurnoId")]
        public int TurnoId
        {
            get {
                    return turnoId; 
            }
            set {
                    turnoId = value; 
            }
        }

        /// <summary>
        /// Truno
        /// </summary> 
        [XmlElement("Truno")]
        public String Truno
        {
            get {
                    return truno; 
            }
            set {
                    truno = value; 
            }
        }

        /// <summary>
        /// CentrotrabajoId
        /// </summary> 
        [XmlElement("CentrotrabajoId")]
        public int CentrotrabajoId
        {
            get {
                    return centrotrabajoId; 
            }
            set {
                    centrotrabajoId = value; 
            }
        }

        /// <summary>
        /// Nombrecct
        /// </summary> 
        [XmlElement("Nombrecct")]
        public String Nombrecct
        {
            get {
                    return nombrecct; 
            }
            set {
                    nombrecct = value; 
            }
        }

        /// <summary>
        /// Clavecct
        /// </summary> 
        [XmlElement("Clavecct")]
        public String Clavecct
        {
            get {
                    return clavecct; 
            }
            set {
                    clavecct = value; 
            }
        }

        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public int SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// Sostenimiento
        /// </summary> 
        [XmlElement("Sostenimiento")]
        public String Sostenimiento
        {
            get {
                    return sostenimiento; 
            }
            set {
                    sostenimiento = value; 
            }
        }

        /// <summary>
        /// SubnivelId
        /// </summary> 
        [XmlElement("SubnivelId")]
        public int SubnivelId
        {
            get {
                    return subnivelId; 
            }
            set {
                    subnivelId = value; 
            }
        }

        /// <summary>
        /// Subnivel
        /// </summary> 
        [XmlElement("Subnivel")]
        public String Subnivel
        {
            get {
                    return subnivel; 
            }
            set {
                    subnivel = value; 
            }
        }

        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public int TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        /// <summary>
        /// Tipoeducacion
        /// </summary> 
        [XmlElement("Tipoeducacion")]
        public String Tipoeducacion
        {
            get {
                    return tipoeducacion; 
            }
            set {
                    tipoeducacion = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public int NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        /// <summary>
        /// Nivel
        /// </summary> 
        [XmlElement("Nivel")]
        public String Nivel
        {
            get {
                    return nivel; 
            }
            set {
                    nivel = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public int RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        /// <summary>
        /// Region
        /// </summary> 
        [XmlElement("Region")]
        public String Region
        {
            get {
                    return region; 
            }
            set {
                    region = value; 
            }
        }

        /// <summary>
        /// ZonaId
        /// </summary> 
        [XmlElement("ZonaId")]
        public int ZonaId
        {
            get {
                    return zonaId; 
            }
            set {
                    zonaId = value; 
            }
        }

        /// <summary>
        /// Numerozona
        /// </summary> 
        [XmlElement("Numerozona")]
        public int Numerozona
        {
            get {
                    return numerozona; 
            }
            set {
                    numerozona = value; 
            }
        }
        /// <summary>
        /// InmuebleId
        /// </summary>
        [XmlElement("InmuebleId")]
        public int InmuebleID
        {
            get { return inmuebleId; }
            set { inmuebleId = value; }
        }
        /// <summary>
        /// Id_entidad
        /// </summary> 
        [XmlElement("Id_entidad")]
        public int Id_entidad
        {
            get { return id_entidad; }
            set { id_entidad = value; }
        }
        #endregion.
    }
}
