using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class TreeUsuarioQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreeUsuarioQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreeUsuarioQry!", ex);
            }
            return resultado;
        }

        public static TreeUsuarioQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TreeUsuarioQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreeUsuarioQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreeUsuarioQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuarioSistema(DbConnection con, Int32 aUsuIdUsuario, Byte aIdSistema) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreeUsuarioQryMD.CountForUsuarioSistema(con, tran, aUsuIdUsuario, aIdSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreeUsuarioQry!", ex);
            }
            return resultado;
        }

        public static TreeUsuarioQryDP[] LoadListForUsuarioSistema(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Byte aIdSistema) 
        {
            TreeUsuarioQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreeUsuarioQryMD.ListForUsuarioSistema(con, tran, startRowIndex, maximumRows, aUsuIdUsuario, aIdSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreeUsuarioQry!", ex);
            }
            return resultado;

        }
    }
}
