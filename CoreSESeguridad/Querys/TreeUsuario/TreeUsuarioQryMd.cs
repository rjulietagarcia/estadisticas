using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TreeUsuarioQryMD
    {
        private TreeUsuarioQryDP treeUsuarioQry = null;

        public TreeUsuarioQryMD(TreeUsuarioQryDP treeUsuarioQry)
        {
            this.treeUsuarioQry = treeUsuarioQry;
        }

        protected static TreeUsuarioQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TreeUsuarioQryDP treeUsuarioQry = new TreeUsuarioQryDP();
            treeUsuarioQry.Llave = dr.IsDBNull(0) ? "" : dr.GetString(0);
            treeUsuarioQry.UsuIdUsuario = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            treeUsuarioQry.SistemaId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            return treeUsuarioQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TreeUsuarioQry
            DbCommand com = con.CreateCommand();
            String countTreeUsuarioQry = String.Format(CultureInfo.CurrentCulture,TreeUsuario.TreeUsuarioQry.TreeUsuarioQryCount,"");
            com.CommandText = countTreeUsuarioQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TreeUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeUsuarioQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TreeUsuarioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeUsuarioQry = String.Format(CultureInfo.CurrentCulture, TreeUsuario.TreeUsuarioQry.TreeUsuarioQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreeUsuarioQry = listTreeUsuarioQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreeUsuarioQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeUsuarioQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeUsuarioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeUsuarioQryDP[])lista.ToArray(typeof(TreeUsuarioQryDP));
        }
        public static Int64 CountForUsuarioSistema(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuarioSistema TreeUsuarioQry
            DbCommand com = con.CreateCommand();
            String countTreeUsuarioQryForUsuarioSistema = String.Format(CultureInfo.CurrentCulture,TreeUsuario.TreeUsuarioQry.TreeUsuarioQryCount,"");
            countTreeUsuarioQryForUsuarioSistema += " WHERE \n";
            String delimitador = "";
            countTreeUsuarioQryForUsuarioSistema += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aIdSistema != 0)
            {
                countTreeUsuarioQryForUsuarioSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTreeUsuarioQryForUsuarioSistema;
            #endregion
            #region Parametros countTreeUsuarioQryForUsuarioSistema
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuarioSistema TreeUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreeUsuarioQryDP[] ListForUsuarioSistema(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario, Byte aIdSistema)
        {
            #region SQL List TreeUsuarioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreeUsuarioQryForUsuarioSistema = String.Format(CultureInfo.CurrentCulture, TreeUsuario.TreeUsuarioQry.TreeUsuarioQrySelect, "",
                String.Format("{0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix)
                );

            //listTreeUsuarioQryForUsuarioSistema = listTreeUsuarioQryForUsuarioSistema.Substring(6);
            //listTreeUsuarioQryForUsuarioSistema += " WHERE \n";
            //String delimitador = "";
            //listTreeUsuarioQryForUsuarioSistema += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
            //    delimitador);
            //delimitador = " AND ";
            //if (aIdSistema != 0)
            //{
            //    listTreeUsuarioQryForUsuarioSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            String filtraList = listTreeUsuarioQryForUsuarioSistema;
            //String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //     ConstantesGlobales.ParameterPrefix,
            //     listTreeUsuarioQryForUsuarioSistema, "",
            //     (startRowIndex + maximumRows).ToString(),
            //     startRowIndex,
            //     maximumRows,
            //     "" // DISTINCT si es que aplica.
            //     );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreeUsuarioQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            else
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}IdSistema", ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, 1);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreeUsuarioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreeUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreeUsuarioQryDP[])lista.ToArray(typeof(TreeUsuarioQryDP));
        }
    }
}
