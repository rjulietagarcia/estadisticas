using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class AccionesQryMD
    {
        private AccionesQryDP accionesQry = null;

        public AccionesQryMD(AccionesQryDP accionesQry)
        {
            this.accionesQry = accionesQry;
        }

        protected static AccionesQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            AccionesQryDP accionesQry = new AccionesQryDP();
            accionesQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            accionesQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            accionesQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            accionesQry.AccionId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            accionesQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            accionesQry.Abreviatura = dr.IsDBNull(5) ? "" : dr.GetString(5);
            accionesQry.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            accionesQry.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            accionesQry.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            accionesQry.FechaInicio = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            accionesQry.FechaFin = dr.IsDBNull(10) ? "" : dr.GetDateTime(10).ToShortDateString();;
            return accionesQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count AccionesQry
            DbCommand com = con.CreateCommand();
            String countAccionesQry = String.Format(CultureInfo.CurrentCulture,Acciones.AccionesQry.AccionesQryCount,"");
            com.CommandText = countAccionesQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count AccionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountAccionesQry = String.Format(CultureInfo.CurrentCulture,Acciones.AccionesQry.AccionesQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountAccionesQry);
                #endregion
            }
            return resultado;
        }
        public static AccionesQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List AccionesQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listAccionesQry = String.Format(CultureInfo.CurrentCulture, Acciones.AccionesQry.AccionesQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listAccionesQry = listAccionesQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listAccionesQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listAccionesQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load AccionesQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista AccionesQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista AccionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaAccionesQry = String.Format(CultureInfo.CurrentCulture,Acciones.AccionesQry.AccionesQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaAccionesQry);
                #endregion
            }
            return (AccionesQryDP[])lista.ToArray(typeof(AccionesQryDP));
        }
        public static Int64 CountForAccion(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Byte aIdAccion, Boolean aBitActivo)
        {
            Int64 resultado = 0;
            #region SQL CountForAccion AccionesQry
            DbCommand com = con.CreateCommand();
            String countAccionesQryForAccion = String.Format(CultureInfo.CurrentCulture,Acciones.AccionesQry.AccionesQryCount,"");
            countAccionesQryForAccion += " WHERE \n";
            String delimitador = "";
            countAccionesQryForAccion += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countAccionesQryForAccion += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countAccionesQryForAccion += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countAccionesQryForAccion += String.Format("    {1} Id_Accion = {0}IdAccion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countAccionesQryForAccion += String.Format("    {1} Bit_Activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countAccionesQryForAccion;
            #endregion
            #region Parametros countAccionesQryForAccion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdAccion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdAccion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForAccion AccionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountAccionesQryForAccion = String.Format(CultureInfo.CurrentCulture,Acciones.AccionesQry.AccionesQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountAccionesQryForAccion);
                #endregion
            }
            return resultado;
        }
        public static AccionesQryDP[] ListForAccion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Byte aIdAccion, Boolean aBitActivo)
        {
            #region SQL List AccionesQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listAccionesQryForAccion = String.Format(CultureInfo.CurrentCulture, Acciones.AccionesQry.AccionesQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listAccionesQryForAccion = listAccionesQryForAccion.Substring(6);
            listAccionesQryForAccion += " WHERE \n";
            String delimitador = "";
            listAccionesQryForAccion += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listAccionesQryForAccion += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listAccionesQryForAccion += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);                        
            delimitador = " AND ";
            if (aIdAccion != 0)
            {
                listAccionesQryForAccion += String.Format("    {1} Id_Accion = {0}IdAccion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
                delimitador = " AND ";
            }
            listAccionesQryForAccion += String.Format("    {1} Bit_Activo = {0}BitActivo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listAccionesQryForAccion, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listAccionesQryForAccion;
            DbDataReader dr;
            #endregion
            #region Parametros Load AccionesQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            if (aIdAccion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}IdAccion", ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdAccion);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}BitActivo",ConstantesGlobales.ParameterPrefix), DbType.Boolean, 0, ParameterDirection.Input, aBitActivo);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista AccionesQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista AccionesQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaAccionesQry = String.Format(CultureInfo.CurrentCulture,Acciones.AccionesQry.AccionesQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaAccionesQry);
                #endregion
            }
            return (AccionesQryDP[])lista.ToArray(typeof(AccionesQryDP));
        }
    }
}
