using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class AccionesQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = AccionesQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un AccionesQry!", ex);
            }
            return resultado;
        }

        public static AccionesQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            AccionesQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = AccionesQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de AccionesQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForAccion(DbConnection con, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Byte aIdAccion, Boolean aBitActivo) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = AccionesQryMD.CountForAccion(con, tran, aIdSistema, aIdModulo, aIdOpcion, aIdAccion, aBitActivo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un AccionesQry!", ex);
            }
            return resultado;
        }

        public static AccionesQryDP[] LoadListForAccion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Byte aIdAccion, Boolean aBitActivo) 
        {
            AccionesQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = AccionesQryMD.ListForAccion(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion, aIdAccion, aBitActivo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de AccionesQry!", ex);
            }
            return resultado;

        }
    }
}
