using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class FltPerfilEducacionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltPerfilEducacionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltPerfilEducacionQry!", ex);
            }
            return resultado;
        }

        public static FltPerfilEducacionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            FltPerfilEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltPerfilEducacionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltPerfilEducacionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForFiltro(DbConnection con, Byte aIdNiveltrabajo, Int32 aUsuIdUsuario) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltPerfilEducacionQryMD.CountForFiltro(con, tran, aIdNiveltrabajo, aUsuIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltPerfilEducacionQry!", ex);
            }
            return resultado;
        }

        public static FltPerfilEducacionQryDP[] LoadListForFiltro(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveltrabajo, Int32 aUsuIdUsuario) 
        {
            FltPerfilEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltPerfilEducacionQryMD.ListForFiltro(con, tran, startRowIndex, maximumRows, aIdNiveltrabajo, aUsuIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltPerfilEducacionQry!", ex);
            }
            return resultado;

        }
    }
}
