using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class FltPerfilEducacionQryMD
    {
        private FltPerfilEducacionQryDP fltPerfilEducacionQry = null;

        public FltPerfilEducacionQryMD(FltPerfilEducacionQryDP fltPerfilEducacionQry)
        {
            this.fltPerfilEducacionQry = fltPerfilEducacionQry;
        }

        protected static FltPerfilEducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            FltPerfilEducacionQryDP fltPerfilEducacionQry = new FltPerfilEducacionQryDP();
            fltPerfilEducacionQry.PerfilEducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            fltPerfilEducacionQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            fltPerfilEducacionQry.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            fltPerfilEducacionQry.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            fltPerfilEducacionQry.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            fltPerfilEducacionQry.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            fltPerfilEducacionQry.NiveltrabajoId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            fltPerfilEducacionQry.UsuIdUsuario = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            fltPerfilEducacionQry.Selected = dr.IsDBNull(8) ? false : dr.GetBoolean(8);
            return fltPerfilEducacionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count FltPerfilEducacionQry
            DbCommand com = con.CreateCommand();
            String countFltPerfilEducacionQry = String.Format(CultureInfo.CurrentCulture,FltPerfilEducacion.FltPerfilEducacionQry.FltPerfilEducacionQryCount,"");
            com.CommandText = countFltPerfilEducacionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count FltPerfilEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltPerfilEducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List FltPerfilEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltPerfilEducacionQry = String.Format(CultureInfo.CurrentCulture, FltPerfilEducacion.FltPerfilEducacionQry.FltPerfilEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltPerfilEducacionQry = listFltPerfilEducacionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltPerfilEducacionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltPerfilEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltPerfilEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltPerfilEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltPerfilEducacionQryDP[])lista.ToArray(typeof(FltPerfilEducacionQryDP));
        }
        public static Int64 CountForFiltro(DbConnection con, DbTransaction tran, Byte aIdNiveltrabajo, Int32 aUsuIdUsuario)
        {
            Int64 resultado = 0;
            #region SQL CountForFiltro FltPerfilEducacionQry
            DbCommand com = con.CreateCommand();
            String countFltPerfilEducacionQryForFiltro = String.Format(CultureInfo.CurrentCulture,FltPerfilEducacion.FltPerfilEducacionQry.FltPerfilEducacionQryCount,"");
            countFltPerfilEducacionQryForFiltro += " WHERE \n";
            String delimitador = "";
            countFltPerfilEducacionQryForFiltro += String.Format("    {1} pe.Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            //countFltPerfilEducacionQryForFiltro += String.Format("    {1} Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
            //    delimitador);
            //delimitador = " AND ";
            com.CommandText = countFltPerfilEducacionQryForFiltro;
            #endregion
            #region Parametros countFltPerfilEducacionQryForFiltro
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            //Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForFiltro FltPerfilEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltPerfilEducacionQryDP[] ListForFiltro(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveltrabajo, Int32 aUsuIdUsuario)
        {
            #region SQL List FltPerfilEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltPerfilEducacionQryForFiltro = String.Format(CultureInfo.CurrentCulture, FltPerfilEducacion.FltPerfilEducacionQry.FltPerfilEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltPerfilEducacionQryForFiltro = listFltPerfilEducacionQryForFiltro.Substring(6);
            String delimitador = " AND ";
            listFltPerfilEducacionQryForFiltro += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario )\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = "";
            listFltPerfilEducacionQryForFiltro += " WHERE \n";
            listFltPerfilEducacionQryForFiltro += String.Format("    {1} pe.Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listFltPerfilEducacionQryForFiltro += String.Format("    {1} pe.Bit_Activo = 1\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltPerfilEducacionQryForFiltro, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltPerfilEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}IdNiveltrabajo", ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltPerfilEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltPerfilEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltPerfilEducacionQryDP[])lista.ToArray(typeof(FltPerfilEducacionQryDP));
        }
    }
}
