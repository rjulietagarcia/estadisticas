using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class PerfilQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilQry!", ex);
            }
            return resultado;
        }

        public static PerfilQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PerfilQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForNombreYNivelTrabajo(DbConnection con, String aNombre, Byte aIdNiveltrabajo) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilQryMD.CountForNombreYNivelTrabajo(con, tran, aNombre, aIdNiveltrabajo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilQry!", ex);
            }
            return resultado;
        }

        public static PerfilQryDP[] LoadListForNombreYNivelTrabajo(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Byte aIdNiveltrabajo) 
        {
            PerfilQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilQryMD.ListForNombreYNivelTrabajo(con, tran, startRowIndex, maximumRows, aNombre, aIdNiveltrabajo);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilQry!", ex);
            }
            return resultado;

        }
    }
}
