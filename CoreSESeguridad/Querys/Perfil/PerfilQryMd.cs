using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class PerfilQryMD
    {
        private PerfilQryDP perfilQry = null;

        public PerfilQryMD(PerfilQryDP perfilQry)
        {
            this.perfilQry = perfilQry;
        }

        protected static PerfilQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilQryDP perfilQry = new PerfilQryDP();
            perfilQry.PerfilId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            perfilQry.Nombre = dr.IsDBNull(1) ? "" : dr.GetString(1);
            perfilQry.Abreviatura = dr.IsDBNull(2) ? "" : dr.GetString(2);
            perfilQry.BitActivo = dr.IsDBNull(3) ? false : dr.GetBoolean(3);
            perfilQry.UsuarioId = dr.IsDBNull(4) ? 0 : dr.GetInt32(4);
            perfilQry.FechaActualizacion = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            perfilQry.NiveltrabajoId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            return perfilQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilQry
            DbCommand com = con.CreateCommand();
            String countPerfilQry = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilQry.PerfilQryCount,"");
            com.CommandText = countPerfilQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PerfilQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilQry = String.Format(CultureInfo.CurrentCulture, Perfil.PerfilQry.PerfilQrySelect, "", ConstantesGlobales.IncluyeRows);
            listPerfilQry = listPerfilQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPerfilQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PerfilQryDP[])lista.ToArray(typeof(PerfilQryDP));
        }
        public static Int64 CountForNombreYNivelTrabajo(DbConnection con, DbTransaction tran, String aNombre, Byte aIdNiveltrabajo)
        {
            Int64 resultado = 0;
            #region SQL CountForNombreYNivelTrabajo PerfilQry
            DbCommand com = con.CreateCommand();
            String countPerfilQryForNombreYNivelTrabajo = String.Format(CultureInfo.CurrentCulture,Perfil.PerfilQry.PerfilQryCount,"");
            countPerfilQryForNombreYNivelTrabajo += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countPerfilQryForNombreYNivelTrabajo += String.Format("    {1} p.Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countPerfilQryForNombreYNivelTrabajo += String.Format("    {1} ult.id_niveltrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countPerfilQryForNombreYNivelTrabajo;
            #endregion
            #region Parametros countPerfilQryForNombreYNivelTrabajo
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForNombreYNivelTrabajo PerfilQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PerfilQryDP[] ListForNombreYNivelTrabajo(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Byte aIdNiveltrabajo)
        {
            #region SQL List PerfilQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilQryForNombreYNivelTrabajo = String.Format(CultureInfo.CurrentCulture, Perfil.PerfilQry.PerfilQrySelect, "", ConstantesGlobales.IncluyeRows);
            listPerfilQryForNombreYNivelTrabajo = listPerfilQryForNombreYNivelTrabajo.Substring(6);
            listPerfilQryForNombreYNivelTrabajo += " AND \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listPerfilQryForNombreYNivelTrabajo += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listPerfilQryForNombreYNivelTrabajo += String.Format("    {1} id_niveltrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listPerfilQryForNombreYNivelTrabajo, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList.Replace("top", "distinct top");
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PerfilQryDP[])lista.ToArray(typeof(PerfilQryDP));
        }
    }
}
