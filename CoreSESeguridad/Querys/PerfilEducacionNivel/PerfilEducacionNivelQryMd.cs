using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class PerfilEducacionNivelQryMD
    {
        private PerfilEducacionNivelQryDP perfilEducacionNivelQry = null;

        public PerfilEducacionNivelQryMD(PerfilEducacionNivelQryDP perfilEducacionNivelQry)
        {
            this.perfilEducacionNivelQry = perfilEducacionNivelQry;
        }

        protected static PerfilEducacionNivelQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            PerfilEducacionNivelQryDP perfilEducacionNivelQry = new PerfilEducacionNivelQryDP();
            perfilEducacionNivelQry.PerfilEducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            perfilEducacionNivelQry.NiveltrabajoId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            perfilEducacionNivelQry.CicloescolarId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            perfilEducacionNivelQry.UsuIdUsuario = dr.IsDBNull(3) ? 0 : dr.GetInt32(3);
            perfilEducacionNivelQry.NombrePerfil = dr.IsDBNull(4) ? "" : dr.GetString(4);
            perfilEducacionNivelQry.AbreviaturaPerfil = dr.IsDBNull(5) ? "" : dr.GetString(5);
            perfilEducacionNivelQry.NivelId = dr.IsDBNull(6) ? (short)0 : dr.GetInt16(6);
            perfilEducacionNivelQry.NombreNivel = dr.IsDBNull(7) ? "" : dr.GetString(7);
            perfilEducacionNivelQry.LetraFolio = dr.IsDBNull(8) ? "" : dr.GetString(8);
            perfilEducacionNivelQry.NombreTipoEducacion = dr.IsDBNull(9) ? "" : dr.GetString(9);
            perfilEducacionNivelQry.SistemaId = dr.IsDBNull(10) ? (Byte)0 : dr.GetByte(10);
            perfilEducacionNivelQry.NombreSistema = dr.IsDBNull(11) ? "" : dr.GetString(11);
            perfilEducacionNivelQry.AbreviaturaSistema = dr.IsDBNull(12) ? "" : dr.GetString(12);
            perfilEducacionNivelQry.CarpetaSistema = dr.IsDBNull(13) ? "" : dr.GetString(13);
            perfilEducacionNivelQry.BitActivo = dr.IsDBNull(14) ? false : dr.GetBoolean(14);
            perfilEducacionNivelQry.SubnivelId = dr.IsDBNull(15) ? (short)0 : dr.GetInt16(15);
            perfilEducacionNivelQry.NombreSubnivel = dr.IsDBNull(16) ? "" : dr.GetString(16);
            return perfilEducacionNivelQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count PerfilEducacionNivelQry
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionNivelQry = String.Format(CultureInfo.CurrentCulture,PerfilEducacionNivel.PerfilEducacionNivelQry.PerfilEducacionNivelQryCount,"");
            com.CommandText = countPerfilEducacionNivelQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count PerfilEducacionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionNivelQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List PerfilEducacionNivelQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionNivelQry = String.Format(CultureInfo.CurrentCulture, PerfilEducacionNivel.PerfilEducacionNivelQry.PerfilEducacionNivelQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listPerfilEducacionNivelQry = listPerfilEducacionNivelQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPerfilEducacionNivelQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPerfilEducacionNivelQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionNivelQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionNivelQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PerfilEducacionNivelQryDP[])lista.ToArray(typeof(PerfilEducacionNivelQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aUsuIdUsuario)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario PerfilEducacionNivelQry
            DbCommand com = con.CreateCommand();
            String countPerfilEducacionNivelQryForUsuario = String.Format(CultureInfo.CurrentCulture,PerfilEducacionNivel.PerfilEducacionNivelQry.PerfilEducacionNivelQryCount,"");
            //countPerfilEducacionNivelQryForUsuario += " WHERE \n";
            String delimitador = " AND ";
            if (aUsuIdUsuario != -1)
            {
                countPerfilEducacionNivelQryForUsuario += String.Format(" {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countPerfilEducacionNivelQryForUsuario;
            #endregion
            #region Parametros countPerfilEducacionNivelQryForUsuario
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario PerfilEducacionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static PerfilEducacionNivelQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario)
        {
            #region SQL List PerfilEducacionNivelQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listPerfilEducacionNivelQryForUsuario = String.Format(CultureInfo.CurrentCulture, PerfilEducacionNivel.PerfilEducacionNivelQry.PerfilEducacionNivelQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listPerfilEducacionNivelQryForUsuario = listPerfilEducacionNivelQryForUsuario.Substring(6);
            //listPerfilEducacionNivelQryForUsuario += " WHERE \n";
            String delimitador = " AND ";
            if (aUsuIdUsuario != -1)
            {
                listPerfilEducacionNivelQryForUsuario += String.Format("    {1} upe.Usu_Id_Usuario = {0}UsuIdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listPerfilEducacionNivelQryForUsuario, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listPerfilEducacionNivelQryForUsuario;
            DbDataReader dr;
            #endregion
            #region Parametros Load PerfilEducacionNivelQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aUsuIdUsuario != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}UsuIdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aUsuIdUsuario);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista PerfilEducacionNivelQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista PerfilEducacionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (PerfilEducacionNivelQryDP[])lista.ToArray(typeof(PerfilEducacionNivelQryDP));
        }
    }
}
