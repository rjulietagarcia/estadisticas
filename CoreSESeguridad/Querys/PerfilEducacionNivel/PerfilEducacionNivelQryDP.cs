using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "PerfilEducacionNivelQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: miércoles, 29 de julio de 2009.</Para>
    /// <Para>Hora: 01:15:21 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "PerfilEducacionNivelQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>CicloescolarId</term><description>Descripcion CicloescolarId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuIdUsuario</term><description>Descripcion UsuIdUsuario</description>
    ///    </item>
    ///    <item>
    ///        <term>NombrePerfil</term><description>Descripcion NombrePerfil</description>
    ///    </item>
    ///    <item>
    ///        <term>AbreviaturaPerfil</term><description>Descripcion AbreviaturaPerfil</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>NombreNivel</term><description>Descripcion NombreNivel</description>
    ///    </item>
    ///    <item>
    ///        <term>LetraFolio</term><description>Descripcion LetraFolio</description>
    ///    </item>
    ///    <item>
    ///        <term>NombreTipoEducacion</term><description>Descripcion NombreTipoEducacion</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>NombreSistema</term><description>Descripcion NombreSistema</description>
    ///    </item>
    ///    <item>
    ///        <term>AbreviaturaSistema</term><description>Descripcion AbreviaturaSistema</description>
    ///    </item>
    ///    <item>
    ///        <term>CarpetaSistema</term><description>Descripcion CarpetaSistema</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>SubnivelId</term><description>Descripcion SubnivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>NombreSubnivel</term><description>Descripcion NombreSubnivel</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "PerfilEducacionNivelQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// PerfilEducacionNivelQryDTO perfileducacionnivelqry = new PerfilEducacionNivelQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("PerfilEducacionNivelQry")]
    public class PerfilEducacionNivelQryDP
    {
        #region Definicion de campos privados.
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Int16 cicloescolarId;
        private Int32 usuIdUsuario;
        private String nombrePerfil;
        private String abreviaturaPerfil;
        private Int16 nivelId;
        private String nombreNivel;
        private String letraFolio;
        private String nombreTipoEducacion;
        private Byte sistemaId;
        private String nombreSistema;
        private String abreviaturaSistema;
        private String carpetaSistema;
        private Boolean bitActivo;
        private Int16 subnivelId;
        private String nombreSubnivel;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// CicloescolarId
        /// </summary> 
        [XmlElement("CicloescolarId")]
        public Int16 CicloescolarId
        {
            get {
                    return cicloescolarId; 
            }
            set {
                    cicloescolarId = value; 
            }
        }

        /// <summary>
        /// UsuIdUsuario
        /// </summary> 
        [XmlElement("UsuIdUsuario")]
        public Int32 UsuIdUsuario
        {
            get {
                    return usuIdUsuario; 
            }
            set {
                    usuIdUsuario = value; 
            }
        }

        /// <summary>
        /// NombrePerfil
        /// </summary> 
        [XmlElement("NombrePerfil")]
        public String NombrePerfil
        {
            get {
                    return nombrePerfil; 
            }
            set {
                    nombrePerfil = value; 
            }
        }

        /// <summary>
        /// AbreviaturaPerfil
        /// </summary> 
        [XmlElement("AbreviaturaPerfil")]
        public String AbreviaturaPerfil
        {
            get {
                    return abreviaturaPerfil; 
            }
            set {
                    abreviaturaPerfil = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public Int16 NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        /// <summary>
        /// NombreNivel
        /// </summary> 
        [XmlElement("NombreNivel")]
        public String NombreNivel
        {
            get {
                    return nombreNivel; 
            }
            set {
                    nombreNivel = value; 
            }
        }

        /// <summary>
        /// LetraFolio
        /// </summary> 
        [XmlElement("LetraFolio")]
        public String LetraFolio
        {
            get {
                    return letraFolio; 
            }
            set {
                    letraFolio = value; 
            }
        }

        /// <summary>
        /// NombreTipoEducacion
        /// </summary> 
        [XmlElement("NombreTipoEducacion")]
        public String NombreTipoEducacion
        {
            get {
                    return nombreTipoEducacion; 
            }
            set {
                    nombreTipoEducacion = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// NombreSistema
        /// </summary> 
        [XmlElement("NombreSistema")]
        public String NombreSistema
        {
            get {
                    return nombreSistema; 
            }
            set {
                    nombreSistema = value; 
            }
        }

        /// <summary>
        /// AbreviaturaSistema
        /// </summary> 
        [XmlElement("AbreviaturaSistema")]
        public String AbreviaturaSistema
        {
            get {
                    return abreviaturaSistema; 
            }
            set {
                    abreviaturaSistema = value; 
            }
        }

        /// <summary>
        /// CarpetaSistema
        /// </summary> 
        [XmlElement("CarpetaSistema")]
        public String CarpetaSistema
        {
            get {
                    return carpetaSistema; 
            }
            set {
                    carpetaSistema = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// SubnivelId
        /// </summary> 
        [XmlElement("SubnivelId")]
        public Int16 SubnivelId
        {
            get {
                    return subnivelId; 
            }
            set {
                    subnivelId = value; 
            }
        }

        /// <summary>
        /// NombreSubnivel
        /// </summary> 
        [XmlElement("NombreSubnivel")]
        public String NombreSubnivel
        {
            get {
                    return nombreSubnivel; 
            }
            set {
                    nombreSubnivel = value; 
            }
        }

        #endregion.
    }
}
