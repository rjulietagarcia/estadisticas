using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class PerfilEducacionNivelQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilEducacionNivelQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilEducacionNivelQry!", ex);
            }
            return resultado;
        }

        public static PerfilEducacionNivelQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            PerfilEducacionNivelQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionNivelQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilEducacionNivelQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuario(DbConnection con, Int32 aUsuIdUsuario) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = PerfilEducacionNivelQryMD.CountForUsuario(con, tran, aUsuIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un PerfilEducacionNivelQry!", ex);
            }
            return resultado;
        }

        public static PerfilEducacionNivelQryDP[] LoadListForUsuario(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aUsuIdUsuario) 
        {
            PerfilEducacionNivelQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = PerfilEducacionNivelQryMD.ListForUsuario(con, tran, startRowIndex, maximumRows, aUsuIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de PerfilEducacionNivelQry!", ex);
            }
            return resultado;

        }
    }
}
