using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class TreePerfilEducacionNivelQryMD
    {
        private TreePerfilEducacionNivelQryDP treePerfilEducacionNivelQry = null;

        public TreePerfilEducacionNivelQryMD(TreePerfilEducacionNivelQryDP treePerfilEducacionNivelQry)
        {
            this.treePerfilEducacionNivelQry = treePerfilEducacionNivelQry;
        }

        protected static TreePerfilEducacionNivelQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            TreePerfilEducacionNivelQryDP treePerfilEducacionNivelQry = new TreePerfilEducacionNivelQryDP();
            treePerfilEducacionNivelQry.Llave = dr.IsDBNull(0) ? "" : dr.GetString(0);
            treePerfilEducacionNivelQry.PerfilEducacionId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            treePerfilEducacionNivelQry.NiveltrabajoId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            treePerfilEducacionNivelQry.SistemaId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            return treePerfilEducacionNivelQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count TreePerfilEducacionNivelQry
            DbCommand com = con.CreateCommand();
            String countTreePerfilEducacionNivelQry = String.Format(CultureInfo.CurrentCulture,TreePerfilEducacionNivel.TreePerfilEducacionNivelQry.TreePerfilEducacionNivelQryCount,"");
            com.CommandText = countTreePerfilEducacionNivelQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count TreePerfilEducacionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreePerfilEducacionNivelQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List TreePerfilEducacionNivelQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreePerfilEducacionNivelQry = String.Format(CultureInfo.CurrentCulture, TreePerfilEducacionNivel.TreePerfilEducacionNivelQry.TreePerfilEducacionNivelQrySelect, "", ConstantesGlobales.IncluyeRows);
            listTreePerfilEducacionNivelQry = listTreePerfilEducacionNivelQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listTreePerfilEducacionNivelQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreePerfilEducacionNivelQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreePerfilEducacionNivelQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreePerfilEducacionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreePerfilEducacionNivelQryDP[])lista.ToArray(typeof(TreePerfilEducacionNivelQryDP));
        }
        public static Int64 CountForPerfilNivelSistema(DbConnection con, DbTransaction tran, Int16 aIdPerfilEducacion, Byte aIdNiveltrabajo, Byte aIdSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForPerfilNivelSistema TreePerfilEducacionNivelQry
            DbCommand com = con.CreateCommand();
            String countTreePerfilEducacionNivelQryForPerfilNivelSistema = String.Format(CultureInfo.CurrentCulture,TreePerfilEducacionNivel.TreePerfilEducacionNivelQry.TreePerfilEducacionNivelQryCount,"");
            countTreePerfilEducacionNivelQryForPerfilNivelSistema += " WHERE \n";
            String delimitador = "";
            if (aIdPerfilEducacion != -1)
            {
                countTreePerfilEducacionNivelQryForPerfilNivelSistema += String.Format("    {1} Id_Perfil_Educacion = {0}IdPerfilEducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdNiveltrabajo != 0)
            {
                countTreePerfilEducacionNivelQryForPerfilNivelSistema += String.Format("    {1} Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSistema != 0)
            {
                countTreePerfilEducacionNivelQryForPerfilNivelSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countTreePerfilEducacionNivelQryForPerfilNivelSistema;
            #endregion
            #region Parametros countTreePerfilEducacionNivelQryForPerfilNivelSistema
            if (aIdPerfilEducacion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPerfilEducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPerfilEducacion);
            }
            if (aIdNiveltrabajo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveltrabajo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            }
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForPerfilNivelSistema TreePerfilEducacionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static TreePerfilEducacionNivelQryDP[] ListForPerfilNivelSistema(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdPerfilEducacion, Byte aIdNiveltrabajo, Byte aIdSistema)
        {
            #region SQL List TreePerfilEducacionNivelQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listTreePerfilEducacionNivelQryForPerfilNivelSistema = String.Format(CultureInfo.CurrentCulture, TreePerfilEducacionNivel.TreePerfilEducacionNivelQry.TreePerfilEducacionNivelQrySelect, "",
                String.Format("{0}perfilEducacion\n", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}nivelTrabajo\n", Model.ConstantesGlobales.ParameterPrefix),
                String.Format("{0}sistema\n", Model.ConstantesGlobales.ParameterPrefix)
                );
                
            //    String.Format(CultureInfo.CurrentCulture, TreePerfilEducacionNivel.TreePerfilEducacionNivelQry.TreePerfilEducacionNivelQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listTreePerfilEducacionNivelQryForPerfilNivelSistema = listTreePerfilEducacionNivelQryForPerfilNivelSistema.Substring(6);
            //listTreePerfilEducacionNivelQryForPerfilNivelSistema += " WHERE \n";
            //String delimitador = "";
            //if (aIdPerfilEducacion != -1)
            //{
            //    listTreePerfilEducacionNivelQryForPerfilNivelSistema += String.Format("    {1} Id_Perfil_Educacion = {0}IdPerfilEducacion\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            //if (aIdNiveltrabajo != 0)
            //{
            //    listTreePerfilEducacionNivelQryForPerfilNivelSistema += String.Format("    {1} Id_NivelTrabajo = {0}IdNiveltrabajo\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            //if (aIdSistema != 0)
            //{
            //    listTreePerfilEducacionNivelQryForPerfilNivelSistema += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
            //        delimitador);
            //    delimitador = " AND ";
            //}
            String filtraList = listTreePerfilEducacionNivelQryForPerfilNivelSistema;
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load TreePerfilEducacionNivelQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdPerfilEducacion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}perfilEducacion", ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPerfilEducacion);
            }
            if (aIdNiveltrabajo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}nivelTrabajo", ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveltrabajo);
            }
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}sistema", ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista TreePerfilEducacionNivelQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista TreePerfilEducacionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (TreePerfilEducacionNivelQryDP[])lista.ToArray(typeof(TreePerfilEducacionNivelQryDP));
        }
    }
}
