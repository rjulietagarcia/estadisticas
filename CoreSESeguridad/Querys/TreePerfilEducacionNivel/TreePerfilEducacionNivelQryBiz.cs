using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class TreePerfilEducacionNivelQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreePerfilEducacionNivelQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreePerfilEducacionNivelQry!", ex);
            }
            return resultado;
        }

        public static TreePerfilEducacionNivelQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            TreePerfilEducacionNivelQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreePerfilEducacionNivelQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreePerfilEducacionNivelQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForPerfilNivelSistema(DbConnection con, Int16 aIdPerfilEducacion, Byte aIdNiveltrabajo, Byte aIdSistema) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = TreePerfilEducacionNivelQryMD.CountForPerfilNivelSistema(con, tran, aIdPerfilEducacion, aIdNiveltrabajo, aIdSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un TreePerfilEducacionNivelQry!", ex);
            }
            return resultado;
        }

        public static TreePerfilEducacionNivelQryDP[] LoadListForPerfilNivelSistema(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdPerfilEducacion, Byte aIdNiveltrabajo, Byte aIdSistema) 
        {
            TreePerfilEducacionNivelQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = TreePerfilEducacionNivelQryMD.ListForPerfilNivelSistema(con, tran, startRowIndex, maximumRows, aIdPerfilEducacion, aIdNiveltrabajo, aIdSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de TreePerfilEducacionNivelQry!", ex);
            }
            return resultado;

        }
    }
}
