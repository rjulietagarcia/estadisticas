using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "TreePerfilEducacionNivelQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 27 de julio de 2009.</Para>
    /// <Para>Hora: 03:27:36 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "TreePerfilEducacionNivelQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>Llave</term><description>Descripcion Llave</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilEducacionId</term><description>Descripcion PerfilEducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "TreePerfilEducacionNivelQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// TreePerfilEducacionNivelQryDTO treeperfileducacionnivelqry = new TreePerfilEducacionNivelQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("TreePerfilEducacionNivelQry")]
    public class TreePerfilEducacionNivelQryDP
    {
        #region Definicion de campos privados.
        private String llave;
        private Int16 perfilEducacionId;
        private Byte niveltrabajoId;
        private Byte sistemaId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// Llave
        /// </summary> 
        [XmlElement("Llave")]
        public String Llave
        {
            get {
                    return llave; 
            }
            set {
                    llave = value; 
            }
        }

        /// <summary>
        /// PerfilEducacionId
        /// </summary> 
        [XmlElement("PerfilEducacionId")]
        public Int16 PerfilEducacionId
        {
            get {
                    return perfilEducacionId; 
            }
            set {
                    perfilEducacionId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public Byte NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        #endregion.
    }
}
