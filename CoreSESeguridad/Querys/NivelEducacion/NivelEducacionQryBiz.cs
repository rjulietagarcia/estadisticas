using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class NivelEducacionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = NivelEducacionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un NivelEducacionQry!", ex);
            }
            return resultado;
        }

        public static NivelEducacionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            NivelEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = NivelEducacionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de NivelEducacionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescipcion(DbConnection con, Int16 aIdTipoeducacion, String aNombre) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = NivelEducacionQryMD.CountForDescipcion(con, tran, aIdTipoeducacion, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un NivelEducacionQry!", ex);
            }
            return resultado;
        }

        public static NivelEducacionQryDP[] LoadListForDescipcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdTipoeducacion, String aNombre) 
        {
            NivelEducacionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = NivelEducacionQryMD.ListForDescipcion(con, tran, startRowIndex, maximumRows, aIdTipoeducacion, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de NivelEducacionQry!", ex);
            }
            return resultado;

        }
    }
}
