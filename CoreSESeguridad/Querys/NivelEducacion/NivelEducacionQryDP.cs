using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "NivelEducacionQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: miércoles, 10 de junio de 2009.</Para>
    /// <Para>Hora: 06:06:22 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "NivelEducacionQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>LetraFolio</term><description>Descripcion LetraFolio</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "NivelEducacionQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// NivelEducacionQryDTO niveleducacionqry = new NivelEducacionQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("NivelEducacionQry")]
    public class NivelEducacionQryDP
    {
        #region Definicion de campos privados.
        private Int16 tipoeducacionId;
        private Int16 nivelId;
        private String nombre;
        private String letraFolio;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public Int16 TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public Int16 NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// LetraFolio
        /// </summary> 
        [XmlElement("LetraFolio")]
        public String LetraFolio
        {
            get {
                    return letraFolio; 
            }
            set {
                    letraFolio = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        #endregion.
    }
}
