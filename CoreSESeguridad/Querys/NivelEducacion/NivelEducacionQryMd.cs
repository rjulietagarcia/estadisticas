using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class NivelEducacionQryMD
    {
        private NivelEducacionQryDP nivelEducacionQry = null;

        public NivelEducacionQryMD(NivelEducacionQryDP nivelEducacionQry)
        {
            this.nivelEducacionQry = nivelEducacionQry;
        }

        protected static NivelEducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            NivelEducacionQryDP nivelEducacionQry = new NivelEducacionQryDP();
            nivelEducacionQry.TipoeducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            nivelEducacionQry.NivelId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            nivelEducacionQry.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            nivelEducacionQry.LetraFolio = dr.IsDBNull(3) ? "" : dr.GetString(3);
            nivelEducacionQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            nivelEducacionQry.UsuarioId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            nivelEducacionQry.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            return nivelEducacionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count NivelEducacionQry
            DbCommand com = con.CreateCommand();
            String countNivelEducacionQry = String.Format(CultureInfo.CurrentCulture,NivelEducacion.NivelEducacionQry.NivelEducacionQryCount,"");
            com.CommandText = countNivelEducacionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count NivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountNivelEducacionQry = String.Format(CultureInfo.CurrentCulture,NivelEducacion.NivelEducacionQry.NivelEducacionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountNivelEducacionQry);
                #endregion
            }
            return resultado;
        }
        public static NivelEducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List NivelEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNivelEducacionQry = String.Format(CultureInfo.CurrentCulture, NivelEducacion.NivelEducacionQry.NivelEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listNivelEducacionQry = listNivelEducacionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listNivelEducacionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load NivelEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista NivelEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista NivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaNivelEducacionQry = String.Format(CultureInfo.CurrentCulture,NivelEducacion.NivelEducacionQry.NivelEducacionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaNivelEducacionQry);
                #endregion
            }
            return (NivelEducacionQryDP[])lista.ToArray(typeof(NivelEducacionQryDP));
        }
        public static Int64 CountForDescipcion(DbConnection con, DbTransaction tran, Int16 aIdTipoeducacion, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescipcion NivelEducacionQry
            DbCommand com = con.CreateCommand();
            String countNivelEducacionQryForDescipcion = String.Format(CultureInfo.CurrentCulture,NivelEducacion.NivelEducacionQry.NivelEducacionQryCount,"");
            countNivelEducacionQryForDescipcion += " WHERE \n";
            String delimitador = "";
            if (aIdTipoeducacion != -1)
            {
                countNivelEducacionQryForDescipcion += String.Format("    {1} id_tipoeducacion = {0}IdTipoeducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre != "")
            {
                countNivelEducacionQryForDescipcion += String.Format("    {1} nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countNivelEducacionQryForDescipcion;
            #endregion
            #region Parametros countNivelEducacionQryForDescipcion
            if (aIdTipoeducacion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTipoeducacion);
            }
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescipcion NivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountNivelEducacionQryForDescipcion = String.Format(CultureInfo.CurrentCulture,NivelEducacion.NivelEducacionQry.NivelEducacionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountNivelEducacionQryForDescipcion);
                #endregion
            }
            return resultado;
        }
        public static NivelEducacionQryDP[] ListForDescipcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdTipoeducacion, String aNombre)
        {
            #region SQL List NivelEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listNivelEducacionQryForDescipcion = String.Format(CultureInfo.CurrentCulture, NivelEducacion.NivelEducacionQry.NivelEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listNivelEducacionQryForDescipcion = listNivelEducacionQryForDescipcion.Substring(6);
            listNivelEducacionQryForDescipcion += " WHERE \n";
            String delimitador = "";
            if (aIdTipoeducacion != -1)
            {
                listNivelEducacionQryForDescipcion += String.Format("    {1} id_tipoeducacion = {0}IdTipoeducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre != "")
            {
                listNivelEducacionQryForDescipcion += String.Format("    {1} nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listNivelEducacionQryForDescipcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load NivelEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdTipoeducacion != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdTipoeducacion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdTipoeducacion);
            }
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista NivelEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista NivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaNivelEducacionQry = String.Format(CultureInfo.CurrentCulture,NivelEducacion.NivelEducacionQry.NivelEducacionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaNivelEducacionQry);
                #endregion
            }
            return (NivelEducacionQryDP[])lista.ToArray(typeof(NivelEducacionQryDP));
        }
    }
}
