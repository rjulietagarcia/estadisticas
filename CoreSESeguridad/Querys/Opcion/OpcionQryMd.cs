using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class OpcionQryMD
    {
        private OpcionQryDP opcionQry = null;

        public OpcionQryMD(OpcionQryDP opcionQry)
        {
            this.opcionQry = opcionQry;
        }

        protected static OpcionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            OpcionQryDP opcionQry = new OpcionQryDP();
            opcionQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            opcionQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            opcionQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            opcionQry.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            opcionQry.Abreviatura = dr.IsDBNull(4) ? "" : dr.GetString(4);
            opcionQry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            opcionQry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            opcionQry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            opcionQry.FechaInicio = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            opcionQry.FechaFin = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            opcionQry.CarpetaOpcion = dr.IsDBNull(10) ? "" : dr.GetString(10);
            opcionQry.SistemaPuerto = dr.IsDBNull(11) ? "" : dr.GetString(11);
            return opcionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count OpcionQry
            DbCommand com = con.CreateCommand();
            String countOpcionQry = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionQry.OpcionQryCount,"");
            com.CommandText = countOpcionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count OpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcionQry = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionQry.OpcionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcionQry);
                #endregion
            }
            return resultado;
        }
        public static OpcionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List OpcionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcionQry = String.Format(CultureInfo.CurrentCulture, Opcion.OpcionQry.OpcionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listOpcionQry = listOpcionQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listOpcionQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listOpcionQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load OpcionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista OpcionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista OpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcionQry = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionQry.OpcionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcionQry);
                #endregion
            }
            return (OpcionQryDP[])lista.ToArray(typeof(OpcionQryDP));
        }
        public static Int64 CountForOpcion(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion)
        {
            Int64 resultado = 0;
            #region SQL CountForOpcion OpcionQry
            DbCommand com = con.CreateCommand();
            String countOpcionQryForOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionQry.OpcionQryCount,"");
            countOpcionQryForOpcion += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countOpcionQryForOpcion += String.Format("    {1} o.Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countOpcionQryForOpcion += String.Format("    {1} o.Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countOpcionQryForOpcion += String.Format("    {1} o.Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countOpcionQryForOpcion;
            #endregion
            #region Parametros countOpcionQryForOpcion
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForOpcion OpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountOpcionQryForOpcion = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionQry.OpcionQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountOpcionQryForOpcion);
                #endregion
            }
            return resultado;
        }
        public static OpcionQryDP[] ListForOpcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion)
        {
            #region SQL List OpcionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listOpcionQryForOpcion = String.Format(CultureInfo.CurrentCulture, Opcion.OpcionQry.OpcionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listOpcionQryForOpcion = listOpcionQryForOpcion.Substring(6);
            listOpcionQryForOpcion += " AND \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listOpcionQryForOpcion += String.Format("    {1} o.Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listOpcionQryForOpcion += String.Format("    {1} o.Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listOpcionQryForOpcion += String.Format("    {1} o.Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listOpcionQryForOpcion += " ORDER BY o.Id_Sistema, o.Id_Modulo, o.Id_Opcion";
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listOpcionQryForOpcion, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listOpcionQryForOpcion;
            DbDataReader dr;
            #endregion
            #region Parametros Load OpcionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista OpcionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista OpcionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaOpcionQry = String.Format(CultureInfo.CurrentCulture,Opcion.OpcionQry.OpcionQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaOpcionQry);
                #endregion
            }
            return (OpcionQryDP[])lista.ToArray(typeof(OpcionQryDP));
        }
    }
}
