using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class MunicipioQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = MunicipioQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un MunicipioQry!", ex);
            }
            return resultado;
        }

        public static MunicipioQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            MunicipioQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MunicipioQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de MunicipioQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, Int16 aIdPais, Int16 aIdEntidad, String aNombre) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = MunicipioQryMD.CountForDescripcion(con, tran, aIdPais, aIdEntidad, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un MunicipioQry!", ex);
            }
            return resultado;
        }

        public static MunicipioQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdPais, Int16 aIdEntidad, String aNombre) 
        {
            MunicipioQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MunicipioQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aIdPais, aIdEntidad, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de MunicipioQry!", ex);
            }
            return resultado;

        }
    }
}
