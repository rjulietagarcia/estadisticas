using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class MunicipioQryMD
    {
        private MunicipioQryDP municipioQry = null;

        public MunicipioQryMD(MunicipioQryDP municipioQry)
        {
            this.municipioQry = municipioQry;
        }

        protected static MunicipioQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            MunicipioQryDP municipioQry = new MunicipioQryDP();
            municipioQry.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            municipioQry.EntidadId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            municipioQry.MunicipioId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            municipioQry.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            municipioQry.ZonaEconomica = dr.IsDBNull(4) ? "" : dr.GetString(4);
            municipioQry.BitActivo = dr.IsDBNull(5) ? false : dr.GetBoolean(5);
            municipioQry.UsuarioId = dr.IsDBNull(6) ? 0 : dr.GetInt32(6);
            municipioQry.FechaActualizacion = dr.IsDBNull(7) ? "" : dr.GetDateTime(7).ToShortDateString();;
            return municipioQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count MunicipioQry
            DbCommand com = con.CreateCommand();
            String countMunicipioQry = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioQry.MunicipioQryCount,"");
            com.CommandText = countMunicipioQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count MunicipioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountMunicipioQry = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioQry.MunicipioQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountMunicipioQry);
                #endregion
            }
            return resultado;
        }
        public static MunicipioQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List MunicipioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listMunicipioQry = String.Format(CultureInfo.CurrentCulture, Municipio.MunicipioQry.MunicipioQrySelect, "", ConstantesGlobales.IncluyeRows);
            listMunicipioQry = listMunicipioQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listMunicipioQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load MunicipioQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista MunicipioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista MunicipioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaMunicipioQry = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioQry.MunicipioQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaMunicipioQry);
                #endregion
            }
            return (MunicipioQryDP[])lista.ToArray(typeof(MunicipioQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, Int16 aIdPais, Int16 aIdEntidad, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion MunicipioQry
            DbCommand com = con.CreateCommand();
            String countMunicipioQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioQry.MunicipioQryCount,"");
            countMunicipioQryForDescripcion += " WHERE \n";
            String delimitador = "";
            countMunicipioQryForDescripcion += String.Format("    {1} Id_Pais = {0}IdPais\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countMunicipioQryForDescripcion += String.Format("    {1} Id_Entidad = {0}IdEntidad\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aNombre != "")
            {
                countMunicipioQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countMunicipioQryForDescripcion;
            #endregion
            #region Parametros countMunicipioQryForDescripcion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPais);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdEntidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdEntidad);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion MunicipioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountMunicipioQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioQry.MunicipioQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountMunicipioQryForDescripcion);
                #endregion
            }
            return resultado;
        }
        public static MunicipioQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdPais, Int16 aIdEntidad, String aNombre)
        {
            #region SQL List MunicipioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listMunicipioQryForDescripcion = String.Format(CultureInfo.CurrentCulture, Municipio.MunicipioQry.MunicipioQrySelect, "", ConstantesGlobales.IncluyeRows);
            listMunicipioQryForDescripcion = listMunicipioQryForDescripcion.Substring(6);
            listMunicipioQryForDescripcion += " WHERE \n";
            String delimitador = "";
            listMunicipioQryForDescripcion += String.Format("    {1} Id_Pais = {0}IdPais\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listMunicipioQryForDescripcion += String.Format("    {1} Id_Entidad = {0}IdEntidad\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            if (aNombre != "")
            {
                listMunicipioQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listMunicipioQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load MunicipioQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPais);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdEntidad",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdEntidad);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista MunicipioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista MunicipioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaMunicipioQry = String.Format(CultureInfo.CurrentCulture,Municipio.MunicipioQry.MunicipioQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaMunicipioQry);
                #endregion
            }
            return (MunicipioQryDP[])lista.ToArray(typeof(MunicipioQryDP));
        }
    }
}
