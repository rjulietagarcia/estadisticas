using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class MembershipUserQryMD
    {
        private MembershipUserQryDP membershipUserQry = null;

        public MembershipUserQryMD(MembershipUserQryDP membershipUserQry)
        {
            this.membershipUserQry = membershipUserQry;
        }

        protected static MembershipUserQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            MembershipUserQryDP membershipUserQry = new MembershipUserQryDP();
            membershipUserQry.Providername = dr.IsDBNull(0) ? "" : dr.GetString(0);
            membershipUserQry.Username = dr.IsDBNull(1) ? "" : dr.GetString(1);
            membershipUserQry.Provideruserkey = dr.IsDBNull(2) ? 0 : dr.GetInt32(2);
            membershipUserQry.Email = dr.IsDBNull(3) ? "" : dr.GetString(3);
            membershipUserQry.Passwordquestion = dr.IsDBNull(4) ? "" : dr.GetString(4);
            membershipUserQry.Comment = dr.IsDBNull(5) ? "" : dr.GetString(5);
            membershipUserQry.Isapproved = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            membershipUserQry.Islockedout = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            membershipUserQry.Creationdate = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            membershipUserQry.Lastlogindate = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            membershipUserQry.Lastactivitydate = dr.IsDBNull(10) ? "" : dr.GetDateTime(10).ToShortDateString();;
            membershipUserQry.Lastpasswordchangeddate = dr.IsDBNull(11) ? "" : dr.GetDateTime(11).ToShortDateString();;
            membershipUserQry.Lastlockedoutdate = dr.IsDBNull(12) ? "" : dr.GetDateTime(12).ToShortDateString();;
            membershipUserQry.Login = dr.IsDBNull(13) ? "" : dr.GetString(13);
            membershipUserQry.Password = dr.IsDBNull(14) ? "" : dr.GetString(14);
            membershipUserQry.UsuarioId = dr.IsDBNull(15) ? 0 : dr.GetInt32(15);
            return membershipUserQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count MembershipUserQry
            DbCommand com = con.CreateCommand();
            String countMembershipUserQry = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQryCount,"");
            com.CommandText = countMembershipUserQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count MembershipUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountMembershipUserQry = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountMembershipUserQry);
                #endregion
            }
            return resultado;
        }
        public static MembershipUserQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List MembershipUserQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listMembershipUserQry = String.Format(CultureInfo.CurrentCulture, MembershipUser.MembershipUserQry.MembershipUserQrySelect, "", ConstantesGlobales.IncluyeRows);
            listMembershipUserQry = listMembershipUserQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listMembershipUserQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load MembershipUserQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista MembershipUserQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista MembershipUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaMembershipUserQry = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaMembershipUserQry);
                #endregion
            }
            return (MembershipUserQryDP[])lista.ToArray(typeof(MembershipUserQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, String aLogin)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario MembershipUserQry
            DbCommand com = con.CreateCommand();
            String countMembershipUserQryForUsuario = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQryCount,"");
            countMembershipUserQryForUsuario += " AND \n";
            String delimitador = "";
            countMembershipUserQryForUsuario += String.Format("    {1} u.Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countMembershipUserQryForUsuario;
            #endregion
            #region Parametros countMembershipUserQryForUsuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario MembershipUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountMembershipUserQryForUsuario = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountMembershipUserQryForUsuario);
                #endregion
            }
            return resultado;
        }
        public static MembershipUserQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aLogin)
        {
            #region SQL List MembershipUserQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listMembershipUserQryForUsuario = String.Format(CultureInfo.CurrentCulture, MembershipUser.MembershipUserQry.MembershipUserQrySelect, "", ConstantesGlobales.IncluyeRows);
            listMembershipUserQryForUsuario = listMembershipUserQryForUsuario.Substring(6);
            listMembershipUserQryForUsuario += " AND \n";
            String delimitador = "";
            listMembershipUserQryForUsuario += String.Format("    {1} u.Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listMembershipUserQryForUsuario, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load MembershipUserQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista MembershipUserQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista MembershipUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaMembershipUserQry = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaMembershipUserQry);
                #endregion
            }
            return (MembershipUserQryDP[])lista.ToArray(typeof(MembershipUserQryDP));
        }
        public static Int64 CountForUsuarioPassword(DbConnection con, DbTransaction tran, String aLogin, String aPassword)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuarioPassword MembershipUserQry
            DbCommand com = con.CreateCommand();
            String countMembershipUserQryForUsuarioPassword = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQryCount,"");
            String delimitador = " AND ";
            countMembershipUserQryForUsuarioPassword += String.Format("    {1} u.Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countMembershipUserQryForUsuarioPassword += String.Format("    {1} u.Password = {0}Password\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countMembershipUserQryForUsuarioPassword;
            #endregion
            #region Parametros countMembershipUserQryForUsuarioPassword
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Password",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aPassword);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuarioPassword MembershipUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountMembershipUserQryForUsuarioPassword = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountMembershipUserQryForUsuarioPassword);
                #endregion
            }
            return resultado;
        }
        public static MembershipUserQryDP[] ListForUsuarioPassword(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aLogin, String aPassword)
        {
            #region SQL List MembershipUserQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listMembershipUserQryForUsuarioPassword = String.Format(CultureInfo.CurrentCulture, MembershipUser.MembershipUserQry.MembershipUserQrySelect, "", ConstantesGlobales.IncluyeRows);
            listMembershipUserQryForUsuarioPassword = listMembershipUserQryForUsuarioPassword.Substring(6);
            String delimitador = " AND ";
            listMembershipUserQryForUsuarioPassword += String.Format("    {1} u.Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listMembershipUserQryForUsuarioPassword += String.Format("    {1} u.Password = {0}Password\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listMembershipUserQryForUsuarioPassword, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load MembershipUserQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Password",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aPassword);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista MembershipUserQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista MembershipUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaMembershipUserQry = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaMembershipUserQry);
                #endregion
            }
            return (MembershipUserQryDP[])lista.ToArray(typeof(MembershipUserQryDP));
        }
        public static Int64 CountForSid(DbConnection con, DbTransaction tran, Int32 aIdUsuario)
        {
            Int64 resultado = 0;
            #region SQL CountForSid MembershipUserQry
            DbCommand com = con.CreateCommand();
            String countMembershipUserQryForSid = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQryCount,"");
            String delimitador = " AND ";
            countMembershipUserQryForSid += String.Format("    {1} u.Id_Usuario = {0}IdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countMembershipUserQryForSid;
            #endregion
            #region Parametros countMembershipUserQryForSid
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForSid MembershipUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountMembershipUserQryForSid = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountMembershipUserQryForSid);
                #endregion
            }
            return resultado;
        }
        public static MembershipUserQryDP[] ListForSid(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aIdUsuario)
        {
            #region SQL List MembershipUserQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listMembershipUserQryForSid = String.Format(CultureInfo.CurrentCulture, MembershipUser.MembershipUserQry.MembershipUserQrySelect, "", ConstantesGlobales.IncluyeRows);
            listMembershipUserQryForSid = listMembershipUserQryForSid.Substring(6);
            String delimitador = " AND ";
            listMembershipUserQryForSid += String.Format("    {1} u.Id_Usuario = {0}IdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listMembershipUserQryForSid, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load MembershipUserQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista MembershipUserQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista MembershipUserQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaMembershipUserQry = String.Format(CultureInfo.CurrentCulture,MembershipUser.MembershipUserQry.MembershipUserQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaMembershipUserQry);
                #endregion
            }
            return (MembershipUserQryDP[])lista.ToArray(typeof(MembershipUserQryDP));
        }
    }
}
