using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "MembershipUserQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 10:18:12 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "MembershipUserQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>Providername</term><description>Descripcion Providername</description>
    ///    </item>
    ///    <item>
    ///        <term>Username</term><description>Descripcion Username</description>
    ///    </item>
    ///    <item>
    ///        <term>Provideruserkey</term><description>Descripcion Provideruserkey</description>
    ///    </item>
    ///    <item>
    ///        <term>Email</term><description>Descripcion Email</description>
    ///    </item>
    ///    <item>
    ///        <term>Passwordquestion</term><description>Descripcion Passwordquestion</description>
    ///    </item>
    ///    <item>
    ///        <term>Comment</term><description>Descripcion Comment</description>
    ///    </item>
    ///    <item>
    ///        <term>Isapproved</term><description>Descripcion Isapproved</description>
    ///    </item>
    ///    <item>
    ///        <term>Islockedout</term><description>Descripcion Islockedout</description>
    ///    </item>
    ///    <item>
    ///        <term>Creationdate</term><description>Descripcion Creationdate</description>
    ///    </item>
    ///    <item>
    ///        <term>Lastlogindate</term><description>Descripcion Lastlogindate</description>
    ///    </item>
    ///    <item>
    ///        <term>Lastactivitydate</term><description>Descripcion Lastactivitydate</description>
    ///    </item>
    ///    <item>
    ///        <term>Lastpasswordchangeddate</term><description>Descripcion Lastpasswordchangeddate</description>
    ///    </item>
    ///    <item>
    ///        <term>Lastlockedoutdate</term><description>Descripcion Lastlockedoutdate</description>
    ///    </item>
    ///    <item>
    ///        <term>Login</term><description>Descripcion Login</description>
    ///    </item>
    ///    <item>
    ///        <term>Password</term><description>Descripcion Password</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "MembershipUserQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// MembershipUserQryDTO membershipuserqry = new MembershipUserQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("MembershipUserQry")]
    public class MembershipUserQryDP
    {
        #region Definicion de campos privados.
        private String providername;
        private String username;
        private Int32 provideruserkey;
        private String email;
        private String passwordquestion;
        private String comment;
        private Boolean isapproved;
        private Boolean islockedout;
        private String creationdate;
        private String lastlogindate;
        private String lastactivitydate;
        private String lastpasswordchangeddate;
        private String lastlockedoutdate;
        private String login;
        private String password;
        private Int32 usuarioId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// Providername
        /// </summary> 
        [XmlElement("Providername")]
        public String Providername
        {
            get {
                    return providername; 
            }
            set {
                    providername = value; 
            }
        }

        /// <summary>
        /// Username
        /// </summary> 
        [XmlElement("Username")]
        public String Username
        {
            get {
                    return username; 
            }
            set {
                    username = value; 
            }
        }

        /// <summary>
        /// Provideruserkey
        /// </summary> 
        [XmlElement("Provideruserkey")]
        public Int32 Provideruserkey
        {
            get {
                    return provideruserkey; 
            }
            set {
                    provideruserkey = value; 
            }
        }

        /// <summary>
        /// Email
        /// </summary> 
        [XmlElement("Email")]
        public String Email
        {
            get {
                    return email; 
            }
            set {
                    email = value; 
            }
        }

        /// <summary>
        /// Passwordquestion
        /// </summary> 
        [XmlElement("Passwordquestion")]
        public String Passwordquestion
        {
            get {
                    return passwordquestion; 
            }
            set {
                    passwordquestion = value; 
            }
        }

        /// <summary>
        /// Comment
        /// </summary> 
        [XmlElement("Comment")]
        public String Comment
        {
            get {
                    return comment; 
            }
            set {
                    comment = value; 
            }
        }

        /// <summary>
        /// Isapproved
        /// </summary> 
        [XmlElement("Isapproved")]
        public Boolean Isapproved
        {
            get {
                    return isapproved; 
            }
            set {
                    isapproved = value; 
            }
        }

        /// <summary>
        /// Islockedout
        /// </summary> 
        [XmlElement("Islockedout")]
        public Boolean Islockedout
        {
            get {
                    return islockedout; 
            }
            set {
                    islockedout = value; 
            }
        }

        /// <summary>
        /// Creationdate
        /// </summary> 
        [XmlElement("Creationdate")]
        public String Creationdate
        {
            get {
                    return creationdate; 
            }
            set {
                    creationdate = value; 
            }
        }

        /// <summary>
        /// Lastlogindate
        /// </summary> 
        [XmlElement("Lastlogindate")]
        public String Lastlogindate
        {
            get {
                    return lastlogindate; 
            }
            set {
                    lastlogindate = value; 
            }
        }

        /// <summary>
        /// Lastactivitydate
        /// </summary> 
        [XmlElement("Lastactivitydate")]
        public String Lastactivitydate
        {
            get {
                    return lastactivitydate; 
            }
            set {
                    lastactivitydate = value; 
            }
        }

        /// <summary>
        /// Lastpasswordchangeddate
        /// </summary> 
        [XmlElement("Lastpasswordchangeddate")]
        public String Lastpasswordchangeddate
        {
            get {
                    return lastpasswordchangeddate; 
            }
            set {
                    lastpasswordchangeddate = value; 
            }
        }

        /// <summary>
        /// Lastlockedoutdate
        /// </summary> 
        [XmlElement("Lastlockedoutdate")]
        public String Lastlockedoutdate
        {
            get {
                    return lastlockedoutdate; 
            }
            set {
                    lastlockedoutdate = value; 
            }
        }

        /// <summary>
        /// Login
        /// </summary> 
        [XmlElement("Login")]
        public String Login
        {
            get {
                    return login; 
            }
            set {
                    login = value; 
            }
        }

        /// <summary>
        /// Password
        /// </summary> 
        [XmlElement("Password")]
        public String Password
        {
            get {
                    return password; 
            }
            set {
                    password = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        #endregion.
    }
}
