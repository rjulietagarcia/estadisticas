using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class MembershipUserQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = MembershipUserQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un MembershipUserQry!", ex);
            }
            return resultado;
        }

        public static MembershipUserQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            MembershipUserQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MembershipUserQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de MembershipUserQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuario(DbConnection con, String aLogin) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = MembershipUserQryMD.CountForUsuario(con, tran, aLogin);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un MembershipUserQry!", ex);
            }
            return resultado;
        }

        public static MembershipUserQryDP[] LoadListForUsuario(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aLogin) 
        {
            MembershipUserQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MembershipUserQryMD.ListForUsuario(con, tran, startRowIndex, maximumRows, aLogin);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de MembershipUserQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuarioPassword(DbConnection con, String aLogin, String aPassword) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = MembershipUserQryMD.CountForUsuarioPassword(con, tran, aLogin, aPassword);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un MembershipUserQry!", ex);
            }
            return resultado;
        }

        public static MembershipUserQryDP[] LoadListForUsuarioPassword(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aLogin, String aPassword) 
        {
            MembershipUserQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MembershipUserQryMD.ListForUsuarioPassword(con, tran, startRowIndex, maximumRows, aLogin, aPassword);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de MembershipUserQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForSid(DbConnection con, Int32 aIdUsuario) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = MembershipUserQryMD.CountForSid(con, tran, aIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un MembershipUserQry!", ex);
            }
            return resultado;
        }

        public static MembershipUserQryDP[] LoadListForSid(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aIdUsuario) 
        {
            MembershipUserQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = MembershipUserQryMD.ListForSid(con, tran, startRowIndex, maximumRows, aIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de MembershipUserQry!", ex);
            }
            return resultado;

        }
    }
}
