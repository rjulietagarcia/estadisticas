using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class ZonaQryMD
    {
        private ZonaQryDP zonaQry = null;

        public ZonaQryMD(ZonaQryDP zonaQry)
        {
            this.zonaQry = zonaQry;
        }

        protected static ZonaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ZonaQryDP zonaQry = new ZonaQryDP();
            zonaQry.ZonaId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            zonaQry.TipoeducacionId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            zonaQry.NivelId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            zonaQry.Numerozona = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            zonaQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            zonaQry.Clave = dr.IsDBNull(5) ? "" : dr.GetString(5);
            zonaQry.SostenimientoId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            zonaQry.BitActivo = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            zonaQry.UsuarioId = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            zonaQry.FechaActualizacion = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            return zonaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count ZonaQry
            DbCommand com = con.CreateCommand();
            String countZonaQry = String.Format(CultureInfo.CurrentCulture,Zona.ZonaQry.ZonaQryCount,"");
            com.CommandText = countZonaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count ZonaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static ZonaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List ZonaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listZonaQry = String.Format(CultureInfo.CurrentCulture, Zona.ZonaQry.ZonaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listZonaQry = listZonaQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listZonaQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load ZonaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ZonaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ZonaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (ZonaQryDP[])lista.ToArray(typeof(ZonaQryDP));
        }
        public static Int64 CountForDescripcionyidsostenimiento(DbConnection con, DbTransaction tran, String aNombre, Byte aIdSostenimiento)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcionyidsostenimiento ZonaQry
            DbCommand com = con.CreateCommand();
            String countZonaQryForDescripcionyidsostenimiento = String.Format(CultureInfo.CurrentCulture,Zona.ZonaQry.ZonaQryCount,"");
            countZonaQryForDescripcionyidsostenimiento += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countZonaQryForDescripcionyidsostenimiento += String.Format("    {1} nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countZonaQryForDescripcionyidsostenimiento += String.Format("    {1} id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countZonaQryForDescripcionyidsostenimiento;
            #endregion
            #region Parametros countZonaQryForDescripcionyidsostenimiento
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcionyidsostenimiento ZonaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static ZonaQryDP[] ListForDescripcionyidsostenimiento(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Byte aIdSostenimiento)
        {
            #region SQL List ZonaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listZonaQryForDescripcionyidsostenimiento = String.Format(CultureInfo.CurrentCulture, Zona.ZonaQry.ZonaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listZonaQryForDescripcionyidsostenimiento = listZonaQryForDescripcionyidsostenimiento.Substring(6);
            listZonaQryForDescripcionyidsostenimiento += " WHERE \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listZonaQryForDescripcionyidsostenimiento += String.Format("    {1} nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listZonaQryForDescripcionyidsostenimiento += String.Format("    {1} id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listZonaQryForDescripcionyidsostenimiento, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load ZonaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ZonaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ZonaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (ZonaQryDP[])lista.ToArray(typeof(ZonaQryDP));
        }
    }
}
