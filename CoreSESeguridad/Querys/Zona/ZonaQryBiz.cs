using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class ZonaQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ZonaQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ZonaQry!", ex);
            }
            return resultado;
        }

        public static ZonaQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            ZonaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ZonaQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ZonaQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcionyidsostenimiento(DbConnection con, String aNombre, Byte aIdSostenimiento) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ZonaQryMD.CountForDescripcionyidsostenimiento(con, tran, aNombre, aIdSostenimiento);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ZonaQry!", ex);
            }
            return resultado;
        }

        public static ZonaQryDP[] LoadListForDescripcionyidsostenimiento(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Byte aIdSostenimiento) 
        {
            ZonaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ZonaQryMD.ListForDescripcionyidsostenimiento(con, tran, startRowIndex, maximumRows, aNombre, aIdSostenimiento);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ZonaQry!", ex);
            }
            return resultado;

        }
    }
}
