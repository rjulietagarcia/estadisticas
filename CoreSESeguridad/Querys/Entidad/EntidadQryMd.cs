using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class EntidadQryMD
    {
        private EntidadQryDP entidadQry = null;

        public EntidadQryMD(EntidadQryDP entidadQry)
        {
            this.entidadQry = entidadQry;
        }

        protected static EntidadQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            EntidadQryDP entidadQry = new EntidadQryDP();
            entidadQry.PaisId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            entidadQry.EntidadId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            entidadQry.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            entidadQry.Abreviatura = dr.IsDBNull(3) ? "" : dr.GetString(3);
            entidadQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            entidadQry.UsuarioId = dr.IsDBNull(5) ? 0 : dr.GetInt32(5);
            entidadQry.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            return entidadQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count EntidadQry
            DbCommand com = con.CreateCommand();
            String countEntidadQry = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadQry.EntidadQryCount,"");
            com.CommandText = countEntidadQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count EntidadQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountEntidadQry = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadQry.EntidadQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountEntidadQry);
                #endregion
            }
            return resultado;
        }
        public static EntidadQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List EntidadQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listEntidadQry = String.Format(CultureInfo.CurrentCulture, Entidad.EntidadQry.EntidadQrySelect, "", ConstantesGlobales.IncluyeRows);
            listEntidadQry = listEntidadQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listEntidadQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load EntidadQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista EntidadQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista EntidadQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaEntidadQry = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadQry.EntidadQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaEntidadQry);
                #endregion
            }
            return (EntidadQryDP[])lista.ToArray(typeof(EntidadQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, Int16 aIdPais, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion EntidadQry
            DbCommand com = con.CreateCommand();
            String countEntidadQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadQry.EntidadQryCount,"");
            countEntidadQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aIdPais != -1)
            {
                countEntidadQryForDescripcion += String.Format("    {1} Id_Pais = {0}IdPais\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre != "")
            {
                countEntidadQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countEntidadQryForDescripcion;
            #endregion
            #region Parametros countEntidadQryForDescripcion
            if (aIdPais != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPais);
            }
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion EntidadQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountEntidadQryForDescripcion = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadQry.EntidadQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountEntidadQryForDescripcion);
                #endregion
            }
            return resultado;
        }
        public static EntidadQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdPais, String aNombre)
        {
            #region SQL List EntidadQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listEntidadQryForDescripcion = String.Format(CultureInfo.CurrentCulture, Entidad.EntidadQry.EntidadQrySelect, "", ConstantesGlobales.IncluyeRows);
            listEntidadQryForDescripcion = listEntidadQryForDescripcion.Substring(6);
            listEntidadQryForDescripcion += " WHERE \n";
            String delimitador = "";
            if (aIdPais != -1)
            {
                listEntidadQryForDescripcion += String.Format("    {1} Id_Pais = {0}IdPais\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre != "")
            {
                listEntidadQryForDescripcion += String.Format("    {1} Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listEntidadQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load EntidadQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdPais != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdPais",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdPais);
            }
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista EntidadQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista EntidadQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaEntidadQry = String.Format(CultureInfo.CurrentCulture,Entidad.EntidadQry.EntidadQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaEntidadQry);
                #endregion
            }
            return (EntidadQryDP[])lista.ToArray(typeof(EntidadQryDP));
        }
    }
}
