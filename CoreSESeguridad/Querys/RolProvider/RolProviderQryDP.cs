using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "RolProviderQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 10:54:48 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "RolProviderQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>Firstname</term><description>Descripcion Firstname</description>
    ///    </item>
    ///    <item>
    ///        <term>Lastname</term><description>Descripcion Lastname</description>
    ///    </item>
    ///    <item>
    ///        <term>Rol</term><description>Descripcion Rol</description>
    ///    </item>
    ///    <item>
    ///        <term>Login</term><description>Descripcion Login</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "RolProviderQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// RolProviderQryDTO rolproviderqry = new RolProviderQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("RolProviderQry")]
    public class RolProviderQryDP
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private Int32 usuarioId;
        private String firstname;
        private String lastname;
        private String rol;
        private String login;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// Firstname
        /// </summary> 
        [XmlElement("Firstname")]
        public String Firstname
        {
            get {
                    return firstname; 
            }
            set {
                    firstname = value; 
            }
        }

        /// <summary>
        /// Lastname
        /// </summary> 
        [XmlElement("Lastname")]
        public String Lastname
        {
            get {
                    return lastname; 
            }
            set {
                    lastname = value; 
            }
        }

        /// <summary>
        /// Rol
        /// </summary> 
        [XmlElement("Rol")]
        public String Rol
        {
            get {
                    return rol; 
            }
            set {
                    rol = value; 
            }
        }

        /// <summary>
        /// Login
        /// </summary> 
        [XmlElement("Login")]
        public String Login
        {
            get {
                    return login; 
            }
            set {
                    login = value; 
            }
        }

        #endregion.
    }
}
