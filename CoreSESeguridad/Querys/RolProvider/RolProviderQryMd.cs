using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class RolProviderQryMD
    {
        private RolProviderQryDP rolProviderQry = null;

        public RolProviderQryMD(RolProviderQryDP rolProviderQry)
        {
            this.rolProviderQry = rolProviderQry;
        }

        protected static RolProviderQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            RolProviderQryDP rolProviderQry = new RolProviderQryDP();
            rolProviderQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            rolProviderQry.UsuarioId = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            rolProviderQry.Firstname = dr.IsDBNull(2) ? "" : dr.GetString(2);
            rolProviderQry.Lastname = dr.IsDBNull(3) ? "" : dr.GetString(3);
            rolProviderQry.Rol = dr.IsDBNull(4) ? "" : dr.GetString(4);
            rolProviderQry.Login = dr.IsDBNull(5) ? "" : dr.GetString(5);
            return rolProviderQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count RolProviderQry
            DbCommand com = con.CreateCommand();
            String countRolProviderQry = String.Format(CultureInfo.CurrentCulture,RolProvider.RolProviderQry.RolProviderQryCount,"");
            com.CommandText = countRolProviderQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count RolProviderQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountRolProviderQry = String.Format(CultureInfo.CurrentCulture,RolProvider.RolProviderQry.RolProviderQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountRolProviderQry);
                #endregion
            }
            return resultado;
        }
        public static RolProviderQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List RolProviderQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listRolProviderQry = String.Format(CultureInfo.CurrentCulture, RolProvider.RolProviderQry.RolProviderQrySelect, "", ConstantesGlobales.IncluyeRows);
            listRolProviderQry = listRolProviderQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listRolProviderQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load RolProviderQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista RolProviderQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista RolProviderQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaRolProviderQry = String.Format(CultureInfo.CurrentCulture,RolProvider.RolProviderQry.RolProviderQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaRolProviderQry);
                #endregion
            }
            return (RolProviderQryDP[])lista.ToArray(typeof(RolProviderQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, String aLogin)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario RolProviderQry
            DbCommand com = con.CreateCommand();
            String countRolProviderQryForUsuario = String.Format(CultureInfo.CurrentCulture,RolProvider.RolProviderQry.RolProviderQryCount,"");
            countRolProviderQryForUsuario += " WHERE \n";
            String delimitador = "";
            countRolProviderQryForUsuario += String.Format("    {1} u.Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countRolProviderQryForUsuario;
            #endregion
            #region Parametros countRolProviderQryForUsuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario RolProviderQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountRolProviderQryForUsuario = String.Format(CultureInfo.CurrentCulture,RolProvider.RolProviderQry.RolProviderQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountRolProviderQryForUsuario);
                #endregion
            }
            return resultado;
        }
        public static RolProviderQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aLogin)
        {
            #region SQL List RolProviderQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listRolProviderQryForUsuario = String.Format(CultureInfo.CurrentCulture, RolProvider.RolProviderQry.RolProviderQrySelect, "", ConstantesGlobales.IncluyeRows);
            listRolProviderQryForUsuario = listRolProviderQryForUsuario.Substring(6);
            listRolProviderQryForUsuario += " WHERE \n";
            String delimitador = "";
            listRolProviderQryForUsuario += String.Format("    {1} u.Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listRolProviderQryForUsuario += " ORDER BY ua.Id_Sistema";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listRolProviderQryForUsuario, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load RolProviderQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista RolProviderQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista RolProviderQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaRolProviderQry = String.Format(CultureInfo.CurrentCulture,RolProvider.RolProviderQry.RolProviderQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaRolProviderQry);
                #endregion
            }
            return (RolProviderQryDP[])lista.ToArray(typeof(RolProviderQryDP));
        }
    }
}
