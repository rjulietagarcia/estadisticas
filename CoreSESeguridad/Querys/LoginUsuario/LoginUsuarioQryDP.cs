using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "LoginUsuarioQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 25 de mayo de 2009.</Para>
    /// <Para>Hora: 12:57:43 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "LoginUsuarioQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveltrabajoId</term><description>Descripcion NiveltrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    ///    <item>
    ///        <term>ZonaId</term><description>Descripcion ZonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>SectorId</term><description>Descripcion SectorId</description>
    ///    </item>
    ///    <item>
    ///        <term>CentrotrabajoId</term><description>Descripcion CentrotrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>PerfilId</term><description>Descripcion PerfilId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveleducacionId</term><description>Descripcion NiveleducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Login</term><description>Descripcion Login</description>
    ///    </item>
    ///    <item>
    ///        <term>Password</term><description>Descripcion Password</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "LoginUsuarioQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// LoginUsuarioQryDTO loginusuarioqry = new LoginUsuarioQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("LoginUsuarioQry")]
    public class LoginUsuarioQryDP
    {
        #region Definicion de campos privados.
        private int usuarioId;
        private int niveltrabajoId;
        private int regionId;
        private int zonaId;
        private int sectorId;
        private int centrotrabajoId;
        private int perfilId;
        private int niveleducacionId;
        private int sostenimientoId;
        private String login;
        private String password;
        private bool bit_Activo;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public int UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// NiveltrabajoId
        /// </summary> 
        [XmlElement("NiveltrabajoId")]
        public int NiveltrabajoId
        {
            get {
                    return niveltrabajoId; 
            }
            set {
                    niveltrabajoId = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public int RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        /// <summary>
        /// ZonaId
        /// </summary> 
        [XmlElement("ZonaId")]
        public int ZonaId
        {
            get {
                    return zonaId; 
            }
            set {
                    zonaId = value; 
            }
        }

        /// <summary>
        /// SectorId
        /// </summary> 
        [XmlElement("SectorId")]
        public int SectorId
        {
            get {
                    return sectorId; 
            }
            set {
                    sectorId = value; 
            }
        }

        /// <summary>
        /// CentrotrabajoId
        /// </summary> 
        [XmlElement("CentrotrabajoId")]
        public int CentrotrabajoId
        {
            get {
                    return centrotrabajoId; 
            }
            set {
                    centrotrabajoId = value; 
            }
        }

        /// <summary>
        /// PerfilId
        /// </summary> 
        [XmlElement("PerfilId")]
        public int PerfilId
        {
            get {
                    return perfilId; 
            }
            set {
                    perfilId = value; 
            }
        }

        /// <summary>
        /// NiveleducacionId
        /// </summary> 
        [XmlElement("NiveleducacionId")]
        public int NiveleducacionId
        {
            get {
                    return niveleducacionId; 
            }
            set {
                    niveleducacionId = value; 
            }
        }

        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public int SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// Login
        /// </summary> 
        [XmlElement("Login")]
        public String Login
        {
            get {
                    return login; 
            }
            set {
                    login = value; 
            }
        }

        /// <summary>
        /// Password
        /// </summary> 
        [XmlElement("Password")]
        public String Password
        {
            get {
                    return password; 
            }
            set {
                    password = value; 
            }
        }

        /// <summary>
        /// Password
        /// </summary> 
        [XmlElement("Bit_Activo")]
        public bool Bit_Activo
        {
            get
            {
                return bit_Activo;
            }
            set
            {
                bit_Activo = value;
            }
        }
        #endregion.
    }
}
