using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class LoginUsuarioQryMD
    {
        private LoginUsuarioQryDP loginUsuarioQry = null;

        public LoginUsuarioQryMD(LoginUsuarioQryDP loginUsuarioQry)
        {
            this.loginUsuarioQry = loginUsuarioQry;
        }

        protected static LoginUsuarioQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            LoginUsuarioQryDP loginUsuarioQry = new LoginUsuarioQryDP();
            loginUsuarioQry.UsuarioId = dr.IsDBNull(0) ? 0 :        int.Parse(dr.GetValue(0).ToString());
            loginUsuarioQry.NiveltrabajoId = dr.IsDBNull(1) ? 0 :   int.Parse(dr.GetValue(1).ToString());
            loginUsuarioQry.RegionId = dr.IsDBNull(2) ? 0 :         int.Parse(dr.GetValue(2).ToString()) ;
            loginUsuarioQry.ZonaId = dr.IsDBNull(3) ? 0 :           int.Parse(dr.GetValue(3).ToString());
            loginUsuarioQry.SectorId = dr.IsDBNull(4) ? 0 :         int.Parse(dr.GetValue(4).ToString());
            loginUsuarioQry.CentrotrabajoId = dr.IsDBNull(5) ? 0 :  int.Parse(dr.GetValue(5).ToString());
            loginUsuarioQry.PerfilId = dr.IsDBNull(6) ? 0 :         int.Parse(dr.GetValue(5).ToString());
            loginUsuarioQry.NiveleducacionId = dr.IsDBNull(7) ? 0 : int.Parse(dr.GetValue(7).ToString());
            loginUsuarioQry.SostenimientoId = dr.IsDBNull(8) ? 0 :  int.Parse(dr.GetValue(8).ToString());
            loginUsuarioQry.Login = dr.IsDBNull(9) ? "" : dr.GetString(9);
            loginUsuarioQry.Password = dr.IsDBNull(10) ? "" : dr.GetString(10);
            loginUsuarioQry.Bit_Activo = dr.IsDBNull(11) ? bool.Parse("0") : bool.Parse(dr.GetValue(11).ToString());
            return loginUsuarioQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count LoginUsuarioQry
            DbCommand com = con.CreateCommand();
            String countLoginUsuarioQry = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQryCount,"");
            com.CommandText = countLoginUsuarioQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count LoginUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountLoginUsuarioQry = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountLoginUsuarioQry);
                #endregion
            }
            return resultado;
        }
        public static LoginUsuarioQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List LoginUsuarioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listLoginUsuarioQry = String.Format(CultureInfo.CurrentCulture, LoginUsuario.LoginUsuarioQry.LoginUsuarioQrySelect, "", ConstantesGlobales.IncluyeRows);
            listLoginUsuarioQry = listLoginUsuarioQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listLoginUsuarioQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load LoginUsuarioQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista LoginUsuarioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista LoginUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaLoginUsuarioQry = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaLoginUsuarioQry);
                #endregion
            }
            return (LoginUsuarioQryDP[])lista.ToArray(typeof(LoginUsuarioQryDP));
        }
        public static Int64 CountForUsuario(DbConnection con, DbTransaction tran, Int32 aIdUsuario)
        {
            Int64 resultado = 0;
            #region SQL CountForUsuario LoginUsuarioQry
            DbCommand com = con.CreateCommand();
            String countLoginUsuarioQryForUsuario = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQryCount,"");
            countLoginUsuarioQryForUsuario += " AND \n";
            String delimitador = "";
            countLoginUsuarioQryForUsuario += String.Format("    {1} u.Id_Usuario = {0}IdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countLoginUsuarioQryForUsuario;
            #endregion
            #region Parametros countLoginUsuarioQryForUsuario
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForUsuario LoginUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountLoginUsuarioQryForUsuario = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountLoginUsuarioQryForUsuario);
                #endregion
            }
            return resultado;
        }
        public static LoginUsuarioQryDP[] ListForUsuario(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aIdUsuario)
        {
            #region SQL List LoginUsuarioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listLoginUsuarioQryForUsuario = String.Format(CultureInfo.CurrentCulture, LoginUsuario.LoginUsuarioQry.LoginUsuarioQrySelect, "", ConstantesGlobales.IncluyeRows);
            listLoginUsuarioQryForUsuario = listLoginUsuarioQryForUsuario.Substring(6);
            listLoginUsuarioQryForUsuario += " AND \n";
            String delimitador = "";
            listLoginUsuarioQryForUsuario += String.Format("    {1} u.Id_Usuario = {0}IdUsuario\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listLoginUsuarioQryForUsuario, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load LoginUsuarioQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdUsuario",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdUsuario);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista LoginUsuarioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista LoginUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaLoginUsuarioQry = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaLoginUsuarioQry);
                #endregion
            }
            return (LoginUsuarioQryDP[])lista.ToArray(typeof(LoginUsuarioQryDP));
        }
        public static Int64 CountForLoginPassword(DbConnection con, DbTransaction tran, String aLogin, String aPassword)
        {
            Int64 resultado = 0;
            #region SQL CountForLoginPassword LoginUsuarioQry
            DbCommand com = con.CreateCommand();
            String countLoginUsuarioQryForLoginPassword = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQryCount,"");
            countLoginUsuarioQryForLoginPassword += " AND \n";
            String delimitador = "";
            countLoginUsuarioQryForLoginPassword += String.Format("    {1} u.Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countLoginUsuarioQryForLoginPassword += String.Format("    {1} u.Password = {0}Password\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countLoginUsuarioQryForLoginPassword;
            #endregion
            #region Parametros countLoginUsuarioQryForLoginPassword
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Password",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aPassword);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLoginPassword LoginUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountLoginUsuarioQryForLoginPassword = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountLoginUsuarioQryForLoginPassword);
                #endregion
            }
            return resultado;
        }
        public static LoginUsuarioQryDP[] ListForLoginPassword(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aLogin, String aPassword)
        {
            #region SQL List LoginUsuarioQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listLoginUsuarioQryForLoginPassword = String.Format(CultureInfo.CurrentCulture, LoginUsuario.LoginUsuarioQry.LoginUsuarioQrySelect, "", ConstantesGlobales.IncluyeRows);
            listLoginUsuarioQryForLoginPassword = listLoginUsuarioQryForLoginPassword.Substring(6);
            listLoginUsuarioQryForLoginPassword += " AND \n";
            String delimitador = "";
            listLoginUsuarioQryForLoginPassword += String.Format("    {1} u.Login = {0}Login\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listLoginUsuarioQryForLoginPassword += String.Format("    {1} u.Password = {0}Password\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            //delimitador = " AND ";
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listLoginUsuarioQryForLoginPassword, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            //com.CommandText = filtraList;
            com.CommandText = "SELECT  "+ listLoginUsuarioQryForLoginPassword;
            DbDataReader dr;
            #endregion
            #region Parametros Load LoginUsuarioQry
           // Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
           // Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Login",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aLogin);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Password",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 30, ParameterDirection.Input, aPassword);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista LoginUsuarioQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista LoginUsuarioQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaLoginUsuarioQry = String.Format(CultureInfo.CurrentCulture,LoginUsuario.LoginUsuarioQry.LoginUsuarioQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaLoginUsuarioQry);
                #endregion
            }
            return (LoginUsuarioQryDP[])lista.ToArray(typeof(LoginUsuarioQryDP));
        }
    }
}
