using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class LoginUsuarioQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = LoginUsuarioQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un LoginUsuarioQry!", ex);
            }
            return resultado;
        }

        public static LoginUsuarioQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            LoginUsuarioQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = LoginUsuarioQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de LoginUsuarioQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForUsuario(DbConnection con, Int32 aIdUsuario) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = LoginUsuarioQryMD.CountForUsuario(con, tran, aIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un LoginUsuarioQry!", ex);
            }
            return resultado;
        }

        public static LoginUsuarioQryDP[] LoadListForUsuario(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aIdUsuario) 
        {
            LoginUsuarioQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = LoginUsuarioQryMD.ListForUsuario(con, tran, startRowIndex, maximumRows, aIdUsuario);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de LoginUsuarioQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLoginPassword(DbConnection con, String aLogin, String aPassword) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = LoginUsuarioQryMD.CountForLoginPassword(con, tran, aLogin, aPassword);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un LoginUsuarioQry!", ex);
            }
            return resultado;
        }

        public static LoginUsuarioQryDP[] LoadListForLoginPassword(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aLogin, String aPassword) 
        {
            LoginUsuarioQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = LoginUsuarioQryMD.ListForLoginPassword(con, tran, startRowIndex, maximumRows, aLogin, aPassword);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de LoginUsuarioQry!", ex);
            }
            return resultado;

        }
    }
}
