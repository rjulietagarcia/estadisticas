using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class FltListaUsuariosQryMD
    {
        private FltListaUsuariosQryDP fltListaUsuariosQry = null;

        public FltListaUsuariosQryMD(FltListaUsuariosQryDP fltListaUsuariosQry)
        {
            this.fltListaUsuariosQry = fltListaUsuariosQry;
        }

        protected static FltListaUsuariosQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            FltListaUsuariosQryDP fltListaUsuariosQry = new FltListaUsuariosQryDP();
            fltListaUsuariosQry.UsuarioId = dr.IsDBNull(0) ? 0 : dr.GetInt32(0);
            fltListaUsuariosQry.Login = dr.IsDBNull(1) ? "" : dr.GetString(1);
            fltListaUsuariosQry.Nombre = dr.IsDBNull(2) ? "" : dr.GetString(2);
            fltListaUsuariosQry.ApellidoPaterno = dr.IsDBNull(3) ? "" : dr.GetString(3);
            fltListaUsuariosQry.ApellidoMaterno = dr.IsDBNull(4) ? "" : dr.GetString(4);
            fltListaUsuariosQry.FechaNacimiento = dr.IsDBNull(5) ? "" : dr.GetDateTime(5).ToShortDateString();;
            fltListaUsuariosQry.NiveleducacionId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            fltListaUsuariosQry.RegionId = dr.IsDBNull(7) ? (short)0 : dr.GetInt16(7);
            fltListaUsuariosQry.ZonaId = dr.IsDBNull(8) ? (short)0 : dr.GetInt16(8);
            fltListaUsuariosQry.CctntId = dr.IsDBNull(9) ? (short)0 : dr.GetInt16(9);
            return fltListaUsuariosQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count FltListaUsuariosQry
            DbCommand com = con.CreateCommand();
            String countFltListaUsuariosQry = String.Format(CultureInfo.CurrentCulture,FltListaUsuarios.FltListaUsuariosQry.FltListaUsuariosQryCount,"");
            com.CommandText = countFltListaUsuariosQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count FltListaUsuariosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltListaUsuariosQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List FltListaUsuariosQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltListaUsuariosQry = String.Format(CultureInfo.CurrentCulture, FltListaUsuarios.FltListaUsuariosQry.FltListaUsuariosQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltListaUsuariosQry = listFltListaUsuariosQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltListaUsuariosQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltListaUsuariosQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltListaUsuariosQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltListaUsuariosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltListaUsuariosQryDP[])lista.ToArray(typeof(FltListaUsuariosQryDP));
        }
        public static Int64 CountForFiltro(DbConnection con, DbTransaction tran, Byte aIdNiveleducacion, Int16 aIdRegion, Int16 aIdZona, Int16 aIdCctnt, String aApellidoPaterno, String aApellidoMaterno, String aNombre)
        {
            Int64 resultado = 0;
            #region SQL CountForFiltro FltListaUsuariosQry
            DbCommand com = con.CreateCommand();
            String countFltListaUsuariosQryForFiltro = String.Format(CultureInfo.CurrentCulture,FltListaUsuarios.FltListaUsuariosQry.FltListaUsuariosQryCount,"");
            countFltListaUsuariosQryForFiltro += " WHERE \n";
            String delimitador = "";
            if (aIdNiveleducacion != 0)
            {
                countFltListaUsuariosQryForFiltro += String.Format("    {1} ct.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != 0)
            {
                countFltListaUsuariosQryForFiltro += String.Format("    {1} ult.Id_Region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                countFltListaUsuariosQryForFiltro += String.Format("    {1} ult.Id_Zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdCctnt != -1)
            {
                countFltListaUsuariosQryForFiltro += String.Format("    {1} ct.id_CCTNT = {0}IdCctnt\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aApellidoPaterno.Length != 0)
            {
                countFltListaUsuariosQryForFiltro += String.Format("    {1} p.Apellido_Paterno LIKE {0}ApellidoPaterno\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aApellidoMaterno.Length != 0)
            {
                countFltListaUsuariosQryForFiltro += String.Format("    {1} p.Apellido_Materno LIKE {0}ApellidoMaterno\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre.Length != 0)
            {
                countFltListaUsuariosQryForFiltro += String.Format("    {1} p.Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countFltListaUsuariosQryForFiltro;
            #endregion
            #region Parametros countFltListaUsuariosQryForFiltro
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            if (aIdRegion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            }
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            if (aIdCctnt != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCctnt",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCctnt);
            }
            if (aApellidoPaterno.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}ApellidoPaterno",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aApellidoPaterno);
            }
            if (aApellidoMaterno.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}ApellidoMaterno",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aApellidoMaterno);
            }
            if (aNombre.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForFiltro FltListaUsuariosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltListaUsuariosQryDP[] ListForFiltro(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Int16 aIdZona, Int16 aIdCctnt, String aApellidoPaterno, String aApellidoMaterno, String aNombre)
        {
            #region SQL List FltListaUsuariosQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltListaUsuariosQryForFiltro = String.Format(CultureInfo.CurrentCulture, FltListaUsuarios.FltListaUsuariosQry.FltListaUsuariosQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltListaUsuariosQryForFiltro = listFltListaUsuariosQryForFiltro.Substring(6);
            listFltListaUsuariosQryForFiltro += " WHERE \n";
            String delimitador = "";
            if (aIdNiveleducacion != 0)
            {
                listFltListaUsuariosQryForFiltro += String.Format("    {1} ct.Id_NivelEducacion = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != 0)
            {
                listFltListaUsuariosQryForFiltro += String.Format("    {1} ult.Id_Region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                listFltListaUsuariosQryForFiltro += String.Format("    {1} ult.Id_Zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdCctnt != -1)
            {
                listFltListaUsuariosQryForFiltro += String.Format("    {1} ct.id_CCTNT = {0}IdCctnt\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aApellidoPaterno.Length != 0)
            {
                listFltListaUsuariosQryForFiltro += String.Format("    {1} p.Apellido_Paterno LIKE {0}ApellidoPaterno\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aApellidoMaterno.Length != 0)
            {
                listFltListaUsuariosQryForFiltro += String.Format("    {1} p.Apellido_Materno LIKE {0}ApellidoMaterno\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombre.Length != 0)
            {
                listFltListaUsuariosQryForFiltro += String.Format("    {1} p.Nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltListaUsuariosQryForFiltro, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltListaUsuariosQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdNiveleducacion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            }
            if (aIdRegion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            }
            if (aIdZona != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            }
            if (aIdCctnt != -1)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdCctnt",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdCctnt);
            }
            if (aApellidoPaterno.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}ApellidoPaterno",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aApellidoPaterno);
            }
            if (aApellidoMaterno.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}ApellidoMaterno",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 50, ParameterDirection.Input, aApellidoMaterno);
            }
            if (aNombre.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltListaUsuariosQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltListaUsuariosQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltListaUsuariosQryDP[])lista.ToArray(typeof(FltListaUsuariosQryDP));
        }
    }
}
