using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class FltListaUsuariosQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltListaUsuariosQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltListaUsuariosQry!", ex);
            }
            return resultado;
        }

        public static FltListaUsuariosQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            FltListaUsuariosQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltListaUsuariosQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltListaUsuariosQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForFiltro(DbConnection con, Byte aIdNiveleducacion, Int16 aIdRegion, Int16 aIdZona, Int16 aIdCctnt, String aApellidoPaterno, String aApellidoMaterno, String aNombre) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = FltListaUsuariosQryMD.CountForFiltro(con, tran, aIdNiveleducacion, aIdRegion, aIdZona, aIdCctnt, aApellidoPaterno, aApellidoMaterno, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un FltListaUsuariosQry!", ex);
            }
            return resultado;
        }

        public static FltListaUsuariosQryDP[] LoadListForFiltro(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Int16 aIdZona, Int16 aIdCctnt, String aApellidoPaterno, String aApellidoMaterno, String aNombre) 
        {
            FltListaUsuariosQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = FltListaUsuariosQryMD.ListForFiltro(con, tran, startRowIndex, maximumRows, aIdNiveleducacion, aIdRegion, aIdZona, aIdCctnt, aApellidoPaterno, aApellidoMaterno, aNombre);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de FltListaUsuariosQry!", ex);
            }
            return resultado;

        }
    }
}
