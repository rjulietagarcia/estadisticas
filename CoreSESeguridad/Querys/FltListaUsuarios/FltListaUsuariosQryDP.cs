using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "FltListaUsuariosQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 21 de julio de 2009.</Para>
    /// <Para>Hora: 12:47:49 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "FltListaUsuariosQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>Login</term><description>Descripcion Login</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombre</term><description>Descripcion Nombre</description>
    ///    </item>
    ///    <item>
    ///        <term>ApellidoPaterno</term><description>Descripcion ApellidoPaterno</description>
    ///    </item>
    ///    <item>
    ///        <term>ApellidoMaterno</term><description>Descripcion ApellidoMaterno</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaNacimiento</term><description>Descripcion FechaNacimiento</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveleducacionId</term><description>Descripcion NiveleducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    ///    <item>
    ///        <term>ZonaId</term><description>Descripcion ZonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>CctntId</term><description>Descripcion CctntId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "FltListaUsuariosQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// FltListaUsuariosQryDTO fltlistausuariosqry = new FltListaUsuariosQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("FltListaUsuariosQry")]
    public class FltListaUsuariosQryDP
    {
        #region Definicion de campos privados.
        private Int32 usuarioId;
        private String login;
        private String nombre;
        private String apellidoPaterno;
        private String apellidoMaterno;
        private String fechaNacimiento;
        private Byte niveleducacionId;
        private Int16 regionId;
        private Int16 zonaId;
        private Int16 cctntId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// Login
        /// </summary> 
        [XmlElement("Login")]
        public String Login
        {
            get {
                    return login; 
            }
            set {
                    login = value; 
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get {
                    return nombre; 
            }
            set {
                    nombre = value; 
            }
        }

        /// <summary>
        /// ApellidoPaterno
        /// </summary> 
        [XmlElement("ApellidoPaterno")]
        public String ApellidoPaterno
        {
            get {
                    return apellidoPaterno; 
            }
            set {
                    apellidoPaterno = value; 
            }
        }

        /// <summary>
        /// ApellidoMaterno
        /// </summary> 
        [XmlElement("ApellidoMaterno")]
        public String ApellidoMaterno
        {
            get {
                    return apellidoMaterno; 
            }
            set {
                    apellidoMaterno = value; 
            }
        }

        /// <summary>
        /// FechaNacimiento
        /// </summary> 
        [XmlElement("FechaNacimiento")]
        public String FechaNacimiento
        {
            get {
                    return fechaNacimiento; 
            }
            set {
                    fechaNacimiento = value; 
            }
        }

        /// <summary>
        /// NiveleducacionId
        /// </summary> 
        [XmlElement("NiveleducacionId")]
        public Byte NiveleducacionId
        {
            get {
                    return niveleducacionId; 
            }
            set {
                    niveleducacionId = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public Int16 RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        /// <summary>
        /// ZonaId
        /// </summary> 
        [XmlElement("ZonaId")]
        public Int16 ZonaId
        {
            get {
                    return zonaId; 
            }
            set {
                    zonaId = value; 
            }
        }

        /// <summary>
        /// CctntId
        /// </summary> 
        [XmlElement("CctntId")]
        public Int16 CctntId
        {
            get {
                    return cctntId; 
            }
            set {
                    cctntId = value; 
            }
        }

        #endregion.
    }
}
