using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class FltNivelEducacionQryMD
    {
        private FltNivelEducacionQryDP fltNivelEducacionQry = null;

        public FltNivelEducacionQryMD(FltNivelEducacionQryDP fltNivelEducacionQry)
        {
            this.fltNivelEducacionQry = fltNivelEducacionQry;
        }

        protected static FltNivelEducacionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            FltNivelEducacionQryDP fltNivelEducacionQry = new FltNivelEducacionQryDP();
            fltNivelEducacionQry.SistemaId = Convert.ToInt16(dr.IsDBNull(0) ? 0 : dr.GetInt32(0));
            fltNivelEducacionQry.ModuloId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            fltNivelEducacionQry.OpcionId = Convert.ToInt16(dr.IsDBNull(2) ? 0 : dr.GetInt32(2));
            fltNivelEducacionQry.AccionId = Convert.ToInt16(dr.IsDBNull(3) ? 0 : dr.GetInt32(3));
            fltNivelEducacionQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            return fltNivelEducacionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count FltNivelEducacionQry
            DbCommand com = con.CreateCommand();
            String countFltNivelEducacionQry = String.Format(CultureInfo.CurrentCulture,FltNivelEducacion.FltNivelEducacionQry.FltNivelEducacionQryCount,"");
            com.CommandText = countFltNivelEducacionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count FltNivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static FltNivelEducacionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List FltNivelEducacionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listFltNivelEducacionQry = String.Format(CultureInfo.CurrentCulture, FltNivelEducacion.FltNivelEducacionQry.FltNivelEducacionQrySelect, "", ConstantesGlobales.IncluyeRows);
            listFltNivelEducacionQry = listFltNivelEducacionQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listFltNivelEducacionQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load FltNivelEducacionQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista FltNivelEducacionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista FltNivelEducacionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (FltNivelEducacionQryDP[])lista.ToArray(typeof(FltNivelEducacionQryDP));
        }
    }
}
