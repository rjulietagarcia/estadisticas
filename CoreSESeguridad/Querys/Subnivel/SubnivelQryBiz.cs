using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class SubnivelQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SubnivelQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un SubnivelQry!", ex);
            }
            return resultado;
        }

        public static SubnivelQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            SubnivelQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SubnivelQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de SubnivelQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForNiveleducacion(DbConnection con, Int16 aIdNivel) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = SubnivelQryMD.CountForNiveleducacion(con, tran, aIdNivel);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un SubnivelQry!", ex);
            }
            return resultado;
        }

        public static SubnivelQryDP[] LoadListForNiveleducacion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int16 aIdNivel) 
        {
            SubnivelQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = SubnivelQryMD.ListForNiveleducacion(con, tran, startRowIndex, maximumRows, aIdNivel);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de SubnivelQry!", ex);
            }
            return resultado;

        }
    }
}
