using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class SubnivelQryMD
    {
        private SubnivelQryDP subnivelQry = null;

        public SubnivelQryMD(SubnivelQryDP subnivelQry)
        {
            this.subnivelQry = subnivelQry;
        }

        protected static SubnivelQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            SubnivelQryDP subnivelQry = new SubnivelQryDP();
            subnivelQry.TipoeducacionId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            subnivelQry.NivelId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            subnivelQry.SubnivelId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            subnivelQry.Nombre = dr.IsDBNull(3) ? "" : dr.GetString(3);
            subnivelQry.BitActivo = dr.IsDBNull(4) ? false : dr.GetBoolean(4);
            subnivelQry.UsuarioId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            subnivelQry.FechaActualizacion = dr.IsDBNull(6) ? "" : dr.GetDateTime(6).ToShortDateString();;
            return subnivelQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count SubnivelQry
            DbCommand com = con.CreateCommand();
            String countSubnivelQry = String.Format(CultureInfo.CurrentCulture,Subnivel.SubnivelQry.SubnivelQryCount,"");
            com.CommandText = countSubnivelQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count SubnivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SubnivelQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List SubnivelQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSubnivelQry = String.Format(CultureInfo.CurrentCulture, Subnivel.SubnivelQry.SubnivelQrySelect, "", ConstantesGlobales.IncluyeRows);
            listSubnivelQry = listSubnivelQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSubnivelQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load SubnivelQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SubnivelQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SubnivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SubnivelQryDP[])lista.ToArray(typeof(SubnivelQryDP));
        }
        public static Int64 CountForNiveleducacion(DbConnection con, DbTransaction tran, Int16 aIdNivel)
        {
            Int64 resultado = 0;
            #region SQL CountForNiveleducacion SubnivelQry
            DbCommand com = con.CreateCommand();
            String countSubnivelQryForNiveleducacion = String.Format(CultureInfo.CurrentCulture,Subnivel.SubnivelQry.SubnivelQryCount,"");
            countSubnivelQryForNiveleducacion += " AND \n";
            String delimitador = "";
            countSubnivelQryForNiveleducacion += String.Format("    {1} id_nivel = {0}IdNivel\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countSubnivelQryForNiveleducacion;
            #endregion
            #region Parametros countSubnivelQryForNiveleducacion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdNivel);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForNiveleducacion SubnivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static SubnivelQryDP[] ListForNiveleducacion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int16 aIdNivel)
        {
            #region SQL List SubnivelQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listSubnivelQryForNiveleducacion = String.Format(CultureInfo.CurrentCulture, Subnivel.SubnivelQry.SubnivelQrySelect, "", ConstantesGlobales.IncluyeRows);
            listSubnivelQryForNiveleducacion = listSubnivelQryForNiveleducacion.Substring(6);
            listSubnivelQryForNiveleducacion += " AND \n";
            String delimitador = "";
            listSubnivelQryForNiveleducacion += String.Format("    {1} id_nivel = {0}IdNivel\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listSubnivelQryForNiveleducacion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load SubnivelQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdNivel);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista SubnivelQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista SubnivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (SubnivelQryDP[])lista.ToArray(typeof(SubnivelQryDP));
        }
    }
}
