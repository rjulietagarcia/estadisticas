using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "CctntBusquedaQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: martes, 30 de junio de 2009.</Para>
    /// <Para>Hora: 03:01:43 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CctntBusquedaQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>CctntId</term><description>Descripcion CctntId</description>
    ///    </item>
    ///    <item>
    ///        <term>NiveleducacionId</term><description>Descripcion NiveleducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Niveleducacion</term><description>Descripcion Niveleducacion</description>
    ///    </item>
    ///    <item>
    ///        <term>TurnoId</term><description>Descripcion TurnoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Truno</term><description>Descripcion Truno</description>
    ///    </item>
    ///    <item>
    ///        <term>CentrotrabajoId</term><description>Descripcion CentrotrabajoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nombrecct</term><description>Descripcion Nombrecct</description>
    ///    </item>
    ///    <item>
    ///        <term>Clavecct</term><description>Descripcion Clavecct</description>
    ///    </item>
    ///    <item>
    ///        <term>SostenimientoId</term><description>Descripcion SostenimientoId</description>
    ///    </item>
    ///    <item>
    ///        <term>Sostenimiento</term><description>Descripcion Sostenimiento</description>
    ///    </item>
    ///    <item>
    ///        <term>SubnivelId</term><description>Descripcion SubnivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>Subnivel</term><description>Descripcion Subnivel</description>
    ///    </item>
    ///    <item>
    ///        <term>TipoeducacionId</term><description>Descripcion TipoeducacionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Tipoeducacion</term><description>Descripcion Tipoeducacion</description>
    ///    </item>
    ///    <item>
    ///        <term>NivelId</term><description>Descripcion NivelId</description>
    ///    </item>
    ///    <item>
    ///        <term>Nivel</term><description>Descripcion Nivel</description>
    ///    </item>
    ///    <item>
    ///        <term>RegionId</term><description>Descripcion RegionId</description>
    ///    </item>
    ///    <item>
    ///        <term>Region</term><description>Descripcion Region</description>
    ///    </item>
    ///    <item>
    ///        <term>ZonaId</term><description>Descripcion ZonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Numerozona</term><description>Descripcion Numerozona</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CctntBusquedaQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// CctntBusquedaQryDTO cctntbusquedaqry = new CctntBusquedaQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("CctntBusquedaQry")]
    public class CctntBusquedaQryDP
    {
        #region Definicion de campos privados.
        private Int16 cctntId;
        private Byte niveleducacionId;
        private String niveleducacion;
        private Int16 turnoId;
        private String truno;
        private Int16 centrotrabajoId;
        private String nombrecct;
        private String clavecct;
        private Byte sostenimientoId;
        private String sostenimiento;
        private Int16 subnivelId;
        private String subnivel;
        private Int16 tipoeducacionId;
        private String tipoeducacion;
        private Int16 nivelId;
        private String nivel;
        private Int16 regionId;
        private String region;
        private Int16 zonaId;
        private Int16 numerozona;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// CctntId
        /// </summary> 
        [XmlElement("CctntId")]
        public Int16 CctntId
        {
            get {
                    return cctntId; 
            }
            set {
                    cctntId = value; 
            }
        }

        /// <summary>
        /// NiveleducacionId
        /// </summary> 
        [XmlElement("NiveleducacionId")]
        public Byte NiveleducacionId
        {
            get {
                    return niveleducacionId; 
            }
            set {
                    niveleducacionId = value; 
            }
        }

        /// <summary>
        /// Niveleducacion
        /// </summary> 
        [XmlElement("Niveleducacion")]
        public String Niveleducacion
        {
            get {
                    return niveleducacion; 
            }
            set {
                    niveleducacion = value; 
            }
        }

        /// <summary>
        /// TurnoId
        /// </summary> 
        [XmlElement("TurnoId")]
        public Int16 TurnoId
        {
            get {
                    return turnoId; 
            }
            set {
                    turnoId = value; 
            }
        }

        /// <summary>
        /// Truno
        /// </summary> 
        [XmlElement("Truno")]
        public String Truno
        {
            get {
                    return truno; 
            }
            set {
                    truno = value; 
            }
        }

        /// <summary>
        /// CentrotrabajoId
        /// </summary> 
        [XmlElement("CentrotrabajoId")]
        public Int16 CentrotrabajoId
        {
            get {
                    return centrotrabajoId; 
            }
            set {
                    centrotrabajoId = value; 
            }
        }

        /// <summary>
        /// Nombrecct
        /// </summary> 
        [XmlElement("Nombrecct")]
        public String Nombrecct
        {
            get {
                    return nombrecct; 
            }
            set {
                    nombrecct = value; 
            }
        }

        /// <summary>
        /// Clavecct
        /// </summary> 
        [XmlElement("Clavecct")]
        public String Clavecct
        {
            get {
                    return clavecct; 
            }
            set {
                    clavecct = value; 
            }
        }

        /// <summary>
        /// SostenimientoId
        /// </summary> 
        [XmlElement("SostenimientoId")]
        public Byte SostenimientoId
        {
            get {
                    return sostenimientoId; 
            }
            set {
                    sostenimientoId = value; 
            }
        }

        /// <summary>
        /// Sostenimiento
        /// </summary> 
        [XmlElement("Sostenimiento")]
        public String Sostenimiento
        {
            get {
                    return sostenimiento; 
            }
            set {
                    sostenimiento = value; 
            }
        }

        /// <summary>
        /// SubnivelId
        /// </summary> 
        [XmlElement("SubnivelId")]
        public Int16 SubnivelId
        {
            get {
                    return subnivelId; 
            }
            set {
                    subnivelId = value; 
            }
        }

        /// <summary>
        /// Subnivel
        /// </summary> 
        [XmlElement("Subnivel")]
        public String Subnivel
        {
            get {
                    return subnivel; 
            }
            set {
                    subnivel = value; 
            }
        }

        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public Int16 TipoeducacionId
        {
            get {
                    return tipoeducacionId; 
            }
            set {
                    tipoeducacionId = value; 
            }
        }

        /// <summary>
        /// Tipoeducacion
        /// </summary> 
        [XmlElement("Tipoeducacion")]
        public String Tipoeducacion
        {
            get {
                    return tipoeducacion; 
            }
            set {
                    tipoeducacion = value; 
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public Int16 NivelId
        {
            get {
                    return nivelId; 
            }
            set {
                    nivelId = value; 
            }
        }

        /// <summary>
        /// Nivel
        /// </summary> 
        [XmlElement("Nivel")]
        public String Nivel
        {
            get {
                    return nivel; 
            }
            set {
                    nivel = value; 
            }
        }

        /// <summary>
        /// RegionId
        /// </summary> 
        [XmlElement("RegionId")]
        public Int16 RegionId
        {
            get {
                    return regionId; 
            }
            set {
                    regionId = value; 
            }
        }

        /// <summary>
        /// Region
        /// </summary> 
        [XmlElement("Region")]
        public String Region
        {
            get {
                    return region; 
            }
            set {
                    region = value; 
            }
        }

        /// <summary>
        /// ZonaId
        /// </summary> 
        [XmlElement("ZonaId")]
        public Int16 ZonaId
        {
            get {
                    return zonaId; 
            }
            set {
                    zonaId = value; 
            }
        }

        /// <summary>
        /// Numerozona
        /// </summary> 
        [XmlElement("Numerozona")]
        public Int16 Numerozona
        {
            get {
                    return numerozona; 
            }
            set {
                    numerozona = value; 
            }
        }

        #endregion.
    }
}
