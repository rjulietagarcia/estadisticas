using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CctntBusquedaQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CctntBusquedaQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CctntBusquedaQry!", ex);
            }
            return resultado;
        }

        public static CctntBusquedaQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CctntBusquedaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CctntBusquedaQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CctntBusquedaQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForClaveDescripcion(DbConnection con, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, String aClavecct, String aNombrecct,int aSubnivel) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CctntBusquedaQryMD.CountForClaveDescripcion(con, tran, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona, aClavecct, aNombrecct,aSubnivel);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CctntBusquedaQry!", ex);
            }
            return resultado;
        }

        public static CctntBusquedaQryDP[] LoadListForClaveDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, String aClavecct, String aNombrecct,int aSubnivel) 
        {
            CctntBusquedaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CctntBusquedaQryMD.ListForClaveDescripcion(con, tran, startRowIndex, maximumRows, aIdNiveleducacion, aIdRegion, aIdSostenimiento, aIdZona, aClavecct, aNombrecct,aSubnivel);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CctntBusquedaQry!", ex);
            }
            return resultado;

        }
    }
}
