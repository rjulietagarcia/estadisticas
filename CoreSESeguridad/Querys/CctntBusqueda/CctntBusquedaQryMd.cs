using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CctntBusquedaQryMD
    {
        private CctntBusquedaQryDP cctntBusquedaQry = null;

        public CctntBusquedaQryMD(CctntBusquedaQryDP cctntBusquedaQry)
        {
            this.cctntBusquedaQry = cctntBusquedaQry;
        }

        protected static CctntBusquedaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CctntBusquedaQryDP cctntBusquedaQry = new CctntBusquedaQryDP();
            cctntBusquedaQry.CctntId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            cctntBusquedaQry.NiveleducacionId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            cctntBusquedaQry.Niveleducacion = dr.IsDBNull(2) ? "" : dr.GetString(2);
            cctntBusquedaQry.TurnoId = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            cctntBusquedaQry.Truno = dr.IsDBNull(4) ? "" : dr.GetString(4);
            cctntBusquedaQry.CentrotrabajoId = dr.IsDBNull(5) ? (short)0 : dr.GetInt16(5);
            cctntBusquedaQry.Nombrecct = dr.IsDBNull(6) ? "" : dr.GetString(6);
            cctntBusquedaQry.Clavecct = dr.IsDBNull(7) ? "" : dr.GetString(7);
            cctntBusquedaQry.SostenimientoId = dr.IsDBNull(8) ? (Byte)0 : dr.GetByte(8);
            cctntBusquedaQry.Sostenimiento = dr.IsDBNull(9) ? "" : dr.GetString(9);
            cctntBusquedaQry.SubnivelId = dr.IsDBNull(10) ? (short)0 : dr.GetInt16(10);
            cctntBusquedaQry.Subnivel = dr.IsDBNull(11) ? "" : dr.GetString(11);
            cctntBusquedaQry.TipoeducacionId = dr.IsDBNull(12) ? (short)0 : dr.GetInt16(12);
            cctntBusquedaQry.Tipoeducacion = dr.IsDBNull(13) ? "" : dr.GetString(13);
            cctntBusquedaQry.NivelId = dr.IsDBNull(14) ? (short)0 : dr.GetInt16(14);
            cctntBusquedaQry.Nivel = dr.IsDBNull(15) ? "" : dr.GetString(15);
            cctntBusquedaQry.RegionId = dr.IsDBNull(16) ? (short)0 : dr.GetInt16(16);
            cctntBusquedaQry.Region = dr.IsDBNull(17) ? "" : dr.GetString(17);
            cctntBusquedaQry.ZonaId = dr.IsDBNull(18) ? (short)0 : dr.GetInt16(18);
            cctntBusquedaQry.Numerozona = dr.IsDBNull(19) ? (short)0 : dr.GetInt16(19);
            return cctntBusquedaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CctntBusquedaQry
            DbCommand com = con.CreateCommand();
            String countCctntBusquedaQry = String.Format(CultureInfo.CurrentCulture,CctntBusqueda.CctntBusquedaQry.CctntBusquedaQryCount,"");
            com.CommandText = countCctntBusquedaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CctntBusquedaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CctntBusquedaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CctntBusquedaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCctntBusquedaQry = String.Format(CultureInfo.CurrentCulture, CctntBusqueda.CctntBusquedaQry.CctntBusquedaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCctntBusquedaQry = listCctntBusquedaQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCctntBusquedaQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CctntBusquedaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CctntBusquedaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CctntBusquedaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CctntBusquedaQryDP[])lista.ToArray(typeof(CctntBusquedaQryDP));
        }
        public static Int64 CountForClaveDescripcion(DbConnection con, DbTransaction tran, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, String aClavecct, String aNombrecct,int aSubnivel)
        {
            Int64 resultado = 0;
            #region SQL CountForClaveDescripcion CctntBusquedaQry
            DbCommand com = con.CreateCommand();
            String countCctntBusquedaQryForClaveDescripcion = String.Format(CultureInfo.CurrentCulture,CctntBusqueda.CctntBusquedaQry.CctntBusquedaQryCount,"");
            if((aClavecct!="") || (aNombrecct!="")||(aIdNiveleducacion!=0)||(aIdRegion!=0)||(aIdSostenimiento!=0)||(aIdZona!=-1))
            {
                countCctntBusquedaQryForClaveDescripcion += " AND \n";
            }
            String delimitador = "";
            if (aIdNiveleducacion != 0)
            {
                countCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.Id_Nivel = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != 0)
            {
                countCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                countCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                countCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aClavecct != "")
            {
                countCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.Clave LIKE {0}Clavecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombrecct != "")
            {
                countCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.Nombre LIKE {0}Nombrecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aSubnivel != 0 && aSubnivel != -1)
            {
                countCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.id_subnivel = {0}Subnivel\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCctntBusquedaQryForClaveDescripcion;
            #endregion
            #region Parametros countCctntBusquedaQryForClaveDescripcion
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}Subnivel", ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aSubnivel);
            if (aClavecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clavecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aClavecct);
            }
            if (aNombrecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombrecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombrecct);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForClaveDescripcion CctntBusquedaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CctntBusquedaQryDP[] ListForClaveDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdNiveleducacion, Int16 aIdRegion, Byte aIdSostenimiento, Int16 aIdZona, String aClavecct, String aNombrecct,int aSubnivel )
        {
            #region SQL List CctntBusquedaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCctntBusquedaQryForClaveDescripcion = String.Format(CultureInfo.CurrentCulture, CctntBusqueda.CctntBusquedaQry.CctntBusquedaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listCctntBusquedaQryForClaveDescripcion = listCctntBusquedaQryForClaveDescripcion.Substring(6);
            if ((aClavecct != "") || (aNombrecct != "") || (aIdNiveleducacion != 0) || (aIdRegion != 0) || (aIdSostenimiento != 0) || (aIdZona != -1))
            {
                listCctntBusquedaQryForClaveDescripcion += " AND ";
            }
            String delimitador = "";
            if (aIdNiveleducacion != 0)
            {
                listCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.Id_Nivel = {0}IdNiveleducacion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdRegion != 0)
            {
                listCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdSostenimiento != 0)
            {
                listCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.id_sostenimiento = {0}IdSostenimiento\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdZona != -1)
            {
                listCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.id_zona = {0}IdZona\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aClavecct != "")
            {
                listCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.Clave LIKE {0}Clavecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aNombrecct != "")
            {
                listCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.Nombre LIKE {0}Nombrecct\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aSubnivel != 0 && aSubnivel != -1)
            {
                listCctntBusquedaQryForClaveDescripcion += String.Format("    {1} ct.id_subnivel = {0}Subnivel\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listCctntBusquedaQryForClaveDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load CctntBusquedaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNiveleducacion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdNiveleducacion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSostenimiento",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSostenimiento);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdZona",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdZona);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture, "{0}Subnivel", ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aSubnivel);
            if (aClavecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Clavecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aClavecct);
            }
            if (aNombrecct != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombrecct",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombrecct);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CctntBusquedaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CctntBusquedaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CctntBusquedaQryDP[])lista.ToArray(typeof(CctntBusquedaQryDP));
        }
    }
}
