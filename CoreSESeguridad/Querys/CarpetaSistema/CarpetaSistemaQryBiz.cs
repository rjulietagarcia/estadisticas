using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class CarpetaSistemaQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CarpetaSistemaQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CarpetaSistemaQry!", ex);
            }
            return resultado;
        }

        public static CarpetaSistemaQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            CarpetaSistemaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CarpetaSistemaQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CarpetaSistemaQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForCarpeta(DbConnection con, String aCarpetaSistema) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = CarpetaSistemaQryMD.CountForCarpeta(con, tran, aCarpetaSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un CarpetaSistemaQry!", ex);
            }
            return resultado;
        }

        public static CarpetaSistemaQryDP[] LoadListForCarpeta(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aCarpetaSistema) 
        {
            CarpetaSistemaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = CarpetaSistemaQryMD.ListForCarpeta(con, tran, startRowIndex, maximumRows, aCarpetaSistema);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de CarpetaSistemaQry!", ex);
            }
            return resultado;

        }
    }
}
