using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class CarpetaSistemaQryMD
    {
        private CarpetaSistemaQryDP carpetaSistemaQry = null;

        public CarpetaSistemaQryMD(CarpetaSistemaQryDP carpetaSistemaQry)
        {
            this.carpetaSistemaQry = carpetaSistemaQry;
        }

        protected static CarpetaSistemaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            CarpetaSistemaQryDP carpetaSistemaQry = new CarpetaSistemaQryDP();
            carpetaSistemaQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            carpetaSistemaQry.CarpetaSistema = dr.IsDBNull(1) ? "" : dr.GetString(1);
            return carpetaSistemaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count CarpetaSistemaQry
            DbCommand com = con.CreateCommand();
            String countCarpetaSistemaQry = String.Format(CultureInfo.CurrentCulture,CarpetaSistema.CarpetaSistemaQry.CarpetaSistemaQryCount,"");
            com.CommandText = countCarpetaSistemaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count CarpetaSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CarpetaSistemaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List CarpetaSistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCarpetaSistemaQry = String.Format(CultureInfo.CurrentCulture, CarpetaSistema.CarpetaSistemaQry.CarpetaSistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCarpetaSistemaQry = listCarpetaSistemaQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCarpetaSistemaQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCarpetaSistemaQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load CarpetaSistemaQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CarpetaSistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CarpetaSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CarpetaSistemaQryDP[])lista.ToArray(typeof(CarpetaSistemaQryDP));
        }
        public static Int64 CountForCarpeta(DbConnection con, DbTransaction tran, String aCarpetaSistema)
        {
            Int64 resultado = 0;
            #region SQL CountForCarpeta CarpetaSistemaQry
            DbCommand com = con.CreateCommand();
            String countCarpetaSistemaQryForCarpeta = String.Format(CultureInfo.CurrentCulture,CarpetaSistema.CarpetaSistemaQry.CarpetaSistemaQryCount,"");
            countCarpetaSistemaQryForCarpeta += " WHERE \n";
            String delimitador = "";
            if (aCarpetaSistema.Length != 0)
            {
                countCarpetaSistemaQryForCarpeta += String.Format("    {1} Carpeta_Sistema LIKE {0}CarpetaSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countCarpetaSistemaQryForCarpeta;
            #endregion
            #region Parametros countCarpetaSistemaQryForCarpeta
            if (aCarpetaSistema.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CarpetaSistema",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 255, ParameterDirection.Input, aCarpetaSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForCarpeta CarpetaSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static CarpetaSistemaQryDP[] ListForCarpeta(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aCarpetaSistema)
        {
            #region SQL List CarpetaSistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listCarpetaSistemaQryForCarpeta = String.Format(CultureInfo.CurrentCulture, CarpetaSistema.CarpetaSistemaQry.CarpetaSistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listCarpetaSistemaQryForCarpeta = listCarpetaSistemaQryForCarpeta.Substring(6);
            listCarpetaSistemaQryForCarpeta += " WHERE \n";
            String delimitador = "";
            if (aCarpetaSistema.Length != 0)
            {
                listCarpetaSistemaQryForCarpeta += String.Format("    {1} Carpeta_Sistema LIKE {0}CarpetaSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listCarpetaSistemaQryForCarpeta, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listCarpetaSistemaQryForCarpeta;
            DbDataReader dr;
            #endregion
            #region Parametros Load CarpetaSistemaQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aCarpetaSistema.Length != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}CarpetaSistema",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 255, ParameterDirection.Input, aCarpetaSistema);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista CarpetaSistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista CarpetaSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (CarpetaSistemaQryDP[])lista.ToArray(typeof(CarpetaSistemaQryDP));
        }
    }
}
