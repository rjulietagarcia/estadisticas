using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "CarpetaSistemaQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: lunes, 17 de agosto de 2009.</Para>
    /// <Para>Hora: 11:05:17 a.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "CarpetaSistemaQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>SistemaId</term><description>Descripcion SistemaId</description>
    ///    </item>
    ///    <item>
    ///        <term>CarpetaSistema</term><description>Descripcion CarpetaSistema</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "CarpetaSistemaQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// CarpetaSistemaQryDTO carpetasistemaqry = new CarpetaSistemaQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("CarpetaSistemaQry")]
    public class CarpetaSistemaQryDP
    {
        #region Definicion de campos privados.
        private Byte sistemaId;
        private String carpetaSistema;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// SistemaId
        /// </summary> 
        [XmlElement("SistemaId")]
        public Byte SistemaId
        {
            get {
                    return sistemaId; 
            }
            set {
                    sistemaId = value; 
            }
        }

        /// <summary>
        /// CarpetaSistema
        /// </summary> 
        [XmlElement("CarpetaSistema")]
        public String CarpetaSistema
        {
            get {
                    return carpetaSistema; 
            }
            set {
                    carpetaSistema = value; 
            }
        }

        #endregion.
    }
}
