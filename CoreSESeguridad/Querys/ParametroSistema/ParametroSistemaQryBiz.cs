using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class ParametroSistemaQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ParametroSistemaQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ParametroSistemaQry!", ex);
            }
            return resultado;
        }

        public static ParametroSistemaQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            ParametroSistemaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ParametroSistemaQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ParametroSistemaQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForParametro(DbConnection con, Int32 aIdParametro) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ParametroSistemaQryMD.CountForParametro(con, tran, aIdParametro);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ParametroSistemaQry!", ex);
            }
            return resultado;
        }

        public static ParametroSistemaQryDP[] LoadListForParametro(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Int32 aIdParametro) 
        {
            ParametroSistemaQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ParametroSistemaQryMD.ListForParametro(con, tran, startRowIndex, maximumRows, aIdParametro);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ParametroSistemaQry!", ex);
            }
            return resultado;

        }
    }
}
