using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class ParametroSistemaQryMD
    {
        private ParametroSistemaQryDP parametroSistemaQry = null;

        public ParametroSistemaQryMD(ParametroSistemaQryDP parametroSistemaQry)
        {
            this.parametroSistemaQry = parametroSistemaQry;
        }

        protected static ParametroSistemaQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ParametroSistemaQryDP parametroSistemaQry = new ParametroSistemaQryDP();
            parametroSistemaQry.Valor = dr.IsDBNull(0) ? "" : dr.GetString(0);
            parametroSistemaQry.ParametroId = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
            return parametroSistemaQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count ParametroSistemaQry
            DbCommand com = con.CreateCommand();
            String countParametroSistemaQry = String.Format(CultureInfo.CurrentCulture,ParametroSistema.ParametroSistemaQry.ParametroSistemaQryCount,"");
            com.CommandText = countParametroSistemaQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count ParametroSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountParametroSistemaQry = String.Format(CultureInfo.CurrentCulture,ParametroSistema.ParametroSistemaQry.ParametroSistemaQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountParametroSistemaQry);
                #endregion
            }
            return resultado;
        }
        public static ParametroSistemaQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List ParametroSistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listParametroSistemaQry = String.Format(CultureInfo.CurrentCulture, ParametroSistema.ParametroSistemaQry.ParametroSistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listParametroSistemaQry = listParametroSistemaQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listParametroSistemaQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load ParametroSistemaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ParametroSistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ParametroSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaParametroSistemaQry = String.Format(CultureInfo.CurrentCulture,ParametroSistema.ParametroSistemaQry.ParametroSistemaQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaParametroSistemaQry);
                #endregion
            }
            return (ParametroSistemaQryDP[])lista.ToArray(typeof(ParametroSistemaQryDP));
        }
        public static Int64 CountForParametro(DbConnection con, DbTransaction tran, Int32 aIdParametro)
        {
            Int64 resultado = 0;
            #region SQL CountForParametro ParametroSistemaQry
            DbCommand com = con.CreateCommand();
            String countParametroSistemaQryForParametro = String.Format(CultureInfo.CurrentCulture,ParametroSistema.ParametroSistemaQry.ParametroSistemaQryCount,"");
            countParametroSistemaQryForParametro += " WHERE \n";
            String delimitador = "";
            countParametroSistemaQryForParametro += String.Format("    {1} id_parametro = {0}IdParametro\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countParametroSistemaQryForParametro;
            #endregion
            #region Parametros countParametroSistemaQryForParametro
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdParametro",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdParametro);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForParametro ParametroSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorCountParametroSistemaQryForParametro = String.Format(CultureInfo.CurrentCulture,ParametroSistema.ParametroSistemaQry.ParametroSistemaQryCount, "" );
                System.Diagnostics.Debug.WriteLine(errorCountParametroSistemaQryForParametro);
                #endregion
            }
            return resultado;
        }
        public static ParametroSistemaQryDP[] ListForParametro(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Int32 aIdParametro)
        {
            #region SQL List ParametroSistemaQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listParametroSistemaQryForParametro = String.Format(CultureInfo.CurrentCulture, ParametroSistema.ParametroSistemaQry.ParametroSistemaQrySelect, "", ConstantesGlobales.IncluyeRows);
            listParametroSistemaQryForParametro = listParametroSistemaQryForParametro.Substring(6);
            listParametroSistemaQryForParametro += " WHERE \n";
            String delimitador = "";
            listParametroSistemaQryForParametro += String.Format("    {1} id_parametro = {0}IdParametro\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listParametroSistemaQryForParametro, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load ParametroSistemaQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdParametro",ConstantesGlobales.ParameterPrefix), DbType.Int32, 0, ParameterDirection.Input, aIdParametro);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ParametroSistemaQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ParametroSistemaQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                String errorLoadListaParametroSistemaQry = String.Format(CultureInfo.CurrentCulture,ParametroSistema.ParametroSistemaQry.ParametroSistemaQrySelect, "" );
                System.Diagnostics.Debug.WriteLine(errorLoadListaParametroSistemaQry);
                #endregion
            }
            return (ParametroSistemaQryDP[])lista.ToArray(typeof(ParametroSistemaQryDP));
        }
    }
}
