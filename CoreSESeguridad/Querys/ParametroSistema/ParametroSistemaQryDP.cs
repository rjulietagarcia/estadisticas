using System;
using System.Text;
using System.Xml.Serialization;

namespace Mx.Gob.Nl.Educacion.Querys
{
    /// <summary>
    /// <Para>Genere la estructura para "ParametroSistemaQry".</Para>
    /// <Para>Autor: Generador automático de código.</Para>
    /// <Para>Fecha: miércoles, 27 de mayo de 2009.</Para>
    /// <Para>Hora: 12:51:51 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "ParametroSistemaQry".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>Valor</term><description>Descripcion Valor</description>
    ///    </item>
    ///    <item>
    ///        <term>ParametroId</term><description>Descripcion ParametroId</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "ParametroSistemaQryDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Querys;
    /// ...
    /// ParametroSistemaQryDTO parametrosistemaqry = new ParametroSistemaQryDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Querys" />
    [XmlRoot("ParametroSistemaQry")]
    public class ParametroSistemaQryDP
    {
        #region Definicion de campos privados.
        private String valor;
        private Int32 parametroId;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// Valor
        /// </summary> 
        [XmlElement("Valor")]
        public String Valor
        {
            get {
                    return valor; 
            }
            set {
                    valor = value; 
            }
        }

        /// <summary>
        /// ParametroId
        /// </summary> 
        [XmlElement("ParametroId")]
        public Int32 ParametroId
        {
            get {
                    return parametroId; 
            }
            set {
                    parametroId = value; 
            }
        }

        #endregion.
    }
}
