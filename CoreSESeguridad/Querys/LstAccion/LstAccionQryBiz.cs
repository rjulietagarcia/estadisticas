using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class LstAccionQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = LstAccionQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un LstAccionQry!", ex);
            }
            return resultado;
        }

        public static LstAccionQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            LstAccionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = LstAccionQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de LstAccionQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForLlave(DbConnection con, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Byte aIdAccion) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = LstAccionQryMD.CountForLlave(con, tran, aIdSistema, aIdModulo, aIdOpcion, aIdAccion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un LstAccionQry!", ex);
            }
            return resultado;
        }

        public static LstAccionQryDP[] LoadListForLlave(DbConnection con, Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Byte aIdAccion) 
        {
            LstAccionQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = LstAccionQryMD.ListForLlave(con, tran, startRowIndex, maximumRows, aIdSistema, aIdModulo, aIdOpcion, aIdAccion);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de LstAccionQry!", ex);
            }
            return resultado;

        }
    }
}
