using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class LstAccionQryMD
    {
        private LstAccionQryDP lstAccionQry = null;

        public LstAccionQryMD(LstAccionQryDP lstAccionQry)
        {
            this.lstAccionQry = lstAccionQry;
        }

        protected static LstAccionQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            LstAccionQryDP lstAccionQry = new LstAccionQryDP();
            lstAccionQry.SistemaId = dr.IsDBNull(0) ? (Byte)0 : dr.GetByte(0);
            lstAccionQry.ModuloId = dr.IsDBNull(1) ? (Byte)0 : dr.GetByte(1);
            lstAccionQry.OpcionId = dr.IsDBNull(2) ? (Byte)0 : dr.GetByte(2);
            lstAccionQry.AccionId = dr.IsDBNull(3) ? (Byte)0 : dr.GetByte(3);
            lstAccionQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            lstAccionQry.Abreviatura = dr.IsDBNull(5) ? "" : dr.GetString(5);
            lstAccionQry.BitActivo = dr.IsDBNull(6) ? false : dr.GetBoolean(6);
            lstAccionQry.UsuarioId = dr.IsDBNull(7) ? 0 : dr.GetInt32(7);
            lstAccionQry.FechaActualizacion = dr.IsDBNull(8) ? "" : dr.GetDateTime(8).ToShortDateString();;
            lstAccionQry.FechaInicio = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            lstAccionQry.FechaFin = dr.IsDBNull(10) ? "" : dr.GetDateTime(10).ToShortDateString();;
            return lstAccionQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count LstAccionQry
            DbCommand com = con.CreateCommand();
            String countLstAccionQry = String.Format(CultureInfo.CurrentCulture,LstAccion.LstAccionQry.LstAccionQryCount,"");
            com.CommandText = countLstAccionQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count LstAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static LstAccionQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List LstAccionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listLstAccionQry = String.Format(CultureInfo.CurrentCulture, LstAccion.LstAccionQry.LstAccionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listLstAccionQry = listLstAccionQry.Substring(6);
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listLstAccionQry, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listLstAccionQry;
            DbDataReader dr;
            #endregion
            #region Parametros Load LstAccionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista LstAccionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista LstAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (LstAccionQryDP[])lista.ToArray(typeof(LstAccionQryDP));
        }
        public static Int64 CountForLlave(DbConnection con, DbTransaction tran, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Byte aIdAccion)
        {
            Int64 resultado = 0;
            #region SQL CountForLlave LstAccionQry
            DbCommand com = con.CreateCommand();
            String countLstAccionQryForLlave = String.Format(CultureInfo.CurrentCulture,LstAccion.LstAccionQry.LstAccionQryCount,"");
            countLstAccionQryForLlave += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                countLstAccionQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                countLstAccionQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                countLstAccionQryForLlave += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdAccion != 0)
            {
                countLstAccionQryForLlave += String.Format("    {1} Id_Accion = {0}IdAccion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            com.CommandText = countLstAccionQryForLlave;
            #endregion
            #region Parametros countLstAccionQryForLlave
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aIdAccion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdAccion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdAccion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForLlave LstAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static LstAccionQryDP[] ListForLlave(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, Byte aIdSistema, Byte aIdModulo, Byte aIdOpcion, Byte aIdAccion)
        {
            #region SQL List LstAccionQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listLstAccionQryForLlave = String.Format(CultureInfo.CurrentCulture, LstAccion.LstAccionQry.LstAccionQrySelect, "", ConstantesGlobales.IncluyeRows);
            //listLstAccionQryForLlave = listLstAccionQryForLlave.Substring(6);
            listLstAccionQryForLlave += " WHERE \n";
            String delimitador = "";
            if (aIdSistema != 0)
            {
                listLstAccionQryForLlave += String.Format("    {1} Id_Sistema = {0}IdSistema\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdModulo != 0)
            {
                listLstAccionQryForLlave += String.Format("    {1} Id_Modulo = {0}IdModulo\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdOpcion != 0)
            {
                listLstAccionQryForLlave += String.Format("    {1} Id_Opcion = {0}IdOpcion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            if (aIdAccion != 0)
            {
                listLstAccionQryForLlave += String.Format("    {1} Id_Accion = {0}IdAccion\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            //String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
            //    ConstantesGlobales.ParameterPrefix,
            //    listLstAccionQryForLlave, "", 
            //    (startRowIndex + maximumRows).ToString(),
            //    startRowIndex,
            //    maximumRows,
            //    "" // DISTINCT si es que aplica.
            //    );
            com.CommandText = listLstAccionQryForLlave;
            DbDataReader dr;
            #endregion
            #region Parametros Load LstAccionQry
            //Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            //Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aIdSistema != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdSistema",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdSistema);
            }
            if (aIdModulo != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdModulo",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdModulo);
            }
            if (aIdOpcion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdOpcion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdOpcion);
            }
            if (aIdAccion != 0)
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdAccion",ConstantesGlobales.ParameterPrefix), DbType.Byte, 0, ParameterDirection.Input, aIdAccion);
            }
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista LstAccionQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista LstAccionQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (LstAccionQryDP[])lista.ToArray(typeof(LstAccionQryDP));
        }
    }
}
