using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using Mx.Com.Cimar.Supports.DataBases;

namespace Mx.Gob.Nl.Educacion.Querys
{
    public class ZonaPorRegionNivelQryBiz
    {
        public static Int64 Count(DbConnection con) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ZonaPorRegionNivelQryMD.Count(con, tran);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ZonaPorRegionNivelQry!", ex);
            }
            return resultado;
        }

        public static ZonaPorRegionNivelQryDP[] LoadList(DbConnection con, Int64 startRowIndex, Int64 maximumRows) 
        {
            ZonaPorRegionNivelQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ZonaPorRegionNivelQryMD.List(con, tran, startRowIndex, maximumRows);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ZonaPorRegionNivelQry!", ex);
            }
            return resultado;

        }
        public static Int64 CountForDescripcion(DbConnection con, String aNombre, Int16 aIdRegion, Int16 aIdNivel) 
        {
            Int64 resultado = 0;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = con.BeginTransaction();
                    resultado = ZonaPorRegionNivelQryMD.CountForDescripcion(con, tran, aNombre, aIdRegion, aIdNivel);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener la cantidad de un ZonaPorRegionNivelQry!", ex);
            }
            return resultado;
        }

        public static ZonaPorRegionNivelQryDP[] LoadListForDescripcion(DbConnection con, Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdRegion, Int16 aIdNivel) 
        {
            ZonaPorRegionNivelQryDP[] resultado = null;
            try
            {
                con.Open();
                try
                {
                    DbTransaction tran = null;
                    resultado = ZonaPorRegionNivelQryMD.ListForDescripcion(con, tran, startRowIndex, maximumRows, aNombre, aIdRegion, aIdNivel);
                }
                finally
                {
                    con.Close();
                }
            }
            catch (DbException ex)
            {
                throw new BusinessException("Error interno al intentar obtener una lista de ZonaPorRegionNivelQry!", ex);
            }
            return resultado;

        }
    }
}
