using System;
using System.Text;
using System.Globalization;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Runtime.InteropServices;
using Mx.Com.Cimar.Supports.DataBases;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion.Querys
{
    internal class ZonaPorRegionNivelQryMD
    {
        private ZonaPorRegionNivelQryDP zonaPorRegionNivelQry = null;

        public ZonaPorRegionNivelQryMD(ZonaPorRegionNivelQryDP zonaPorRegionNivelQry)
        {
            this.zonaPorRegionNivelQry = zonaPorRegionNivelQry;
        }

        protected static ZonaPorRegionNivelQryDP ReadRow(DbDataReader dr)
        {
            #region Read Row
            ZonaPorRegionNivelQryDP zonaPorRegionNivelQry = new ZonaPorRegionNivelQryDP();
            zonaPorRegionNivelQry.ZonaId = dr.IsDBNull(0) ? (short)0 : dr.GetInt16(0);
            zonaPorRegionNivelQry.TipoeducacionId = dr.IsDBNull(1) ? (short)0 : dr.GetInt16(1);
            zonaPorRegionNivelQry.NivelId = dr.IsDBNull(2) ? (short)0 : dr.GetInt16(2);
            zonaPorRegionNivelQry.Numerozona = dr.IsDBNull(3) ? (short)0 : dr.GetInt16(3);
            zonaPorRegionNivelQry.Nombre = dr.IsDBNull(4) ? "" : dr.GetString(4);
            zonaPorRegionNivelQry.Clave = dr.IsDBNull(5) ? "" : dr.GetString(5);
            zonaPorRegionNivelQry.SostenimientoId = dr.IsDBNull(6) ? (Byte)0 : dr.GetByte(6);
            zonaPorRegionNivelQry.BitActivo = dr.IsDBNull(7) ? false : dr.GetBoolean(7);
            zonaPorRegionNivelQry.UsuarioId = dr.IsDBNull(8) ? 0 : dr.GetInt32(8);
            zonaPorRegionNivelQry.FechaActualizacion = dr.IsDBNull(9) ? "" : dr.GetDateTime(9).ToShortDateString();;
            zonaPorRegionNivelQry.RegionId = dr.IsDBNull(10) ? (short)0 : dr.GetInt16(10);
            return zonaPorRegionNivelQry;
            #endregion
        }
        public static Int64 Count(DbConnection con, DbTransaction tran)
        {
            Int64 resultado = 0;
            #region SQL Count ZonaPorRegionNivelQry
            DbCommand com = con.CreateCommand();
            String countZonaPorRegionNivelQry = String.Format(CultureInfo.CurrentCulture,ZonaPorRegionNivel.ZonaPorRegionNivelQry.ZonaPorRegionNivelQryCount,"");
            com.CommandText = countZonaPorRegionNivelQry;
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Count ZonaPorRegionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static ZonaPorRegionNivelQryDP[] List(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows)
        {
            #region SQL List ZonaPorRegionNivelQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listZonaPorRegionNivelQry = String.Format(CultureInfo.CurrentCulture, ZonaPorRegionNivel.ZonaPorRegionNivelQry.ZonaPorRegionNivelQrySelect, "", ConstantesGlobales.IncluyeRows);
            listZonaPorRegionNivelQry = listZonaPorRegionNivelQry.Substring(6);
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listZonaPorRegionNivelQry, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList;
            DbDataReader dr;
            #endregion
            #region Parametros Load ZonaPorRegionNivelQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ZonaPorRegionNivelQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ZonaPorRegionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (ZonaPorRegionNivelQryDP[])lista.ToArray(typeof(ZonaPorRegionNivelQryDP));
        }
        public static Int64 CountForDescripcion(DbConnection con, DbTransaction tran, String aNombre, Int16 aIdRegion, Int16 aIdNivel)
        {
            Int64 resultado = 0;
            #region SQL CountForDescripcion ZonaPorRegionNivelQry
            DbCommand com = con.CreateCommand();
            String countZonaPorRegionNivelQryForDescripcion = String.Format(CultureInfo.CurrentCulture,ZonaPorRegionNivel.ZonaPorRegionNivelQry.ZonaPorRegionNivelQryCount,"");
            countZonaPorRegionNivelQryForDescripcion += " and \n";
            String delimitador = "";
            if (aNombre != "")
            {
                countZonaPorRegionNivelQryForDescripcion += String.Format("    {1} z.nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            countZonaPorRegionNivelQryForDescripcion += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            countZonaPorRegionNivelQryForDescripcion += String.Format("    {1} z.id_nivel = {0}IdNivel\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            com.CommandText = countZonaPorRegionNivelQryForDescripcion;
            #endregion
            #region Parametros countZonaPorRegionNivelQryForDescripcion
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdNivel);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                resultado = Convert.ToInt64(com.ExecuteScalar().ToString());
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error CountForDescripcion ZonaPorRegionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return resultado;
        }
        public static ZonaPorRegionNivelQryDP[] ListForDescripcion(DbConnection con, DbTransaction tran,
            Int64 startRowIndex, Int64 maximumRows, String aNombre, Int16 aIdRegion, Int16 aIdNivel)
        {
            #region SQL List ZonaPorRegionNivelQry
            ArrayList lista = new ArrayList();
            DbCommand com = con.CreateCommand();
            String listZonaPorRegionNivelQryForDescripcion = String.Format(CultureInfo.CurrentCulture, ZonaPorRegionNivel.ZonaPorRegionNivelQry.ZonaPorRegionNivelQrySelect, "", ConstantesGlobales.IncluyeRows);
            listZonaPorRegionNivelQryForDescripcion = listZonaPorRegionNivelQryForDescripcion.Substring(6);
            listZonaPorRegionNivelQryForDescripcion += " and \n";
            String delimitador = "";
            if (aNombre != "")
            {
                listZonaPorRegionNivelQryForDescripcion += String.Format("    {1} z.nombre LIKE {0}Nombre\n", Model.ConstantesGlobales.ParameterPrefix,
                    delimitador);
                delimitador = " AND ";
            }
            listZonaPorRegionNivelQryForDescripcion += String.Format("    {1} ct.id_region = {0}IdRegion\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            listZonaPorRegionNivelQryForDescripcion += String.Format("    {1} z.id_nivel = {0}IdNivel\n", Model.ConstantesGlobales.ParameterPrefix,
                delimitador);
            delimitador = " AND ";
            String filtraList = String.Format(CultureInfo.CurrentCulture, ConstantesGlobales.TraeUnosRegistros,
                ConstantesGlobales.ParameterPrefix,
                listZonaPorRegionNivelQryForDescripcion, "", 
                (startRowIndex + maximumRows).ToString(),
                startRowIndex,
                maximumRows,
                "" // DISTINCT si es que aplica.
                );
            com.CommandText = filtraList.Replace("top","distinct top");
            DbDataReader dr;
            #endregion
            #region Parametros Load ZonaPorRegionNivelQry
            Common.CreateParameter(com, String.Format("{0}RegPagina", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, maximumRows);
            Common.CreateParameter(com, String.Format("{0}startRowIndex", ConstantesGlobales.ParameterPrefix), DbType.Int64, 0, ParameterDirection.Input, startRowIndex);
            if (aNombre != "")
            {
                Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}Nombre",ConstantesGlobales.ParameterPrefix), DbType.AnsiString, 100, ParameterDirection.Input, aNombre);
            }
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdRegion",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdRegion);
            Common.CreateParameter(com, String.Format(CultureInfo.CurrentCulture,"{0}IdNivel",ConstantesGlobales.ParameterPrefix), DbType.Int16, 0, ParameterDirection.Input, aIdNivel);
            #endregion
            if (tran != null)
            {
                com.Transaction = tran;
            }
            try
            {
                #region Lee la lista ZonaPorRegionNivelQry
                dr = com.ExecuteReader(CommandBehavior.Default);
                try
                {
                    while (dr.Read())
                    {
                        lista.Add(ReadRow(dr));
                    }
                }
                finally
                {
                    dr.Close();
                }
                #endregion
            }
            catch (DbException ex)
            {
                LimpiaConsulta.LimpiaConsultas(con, com, tran, ex);
                #region Error Load Lista ZonaPorRegionNivelQry
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw ex;
                #endregion
            }
            return (ZonaPorRegionNivelQryDP[])lista.ToArray(typeof(ZonaPorRegionNivelQryDP));
        }
    }
}
