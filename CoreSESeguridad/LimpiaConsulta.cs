using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Mx.Gob.Nl.Educacion.Model;

namespace Mx.Gob.Nl.Educacion
{
    public class LimpiaConsulta
    {
        public static string ReemplazaCaracteres(String source)
        {
            string origenReemplazo = ConstantesGlobales.OrigenReemplazo;
            string destinoReemplazo = ConstantesGlobales.DestinoReemplazo;
            int longCadena = origenReemplazo.Length;
            for (int i = 0; i < longCadena; i++)
            {
                source = source.Replace(origenReemplazo.Substring(i, 1),
                    destinoReemplazo.Substring(i, 1));
            }
            return source;
        }

        public static void LimpiaConsultas(IDbConnection con, IDbCommand com, IDbTransaction tran, Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Parameters :");
            foreach (IDataParameter param in com.Parameters)
            {
                sb.AppendLine(param.ParameterName + " : " + param.Value.ToString());
            }
            sb.AppendLine("Query");
            sb.AppendLine(com.CommandText);
            sb.AppendLine("Error");
            sb.AppendLine(ex.Message);
            //EventLog.WriteEntry("Celfos", sb.ToString(), EventLogEntryType.Error, 1000, 0);
            IDbCommand cmd = con.CreateCommand();
            if (tran != null)
                cmd.Transaction = tran;
            cmd.CommandText = ConstantesGlobales.LimpiaConsulta;
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception exi)
            {
                System.Diagnostics.Debug.WriteLine(exi.Message);
                //EventLog.WriteEntry("Celfos", exi.Message, EventLogEntryType.Error, 2000, 1);
            }
        }
    }

}
