using System;
using System.Collections.Generic;
using System.Text;
//using Mx.Gob.Nl.Educacion.WsSESeguridad;
using System.Xml;
using System.Xml.Serialization;
using SEroot.WsSESeguridad;
using SEroot;

namespace Mx.Gob.Nl.Educacion
{
    /// <summary>
    /// <Para>Genere la estructura para "Usuario".</Para>
    /// <Para>Autor: Generador autom�tico de c�digo.</Para>
    /// <Para>Fecha: domingo, 24 de mayo de 2009.</Para>
    /// <Para>Hora: 08:33:39 p.m.</Para>
    /// </summary>
    /// <remarks>
    /// <Para>Recuerde utilizar esta clase cuando se desee representar "Usuario".</Para>
    /// <Para>Propiedades.
    /// <list type="bullet">
    ///    <item>
    ///        <term>UsuarioId</term><description>Descripcion UsuarioId</description>
    ///    </item>
    ///    <item>
    ///        <term>PersonaId</term><description>Descripcion PersonaId</description>
    ///    </item>
    ///    <item>
    ///        <term>Login</term><description>Descripcion Login</description>
    ///    </item>
    ///    <item>
    ///        <term>Password</term><description>Descripcion Password</description>
    ///    </item>
    ///    <item>
    ///        <term>BitBloqueado</term><description>Descripcion BitBloqueado</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo</term><description>Descripcion BitActivo</description>
    ///    </item>
    ///    <item>
    ///        <term>BitActivo2</term><description>Descripcion BitActivo2</description>
    ///    </item>
    ///    <item>
    ///        <term>Usuario2Id</term><description>Descripcion Usuario2Id</description>
    ///    </item>
    ///    <item>
    ///        <term>FechaActualizacion</term><description>Descripcion FechaActualizacion</description>
    ///    </item>
    ///    <item>
    ///        <term>Persona</term><description>Descripcion Persona</description>
    ///    </item>
    /// </list>
    /// </Para>
    /// </remarks>
    /// <example>
    /// Este es un ejemplo de como utilizar la clase "UsuarioDTO".
    /// <code>
    /// using Mx.Gob.Nl.Educacion.Model;
    /// ...
    /// UsuarioDTO usuario = new UsuarioDTO();
    /// </code>
    /// </example>
    /// <seealso cref="Mx.Gob.Nl.Educacion.Model" />
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    [XmlRoot("UsuarioSeDP")]
    public class UsuarioSeDP
    {
        #region Definicion de campos privados.
        private Int32 usuarioId;
        private Int32 personaId;
        private String login;
        private Boolean bitBloqueado;
        private Boolean bitActivo;
        private Boolean bitActivo2;
        private Int32 usuario2Id;
        private String fechaActualizacion;
        private PersonaDP persona;
        private NiveltrabajoDP nivelTrabajo;
        private RegionDP region;
        private ZonaDP zona;
        private SectorDP sector;
        private PerfilDP perfilAnterior;
        private PerfilDP[] listaPerfilAnterior;
        private CentrotrabajoDP[] centroTrabajo;
        private CctntUsrQryDP[] cctnts;
        private NivelEducacionNvoDP[] nivelesEducacion;
        private SostenimientoQryDP[] sostenimientos;
        
        private PerfilEducacionUsuarioDP[] perfilEducacion;
        private PaginaDP paginaActual;
        private SeleccionSeDP seleccionees;
        private EntidadDP entidad;
	
        #endregion.

        #region Definicion de propiedades.
    
        [XmlElement("Entidad")]
        public EntidadDP EntidadDP
        {
            get { return entidad; }
            set { entidad = value; }
        }

        [XmlElement("Selecciones")]
        public SeleccionSeDP Selecciones
        {
            get { return seleccionees; }
            set { seleccionees = value; }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get {
                    return usuarioId; 
            }
            set {
                    usuarioId = value; 
            }
        }

        /// <summary>
        /// PersonaId
        /// </summary> 
        [XmlElement("PersonaId")]
        public Int32 PersonaId
        {
            get {
                    return personaId; 
            }
            set {
                    personaId = value; 
            }
        }

        /// <summary>
        /// Login
        /// </summary> 
        [XmlElement("Login")]
        public String Login
        {
            get {
                    return login; 
            }
            set {
                    login = value; 
            }
        }

        ///// <summary>
        ///// Password
        ///// </summary> 
        //[XmlElement("Password")]
        //public String Password
        //{
        //    get {
        //            return password; 
        //    }
        //    set {
        //            password = value; 
        //    }
        //}

        /// <summary>
        /// BitBloqueado
        /// </summary> 
        [XmlElement("BitBloqueado")]
        public Boolean BitBloqueado
        {
            get {
                    return bitBloqueado; 
            }
            set {
                    bitBloqueado = value; 
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get {
                    return bitActivo; 
            }
            set {
                    bitActivo = value; 
            }
        }

        /// <summary>
        /// BitActivo2
        /// </summary> 
        [XmlElement("BitActivo2")]
        public Boolean BitActivo2
        {
            get {
                    return bitActivo2; 
            }
            set {
                    bitActivo2 = value; 
            }
        }

        /// <summary>
        /// Usuario2Id
        /// </summary> 
        [XmlElement("Usuario2Id")]
        public Int32 Usuario2Id
        {
            get {
                    return usuario2Id; 
            }
            set {
                    usuario2Id = value; 
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get {
                    return fechaActualizacion; 
            }
            set {
                    fechaActualizacion = value; 
            }
        }

        /// <summary>
        /// Persona
        /// </summary> 
        [XmlElement("Persona")]
        public PersonaDP Persona
        {
            get {
                    return persona; 
            }
            set {
                    persona = value; 
            }
        }

        /// <summary>
        /// nivelTrabajo
        /// </summary> 
        [XmlElement("NivelTrabajo")]
        public NiveltrabajoDP NivelTrabajo
        {
            get
            {
                return nivelTrabajo;
            }
            set
            {
                nivelTrabajo = value;
            }
        }

        /// <summary>
        /// Region
        /// </summary> 
        [XmlElement("Region")]
        public RegionDP Region
        {
            get
            {
                return region;
            }
            set
            {
                region = value;
            }
        }

        /// <summary>
        /// Zona
        /// </summary> 
        [XmlElement("Zona")]
        public ZonaDP Zona
        {
            get
            {
                return zona;
            }
            set
            {
                zona = value;
            }
        }

        /// <summary>
        /// Sector
        /// </summary> 
        [XmlElement("Sector")]
        public SectorDP Sector
        {
            get
            {
                return sector;
            }
            set
            {
                sector = value;
            }
        }

        /// <summary>
        /// PerfilAnterior
        /// </summary> 
        [XmlElement("PerfilAnterior")]
        public PerfilDP PerfilAnterior
        {
            get
            {
                return perfilAnterior;
            }
            set
            {
                perfilAnterior = value;
            }
        }

        /// <summary>
        /// CentrosTrabajo
        /// </summary> 
        [XmlElement("CentrosTrabajo")]
        public CentrotrabajoDP[] CentrosTrabajo
        {
            get
            {
                return centroTrabajo;
            }
            set
            {
                centroTrabajo = value;
            }
        }

        /// <summary>
        /// NivelesEducacion
        /// </summary> 
        [XmlElement("NivelesEducacion")]
        public NivelEducacionNvoDP[] NivelesEducacion
        {
            get
            {
                return nivelesEducacion;
            }
            set
            {
                nivelesEducacion = value;
            }
        }

        /// <summary>
        /// Sostenimientos
        /// </summary> 
        [XmlElement("Sostenimientos")]
        public SostenimientoQryDP[] Sostenimientos
        {
            get
            {
                return sostenimientos;
            }
            set
            {
                sostenimientos = value;
            }
        }

        /// <summary>
        /// Perfiles Educacion
        /// </summary> 
        [XmlElement("PerfilEducacion")]
        public PerfilEducacionUsuarioDP[] PerfilEducacion
        {
            get
            {
                return perfilEducacion;
            }
            set
            {
                perfilEducacion = value;
            }
        }

        /// <summary>
        /// Pagina actual <remarks>P�gina donde me encuentro posicionado, con sus opciones de nivel de trabajo, sostenimiento y acciones permitidas.</remarks>
        /// </summary> 
        [XmlElement("PerfilEducacion")]
        public PaginaDP PaginaActual
        {
            get { return paginaActual; }
            set { paginaActual = value; }
        }

        /// <summary>
        /// Llaves de CctNt's para el nuevo manejo de seguridad de las pantallas.
        /// </summary> 
        [XmlElement("CctNTs")]
        public CctntUsrQryDP[] CctNTs
        {
            get { return cctnts; }
            set { cctnts = value; }
        }
        [XmlElement("ListaPerfilAnterior")]
        public PerfilDP[] ListaPerfilAnterior
        {
            get { return listaPerfilAnterior; }
            set { listaPerfilAnterior = value; }
        }

        #endregion.
    }
}
