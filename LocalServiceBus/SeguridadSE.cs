using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web.UI;
//using Mx.Gob.Nl.Educacion.WsSESeguridad;
using System.Web;
using SEroot.WsSESeguridad;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Security;
using System.Collections;

namespace Mx.Gob.Nl.Educacion
{
    public class SeguridadSE
    {
        static WsSESeguridad wse = null;

        private static void getConexion() {
            if (wse == null)
                wse = new WsSESeguridad();
            if (ConfigurationManager.AppSettings["WebServiceSeguridad"] == null)
            {
                throw new Exception("Necesitar crear la definici�n para el servicio 'WebServiceSeguridad' dentro de la secci�n AppSettings");
            }
            else
                wse.Url = ConfigurationManager.AppSettings["WebServiceSeguridad"];
        }

        public static void ValidateCrossUser() {
            HttpContext pagina = HttpContext.Current;
            UsuarioSeDP usr = (UsuarioSeDP)pagina.Session["usuarioSEP"];
            string username = pagina.User.Identity.Name;
            int usuarioId = Convert.ToInt32("0"+username.Split('|')[0]);
            if (usr.UsuarioId != usuarioId)
            {
                pagina.Session.Clear();
                GetUsuario(HttpContext.Current);

                if (pagina.Request.RawUrl.ToUpper().Contains("DEFAULT.ASPX"))
                    DeserializeAndGet(usr);
                return;
            }
        }
        public static UsuarioSeDP GetUsuario(HttpContext pagina) {
            UsuarioSeDP usr = null;
            if (pagina.User.Identity.Name != "")
            {

                if (pagina.Session["usuarioSEP"] == null)
                {
                    
                    getConexion();
                    #region Llaves
                    string username = pagina.User.Identity.Name;
                    string[] opciones = username.Split('|');
                    int usuarioId = Convert.ToInt32(opciones[0]);
                    int nivelTrabajoId = Convert.ToInt32(opciones[1]);
                    int regionId = Convert.ToInt32(opciones[2]);
                    int zonaId = Convert.ToInt32(opciones[3]);
                    int sectorId = Convert.ToInt32(opciones[4]);
                    // Centro trabajo
                    string[] cct = opciones[5].Split(',');
                    int id_CentroTrabajo = 0;
                    if(cct.Length > 0 )
                        id_CentroTrabajo=int.Parse(cct[0]);

                    int perfilId = Convert.ToInt32(opciones[6]);
                    // Nivel Educacion
                    string[] nivelesEducacion = opciones[7].Split(',');
                    // Sostenimiento
                    string[] sostenimientos = opciones[8].Split(',');
                    #endregion

                    usr = new UsuarioSeDP();
                    pagina.Session["usuarioSEP"] = usr;                    

                    #region asigno valores
                    GetUsuario(usuarioId, usr);
                    GetPersona(usr);
                    GetNivelTrabajo(nivelTrabajoId, usr);
                    GetEntidad(id_CentroTrabajo, usr);
                    GetRegion(regionId, usr);
                    GetZona(zonaId, usr);
                    GetSector(sectorId, usr);
                    GetPerfil(perfilId, usr);
                    GetListaPerfil(usr);
                    GetCentrosTrabajo(cct, usr);
                    

                    //GetNivelesEducacion(nivelesEducacion, usr);
                    GetNivelesEducacion(usr);
                    //GetSostenimientos(sostenimientos, usr);
                    GetSostenimientos(usr);
                    GetPerfilesEducacion(usr);
                    GetCctNts(usr);
                    GetPrimeraSeleccion(usr);
                    #endregion
                }
                else
                {
                    usr = (UsuarioSeDP)pagina.Session["usuarioSEP"];
                    #region por revisar
                    //string username = pagina.User.Identity.Name;
                    //string[] opciones = username.Split('|');
                    //int usuarioId = Convert.ToInt32(opciones[0]);
                    //if (usr.UsuarioId != usuarioId) {
                    //    pagina.Session.Clear();
                    //    return GetUsuario(pagina);
                    //}
                    #endregion
                }
                // Actualiza la p�gina actual.
                if (!(pagina.Request.RawUrl.ToUpper().Contains("DEFAULT.ASPX") || pagina.Request.RawUrl.ToUpper().Contains("CATALAGOS.ASMX")))
                    if ((usr.PaginaActual == null)||(
                        (usr.PaginaActual.Acciones == null) ||
                        (usr.PaginaActual.Acciones.Length == 0)                    
                        ))
                        GetPagina(usr, pagina);
            }
            return usr;

        }

        public static UsuarioSeDP GetUsuario(Page pagina)
        {
            UsuarioSeDP usr = null;
            if (pagina.User.Identity.Name != "")
            {

                if (pagina.Session["usuarioSEP"] == null)
                {
                    getConexion();
                    #region Llaves
                    string username = pagina.User.Identity.Name;
                    string[] opciones = username.Split('|');
                    int usuarioId = Convert.ToInt32(opciones[0]);
                    int nivelTrabajoId = Convert.ToInt32(opciones[1]);
                    int regionId = Convert.ToInt32(opciones[2]);
                    int zonaId = Convert.ToInt32(opciones[3]);
                    int sectorId = Convert.ToInt32(opciones[4]);
                    // Centro trabajo
                    string[] cct = opciones[5].Split(',');
                    int perfilId = Convert.ToInt32(opciones[6]);
                    // Nivel Educacion
                    string[] nivelesEducacion = opciones[7].Split(',');
                    // Sostenimiento
                    string[] sostenimientos = opciones[8].Split(',');
                    #endregion

                    usr = new UsuarioSeDP();
                    pagina.Session["usuarioSEP"] = usr;

                    #region asigno valores
                    GetUsuario(usuarioId, usr);
                    GetPersona(usr);
                    GetNivelTrabajo(nivelTrabajoId, usr);
                    GetRegion(regionId, usr);
                    GetZona(zonaId, usr);
                    GetSector(sectorId, usr);
                    GetPerfil(perfilId, usr);
                    GetListaPerfil(usr);
                    GetCentrosTrabajo(cct, usr);
                    //GetNivelesEducacion(nivelesEducacion, usr);
                    
                    //GetSostenimientos(sostenimientos, usr);
                    GetSostenimientos(usr);
                    GetNivelesEducacion(usr);
                    GetPerfilesEducacion(usr);
                    GetCctNts(usr);
                    GetPrimeraSeleccion(usr);
                    #endregion
                }
                else
                {
                    usr = (UsuarioSeDP)pagina.Session["usuarioSEP"];
                    string username = pagina.User.Identity.Name;
                    string[] opciones = username.Split('|');
                    int usuarioId = Convert.ToInt32(opciones[0]);
                    if (usr.UsuarioId != usuarioId)
                    {
                        pagina.Session.Clear();
                        return GetUsuario(pagina);
                    }
                }
                // Actualiza la p�gina actual.
                GetPagina(usr, HttpContext.Current);
            }
            return usr;
        }

        public static void SerializeAndSave(SessionesDP source, SEroot.SeleccionSeDP obj)
        {
            try
            {
                // instantiate a MemoryStream and a new instance of our class          
                MemoryStream ms = new MemoryStream();                
                // create a new BinaryFormatter instance 
                BinaryFormatter b = new BinaryFormatter();
                // serialize the class into the MemoryStream 
                b.Serialize(ms, obj);
                ms.Seek(0, 0);
                //Clean up 
                source.Datos = ms.ToArray();
                ms.Close();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Trace.Warn("SeguridadSE", ex.Message, ex);
            }
        }

        public static SEroot.SeleccionSeDP RetrieveAndDeserialize(SessionesDP ses)
        {
            MemoryStream ms2 = new MemoryStream();
            byte[] buf = ses.Datos;
            ms2.Write(buf, 0, buf.Length);
            ms2.Seek(0, 0);
            BinaryFormatter b = new BinaryFormatter();
            SEroot.SeleccionSeDP c = (SEroot.SeleccionSeDP)b.Deserialize(ms2);
            ms2.Close();
            return c;
        }

        public static void DeleteSession() {
            SessionesDP ses = new SessionesDP();
            ses.SessionId = HttpContext.Current.Session.SessionID;
            try
            {
                wse.SessionesDelete(ses);
            }
            catch { }
        }

        public static void DeserializeAndGet(UsuarioSeDP usr) {
            SessionesPk ses = new SessionesPk();
            FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;
            FormsAuthenticationTicket ticket = id.Ticket;
            Int32 userId = usr.UsuarioId;
            ses.SessionId = HttpContext.Current.Session.SessionID;
                //System.DateTime.Now.ToString("yyyyMMdd")+HttpContext.Current.Session.SessionID+ userId.ToString("0000000000000000");
//                ticket.IssueDate.ToString("yyyyMMddHHmmss") 
            SessionesDP valor = wse.SessionesLoad(ses);
            if ((valor != null))
                usr.Selecciones = RetrieveAndDeserialize(valor);
        }

        public static CcntFiltrosQryDP cctSelected;
        public static void SetCctNt(Page pagina, int cctntid) 
        { 
            UsuarioSeDP usr = null;
            if (pagina.User.Identity.Name != "")
            {
                usr = (UsuarioSeDP)pagina.Session["usuarioSEP"];
                getConexion();
                int llaveCCTNTId = 0;
               
                int.TryParse(cctntid.ToString(), out llaveCCTNTId);
                CcntFiltrosQryDP[] cct = wse.CcntFiltrosQryLoadListForFiltros(0, 1, 0, -1, 0, -1, llaveCCTNTId);
                usr.Selecciones.CentroTrabajoSeleccionado = cct[0];
                //SetSeleccionesDeSession(usr);
            }

            cctSelected = new CcntFiltrosQryDP();
            cctSelected = usr.Selecciones.CentroTrabajoSeleccionado;
        }

        private static void SetSeleccionesDeSession(UsuarioSeDP usr)
        {
            SessionesDP ses = new SessionesDP();
            ses.Fecha = DateTime.Now.ToShortDateString();
            FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;
            FormsAuthenticationTicket ticket = id.Ticket;
            Int32 userId = Int32.Parse("0" + HttpContext.Current.User.Identity.Name.Split('|')[0]);
            ses.SessionId = HttpContext.Current.Session.SessionID;
                //System.DateTime.Now.ToString("yyyyMMdd") + HttpContext.Current.Session.SessionID + userId.ToString("0000000000000000");                
//                ticket.IssueDate.ToString("yyyyMMddHHmmss") + userId.ToString("0000000000000000");
            SerializeAndSave(ses, usr.Selecciones);
            wse.SessionesSave(ses);
        }

        public static CicloescolarDP cicloSelected;
        public static void SetCicloEscolar(Page pagina, int cicloEscolarId) {
            UsuarioSeDP usr = null;
            if (pagina.User.Identity.Name != "")
            {
                usr = (UsuarioSeDP)pagina.Session["usuarioSEP"];
                if (usr.Selecciones.CicloEscolarSeleccionado.CicloescolarId != cicloEscolarId)
                {
                    getConexion();
                    CicloescolarPk pk = new CicloescolarPk();
                    pk.TipocicloId = 1;
                    pk.CicloescolarId = Convert.ToInt16(cicloEscolarId);
                    usr.Selecciones.CicloEscolarSeleccionado = wse.CicloescolarLoad(pk);
                    //SetSeleccionesDeSession(usr);
                }
            }
            cicloSelected = new CicloescolarDP();
            cicloSelected = usr.Selecciones.CicloEscolarSeleccionado;
        }

        private static void GetCctNts(UsuarioSeDP usr) 
        {
            int cnt = Convert.ToInt32(wse.CctntUsrQryCountForUsuario(usr.UsuarioId));
            if (cnt != 0)
                usr.CctNTs = wse.CctntUsrQryLoadListForUsuario(0, cnt, usr.UsuarioId);
            else
                usr.CctNTs = new CctntUsrQryDP[0];
        }


        //private static void GetSostenimientos(string[] sostenimientos, UsuarioSeDP usr)
        //{
        //    usr.Sostenimientos = new SostenimientoQryDP[sostenimientos.Length];
        //    int i = 0;
        //    foreach (string sostenimiento in sostenimientos)
        //    {
        //        SostenimientoPk pkSostenimiento = new SostenimientoPk();
        //        pkSostenimiento.SostenimientoId = Convert.ToByte(sostenimiento);
        //        usr.Sostenimientos[i] = wse.SostenimientoLoad(pkSostenimiento);
        //        i++;
        //    }
        //}
        private static void GetSostenimientos(UsuarioSeDP usr)
        {
            //int cantidad = int.Parse(wse.Sostenimiento2QryCountForUsuario(usr.UsuarioId).ToString());
            //cantidad = cantidad == 0 ? 1 : cantidad;
            //cantidad = cantidad > 250 ? 250 : cantidad;
            //usr.Sostenimientos = new SostenimientoQryDP[cantidad];
            usr.Sostenimientos = wse.Sostenimiento2QryLoadListForUsuario(0, 0, usr.UsuarioId);
        }
        private static void GetPrimeraSeleccion(UsuarioSeDP usr) 
        {
            DeserializeAndGet(usr);
            if (usr.Selecciones == null)
            {
                usr.Selecciones = new SEroot.SeleccionSeDP();
                usr.Selecciones.CentroTrabajoSeleccionado = new CcntFiltrosQryDP();
                if (usr.CctNTs.Length != 0)
                {
                    #region Centro de Trabajo Seleccionado
                    usr.Selecciones.CentroTrabajoSeleccionado.CctntId =
                        usr.CctNTs[0].CctntId;
                    usr.Selecciones.CentroTrabajoSeleccionado.NiveleducacionId =
                        usr.CctNTs[0].NiveleducacionId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Niveleducacion =
                        usr.CctNTs[0].Niveleducacion;
                    usr.Selecciones.CentroTrabajoSeleccionado.TurnoId =
                        usr.CctNTs[0].TurnoId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Truno =
                        usr.CctNTs[0].Truno;
                    usr.Selecciones.CentroTrabajoSeleccionado.CentrotrabajoId =
                        usr.CctNTs[0].CentrotrabajoId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Nombrecct =
                        usr.CctNTs[0].Nombrecct;
                    usr.Selecciones.CentroTrabajoSeleccionado.Clavecct =
                        usr.CctNTs[0].Clavecct;
                    usr.Selecciones.CentroTrabajoSeleccionado.SostenimientoId =
                        usr.CctNTs[0].SostenimientoId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Sostenimiento =
                        usr.CctNTs[0].Sostenimiento;
                    usr.Selecciones.CentroTrabajoSeleccionado.SubnivelId =
                        usr.CctNTs[0].SubnivelId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Subnivel =
                        usr.CctNTs[0].Subnivel;
                    usr.Selecciones.CentroTrabajoSeleccionado.TipoeducacionId =
                        usr.CctNTs[0].TipoeducacionId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Tipoeducacion =
                        usr.CctNTs[0].Tipoeducacion;
                    usr.Selecciones.CentroTrabajoSeleccionado.NivelId =
                        usr.CctNTs[0].NivelId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Nivel =
                        usr.CctNTs[0].Nivel;
                    usr.Selecciones.CentroTrabajoSeleccionado.RegionId =
                        usr.CctNTs[0].RegionId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Region =
                        usr.CctNTs[0].Region;
                    usr.Selecciones.CentroTrabajoSeleccionado.ZonaId =
                        usr.CctNTs[0].ZonaId;
                    usr.Selecciones.CentroTrabajoSeleccionado.Numerozona =
                        usr.CctNTs[0].Numerozona;
                    #endregion
                }

                #region Ciclo Escolar
                if (HttpContext.Current.Application["cicloescolaractualid"] == null)
                {
                    SEroot.WSEscolar.WsEscolar ws = new SEroot.WSEscolar.WsEscolar();
                    SEroot.WSEscolar.CicloEscolarDP ciclo = ws.LoadCiclo(1).cicloActual; // HARDCODE POR AHORA PARA CICLOESCOLAR ACTUAL NIVEL 1(BASICA)
                    HttpContext.Current.Application["cicloescolaractualid"] = ciclo.cicloEscolarId.ToString();
                }

                int cicloescolaractualid = Convert.ToInt32(HttpContext.Current.Application["cicloescolaractualid"]);
                CicloescolarPk pk = new CicloescolarPk();
                pk.CicloescolarId = Convert.ToInt16(cicloescolaractualid);
                pk.TipocicloId = 1;
                usr.Selecciones.CicloEscolarSeleccionado = wse.CicloescolarLoad(pk);
                HttpContext.Current.Application.Add("cicloescolaractualid", usr.Selecciones.CicloEscolarSeleccionado);

                #endregion

                //SetSeleccionesDeSession(usr);
            }
        }

        //private static void GetNivelesEducacion(string[] nivelesEducacion, UsuarioSeDP usr)
        //{
        //    usr.NivelesEducacion = new NivelEducacionQryDP[nivelesEducacion.Length];
        //    int i = 0;
        //    foreach (string nivel in nivelesEducacion)
        //    {
        //        NiveleducacionbasicaPk pkNivelEducacionBasica = new NiveleducacionbasicaPk();
        //        pkNivelEducacionBasica.NiveleducacionId = Convert.ToByte(nivel);
        //        usr.NivelesEducacion[i] = wse.NiveleducacionbasicaLoad(pkNivelEducacionBasica);
        //        i++;
        //    }
        //}

        //private static NivelEducacionNvoDP ConvertNivelEducacionNvo2NivelEducacionQry(NivelEducacionQryDP source)
        //{
        //    NivelEducacionNvoDP resultado = null;
        //    if (source != null) {
        //        resultado = new NivelEducacionNvoDP();
        //        resultado.BitActivo = source.BitActivo;
        //        resultado.FechaActualizacion = source.FechaActualizacion;
        //        resultado.LetraFolio = source.LetraFolio;
        //        resultado.NivelId = source.NivelId;
        //        resultado.Nombre = source.Nombre;
        //        resultado.TipoeducacionId = source.TipoeducacionId;
        //        resultado.UsuarioId = source.UsuarioId;
        //    }
        //    return resultado;
        //}

        private static NivelEducacionNvoDP[] ConvertArrayNivelEducacionNvo2ArrayNivelEducacionQry(NivelEducacionQryDP[] source, UsuarioSeDP usr)
        {
            NivelEducacionNvoDP[] resultado = null;            
            if (source != null) 
            {
                resultado = new NivelEducacionNvoDP[source.Length];
                int i = 0;
                
                foreach (NivelEducacionQryDP linea in source) 
                {
                    NivelEducacionNvoDP level = new NivelEducacionNvoDP();
                    
                    
                    level.BitActivo = linea.BitActivo;
                    level.FechaActualizacion = linea.FechaActualizacion;
                    level.LetraFolio = linea.LetraFolio;
                    level.NivelId = linea.NivelId;
                    level.Nombre = linea.Nombre;
                    level.TipoeducacionId = linea.TipoeducacionId;
                    level.UsuarioId = usr.UsuarioId;

                    level.SubNiveles = convert(wse.SubnivelEducacionQryLoadListForUsuario(1, 10, usr.UsuarioId, linea.NivelId));

                    
                        bool meter = true;
                        foreach (NivelEducacionNvoDP tmp in resultado)
                        {
                            if (tmp != null)
                            {
                                if (tmp.Nombre == level.Nombre)
                                {
                                    meter = false;
                                    break;
                                }
                            }
                        }
                        if (meter)
                        {
                            resultado[i] = level;
                            i++;
                        }
                    }

             }
                            
                    
            return resultado;
        }


        private static SubnivelDP[] convert(SubnivelEducacionQryDP[] source)
        {

            ArrayList result = new ArrayList();
            SubnivelDP tmp;
            foreach (SubnivelEducacionQryDP nivel in source)
            {
                
                    tmp = new SubnivelDP();
                    tmp.Nombre = nivel.Nombresubnivel;
                    tmp.SubnivelId = nivel.SubnivelId;
                    tmp.NivelId = nivel.NivelId;
                    tmp.TipoeducacionId = nivel.TipoeducacionId;
                    tmp.BitActivo = nivel.BitActivo;
                    tmp.UsuarioId = short.Parse(nivel.UsuarioId.ToString());
                    result.Add(tmp);
                
            }

            return (SubnivelDP[])result.ToArray(typeof(SubnivelDP));
        }

        private static void GetNivelesEducacion(UsuarioSeDP usr)
        {
            
            //int cantidad = int.Parse(wse.NivelEducacion2QryCountForUsuario(usr.UsuarioId).ToString());
            //cantidad = cantidad == 0 ? 1 : cantidad;
            //cantidad = cantidad > 250 ? 250 : cantidad;
            //usr.NivelesEducacion = new NivelEducacionNvoDP[cantidad];
            NivelEducacionNvoDP[] resultado = ConvertArrayNivelEducacionNvo2ArrayNivelEducacionQry(wse.NivelEducacion2QryLoadListForUsuario(0, 0, usr.UsuarioId),usr);
            usr.NivelesEducacion = resultado;
        }


        private static void GetCentrosTrabajo(string[] cct, UsuarioSeDP usr)
        {
            usr.CentrosTrabajo = new CentrotrabajoDP[cct.Length];
            int i = 0;
            foreach (string centro in cct)
            {
                CentrotrabajoPk pkCentroTrabajo = new CentrotrabajoPk();
                pkCentroTrabajo.CentrotrabajoId = int.Parse(centro.ToString());
                usr.CentrosTrabajo[i] = wse.CentrotrabajoLoad(pkCentroTrabajo);
                i++;
            }
        }

        private static void GetEntidad(int id_centrotrabajo,UsuarioSeDP usr)
        {
            CentrotrabajoPk centroTrabajoPK = new CentrotrabajoPk();
            centroTrabajoPK.CentrotrabajoId = id_centrotrabajo;
            CentrotrabajoDP centroTrabajoDP = wse.CentrotrabajoLoad(centroTrabajoPK);


            EntidadPk entidadPK = new EntidadPk();
            entidadPK.EntidadId = short.Parse(centroTrabajoDP.EntidadId.ToString());
            entidadPK.PaisId = 223;
            EntidadDP entidad = wse.EntidadLoad(entidadPK);


            usr.EntidadDP = wse.EntidadLoad(entidadPK);
            
        }


        private static void GetPerfil(int perfilId, UsuarioSeDP usr)
        {
            PerfilPk pkPerfil = new PerfilPk();
            pkPerfil.PerfilId = perfilId;
            usr.PerfilAnterior = wse.PerfilLoad(pkPerfil);
        }

        private static void GetListaPerfil(UsuarioSeDP usr)
        {
            usr.ListaPerfilAnterior = wse.ListaPerfil(usr.UsuarioId);
        }


        private static void GetSector(int sectorId, UsuarioSeDP usr)
        {
            SectorPk pkSector = new SectorPk();
            pkSector.SectorId = Convert.ToInt16(sectorId);
            usr.Sector = wse.SectorLoad(pkSector);
        }

        private static void GetZona(int zonaId, UsuarioSeDP usr)
        {
            ZonaPk pkZona = new ZonaPk();
            pkZona.ZonaId = Convert.ToInt16(zonaId);
            usr.Zona = wse.ZonaLoad(pkZona);
        }

        private static void GetRegion(int regionId, UsuarioSeDP usr)
        {
            RegionPk pkRegion = new RegionPk();
            pkRegion.EntidadId = usr.EntidadDP.EntidadId;
            pkRegion.PaisId = 223;
            pkRegion.RegionId = int.Parse(regionId.ToString());
            usr.Region = wse.RegionLoad(pkRegion);
        }

        private static void GetNivelTrabajo(int nivelTrabajoId, UsuarioSeDP usr)
        {
            NiveltrabajoPk pkNivel = new NiveltrabajoPk();
            pkNivel.NiveltrabajoId = Convert.ToByte(nivelTrabajoId);
            usr.NivelTrabajo = wse.NiveltrabajoLoad(pkNivel);
        }

        private static void GetPersona(UsuarioSeDP usr)
        {
            PersonaPk pkPersona = new PersonaPk();
            pkPersona.PersonaId = usr.PersonaId;
            usr.Persona = wse.PersonaLoad(pkPersona);
        }

        private static void GetUsuario(int usuarioId, UsuarioSeDP usr)
        {
            UsuarioPk pk = new UsuarioPk();
            pk.UsuarioId = usuarioId;
            UsuarioDP usrActual = wse.UsuarioLoad(pk);
            usr.BitActivo = usrActual.BitActivo;
            usr.BitActivo2 = usrActual.BitActivo2;
            usr.BitBloqueado = usrActual.BitBloqueado;
            usr.FechaActualizacion = usrActual.FechaActualizacion;
            usr.Login = usrActual.Login;
            usr.PersonaId = usrActual.PersonaId;
            usr.UsuarioId = usuarioId;
            usr.Usuario2Id = usuarioId;
        }

        private static void GetPerfilesEducacion(UsuarioSeDP usr)
        {
            PerfilEducacionUsuarioQryDP[] qry = wse.PerfilEducacionUsuarioQryLoadListForUsuario(
                0, 100, usr.UsuarioId, -1);
            usr.PerfilEducacion = new PerfilEducacionUsuarioDP[qry.Length];
            int i = 0;
            foreach (PerfilEducacionUsuarioQryDP perfil in qry)
            {
                usr.PerfilEducacion[i] = new PerfilEducacionUsuarioDP();

                usr.PerfilEducacion[i].AbreviaturaPerfil = perfil.AbreviaturaPerfil;
                usr.PerfilEducacion[i].AbreviaturaNivelTrabajo = perfil.AbreviaturaNivelTrabajo;
                usr.PerfilEducacion[i].BitActivo = perfil.BitActivo;
                usr.PerfilEducacion[i].CicloescolarId = perfil.CicloescolarId;
                usr.PerfilEducacion[i].FechaActualizacion = perfil.FechaActualizacion;

                usr.PerfilEducacion[i].NivelesEducacionPerfil = wse.PerfilEducacionNivelQryLoadListForUsuario(
                    0, 1000, usr.UsuarioId);

                usr.PerfilEducacion[i].NiveltrabajoId = perfil.NiveltrabajoId;
                usr.PerfilEducacion[i].NombreNivelTrabajo = perfil.NombreNivelTrabajo;
                usr.PerfilEducacion[i].NombrePerfil = perfil.NombrePerfil;
                usr.PerfilEducacion[i].PerfilEducacionId = perfil.PerfilEducacionId;
                usr.PerfilEducacion[i].SostenimientoPerfil = wse.PerfilEducacionSostenimientoQryLoadListForUsuario(
                    0, 1000, usr.UsuarioId);
                usr.PerfilEducacion[i].UsuarioId = perfil.UsuarioId;
                usr.PerfilEducacion[i].UsuIdUsuario = perfil.UsuIdUsuario;

                i++;

            }
        }


        private static SistemaPuertoQryDP getSistemaPort(string port)
        {
            // Eliminar las opciones de puertos JAEC 29/10/2009
            //

            //if (port.Length != 0)
            //{
            //    SistemaPuertoQryDP[] sis = wse.SistemaPuertoQryLoadListForPuerto(0, 1, port);
            //    if (sis.Length != 0)
            //        return sis[0];
            //}
            return new SistemaPuertoQryDP();            
        } 



        private static void GetPagina(UsuarioSeDP usr, HttpContext pagina)
        {
//            
            if (pagina.Request.RawUrl.ToUpper().Contains("CATALAGOS.ASMX"))
                return;
            
            // Parche JEscalera 28/10/2009, hay que revizar con rodrox.
            //usr.PaginaActual = null;
            
            if (usr.PaginaActual == null)
            {
                usr.PaginaActual = new PaginaDP();
                usr.PaginaActual.RawUrl = "";
                usr.PaginaActual.OpcionSeleccionada = new AccionDP();
                usr.PaginaActual.Acciones = new AccionDP[0];
                usr.PaginaActual.NivelEducacion = new NivelEducacionNvoDP[0];
                usr.PaginaActual.NivelTrabajo = new NiveltrabajoDP[0];
                usr.PaginaActual.Sostenimiento = new SostenimientoDP[0];
            }

            string rawUrl = pagina.Request.RawUrl.ToUpper();

            //prov.FindSiteMapNode(rawUrl);
            // SiteMapNode nodo = null;
            string llamadaUrl = "";
            string llave = "0|0|0|0";


            // Reparacion de pagina cuando cruza aplicaci�n, forzada.
            // JAEC 13/11/2009
            if (!usr.PaginaActual.RawUrl.Replace("~/", "").ToUpper().Contains(rawUrl.Replace("~/", ""))||
                usr.PaginaActual.RawUrl == string.Empty
                ||rawUrl.ToUpper().Contains("REFRESH")
                )
            {

                getConexion();
                // Eliminar las opciones de puertos JAEC 29/10/2009
                //
                //if ((pagina.Request.Url.Port != 80) && (pagina.Request.Url.Port != 8044))
                //{
                //    llamadaUrl = "http://localhost:" + pagina.Request.Url.Port + "/" + rawUrl.Replace("~/../", "/");
                //}
                //else
                    llamadaUrl = rawUrl.Replace("~/../", "/");
                   //llamadaUrl = rawUrl.Replace("/SEP/","/");
                //llave = wse.GetLlavesCarpeta(usr.UsuarioId, llamadaUrl);
                if (llave == "0|0|0|2") {
                    usr.PaginaActual.RawUrl = "";
                    usr.PaginaActual.OpcionSeleccionada = new AccionDP();
                    usr.PaginaActual.Acciones = new AccionDP[0];
                    usr.PaginaActual.NivelEducacion = new NivelEducacionNvoDP[0];
                    usr.PaginaActual.NivelTrabajo = new NiveltrabajoDP[0];
                    usr.PaginaActual.Sostenimiento = new SostenimientoDP[0];
                }
                else
                    if (llave != "0|0|0|0") {
                        if (usr.PaginaActual.RawUrl.Length == 0)
                            usr.PaginaActual.RawUrl = "ENTRA||| |";
                    }
                
            }
            // si no existe el nodo, no entro para copiar las opciones anteriores (compatibilidad)
            if ((llave != "0|0|0|0"))
            {
                // entra solo si cambie de pagina.
                if (
                    !rawUrl.Contains(usr.PaginaActual.RawUrl.ToUpper())
                    // Reparacion de pagina cuando cruza aplicaci�n, forzada.
                    // JAEC 13/11/2009
                    || rawUrl.ToUpper().Contains("REFRESH")
                    )
                {
                    #region Inicializo Llaves
                    string[] llaves = llave.Split('|');
                    byte sistema = Convert.ToByte(llaves[0]);
                    byte modulo = Convert.ToByte(llaves[1]);
                    byte opcion = Convert.ToByte(llaves[2]);
                    byte tieneAster = Convert.ToByte(llaves[3]);
                    bool bSistema = false;
                    #endregion

                    // Reviso que si cambie de sistema
                    if ((usr.PaginaActual.Acciones != null) && (usr.PaginaActual.Acciones.Length != 0))
                        if (sistema == usr.PaginaActual.Acciones[0].SistemaId)
                            bSistema = true;
                    // Actualizo los datos, sino me quedo con los datos anteriores.
                    usr.PaginaActual.RawUrl = rawUrl;
                    // Cargo las acciones de la p�gina.
                    getConexion();
                    GetAcciones(usr, sistema, modulo, opcion, tieneAster);

                    // Si cambie de sistema, actualizo la seguridad de la p�gina.
                    if (!bSistema)
                    {
                        GetSeguridadPagina(usr, sistema);
                    }
                }
            }
            else
                if (rawUrl.ToUpper().Contains("DEFAULT.ASPX"))

                    if (usr.PaginaActual.Acciones != null)
                    {
                        usr.PaginaActual.Acciones = new AccionDP[0];
                        usr.PaginaActual.OpcionSeleccionada = new AccionDP();
                    }
            // if ((usr.PaginaActual.Acciones != null) && (usr.PaginaActual.Acciones.Length != 0))
            usr.PaginaActual.RawUrl = rawUrl.Replace("~/../", "/");
        }

        private static void GetSeguridadPagina(UsuarioSeDP usr, byte sistema)
        {
            NivelEducacionNvoDP[] nivelArray = new NivelEducacionNvoDP[0];
            NiveltrabajoDP[] nivelTrabajoArray = new NiveltrabajoDP[0];
            SostenimientoDP[] sostenimientoArray = new SostenimientoDP[0];

            foreach (PerfilEducacionUsuarioDP perfil in usr.PerfilEducacion)
            {
                foreach (PerfilEducacionNivelQryDP perf in perfil.NivelesEducacionPerfil)
                {
                    if (perf.SistemaId == sistema)
                    {
                        nivelArray = GetNivelEducacionBasica(usr, nivelArray, perf);
                        nivelTrabajoArray = GetNivelTrabajo(usr, nivelTrabajoArray, perfil);
                    }
                }

                foreach (PerfilEducacionSostenimientoQryDP perf in perfil.SostenimientoPerfil)
                {
                    if (perf.SistemaId == sistema)
                    {
                        sostenimientoArray = GetSostenimiento(usr, sostenimientoArray, perf);
                    }
                }
            }

            usr.PaginaActual.NivelEducacion = nivelArray;
            usr.PaginaActual.NivelTrabajo = nivelTrabajoArray;
            usr.PaginaActual.Sostenimiento = sostenimientoArray;
        }

        private static SostenimientoDP[] GetSostenimiento(UsuarioSeDP usr, SostenimientoDP[] sostenimientoArray, PerfilEducacionSostenimientoQryDP perf)
        {
            #region Sostenimiento
            Array.Resize<SostenimientoDP>(ref sostenimientoArray, sostenimientoArray.Length+1);

            SostenimientoDP sostenimiento = new SostenimientoDP();

            sostenimiento.BitActivo = true;
            sostenimiento.Bitestatal = perf.Bitestatal;
            sostenimiento.Bitfederal = perf.Bitfederal;
            sostenimiento.Bitparticular = perf.Bitparticular;
            sostenimiento.FechaActualizacion = "";
            sostenimiento.Nombre = perf.NombreSostenimiento;
            sostenimiento.SostenimientoId = perf.SostenimientoId;
            sostenimiento.UsuarioId = usr.UsuarioId;
            sostenimientoArray[sostenimientoArray.Length-1] = sostenimiento;
            #endregion
            return sostenimientoArray;
        }

        private static NiveltrabajoDP[] GetNivelTrabajo(UsuarioSeDP usr, NiveltrabajoDP[] nivelTrabajoArray, PerfilEducacionUsuarioDP perfil)
        {
            #region Nivel de trabajo

            Array.Resize<NiveltrabajoDP>(ref nivelTrabajoArray, nivelTrabajoArray.Length + 1);
            NiveltrabajoDP nivelTrabajo = new NiveltrabajoDP();
            nivelTrabajo.Abreviatura =
                perfil.AbreviaturaNivelTrabajo;
            nivelTrabajo.BitActivo = true;
            nivelTrabajo.FechaActualizacion = "";
            nivelTrabajo.NiveltrabajoId =
                 perfil.NiveltrabajoId;
            nivelTrabajo.Nombre =
                perfil.NombreNivelTrabajo;
            nivelTrabajo.UsuarioId =
                usr.UsuarioId;
            nivelTrabajoArray[nivelTrabajoArray.Length-1] = nivelTrabajo;
            return nivelTrabajoArray;
            #endregion
        }

        private static Boolean IsNivelEducacion(NivelEducacionNvoDP[] source, Int16 nivelId, Int16 tipoEducacionId) 
        {
            Boolean resultado = false;
            foreach (NivelEducacionNvoDP linea in source) 
            { 
                if ((linea.TipoeducacionId == tipoEducacionId) &&(linea.NivelId == nivelId )) {
                    resultado = true;
                    break;
                }
            }
            return resultado;
        }

        private static Int32 GetIndexNivelEducacion(NivelEducacionNvoDP[] source, Int16 nivelId, Int16 tipoEducacionId) {
            Int32 i = 0;
            foreach (NivelEducacionNvoDP linea in source)
            {
                if ((linea.TipoeducacionId == tipoEducacionId) && (linea.NivelId == nivelId))
                {
                    break;
                }
                i++;
            }
            return i;
        }

        private static void GetSubNivel(NivelEducacionNvoDP source, PerfilEducacionNivelQryDP perf, Int32 usuarioId, SubnivelDP[] arreglo ) 
        {
            if (perf.SubnivelId != 0)
            {
                if (source.SubNiveles == null)
                {
                    source.SubNiveles = new SubnivelDP[0];
                    arreglo = source.SubNiveles;
                }
                SubnivelDP sub = new SubnivelDP();
                sub.BitActivo = true;
                sub.FechaActualizacion = "";
                sub.NivelId = Convert.ToByte(perf.NivelId);
                sub.Nombre = perf.NombreSubnivel;
                sub.SubnivelId = perf.SubnivelId;
                sub.TipoeducacionId = Convert.ToInt16(sub.NivelId.ToString()[0]);
                sub.UsuarioId = 0;
                Array.Resize<SubnivelDP>(ref arreglo, arreglo.Length + 1);                
                // sub.UsuarioId = usuarioId;
            }
            else
            {
                source.SubNiveles = new SubnivelDP[0];
            }
        }


        private static NivelEducacionNvoDP[] GetNivelEducacionBasica(UsuarioSeDP usr, NivelEducacionNvoDP[] nivelArray, PerfilEducacionNivelQryDP perf)
        {
            #region Nivel de Educacion
            NivelEducacionNvoDP nivel = new NivelEducacionNvoDP();

            if (!IsNivelEducacion(nivelArray, Convert.ToByte(perf.NivelId), Convert.ToInt16(nivel.NivelId.ToString()[0])))
            {
                nivel.BitActivo = true;
                nivel.FechaActualizacion = "";
                nivel.LetraFolio = perf.LetraFolio;
                nivel.NivelId = Convert.ToByte(perf.NivelId);
                nivel.Nombre = perf.NombreNivel;
                nivel.TipoeducacionId = Convert.ToInt16(nivel.NivelId.ToString()[0]);
                nivel.UsuarioId = usr.UsuarioId;
                Array.Resize<NivelEducacionNvoDP>(ref nivelArray, nivelArray.Length + 1);
                nivelArray[nivelArray.Length - 1] = nivel;
            }
            int i = GetIndexNivelEducacion(nivelArray, Convert.ToByte(perf.NivelId), Convert.ToInt16(nivel.NivelId.ToString()[0]));
            GetSubNivel(nivelArray[i], perf, usr.UsuarioId, nivelArray[i].SubNiveles);
            #endregion
            return nivelArray;
        }

        private static void GetAcciones(UsuarioSeDP usr, byte sistema, byte modulo, byte opcion, byte tieneAster)
        {
            AccionesQryDP[] acciones = wse.AccionesQryLoadListForAccion(
                0, 100, sistema, modulo, opcion, 0, true);
            usr.PaginaActual.Acciones = new AccionDP[acciones.Length];
            int i = 0;
            foreach (AccionesQryDP accion in acciones)
            {
                #region Acciones
                usr.PaginaActual.Acciones[i] = new AccionDP();
                usr.PaginaActual.Acciones[i].Abreviatura = accion.Abreviatura;
                usr.PaginaActual.Acciones[i].AccionId = accion.AccionId;
                usr.PaginaActual.Acciones[i].BitActivo = accion.BitActivo;
                usr.PaginaActual.Acciones[i].FechaActualizacion = accion.FechaActualizacion;
                usr.PaginaActual.Acciones[i].FechaFin = accion.FechaFin;
                usr.PaginaActual.Acciones[i].FechaInicio = accion.FechaInicio;
                usr.PaginaActual.Acciones[i].ModuloId = modulo;
                usr.PaginaActual.Acciones[i].Nombre = accion.Nombre;
                OpcionPk opcionPk = new OpcionPk();
                opcionPk.ModuloId = modulo;
                opcionPk.OpcionId = opcion;
                opcionPk.SistemaId = sistema;
                usr.PaginaActual.Acciones[i].Opcion = wse.OpcionLoad(opcionPk);

                usr.PaginaActual.Acciones[i].OpcionId = opcion;
                usr.PaginaActual.Acciones[i].SistemaId = sistema;
                usr.PaginaActual.Acciones[i].UsuarioId = usr.UsuarioId;
                i++;
                #endregion
            }
            if (tieneAster == 0) {
                if (usr.PaginaActual.Acciones.Length != 0) 
                {
                    if (usr.PaginaActual.OpcionSeleccionada == null)
                        usr.PaginaActual.OpcionSeleccionada = new AccionDP();
                    usr.PaginaActual.OpcionSeleccionada.Abreviatura = 
                        usr.PaginaActual.Acciones[0].Abreviatura;
                    usr.PaginaActual.OpcionSeleccionada.AccionId =
                        usr.PaginaActual.Acciones[0].AccionId;
                    usr.PaginaActual.OpcionSeleccionada.BitActivo =
                        usr.PaginaActual.Acciones[0].BitActivo;
                    usr.PaginaActual.OpcionSeleccionada.FechaActualizacion =
                        usr.PaginaActual.Acciones[0].FechaActualizacion;
                    usr.PaginaActual.OpcionSeleccionada.FechaFin =
                        usr.PaginaActual.Acciones[0].FechaFin;
                    usr.PaginaActual.OpcionSeleccionada.FechaInicio =
                        usr.PaginaActual.Acciones[0].FechaInicio;
                    usr.PaginaActual.OpcionSeleccionada.ModuloId =
                        usr.PaginaActual.Acciones[0].ModuloId;
                    usr.PaginaActual.OpcionSeleccionada.Nombre =
                        usr.PaginaActual.Acciones[0].Nombre;
                    usr.PaginaActual.OpcionSeleccionada.Opcion =
                        usr.PaginaActual.Acciones[0].Opcion;
                    usr.PaginaActual.OpcionSeleccionada.OpcionId =
                        usr.PaginaActual.Acciones[0].OpcionId;
                    usr.PaginaActual.OpcionSeleccionada.SistemaId =
                        usr.PaginaActual.Acciones[0].SistemaId;
                    usr.PaginaActual.OpcionSeleccionada.UsuarioId =
                        usr.PaginaActual.Acciones[0].UsuarioId;
                }
            }
            // Obtengo la seleccion previa.
            DeserializeAndGet(usr);
        }

        public static bool ExistePerfil(PerfilDP[] listaPerfiles, int perfilCompare)
        { 
            bool result  = false;
                foreach (PerfilDP perfiltmp in listaPerfiles)
                {
                    if (perfiltmp.PerfilId == perfilCompare)
                    {
                        result = true;
                        break;
                    }
                }
                return result;
        }
        
    }
}
