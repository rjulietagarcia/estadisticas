using System;
using System.Collections.Generic;
using System.Text;
//using Mx.Com.Cimar.Supports.Web.MemberShip;
//using Mx.Gob.Nl.Educacion.WsSESeguridad;
using System.Security.Principal;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web;
//using Mx.Com.Cimar.Supports.Web.SiteMapProvider;
using System.Collections;
using System.IO;
using SEroot.WsSESeguridad;

namespace Mx.Gob.Nl.Educacion
{
    public class SepSecurity
    {
        WsSESeguridad wse;

        public SepSecurity() {
            cargaSeguridad();
        }
        public void cargaSeguridad() {
            wse = new WsSESeguridad();
            if (System.Configuration.ConfigurationManager.AppSettings["WebServiceSeguridad"] == null)
            {
                throw new Exception("Necesitar crear la definici�n para el servicio 'WebServiceSeguridad' dentro de la secci�n AppSettings");
            }
            else
            wse.Url = System.Configuration.ConfigurationManager.AppSettings["WebServiceSeguridad"];
            // TODO: Poner aqui el URL.
        }

        public bool ValidateUser(string username, string password) 
        {
            ValidateUserQryDP[] vu = 
                wse.ValidateUserQryLoadListForUsuarioPassword(0, 1, username, password);
            bool boolReturn = ((vu != null) && (vu.Length == 1));
            return boolReturn;
        }
        //public MembershipUserDP[] GetUserForSid(object sid) 
        //{
        //    int realSid = (int)sid;
        //    MembershipUserQryDP[] usrs = wse.MembershipUserQryLoadListForSid(0, 1, realSid);
        //    return ConvertMemberShip(usrs);
        //}

        //private static MembershipUserDP[] ConvertMemberShip(MembershipUserQryDP[] usrs)
        //{
        //    MembershipUserDP[] resultado = null;
        //    if (usrs != null)
        //    {
        //        resultado = new MembershipUserDP[usrs.Length];
        //        int i = 0;
        //        foreach (MembershipUserQryDP usr in usrs)
        //        {
        //            resultado[i] = new MembershipUserDP();
        //            resultado[i].Comment = usr.Comment;
        //            resultado[i].Creationdate = usr.Creationdate;
        //            resultado[i].Email = usr.Email;
        //            resultado[i].Isapproved = usr.Isapproved;
        //            resultado[i].Islockedout = usr.Islockedout;
        //            resultado[i].Lastactivitydate = usr.Lastactivitydate;
        //            resultado[i].Lastlockedoutdate = usr.Lastlockedoutdate;
        //            resultado[i].Lastlogindate = usr.Lastlogindate;
        //            resultado[i].Lastpasswordchangeddate = usr.Lastpasswordchangeddate;
        //            resultado[i].Login = usr.Login;
        //            resultado[i].Password = usr.Password;
        //            resultado[i].Passwordquestion = usr.Passwordquestion;
        //            resultado[i].Providername = usr.Providername;
        //            resultado[i].Provideruserkey = usr.Provideruserkey;
        //            resultado[i].Username = usr.Username;
        //            resultado[i].UsuarioId = usr.UsuarioId;
        //        }
        //    }
        //    return resultado;
        //}
        
        //public MembershipUserDP[] GetUserForUserName(string username) 
        //{
        //    string[] parametros = username.Split('|');
        //    int realSid = Convert.ToInt32(parametros[0]);
        //    MembershipUserQryDP[] usrs = wse.MembershipUserQryLoadListForSid(0, 1, realSid);
        //    return ConvertMemberShip(usrs);
        //}

        public string LoginIn(string username, string password) {
            if (HttpContext.Current != null)
                if (HttpContext.Current.Session != null)
                    HttpContext.Current.Session.Clear();
            return wse.LoginIn(username, password);
        }

        public void saveLog(int opcionId, string username, string ip) 
        {
            string[] parametros = username.Split('|');
            int realSid = Convert.ToInt32(parametros[0]);
            AccesologDP acceso = new AccesologDP();
            acceso.Fecha = DateTime.Now.ToString();
            acceso.Ip = ip;
            acceso.LogId = 0;
            acceso.OpcionId = opcionId;
            acceso.UsuarioId = realSid;
            try
            {
                wse.AccesologSave(acceso);
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public StringDictionary GetUserPermision(Page page) 
        {
            // SeguridadSE.ValidateCrossUser();
            HttpContext lPage = HttpContext.Current;
            StringDictionary resultado = null;
            if ((lPage.Session["opcionesUsuario"] == null) || (!(lPage.Session["opcionesUsuario"] is StringDictionary)))
            {
                string username = lPage.User.Identity.Name;
                resultado = new StringDictionary();
                resultado.Add("0|0|0", "true");
                string sOpciones = wse.GetUserPermision(username);
                if (sOpciones.Length != 0)
                {
                    string[] opciones = sOpciones.Split(',');
                    foreach (string opcion in opciones)
                    {
                        string[] llave = opcion.Split('=');
                        string[] llaves = llave[0].Split('|');
                        if (!resultado.ContainsKey(llaves[0] + "|0|0"))
                            resultado.Add(llaves[0] + "|0|0", "true");
                        if (!resultado.ContainsKey(llaves[0] + "|" + llaves[1] + "|0"))
                            resultado.Add(llaves[0] + "|" + llaves[1] + "|0", "true");
                        resultado.Add(llave[0], llave[1]);
                    }
                }
                lPage.Session["opcionesUsuario"] = resultado;
            }
            else
            {
                resultado = (StringDictionary)lPage.Session["opcionesUsuario"];
            }
            return resultado;
        }

        //public SiteMapNode BuildSiteMap(CimarSiteMapProvider siteMapProvider, ref SiteMapNode sourceNode)
        //{
        //    if ((sourceNode != null) && (sourceNode.HasChildNodes))
        //        return sourceNode;
            
        //    string llave = "0|0|0";
        //    if (sourceNode == null)
        //    {
        //        sourceNode = new SiteMapNode(siteMapProvider,
        //            llave,
        //            "~/Default.aspx",
        //            "Inicio",
        //            "P�gina de Inicio");
        //    }
        //    else
        //    {
        //        llave = sourceNode.Key;
        //    }

        //    string[] llaves = llave.Split('|');
        //    if ((llaves[0] == "0") && (llaves[1] == "0") && (llaves[2] == "0"))
        //        GetSistemas(ref siteMapProvider, ref sourceNode);
        //    else
        //        if ((llaves[0] != "0") && (llaves[1] == "0") && (llaves[2] == "0"))
        //            GetModulos(siteMapProvider, llave, ref sourceNode);
        //        else
        //            if ((llaves[0] != "0") && (llaves[1] != "0") && (llaves[2] == "0"))
        //                GetOpciones(siteMapProvider, llave, ref sourceNode);
        //    return sourceNode;
        //}

        //private string GetSistemas(ref CimarSiteMapProvider siteMapProvider, ref SiteMapNode sourceNode)
        //{
        //    #region Sistemas
        //    string llave = "0|0|0";
        //    string[] llaves = llave.Split('|');
        //    // Incluyo ahora los sistemas.
        //    if ((llaves[0] == "0") && (llaves[1] == "0") && (llaves[2] == "0"))
        //    {
        //        SiteMapNode childNode = null;
        //        SistemaQryDP[] sistemas = wse.SistemaQryLoadListForSistema(0, 100, (byte)0);
        //        foreach (SistemaQryDP sistema in sistemas)
        //        {
        //            llave = sistema.SistemaId.ToString() + "|0|0";
        //            childNode = new SiteMapNode(siteMapProvider,
        //                                        llave,
        //                                        "~/Default.aspx?Sistema=" + sistema.SistemaId.ToString(),
        //                                        sistema.Nombre,
        //                                        sistema.CarpetaSistema);
        //            siteMapProvider.AddNodeNow(childNode, sourceNode);
        //            // childNode.Roles = "*";
        //            llaves = GetModulos(siteMapProvider, llave, ref childNode);

        //        }
        //    }
        //    #endregion
        //    return llave;
        //}

        //private string[] GetModulos(CimarSiteMapProvider siteMapProvider, 
        //    string llave, 
        //    ref SiteMapNode childNode)
        //{
        //    string[] llaves;
        //    // Casi seguro que aqui van los m�dulos
        //    #region Modulos
        //    // Incluyo ahora los m�dulos de un sistema
        //    llaves = llave.Split('|');
        //    if ((llaves[0] != "0") && (llaves[1] == "0") && (llaves[2] == "0"))
        //    {
        //        byte lSistema = Convert.ToByte(llaves[0]);
        //        SiteMapNode childNodeModulo = null;
        //        ModuloQryDP[] modulos = wse.ModuloQryLoadListForModulo(0, 100, lSistema, 0);
        //        foreach (ModuloQryDP modulo in modulos)
        //        {
        //            llave = lSistema + "|" + modulo.ModuloId.ToString() + "|0";
        //            childNodeModulo = new SiteMapNode(siteMapProvider,
        //                                        llave,
        //                                        String.Format("~/Default.aspx?Sistema={0}&Modulo={1}",
        //                                        lSistema.ToString(),
        //                                        modulo.ModuloId.ToString()),
        //                                        modulo.Nombre,
        //                                        modulo.Abreviatura);
        //            // childNodeModulo.Roles = "*";
        //            siteMapProvider.AddNodeNow(childNodeModulo, childNode);

        //            llaves = GetOpciones(siteMapProvider, llave, ref childNodeModulo);
        //        }
        //    }
        //    #endregion
        //    return llaves;
        //}

        //private string[] GetOpciones(CimarSiteMapProvider siteMapProvider,
        //    string llave, 
        //    ref SiteMapNode childNodeModulo)
        //{
        //    string[] llaves;
        //    int lUsr = 0;
        //    string userName = "0";
        //    if (HttpContext.Current != null) {
        //        userName = HttpContext.Current.User.Identity.Name.Trim();
        //    }
        //    int.TryParse(userName.Split('|')[0], out lUsr);
        //    if (lUsr == 0)
        //        lUsr = 500;
            
        //    string parentNodeTitle = "";
        //    string urlEncode = "";

        //    #region opciones
        //    // Incluyo ahora las opciones de un sistema
        //    llaves = llave.Split('|');
        //    if ((llaves[0] != "0") && (llaves[1] != "0") && (llaves[2] == "0"))
        //    {
        //        byte lSistema = Convert.ToByte(llaves[0]);
        //        byte lModulo = Convert.ToByte(llaves[1]);
        //        SiteMapNode childNodeOpcion = null;
        //        // Eliminar las opciones de puertos JAEC 29/10/2009
        //        //
        //        //int puerto = HttpContext.Current.Request.Url.Port;

        //        OpcionQryDP[] opciones = wse.OpcionQryLoadListForOpcion(0, 100, lSistema, lModulo, 0);

        //        foreach (OpcionQryDP opcion in opciones)
        //        {
        //            llave = lSistema + "|" + lModulo.ToString() + "|" + opcion.OpcionId.ToString();
        //            urlEncode = HttpContext.Current.Server.UrlEncode(opcion.CarpetaOpcion);
        //            string sourcePath = "";

        //            // Eliminar las opciones de puertos JAEC 29/10/2009
        //            //
        //            //if ((puerto != 80) && (
        //            //     puerto != 8044))
        //            //    sourcePath = "~/MainMenu.aspx?usuario={0}&Sistema={1}&nombre={2}&returnPath={3}";
        //            //else
        //                sourcePath = "~/../SeguridadGlobal/MainMenu.aspx?usuario={0}&Sistema={1}&nombre={2}&returnPath={3}";

        //            if (opcion.SistemaPuerto.Length != 0)
        //            {
        //                // Eliminar las opciones de puertos JAEC 29/10/2009
        //                //
        //                //if ((puerto != 80) && (
        //                //     puerto != 8044))
        //                //    childNodeOpcion = new SiteMapNode(siteMapProvider,
        //                //                            llave,
        //                //                            "~/" + opcion.CarpetaOpcion,
        //                //                            opcion.Nombre,
        //                //                            opcion.Abreviatura);
        //                //else
        //                    childNodeOpcion = new SiteMapNode(siteMapProvider,
        //                                            llave,
        //                                            "~/../" + opcion.CarpetaOpcion,
        //                                            opcion.Nombre,
        //                                            opcion.Abreviatura);
        //            }
        //            else
        //            {
        //                if (parentNodeTitle.Length == 0)
        //                {
        //                    SistemaQryDP[] sistemas = wse.SistemaQryLoadListForSistema(0, 100, lSistema);
        //                    if ((sistemas != null) && (sistemas.Length != 0))
        //                        parentNodeTitle = sistemas[0].Nombre;
        //                }

        //                childNodeOpcion = new SiteMapNode(siteMapProvider,
        //                                            llave,
        //                                            String.Format(sourcePath,
        //                                                lUsr, lSistema, parentNodeTitle, urlEncode
        //                                                ),
        //                                            opcion.Nombre,
        //                                            opcion.Abreviatura);
        //            }
        //            siteMapProvider.AddNodeNow(childNodeOpcion, childNodeModulo);
        //        }
        //    }
        //    #endregion
        //    return llaves;
        //}

        public Boolean GetAccessPage(string RawUrl, Page pagina) 
        {
            UsuarioSeDP usr =
                SeguridadSE.GetUsuario(pagina);
            if ((RawUrl.ToUpper().Contains("Default.aspx".ToUpper())) || (RawUrl.ToUpper().Contains("Index.aspx".ToUpper())) || (RawUrl.ToUpper().Contains("NuevaPagina.aspx".ToUpper())) || (RawUrl.ToUpper().Contains("AccesoDenegado.aspx".ToUpper())) || (RawUrl.ToUpper().Contains("Logon.aspx".ToUpper())) || (RawUrl.ToUpper().Contains("Index.aspx".ToUpper())) || (RawUrl.ToUpper().Contains("Logout.aspx".ToUpper())))
            {
                return true;
            }
            else
            {
                if (usr.PaginaActual != null)
                    return (
                        (RawUrl.ToUpper().Replace("~/", "").Contains(usr.PaginaActual.RawUrl.Replace("~/", "").ToUpper()))
                        ||
                        (usr.PaginaActual.RawUrl.Replace("~/", "").ToUpper().Contains(RawUrl.ToUpper().Replace("~/", "")))
                        )
                        ;
                else
                    return false;
            }
        }

        private void doEliminaTemporales()
        {
            string pathPDF = HttpContext.Current.Server.MapPath("~/PDFReports/");
            if (Directory.Exists(pathPDF))
            {
                string[] lArchivos = Directory.GetFiles(pathPDF, "*.pdf");
                foreach (string lArchivo in lArchivos)
                {
                    if (File.Exists(lArchivo))
                        File.Delete(lArchivo);
                }
            }
            else 
            {
                try
                {
                    Directory.CreateDirectory(pathPDF);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }

        }

        protected Hashtable tabla = new Hashtable(6);

        public void OnApplication_Start(object sender, EventArgs e)
        {
            HttpContext.Current.Application.Lock();
            try
            {
                if (HttpContext.Current.Application["sesionesactivas"] == null)
                    HttpContext.Current.Application["sesionesactivas"] = 0;
                if (HttpContext.Current.Application["sesionesultimas"] == null)
                    HttpContext.Current.Application["sesionesultimas"] = tabla;
                // HARDCODE POR AHORA PARA CICLOESCOLAR ACTUAL NIVEL 1(BASICA)
                if (HttpContext.Current.Application["cicloescolaractualid"] == null)
                {
                    try
                    {
                        //CicloEscolarActualQryDP[] qry = wse.CicloEscolarActualQryLoadListForTipoCiclo(0, 1, 1);
                        //if ((qry != null) && (qry.Length != 0))
                        //    HttpContext.Current.Application["cicloescolaractualid"] = qry[0].CicloescolarId.ToString();
                        //else

                        SEroot.WSEscolar.WsEscolar ws = new SEroot.WSEscolar.WsEscolar();
                        HttpContext.Current.Application["cicloescolaractualid"] = ws.LoadCiclo(1).cicloActual.cicloEscolarId.ToString(); // HARDCODE POR AHORA PARA CICLOESCOLAR ACTUAL NIVEL 1(BASICA)
                    }
                    catch
                    {
                        HttpContext.Current.Application["cicloescolaractualid"] = 5;
                    }
                }
                doEliminaTemporales();
            }
            finally {
                HttpContext.Current.Application.UnLock();
            }
        }

        public void OnSession_Start(Object sender, EventArgs e)
        {
            //HttpContext.Current.Application.Lock();
            //try
            //{
            //    if (HttpContext.Current.Application["sesionesactivas"] == null)
            //    {
            //        HttpContext.Current.Application["sesionesactivas"] = 0;
            //    }
            //    HttpContext.Current.Application["sesionesactivas"] = (int)HttpContext.Current.Application["sesionesactivas"] + 1;
            //    UpdateSessionesTabla();
            //    HttpContext.Current.Session["dsGrid"] = null;
            //}
            //finally
            //{
            //    HttpContext.Current.Application.UnLock();
            //}
        }

        private void UpdateSessionesTabla()
        {
            //string llave = DateTime.Today.ToShortDateString() + " H" + DateTime.Now.Hour.ToString();
            //if (HttpContext.Current.Application["sesionesultimas"] != null)
            //    tabla = (Hashtable)HttpContext.Current.Application["sesionesultimas"];
            //if (tabla.ContainsKey(llave))
            //    tabla.Remove(llave);
            //tabla.Add(llave, HttpContext.Current.Application["sesionesactivas"]);
            //HttpContext.Current.Application["sesionesultimas"] = tabla;
        }

        public void OnSession_End(Object sender, EventArgs e)
        {
            //if (HttpContext.Current != null)
            //{
            //    if (HttpContext.Current.Application != null)
            //    {
            //        HttpContext.Current.Application.Lock();
            //        try
            //        {
            //            UsuarioSeDP usr = SeguridadSE.GetUsuario(HttpContext.Current);
            //            SeguridadSE.DeserializeAndGet(usr);
            //            if (HttpContext.Current.Application["sesionesactivas"] != null)
            //            {
            //                HttpContext.Current.Application["sesionesactivas"] = (int)HttpContext.Current.Application["sesionesactivas"] - 1;
            //                UpdateSessionesTabla();
            //            }
            //        }
            //        finally
            //        {
            //            HttpContext.Current.Application.UnLock();
            //        }
            //    }
            //}
        }

        public void OnApplication_End(object sender, EventArgs e)
        {
            //if (HttpContext.Current != null)
            //{
            //    if (HttpContext.Current.Application != null)
            //    {
            //        HttpContext.Current.Application.Lock();
            //        try
            //        {
            //            if (HttpContext.Current.Application["sesionesactivas"] != null)
            //            {
            //                HttpContext.Current.Application["sesionesactivas"] = 0;
            //                doEliminaTemporales();
            //            }
            //        }
            //        finally
            //        {
            //            HttpContext.Current.Application.UnLock();
            //        }
            //    }
            //}
        }

        //private string changeRawPath(string source, SistemaPuertoQryDP sistema) 
        //{
        //    if ((sistema.Puerto!=null) &&(sistema.Puerto.Length != 0))
        //    {
        //        source = source.ToUpper();
        //        if (source.StartsWith("~/"))
        //            source = source.Remove(0, 2);
        //        if (source.StartsWith("/"))
        //            source = source.Remove(0, 1);
        //        if (source.StartsWith(sistema.CarpetaSistema.ToUpper()))
        //            return "http://localhost:" + sistema.Puerto + "/" + source.Remove(0, sistema.CarpetaSistema.Length);
        //    }
        //    return "";
        //}

        //private SistemaPuertoQryDP getSistema(string sourceUrl) 
        //{
        //    string source = sourceUrl.ToUpper();
        //    if (source.StartsWith("~/"))
        //        source = source.Remove(0, 2);
        //    if (source.StartsWith("/"))
        //        source = source.Remove(0, 1);
        //    string[] carpetas = source.Split('/');
        //    if (carpetas.Length != 0) {
        //        SistemaPuertoQryDP[] sis = wse.SistemaPuertoQryLoadListForCarpeta(0,1 ,carpetas[0].ToUpper()+"/");
        //        if (sis.Length != 0)
        //            return sis[0];
        //    }
        //    return new SistemaPuertoQryDP();
        //}

        public void OnApplication_Error(object sender, EventArgs e)
        {
            System.Web.HttpApplication lApp = ((System.Web.HttpApplication)(sender));
            Exception ex = lApp.Context.Error;
            if (HttpContext.Current != null)
                if (HttpContext.Current.Session != null)
                    HttpContext.Current.Session["ErrorApplicacion"] = ex;
            //if (lApp.Request.Url.Authority.Contains(":"))
            //    if (lApp.Request.Url.Port != 8044)
            HttpContext.Current.Trace.Warn("SepSecurity.OnApplication_Error", ex.Message, ex);
                    //System.Diagnostics.EventLog.WriteEntry("SE", ex.Message, System.Diagnostics.EventLogEntryType.Information);

            //if ((ex != null) && (lApp.Context.Error is HttpException) && (ex.Message.Contains("does not exist") || ex.Message.Contains(" no existe.")))
            //{
            //    if (!lApp.Request.RawUrl.ToUpper().Contains("ACCESODENEGADO.ASPX"))
            //    {

                    // Eliminar las opciones de puertos JAEC 29/10/2009
                    //
                    // HAY QUE INCLUIR LA PANALLA DE ERRORES AQUI.
                    //
                    //string sourceUrl = lApp.Request.RawUrl;
                    //if (lApp.Request.Url.Authority.Contains(":"))
                    //{
                    //    //if (lApp.Request.Url.Port != 8044)
                    //    //{
                    //        // Reemplazo el inicio por el nuevo puerto.
                    //        lApp.Server.ClearError();
                    //        lApp.Response.Redirect(changeRawPath(sourceUrl,
                    //            getSistema(sourceUrl))
                    //            );
                    //    //}
                    //}
            //    }
            //}
            if (lApp !=null)
                lApp.Response.Redirect("~/Errores.aspx");
        }

        ///// <summary>
        ///// Cambia la ruta del Sitemap Navigator
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        ///// <returns></returns>
        //public SiteMapNode ChangePorts(Object sender,
        //                             SiteMapResolveEventArgs e) 
        //{
            
        //    SiteMapNode currentNode = null;
        //    if (e.Provider.CurrentNode != null)
        //        currentNode = e.Provider.CurrentNode.Clone(true);
        //    if (currentNode == null)
        //    {
        //        byte sistemaId = 0;
        //        byte moduloId = 0;
        //        byte opcionId = 0;
        //        string llave = "0|0|0";
        //        UsuarioSeDP usr = SeguridadSE.GetUsuario(HttpContext.Current);
        //        if (usr.PaginaActual != null) {
        //            if (usr.PaginaActual.OpcionSeleccionada != null)
        //            {
        //                sistemaId = usr.PaginaActual.OpcionSeleccionada.SistemaId;
        //                moduloId = usr.PaginaActual.OpcionSeleccionada.ModuloId;
        //                opcionId = usr.PaginaActual.OpcionSeleccionada.OpcionId;
        //                llave = sistemaId.ToString("0") + "|" + moduloId.ToString("0") + "|" + opcionId.ToString("0");
        //            }
        //        }
        //        if (llave == "0|0|0")
        //            currentNode = e.Provider.RootNode.Clone(true);
        //        else
        //        {
        //            //if (e.Provider.FindSiteMapNodeFromKey(llave)!=null)
        //            currentNode = e.Provider.FindSiteMapNodeFromKey(llave);
        //            if (currentNode != null)
        //                currentNode = currentNode.Clone(true);
        //            // .Clone(true);
        //        }
        //    }
           
        //    return currentNode;
        //    // return e.Provider.CurrentNode;
        //}

    }
}
