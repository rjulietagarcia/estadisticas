using System;
using System.Data;

namespace SEroot
{
    public class ValidaRC
    {
        public static bool ValidarRC(ref SEroot.WSEscolar.PersonaDP persona)
        {
            bool encontrado = false;
            
                SEroot.WSEscolar.WsEscolar ws = new SEroot.WSEscolar.WsEscolar();
                try
                {
                    SEroot.WSEscolar.DsIndividuoSep ds = ws.BuscaIndividuoRCRemoto(persona.apellidoPaterno, persona.apellidoMaterno, persona.nombre, persona.fechaNacimiento);
                    if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["CURP"].ToString().Trim().Length > 2)
                    {
                        encontrado = true;
                        persona.apellidoPaterno = ds.Tables[0].Rows[0]["APELLIDO_PATERNO"].ToString();
                        persona.sexo = ds.Tables[0].Rows[0]["SEXO"].ToString().ToUpper() == "F" ? "M" : "H";
                        persona.fechaNacimiento = ds.Tables[0].Rows[0]["FECHA_NACI"].ToString();
                        persona.curp = ds.Tables[0].Rows[0]["CURP"].ToString();
                        persona.crip = ds.Tables[0].Rows[0]["CRIP"].ToString();
                        persona.apellidoPaterno = ds.Tables[0].Rows[0]["APELLIDO_PATERNO"].ToString();
                        persona.apellidoMaterno = ds.Tables[0].Rows[0]["APELLIDO_MATERNO"].ToString();
                        persona.nombre = ds.Tables[0].Rows[0]["NOMBRE"].ToString();
                        persona.bit_buscado = true;
                        persona.bit_encontrado = true;
                    }
                    else
                    {
                        persona.bit_buscado = true;
                        persona.bit_encontrado = false;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al buscar persona en registro civil");
                }
            
            return encontrado;
        }
    }
}
