using System;
using System.Collections.Generic;
using System.Text;
using SEroot.WsSESeguridad;

namespace SEroot.DataProvider
{
    public class AccionDataProvider
    {
        private WsSESeguridad.WsSESeguridad wsSESeguridad;

        public AccionDataProvider()
        {
            wsSESeguridad = new WsSESeguridad.WsSESeguridad();
            wsSESeguridad.Url = System.Configuration.ConfigurationManager.AppSettings["WebServiceSeguridad"];
        }
        
        public int Save(AccionDP accion)
        {
            
            int lResultado = 0;
            string lSalida = "Error";
            
            try
            {
                lSalida = wsSESeguridad.AccionSave(accion);
                lResultado = int.Parse("0" + lSalida);
            }
            catch
            {
                throw new Exception(lSalida);
            }
            return lResultado;
        }
        
        public AccionDP[] LoadList(int startRowIndex, int maximumRows)
        {
            return wsSESeguridad.AccionLoadList(startRowIndex, maximumRows);
        }

        public AccionDP[] LoadListFlt(int startRowIndex, int maximumRows, byte aSistema, byte aModulo, byte aOpcion)
        {
            List<AccionDP> lista = new List<AccionDP>();
            if ((aSistema != (byte)0) && (aModulo != (byte)0) && (aOpcion != (byte)0))
            {
                if (maximumRows == 0)
                    maximumRows = 1;
                LstAccionQryDP[] lst = wsSESeguridad.LstAccionQryLoadListForLlave(startRowIndex, maximumRows, aSistema, aModulo, aOpcion, 0);
                foreach (LstAccionQryDP acc in lst)
                {
                    AccionDP accion = new AccionDP();
                    accion.Abreviatura = acc.Abreviatura;
                    accion.AccionId = acc.AccionId;
                    accion.BitActivo = acc.BitActivo;
                    accion.FechaActualizacion = acc.FechaActualizacion;
                    accion.FechaFin = acc.FechaFin;
                    accion.FechaInicio = acc.FechaInicio;
                    accion.ModuloId = acc.ModuloId;
                    accion.Nombre = acc.Nombre;
                    accion.OpcionId = acc.OpcionId;
                    accion.SistemaId = acc.SistemaId;
                    accion.UsuarioId = acc.UsuarioId;
                    lista.Add(accion);
                }
            }
            return lista.ToArray();
        }

        public Int32 FltCount(int startRowIndex, int maximumRows, byte aSistema, byte aModulo, byte aOpcion)
        {
            if ((aSistema != (byte)0) && (aModulo != (byte)0) && (aOpcion != (byte)0))
                return Convert.ToInt32(wsSESeguridad.LstAccionQryCountForLlave(aSistema, aModulo, aOpcion, 0));
            else
                return 0;
        }

        public Int32 Count()
        {
            return Convert.ToInt32(wsSESeguridad.AccionCount());
        }
        
        public int Delete(AccionDP accion)
        {
            
            int lResultado = 0;
            string lSalida = "Error";
            
            try
            {
                lSalida = wsSESeguridad.AccionDelete(accion);
                lResultado = int.Parse("0" + lSalida);
            }
            catch
            {
                throw new Exception(lSalida);
            }
            return lResultado;
        }
        

        public AccionDP[] AccionLoad(Byte sistemaId, Byte moduloId, Byte opcionId, Byte accionId)

        {
            AccionPk pk = new AccionPk();

            pk.SistemaId = sistemaId;
            pk.ModuloId = moduloId;
            pk.OpcionId = opcionId;
            pk.AccionId = accionId;

            return new AccionDP[1] {wsSESeguridad.AccionLoad(pk)};
        }
    }
}
