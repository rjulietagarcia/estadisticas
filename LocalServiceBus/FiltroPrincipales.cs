using System;
using System.Collections.Generic;
using System.Text;
using SEroot.WsSESeguridad;
using System.Configuration;
using AjaxControlToolkit;
using System.Collections.Specialized;
using Mx.Gob.Nl.Educacion;
using System.Web;
using System.Web.UI;

namespace SEroot
{
    public class FiltroPrincipales
    {
        private UsuarioSeDP usr = null;
        private WsSESeguridad.WsSESeguridad ws;
        private string seccionConfiguracion = "WebServiceSeguridad";

        private void getUsr()
        {
            if (usr == null)
                usr = SeguridadSE.GetUsuario(HttpContext.Current);
        }

        private CascadingDropDownNameValue[] GetNivelEducacionCCT(StringDictionary knownCategoryValuesDictionary)
        {
            return GetNivelEducacionCCT(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetNivelEducacionCCT(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            string nivelEducacionSelected = usr.Selecciones.CentroTrabajoSeleccionado.NiveleducacionId.ToString();
            if (usr.NivelTrabajo.NiveltrabajoId == 4)
            {
                string descripcion = usr.Selecciones.CentroTrabajoSeleccionado.Niveleducacion;
                string valor = usr.Selecciones.CentroTrabajoSeleccionado.NiveleducacionId.ToString();
                CascadingDropDownNameValue[] result = new CascadingDropDownNameValue[1];
                result[0] = new CascadingDropDownNameValue(descripcion, valor, true);
                return result;
            }
            if (usr.NivelTrabajo.NiveltrabajoId < 4 && usr.NivelesEducacion.Length > 0)
            {
                int j = 0;
                CascadingDropDownNameValue[] usrwNivel = new CascadingDropDownNameValue[usr.NivelesEducacion.Length];
                foreach (NivelEducacionNvoDP linea in usr.NivelesEducacion)
                {
                    string descripcion = linea.Nombre;
                    string valor = linea.NivelId.ToString();
                    usrwNivel[j] = new CascadingDropDownNameValue(descripcion, valor, valor == nivelEducacionSelected);
                    j++;
                }
                return usrwNivel;
            }

            int cantidad = 0;
            cantidad = Convert.ToInt32(ws.NivelEducacionQryCount());
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            NivelEducacionQryDP[] res = ws.NivelEducacionQryLoadList(0, cantidad);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];


            foreach (NivelEducacionQryDP linea in res)
            {
                string descripcion = linea.Nombre.Replace("NO REQUIERE", "TODOS");
                string valor = linea.NivelId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, valor == nivelEducacionSelected);
                if (usr.NivelTrabajo.NiveltrabajoId == 1)
                    resultado[i].isDefaultValue = false;
                i++;
            }
            if (usr.NivelTrabajo.NiveltrabajoId == 1) resultado[0].isDefaultValue = true;
            return resultado;
        }

        private CascadingDropDownNameValue[] GetRegionPorNivelEducacion(StringDictionary knownCategoryValuesDictionary)
        {
            return GetRegionPorNivelEducacion(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetRegionPorNivelEducacion(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            if (usr.NivelTrabajo.NiveltrabajoId > 1 && usr.NivelTrabajo.NiveltrabajoId < 5)
            {
                string descripcion = usr.Selecciones.CentroTrabajoSeleccionado.Region;
                string valor = usr.Selecciones.CentroTrabajoSeleccionado.RegionId.ToString();
                CascadingDropDownNameValue[] result = new CascadingDropDownNameValue[1];
                result[0] = new CascadingDropDownNameValue(descripcion, valor, true);
                return result;
            }
            int cantidad = 0;

            cantidad = Convert.ToInt32(ws.RegionQryCount());

            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            RegionQryDP[] res = ws.RegionQryLoadList(0, cantidad);
            string regionSelected = usr.Selecciones.CentroTrabajoSeleccionado.RegionId.ToString();
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (RegionQryDP linea in res)
            {
                string descripcion = linea.Nombre.Replace("NO REQUIERE", "TODOS");
                string valor = linea.RegionId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, valor == regionSelected);
                if (usr.NivelTrabajo.NiveltrabajoId == 1)
                    resultado[i].isDefaultValue = false;
                i++;
            }
            if (usr.NivelTrabajo.NiveltrabajoId == 1) resultado[0].isDefaultValue = true;
            return resultado;
        }

        private CascadingDropDownNameValue[] GetSostenimiento(StringDictionary knownCategoryValuesDictionary)
        {
            return GetSostenimiento(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetSostenimiento(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            string sostenimientoSelected = usr.Selecciones.CentroTrabajoSeleccionado.SostenimientoId.ToString();
            if (usr.NivelTrabajo.NiveltrabajoId == 4)
            {
                string descripcion = usr.Selecciones.CentroTrabajoSeleccionado.Sostenimiento;
                string valor = usr.Selecciones.CentroTrabajoSeleccionado.SostenimientoId.ToString();
                CascadingDropDownNameValue[] result = new CascadingDropDownNameValue[1];
                result[0] = new CascadingDropDownNameValue(descripcion, valor, true);
                return result;
            }
            if (usr.NivelTrabajo.NiveltrabajoId < 4 && usr.Sostenimientos.Length > 0)
            {
                int j = 0;
                CascadingDropDownNameValue[] usrSostenimiento = new CascadingDropDownNameValue[usr.Sostenimientos.Length];
                foreach (SostenimientoQryDP linea in usr.Sostenimientos)
                {
                    string descripcion = linea.Nombre;
                    string valor = linea.SostenimientoId.ToString();
                    usrSostenimiento[j] = new CascadingDropDownNameValue(descripcion, valor, valor == sostenimientoSelected);
                    j++;
                }
                return usrSostenimiento;
            }
            int cantidad = 0;
            string regionString = knownCategoryValuesDictionary["REGIONPORNIVELEDUCACION"];

            short llaveRegion = 0;
            short.TryParse(regionString, out llaveRegion);
            SostenimientoQryDP[] res;

            if (llaveRegion == 0)
            {
                cantidad = Convert.ToInt32(ws.SostenimientofullQryCount());
                SostenimientofullQryDP[] res2 = ws.SostenimientofullQryLoadList(0, cantidad);
                res = new SostenimientoQryDP[res2.Length];
                int j = 0;
                foreach (SostenimientofullQryDP r2 in res2) {
                    res[j] = new SostenimientoQryDP();
                    res[j].SostenimientoId = r2.SostenimientoId;
                    res[j].Nombre = r2.Nombre;
                    j++;
                }
            }
            else
            {
                cantidad = Convert.ToInt32(ws.SostenimientoQryCountForDescripcion(filtro, llaveRegion));
                cantidad = cantidad == 0 ? 1 : cantidad;
                cantidad = cantidad > 250 ? 250 : cantidad;
                res = ws.SostenimientoQryLoadListForDescripcion(0, cantidad, filtro, llaveRegion);
            }


            int i = 1;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length + 1];

            resultado[0] = new CascadingDropDownNameValue("TODOS", "-1", sostenimientoSelected == "0");

            foreach (SostenimientoQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.SostenimientoId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, valor == sostenimientoSelected);
                if (usr.NivelTrabajo.NiveltrabajoId == 1)
                    resultado[i].isDefaultValue = false;
                i++;
            }
            if (usr.NivelTrabajo.NiveltrabajoId == 1) resultado[0].isDefaultValue = true;
            return resultado;
        }

        private CascadingDropDownNameValue[] GetZonaCCTEscolar(StringDictionary knownCategoryValuesDictionary)
        {
            return GetZonaCCTEscolar(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetZonaCCTEscolar(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            if (usr.NivelTrabajo.NiveltrabajoId == 4 || usr.NivelTrabajo.NiveltrabajoId == 3)
            {
                string descripcion = usr.Selecciones.CentroTrabajoSeleccionado.Numerozona.ToString();
                string valor = usr.Selecciones.CentroTrabajoSeleccionado.ZonaId.ToString();
                CascadingDropDownNameValue[] result = new CascadingDropDownNameValue[1];
                result[0] = new CascadingDropDownNameValue(descripcion, valor, true);
                return result;
            }
            int cantidad = 0;
            string nivelString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];
            string regionString = knownCategoryValuesDictionary["REGIONPORNIVELEDUCACION"];
            string sosteniminetoString = knownCategoryValuesDictionary["SOSTENIMIENTO"];

            byte llaveNivel = 0;
            byte llaveRegion = 0;
            byte llaveSostenimiento = 0;

            byte.TryParse(nivelString, out llaveNivel);
            byte.TryParse(regionString, out llaveRegion);
            byte.TryParse(sosteniminetoString, out llaveSostenimiento);
            filtro = "";
            cantidad = Convert.ToInt32(ws.CttEscolarQryCountForDescripcion(llaveNivel, llaveRegion, llaveSostenimiento, "E", filtro));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CttEscolarQryDP[] res = ws.CttEscolarQryLoadListForDescripcion(0, cantidad, llaveNivel, llaveRegion, llaveSostenimiento, "E", filtro);
            string zonaSelected = usr.Selecciones.CentroTrabajoSeleccionado.ZonaId.ToString();
            int i = 1;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length + 1];
            resultado[0] = new CascadingDropDownNameValue("TODOS", "-1", zonaSelected == "0");
            foreach (CttEscolarQryDP linea in res)
            {
                string descripcion = linea.Numerozona.ToString() + " - " + linea.Nombre;
                string valor = linea.ZonaId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, valor == zonaSelected);
                if (usr.NivelTrabajo.NiveltrabajoId == 1)
                    resultado[i].isDefaultValue = false;
                i++;
            }
            if (usr.NivelTrabajo.NiveltrabajoId == 1) resultado[0].isDefaultValue = true;
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCentroTrabajoPorZona(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCentroTrabajoPorZona(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCentroTrabajoPorZona(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string zonaString = knownCategoryValuesDictionary["ZONAPORREGIONNIVEL"];
            string niveleducacionString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];
            string regionString = knownCategoryValuesDictionary["Regionporniveleducacion"];
            string sostenimientoString = knownCategoryValuesDictionary["SOSTENIMIENTO"];
            string subnivelString = knownCategoryValuesDictionary["SUBNIVELPORNIVEL"];
            short llaveZona = 0;
            short.TryParse(zonaString, out llaveZona);
            byte llaveNiveleducacion = 0;
            byte.TryParse(niveleducacionString, out llaveNiveleducacion);
            short llaveRegion = 0;
            short.TryParse(regionString, out llaveRegion);
            byte llaveSostenimiento = 0;
            byte.TryParse(sostenimientoString, out llaveSostenimiento);
            short llaveSubnivel = 0;
            short.TryParse(subnivelString, out llaveSubnivel);
            CascadingDropDownNameValue[] resultado;
            if ((llaveZona != -1 || filtro != "%%"))// || (llaveClave != "") || (llaveDescripcion != ""))
            {
                string[] datos = filtro.Split('|');
                string llaveClave = "";
                string llaveDescripcion = "";

                llaveClave = filtro.Split('|')[0] + "%";
                llaveClave = llaveClave.Replace("%%%", "").Replace("%%", "");

                if (datos.Length > 1)
                {
                    llaveDescripcion = "%" + filtro.Split('|')[1];
                    llaveDescripcion = llaveDescripcion.Replace("%%", "");
                }

                if (llaveClave.Contains(">"))
                    llaveClave = "";
                if (llaveDescripcion.Contains(">"))
                    llaveDescripcion = "";



                cantidad = Convert.ToInt32(ws.CctntBusquedaQryCountForClaveDescripcion(llaveNiveleducacion, llaveRegion, llaveSostenimiento, llaveZona, llaveClave, llaveDescripcion, llaveSubnivel));
                cantidad = cantidad == 0 ? 1 : cantidad;
                cantidad = cantidad > 250 ? 250 : cantidad;
                string cctSelected = usr.Selecciones.CentroTrabajoSeleccionado.CentrotrabajoId.ToString();
                int i = 0;

                CctntBusquedaQryDP[] res = ws.CctntBusquedaQryLoadListForClaveDescripcion(0, cantidad, llaveNiveleducacion, llaveRegion, llaveSostenimiento, llaveZona, llaveClave, llaveDescripcion, llaveSubnivel);
                //resultado = new CascadingDropDownNameValue[res.Length + 1];
                resultado = new CascadingDropDownNameValue[res.Length];
                //resultado[0] = new CascadingDropDownNameValue("TODOS", "-1", cctSelected == "0");
                foreach (CctntBusquedaQryDP linea in res)
                {
                    string descripcion = linea.Clavecct + " - " + linea.Nombrecct + " - " + linea.Truno + " - " + linea.Nivel;
                    string valor = linea.CctntId.ToString();
                    resultado[i] = new CascadingDropDownNameValue(descripcion, valor, valor == cctSelected);
                    if (linea.CentrotrabajoId.ToString() == cctSelected && usr.NivelTrabajo.NiveltrabajoId == 1)
                        resultado[i].isDefaultValue = true;
                    else
                        resultado[i].isDefaultValue = false;
                    i++;
                }
            }
            else
            {
                resultado = new CascadingDropDownNameValue[1];
                resultado[0] = new CascadingDropDownNameValue("TODOS", "-1", true);
            }
            if (usr.NivelTrabajo.NiveltrabajoId == 1) resultado[0].isDefaultValue = true;
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCctNtBusqueda(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCctNtBusqueda(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCctNtBusqueda(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string zonaString = knownCategoryValuesDictionary["ZONAPORREGIONNIVEL"];
            string niveleducacionString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];
            string regionString = knownCategoryValuesDictionary["Regionporniveleducacion"];
            string sostenimientoString = knownCategoryValuesDictionary["SOSTENIMIENTO"];
            string subnivelString = knownCategoryValuesDictionary["SUBNIVELPORNIVEL"];
            short llaveZona = 0;
            short.TryParse(zonaString, out llaveZona);
            byte llaveNiveleducacion = 0;
            byte.TryParse(niveleducacionString, out llaveNiveleducacion);
            short llaveRegion = 0;
            short.TryParse(regionString, out llaveRegion);
            byte llaveSostenimiento = 0;
            byte.TryParse(sostenimientoString, out llaveSostenimiento);
            short llaveSubnivel = 0;
            short.TryParse(subnivelString, out llaveSubnivel);
            cantidad = Convert.ToInt32(ws.CctntBusquedaQryCountForClaveDescripcion(llaveNiveleducacion, llaveRegion, llaveSostenimiento, llaveZona,"","",llaveSubnivel));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CctntBusquedaQryDP[] res = ws.CctntBusquedaQryLoadListForClaveDescripcion(0, cantidad, llaveNiveleducacion, llaveRegion, llaveSostenimiento, llaveZona, "", "",llaveSubnivel);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CctntBusquedaQryDP linea in res)
            {
                string descripcion = linea.Clavecct + "-" + linea.Nombrecct + " - " + linea.Truno;
                string valor = linea.CentrotrabajoId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                i++;
            }
            if (usr.NivelTrabajo.NiveltrabajoId == 1) resultado[0].isDefaultValue = true;
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCicloEscolar(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCicloEscolar(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCicloEscolar(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            int cantidad = 0;
            string tipoCicloString = "1";

            short llaveTipoCiclo = 1;
            short.TryParse(tipoCicloString, out llaveTipoCiclo);

            cantidad = Convert.ToInt32(ws.CicloPorTipoEducacionQryCountForDescripcion(filtro, llaveTipoCiclo));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;

            CicloPorTipoEducacionQryDP[] res = ws.CicloPorTipoEducacionQryLoadListForDescripcion(0, cantidad, filtro, llaveTipoCiclo);
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CicloPorTipoEducacionQryDP linea in res)
            {
                string descripcion = linea.Nombre;
                string valor = linea.CicloescolarId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, i == 0);
                if (linea.BitCicloactual == true)
                    resultado[i].isDefaultValue = true;
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetCCTsdeUsuario(StringDictionary knownCategoryValuesDictionary)
        {
            return GetCCTsdeUsuario(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetCCTsdeUsuario(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            
            string username = HttpContext.Current.User.Identity.Name;
            string[] opciones = username.Split('|');
            int usuarioId = Convert.ToInt32(opciones[0]);
            int cantidad = 0;
            
            cantidad = Convert.ToInt32(ws.CctntUsrQryCountForUsuario(usuarioId));
            cantidad = cantidad == 0 ? 1 : cantidad;
            cantidad = cantidad > 250 ? 250 : cantidad;
            //CentrotrabajoDP[] res = usr.CentrosTrabajo;
            CctntUsrQryDP[] res = ws.CctntUsrQryLoadListForUsuario(0, cantidad, usuarioId);
            string cctntSelected = usr.Selecciones.CentroTrabajoSeleccionado.CctntId.ToString();
            int i = 0;
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[res.Length];
            foreach (CctntUsrQryDP linea in res)
            {
                //string descripcion = linea.Clave + " - " + linea.Nombre + " - " + linea.TurnoId.ToString().Replace("1","MATUTINO").Replace("2","VESPERTINO").Replace("3","NOCTURNO").Replace("2","DISCONTINUO").Replace("5","MIXTO");
                string descripcion = linea.Clavecct + " - " + linea.Nombrecct + " - " + linea.Truno + " - " + linea.Niveleducacion;
                string valor = linea.CctntId.ToString();
                resultado[i] = new CascadingDropDownNameValue(descripcion, valor, valor == cctntSelected);
                i++;
            }
            return resultado;
        }

        private CascadingDropDownNameValue[] GetSubniveles(StringDictionary knownCategoryValuesDictionary)
        {
            return GetSubniveles(knownCategoryValuesDictionary, "%");
        }
        private CascadingDropDownNameValue[] GetSubniveles(StringDictionary knownCategoryValuesDictionary, string filtro)
        {
            CascadingDropDownNameValue[] resultado = null;
            if (usr.NivelTrabajo.NiveltrabajoId == 4)
            {
                resultado = new CascadingDropDownNameValue[1];
                resultado[0] = new CascadingDropDownNameValue(usr.Selecciones.CentroTrabajoSeleccionado.Subnivel, usr.Selecciones.CentroTrabajoSeleccionado.SubnivelId.ToString(), true);
                return resultado;
            }
            string niveleducacionString = knownCategoryValuesDictionary["NIVELEDUCACIONCCT"];

            if (usr.NivelesEducacion.Length < 1)
            {
                
                short llaveNE = 0;
                short.TryParse(niveleducacionString,out llaveNE);
                int cantidad = 0;
                cantidad = Convert.ToInt32(ws.SubnivelQryCountForNiveleducacion(llaveNE));
                cantidad = cantidad == 0 ? 1 : cantidad;
                cantidad = cantidad > 250 ? 250 : cantidad;
                SubnivelQryDP[] res = ws.SubnivelQryLoadListForNiveleducacion(0, cantidad, llaveNE);
                int i = 0;
                resultado = new CascadingDropDownNameValue[res.Length];
                foreach (SubnivelQryDP linea in res)
                {
                    string descripcion = linea.Nombre;
                    string valor = linea.SubnivelId.ToString();
                    resultado[i] = new CascadingDropDownNameValue(descripcion, valor);
                    i++;
                }
                resultado[0].isDefaultValue = true;
                if (resultado[0].name == "ND") 
                    resultado[0].name = resultado[0].name.Replace("ND","TODOS");
                return resultado;
            }
            else
            {
               

                for (int i = 0; i < usr.NivelesEducacion.Length; i++)
                {
                    if (usr.NivelesEducacion[i].NivelId == short.Parse(niveleducacionString))
                    {
                        int j = 0;

                        resultado = new CascadingDropDownNameValue[usr.NivelesEducacion[i].SubNiveles.Length];
                        
                        foreach (SubnivelDP sub in usr.NivelesEducacion[i].SubNiveles)
                        {
                            resultado[j] = new CascadingDropDownNameValue(sub.Nombre, sub.SubnivelId.ToString());
                            j++;
                        }
                   
                    }
                   
                }
               
                return resultado;
                                
            }

        }
        /// <summary>
        /// Constructor del cat�logo
        /// <remarks>Recuerda que hay que cambiar la clase del Web Services a consultar</remarks>
        /// </summary>
        public FiltroPrincipales() {
            // Inicializo el Web Services Aqui
            ws = new WsSESeguridad.WsSESeguridad();
            ws.Url = ConfigurationManager.AppSettings[seccionConfiguracion];
        }

        public CascadingDropDownNameValue[] GetDropDownContentsFilter(string knownCategoryValues, string category, string filter) 
        {
            getUsr();
            if (category.IndexOf(';') != -1)
            {
                if (knownCategoryValues == "")
                {
                    knownCategoryValues = category + ":-1";
                    string[] categorias = category.Split(';');
                    category = categorias[categorias.Length - 1];
                }
            }
            // Get a dictionary of known category/value pairs
            StringDictionary knownCategoryValuesDictionary = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
            CascadingDropDownNameValue[] resultado = new CascadingDropDownNameValue[0];
            switch (category.ToUpper())
            {
                case "SOSTENIMIENTO":
                    resultado = GetSostenimiento(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "NIVELEDUCACIONCCT":
                    resultado = GetNivelEducacionCCT(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "REGIONPORNIVELEDUCACION":
                    resultado = GetRegionPorNivelEducacion(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "ZONAPORREGIONNIVEL":
                    resultado = GetZonaCCTEscolar(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "CCTPORZONA":
                    resultado = GetCentroTrabajoPorZona(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "CTNTBUSQUEDA":
                    resultado = GetCctNtBusqueda(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "CICLOESCOLAR":
                    resultado = GetCicloEscolar(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "CCTSDEUSUARIO":
                    resultado = GetCCTsdeUsuario(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;
                case "SUBNIVELPORNIVEL":
                    resultado = GetSubniveles(knownCategoryValuesDictionary, "%" + filter.ToUpper() + "%");
                    break;

            }

            // Perform a simple query against the data document
            return resultado;
        
        }
    }
}
