using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using SEroot.WsSESeguridad;

namespace Mx.Gob.Nl.Educacion
{
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    [XmlRoot("NivelEducacionNvoDP")]
    public class NivelEducacionNvoDP
    {
        #region Definicion de campos privados.
        private Int16 tipoeducacionId;
        private Int16 nivelId;
        private String nombre;
        private String letraFolio;
        private Boolean bitActivo;
        private Int32 usuarioId;
        private String fechaActualizacion;
        private SubnivelDP[] subNiveles;
        #endregion.

        #region Definicion de propiedades.
        /// <summary>
        /// TipoeducacionId
        /// </summary> 
        [XmlElement("TipoeducacionId")]
        public Int16 TipoeducacionId
        {
            get
            {
                return tipoeducacionId;
            }
            set
            {
                tipoeducacionId = value;
            }
        }

        /// <summary>
        /// NivelId
        /// </summary> 
        [XmlElement("NivelId")]
        public Int16 NivelId
        {
            get
            {
                return nivelId;
            }
            set
            {
                nivelId = value;
            }
        }

        /// <summary>
        /// Nombre
        /// </summary> 
        [XmlElement("Nombre")]
        public String Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }

        /// <summary>
        /// LetraFolio
        /// </summary> 
        [XmlElement("LetraFolio")]
        public String LetraFolio
        {
            get
            {
                return letraFolio;
            }
            set
            {
                letraFolio = value;
            }
        }

        /// <summary>
        /// BitActivo
        /// </summary> 
        [XmlElement("BitActivo")]
        public Boolean BitActivo
        {
            get
            {
                return bitActivo;
            }
            set
            {
                bitActivo = value;
            }
        }

        /// <summary>
        /// UsuarioId
        /// </summary> 
        [XmlElement("UsuarioId")]
        public Int32 UsuarioId
        {
            get
            {
                return usuarioId;
            }
            set
            {
                usuarioId = value;
            }
        }

        /// <summary>
        /// FechaActualizacion
        /// </summary> 
        [XmlElement("FechaActualizacion")]
        public String FechaActualizacion
        {
            get
            {
                return fechaActualizacion;
            }
            set
            {
                fechaActualizacion = value;
            }
        }

        /// <summary>
        /// SubNivel
        /// </summary> 
        [XmlElement("SubNivel")]
        public SubnivelDP[] SubNiveles
        {
            get
            {
                return subNiveles;
            }
            set
            {
                subNiveles = value;
            }
        }
        #endregion.

    }
}
