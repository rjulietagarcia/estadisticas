using System;
using System.Collections.Generic;
using System.Text;
using SEroot.WsSESeguridad;
using System.Xml.Serialization;


namespace SEroot
{
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    [XmlRoot("SeleccionSeDP")]
    public class SeleccionSeDP
    {

        private CcntFiltrosQryDP centroTrabajoSeleccionado;
        private CicloescolarDP cicloEscolarSeleccionado;

        [XmlElement("CentroTrabajoSeleccionado")]
        public CcntFiltrosQryDP CentroTrabajoSeleccionado
        {
            get { return centroTrabajoSeleccionado; }
            set { centroTrabajoSeleccionado = value; }
        }

        [XmlElement("CicloEscolarSeleccionado")]
        public CicloescolarDP CicloEscolarSeleccionado
        {
            get { return cicloEscolarSeleccionado; }
            set { cicloEscolarSeleccionado = value; }
        }
	
    }
}
