using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
//using Mx.Gob.Nl.Educacion.WsSESeguridad;
using SEroot.WsSESeguridad;
using SEroot;

namespace Mx.Gob.Nl.Educacion
{
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://tempuri.org/")]
    [XmlRoot("PaginaDP")]
    public class PaginaDP
    {
        #region variables de respaldo
        private AccionDP[] acciones;
        private NivelEducacionNvoDP[] nivelEducacion;
        private SostenimientoDP[] sostenimiento;
        private NiveltrabajoDP[] nivelTrabajo;
        private string rawUrl;
        private AccionDP opcionSeleccionada;
        #endregion

        #region Propiedades
        [XmlElement("Acciones")]
        public AccionDP[] Acciones
        {
            get { return acciones; }
            set { acciones = value; }
        }

        [XmlElement("NivelEducacion")]
        public NivelEducacionNvoDP[] NivelEducacion
        {
            get { return nivelEducacion; }
            set { nivelEducacion = value; }
        }

        [XmlElement("Sostenimiento")]
        public SostenimientoDP[] Sostenimiento
        {
            get { return sostenimiento; }
            set { sostenimiento = value; }
        }

        [XmlElement("NivelTrabajo")]
        public NiveltrabajoDP[] NivelTrabajo
        {
            get { return nivelTrabajo; }
            set { nivelTrabajo = value; }
        }

        [XmlElement("RawUrl")]
        public string RawUrl
        {
            get { return rawUrl; }
            set { rawUrl = value; }
        }

        [XmlElement("OpcionSeleccionada")]
        public AccionDP OpcionSeleccionada
        {
            get { return opcionSeleccionada; }
            set { opcionSeleccionada = value; }
        }
        #endregion
    }
}
