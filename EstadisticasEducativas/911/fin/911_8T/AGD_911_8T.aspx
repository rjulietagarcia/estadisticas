<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8T(Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_8T.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8T.AGD_911_8T" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    
    <script type="text/javascript">
        var _enter=true;
    </script>

    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 10;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1300px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>BACHILLERATO TECNOL�GICO</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1300px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8T',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_8T',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('AGD_911_8T',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li>
        <li onclick="openPage('AG1_911_8T',false)"><a href="#" title="" ><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL Y PLANTELES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td style="vertical-align: top">
                    <table style="width: 600px">
                        <tr>
                            <td style="padding-bottom: 15px">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="2. TOTAL DE ALUMNOS (suma de las carreras o especialidades)."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                    <table>
            <tr>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEMESTRE"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblSexo" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEXO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="INSCRIPCI�N TOTAL" Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="APROBADOS EN TODAS LAS ASIGNATURAS"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblReprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="REPROBADOS DE 1 A 5 ASIGNATURAS"
                        Width="95px"></asp:Label></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 Y 2"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV2" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV3" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV4" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV5" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV6" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV7" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV8" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr><tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 Y 4"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV9" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV10" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV11" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV12" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV13" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV14" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV15" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV16" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 Y 6"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV17" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV18" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV19" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV20" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV21" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV22" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV23" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV24" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 Y 8"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV25" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV26" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV27" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV28" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:Label ID="lblMuj4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV29" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV30" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV31" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV32" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV33" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV34" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV35" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV36" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table >
                        <tr>
                            <td style="width: 386px"  >
                    <asp:Label ID="lblInstruccion3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. De la existencia total, escriba el n�mero de alumnos de nacionalidad extranjera, deslos�ndolo por sexo."></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align:center; width: 386px;">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"></asp:Label></td>
                <td>
                    <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"></asp:Label></td>
                <td>
                    <asp:Label ID="lblTotal3a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEU" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ESTADOS UNIDOS"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV62" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20101" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV63" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20102" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV64" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20103" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCANADA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CANAD�"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV65" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20201" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV66" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20202" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV67" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20203" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCAYC" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CENTROAM�RICA Y EL CARIBE"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV68" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20301" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV69" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20302" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV70" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20303" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblSUDAM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUDAM�RICA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV71" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20401" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV72" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20402" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV73" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20403" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblAFRICA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="�FRICA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV74" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20501" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV75" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20502" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV76" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20503" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblASIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ASIA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV77" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20601" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV78" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20602" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV79" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20603" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEUROPA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EUROPA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV80" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20701" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV81" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20702" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV82" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20703" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblOCEANIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OCEAN�A"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV83" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20801" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV84" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20802" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV85" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20803" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblTotal3b" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV86" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="21061" ></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 386px" >
                    <asp:Label ID="lblInstruccion4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Escriba, por grado, el n�mero de grupos existentes."></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 386px;">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblGRUPOS" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o. (1o. y 2o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV87" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30101" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem2y3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o. (3o. y 4o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV88" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30201" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem4y5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o. (5o. y 6o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV89" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30301" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o. (7o. y 8o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV90" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30401" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV91" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30501" ></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('ACG_911_8T',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('ACG_911_8T',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AG1_911_8T',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG1_911_8T',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
       
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		        Disparador(<%=hidDisparador.Value %>);
        </script>
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
