
<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8T (Alumnos por Carrera)" AutoEventWireup="true" CodeBehind="ACG_911_8T.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8T.ACG_911_8T" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

  
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado JONATHAN ERRO EN EL JS--%>
   <script type="text/javascript" src="../../../tema/js/ArrowsHandler.js">
    </script> 
    <script type="text/javascript">
    MaxCol =  5;
    MaxRow = 10;
    TabIndexesArray = MultiDimensionalArray(3, MaxRow, MaxCol);
    </script>
    <%--Agregado--%>

     <div id="logo"></div>
    <div style="min-width:1300px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>BACHILLERATO TECNOL�GICO</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1300px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8T',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_8T',true)"><a href="#" title="" class="activo"><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('AGD_911_8T',false)"><a href="#" title="" ><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL Y PLANTELES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel><center>
 
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table style="width: 671px">
                        <tr>
                            <td colspan="7" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                    Text="I. ALUMNOS POR CARRERA O ESPECIALIDAD Y GRUPOS" Width="100%"></asp:Label></td>
                            <td colspan="1" style="padding-bottom: 10px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" style="text-align: left">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba por carrera o especialidad el n�mero de alumnos. Desglose la inscripci�n total, la existencia, los aprobados en todas las asignaturas y los reprobados de una a cinco asignaturas, seg�n el sexo."
                        Width="100%"></asp:Label></td>
                            <td colspan="1" style="text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" style="height: 19px; text-align: left">
                            </td>
                            <td colspan="1" style="height: 19px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                                <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Nombre de la carrera o especialidad"
                                    Width="210px"></asp:Label></td>
                            <td colspan="4" style="text-align: left">
                                <asp:TextBox ID="txtVA1" runat="server" Columns="30" MaxLength="30" TabIndex="10101" CssClass="lblNegro"></asp:TextBox>
                                &nbsp; &nbsp; &nbsp;
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ID"></asp:Label>
                            <asp:TextBox ID="txtVA0" runat="server" Columns="5" MaxLength="5" Width="24px" BackColor="Bisque" Font-Bold="True"  style="text-align: right" ReadOnly="True">0</asp:TextBox></td>
                            <td colspan="1" style="text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                                <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Font-Bold="True" Text="�REA DE ESTUDIO"
                                    Width="105px"></asp:Label></td>
                            <td colspan="4" style="text-align: left">
                                <asp:TextBox ID="txtVA2" runat="server" Columns="1" MaxLength="1" TabIndex="10201" CssClass="lblNegro"></asp:TextBox>
                                <asp:TextBox ID="txtVA3" runat="server" Columns="30" MaxLength="30" TabIndex="10202" CssClass="lblNegro" ></asp:TextBox>&nbsp;<asp:DropDownList
                                    ID="ddl_Areas" runat="server" onchange="setAreasEstudio('ctl00_cphMainMaster_ddl_Areas','ctl00_cphMainMaster_txtVA2','ctl00_cphMainMaster_txtVA3');" >
                                </asp:DropDownList></td>
                            <td colspan="1" style="text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px; text-align: left">
                                <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="CLAVE"
                                    Width="60px"></asp:Label></td>
                            <td colspan="" style="width: 110px; text-align: center">
                                <asp:TextBox ID="txtVA4" runat="server" Columns="8" MaxLength="9" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 110px; text-align: right">
                                &nbsp;<asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DURACI�N"
                                    Width="60px"></asp:Label></td>
                            <td colspan="2" style="width: 110px; text-align: left">
                                <asp:TextBox ID="txtVA5" runat="server" Columns="30" MaxLength="30" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 110px; text-align: right">
                                &nbsp;
                                </td>
                            <td style="width: 110px; text-align: left">
                                </td>
                            <td style="width: 110px; text-align: left">
                            </td>
                        </tr>
            <tr>
                <td style="text-align: center; width: 110px;" class="linaBajoAlto">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                        Width="60px"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoAlto">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEMESTRES"
                        Width="80px"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoAlto">
                    <asp:Label ID="lblSexo" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEXO"
                        Width="80px"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="INSCRIPCI�N TOTAL" Width="100px"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoAlto">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EXISTENCIA"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoAlto">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="APROBADOS EN TODAS LAS ASIGNATURAS"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoAlto">
                    <asp:Label ID="lblReprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="REPROBADOS DE 1 A 5 ASIGNATURAS"
                        Width="100px"></asp:Label></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 Y 2"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA6" runat="server" Columns="5" MaxLength="5" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA7" runat="server" Columns="5" MaxLength="5" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA8" runat="server" Columns="5" MaxLength="5" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA9" runat="server" Columns="5" MaxLength="5" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA10" runat="server" Columns="5" MaxLength="5" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA11" runat="server" Columns="5" MaxLength="5" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA12" runat="server" Columns="5" MaxLength="5" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA13" runat="server" Columns="5" MaxLength="5" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr><tr>
                <td rowspan="2" style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 Y 4"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA14" runat="server" Columns="5" MaxLength="5" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA15" runat="server" Columns="5" MaxLength="5" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA16" runat="server" Columns="5" MaxLength="5" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA17" runat="server" Columns="5" MaxLength="5" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA18" runat="server" Columns="5" MaxLength="5" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA19" runat="server" Columns="5" MaxLength="5" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA20" runat="server" Columns="5" MaxLength="5" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA21" runat="server" Columns="5" MaxLength="5" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 Y 6"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA22" runat="server" Columns="5" MaxLength="5" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA23" runat="server" Columns="5" MaxLength="5" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA24" runat="server" Columns="5" MaxLength="5" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA25" runat="server" Columns="5" MaxLength="5" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA26" runat="server" Columns="5" MaxLength="5" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA27" runat="server" Columns="5" MaxLength="5" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA28" runat="server" Columns="5" MaxLength="5" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA29" runat="server" Columns="5" MaxLength="5" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 Y 8"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:Label ID="lbl6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA30" runat="server" Columns="5" MaxLength="5" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA31" runat="server" Columns="5" MaxLength="5" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA32" runat="server" Columns="5" MaxLength="5" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajo">
                    <asp:TextBox ID="txtVA33" runat="server" Columns="5" MaxLength="5" TabIndex="20704" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:Label ID="lblMuj4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA34" runat="server" Columns="5" MaxLength="5" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA35" runat="server" Columns="5" MaxLength="5" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA36" runat="server" Columns="5" MaxLength="5" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA37" runat="server" Columns="5" MaxLength="5" TabIndex="20804" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center;" class="linaBajo">
                    <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA38" runat="server" Columns="5" MaxLength="5" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA39" runat="server" Columns="5" MaxLength="5" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA40" runat="server" Columns="5" MaxLength="5" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center; width: 110px;" class="linaBajoS">
                    <asp:TextBox ID="txtVA41" runat="server" Columns="5" MaxLength="5" TabIndex="20904" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="width: 110px; text-align: center">
                    &nbsp;</td>
            </tr>
        </table>
       
            <table>
            <tr>
                    <td>
                        <asp:LinkButton ID="lnkEliminarCarrera" runat="server" OnClick="lnkEliminarCarrera_Click">Eliminar Carrera Actual</asp:LinkButton>
                    </td>
                </tr>
                <tr><td>
                    Carreras:
                </td>
                    <td style="font-weight: bold;">
                       <asp:DropDownList ID="ddlID_Carrera" runat="server"  onchange='OnChange(this);'>
                       </asp:DropDownList > de 
                       <asp:TextBox ID="lblUnodeN" runat="server" Columns="5" MaxLength="5" Width="24px" BackColor="Bisque" Font-Bold="True" ReadOnly="True">0</asp:TextBox>
                    </td>
                    <td id="td_Nuevo" runat="server"><span onclick = "navegarCarreras_new(-1);" >&nbsp;<a href="#">Agregar Otra <strong>*</strong></a></span></td>
                </tr>
            </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_8T',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_8T',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AGD_911_8T',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_8T',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hidIdCtrl" runat= "server" value="" />
        <input id="hidListaTxtBoxs" type="Hidden" runat = "server" value="" />
        <input id="hidDisparador" type="Hidden" runat = "server" value="90" /><br />
         </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
       
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                var ID_Carrera =0; 
                
                function OnChange(dropdown)
                {
                    var myindex  = dropdown.selectedIndex
                    var SelValue = dropdown.options[myindex].value
                    navegarCarreras_new(SelValue);
                    return true;
                }
                
                function navegarCarreras_new(id_carrera){
                     ID_Carrera = (id_carrera);
                     var ID_Carreratmp = ID_Carrera; 
                                        
                    if (id_carrera == -1)
                    {
                       var totalRegs = document.getElementById('<%=lblUnodeN.ClientID%>').value;
                       ID_Carrera = eval(totalRegs) + 1; 
                       alert(ID_Carrera);
                    }
                
                    openPage('ACG_911_8T');
                }
                function ReceiveServerData_Variables(rValue){
                    _gRValue = rValue;
                    window.setTimeout('__ReceiveServerData_Variables(_gRValue)',0);
                }
                function LimpiarFallas(){
                   var lista = document.getElementById(hidListaTxtBoxs).value;
                   var cajas = lista.split('|');
                   var i = 0;
                   for (i = 0; i < cajas.length-1; i ++)
                   {
                        var txt = cajas[i].split(',');
                        var obj = document.getElementById(txt[0]);
                        obj.className='txtVarCorrecto';
                   }
                }
                function __ReceiveServerData_Variables(rValue){
                   if (rValue != null){
                        document.getElementById("divWait").style.visibility = "hidden";
                        var rows = rValue.split('|');
                        var vi = 1;
                        if (rValue != "")  LimpiarFallas();
                        var detalles="<ul>";
                        for(vi = 1; vi < rows.length; vi ++){
                            var res = rows[vi];
                            var colums = res.split('!');
                            var caja = document.getElementById("ctl00_cphMainMaster_txtV" + colums[0]);
                          
                            if (caja!=null){
                                 caja.className = "txtVarFallas";
                            }
                            detalles = detalles + "<li>" + colums[1]+ "</li>";
                       }
                       if (CambiarPagina != "") {  // para que no se borre la informacion a menos que se mueva de la pagina
                            var obj = document.getElementById('divResultado');
                            obj.innerHTML =  detalles + "</ul>";
                       }
                       var Cambiar = "No";
                       if ( rValue != "" && CambiarPagina != "" ){
                          if(ir1)
                            {
                                
                                Cambiar = "Si";
                            }
                            else
                            {
                                alert("Se encontraron errores de captura\nDebe corregirlos para avanzar a la siguiente p"+'\u00e1'+"gina.");
                                Cambiar = "No";
                            }
                       }
                       else if(rValue == "" && CambiarPagina != ""){
                          Cambiar = "Si";
                       }
                       if (Cambiar == "Si"){
                             window.location.href=CambiarPagina+".aspx?idC=" + ID_Carrera;
                       } 
                   }
                }
                function EnviarDatosServer(EjecutarValidacion){
                     if (EjecutarValidacion == "Ejecutar");
                          document.getElementById("divWait").style.visibility = "visible";
                     var lista = document.getElementById(hidListaTxtBoxs).value;
                     var rows = lista.split('|');
                     var Salida = "NoVariable=Valor|"; 
                     var i;
                     var valor = 0;
                     var NoVariable;
                     for( i = 0; i < rows.length-1; i++ )
                     {
                        var Datos = rows[i].split(',');
                      
                        var caja =  document.getElementById(Datos[0])
                        if (caja != null){
                                valor = caja.value;
                                if (i > 0 && i <= 1 ){valor = "'" + valor + "'"; }
                               
                                if (trim(valor) == "" && i > 5){valor = 0;}
                                NoVariable = Datos[1];
                                
                                NoVariable = Datos[1];
                                var ReadOnly; 
                                if (caja.readOnly)
                                    ReadOnly = "false";
                                else
                                    ReadOnly = "true";
                                
                                Salida = Salida + NoVariable + "=" + valor + "=" + ReadOnly + "|";
                        }
                    }
                    //EjecutarValidacion = "NoEjecutar/Ejecutar"
                    Salida = Salida + "!" + EjecutarValidacion + "!" + document.getElementById("<%=hidIdCtrl.ClientID %>").value;
                    CallServer_Variables(Salida,"");
                }
                                
                var FirstTime = true;
                function Disparador(segundos){
                    CambiarPagina = "";
                    if (!FirstTime){
                        EnviarDatosServer("NoEjecutar");
                    }
                    else{
                        document.getElementById("divWait").style.visibility = "hidden";
                    }
                    //alert(FirstTime);
                    FirstTime = false;
                    var segs = eval(segundos * 1000);
                    window.setTimeout("Disparador("+segundos+")",segs);
                }
                function OcultarNuevoRegistro(){
                   if (document.getElementById("ctl00_cphMainMaster_txtVA6").disabled== true){
                      document.getElementById("NuevoReg").style.display = "none";
                   }
                }
                function trim(stringToTrim) {
                    return stringToTrim.replace(/^\s+|\s+$/g,"");
                }
                function ltrim(stringToTrim) {
                    return stringToTrim.replace(/^\s+/,"");
                }
                function rtrim(stringToTrim) {
                    return stringToTrim.replace(/\s+$/,"");S
                }
                              
               function ErrorServidor(oMensaje, oContexto) {alert(oMensaje);}

//                function enter(e)
//               {
//                   if (event.keyCode == 37)
//                   {
//                        var obj = document.getElementById('ctl00_cphMainMaster_txtVA'+eval(tabX-1));                      
//                        if (obj!= null)
//                             obj.focus();
//                   }
//                   if (_enter)
//                   {
//                       if (event.keyCode==13 || event.keyCode == 39)
//                       {
//                           event.keyCode=9; 
//                           return event.keyCode;
//                       }  
//                       else
//                          return  true;
//                   }
//               }
//               var tabX = 0;
//               function VerTab(obj){
//                      var id  = new String();
//                      id = obj.id;
//                      var dato = id.substr(25,id.length-5);
//                      tabX = eval(dato);
//               }
                function Num(evt){    
                    
                   return  keyRestrict(evt,'1234567890');
                }
                
                
               function OnChange(dropdown)
                {
                    var myindex  = dropdown.selectedIndex
                    var SelValue = dropdown.options[myindex].value
                    navegarCarreras_new(SelValue);
                    return true;
                }
                function navegarCarreras_new(id_carrera){
                     ID_Carrera = (id_carrera);
                     var ID_Carreratmp = ID_Carrera; 
                                        
                    if (id_carrera == -1)
                    {
                       var totalRegs = document.getElementById('<%=lblUnodeN.ClientID%>').value;
                       ID_Carrera = eval(totalRegs) + 1; 
                       alert(ID_Carrera);
                    }
                
                    openPage('ACG_911_8T');
                }
                 function OcultarNuevoRegistro(){
                   if (document.getElementById("ctl00_cphMainMaster_txtVA6").disabled== true){
                      document.getElementById("NuevoReg").style.display = "none";
                   }
                }
                var tabX = 0;
               function VerTab(obj){
                      var id  = new String();
                      id = obj.id;
                      var dato = id.substr(25,id.length-5);
                      tabX = eval(dato);
               }
               
               
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado-- JONATHAN MODIFICAR DESPUES--%>
    <script type="text/javascript">
    GetTabIndexes();
    </script>
    <%--Agregado--%>
     
</asp:Content>
