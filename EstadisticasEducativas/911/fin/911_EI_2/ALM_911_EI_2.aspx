<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-2(ALUMNOS)" AutoEventWireup="true" CodeBehind="ALM_911_EI_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_EI_2.ALM_911_EI_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
        <table style="width: 100%;">
                <tr>
                    <td>
                        <span>EDUCACI�N INICIAL</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                           2012-2013</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
                </tr>
            </table>
    </div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_EI_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ALM_911_EI_2',true)"><a href="#" title="" class="activo"><span>ALUMNOS INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_EI_2',false)"><a href="#" title="" ><span> ALUMNOS PREESCOLAR</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div>  
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 540px">
            <tr>
                <td style="width: 340px">
                    <table  style="text-align: center; width: 540px;" >
                        <tr>
                            <td colspan="6" rowspan="1" style="text-align: center; padding-bottom: 10px; height: 26px;">
                                <asp:Label ID="lblInformacion" runat="server" CssClass="lblRojo" Text="I. INFORMACI�N GENERAL" Font-Size="16px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="height: 26px; text-align: left; width: 90px;">
                                <asp:Label ID="lblInstrucciones11" runat="server" CssClass="lblRojo" Text="1. Marque las secciones que atiende el CENDI."
                                    Width="540px"></asp:Label>&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: right">
                                <asp:Label ID="lblLactantes" runat="server" CssClass="lblGrisTit" Text="LACTANTES" Width="90px"></asp:Label>
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: left">
                                <asp:CheckBox ID="optV1" runat="server" Text=" " TabIndex="10101" onclick="OPTs2Txt();" />
                                <asp:TextBox ID="txtV1" runat="server" Width="12px" style="visibility:hidden"></asp:TextBox></td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: right">
                                <asp:Label ID="lblMaternal" runat="server" CssClass="lblGrisTit" Text="MATERNAL" Width="90px"></asp:Label>
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: left">
                                <asp:CheckBox ID="optV2" runat="server" Text=" " TabIndex="10102" onclick="OPTs2Txt();"/>
                                <asp:TextBox ID="txtV2" runat="server" Width="12px" style="visibility:hidden"></asp:TextBox></td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: right">
                                <asp:Label ID="lblPreescolar" runat="server" CssClass="lblGrisTit" Text="PREESCOLAR" Width="90px"></asp:Label>
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: left">
                                <asp:CheckBox ID="optV3" runat="server" Text="  " TabIndex="10103"  onclick="OPTs2Txt();" />
                                <asp:TextBox ID="txtV3" runat="server" Width="12px" style="visibility:hidden"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="padding-bottom: 10px; height: 26px; text-align: left">
                                <asp:Label ID="lblInstrucciones12" runat="server" CssClass="lblRojo" Text="2. De  la existencia total, anote el n�mero de los alumnos con discapacidad, desglos�ndolo por sexo."
                                    Width="600px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: left; height: 26px; width: 90px;">
                                <asp:Label ID="lblHombres12" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="90px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV4" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: left; width: 90px; height: 26px;">
                                <asp:Label ID="lblMujeres12" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="90px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                <asp:TextBox ID="txtV5" runat="server" Columns="3" TabIndex="10202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: left; width: 90px; height: 26px;">
                                <asp:Label ID="lblTotal12" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV6" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="height: 26px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="height: 26px; text-align: left">
                                <asp:Label ID="lblInstrucciones23" runat="server" CssClass="lblRojo" Text="3. De la existencia total, escriba el n�mero de los alumnos con capacidades y aptitudes sobresalientes, desglos�ndolo por sexo."
                                    Width="540px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="" rowspan="1" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV7" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV8" runat="server" Columns="3" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblTotal3" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV9" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="height: 26px; text-align: left">
                                <asp:Label ID="lblInstrucciones24" runat="server" CssClass="lblRojo" Text="4. De la existencia total, escriba el n�mero de los alumnos que fueron atendidos por la Unidad de Servicios de Apoyo a la Educaci�n Regular (USAER), desglos�ndolo por sexo."
                                    Width="540px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV10" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV11" runat="server" Columns="3" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblTotal4" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV12" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="padding-bottom: 10px;
                                text-align: center">
                                <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Font-Size="16px" Text="II. POBLACI�N INFANTIL ATENDIDA EN LAS SECCIONES DE LACTANTES Y MATERNALES"
                                    Width="540px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="lblInstrucciones21" runat="server" CssClass="lblRojo" Text="1. Desglose el n�mero total de ni�os existentes en las secciones de lactantes y maternales desglos�ndolo por estrato de edad y sexo."
                                    Width="540px"></asp:Label>
                                <asp:Label ID="lblInstrucciones22" runat="server" CssClass="lblRojo" Text="2. Grupos: Escriba por seccion la cantidad de grupos existentes, seg�n el estrato de edad."
                                    Width="540px"></asp:Label>
                                <asp:Label ID="lblNota21" runat="server" CssClass="lblRojo" Text="IMPORTANTE: Consulte las tablas de estratos de edad en la secci�n de glosario."
                                    Width="540px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6" rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="height: 26px; text-align: center; width: 90px;">
                            </td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                &nbsp;</td>
                            <td style="text-align: center; width: 270px; height: 26px;" colspan="3">
                                <asp:Label ID="lblEstratos" runat="server" CssClass="lblRojo" Text="ESTRATO DE EDAD"
                                    Width="270px"></asp:Label></td>
                            <td style="text-align: center; width: 90px; height: 26px;">
                                </td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                </td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:Label ID="lblEst1" runat="server" CssClass="lblGrisTit" Text="1" Width="90px"></asp:Label></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                                <asp:Label ID="lblEst2" runat="server" CssClass="lblGrisTit" Text="2" Width="90px"></asp:Label></td>
                            <td style="height: 26px; text-align: center; width: 90px;">
                            <asp:Label ID="lblEst3" runat="server" CssClass="lblGrisTit" Text="3" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Text="TOTAL" Width="90px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: center; width: 90px;" colspan="" rowspan="4">
                                <asp:Label ID="lblLactante2" runat="server" CssClass="lblRojo" Text="LACTANTE" Width="90px"></asp:Label></td>
                            <td style="height: 26px; text-align: left; width: 90px;">
                            <asp:Label ID="lblLactNi�os" runat="server" CssClass="lblGrisTit" Text="NI�OS" Width="90px"></asp:Label></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV13" runat="server" Columns="3" TabIndex="20101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV14" runat="server" Columns="3" TabIndex="20102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV15" runat="server" Columns="3" TabIndex="20103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV16" runat="server" Columns="3" TabIndex="20104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: left; width: 90px;">
                                <asp:Label ID="lblLactNi�as" runat="server" CssClass="lblGrisTit" Text="NI�AS" Width="90px"></asp:Label></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV17" runat="server" Columns="3" TabIndex="20201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV18" runat="server" Columns="3" TabIndex="20202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV19" runat="server" Columns="3" TabIndex="20203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV20" runat="server" Columns="3" TabIndex="20204" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: left; width: 90px;">
                                <asp:Label ID="lblLactSubTot" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL" Width="90px"></asp:Label></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV21" runat="server" Columns="3" TabIndex="20301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV22" runat="server" Columns="3" TabIndex="20302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV23" runat="server" Columns="3" TabIndex="20303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV24" runat="server" Columns="3" TabIndex="20304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblLactGpos" runat="server" CssClass="lblGrisTit" Text="GRUPOS" Width="90px"></asp:Label></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV25" runat="server" Columns="3" TabIndex="20401" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV26" runat="server" Columns="3" TabIndex="20402" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV27" runat="server" Columns="3" TabIndex="20403" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV28" runat="server" Columns="3" TabIndex="20404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="4" style="width: 90px; height: 26px; text-align: center">
                                <asp:Label ID="lblMaternal2" runat="server" CssClass="lblRojo" Text="MATERNAL" Width="90px"></asp:Label></td>
                            <td style="width: 90px; height: 26px; text-align: left">
                                <asp:Label ID="lblMatNi�os" runat="server" CssClass="lblGrisTit" Text="NI�OS" Width="90px"></asp:Label></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV29" runat="server" Columns="3" TabIndex="20501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV30" runat="server" Columns="3" TabIndex="20502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV31" runat="server" Columns="3" TabIndex="20503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV32" runat="server" Columns="3" TabIndex="20504" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: left; width: 90px;">
                                <asp:Label ID="lblMatNi�as" runat="server" CssClass="lblGrisTit" Text="NI�AS" Width="90px"></asp:Label></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV33" runat="server" Columns="3" TabIndex="20601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV34" runat="server" Columns="3" TabIndex="20602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV35" runat="server" Columns="3" TabIndex="20603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV36" runat="server" Columns="3" TabIndex="20604" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: left; width: 90px;">
                                <asp:Label ID="lblMatSubTot" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL" Width="90px"></asp:Label></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV37" runat="server" Columns="3" TabIndex="20701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV38" runat="server" Columns="3" TabIndex="20702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV39" runat="server" Columns="3" TabIndex="20703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV40" runat="server" Columns="3" TabIndex="20704" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: left; width: 90px;">
                                <asp:Label ID="lblMatGpos" runat="server" CssClass="lblGrisTit" Text="GRUPOS" Width="90px"></asp:Label></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV41" runat="server" Columns="3" TabIndex="20801" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV42" runat="server" Columns="3" TabIndex="20802" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="text-align: center; width: 90px;">
                                <asp:TextBox ID="txtV43" runat="server" Columns="3" TabIndex="20803" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 90px; text-align: center">
                                <asp:TextBox ID="txtV44" runat="server" Columns="3" TabIndex="20804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 90px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_EI_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_EI_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('APRE_911_EI_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('APRE_911_EI_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
      <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />       
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
       
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         </center> 
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                function PintaOPTs(){
                     marcar('V1');
                     marcar('V2');
                     marcar('V3');
                }  
                
                function marcar(variable){
                     var txtv = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                     if (txtv != null) {
                         var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                         if (txtv.value == 'X'){
                             chk.checked = true;
                         } else {
                             chk.checked = false;
                         }
                      
                     }
                }
                
                function OPTs2Txt(){
                     marcarTXT('V1');
                     marcarTXT('V2');
                     marcarTXT('V3');
                }    
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         
                         var txt = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                             
                         
                     }
                } 
                Disparador(<%=this.hidDisparador.Value %>);
                PintaOPTs();
                
                var _enter=true;
                MaxCol = 5;
                MaxRow = 9;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();            
        </script>
</asp:Content>