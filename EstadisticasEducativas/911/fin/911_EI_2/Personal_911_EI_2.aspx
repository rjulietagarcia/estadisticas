<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-2(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_EI_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_EI_2.Personal_911_EI_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>    
   <%-- <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />--%>
<div id="logo"></div>
<div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="EDUCACI�N INICIAL"   Font-Bold="True" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblCiclo" runat="server" Text="CICLO ESCOLAR 2012-2013"  Font-Bold="True" ></asp:Label>
            </td>
        </tr>
        <tr style="font-weight: bold">
            <td style="text-align: center">
                <span   font-weight: bold;">CENTRO DE TRABAJO: <%=hdnCct.Value %></span>
            </td>
        </tr>
    </table>
    
    
    </div>
    </div>
    
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="OpenPageCharged('Identificacion_911_EI_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="OpenPageCharged('ALM_911_EI_2',true)"><a href="#" title=""><span>ALUMNOS INICIAL</span></a></li>
        <li onclick="OpenPageCharged('APRE_911_EI_2',true)"><a href="#" title=""><span> ALUMNOS PREESCOLAR</span></a></li>
        <li onclick="OpenPageCharged('Personal_911_EI_2',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="OpenPageCharged('Inmueble_911_EI_2',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
  
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

 
           <table >
                        <tr>
                            <td colspan="5" style="padding-bottom: 10px; width: 90px;  text-align: left">
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblRojo" Font-Size="16px"
                                    Text="IV. PERSONAL POR FUNCI�N" Width="249px"></asp:Label></td>
                            <td>
                            </td>
                            <td colspan="2">
                            </td>
                            <td >
                            </td>
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba el n�mero del personal de servicio pedag�gico."
                                    Width="360px"></asp:Label></td>
                            <td>
                            </td>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="lblInstrucciones5" runat="server" CssClass="lblRojo" Text="5. Escriba el n�mero del personal directivo y administrativo."
                                    Width="200px"></asp:Label></td>
                            <td >
                            </td>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="lblInstrucciones8" runat="server" CssClass="lblRojo" Text="8. Escriba el n�mero del personal de servicios generales."
                                    Width="241px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <asp:Label ID="lblJefePedagogia" runat="server" CssClass="lblGrisTit" Text="JEFE DEL �REA PEDAG�GICA" Width="216px"></asp:Label></td>
                            <td  >
                                <asp:TextBox ID="txtV229" runat="server" Columns="2" TabIndex="10101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblDirectores5" runat="server" CssClass="lblGrisTit" Text="DIRECTORES" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV270" runat="server" Columns="2" TabIndex="11801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblAuxIntendencia8" runat="server" CssClass="lblGrisTit" Text="AUXILIARES DE INTENDENCIA"
                                    Width="202px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV290" runat="server" Columns="2" TabIndex="13801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <asp:Label ID="lblProfMusica" runat="server" CssClass="lblGrisTit" Text="PROFESORES DE ENSE�ANZA MUSICAL" Width="216px"></asp:Label></td>
                            <td  >
                                <asp:TextBox ID="txtV230" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblSubdirectores5" runat="server" CssClass="lblGrisTit" Text="SUBDIRECTORES"
                                    Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV271" runat="server" Columns="2" TabIndex="11901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblLavanderas8" runat="server" CssClass="lblGrisTit" Text="LAVANDERAS" Width="202px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV291" runat="server" Columns="2" TabIndex="13901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <asp:Label ID="lblProfEdFisica" runat="server" CssClass="lblGrisTit" Text="PROFESORES DE EDUCACI�N F�SICA" Width="216px"></asp:Label></td>
                            <td  >
                                <asp:TextBox ID="txtV231" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblSecretarias5" runat="server" CssClass="lblGrisTit" Text="SECRETARIAS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV272" runat="server" Columns="2" TabIndex="12001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblVigilantes8" runat="server" CssClass="lblGrisTit" Text="VIGILANTES" Width="202px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV292" runat="server" Columns="2" TabIndex="14001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" >
                                <asp:Label ID="lblSubtotal1" runat="server" CssClass="lblRojo" Text="SUBTOTAL (1)" Height="17px" Width="216px"></asp:Label></td>
                            <td  >
                                <asp:TextBox ID="txtV232" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblAdminAux5" runat="server" CssClass="lblGrisTit" Text="ADMINISTRATIVO Y AUXILIAR"
                                    Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV273" runat="server" Columns="2" TabIndex="12101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblConserje8" runat="server" CssClass="lblGrisTit" Text="ASISTENTES DE SERVICIOS DE MANTENIMIENTO (CONSERJES)"
                                    Width="202px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV293" runat="server" Columns="2" TabIndex="14101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="5"  style="width: 90px; height: 26px">
                            </td>
                            <td  >
                            </td>
                            <td>
                                <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Text="TOTAL (2)" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV274" runat="server" Columns="2" TabIndex="12201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblAuxMantenimiento8" runat="server" CssClass="lblGrisTit" Text="AUXILIARES DE MANTENIMIENTO"
                                    Width="202px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV294" runat="server" Columns="2" TabIndex="14201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="5"  style="padding-bottom: 10px; width: 90px; 
                                text-align: left">
                                <asp:Label ID="lblInstrucciones2" runat="server" CssClass="lblRojo" Text="2. Desglose a las puericulturas por secci�n y estrato de edad."
                                    Width="344px"></asp:Label></td>
                            <td  >
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblTotal5" runat="server" CssClass="lblRojo" Text="TOTAL (5)" Width="202px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV295" runat="server" Columns="2" TabIndex="14301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="">
                                </td>
                            <td colspan="" rowspan="">
                                <asp:Label ID="lblLactantes2" runat="server" CssClass="lblRojo" Text="LACTANTES" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblMaternales2" runat="server" CssClass="lblRojo" Text="MATERNALES" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblTotal42" runat="server" CssClass="lblRojo" Text="TOTAL" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td>
                                </td>
                            <td>
                                </td>
                            <td style="width: 340px;  text-align: left;" colspan="2">
                                <asp:Label ID="lblInstrucciones6" runat="server" CssClass="lblRojo" Text="6. Escriba el n�mero del personal de servicios especiales."
                                    Width="200px"></asp:Label></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="lblEstr1" runat="server" CssClass="lblGrisTit" Text="1" Width="90px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV233" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV234" runat="server" Columns="2" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV235" runat="server" Columns="2" TabIndex="10503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td>
                                </td>
                            <td>
                                <asp:Label ID="lblMedicos6" runat="server" CssClass="lblGrisTit" Text="M�DICOS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV275" runat="server" Columns="2" TabIndex="12301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td colspan="2" rowspan="2" style="text-align: left">
                                <asp:Label ID="lblInstrucciones9" runat="server" CssClass="lblRojo" Text="9. Personal con otro tipo de funciones."
                                    Width="241px"></asp:Label>
                                <asp:Label ID="lblNota9" runat="server" CssClass="lblRojo" Text="Desagregue al personal que no se considero en los rubros anteriores seg�n la actividad que desempe�a."
                                    Width="241px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="lblEstr2" runat="server" CssClass="lblGrisTit" Text="2" Width="90px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV236" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV237" runat="server" Columns="2" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV238" runat="server" Columns="2" TabIndex="10603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td>
                                </td>
                            <td>
                                <asp:Label ID="lblOdontologos6" runat="server" CssClass="lblGrisTit" Text="ODONT�LOGOS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV276" runat="server" Columns="2" TabIndex="12401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblEstr3" runat="server" CssClass="lblGrisTit" Text="3" Width="90px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV239" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV240" runat="server" Columns="2" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV241" runat="server" Columns="2" TabIndex="10703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td>
                                </td>
                            <td>
                                <asp:Label ID="lblPsicologos6" runat="server" CssClass="lblGrisTit" Text="PSIC�LOGOS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV277" runat="server" Columns="2" TabIndex="12501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                            <asp:TextBox ID="txtV296" runat="server" Columns="30" TabIndex="14401" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td>
                            <asp:TextBox ID="txtV297" runat="server" Columns="2" TabIndex="14501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblSubtotal2" runat="server" CssClass="lblRojo" Text="SUBTOTAL (2)" Width="90px"></asp:Label></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox ID="txtV242" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblEnfermeras6" runat="server" CssClass="lblGrisTit" Text="ENFERMERAS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV278" runat="server" Columns="2" TabIndex="12601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                            <asp:TextBox ID="txtV298" runat="server" Columns="30" TabIndex="14601" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td>
                            <asp:TextBox ID="txtV299" runat="server" Columns="2" TabIndex="14701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="5" >
                            </td>
                            <td  >
                            </td>
                            <td>
                                <asp:Label ID="lblTrabSoc6" runat="server" CssClass="lblGrisTit" Text="TRABAJADORAS SOCIALES"
                                    Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV279" runat="server" Columns="2" TabIndex="12701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                            <asp:TextBox ID="txtV300" runat="server" Columns="30" TabIndex="14801" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td>
                            <asp:TextBox ID="txtV301" runat="server" Columns="2" TabIndex="14901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="5"  style="padding-bottom: 10px; width: 90px; 
                                text-align: left">
                                <asp:Label ID="lblInstrucciones3" runat="server" CssClass="lblRojo" Text="3. Desglose a las educadoras de lactantes  y maternales  por estrato de edad y a las de preescolar por grado."
                                    Width="344px"></asp:Label></td>
                            <td  >
                            </td>
                            <td>
                                <asp:Label ID="lblTotal3" runat="server" CssClass="lblRojo" Text="TOTAL (3)" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV280" runat="server" Columns="2" TabIndex="12801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                            <asp:TextBox ID="txtV302" runat="server" Columns="30" TabIndex="15001" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td>
                            <asp:TextBox ID="txtV303" runat="server" Columns="2" TabIndex="15101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="" style="height: 33px;">
                                </td>
                            <td colspan="" rowspan="" style="height: 33px;">
                                <asp:Label ID="lblLactantes3" runat="server" CssClass="lblRojo" Text="LACTANTES" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td style="height: 33px;">
                                <asp:Label ID="lblMaternales3" runat="server" CssClass="lblRojo" Text="MATERNALES" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td style="height: 33px">
                                <asp:Label ID="lblPreescolar3" runat="server" CssClass="lblRojo" Text="PREESCOLAR" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td style="height: 33px">
                                <asp:Label ID="lblTotal43" runat="server" CssClass="lblRojo" Text="TOTAL" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td style="height: 33px; width: 20px;">
                                </td>
                            <td style="text-align: left;" colspan="2" rowspan="2">
                                <asp:Label ID="lblInstrucciones7" runat="server" CssClass="lblRojo" Text="7. Escriba el n�mero del personal de servicio de nutrici�n."
                                    Width="200px"></asp:Label></td>
                            <td style=" height: 33px; width: 20px;">
                            </td>
                            <td style="height: 33px; text-align: left">
                                <asp:Label ID="lblTotal6" runat="server" CssClass="lblRojo" Text="TOTAL (6)" Width="202px"></asp:Label></td>
                            <td style="height: 33px">
                            <asp:TextBox ID="txtV304" runat="server" Columns="2" TabIndex="15201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="lblEstr13" runat="server" CssClass="lblGrisTit" Text="1" Width="90px"></asp:Label></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV243" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV244" runat="server" Columns="2" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV245" runat="server" Columns="2" TabIndex="10903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV246" runat="server" Columns="2" TabIndex="10904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="lblEstr23" runat="server" CssClass="lblGrisTit" Text="2" Width="90px"></asp:Label></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV247" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV248" runat="server" Columns="2" TabIndex="11002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV249" runat="server" Columns="2" TabIndex="11003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV250" runat="server" Columns="2" TabIndex="11004" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td>
                                <asp:Label ID="lblEconomas7" runat="server" CssClass="lblGrisTit" Text="EC�NOMAS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV281" runat="server" Columns="2" TabIndex="12901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblTotalPers" runat="server" CssClass="lblRojo" Text="TOTAL DE PERSONAL"
                                    Width="202px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV305" runat="server" Columns="3" TabIndex="15301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblEstr33" runat="server" CssClass="lblGrisTit" Text="3" Width="90px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV251" runat="server" Columns="2" TabIndex="11101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV252" runat="server" Columns="2" TabIndex="11102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV253" runat="server" Columns="2" TabIndex="11103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV254" runat="server" Columns="2" TabIndex="11104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblNutriologas7" runat="server" CssClass="lblGrisTit" Text="NUTRI�LOGAS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV282" runat="server" Columns="2" TabIndex="13001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNotaTotal" runat="server" CssClass="lblRojo" Text="(Sume los totales 1 al 6)"
                                    Width="202px"></asp:Label></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblSubtotal3" runat="server" CssClass="lblRojo" Text="SUBTOTAL (3)" Width="90px"></asp:Label></td>
                            <td style=" text-align: center;">
                            </td>
                            <td style=" text-align: center;">
                            </td>
                            <td style=" text-align: center;">
                                </td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV255" runat="server" Columns="2" TabIndex="11201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblDietistas7" runat="server" CssClass="lblGrisTit" Text="DIETISTAS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV283" runat="server" Columns="2" TabIndex="13101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" >
                            </td>
                            <td  >
                            </td>
                            <td>
                                <asp:Label ID="lblCocineras7" runat="server" CssClass="lblGrisTit" Text="COCINERAS" Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV284" runat="server" Columns="2" TabIndex="13201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5"  style="padding-bottom: 10px; width: 90px; 
                                text-align: left">
                                <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="4. Desglose a las asistentes educativas de lactantes  y maternales  por estrato  de edad y a las de preescolar  por grado."
                                    Width="344px"></asp:Label></td>
                            <td  >
                            </td>
                            <td>
                                <asp:Label ID="lblAuxCocina7" runat="server" CssClass="lblGrisTit" Text="AUXILIARES DE COCINA"
                                    Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV285" runat="server" Columns="2" TabIndex="13301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="">
                                </td>
                            <td colspan="" rowspan="">
                                <asp:Label ID="lblLactantes4" runat="server" CssClass="lblRojo" Text="LACTANTES" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblMaternales4" runat="server" CssClass="lblRojo" Text="MATERNALES" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblPreescolar4" runat="server" CssClass="lblRojo" Text="PREESCOLAR" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblTotal44" runat="server" CssClass="lblRojo" Text="TOTAL" Width="51px" Font-Size="8pt"></asp:Label></td>
                            <td>
                                </td>
                            <td>
                                <asp:Label ID="lblEncAlmacen7" runat="server" CssClass="lblGrisTit" Text="ENCARGADOS DE ALMAC�N"
                                    Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV286" runat="server" Columns="2" TabIndex="13401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="lblEstr14" runat="server" CssClass="lblGrisTit" Text="1" Width="90px"></asp:Label></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV256" runat="server" Columns="2" TabIndex="11301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV257" runat="server" Columns="2" TabIndex="11302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV258" runat="server" Columns="2" TabIndex="11303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV259" runat="server" Columns="2" TabIndex="11304" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td>
                                <asp:Label ID="lblEncBcoLeche7" runat="server" CssClass="lblGrisTit" Text="ENCARGADOS DEL BANCO DE LECHE"
                                    Width="121px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV287" runat="server" Columns="2" TabIndex="13501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <asp:Label ID="lblEstr24" runat="server" CssClass="lblGrisTit" Text="2" Width="90px"></asp:Label></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV260" runat="server" Columns="2" TabIndex="11401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV261" runat="server" Columns="2" TabIndex="11402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV262" runat="server" Columns="2" TabIndex="11403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV263" runat="server" Columns="2" TabIndex="11404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td>
                                <asp:Label ID="lblAuxBcoLeche7" runat="server" CssClass="lblGrisTit" Text="AUXILIARES DEL BANCO DE LECHE"
                                    Width="121px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV288" runat="server" Columns="2" TabIndex="13601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblEstr34" runat="server" CssClass="lblGrisTit" Text="3" Width="90px"></asp:Label></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV264" runat="server" Columns="2" TabIndex="11501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV265" runat="server" Columns="2" TabIndex="11502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV266" runat="server" Columns="2" TabIndex="11503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV267" runat="server" Columns="2" TabIndex="11504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                                </td>
                            <td>
                                <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Text="TOTAL (4)" Width="121px"></asp:Label></td>
                            <td>
                            <asp:TextBox ID="txtV289" runat="server" Columns="2" TabIndex="13701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblSubtotal4" runat="server" CssClass="lblRojo" Text="SUBTOTAL (4)" Width="90px"></asp:Label></td>
                            <td style=" text-align: center;">
                            </td>
                            <td style=" text-align: center;">
                            </td>
                            <td style=" text-align: center;">
                                </td>
                            <td style=" text-align: center;">
                                <asp:TextBox ID="txtV268" runat="server" Columns="2" TabIndex="11601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 90px;  text-align: left;" colspan="3">
                                <asp:Label ID="lblNota4" runat="server" CssClass="lblRojo" Text="Sume los subtotales  1 al 4." Width="218px"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblTotal1" runat="server" CssClass="lblRojo" Text="TOTAL (1)" Width="38px"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV269" runat="server" Columns="3" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
    
     <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('APRE_911_EI_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('APRE_911_EI_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="OpenPageCharged('Inmueble_911_EI_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Inmueble_911_EI_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana,ir){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV296'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV298'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV300'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV302'));
                    openPage(ventana,ir);
                } 
                Disparador(<%=this.hidDisparador.Value %>);
                
                 var _enter=true;
                MaxCol = 5;
                MaxRow = 53;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);  
                GetTabIndexes();                        
        </script>   
</asp:Content>