<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-2(Preescolar)" AutoEventWireup="true" CodeBehind="APRE_911_EI_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_EI_2.APRE_911_EI_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />

    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
        <table style="width: 100%;">
                <tr>
                    <td>
                        <span>EDUCACI�N INICIAL</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                           2012-2013</span></td>
                </tr>
                <tr>
                    <td>
                        <span>
                            <asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
                </tr>
            </table>
    </div>
    </div>

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_EI_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ALM_911_EI_2',true)"><a href="#" title="" ><span>ALUMNOS INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_EI_2',true)"><a href="#" title="" class="activo"><span> ALUMNOS PREESCOLAR</span></a></li>
        <li onclick="openPage('Personal_911_EI_2',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td style="width: 340px">
                    <table  style="text-align: center" >
                        <tr>
                            <td colspan="9" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="lblPreescolar" runat="server" CssClass="lblRojo" Font-Size="16px"
                                    Text="III. POBLACI�N INFANTIL ATENDIDA EN LA SECCI�N DE PREESCOLAR"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: left">
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba el total de alumnos que se atendieron en la secci�n de preescolar, desglos�ndolo por grado, sexo, inscripci�n total, existencia, promovidos y edad. Verifique que la suma de los subtotales de los alumnos por edad sea igual al total."
                                    Width="600px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-right: 5px; padding-left: 5px; width: 81px; height: 3px;
                                text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="height: 3px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 3px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                            <td style="width: 67px; height: 3px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" style="width: 81px; height: 3px; text-align: center; padding-right: 5px; padding-left: 5px;">
                                </td>
                            <td colspan="2" rowspan="" style="text-align: center; height: 3px;">
                                </td>
                            <td colspan="" rowspan="" style="width: 67px; text-align: center; height: 3px;">
                                <asp:Label ID="lbl3A�os" runat="server" CssClass="lblRojo" Text="3 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px;">
                                <asp:Label ID="lbl4A�os" runat="server" CssClass="lblRojo" Text="4 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl5A�os" runat="server" CssClass="lblRojo" Text="5 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lbl6A�os" runat="server" CssClass="lblRojo" Text="6 a�os"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="width: 67px; height: 3px">
                                <asp:Label ID="lblGrupos1" runat="server" CssClass="lblRojo" Text="GRUPOS"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="9" style="padding-right: 5px; width: 81px">
                                <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1�"></asp:Label></td>
                            <td style="width: 81px; height: 3px; text-align: left;" rowspan="3">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotH1" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="txtV45" runat="server" Columns="4" TabIndex="10101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="txtV46" runat="server" Columns="4" TabIndex="10102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV47" runat="server" Columns="4" TabIndex="10103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV48" runat="server" Columns="4" TabIndex="10104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV49" runat="server" Columns="4" TabIndex="10105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaH1" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="txtV50" runat="server" Columns="4" TabIndex="10201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px;">
                                <asp:TextBox ID="txtV51" runat="server" Columns="4" TabIndex="10202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV52" runat="server" Columns="4" TabIndex="10203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV53" runat="server" Columns="4" TabIndex="10204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV54" runat="server" Columns="4" TabIndex="10205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosH1" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV55" runat="server" Columns="4" TabIndex="10301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV56" runat="server" Columns="4" TabIndex="10302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV57" runat="server" Columns="4" TabIndex="10303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV58" runat="server" Columns="4" TabIndex="10304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV59" runat="server" Columns="4" TabIndex="10305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 81px; text-align: left; height: 3px;">
                            <asp:Label ID="lblMujeres1" runat="server" CssClass="lblRojo" Text="MUJERES" Height="17px"></asp:Label></td>
                            <td style="width: 120px; height: 26px; text-align: left">
                            <asp:Label ID="lblInscTotM1" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV60" runat="server" Columns="4" TabIndex="10401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV61" runat="server" Columns="4" TabIndex="10402" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV62" runat="server" Columns="4" TabIndex="10403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV63" runat="server" Columns="4" TabIndex="10404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV64" runat="server" Columns="4" TabIndex="10405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left;">
                            <asp:Label ID="lblExistenciaM1" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV65" runat="server" Columns="4" TabIndex="10501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV66" runat="server" Columns="4" TabIndex="10502" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV67" runat="server" Columns="4" TabIndex="10503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV68" runat="server" Columns="4" TabIndex="10504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV69" runat="server" Columns="4" TabIndex="10505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosM1" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV70" runat="server" Columns="4" TabIndex="10601" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV71" runat="server" Columns="4" TabIndex="10602" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV72" runat="server" Columns="4" TabIndex="10603" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV73" runat="server" Columns="4" TabIndex="10604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV74" runat="server" Columns="4" TabIndex="10605" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 81px; height: 3px; text-align: left">
                            <asp:Label ID="lblSubtotal1" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotSubT1" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV75" runat="server" Columns="4" TabIndex="10701" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV76" runat="server" Columns="4" TabIndex="10702" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV77" runat="server" Columns="4" TabIndex="10703" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV78" runat="server" Columns="4" TabIndex="10704" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV79" runat="server" Columns="4" TabIndex="10705" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaSubT1" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV80" runat="server" Columns="4" TabIndex="10801" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV81" runat="server" Columns="4" TabIndex="10802" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV82" runat="server" Columns="4" TabIndex="10803" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV83" runat="server" Columns="4" TabIndex="10804" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV84" runat="server" Columns="4" TabIndex="10805" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosSubT1" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV85" runat="server" Columns="4" TabIndex="10901" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV86" runat="server" Columns="4" TabIndex="10902" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV87" runat="server" Columns="4" TabIndex="10903" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV88" runat="server" Columns="4" TabIndex="10904" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV89" runat="server" Columns="4" TabIndex="10905" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV90" runat="server" Columns="2" TabIndex="10906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 81px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 81px; height: 3px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="9" style="padding-right: 5px; padding-left: 5px; width: 81px;
                                height: 3px; text-align: center">
                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2�"></asp:Label></td>
                            <td rowspan="3" style="width: 81px; height: 3px; text-align: left">
                            <asp:Label ID="lblHombres2" runat="server" CssClass="lblRojo" Text="HOMBRES" Height="17px"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left;">
                                <asp:Label ID="lblInscTotH2" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV91" runat="server" Columns="4" TabIndex="11001" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV92" runat="server" Columns="4" TabIndex="11002" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV93" runat="server" Columns="4" TabIndex="11003" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV94" runat="server" Columns="4" TabIndex="11004" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV95" runat="server" Columns="4" TabIndex="11005" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaH2" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV96" runat="server" Columns="4" TabIndex="11101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV97" runat="server" Columns="4" TabIndex="11102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV98" runat="server" Columns="4" TabIndex="11103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV99" runat="server" Columns="4" TabIndex="11104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV100" runat="server" Columns="4" TabIndex="11105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosH2" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td colspan="1" rowspan="1" style="width: 50px; height: 3px; text-align: left">
                                <asp:TextBox ID="txtV101" runat="server" Columns="4" TabIndex="11201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV102" runat="server" Columns="4" TabIndex="11202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV103" runat="server" Columns="4" TabIndex="11203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV104" runat="server" Columns="4" TabIndex="11204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV105" runat="server" Columns="4" TabIndex="11205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="3" style="width: 81px; height: 3px; text-align: left">
                            <asp:Label ID="lblMujeres2" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotM2" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV106" runat="server" Columns="4" TabIndex="11301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV107" runat="server" Columns="4" TabIndex="11302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV108" runat="server" Columns="4" TabIndex="11303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV109" runat="server" Columns="4" TabIndex="11304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV110" runat="server" Columns="4" TabIndex="11305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaM2" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV111" runat="server" Columns="4" TabIndex="11401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV112" runat="server" Columns="4" TabIndex="11402" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV113" runat="server" Columns="4" TabIndex="11403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV114" runat="server" Columns="4" TabIndex="11404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV115" runat="server" Columns="4" TabIndex="11405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosM2" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV116" runat="server" Columns="4" TabIndex="11501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV117" runat="server" Columns="4" TabIndex="11502" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV118" runat="server" Columns="4" TabIndex="11503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV119" runat="server" Columns="4" TabIndex="11504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV120" runat="server" Columns="4" TabIndex="11505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 81px; height: 3px; text-align: left">
                            <asp:Label ID="lblSubtotal2" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotSubT2" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV121" runat="server" Columns="4" TabIndex="11601" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV122" runat="server" Columns="4" TabIndex="11602" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV123" runat="server" Columns="4" TabIndex="11603" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV124" runat="server" Columns="4" TabIndex="11604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV125" runat="server" Columns="4" TabIndex="11605" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaSubT2" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td style="width: 50px; height: 3px; text-align: left">
                            <asp:TextBox ID="txtV126" runat="server" Columns="4" TabIndex="11701" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV127" runat="server" Columns="4" TabIndex="11702" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV128" runat="server" Columns="4" TabIndex="11703" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV129" runat="server" Columns="4" TabIndex="11704" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV130" runat="server" Columns="4" TabIndex="11705" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosSubT2" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td style="width: 50px; height: 3px; text-align: left">
                            <asp:TextBox ID="txtV131" runat="server" Columns="4" TabIndex="11801" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV132" runat="server" Columns="4" TabIndex="11802" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV133" runat="server" Columns="4" TabIndex="11803" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV134" runat="server" Columns="4" TabIndex="11804" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV135" runat="server" Columns="4" TabIndex="11805" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV136" runat="server" Columns="2" TabIndex="11806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 81px;
                                height: 3px; text-align: center">
                            </td>
                            <td colspan="2" rowspan="1" style="width: 81px; height: 3px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="9" style="padding-right: 5px; padding-left: 5px; width: 81px;
                                height: 3px; text-align: center">
                            <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3�"></asp:Label></td>
                            <td rowspan="3" style="width: 81px; height: 3px; text-align: left">
                            <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left;">
                                <asp:Label ID="lblInscTotH3" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV137" runat="server" Columns="4" TabIndex="11901" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV138" runat="server" Columns="4" TabIndex="11902" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV139" runat="server" Columns="4" TabIndex="11903" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV140" runat="server" Columns="4" TabIndex="11904" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV141" runat="server" Columns="4" TabIndex="11905" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaH3" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV142" runat="server" Columns="4" TabIndex="12001" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV143" runat="server" Columns="4" TabIndex="12002" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV144" runat="server" Columns="4" TabIndex="12003" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV145" runat="server" Columns="4" TabIndex="12004" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV146" runat="server" Columns="4" TabIndex="12005" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosH3" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV147" runat="server" Columns="4" TabIndex="12101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV148" runat="server" Columns="4" TabIndex="12102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV149" runat="server" Columns="4" TabIndex="12103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV150" runat="server" Columns="4" TabIndex="12104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV151" runat="server" Columns="4" TabIndex="12105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="3" style="width: 81px; height: 3px; text-align: left">
                            <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotM3" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV152" runat="server" Columns="4" TabIndex="12201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV153" runat="server" Columns="4" TabIndex="12202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV154" runat="server" Columns="4" TabIndex="12203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV155" runat="server" Columns="4" TabIndex="12204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV156" runat="server" Columns="4" TabIndex="12205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaM3" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV157" runat="server" Columns="4" TabIndex="12301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV158" runat="server" Columns="4" TabIndex="12302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV159" runat="server" Columns="4" TabIndex="12303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV160" runat="server" Columns="4" TabIndex="12304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV161" runat="server" Columns="4" TabIndex="12305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 27px; text-align: left">
                                <asp:Label ID="lblPromovidosM3" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 27px; text-align: left">
                            <asp:TextBox ID="txtV162" runat="server" Columns="4" TabIndex="12401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 27px">
                            <asp:TextBox ID="txtV163" runat="server" Columns="4" TabIndex="12402" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: left">
                            <asp:TextBox ID="txtV164" runat="server" Columns="4" TabIndex="12403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px">
                            <asp:TextBox ID="txtV165" runat="server" Columns="4" TabIndex="12404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px">
                            <asp:TextBox ID="txtV166" runat="server" Columns="4" TabIndex="12405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 81px; height: 3px; text-align: left">
                            <asp:Label ID="lblSubtotal3" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotSubT3" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV167" runat="server" Columns="4" TabIndex="12501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV168" runat="server" Columns="4" TabIndex="12502" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV169" runat="server" Columns="4" TabIndex="12503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV170" runat="server" Columns="4" TabIndex="12504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV171" runat="server" Columns="4" TabIndex="12505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaSubT3" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV172" runat="server" Columns="4" TabIndex="12601" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV173" runat="server" Columns="4" TabIndex="12602" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV174" runat="server" Columns="4" TabIndex="12603" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV175" runat="server" Columns="4" TabIndex="12604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV176" runat="server" Columns="4" TabIndex="12605" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosSubT3" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV177" runat="server" Columns="4" TabIndex="12701" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV178" runat="server" Columns="4" TabIndex="12702" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV179" runat="server" Columns="4" TabIndex="12703" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV180" runat="server" Columns="4" TabIndex="12704" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV181" runat="server" Columns="4" TabIndex="12705" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV182" runat="server" Columns="2" TabIndex="12706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 81px;
                                height: 3px; text-align: center">
                            </td>
                            <td rowspan="1" style="width: 81px; height: 3px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                            </td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="9" style="padding-right: 5px; padding-left: 5px; width: 81px;
                                height: 3px; text-align: center">
                                <asp:Label ID="Label36" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td rowspan="3" style="width: 81px; height: 3px; text-align: left">
                                <asp:Label ID="Label33" runat="server" CssClass="lblRojo" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotHT" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV183" runat="server" Columns="4" TabIndex="12801" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV184" runat="server" Columns="4" TabIndex="12802" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV185" runat="server" Columns="4" TabIndex="12803" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV186" runat="server" Columns="4" TabIndex="12804" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV187" runat="server" Columns="4" TabIndex="12805" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaHT" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV188" runat="server" Columns="4" TabIndex="12901" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV189" runat="server" Columns="4" TabIndex="12902" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV190" runat="server" Columns="4" TabIndex="12903" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV191" runat="server" Columns="4" TabIndex="12904" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV192" runat="server" Columns="4" TabIndex="12905" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosHT" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV193" runat="server" Columns="4" TabIndex="13001" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV194" runat="server" Columns="4" TabIndex="13002" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV195" runat="server" Columns="4" TabIndex="13003" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV196" runat="server" Columns="4" TabIndex="13004" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV197" runat="server" Columns="4" TabIndex="13005" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="3" style="width: 81px; height: 3px; text-align: left">
                                <asp:Label ID="Label34" runat="server" CssClass="lblRojo" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotMT" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV198" runat="server" Columns="4" TabIndex="13101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV199" runat="server" Columns="4" TabIndex="13102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV200" runat="server" Columns="4" TabIndex="13103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV201" runat="server" Columns="4" TabIndex="13104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV202" runat="server" Columns="4" TabIndex="13105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaMT" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV203" runat="server" Columns="4" TabIndex="13201" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV204" runat="server" Columns="4" TabIndex="13202" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV205" runat="server" Columns="4" TabIndex="13203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV206" runat="server" Columns="4" TabIndex="13204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV207" runat="server" Columns="4" TabIndex="13205" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosMT" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                            <asp:TextBox ID="txtV208" runat="server" Columns="4" TabIndex="13301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                            <asp:TextBox ID="txtV209" runat="server" Columns="4" TabIndex="13302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV210" runat="server" Columns="4" TabIndex="13303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV211" runat="server" Columns="4" TabIndex="13304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV212" runat="server" Columns="4" TabIndex="13305" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="3" style="width: 81px; height: 3px; text-align: left">
                                <asp:Label ID="Label35" runat="server" CssClass="lblRojo" Text="SUBTOTAL"></asp:Label></td>
                            <td rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblInscTotSubTT" runat="server" CssClass="lblGrisTit" Text="INSCRIPCI�N TOTAL"
                                    Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV213" runat="server" Columns="4" TabIndex="13401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV214" runat="server" Columns="4" TabIndex="13402" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV215" runat="server" Columns="4" TabIndex="13403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV216" runat="server" Columns="4" TabIndex="13404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV217" runat="server" Columns="4" TabIndex="13405" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblExistenciaSubTT" runat="server" CssClass="lblGrisTit" Text="EXISTENCIA" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV218" runat="server" Columns="4" TabIndex="13501" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV219" runat="server" Columns="4" TabIndex="13502" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV220" runat="server" Columns="4" TabIndex="13503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV221" runat="server" Columns="4" TabIndex="13504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV222" runat="server" Columns="4" TabIndex="13505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 120px; height: 26px; text-align: left">
                                <asp:Label ID="lblPromovidosSubTT" runat="server" CssClass="lblGrisTit" Text="PROMOVIDOS" Width="120px"></asp:Label></td>
                            <td rowspan="1" style="width: 40px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV223" runat="server" Columns="4" TabIndex="13601" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV224" runat="server" Columns="4" TabIndex="13602" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td rowspan="1" style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV225" runat="server" Columns="4" TabIndex="13603" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV226" runat="server" Columns="4" TabIndex="13604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV227" runat="server" Columns="4" TabIndex="13605" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV228" runat="server" Columns="2" TabIndex="13606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
     <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('ALM_911_EI_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('ALM_911_EI_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal_911_EI_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_EI_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        
       
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />       
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                Disparador(<%=this.hidDisparador.Value %>);
                
                 var _enter=true;
                MaxCol = 7;
                MaxRow = 37;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);  
                GetTabIndexes();               
        </script> 
</asp:Content>