using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_4
{
    public partial class AGT_911_4 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV514.Attributes["onkeypress"] = "return Num(event)";
                txtV515.Attributes["onkeypress"] = "return Num(event)";
                txtV516.Attributes["onkeypress"] = "return Num(event)";
                txtV517.Attributes["onkeypress"] = "return Num(event)";
                txtV518.Attributes["onkeypress"] = "return Num(event)";
                txtV519.Attributes["onkeypress"] = "return Num(event)";
                txtV520.Attributes["onkeypress"] = "return Num(event)";
                txtV521.Attributes["onkeypress"] = "return Num(event)";
                txtV522.Attributes["onkeypress"] = "return Num(event)";
                txtV523.Attributes["onkeypress"] = "return Num(event)";
                txtV524.Attributes["onkeypress"] = "return Num(event)";
                txtV525.Attributes["onkeypress"] = "return Num(event)";
                txtV526.Attributes["onkeypress"] = "return Num(event)";
                txtV527.Attributes["onkeypress"] = "return Num(event)";
                txtV528.Attributes["onkeypress"] = "return Num(event)";
                txtV529.Attributes["onkeypress"] = "return Num(event)";
                txtV530.Attributes["onkeypress"] = "return Num(event)";
                txtV531.Attributes["onkeypress"] = "return Num(event)";
                txtV532.Attributes["onkeypress"] = "return Num(event)";
                txtV533.Attributes["onkeypress"] = "return Num(event)";
                txtV534.Attributes["onkeypress"] = "return Num(event)";
                txtV535.Attributes["onkeypress"] = "return Num(event)";
                txtV536.Attributes["onkeypress"] = "return Num(event)";
                txtV537.Attributes["onkeypress"] = "return Num(event)";
                txtV538.Attributes["onkeypress"] = "return Num(event)";
                txtV539.Attributes["onkeypress"] = "return Num(event)";
                txtV540.Attributes["onkeypress"] = "return Num(event)";
                txtV541.Attributes["onkeypress"] = "return Num(event)";
                txtV542.Attributes["onkeypress"] = "return Num(event)";
                txtV543.Attributes["onkeypress"] = "return Num(event)";
                txtV544.Attributes["onkeypress"] = "return Num(event)";
                txtV545.Attributes["onkeypress"] = "return Num(event)";
                txtV546.Attributes["onkeypress"] = "return Num(event)";
                txtV547.Attributes["onkeypress"] = "return Num(event)";
                txtV548.Attributes["onkeypress"] = "return Num(event)";
                txtV549.Attributes["onkeypress"] = "return Num(event)";
                txtV550.Attributes["onkeypress"] = "return Num(event)";
                txtV551.Attributes["onkeypress"] = "return Num(event)";
                txtV552.Attributes["onkeypress"] = "return Num(event)";
                txtV553.Attributes["onkeypress"] = "return Num(event)";
                txtV554.Attributes["onkeypress"] = "return Num(event)";
                txtV555.Attributes["onkeypress"] = "return Num(event)";
                txtV556.Attributes["onkeypress"] = "return Num(event)";
                txtV557.Attributes["onkeypress"] = "return Num(event)";
                txtV558.Attributes["onkeypress"] = "return Num(event)";
                txtV559.Attributes["onkeypress"] = "return Num(event)";
                txtV560.Attributes["onkeypress"] = "return Num(event)";
                txtV561.Attributes["onkeypress"] = "return Num(event)";
                txtV562.Attributes["onkeypress"] = "return Num(event)";
                txtV563.Attributes["onkeypress"] = "return Num(event)";
                txtV564.Attributes["onkeypress"] = "return Num(event)";
                txtV565.Attributes["onkeypress"] = "return Num(event)";
                txtV566.Attributes["onkeypress"] = "return Num(event)";
                txtV567.Attributes["onkeypress"] = "return Num(event)";
                txtV568.Attributes["onkeypress"] = "return Num(event)";
                txtV569.Attributes["onkeypress"] = "return Num(event)";
                txtV570.Attributes["onkeypress"] = "return Num(event)";
                txtV571.Attributes["onkeypress"] = "return Num(event)";
                txtV572.Attributes["onkeypress"] = "return Num(event)";
                txtV573.Attributes["onkeypress"] = "return Num(event)";
                txtV574.Attributes["onkeypress"] = "return Num(event)";
                txtV575.Attributes["onkeypress"] = "return Num(event)";
                txtV576.Attributes["onkeypress"] = "return Num(event)";
                txtV577.Attributes["onkeypress"] = "return Num(event)";
                txtV578.Attributes["onkeypress"] = "return Num(event)";
                txtV579.Attributes["onkeypress"] = "return Num(event)";
                txtV580.Attributes["onkeypress"] = "return Num(event)";
                txtV581.Attributes["onkeypress"] = "return Num(event)";
                txtV582.Attributes["onkeypress"] = "return Num(event)";
                txtV583.Attributes["onkeypress"] = "return Num(event)";
                txtV584.Attributes["onkeypress"] = "return Num(event)";
                txtV585.Attributes["onkeypress"] = "return Num(event)";
                txtV586.Attributes["onkeypress"] = "return Num(event)";
                txtV587.Attributes["onkeypress"] = "return Num(event)";
                txtV588.Attributes["onkeypress"] = "return Num(event)";
                txtV589.Attributes["onkeypress"] = "return Num(event)";
                txtV590.Attributes["onkeypress"] = "return Num(event)";
                txtV591.Attributes["onkeypress"] = "return Num(event)";
                txtV592.Attributes["onkeypress"] = "return Num(event)";
                txtV593.Attributes["onkeypress"] = "return Num(event)";
                txtV594.Attributes["onkeypress"] = "return Num(event)";
                txtV595.Attributes["onkeypress"] = "return Num(event)";
                txtV596.Attributes["onkeypress"] = "return Num(event)";
                txtV597.Attributes["onkeypress"] = "return Num(event)";
                txtV598.Attributes["onkeypress"] = "return Num(event)";
                txtV599.Attributes["onkeypress"] = "return Num(event)";
                txtV600.Attributes["onkeypress"] = "return Num(event)";
                txtV601.Attributes["onkeypress"] = "return Num(event)";
                txtV602.Attributes["onkeypress"] = "return Num(event)";
                txtV603.Attributes["onkeypress"] = "return Num(event)";
                txtV604.Attributes["onkeypress"] = "return Num(event)";
                txtV605.Attributes["onkeypress"] = "return Num(event)";
                txtV606.Attributes["onkeypress"] = "return Num(event)";
                txtV607.Attributes["onkeypress"] = "return Num(event)";
                txtV608.Attributes["onkeypress"] = "return Num(event)";
                txtV609.Attributes["onkeypress"] = "return Num(event)";
                txtV610.Attributes["onkeypress"] = "return Num(event)";
                txtV611.Attributes["onkeypress"] = "return Num(event)";
                txtV612.Attributes["onkeypress"] = "return Num(event)";
                txtV613.Attributes["onkeypress"] = "return Num(event)";
                txtV614.Attributes["onkeypress"] = "return Num(event)";
                txtV615.Attributes["onkeypress"] = "return Num(event)";
                txtV616.Attributes["onkeypress"] = "return Num(event)";
                txtV617.Attributes["onkeypress"] = "return Num(event)";
                txtV618.Attributes["onkeypress"] = "return Num(event)";
                txtV619.Attributes["onkeypress"] = "return Num(event)";
                txtV620.Attributes["onkeypress"] = "return Num(event)";
                txtV621.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();


                //lnkModificacionDatosAl.NavigateUrl = Class911.Liga_ModificacionDatosAlumno(controlDP);
                //lnkControlEscolarABC.NavigateUrl = Class911.Liga_ControlEscolar(controlDP);
                //lnkCalificaciones.NavigateUrl = Class911.Liga_Calificaciones(controlDP);

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        protected void lnkRefrescar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
            // Actualiza Info
            Class911.CargaInicialCuestionario(controlDP, 1);
            // Recarga datos
            hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);
            Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

            #region Volver a escribir JS Call Back
            String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
            String callbackScript_Variables;
            callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
            #endregion

        }
        //*********************************


    }
}
