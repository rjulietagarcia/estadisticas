<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="Oficializar_911_8N.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8N.Oficializar_911_8N" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
<div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">
    <table>
        <tr><td style="width:120px;"></td><td><span>EDUCACIÓN NORMAL</span></td>
        </tr>
        <tr><td></td><td><span>2012-2013</span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCentroTrabajo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul id="lista1" class="left">
        <li onclick="openPage('Identificacion_911_8N',true)"><a href="#" title="" ><span>IDENTIFICACIÓN</span></a></li>
        <li onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title="" ><span>CARACTERÍSTICAS Y PERSONAL</span></a></li>
        <li onclick="openPage('ACG_911_8N',true)"><a href="#" title="" ><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('Total_911_8N',true)"><a href="#" title="" ><span>TOTAL DE LICENCIATURAS</span></a></li>
        <li onclick="openPage('TotalAlumn_911_8N',true)"><a href="#" title=""><span>TOTAL ALUMNOS POR GRUPO</span></a></li>
        <li onclick="openPage('PlantelesEx_911_8N',true)"><a href="#" title="" ><span>PLANTELES DE EXTENSIÓN</span></a></li>
        <li onclick="openPage('Inmueble_911_8N',true)"><a href="#" title="" ><span>INMUEBLE</span></a></li>
        <li onclick="openPage('Anexo_911_8N',true)"><a href="#" title="" ><span>ANEXO</span></a></li>
        <li onclick="openPage('Oficializar_911_8N',true)"><a href="#" title="" class="activo"><span>OFICIALIZACIÓN</span></a></li></ul>
      <ul  id="lista2" style="display:none;" class="left">
        <li onclick="openPage('Identificacion_911_8N',false)"><a href="#" title="" ><span>IDENTIFICACIÓN</span></a></li>
        <li onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title="" ><span>CARACTERÍSTICAS Y PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL DE LICENCIATURAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL ALUMNOS POR GRUPO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PLANTELES DE EXTENSIÓN</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li ><a href="#" title="" class="activo"><span>OFICIALIZACIÓN</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br /> 
    <div id="tooltipayuda" class="balloonstyle">
    <p>En la sección de observaciones favor de referir las correcciones que deban realizarse a los datos de identificación del plantel y notificar alguna queja, inquietud o sugerencia para mejorar el proceso del levantamiento estadístico.</p>
    <p>No debe oficializar la estadística si no ha terminado el ingreso de calificaciones en el sistema.</p>
    <p>Si la escuela no generará estadísticas educativas favor de indicar el motivo y dar clic en el botón de OFICIALIZAR.</p>
    <p>Una vez concluido el trabajo de revisar y complementar la información se procede a oficializar la Estadística Educativa presionado un clic en el botón OFICIALIZAR.</p>
    <p>El sistema realizará una serie de validaciones para detectar inconsistencias en los datos y si la información es válida se dará por concluido el levantamiento estadístico, y se habilitarán las opciones para generar el comprobante de captura y la impresión del cuestionario lleno.</p>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

     <center>

                <table class="fondot" align="center" cellspacing="0" cellpadding="0"  border="0"> 
                    <tr> 
                        <td class="EsqSupIzq" style="width: 15px"> 
                        </td> 
                        <td class="RepSup"> 
                        </td> 
                        <td class="EsqSupDer"> 
                        </td> 
                    </tr> 
                    <tr> 
                        <td class="RepLatIzq" style="width: 15px"> 
                        </td> 
                        <td> 
                            <table id="Table2" cellspacing="0" cellpadding="0" width="440" border="0"> 
                                <tr> 
                                    <td> 
                                    </td> 
                                </tr> 
                                <tr> 
                                    <td> 
                                        <table style="width: 620px">
                    <tr>
                        <td colspan="2" style="height: 28px">
                            <br />
                            <asp:Label ID="Label2" runat="server" CssClass="titulopagina" Text="OFICIALIZAR LA INFORMACIÓN ESTADÍSTICA" Font-Size="16pt"></asp:Label></td>
                    </tr>
                    <tr>
                        <td  colspan="2" >
                            <table style="width:90%;">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Text="Observaciones:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtObservaciones" runat="server" Height="74px" MaxLength="1000" TextMode="MultiLine" Width="97%"></asp:TextBox>
                                         <br />
                                        <asp:Button ID="cmdGuardarObs" runat="server" CssClass="botones2" OnClick="cmdGuardarObs_Click"
                                            Text="Guardar Observacion" />
                                    </td>
                                </tr>
                            </table>
                           </td>
                           
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;<asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="Declaro los datos ingresados en el cuestionario como verídicos y finalizo la captura de los mismos." Width="264px"></asp:Label></td>
                        <td>
                            <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Text="No fue posible contestar el cuestionario por el siguiente motivo:"
                                Width="222px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Button ID="cmdOficializar" runat="server" CssClass="botones2" Text="Oficializar" OnClick="cmdOficializar_Click" /></td>
                        <td>
                            <asp:DropDownList ID="ddlMotivos" runat="server" Width="228px" CssClass="lblNegro">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Text="Generar comprobante de captura."></asp:Label></td>
                        <td>
                            <asp:Button ID="cmdGenerarComprobante" runat="server" CssClass="botones2" Text="Aceptar" OnClick="cmdGenerarComprobante_Click" /></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Text="Imprimir el cuestionario lleno."></asp:Label></td>
                        <td>
                            <asp:Button ID="cmdImprimirCuestionario_Lleno" runat="server" CssClass="botones2" Text="Aceptar" OnClick="cmdImprimirCuestionario_Lleno_Click" /></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Text="Imprimir solo el formato del cuestionario."></asp:Label></td>
                        <td>
                            <asp:Button ID="cmdImprimircuestionario" runat="server" CssClass="botones2" Text="Aceptar" OnClick="cmdImprimircuestionario_Click" /></td>
                    </tr>
                </table>
                                    </td> 
                                </tr> 
                                <tr> 
                                    <td> 
                                    </td> 
                                </tr> 
                            </table> 
                        </td> 
                        <td class="RepLatDer"> 
                        </td> 
                    </tr> 
                    <tr> 
                        <td class="EsqInfIzq" style="height: 18px; width: 15px;"> 
                        </td> 
                        <td class="RepInf" style="height: 18px"> 
                        </td> 
                        <td class="EsqInfDer" style="height: 18px"> 
                        </td> 
                    </tr> 
                </table>              
    
            &nbsp;
              
            
            <div > 
                <asp:Label ID="lblResultado" runat="server"></asp:Label>
            </div>
 


 
            
            <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
            <input id="hidIdCtrl" type="hidden" runat= "server" />
            <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
            <script type="text/javascript">
                function openPage(page){window.location.href=page+".aspx";}
            </script> 
        
         <asp:Table runat="server" ID="table1" Visible="False">
             <asp:TableRow ID="TableRow1" runat="server">
                 <asp:TableCell ID="TableCell1" runat="server" CssClass="Bordes1">Base de Datos</asp:TableCell>
                 <asp:TableCell ID="TableCell2" runat="server" CssClass="Bordes2">Web Service</asp:TableCell>
             </asp:TableRow> 
          </asp:Table>
        </center>
        <center>
            &nbsp;</center>
        <center>
         <asp:Table ID="tblDatos" runat="server" Visible="False" >    
             <asp:TableRow ID="TableRow2" runat="server">
                 <asp:TableCell ID="TableCell3" runat="server" CssClass="BordesX">NoVar</asp:TableCell>
                 <asp:TableCell ID="TableCell4" runat="server" CssClass="BordesX">Valor</asp:TableCell>
             </asp:TableRow>
         </asp:Table>
            &nbsp;</center>
        <center>
            <asp:Table ID="tblFallas" runat="server" Visible="False" >
                <asp:TableRow ID="TableRow3" runat="server">
                    <asp:TableCell ID="TableCell5" runat="server" CssClass="BordesX">NoVar</asp:TableCell>
                    <asp:TableCell ID="TableCell6" runat="server" CssClass="BordesX">Descripción</asp:TableCell>
                    <asp:TableCell ID="TableCell7" runat="server" CssClass="BordesX">Inconsitencia</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </center>
        <asp:HiddenField ID="hidRuta" runat="server" />
         <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script language = "javascript" type="text/javascript">
        motivadoOficializar();
           function AbrirPDF(){
               var ruta = document.getElementById("ctl00_cphMainMaster_hidRuta").value
               if (ruta != ""){
                    window.open (ruta,'','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668')
               }
           }
           AbrirPDF();
        </script>
</asp:Content>
