<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="Caracteristicas_911_8N.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8N.Caracteristicas_911_8N" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <%--Agregado--%>
    <script type="text/javascript">
        var _enter=true;
        MaxCol = 5;
        MaxRow = 15;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
   

    <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">
    <table>
        <tr><td style="width:120px;"></td><td><span>EDUCACI�N NORMAL</span></td>
        </tr>
        <tr><td></td><td><span>2012-2013</span></td>
        </tr>
        <tr><td></td><td><span><asp:Label ID="lblCentroTrabajo" runat="server" Text="temp"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8N',true)"><a href="#" title="" ><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Caracteristicas_911_8N',true)"><a href="#" title="" class="activo"><span>CARACTER�STICAS Y PERSONAL</span></a></li>
        <li onclick="openPage('ACG_911_8N',false)"><a href="#" title="" ><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL DE LICENCIATURAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL ALUMNOS POR GRUPO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PLANTELES DE EXTENSI�N</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    
    <br />
    <div id="tooltipayuda" class="balloonstyle">
        <p>UTILICE EL CUESTIONARIO PARA REPORTAR LICENCIATURAS Y OTRO PARA PROGRAMAS DE POSGRADO</p>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td valign="top">
                    <table style="width: 450px">
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Font-Size="16px" Text="I. CARACTER�STICAS" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="1. Escriba la existencia de alumnos de licenciatura o posgrado, seg�n el tipo de servicio.<br/>Incluya a los alumnos que son atendidos en los planteles de extensi�n o m�dulos."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: right">
                    </td>
                <td style="text-align:center">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ALUMNOS"
                        Width="100%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblPreescolar" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PREESCOLAR"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV2" runat="server" Columns="4" MaxLength="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblPrimaria" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PRIMARIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV4" runat="server" Columns="4" MaxLength="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblSecundaria" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SECUNDARIA, CURSOS ORDINARIOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV6" runat="server" Columns="4" MaxLength="3" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEspecial" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ESPECIAL, CURSOS ORDINARIOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV8" runat="server" Columns="4" MaxLength="3" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEduFisica" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EDUCACI�N F�SICA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV10" runat="server" Columns="4" MaxLength="3" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblTotalC" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV12" runat="server" Columns="4" MaxLength="3" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Escriba el n�mero de licenciaturas o programas de posgrado en el nivel de estudios que le corresponda e indique la existencia de alumnos de cada nivel, incluyendo los de tronco com�n, y los que son atendidos en los planteles de extensi�n o m�dulos."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td colspan="2"></td>
                <td style="text-align:center">
                    <asp:Label ID="lblLicProg" runat="server" CssClass="lblRojo" Font-Bold="True" Text="LICENCIATURAS/PROGRAMAS"
                        Width="100%"></asp:Label></td>
                <td style="text-align:center">
                    <asp:Label ID="lblTAlumn" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ALUMNOS"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left">
                    <asp:Label ID="lblLicenciatura" runat="server" CssClass="lblRojo" Font-Bold="True" Text="LICENCIATURA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV13" runat="server" Columns="4" MaxLength="3" TabIndex="10701" CssClass="lblNegro"></asp:TextBox>
                    </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV14" runat="server" Columns="4" MaxLength="3" TabIndex="10702" CssClass="lblNegro"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td rowspan="3"  style="text-align: left">
                    <asp:Label ID="lblProg" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PROGRAMAS DE POSGRADO"
                        Width="100%"></asp:Label></td>
                 <td style="text-align: left">
                    <asp:Label ID="lblEspecialidad" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ESPECIALIDAD"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV15" runat="server" Columns="4" MaxLength="3" TabIndex="10801" CssClass="lblNegro"></asp:TextBox>
                    </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV16" runat="server" Columns="4" MaxLength="3" TabIndex="10802" CssClass="lblNegro"></asp:TextBox>
                    </td>
            </tr>
            <tr>
               
                 <td style="text-align: left">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MAESTRIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV17" runat="server" Columns="4" MaxLength="3" TabIndex="10901" CssClass="lblNegro"></asp:TextBox>
                    </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV18" runat="server" Columns="4" MaxLength="3" TabIndex="10902" CssClass="lblNegro"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                
                 <td style="text-align: left">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DOCTORADO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV19" runat="server" Columns="4" MaxLength="3" TabIndex="11001" CssClass="lblNegro"></asp:TextBox>
                    </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV20" runat="server" Columns="4" MaxLength="3" TabIndex="11002" CssClass="lblNegro"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV21" runat="server" Columns="4" MaxLength="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox>
                    </td>
                    <td style="text-align: center">
                    <asp:TextBox ID="txtV22" runat="server" Columns="4" MaxLength="3" TabIndex="11102" CssClass="lblNegro"></asp:TextBox>
                    </td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table style="width: 450px">
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblPERSONAL" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Font-Size="16px" Text="II. PERSONAL POR FUNCI�N" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInstruccionIII1" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="1. Escriba el personal seg�n la funci�n que realiza, independientemente de su nombramiento, tipo y fuente de pago. Si una persona desempe�a dos o m�s funciones, an�tela en aqu�lla a la quededique m�s tiempo."
                                    Width="100%"></asp:Label>
                                    <br />
                                <asp:Label ID="lblNota" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="Nota: Considere exclusivamente al personal que labora en el turno al que se refiere este cuestionario, independientemente de que labore en otro turno."
                                    Width="100%"></asp:Label>
                                    <br /><br />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblDirectivo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PERSONAL DIRECTIVO"
                        Width="100%"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblConGrupo" runat="server" CssClass="lblRojo" Font-Bold="True" Text="CON GRUPO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV23" runat="server" Columns="4" MaxLength="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblSinGrupo" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SIN GRUPO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV24" runat="server" Columns="4" MaxLength="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblDocente" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PERSONAL DOCENTE"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV25" runat="server" Columns="4" MaxLength="3" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblDocenteEsp" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PERSONAL DOCENTE ESPECIAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEdFisica" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PROFESORES DE EDUCACI�N F�SICA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV26" runat="server" Columns="4" MaxLength="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblActArt" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PROFESORES DE ACTIVIDADES ART�STICAS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV27" runat="server" Columns="4" MaxLength="2" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblActTec" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PROFESORES DE ACTIVIDADES TECNOL�GICAS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV28" runat="server" Columns="4" MaxLength="2" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblIdiomas" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PROFESORES DE IDIOMAS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV565" runat="server" Columns="4" MaxLength="2" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblAdministrativo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV29" runat="server" Columns="4" MaxLength="3" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblTotalP" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL DE PERSONAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: right">
                    <asp:TextBox ID="txtV30" runat="server" Columns="4" MaxLength="4" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                    
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                            <br />
                            
                                <asp:Label ID="lblInstruccionIII2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Sume el personal directivo con grupo, personal docente y personal docente especial, y an�telo seg�n el tiempo que dedica a la funci�n acad�mica."
                                    Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblTCompleto" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TIEMPO COMPLETO"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV532" runat="server" Columns="4" MaxLength="3" TabIndex="21001" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblT3cuartos" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TRES CUARTOS DE TIEMPO"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV533" runat="server" Columns="4" MaxLength="3" TabIndex="21101" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblTmedio" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MEDIO TIEMPO"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV534" runat="server" Columns="4" MaxLength="3" TabIndex="21201" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblTHoras" runat="server" CssClass="lblRojo" Font-Bold="True" Text="POR HORAS"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV535" runat="server" Columns="4" MaxLength="3" TabIndex="21301" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTTotal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV536" runat="server" Columns="4" MaxLength="4" TabIndex="21401" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
                
            </tr>
        </table>
        <br />
       <hr />
        <table  align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_8N',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_8N',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('ACG_911_8N',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('ACG_911_8N',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input type="hidden" id="hidIdCtrl"  runat= "server" />
        <input type="hidden" id="hidListaTxtBoxs"  runat = "server" />
        <input type="hidden" id="hidDisparador"  runat = "server" value="90" /><br />
        </center>
        
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
         
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
</asp:Content>
