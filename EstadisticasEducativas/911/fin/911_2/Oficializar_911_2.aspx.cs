using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_2
{
    public partial class Oficializar_911_2 : System.Web.UI.Page
    {
        ArrayList listaVariablesDP = new ArrayList();
        ServiceEstadisticas wsEstadisiticas = new ServiceEstadisticas();
        ControlDP controlDP = null;

        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                if (controlDP != null)
                {
                    ObservacionesDP observacionDP = wsEstadisiticas.LeerObservacion(controlDP.ID_Control);
                    txtObservaciones.Text = observacionDP.Observacion;

                    this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";

                    LlenarMotivos();
                    EstadoBotones(controlDP);

                    if (controlDP.Estatus == 0)
                        pnlOficializado.Visible = false;
                    else
                        pnlOficializado.Visible = true;
                }
            }

        }
        private void LlenarMotivos()
        {
            ddlMotivos.Items.Add(new ListItem("Seleccionar ...", "0"));
            ddlMotivos.Items.Add(new ListItem("1.- La escuela est� en proceso de clausura", "1"));
            ddlMotivos.Items.Add(new ListItem("2.- Falta de personal Docente", "2"));
            ddlMotivos.Items.Add(new ListItem("3.- Falta de alumnos", "3"));
            ddlMotivos.Items.Add(new ListItem("4.- Incumplimiento del Director", "4"));
            ddlMotivos.Items.Add(new ListItem("5.- Escuelas de nueva creaci�n", "5"));
            ddlMotivos.Items.Add(new ListItem("6.- Causa administrativa", "6"));
            ddlMotivos.Items.Add(new ListItem("7.- No corresponde la fecha de levantamiento con el inicio de cursos de la escuela", "7"));
            ddlMotivos.Items.Add(new ListItem("8.- Compactaci�n de turno", "8"));
            ddlMotivos.Items.Add(new ListItem("9.- Cambio de turno", "9"));
            //10.- Cerrado
        }
        private void EstadoBotones(ControlDP controlDP)
        {
            if (controlDP.Estatus == 0)
            {
                cmdOficializar.Enabled = true;
                cmdGenerarComprobante.Enabled = false;
                cmdImprimircuestionario.Enabled = true;
                cmdImprimirCuestionario_Lleno.Enabled = false; // ??
                cmdGuardarObs.Enabled = true;
                ddlMotivos.Enabled = true;
            }
            else
            {
                cmdOficializar.Enabled = false;
                ddlMotivos.Enabled = false;
                cmdGenerarComprobante.Enabled = true;
                cmdImprimirCuestionario_Lleno.Enabled = true;
                cmdImprimircuestionario.Enabled = true; // ??
                cmdGuardarObs.Enabled = false;
                if (controlDP.Estatus != 10)
                    ddlMotivos.SelectedValue = controlDP.Estatus.ToString();
            }
        }

        #region Metodos y Propiedades para Oficializar
        protected void cmdOficializar_Click(object sender, EventArgs e)
        {
            //hacer una corrida de todas las validaciones
            //Camibar Bit a Cerrado  

            GuardarObs();

            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);

            string[] usr = User.Identity.Name.Split('|');

            string resValidaCuestionario = "";
            string resValidaAnexo = "";
            if (ddlMotivos.SelectedValue != "0")
                controlDP.Estatus = int.Parse(ddlMotivos.SelectedValue);
            else
            {
                CargarTodoYGuardar(controlDP);
                //if (ValidarTodasLasCalificaciones(controlDP) == 0)
                //{

                    resValidaCuestionario = ValidarCuestionario(controlDP);
                    resValidaAnexo = ValidarAnexo(controlDP);
                //}
                //else
                //{
                //    resValidaCuestionario = "Fallas  5 - 3";
                //}
            }
            if ((resValidaCuestionario == "" && resValidaAnexo == "" && ddlMotivos.SelectedValue == "0") || ddlMotivos.SelectedValue != "0")
            {
                if (ddlMotivos.SelectedValue == "0")
                    controlDP.Estatus = 10;

                wsEstadisiticas.Oficializar_Cuestionario(controlDP, int.Parse(usr[0]));

                EstadoBotones(controlDP);
            }
        }

        private void CargarTodoYGuardar(ControlDP controlDP)
        {
            Class911.CargaInicialCuestionario(controlDP,1);
        }

        private string ValidarCuestionario(ControlDP controlDP)
        {
            string salida = "";

            System.Text.StringBuilder cadena = new System.Text.StringBuilder();
            cadena.Append("NoVarialbe=Valor=ReadOnly|");
            for (int i = 1; i < controlDP.CuestionarioDP.TotalVariables; i++)
            {
                cadena.Append(i.ToString() + "=X=false|");
            }

            cadena.Append("!Ejecutar!" + controlDP.ID_Control.ToString());
            salida = Class911.RaiseCallbackEvent(cadena.ToString(), 1, HttpContext.Current);

            PintarFallas(salida, "VAR");
            return salida;
        }
        private string ValidarAnexo(ControlDP controlDP)
        {
            string res;
            //  |NoVariable = Valor

            string Datos = "NoVariable=Valor=ReadOnly|15=X=false|16=X=false|17=X=false|18=X=false|19=X=false|20=X=false|21=X=false|22=X=false|23=X=false|24=X=false|25=X=false|26=X=false|27=X=false|28=X=false|29=X=false|30=X=false|31=X=false|32=X=false|33=X=false|34=X=false|35=X=false|36=X=false|37=X=false|38=X=false|39=X=false|40=X=false|41=X=false|42=X=false|43=X=false|44=X=false|45=X=false|46=X=false|47=X=false|48=X=false|49=X=false|50=X=false|51=X=false|52=X=false|53=X=false|54=X=false|55=X=false|56=X=false|57=X=false|58=X=false|59=X=false|60=X=false|61=X=false|62=X=false|63=X=false|64=X=false|65=X=false|66=X=false|67=X=false|68=X=false|69=X=false|70=X=false|71=X=false|72=X=false|73=X=false|74=X=false|75=X=false|76=X=false|77=X=false|78=X=false|79=X=false|80=X=false|81=X=false|82=X=false|83=X=false|84=X=false|85=X=false|86=X=false|87=X=false|88=X=false|89=X=false|90=X=false|91=X=false|92=X=false|93=X=false|94=X=false|95=X=false|96=X=false|97=X=false|98=X=false|99=X=false|100=X=false|101=X=false|102=X=false|103=X=false|104=X=false|105=X=false|106=X=false|107=X=false|108=X=false|109=X=false|110=X=false|111=X=false|112=X=false|113=X=false|114=X=false|115=X=false|116=X=false|117=X=false|118=X=false|119=X=false|120=X=false|121=X=false|122=X=false|123=X=false|124=X=false|125=X=false|126=X=false|127=X=false|128=X=false|129=X=false|130=X=false|131=X=false|132=X=false|133=X=false|134=X=false|135=X=false|136=X=false|137=X=false|138=X=false|139=X=false|140=X=false|141=X=false|142=X=false|143=X=false|144=X=false|145=X=false|146=X=false|147=X=false|148=X=false|149=X=false|150=X=false|151=X=false|152=X=false|153=X=false|154=X=false|155=X=false|156=X=false|157=X=false|158=X=false|159=X=false|160=X=false|161=X=false|162=X=false|163=X=false|164=X=false|165=X=false|166=X=false|167=X=false|168=X=false|169=X=false|170=X=false|171=X=false|172=X=false|173=X=false|174=X=false|175=X=false|176=X=false|177=X=false|178=X=false|179=X=false|180=X=false|181=X=false|182=X=false|183=X=false|184=X=false|185=X=false|186=X=false|187=X=false|188=X=false|189=X=false|190=X=false|191=X=false|192=X=false|193=X=false|194=X=false|195=X=false|196=X=false|197=X=false|198=X=false|199=X=false|200=X=false|201=X=false|202=X=false|203=X=false|204=X=false|205=X=false|206=X=false|207=X=false|208=X=false|209=X=false|210=X=false|211=X=false|212=X=false|213=X=false|214=X=false|215=X=false|216=X=false|217=X=false|218=X=false|219=X=false|220=X=false|221=X=false|222=X=false|223=X=false|224=X=false|225=X=false|226=X=false|227=X=false|228=X=false|229=X=false|230=X=false|231=X=false|232=X=false|233=X=false|234=X=false|235=X=false|236=X=false|237=X=false|238=X=false|239=X=false|240=X=false|241=X=false|242=X=false|243=X=false|244=X=false|245=X=false|246=X=false|247=X=false|248=X=false|249=X=false|250=X=false|251=X=false|252=X=false|253=X=false|254=X=false|255=X=false|256=X=false|257=X=false|258=X=false|259=X=false|260=X=false|261=X=false|262=X=false|263=X=false|264=X=false|265=X=false|266=X=false|267=X=false|268=X=false|269=X=false|270=X=false|271=X=false|272=X=false|273=X=false|274=X=false|275=X=false|276=X=false|277=X=false|278=X=false|279=X=false|280=X=false|281=X=false|282=X=false|283=X=false|284=X=false|285=X=false|286=X=false|287=X=false|288=X=false|289=X=false|290=X=false|291=X=false|292=X=false|293=X=false|294=X=false|295=X=false|296=X=false|297=X=false|298=X=false|299=X=false|300=X=false|301=X=false|302=X=false|303=X=false|304=X=false|305=X=false|306=X=false|307=X=false|308=X=false|309=X=false|310=X=false|311=X=false|312=X=false|313=X=false|314=X=false|315=X=false|316=X=false|317=X=false|318=X=false|319=X=false|320=X=false|321=X=false|322=X=false|323=X=false|324=X=false|325=X=false|326=X=false|327=X=false|328=X=false|329=X=false|330=X=false|331=X=false|332=X=false|645=X=false|646=X=false|647=X=false|648=X=false|649=X=false|650=X=false|651=X=false|652=X=false|653=X=false|654=X=false|655=X=false|656=X=false|657=X=false|660=X=false|661=X=false|662=X=false|663=X=false|!Ejecutar!" + controlDP.ID_Control.ToString();
            res = Class911.RaiseCallbackEvent(Datos, 3, HttpContext.Current);

            //|266! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |276! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |277! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |312! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |313! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# # |314! EL TOTAL NO COINCIDE CON LA SUMA DE LOS CAMPOS# #
            PintarFallas(res, "VarAnex");

            return res;
        }
        private void PintarFallas(string datos, string variable)
        {
            //ConfigCapturaDP[] listaConfigCapturaDP = (ConfigCapturaDP[])HttpContext.Current.Session["listaConfigCaptura"];

            Class911 x = new Class911();
            string[] lista = datos.Split('|');
            foreach (string data in lista)
            {
                if (data != "")
                {
                    string[] ele = data.Split('!');
                    TableRow row = new TableRow();
                    TableCell c1 = new TableCell(); c1.CssClass = "BordesX";
                    TableCell c2 = new TableCell(); c2.CssClass = "BordesX";
                    TableCell c3 = new TableCell(); c3.CssClass = "BordesX";
                    ConfigCapturaDP conf = null;
                    if (variable == "VAR")
                    {
                        //conf = x.BuscarConfigCaptura(int.Parse(ele[0]), listaConfigCapturaDP);
                        c2.Text = "";// conf.Descripcion;
                    }
                    else
                    {
                        c2.Text = "";
                    }
                    c1.Text = variable + ele[0];


                    string fallas = "";
                    foreach (string falla in ele[1].Split('#'))
                    {
                        if (falla != "")
                            fallas = fallas + falla;
                    }
                    c3.Text = fallas;

                    row.Cells.Add(c1);
                    row.Cells.Add(c2);
                    row.Cells.Add(c3);
                    tblFallas.Rows.Add(row);
                    tblFallas.Visible = true;
                }
            }
        }
        private int ValidarTodasLasCalificaciones(ControlDP controlDP)
        {
            string res = wsEstadisiticas.ValidarTodasLasCalificaciones(controlDP);
            string Salida = "Faltan alumnos por capturar su calificaci�n";
            int intRes = 0;
            if (int.TryParse(res, out intRes))
            {
                if (intRes == 0)
                {
                    Salida = "";
                }
            }
            if (Salida.Length > 0)
            {
                TableRow row = new TableRow();
                TableCell c1 = new TableCell(); c1.CssClass = "BordesX";
                TableCell c2 = new TableCell(); c2.CssClass = "BordesX";
                TableCell c3 = new TableCell(); c3.CssClass = "BordesX";

                c1.Text = "Calificaciones";
                c2.Text = Salida;
                c3.Text = res;
                row.Cells.Add(c1);
                row.Cells.Add(c2);
                row.Cells.Add(c3);
                tblFallas.Rows.Add(row);
                tblFallas.Visible = true;
            }
            return Salida.Length;
        }

        #endregion

        protected void cmdGenerarComprobante_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarComprobante_PDF(int.Parse(hidIdCtrl.Value));
        }

        protected void cmdImprimirCuestionario_Lleno_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarCuestionario_PDF(int.Parse(hidIdCtrl.Value));
        }

        protected void cmdImprimircuestionario_Click(object sender, EventArgs e)
        {
            hidRuta.Value = Class911.GenerarCuestionario_Bacio_PDF(int.Parse(hidIdCtrl.Value));
        }

        protected void cmdGuardarObs_Click(object sender, EventArgs e)
        {
            GuardarObs();
        }

        private void GuardarObs()
        {
            ObservacionesDP observacionesDP = new ObservacionesDP();

            observacionesDP.ID_Control = int.Parse(hidIdCtrl.Value);
            observacionesDP.Observacion = txtObservaciones.Text;

            SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
            wsEstadisiticas.GuardarObservacion(Class911.GetControlSeleccionado(HttpContext.Current), observacionesDP);
        }


    }
}
