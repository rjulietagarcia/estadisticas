<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.2(Total)" AutoEventWireup="true" CodeBehind="AGT_911_2.aspx.cs" Inherits="EstadisticasEducativas._911.AGT_911_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 6;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PREESCOLAR</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_2',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_2',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_2',true)"><a href="#" title="" ><span>3�</span></a></li>
        <li onclick="openPage('AGT_911_2',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_2',false)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>  
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Revisar la cantidad de alumnos inscritos, existentes y promovidos por grado, edad y g�nero, y en caso de observar alguna inconsistencia en la informaci�n favor de realizar los ajustes pertinentes en el sistema de Control Escolar.</p>
    <p>La cantidad de alumnos promovidos se mostrar� cuando se haya concluido el ingreso de las calificaciones del quinto bimestre. Por lo tanto no deber� oficializar la estad�stica hasta que todas las calificaciones est�n en el sistema y la informaci�n en este cuestionario haya sido debidamente validada.</p>
    <p>Para calcular la edad de los alumnos se ha considerado a�os cumplidos al 8 de julio del a�o en curso, que es la fecha en que termina el ciclo escolar.</p>
    <p>En esta pantalla est�n disponibles los links para acceder al Control Escolar y realizar correcciones en los datos de los alumnos, si utiliza esta opci�n, una vez realizados los cambios, deber� regresar a la estad�stica y presionar clic en el bot�n de Actualizar Datos.</p>
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>     
 <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

            <table>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lbl1" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lbl3" runat="server" Text="3 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lbl4" runat="server" Text="4 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lbl5" runat="server" Text="5 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lbl6" runat="server" Text="6 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV139" runat="server" Columns="4" ReadOnly="True" CssClass="lblNegro" MaxLength="4" TabIndex="10101">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV140" runat="server" Columns="4" ReadOnly="True" TabIndex="10102" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV141" runat="server" Columns="4" ReadOnly="True" TabIndex="10103" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV142" runat="server" Columns="4" ReadOnly="True" TabIndex="10104" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV143" runat="server" Columns="4" ReadOnly="True" TabIndex="10105" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV144" runat="server" Columns="4" ReadOnly="True" TabIndex="10201" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV145" runat="server" Columns="4" ReadOnly="True" TabIndex="10202" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV146" runat="server" Columns="4" ReadOnly="True" TabIndex="10203" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV147" runat="server" Columns="4" ReadOnly="True" TabIndex="10204" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV148" runat="server" Columns="4" ReadOnly="True" TabIndex="10205" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblPromovidosH" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV149" runat="server" Columns="4" ReadOnly="True" TabIndex="10301" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>                        
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV150" runat="server" Columns="4" ReadOnly="True" TabIndex="10302" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV151" runat="server" Columns="4" ReadOnly="True" TabIndex="10303" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV152" runat="server" Columns="4" ReadOnly="True" TabIndex="10304" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV153" runat="server" Columns="4" ReadOnly="True" TabIndex="10305" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                 </tr>
            </table>
            
            <br />
        
            <table>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV154" runat="server" Columns="4" ReadOnly="True" TabIndex="10401" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV155" runat="server" Columns="4" ReadOnly="True" TabIndex="10402" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV156" runat="server" Columns="4" ReadOnly="True" TabIndex="10403" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV157" runat="server" Columns="4" ReadOnly="True" TabIndex="10404" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV158" runat="server" Columns="4" ReadOnly="True" TabIndex="10405" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV159" runat="server" Columns="4" ReadOnly="True" TabIndex="10501" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV160" runat="server" Columns="4" ReadOnly="True" TabIndex="10502" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV161" runat="server" Columns="4" ReadOnly="True" TabIndex="10503" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV162" runat="server" Columns="4" ReadOnly="True" TabIndex="10504" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV163" runat="server" Columns="4" ReadOnly="True" TabIndex="10505" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblPromovidosM" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV164" runat="server" Columns="4" ReadOnly="True" TabIndex="10601" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV165" runat="server" Columns="4" ReadOnly="True" TabIndex="10602" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV166" runat="server" Columns="4" ReadOnly="True" TabIndex="10603" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV167" runat="server" Columns="4" ReadOnly="True" TabIndex="10604" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV168" runat="server" Columns="4" ReadOnly="True" TabIndex="10605" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
            </table>
            
            <br />
        
            <table>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="lblSubtotal" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV169" runat="server" Columns="4" ReadOnly="True" TabIndex="10701" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV170" runat="server" Columns="4" ReadOnly="True" TabIndex="10702" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV171" runat="server" Columns="4" ReadOnly="True" TabIndex="10703" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV172" runat="server" Columns="4" ReadOnly="True" TabIndex="10704" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV173" runat="server" Columns="4" ReadOnly="True" TabIndex="10705" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV174" runat="server" Columns="4" ReadOnly="True" TabIndex="10801" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV175" runat="server" Columns="4" ReadOnly="True" TabIndex="10802" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV176" runat="server" Columns="4" ReadOnly="True" TabIndex="10803" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV177" runat="server" Columns="4" ReadOnly="True" TabIndex="10804" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV178" runat="server" Columns="4" ReadOnly="True" TabIndex="10805" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblPromovidosS" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV179" runat="server" Columns="4" ReadOnly="True" TabIndex="10901" CssClass="lblNegro" MaxLength="4">0</asp:TextBox>
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV180" runat="server" Columns="4" ReadOnly="True" TabIndex="10902" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV181" runat="server" Columns="4" ReadOnly="True" TabIndex="10903" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV182" runat="server" Columns="4" ReadOnly="True" TabIndex="10904" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV183" runat="server" Columns="4" ReadOnly="True" TabIndex="10905" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                </tr>
                <tr>
                    <td rowspan="1" style="width: 75px; height: 16px">
                    </td>
                    <td style="width: 132px; height: 16px">
                        &nbsp;</td>
                    <td style="width: 67px; height: 16px">
                    </td>
                    <td style="width: 67px; height: 16px">
                    </td>
                    <td style="width: 67px; height: 16px">
                    </td>
                    <td style="width: 67px; height: 16px">
                    </td>
                    <td style="width: 67px; height: 16px">
                    </td>
                </tr>
                <tr>
                    <td rowspan="1" style="width: 75px">
                    </td>
                    <td style="width: 132px; text-align: left">
                        <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                    <td style="width: 67px">
                    </td>
                    <td style="width: 67px">
                    </td>
                    <td style="width: 67px">
                    </td>
                    <td style="width: 67px">
                    </td>
                    <td style="width: 67px">
                        <asp:TextBox ID="txtV184" runat="server" Columns="4" ReadOnly="True" TabIndex="11101" CssClass="lblNegro" MaxLength="2">0</asp:TextBox></td>
                </tr>
            </table>
            
            <br /> 
                
        
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG3_911_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG3_911_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AGD_911_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 
            <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type = "hidden" runat = "server"/>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
               
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>
