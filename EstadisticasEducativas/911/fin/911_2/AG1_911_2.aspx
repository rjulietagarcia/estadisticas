<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AG1_911_2.aspx.cs" Inherits="EstadisticasEducativas._911.AG1_911_2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


 
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
   <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 6;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PREESCOLAR</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_2',true)"><a href="#" title="" class="activo"><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_2',false)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>3�</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    <div id="tooltipayuda" class="balloonstyle">
    <p>Revisar la cantidad de alumnos inscritos, existentes y promovidos por grado, edad y g�nero, y en caso de observar alguna inconsistencia en la informaci�n favor de realizar los ajustes pertinentes en el sistema de Control Escolar.</p>
    <p>La cantidad de alumnos promovidos se mostrar� cuando se haya concluido el ingreso de las calificaciones del quinto bimestre. Por lo tanto no deber� oficializar la estad�stica hasta que todas las calificaciones est�n en el sistema y la informaci�n en este cuestionario haya sido debidamente validada.</p> 
    <p>Para calcular la edad de los alumnos se ha considerado a�os cumplidos al 8 de julio del a�o en curso, que es la fecha en que termina el ciclo escolar.</p>
    <p>En esta pantalla est�n disponibles los links para acceder al Control Escolar y realizar correcciones en los datos de los alumnos, si utiliza esta opci�n, una vez realizados los cambios, deber� regresar a la estad�stica y presionar clic en el bot�n de Actualizar Datos.</p>
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>       
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
				     
                            <table style="text-align:center; font-weight: bold;"> 
                            <tr>
                                    <td style="width: 575px">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="lbl1" runat="server" Text="1�" CssClass="lblRojo"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:Label ID="lbl3" runat="server" Text="3 a�os" CssClass="lblRojo"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:Label ID="lbl4" runat="server" Text="4 a�os" CssClass="lblRojo"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:Label ID="lbl5" runat="server" Text="5 a�os" CssClass="lblRojo"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:Label ID="lbl6" runat="server" Text="6 a�os" CssClass="lblRojo"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="3" style="width: 75px">
                                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV1" runat="server" Columns="4" ReadOnly="True" MaxLength="4" TabIndex="10101" ToolTip="ToolTip Ejemplo"></asp:TextBox>
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV2" runat="server" Columns="4" ReadOnly="True" TabIndex="10102" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV3"  runat="server" Columns="4" ReadOnly="True" TabIndex="10103" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV4" runat="server" Columns="4" ReadOnly="True" TabIndex="10104" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV5" runat="server" Columns="4" ReadOnly="True" TabIndex="10105" MaxLength="4"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV6" runat="server" Columns="4" ReadOnly="True" TabIndex="10201" MaxLength="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV7" runat="server" Columns="4" ReadOnly="True" TabIndex="10202" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV8" runat="server" Columns="4" ReadOnly="True" TabIndex="10203" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV9" runat="server" Columns="4" ReadOnly="True" TabIndex="10204" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV10" runat="server" Columns="4" ReadOnly="True" TabIndex="10205" MaxLength="4"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblPromovidosH" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV11" runat="server" Columns="4" ReadOnly="True" TabIndex="10301" MaxLength="4"></asp:TextBox>
                                            </td>                        
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV12" runat="server" Columns="4" ReadOnly="True" TabIndex="10302" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV13" runat="server" Columns="4" ReadOnly="True" TabIndex="10303" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV14" runat="server" Columns="4" ReadOnly="True" TabIndex="10304" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV15" runat="server" Columns="4" ReadOnly="True" TabIndex="10305" MaxLength="4"></asp:TextBox></td>
                                         </tr>
                                    </table><br />
                                </td>
                            </tr>
                            <tr>
                                    <td style="width: 575px">
                                    <table>
                                        <tr>
                                            <td rowspan="3" style="width: 75px">
                                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV16" runat="server" Columns="4" ReadOnly="True" TabIndex="10401" MaxLength="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV17" runat="server" Columns="4" ReadOnly="True" TabIndex="10402" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV18" runat="server" Columns="4" ReadOnly="True" TabIndex="10403" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV19" runat="server" Columns="4" ReadOnly="True" TabIndex="10404" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV20" runat="server" Columns="4" ReadOnly="True" TabIndex="10405" MaxLength="4"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV21" runat="server" Columns="4" ReadOnly="True" TabIndex="10501" MaxLength="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV22" runat="server" Columns="4" ReadOnly="True" TabIndex="10502" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV23" runat="server" Columns="4" ReadOnly="True" TabIndex="10503" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV24" runat="server" Columns="4" ReadOnly="True" TabIndex="10504" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV25" runat="server" Columns="4" ReadOnly="True" TabIndex="10505" MaxLength="4"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblPromovidosM" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV26" runat="server" Columns="4" ReadOnly="True" TabIndex="10601" MaxLength="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV27" runat="server" Columns="4" ReadOnly="True" TabIndex="10602" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV28" runat="server" Columns="4" ReadOnly="True" TabIndex="10603" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV29" runat="server" Columns="4" ReadOnly="True" TabIndex="10604" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV30" runat="server" Columns="4" ReadOnly="True" TabIndex="10605" MaxLength="4"></asp:TextBox></td>
                                        </tr>
                                    </table><br />
                                </td>
                            </tr>
                            <tr>
                                    <td style="height: 102px; width: 575px;">
                                    <table>
                                        <tr>
                                            <td rowspan="3" style="width: 75px">
                                                <asp:Label ID="lblSubtotal" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV31" runat="server" Columns="4" ReadOnly="True" TabIndex="10701" MaxLength="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV32" runat="server" Columns="4" ReadOnly="True" TabIndex="10702" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV33" runat="server" Columns="4" ReadOnly="True" TabIndex="10703" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV34" runat="server" Columns="4" ReadOnly="True" TabIndex="10704" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV35" runat="server" Columns="4" ReadOnly="True" TabIndex="10705" MaxLength="4"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV36" runat="server" Columns="4" ReadOnly="True" TabIndex="10801" MaxLength="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV37" runat="server" Columns="4" ReadOnly="True" TabIndex="10802" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV38" runat="server" Columns="4" ReadOnly="True" TabIndex="10803" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV39" runat="server" Columns="4" ReadOnly="True" TabIndex="10804" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV40" runat="server" Columns="4" ReadOnly="True" TabIndex="10805" MaxLength="4"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblPromovidosS" runat="server" Text="PROMOVIDOS" CssClass="lblGrisTit"></asp:Label></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV41" runat="server" Columns="4" ReadOnly="True" TabIndex="10901" MaxLength="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV42" runat="server" Columns="4" ReadOnly="True" TabIndex="10902" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV43" runat="server" Columns="4" ReadOnly="True" TabIndex="10903" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV44" runat="server" Columns="4" ReadOnly="True" TabIndex="10904" MaxLength="4"></asp:TextBox></td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV45" runat="server" Columns="4" ReadOnly="True" TabIndex="10905" MaxLength="4"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1" style="width: 75px; height: 16px">
                                            </td>
                                            <td style="width: 132px; height: 16px">
                                                &nbsp;</td>
                                            <td style="width: 67px; height: 16px">
                                            </td>
                                            <td style="width: 67px; height: 16px">
                                            </td>
                                            <td style="width: 67px; height: 16px">
                                            </td>
                                            <td style="width: 67px; height: 16px">
                                            </td>
                                            <td style="width: 67px; height: 16px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1" style="width: 75px">
                                            </td>
                                            <td style="width: 132px; text-align: left">
                                                <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                                            <td style="width: 67px">
                                            </td>
                                            <td style="width: 67px">
                                            </td>
                                            <td style="width: 67px">
                                            </td>
                                            <td style="width: 67px">
                                            </td>
                                            <td style="width: 67px">
                                                <asp:TextBox ID="txtV46" runat="server" Columns="4" ReadOnly="True" TabIndex="20101" MaxLength="2"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                           
                       <hr />
                        <table align="center">
                            <tr>
                                <td style="height: 47px" ><span  onclick="openPage('Identificacion_911_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                                <td style="height: 47px" ><span  onclick="openPage('Identificacion_911_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                                <td style="width: 330px;">&nbsp;
                    </td>
                                <td style="height: 47px" ><span  onclick="openPage('AG2_911_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                                <td style="height: 47px" ><span  onclick="openPage('AG2_911_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                            </tr>
                        </table> 
                        
                       <div class="divResultado" id="divResultado"></div> 
            <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
        
        </center>
        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type = "hidden" runat = "server"/>
        
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                Disparador(<%=this.hidDisparador.Value %>); 
             
              
        </script>
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
     
 </asp:Content> 

