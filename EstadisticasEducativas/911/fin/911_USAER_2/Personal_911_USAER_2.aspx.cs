using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_USAER_2
{
    public partial class Personal_911_USAER_2 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV856.Attributes["onkeypress"] = "return Num(event)";
                txtV858.Attributes["onkeypress"] = "return Num(event)";
                txtV866.Attributes["onkeypress"] = "return Num(event)";
                txtV867.Attributes["onkeypress"] = "return Num(event)";
                txtV868.Attributes["onkeypress"] = "return Num(event)";
                txtV869.Attributes["onkeypress"] = "return Num(event)";
                txtV870.Attributes["onkeypress"] = "return Num(event)";
                txtV871.Attributes["onkeypress"] = "return Num(event)";
                txtV872.Attributes["onkeypress"] = "return Num(event)";
                txtV873.Attributes["onkeypress"] = "return Num(event)";
                txtV874.Attributes["onkeypress"] = "return Num(event)";
                txtV875.Attributes["onkeypress"] = "return Num(event)";
                txtV876.Attributes["onkeypress"] = "return Num(event)";
                txtV877.Attributes["onkeypress"] = "return Num(event)";
                txtV878.Attributes["onkeypress"] = "return Num(event)";
                txtV879.Attributes["onkeypress"] = "return Num(event)";
                txtV880.Attributes["onkeypress"] = "return Num(event)";
                txtV881.Attributes["onkeypress"] = "return Num(event)";
                txtV882.Attributes["onkeypress"] = "return Num(event)";
                txtV883.Attributes["onkeypress"] = "return Num(event)";
                txtV884.Attributes["onkeypress"] = "return Num(event)";
                txtV885.Attributes["onkeypress"] = "return Num(event)";
                txtV886.Attributes["onkeypress"] = "return Num(event)";
                txtV887.Attributes["onkeypress"] = "return Num(event)";
                txtV888.Attributes["onkeypress"] = "return Num(event)";
                txtV889.Attributes["onkeypress"] = "return Num(event)";
                txtV890.Attributes["onkeypress"] = "return Num(event)";
                txtV891.Attributes["onkeypress"] = "return Num(event)";
                txtV892.Attributes["onkeypress"] = "return Num(event)";
                txtV893.Attributes["onkeypress"] = "return Num(event)";
                txtV894.Attributes["onkeypress"] = "return Num(event)";
                txtV895.Attributes["onkeypress"] = "return Num(event)";
                txtV896.Attributes["onkeypress"] = "return Num(event)";
                txtV897.Attributes["onkeypress"] = "return Num(event)";
                txtV898.Attributes["onkeypress"] = "return Num(event)";
                txtV899.Attributes["onkeypress"] = "return Num(event)";
                txtV900.Attributes["onkeypress"] = "return Num(event)";
                txtV901.Attributes["onkeypress"] = "return Num(event)";
                txtV902.Attributes["onkeypress"] = "return Num(event)";
                txtV903.Attributes["onkeypress"] = "return Num(event)";
                txtV904.Attributes["onkeypress"] = "return Num(event)";
                txtV905.Attributes["onkeypress"] = "return Num(event)";
                txtV906.Attributes["onkeypress"] = "return Num(event)";
                txtV907.Attributes["onkeypress"] = "return Num(event)";
                txtV908.Attributes["onkeypress"] = "return Num(event)";
                txtV909.Attributes["onkeypress"] = "return Num(event)";
                txtV910.Attributes["onkeypress"] = "return Num(event)";
                txtV911.Attributes["onkeypress"] = "return Num(event)";
                txtV912.Attributes["onkeypress"] = "return Num(event)";
                txtV913.Attributes["onkeypress"] = "return Num(event)";
                txtV914.Attributes["onkeypress"] = "return Num(event)";
                txtV915.Attributes["onkeypress"] = "return Num(event)";
                txtV916.Attributes["onkeypress"] = "return Num(event)";
                txtV917.Attributes["onkeypress"] = "return Num(event)";
                txtV918.Attributes["onkeypress"] = "return Num(event)";
                txtV919.Attributes["onkeypress"] = "return Num(event)";
                txtV920.Attributes["onkeypress"] = "return Num(event)";
                txtV921.Attributes["onkeypress"] = "return Num(event)";
                txtV922.Attributes["onkeypress"] = "return Num(event)";
                txtV923.Attributes["onkeypress"] = "return Num(event)";
                txtV924.Attributes["onkeypress"] = "return Num(event)";
                txtV925.Attributes["onkeypress"] = "return Num(event)";
                txtV926.Attributes["onkeypress"] = "return Num(event)";
                txtV927.Attributes["onkeypress"] = "return Num(event)";
                txtV928.Attributes["onkeypress"] = "return Num(event)";
                txtV929.Attributes["onkeypress"] = "return Num(event)";
                txtV930.Attributes["onkeypress"] = "return Num(event)";
                txtV931.Attributes["onkeypress"] = "return Num(event)";
                txtV932.Attributes["onkeypress"] = "return Num(event)";
                txtV933.Attributes["onkeypress"] = "return Num(event)";
                txtV934.Attributes["onkeypress"] = "return Num(event)";
                txtV935.Attributes["onkeypress"] = "return Num(event)";
                txtV936.Attributes["onkeypress"] = "return Num(event)";
                txtV937.Attributes["onkeypress"] = "return Num(event)";
                txtV938.Attributes["onkeypress"] = "return Num(event)";
                txtV939.Attributes["onkeypress"] = "return Num(event)";
                txtV940.Attributes["onkeypress"] = "return Num(event)";
                txtV941.Attributes["onkeypress"] = "return Num(event)";
                txtV942.Attributes["onkeypress"] = "return Num(event)";
                txtV943.Attributes["onkeypress"] = "return Num(event)";
                txtV944.Attributes["onkeypress"] = "return Num(event)";
                txtV945.Attributes["onkeypress"] = "return Num(event)";
                txtV946.Attributes["onkeypress"] = "return Num(event)";
                txtV947.Attributes["onkeypress"] = "return Num(event)";
                txtV948.Attributes["onkeypress"] = "return Num(event)";
                txtV949.Attributes["onkeypress"] = "return Num(event)";
                txtV950.Attributes["onkeypress"] = "return Num(event)";
                txtV951.Attributes["onkeypress"] = "return Num(event)";
                txtV952.Attributes["onkeypress"] = "return Num(event)";
                txtV953.Attributes["onkeypress"] = "return Num(event)";
                txtV954.Attributes["onkeypress"] = "return Num(event)";
                txtV955.Attributes["onkeypress"] = "return Num(event)";
                txtV956.Attributes["onkeypress"] = "return Num(event)";
                txtV957.Attributes["onkeypress"] = "return Num(event)";
                txtV958.Attributes["onkeypress"] = "return Num(event)";
                txtV959.Attributes["onkeypress"] = "return Num(event)";
                txtV960.Attributes["onkeypress"] = "return Num(event)";
                txtV961.Attributes["onkeypress"] = "return Num(event)";
                txtV963.Attributes["onkeypress"] = "return Num(event)";
                txtV964.Attributes["onkeypress"] = "return Num(event)";
                txtV965.Attributes["onkeypress"] = "return Num(event)";
                txtV966.Attributes["onkeypress"] = "return Num(event)";
                txtV967.Attributes["onkeypress"] = "return Num(event)";
                txtV968.Attributes["onkeypress"] = "return Num(event)";
                txtV969.Attributes["onkeypress"] = "return Num(event)";
                txtV970.Attributes["onkeypress"] = "return Num(event)";
                txtV972.Attributes["onkeypress"] = "return Num(event)";
                txtV973.Attributes["onkeypress"] = "return Num(event)";
                txtV974.Attributes["onkeypress"] = "return Num(event)";
                txtV975.Attributes["onkeypress"] = "return Num(event)";
                txtV976.Attributes["onkeypress"] = "return Num(event)";
                txtV977.Attributes["onkeypress"] = "return Num(event)";
                txtV978.Attributes["onkeypress"] = "return Num(event)";
                txtV979.Attributes["onkeypress"] = "return Num(event)";
                txtV981.Attributes["onkeypress"] = "return Num(event)";
                txtV982.Attributes["onkeypress"] = "return Num(event)";
                txtV983.Attributes["onkeypress"] = "return Num(event)";
                txtV984.Attributes["onkeypress"] = "return Num(event)";
                txtV985.Attributes["onkeypress"] = "return Num(event)";
                txtV986.Attributes["onkeypress"] = "return Num(event)";
                txtV987.Attributes["onkeypress"] = "return Num(event)";
                txtV988.Attributes["onkeypress"] = "return Num(event)";
                txtV989.Attributes["onkeypress"] = "return Num(event)";
                txtV990.Attributes["onkeypress"] = "return Num(event)";
                txtV991.Attributes["onkeypress"] = "return Num(event)";
                txtV992.Attributes["onkeypress"] = "return Num(event)";
                txtV993.Attributes["onkeypress"] = "return Num(event)";
                txtV994.Attributes["onkeypress"] = "return Num(event)";
                txtV995.Attributes["onkeypress"] = "return Num(event)";
                txtV996.Attributes["onkeypress"] = "return Num(event)";
                txtV997.Attributes["onkeypress"] = "return Num(event)";
                txtV998.Attributes["onkeypress"] = "return Num(event)";
                txtV999.Attributes["onkeypress"] = "return Num(event)";
                txtV1000.Attributes["onkeypress"] = "return Num(event)";
                txtV1001.Attributes["onkeypress"] = "return Num(event)";
                txtV1002.Attributes["onkeypress"] = "return Num(event)";
                txtV1003.Attributes["onkeypress"] = "return Num(event)";
                txtV1004.Attributes["onkeypress"] = "return Num(event)";
                txtV1005.Attributes["onkeypress"] = "return Num(event)";
                txtV1006.Attributes["onkeypress"] = "return Num(event)";
                txtV1007.Attributes["onkeypress"] = "return Num(event)";
                txtV1008.Attributes["onkeypress"] = "return Num(event)";
                txtV1009.Attributes["onkeypress"] = "return Num(event)";
                txtV1010.Attributes["onkeypress"] = "return Num(event)";
                txtV1011.Attributes["onkeypress"] = "return Num(event)";
                txtV1012.Attributes["onkeypress"] = "return Num(event)";
                txtV1013.Attributes["onkeypress"] = "return Num(event)";
                txtV1014.Attributes["onkeypress"] = "return Num(event)";
                txtV1015.Attributes["onkeypress"] = "return Num(event)";
                txtV1016.Attributes["onkeypress"] = "return Num(event)";
                txtV1017.Attributes["onkeypress"] = "return Num(event)";
                txtV1018.Attributes["onkeypress"] = "return Num(event)";
                txtV1019.Attributes["onkeypress"] = "return Num(event)";
                txtV1020.Attributes["onkeypress"] = "return Num(event)";
                txtV1023.Attributes["onkeypress"] = "return Num(event)";
                txtV1024.Attributes["onkeypress"] = "return Num(event)";
                txtV1025.Attributes["onkeypress"] = "return Num(event)";
                txtV1026.Attributes["onkeypress"] = "return Num(event)";
                txtV1027.Attributes["onkeypress"] = "return Num(event)";
                txtV1028.Attributes["onkeypress"] = "return Num(event)";
                txtV1029.Attributes["onkeypress"] = "return Num(event)";
                txtV1030.Attributes["onkeypress"] = "return Num(event)";
                txtV1032.Attributes["onkeypress"] = "return Num(event)";
                txtV1033.Attributes["onkeypress"] = "return Num(event)";
                txtV1034.Attributes["onkeypress"] = "return Num(event)";
                txtV1035.Attributes["onkeypress"] = "return Num(event)";
                txtV1036.Attributes["onkeypress"] = "return Num(event)";
                txtV1037.Attributes["onkeypress"] = "return Num(event)";
                txtV1038.Attributes["onkeypress"] = "return Num(event)";
                txtV1039.Attributes["onkeypress"] = "return Num(event)";
                txtV1041.Attributes["onkeypress"] = "return Num(event)";
                txtV1042.Attributes["onkeypress"] = "return Num(event)";
                txtV1043.Attributes["onkeypress"] = "return Num(event)";
                txtV1044.Attributes["onkeypress"] = "return Num(event)";
                txtV1045.Attributes["onkeypress"] = "return Num(event)";
                txtV1046.Attributes["onkeypress"] = "return Num(event)";
                txtV1047.Attributes["onkeypress"] = "return Num(event)";
                txtV1048.Attributes["onkeypress"] = "return Num(event)";
                txtV1049.Attributes["onkeypress"] = "return Num(event)";
                txtV1050.Attributes["onkeypress"] = "return Num(event)";
                txtV1051.Attributes["onkeypress"] = "return Num(event)";
                txtV1052.Attributes["onkeypress"] = "return Num(event)";
                txtV1053.Attributes["onkeypress"] = "return Num(event)";
                txtV1054.Attributes["onkeypress"] = "return Num(event)";
                txtV1055.Attributes["onkeypress"] = "return Num(event)";
                txtV1056.Attributes["onkeypress"] = "return Num(event)";
                txtV1074.Attributes["onkeypress"] = "return Num(event)";
                txtV1075.Attributes["onkeypress"] = "return Num(event)";
                txtV1076.Attributes["onkeypress"] = "return Num(event)";
                txtV1077.Attributes["onkeypress"] = "return Num(event)";
                txtV1078.Attributes["onkeypress"] = "return Num(event)";
                txtV1079.Attributes["onkeypress"] = "return Num(event)";
                txtV1080.Attributes["onkeypress"] = "return Num(event)";
                txtV1061.Attributes["onkeypress"] = "return Num(event)";
                txtV1062.Attributes["onkeypress"] = "return Num(event)";
                txtV1063.Attributes["onkeypress"] = "return Num(event)";
                txtV1064.Attributes["onkeypress"] = "return Num(event)";
                txtV1065.Attributes["onkeypress"] = "return Num(event)";
                txtV1066.Attributes["onkeypress"] = "return Num(event)";
                txtV1067.Attributes["onkeypress"] = "return Num(event)";
                txtV1068.Attributes["onkeypress"] = "return Num(event)";
                txtV1069.Attributes["onkeypress"] = "return Num(event)";
                #endregion


                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;

                optV854.Attributes.Add("onclick", "OPTs3Txt()");
                optV855.Attributes.Add("onclick", "OPTs3Txt()");
                optV860.Attributes.Add("onclick", "OPTs2Txt()");
                optV861.Attributes.Add("onclick", "OPTs2Txt()");
                optV862.Attributes.Add("onclick", "OPTs2Txt()");
                optV863.Attributes.Add("onclick", "OPTs2Txt()");
                optV864.Attributes.Add("onclick", "OPTs2Txt()");
                optV865.Attributes.Add("onclick", "OPTs2Txt()");

                LlenarCBOs();
                setDropdown();
                cboV857.Attributes.Add("onchange", "cboChangeV857(this);");
                cboV859.Attributes.Add("onchange", "cboChangeV859(this);");
                
            }
        }
        private void LlenarCBOs()
        {
            cboV857.Items.Add(new ListItem("", "0"));
            cboV857.Items.Add(new ListItem("PRIMARIA", "1"));
            cboV857.Items.Add(new ListItem("SECUNDARIA", "2"));
            cboV857.Items.Add(new ListItem("PROFESIONAL MEDIO", "3"));
            cboV857.Items.Add(new ListItem("BACHILLERATO", "4"));
            cboV857.Items.Add(new ListItem("NORMAL DE PREESCOLAR", "5"));
            cboV857.Items.Add(new ListItem("NORMAL DE PRIMARIA", "6"));
            cboV857.Items.Add(new ListItem("NORMAL SUPERIOR", "7"));
            cboV857.Items.Add(new ListItem("LICENCIATURA", "8"));
            cboV857.Items.Add(new ListItem("MAESTRIA", "9"));
            cboV857.Items.Add(new ListItem("DOCTORADO", "10"));

            cboV859.Items.Add(new ListItem("", "0"));
            cboV859.Items.Add(new ListItem("PSICÓLOGO","1"));
            cboV859.Items.Add(new ListItem("PEDAGOGO","2"));
            cboV859.Items.Add(new ListItem("DEFICIENCIA MENTAL", "3"));
            cboV859.Items.Add(new ListItem("AUDICIÓN Y LENGUAJE", "4"));
            cboV859.Items.Add(new ListItem("PROBLEMAS DE APRENDIZAJE", "5"));
            cboV859.Items.Add(new ListItem("INADAPTADO SOCIAL", "6"));
            cboV859.Items.Add(new ListItem("MAESTRO DE PRIMARIA", "7"));
            cboV859.Items.Add(new ListItem("APARATO LOCOMOTOR", "8"));
            cboV859.Items.Add(new ListItem("DEFICIENCIA VISUAL", "9"));
            cboV859.Items.Add(new ListItem("COMUNICACIÓN HUMANA", "10"));
            cboV859.Items.Add(new ListItem("NORMAL SUPERIOR", "11"));
            cboV859.Items.Add(new ListItem("SEXOLOGÍA", "12"));
            cboV859.Items.Add(new ListItem("ADMINISTRACIÓN EDUCATIVA", "13"));
            cboV859.Items.Add(new ListItem("ODONTOLOGÍA", "14"));
            cboV859.Items.Add(new ListItem("AUTISMO", "15"));
            cboV859.Items.Add(new ListItem("EDUCACIÓN ESPECIAL", "16"));
            cboV859.Items.Add(new ListItem("INNOVACIÓNES EDUCATIVAS", "17"));
            cboV859.Items.Add(new ListItem("INTERVENCIÓN TEMPRENA", "18"));
            cboV859.Items.Add(new ListItem("MATEMÁTICAS", "19"));
            cboV859.Items.Add(new ListItem("SOCIOLOGÍA", "20"));
            cboV859.Items.Add(new ListItem("INGENIERO MECÁNICO", "21"));
            cboV859.Items.Add(new ListItem("ENSEÑANZA SUPERIOR", "22"));
            cboV859.Items.Add(new ListItem("EDUCACIÓN", "23"));
        }

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        public void setDropdown()
        {
            string Valor1 = this.txtV856.Text;
            this.cboV857.SelectedValue = Valor1;
            this.txtV857.Text = this.cboV857.Text;


            string Valor2 = this.txtV858.Text;
            this.cboV859.SelectedValue = Valor2;
            this.txtV859.Text = this.cboV859.Text;

        
        }

    }
}
