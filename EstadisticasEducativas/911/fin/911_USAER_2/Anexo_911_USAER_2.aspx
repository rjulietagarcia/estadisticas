<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="CUESTIONARIO DE INTEGRACI�N EDUCATIVA" AutoEventWireup="true" CodeBehind="Anexo_911_USAER_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_USAER_2.Anexo_911_USAER_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script>
    <script language="javascript" type="text/javascript" src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
    
        <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 25;
        MaxRow = 23;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%> 

     <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="Label45" runat="server" Text="FIN"></asp:Label>&nbsp;<asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_2', true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_2', true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_2', true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_2', true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_2', true)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_2', true)"><a href="#" title=""><span>SECUNDARIA</span></a></li>
        <li onclick="openPage('AGD_911_USAER_2', true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_USAER_2', true)"><a href="#" title=""><span>PERSONAL Y RECURSOS</span></a></li>
        <li onclick="openPage('Inmueble_911_USAER_2', true)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="openPage('Anexo_911_USAER_2', true)"><a href="#" title="" class="activo"><span>ANEXO</span></a></li>
        <li onclick="openPage('Oficializar_911_USAER_2', false)"><a href="#" title=""><span>OFICIALIZACI�N</span></a></li></ul>
    </div>

   
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
       <center>
       
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td>
                    <asp:Label ID="Label36" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="IMPORTANTE:" ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label37" runat="server" CssClass="lblGris" Font-Bold="True" Text="El objetivo del presente cuestionario es recuperar informaci�n acerca de la poblaci�n estudiantil que presenta Necesidades Educativas Especiales y la condici�n con la que se asocian, inscritos a las escuelas de Educaci�n Inicial, B�sica, Media Superior y Normal del Sistema Educativo Nacional que permita realizar acciones para elevar la calidad de la respuesta educativa que se ofrece a estas alumnas y alumnos."></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label38" runat="server" CssClass="lblGris" Font-Bold="True" Text="Para responder este cuestionario tenga presente las recomendaciones que a continuaci�n se mencionan, de acuerdo con el nivel educativo al que pertenecen."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label39" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Inicial: Completar la columna de Lactantes y Maternales. Cuando ofrezca Educaci�n Preescolar, completar 1�, 2� y 3� grado y el total. Este nivel no contestar� el apartado 6."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label40" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Preescolar: Completar solamente 1�, 2�, 3� grado y el total. Este nivel no contestar� el apartado 6."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label41" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Primaria: Completar solamente 1�, 2�, 3�, 4�, 5�, 6� grado y el total."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label42" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Secundaria: Completar solamente 1�, 2�, 3� grado y el total."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label43" runat="server" CssClass="lblGris" Font-Bold="True" Text="Educaci�n Media Superior y Normal: Completar por ciclo escolar cursado, 1� incluye primer y segundo semestre; 2� incluye tercer y cuarto semestre; 3� incluye quinto y sexto semestre; 4� incluye s�ptimo y octavo semestre; y el total. Estos niveles no contestar�n las preguntas 4 y 5."
                        ></asp:Label></td>
            </tr>
        </table>
        <br />
        <br />
        <table>
            <tr>
                <td colspan="25" style="text-align: justify">
                    <asp:Label ID="Label35" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="II. Alumnos con Necesidades Educativas Especiales"></asp:Label></td>
                <td colspan="1" style="text-align: justify">
                </td>
            </tr>
            <tr>
                <td colspan="16" style="text-align: justify">
                    <asp:Label ID="Label34" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. De la existencia total, escriba el n�mero de alumnos con Necesidades Educativas Especiales:" Width="444px"></asp:Label>
                    <asp:TextBox ID="txtVANX660" runat="server" Columns="4" MaxLength="4" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                <td colspan="9" style="text-align: left">
                    </td>
                <td colspan="1" style="text-align: left">
                </td>
            </tr>
            <tr>
                <td colspan="25" rowspan="1" style="text-align: left">
                    <asp:Label ID="Label33" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. De los alumnos reportados en el punto anterior, desglose de acuerdo con el grado y sexo, seg�n la discapacidad o condici�n que presentan. Para ser preciso en la informaci�n se recomienda revisar el glosario." Width="872px"></asp:Label></td>
                <td colspan="1" rowspan="1" style="text-align: left">
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblCondicion" runat="server" CssClass="lblNegro" Font-Bold="True"
                        Text="Condici�n o discapacidad con la que se asocian las NEE de los alumnos"
                        ></asp:Label></td>
                <td colspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblInicial" runat="server" CssClass="lblNegro" Font-Bold="True" Text="Lactantes y Maternales"
                        ></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="1�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="2�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="3�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="4�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl5o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="5�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl6o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="6�"></asp:Label></td>
                <td colspan="3" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTotal1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="Total"></asp:Label></td>
                <td class="Orila" colspan="1" style="text-align: center">
                </td>
            </tr>
            <tr>
                <td style="text-align: center;" class="linaBajo">
                    <asp:Label ID="lblInicialH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:Label ID="lblInicialM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:Label ID="lblInicialT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl1oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl2oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl3oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center; width: 42px;" class="linaBajo">
                    <asp:Label ID="lbl3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl3oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl4oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl4oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl4oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl5oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl5oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl5oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl6oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl6oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lbl6oT" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblTotal1H" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="H"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblTotal1M" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblTotal1T" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="T"
                        ></asp:Label></td>
                <td class="Orila" style="text-align: center">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="Ceguera" ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX15" runat="server" Columns="2" MaxLength="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX16" runat="server" Columns="2" MaxLength="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX17" runat="server" Columns="3" MaxLength="3" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX18" runat="server" Columns="2" MaxLength="2" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX19" runat="server" Columns="2" MaxLength="2" TabIndex="10305" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX20" runat="server" Columns="3" MaxLength="3" TabIndex="10306" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX21" runat="server" Columns="2" MaxLength="2" TabIndex="10307" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX22" runat="server" Columns="2" MaxLength="2" TabIndex="10308" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX23" runat="server" Columns="3" MaxLength="3" TabIndex="10309" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX24" runat="server" Columns="2" MaxLength="2" TabIndex="10310" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX25" runat="server" Columns="2" MaxLength="2" TabIndex="10311" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX26" runat="server" Columns="3" MaxLength="3" TabIndex="10312" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX27" runat="server" Columns="2" MaxLength="2" TabIndex="10313" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX28" runat="server" Columns="2" MaxLength="2" TabIndex="10314" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX29" runat="server" Columns="3" MaxLength="3" TabIndex="10315" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX30" runat="server" Columns="2" MaxLength="2" TabIndex="10316" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX31" runat="server" Columns="2" MaxLength="2" TabIndex="10317" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX32" runat="server" Columns="3" MaxLength="3" TabIndex="10318" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX33" runat="server" Columns="2" MaxLength="2" TabIndex="10319" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX34" runat="server" Columns="2" MaxLength="2" TabIndex="10320" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX35" runat="server" Columns="3" MaxLength="3" TabIndex="10321" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX36" runat="server" Columns="2" MaxLength="2" TabIndex="10322" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX37" runat="server" Columns="2" MaxLength="2" TabIndex="10323" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX38" runat="server" Columns="3" MaxLength="3" TabIndex="10324" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblVision" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Baja visi�n"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX39" runat="server" Columns="2" MaxLength="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX40" runat="server" Columns="2" MaxLength="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX41" runat="server" Columns="3" MaxLength="3" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX42" runat="server" Columns="2" MaxLength="2" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX43" runat="server" Columns="2" MaxLength="2" TabIndex="10405" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX44" runat="server" Columns="3" MaxLength="3" TabIndex="10406" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX45" runat="server" Columns="2" MaxLength="2" TabIndex="10407" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX46" runat="server" Columns="2" MaxLength="2" TabIndex="10408" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX47" runat="server" Columns="3" MaxLength="3" TabIndex="10409" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX48" runat="server" Columns="2" MaxLength="2" TabIndex="10410" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX49" runat="server" Columns="2" MaxLength="2" TabIndex="10411" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX50" runat="server" Columns="3" MaxLength="3" TabIndex="10412" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX51" runat="server" Columns="2" MaxLength="2" TabIndex="10413" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX52" runat="server" Columns="2" MaxLength="2" TabIndex="10414" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX53" runat="server" Columns="3" MaxLength="3" TabIndex="10415" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX54" runat="server" Columns="2" MaxLength="2" TabIndex="10416" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX55" runat="server" Columns="2" MaxLength="2" TabIndex="10417" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX56" runat="server" Columns="3" MaxLength="3" TabIndex="10418" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX57" runat="server" Columns="2" MaxLength="2" TabIndex="10419" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX58" runat="server" Columns="2" MaxLength="2" TabIndex="10420" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX59" runat="server" Columns="3" MaxLength="3" TabIndex="10421" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX60" runat="server" Columns="2" MaxLength="2" TabIndex="10422" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX61" runat="server" Columns="2" MaxLength="2" TabIndex="10423" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX62" runat="server" Columns="3" MaxLength="3" TabIndex="10424" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Sordera"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX63" runat="server" Columns="2" MaxLength="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX64" runat="server" Columns="2" MaxLength="2" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX65" runat="server" Columns="3" MaxLength="3" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX66" runat="server" Columns="2" MaxLength="2" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX67" runat="server" Columns="2" MaxLength="2" TabIndex="10505" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX68" runat="server" Columns="3" MaxLength="3" TabIndex="10506" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX69" runat="server" Columns="2" MaxLength="2" TabIndex="10507" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX70" runat="server" Columns="2" MaxLength="2" TabIndex="10508" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX71" runat="server" Columns="3" MaxLength="3" TabIndex="10509" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX72" runat="server" Columns="2" MaxLength="2" TabIndex="10510" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX73" runat="server" Columns="2" MaxLength="2" TabIndex="10511" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX74" runat="server" Columns="3" MaxLength="3" TabIndex="10512" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX75" runat="server" Columns="2" MaxLength="2" TabIndex="10513" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX76" runat="server" Columns="2" MaxLength="2" TabIndex="10514" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX77" runat="server" Columns="3" MaxLength="3" TabIndex="10515" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX78" runat="server" Columns="2" MaxLength="2" TabIndex="10516" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX79" runat="server" Columns="2" MaxLength="2" TabIndex="10517" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX80" runat="server" Columns="3" MaxLength="3" TabIndex="10518" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX81" runat="server" Columns="2" MaxLength="2" TabIndex="10519" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX82" runat="server" Columns="2" MaxLength="2" TabIndex="10520" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX83" runat="server" Columns="3" MaxLength="3" TabIndex="10521" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX84" runat="server" Columns="2" MaxLength="2" TabIndex="10522" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX85" runat="server" Columns="2" MaxLength="2" TabIndex="10523" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX86" runat="server" Columns="3" MaxLength="3" TabIndex="10524" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblHipoacusia" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hipoacusia"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX87" runat="server" Columns="2" MaxLength="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX88" runat="server" Columns="2" MaxLength="2" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX89" runat="server" Columns="3" MaxLength="3" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX90" runat="server" Columns="2" MaxLength="2" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX91" runat="server" Columns="2" MaxLength="2" TabIndex="10605" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX92" runat="server" Columns="3" MaxLength="3" TabIndex="10606" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX93" runat="server" Columns="2" MaxLength="2" TabIndex="10607" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX94" runat="server" Columns="2" MaxLength="2" TabIndex="10608" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX95" runat="server" Columns="3" MaxLength="3" TabIndex="10609" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX96" runat="server" Columns="2" MaxLength="2" TabIndex="10610" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX97" runat="server" Columns="2" MaxLength="2" TabIndex="10611" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX98" runat="server" Columns="3" MaxLength="3" TabIndex="10612" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX99" runat="server" Columns="2" MaxLength="2" TabIndex="10613" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX100" runat="server" Columns="2" MaxLength="2" TabIndex="10614" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX101" runat="server" Columns="3" MaxLength="3" TabIndex="10615" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX102" runat="server" Columns="2" MaxLength="2" TabIndex="10616" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX103" runat="server" Columns="2" MaxLength="2" TabIndex="10617" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX104" runat="server" Columns="3" MaxLength="3" TabIndex="10618" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX105" runat="server" Columns="2" MaxLength="2" TabIndex="10619" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX106" runat="server" Columns="2" MaxLength="2" TabIndex="10620" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX107" runat="server" Columns="3" MaxLength="3" TabIndex="10621" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX108" runat="server" Columns="2" MaxLength="2" TabIndex="10622" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX109" runat="server" Columns="2" MaxLength="2" TabIndex="10623" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX110" runat="server" Columns="3" MaxLength="3" TabIndex="10624" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMotriz" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Discapacidad motriz"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX111" runat="server" Columns="2" MaxLength="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX112" runat="server" Columns="2" MaxLength="2" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX113" runat="server" Columns="3" MaxLength="3" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX114" runat="server" Columns="2" MaxLength="2" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX115" runat="server" Columns="2" MaxLength="2" TabIndex="10705" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX116" runat="server" Columns="3" MaxLength="3" TabIndex="10706" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX117" runat="server" Columns="2" MaxLength="2" TabIndex="10707" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX118" runat="server" Columns="2" MaxLength="2" TabIndex="10708" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX119" runat="server" Columns="3" MaxLength="3" TabIndex="10709" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX120" runat="server" Columns="2" MaxLength="2" TabIndex="10710" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX121" runat="server" Columns="2" MaxLength="2" TabIndex="10711" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX122" runat="server" Columns="3" MaxLength="3" TabIndex="10712" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX123" runat="server" Columns="2" MaxLength="2" TabIndex="10713" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX124" runat="server" Columns="2" MaxLength="2" TabIndex="10714" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX125" runat="server" Columns="3" MaxLength="3" TabIndex="10715" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX126" runat="server" Columns="2" MaxLength="2" TabIndex="10716" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX127" runat="server" Columns="2" MaxLength="2" TabIndex="10717" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX128" runat="server" Columns="3" MaxLength="3" TabIndex="10718" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX129" runat="server" Columns="2" MaxLength="2" TabIndex="10719" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX130" runat="server" Columns="2" MaxLength="2" TabIndex="10720" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX131" runat="server" Columns="3" MaxLength="3" TabIndex="10721" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX132" runat="server" Columns="2" MaxLength="2" TabIndex="10722" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX133" runat="server" Columns="2" MaxLength="2" TabIndex="10723" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX134" runat="server" Columns="3" MaxLength="3" TabIndex="10724" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblIntelectual" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Discapacidad Intelectual"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX135" runat="server" Columns="2" MaxLength="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX136" runat="server" Columns="2" MaxLength="2" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX137" runat="server" Columns="3" MaxLength="3" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX138" runat="server" Columns="2" MaxLength="2" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX139" runat="server" Columns="2" MaxLength="2" TabIndex="10805" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX140" runat="server" Columns="3" MaxLength="3" TabIndex="10806" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX141" runat="server" Columns="2" MaxLength="2" TabIndex="10807" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX142" runat="server" Columns="2" MaxLength="2" TabIndex="10808" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX143" runat="server" Columns="3" MaxLength="3" TabIndex="10809" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX144" runat="server" Columns="2" MaxLength="2" TabIndex="10810" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX145" runat="server" Columns="2" MaxLength="2" TabIndex="10811" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX146" runat="server" Columns="3" MaxLength="3" TabIndex="10812" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX147" runat="server" Columns="2" MaxLength="2" TabIndex="10813" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX148" runat="server" Columns="2" MaxLength="2" TabIndex="10814" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX149" runat="server" Columns="3" MaxLength="3" TabIndex="10815" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX150" runat="server" Columns="2" MaxLength="2" TabIndex="10816" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX151" runat="server" Columns="2" MaxLength="2" TabIndex="10817" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX152" runat="server" Columns="3" MaxLength="3" TabIndex="10818" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX153" runat="server" Columns="2" MaxLength="2" TabIndex="10819" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX154" runat="server" Columns="2" MaxLength="2" TabIndex="10820" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX155" runat="server" Columns="3" MaxLength="3" TabIndex="10821" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX156" runat="server" Columns="2" MaxLength="2" TabIndex="10822" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX157" runat="server" Columns="2" MaxLength="2" TabIndex="10823" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX158" runat="server" Columns="3" MaxLength="3" TabIndex="10824" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMultiple" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Discapacidad M�ltiple"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX159" runat="server" Columns="2" MaxLength="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX160" runat="server" Columns="2" MaxLength="2" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX161" runat="server" Columns="3" MaxLength="3" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX162" runat="server" Columns="2" MaxLength="2" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX163" runat="server" Columns="2" MaxLength="2" TabIndex="10905" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX164" runat="server" Columns="3" MaxLength="3" TabIndex="10906" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX165" runat="server" Columns="2" MaxLength="2" TabIndex="10907" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX166" runat="server" Columns="2" MaxLength="2" TabIndex="10908" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX167" runat="server" Columns="3" MaxLength="3" TabIndex="10909" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX168" runat="server" Columns="2" MaxLength="2" TabIndex="10910" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX169" runat="server" Columns="2" MaxLength="2" TabIndex="10911" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX170" runat="server" Columns="3" MaxLength="3" TabIndex="10912" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX171" runat="server" Columns="2" MaxLength="2" TabIndex="10913" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX172" runat="server" Columns="2" MaxLength="2" TabIndex="10914" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX173" runat="server" Columns="3" MaxLength="3" TabIndex="10915" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX174" runat="server" Columns="2" MaxLength="2" TabIndex="10916" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX175" runat="server" Columns="2" MaxLength="2" TabIndex="10917" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX176" runat="server" Columns="3" MaxLength="3" TabIndex="10918" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX177" runat="server" Columns="2" MaxLength="2" TabIndex="10919" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX178" runat="server" Columns="2" MaxLength="2" TabIndex="10920" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX179" runat="server" Columns="3" MaxLength="3" TabIndex="10921" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX180" runat="server" Columns="2" MaxLength="2" TabIndex="10922" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX181" runat="server" Columns="2" MaxLength="2" TabIndex="10923" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX182" runat="server" Columns="3" MaxLength="3" TabIndex="10924" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblAutismo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Autismo"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX183" runat="server" Columns="2" MaxLength="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX184" runat="server" Columns="2" MaxLength="2" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX185" runat="server" Columns="3" MaxLength="3" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX186" runat="server" Columns="2" MaxLength="2" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX187" runat="server" Columns="2" MaxLength="2" TabIndex="11005" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX188" runat="server" Columns="3" MaxLength="3" TabIndex="11006" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX189" runat="server" Columns="2" MaxLength="2" TabIndex="11007" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX190" runat="server" Columns="2" MaxLength="2" TabIndex="11008" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX191" runat="server" Columns="3" MaxLength="3" TabIndex="11009" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX192" runat="server" Columns="2" MaxLength="2" TabIndex="11010" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX193" runat="server" Columns="2" MaxLength="2" TabIndex="11011" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX194" runat="server" Columns="3" MaxLength="3" TabIndex="11012" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX195" runat="server" Columns="2" MaxLength="2" TabIndex="11013" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX196" runat="server" Columns="2" MaxLength="2" TabIndex="11014" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX197" runat="server" Columns="3" MaxLength="3" TabIndex="11015" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX198" runat="server" Columns="2" MaxLength="2" TabIndex="11016" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX199" runat="server" Columns="2" MaxLength="2" TabIndex="11017" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX200" runat="server" Columns="3" MaxLength="3" TabIndex="11018" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX201" runat="server" Columns="2" MaxLength="2" TabIndex="11019" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX202" runat="server" Columns="2" MaxLength="2" TabIndex="11020" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX203" runat="server" Columns="3" MaxLength="3" TabIndex="11021" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX204" runat="server" Columns="2" MaxLength="2" TabIndex="11022" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX205" runat="server" Columns="2" MaxLength="2" TabIndex="11023" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX206" runat="server" Columns="3" MaxLength="3" TabIndex="11024" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblSobresalientes" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Aptitudes sobresalientes"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX207" runat="server" Columns="2" MaxLength="2" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX208" runat="server" Columns="2" MaxLength="2" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX209" runat="server" Columns="3" MaxLength="3" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX210" runat="server" Columns="2" MaxLength="2" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX211" runat="server" Columns="2" MaxLength="2" TabIndex="11105" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX212" runat="server" Columns="3" MaxLength="3" TabIndex="11106" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX213" runat="server" Columns="2" MaxLength="2" TabIndex="11107" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX214" runat="server" Columns="2" MaxLength="2" TabIndex="11108" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX215" runat="server" Columns="3" MaxLength="3" TabIndex="11109" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX216" runat="server" Columns="2" MaxLength="2" TabIndex="11110" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX217" runat="server" Columns="2" MaxLength="2" TabIndex="11111" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX218" runat="server" Columns="3" MaxLength="3" TabIndex="11112" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX219" runat="server" Columns="2" MaxLength="2" TabIndex="11113" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX220" runat="server" Columns="2" MaxLength="2" TabIndex="11114" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX221" runat="server" Columns="3" MaxLength="3" TabIndex="11115" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX222" runat="server" Columns="2" MaxLength="2" TabIndex="11116" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX223" runat="server" Columns="2" MaxLength="2" TabIndex="11117" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX224" runat="server" Columns="3" MaxLength="3" TabIndex="11118" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX225" runat="server" Columns="2" MaxLength="2" TabIndex="11119" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX226" runat="server" Columns="2" MaxLength="2" TabIndex="11120" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX227" runat="server" Columns="3" MaxLength="3" TabIndex="11121" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX228" runat="server" Columns="2" MaxLength="2" TabIndex="11122" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX229" runat="server" Columns="2" MaxLength="2" TabIndex="11123" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX230" runat="server" Columns="3" MaxLength="3" TabIndex="11124" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblComunicacion" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Problemas de comunicaci�n"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX231" runat="server" Columns="2" MaxLength="2" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX232" runat="server" Columns="2" MaxLength="2" TabIndex="11202" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX233" runat="server" Columns="3" MaxLength="3" TabIndex="11203" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX234" runat="server" Columns="2" MaxLength="2" TabIndex="11204" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX235" runat="server" Columns="2" MaxLength="2" TabIndex="11205" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX236" runat="server" Columns="3" MaxLength="3" TabIndex="11206" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX237" runat="server" Columns="2" MaxLength="2" TabIndex="11207" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX238" runat="server" Columns="2" MaxLength="2" TabIndex="11208" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX239" runat="server" Columns="3" MaxLength="3" TabIndex="11209" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX240" runat="server" Columns="2" MaxLength="2" TabIndex="11210" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX241" runat="server" Columns="2" MaxLength="2" TabIndex="11211" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX242" runat="server" Columns="3" MaxLength="3" TabIndex="11212" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX243" runat="server" Columns="2" MaxLength="2" TabIndex="11213" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX244" runat="server" Columns="2" MaxLength="2" TabIndex="11214" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX245" runat="server" Columns="3" MaxLength="3" TabIndex="11215" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX246" runat="server" Columns="2" MaxLength="2" TabIndex="11216" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX247" runat="server" Columns="2" MaxLength="2" TabIndex="11217" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX248" runat="server" Columns="3" MaxLength="3" TabIndex="11218" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX249" runat="server" Columns="2" MaxLength="2" TabIndex="11219" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX250" runat="server" Columns="2" MaxLength="2" TabIndex="11220" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX251" runat="server" Columns="3" MaxLength="3" TabIndex="11221" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX252" runat="server" Columns="2" MaxLength="2" TabIndex="11222" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX253" runat="server" Columns="2" MaxLength="2" TabIndex="11223" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX254" runat="server" Columns="3" MaxLength="3" TabIndex="11224" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="Conducta" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Problemas de conducta"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX255" runat="server" Columns="2" MaxLength="2" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX256" runat="server" Columns="2" MaxLength="2" TabIndex="11302" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX257" runat="server" Columns="3" MaxLength="3" TabIndex="11303" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX258" runat="server" Columns="2" MaxLength="2" TabIndex="11304" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX259" runat="server" Columns="2" MaxLength="2" TabIndex="11305" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX260" runat="server" Columns="3" MaxLength="3" TabIndex="11306" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX261" runat="server" Columns="2" MaxLength="2" TabIndex="11307" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX262" runat="server" Columns="2" MaxLength="2" TabIndex="11308" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX263" runat="server" Columns="3" MaxLength="3" TabIndex="11309" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX264" runat="server" Columns="2" MaxLength="2" TabIndex="11310" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX265" runat="server" Columns="2" MaxLength="2" TabIndex="11311" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX266" runat="server" Columns="3" MaxLength="3" TabIndex="11312" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX267" runat="server" Columns="2" MaxLength="2" TabIndex="11313" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX268" runat="server" Columns="2" MaxLength="2" TabIndex="11314" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX269" runat="server" Columns="3" MaxLength="3" TabIndex="11315" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX270" runat="server" Columns="2" MaxLength="2" TabIndex="11316" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX271" runat="server" Columns="2" MaxLength="2" TabIndex="11317" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX272" runat="server" Columns="3" MaxLength="3" TabIndex="11318" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX273" runat="server" Columns="2" MaxLength="2" TabIndex="11319" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX274" runat="server" Columns="2" MaxLength="2" TabIndex="11320" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX275" runat="server" Columns="3" MaxLength="3" TabIndex="11321" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX276" runat="server" Columns="2" MaxLength="2" TabIndex="11322" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX277" runat="server" Columns="2" MaxLength="2" TabIndex="11323" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX278" runat="server" Columns="3" MaxLength="3" TabIndex="11324" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblOtro" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Otro"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX279" runat="server" Columns="2" MaxLength="2" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX280" runat="server" Columns="2" MaxLength="2" TabIndex="11402" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX281" runat="server" Columns="3" MaxLength="3" TabIndex="11403" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX282" runat="server" Columns="2" MaxLength="2" TabIndex="11404" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX283" runat="server" Columns="2" MaxLength="2" TabIndex="11405" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX284" runat="server" Columns="3" MaxLength="3" TabIndex="11406" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX285" runat="server" Columns="2" MaxLength="2" TabIndex="11407" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX286" runat="server" Columns="2" MaxLength="2" TabIndex="11408" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX287" runat="server" Columns="3" MaxLength="3" TabIndex="11409" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX288" runat="server" Columns="2" MaxLength="2" TabIndex="11410" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX289" runat="server" Columns="2" MaxLength="2" TabIndex="11411" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX290" runat="server" Columns="3" MaxLength="3" TabIndex="11412" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX291" runat="server" Columns="2" MaxLength="2" TabIndex="11413" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX292" runat="server" Columns="2" MaxLength="2" TabIndex="11414" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX293" runat="server" Columns="3" MaxLength="3" TabIndex="11415" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX294" runat="server" Columns="2" MaxLength="2" TabIndex="11416" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX295" runat="server" Columns="2" MaxLength="2" TabIndex="11417" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX296" runat="server" Columns="3" MaxLength="3" TabIndex="11418" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX297" runat="server" Columns="2" MaxLength="2" TabIndex="11419" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX298" runat="server" Columns="2" MaxLength="2" TabIndex="11420" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX299" runat="server" Columns="3" MaxLength="3" TabIndex="11421" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX300" runat="server" Columns="2" MaxLength="2" TabIndex="11422" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX301" runat="server" Columns="2" MaxLength="2" TabIndex="11423" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX302" runat="server" Columns="3" MaxLength="3" TabIndex="11424" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblTotal2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX303" runat="server" Columns="2" MaxLength="2" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 1px" class="linaBajo">
                    <asp:TextBox ID="txtVANX304" runat="server" Columns="2" MaxLength="2" TabIndex="11502" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 2px" class="linaBajo">
                    <asp:TextBox ID="txtVANX305" runat="server" Columns="3" MaxLength="3" TabIndex="11503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX306" runat="server" Columns="2" MaxLength="2" TabIndex="11504" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX307" runat="server" Columns="2" MaxLength="2" TabIndex="11506" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX308" runat="server" Columns="3" MaxLength="3" TabIndex="11507" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX309" runat="server" Columns="2" MaxLength="2" TabIndex="11507" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX310" runat="server" Columns="2" MaxLength="2" TabIndex="11508" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX311" runat="server" Columns="3" MaxLength="3" TabIndex="11509" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX312" runat="server" Columns="2" MaxLength="2" TabIndex="11510" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo" style="width: 42px">
                    <asp:TextBox ID="txtVANX313" runat="server" Columns="2" MaxLength="2" TabIndex="11511" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX314" runat="server" Columns="3" MaxLength="3" TabIndex="11512" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX315" runat="server" Columns="2" MaxLength="2" TabIndex="11513" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX316" runat="server" Columns="2" MaxLength="2" TabIndex="11514" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX317" runat="server" Columns="3" MaxLength="3" TabIndex="11515" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX318" runat="server" Columns="2" MaxLength="2" TabIndex="11516" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX319" runat="server" Columns="2" MaxLength="2" TabIndex="11517" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX320" runat="server" Columns="3" MaxLength="3" TabIndex="11518" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX321" runat="server" Columns="2" MaxLength="2" TabIndex="11519" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX322" runat="server" Columns="2" MaxLength="2" TabIndex="11520" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX323" runat="server" Columns="3" MaxLength="3" TabIndex="11521" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX324" runat="server" Columns="2" MaxLength="2" TabIndex="11522" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX325" runat="server" Columns="2" MaxLength="2" TabIndex="11523" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVANX326" runat="server" Columns="3" MaxLength="3" TabIndex="11524" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">
                </td>
            </tr>
            
        </table>
        <br />
        <table style="width: 1000px">
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="lblALUMNOS" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="III. Apoyo de Educaci�n Especial" ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label30" runat="server" CssClass="lblRojo" Font-Bold="True" Text='3. Si la escuela cuenta con alg�n servicio de apoyo de Educaci�n Especial o ninguno, marque con una "X" en la columna correspondiente, si recibe apoyo de otro servicio, especifique . Marque qu� tipo de atenci�n recibe.'
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center" valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label22" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TIPO DE SERVICIO"
                                    ></asp:Label></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="UNIDAD DE SERVICIOS DE APOYO A LA EDUCACI�N REGULAR (USAER)"
                                    ></asp:Label></td>
                            <td>
                                <asp:RadioButton ID="optVANX327" runat="server" GroupName="Servicio" TabIndex="320" />
                                <asp:TextBox ID="txtVANX327" runat="server" Width="16px"  style="visibility:hidden;" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left; height: 22px;">
                                <asp:Label ID="Label19" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CENTRO DE ATENCI�N M�LTIPLE (CAM)"
                                    ></asp:Label></td>
                            <td style="height: 22px">
                                <asp:RadioButton ID="optVANX328" runat="server" GroupName="Servicio" TabIndex="321" />
                                <asp:TextBox ID="txtVANX328" runat="server" Width="16px" style="visibility:hidden;" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left; height: 41px;">
                                <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NINGUNO"
                                    ></asp:Label></td>
                            <td style="height: 41px">
                                <asp:RadioButton ID="optVANX329" runat="server" GroupName="Servicio" TabIndex="322" />
                                <asp:TextBox ID="txtVANX329" runat="server" Width="16px" style="visibility:hidden;" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRO (ESPECIFIQUE)"
                                    ></asp:Label>
                                <asp:TextBox ID="txtVANX659" runat="server" Columns="60" CssClass="lblNegro" MaxLength="60"
                                    TabIndex="323"></asp:TextBox></td>
                            <td>
                                <asp:RadioButton ID="optVANX330" runat="server" GroupName="Servicio" TabIndex="323" />
                                <asp:TextBox ID="txtVANX330" runat="server" Width="16px" style="visibility:hidden;" ></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: center" valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label23" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TIPO DE ATENCI�N"
                                    ></asp:Label></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ITINERANTE"
                                    ></asp:Label></td>
                            <td>
                                <asp:RadioButton ID="optVANX332" runat="server" GroupName="Atencion" TabIndex="324" />
                                <asp:TextBox ID="txtVANX332" runat="server" Width="16px" style="visibility:hidden;" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PERMANENTE"
                                    ></asp:Label></td>
                            <td>
                                <asp:RadioButton ID="optVANX331" runat="server" GroupName="Atencion" TabIndex="325" />
                                <asp:TextBox ID="txtVANX331" runat="server" Width="16px"  style="visibility:hidden;" ></asp:TextBox></td>
                        </tr>
                    </table>
                    </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label31" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="IV. Respuesta Educativa" ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Escriba el n�mero de alumnos con Necesidades Educativas Especiales que cuentan con Informe de Evaluaci�n Psicopedag�gica."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX645" runat="server" Columns="2" MaxLength="2" TabIndex="11701" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX646" runat="server" Columns="2" MaxLength="2" TabIndex="11702" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX647" runat="server" Columns="3" MaxLength="3" TabIndex="11703" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5. Escriba el n�mero de alumnos con Necesidades Educativas Especiales que cuentan con Propuesta Curricular Adaptada."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX648" runat="server" Columns="2" MaxLength="2" TabIndex="11801" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX649" runat="server" Columns="2" MaxLength="2" TabIndex="11802" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX650" runat="server" Columns="3" MaxLength="3" TabIndex="11803" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6. Del total de alumnos con Necesidades Educativas Especiales escriba el n�mero de promovidos y no promovidos desglos�ndolos por sexo."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center" valign="top" colspan="2">
                    &nbsp;<table>
                        <tr>
                            <td style="height: 70px;">
                    <table>
                        <tr>
                            <td colspan="8">
                                <asp:Label ID="Label13" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PROMOVIDOS/APROBADOS"
                                    ></asp:Label></td>
                            <td colspan="1" style="width: 47px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX651" runat="server" Columns="3" MaxLength="3" TabIndex="12001" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 8px">
                            </td>
                            <td>
                                <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX652" runat="server" Columns="3" MaxLength="3" TabIndex="12002" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 10px">
                            </td>
                            <td>
                                <asp:Label ID="Label28" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX653" runat="server" Columns="3" MaxLength="3" TabIndex="12003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 47px">
                                &nbsp; &nbsp; &nbsp; &nbsp;
                            </td>
                        </tr>
                    </table>
                            </td>
                            <td style="height: 70px;">
                    <table>
                        <tr>
                            <td colspan="8" style="height: 20px">
                                <asp:Label ID="Label14" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NO PROMOVIDOS/REPROBADOS"
                                    ></asp:Label></td>
                            <td colspan="1" style="width: 74px; height: 20px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX654" runat="server" Columns="3" MaxLength="3" TabIndex="12004" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 14px">
                            </td>
                            <td>
                                <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX655" runat="server" Columns="3" MaxLength="3" TabIndex="12005" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 7px">
                            </td>
                            <td>
                                <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX656" runat="server" Columns="3" MaxLength="3" TabIndex="12006" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 74px">
                                &nbsp; &nbsp; &nbsp; &nbsp;
                            </td>
                        </tr>
                    </table>
                            </td>
                            <td style="width: 100px; height: 70px;">
                                <table>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:Label ID="Label44" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                                ></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px">
                                            <asp:TextBox ID="txtVANX657" runat="server" Columns="3" MaxLength="3" TabIndex="12007" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label32" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="V. Alumnos Hospitalizados" ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: justify">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7. Escriba el n�mero de alumnos que por motivos de salud estuvieron hospitalizados y se ausentaron por m�s de un mes del sal�n de clases."
                        ></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Hombres"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX661" runat="server" Columns="2" MaxLength="2" TabIndex="12201" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Mujeres"
                                    ></asp:Label></td>
                            <td style="width: 50px">
                                <asp:TextBox ID="txtVANX662" runat="server" Columns="2" MaxLength="2" TabIndex="12202" CssClass="lblNegro"></asp:TextBox></td>
                            <td nowrap="nowrap" style="width: 50px">
                            </td>
                            <td>
                                <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Total"
                                    ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtVANX663" runat="server" Columns="3" MaxLength="3" TabIndex="12203" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        &nbsp;<br />
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Inmueble_911_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir a p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Inmueble_911_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Oficializar_911_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Oficializar_911_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir a p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
        
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
         
     <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                 function PintaOPTs(){
                     marcar('VANX327');
                     marcar('VANX328');
                     marcar('VANX329');
                     marcar('VANX330');
                     marcar('VANX331');
                     marcar('VANX332');
                }  
                
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             } 
                              if (variable == 'VANX330')
                                     if (chk.checked) document.getElementById('ctl00_cphMainMaster_txtVANX659').disabled = false;
                                     else  document.getElementById('ctl00_cphMainMaster_txtVANX659').disabled = true;
                                     
//                           if (variable == 'VANX329'){
//                             if(chk.checked){
//                                 document.getElementById('optVANX331').checked = false;
//                                 document.getElementById('optVANX331').disabled = true;
//                                 document.getElementById('optVANX332').checked = false;
//                                 document.getElementById('optVANX332').disabled = true;
//                             }
//                             else{
//                                 document.getElementById('optVANX331').disabled = false;
//                                 document.getElementById('optVANX332').disabled = false;
//                             }
//                           }         
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }
                
                function OPTs2Txt(){
                     marcarTXT('VANX329');
                     marcarTXT('VANX327');
                     marcarTXT('VANX328');
                     marcarTXT('VANX330');
                     marcarTXT('VANX331');
                     marcarTXT('VANX332');
                }    
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         
                         var txt = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                           
//                         if (variable == 'VANX329'){
//                             if(chk.checked){
//                                 document.getElementById('optVANX331').checked = false;
//                                 document.getElementById('optVANX331').disabled = true;
//                                 document.getElementById('optVANX332').checked = false;
//                                 document.getElementById('optVANX332').disabled = true;
//                             }
//                             else{
//                                 document.getElementById('optVANX331').disabled = false;
//                                 document.getElementById('optVANX332').disabled = false;
//                             }
//                         }  
                         if (variable == 'VANX330'){
                               if (chk.checked) document.getElementById('ctl00_cphMainMaster_txtVANX659').disabled = false;
                               else  {
                                    
                                  document.getElementById('ctl00_cphMainMaster_txtVANX659').disabled = true;
                                  document.getElementById('ctl00_cphMainMaster_txtVANX659').value = '';
                               }
                         }
                     }
                } 
                
             
               PintaOPTs();
               Disparador(<%=hidDisparador.Value %>);             
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
