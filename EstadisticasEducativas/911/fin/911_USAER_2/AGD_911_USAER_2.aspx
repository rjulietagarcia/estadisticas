<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-2(Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_USAER_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_USAER_2.AGD_911_USAER_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 36;
        MaxRow = 23;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%> 

     <div id="logo"></div>
    <div style="min-width:1600px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="Label5" runat="server" Text="FIN"></asp:Label>&nbsp;<asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1600px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_2',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_2',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_2',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_2',true)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_2',true)"><a href="#" title=""><span>SECUNDARIA</span></a></li>
        <li onclick="openPage('AGD_911_USAER_2',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_USAER_2',false)"><a href="#" title=""><span>PERSONAL Y RECURSOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
        
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table id="TABLE1" style="text-align: center; width: 750px;" >
                        <tr>
                            <td colspan="6" style="text-align: left; vertical-align: top;">
                                <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="6. Escriba, por escuela, el total de alumnos beneficiados al inicio del ciclo escolar, seg�n el servicio o nivel educativo."
                                    Width="540px"></asp:Label></td>
                            <td colspan="1" style="padding-right: 10px; padding-left: 10px; width: 270px;
                                text-align: left">
                            </td>
                            <td colspan="4" style="padding-right: 10px; padding-left: 10px; width: 270px;
                                text-align: left">
                                <asp:Label ID="lbl7" runat="server" CssClass="lblRojo" Text="7. Escriba, por escuela, el n�mero de maestros que recibieron asesor�a y la cantidad de padres de familia orientados durante el ciclo escolar, adem�s, la cantidad de alumnos con capacidades y aptitudes sobresalientes (CAS)."
                                    Width="350px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: center">
                                <asp:Label ID="lblAlumnos6" runat="server" CssClass="lblRojo" Text="ALUMNOS BENEFICIADOS" Width="480px"></asp:Label>
                                </td>
                            <td colspan="1" style="text-align: center">
                            </td>
                            <td colspan="4" style="text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center;">
                                <asp:Label ID="lblEscuela6" runat="server" CssClass="lblRojo" Text="ESCUELA" ></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblInicial6" runat="server" CssClass="lblRojo" Text="INICIAL" ></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblPreescolar6" runat="server" CssClass="lblRojo" Text="PREESCOLAR" ></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblPrimaria6" runat="server" CssClass="lblRojo" Text="PRIMARIA" ></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblSecundaria6" runat="server" CssClass="lblRojo" Text="SECUNDARIA" ></asp:Label>&nbsp;</td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotal61" runat="server" CssClass="lblRojo" Text="TOTAL" ></asp:Label></td>
                            <td colspan="" style="padding-left: 10px; text-align: center">
                                </td>
                            <td colspan="" style="text-align: center">
                                <asp:Label ID="lblEscuela7" runat="server" CssClass="lblRojo" Text="ESCUELA" ></asp:Label></td>
                            <td colspan="" style="text-align: center">
                                <asp:Label ID="lblMaestros7" runat="server" CssClass="lblRojo" Text="MAESTROS ASESORADOS"
                                    Width="80px"></asp:Label></td>
                            <td colspan="" style="text-align: center">
                                <asp:Label ID="lblPadres8" runat="server" CssClass="lblRojo" Text="PADRES ORIENTADOS"
                                    Width="80px"></asp:Label></td>
                            <td colspan="" style="text-align: center">
                                <asp:Label ID="lblAlumnosCAS7" runat="server" CssClass="lblRojo" Text="ALUMNOS CAS"
                                    Width="80px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc16" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV572" runat="server" Columns="5" TabIndex="10101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV573" runat="server" Columns="5" TabIndex="10102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV574" runat="server" Columns="5" TabIndex="10103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV575" runat="server" Columns="5" TabIndex="10104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV576" runat="server" Columns="5" TabIndex="10105" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc17" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV627" runat="server" Columns="4" TabIndex="11301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV638" runat="server" Columns="4" TabIndex="11302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV649" runat="server" Columns="4" TabIndex="11303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc26" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV577" runat="server" Columns="5" TabIndex="10201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV578" runat="server" Columns="5" TabIndex="10202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV579" runat="server" Columns="5" TabIndex="10203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV580" runat="server" Columns="5" TabIndex="10204" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV581" runat="server" Columns="5" TabIndex="10205" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc27" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV628" runat="server" Columns="4" TabIndex="11401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV639" runat="server" Columns="4" TabIndex="11402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV650" runat="server" Columns="4" TabIndex="11403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                            <asp:Label ID="lblEsc36" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV582" runat="server" Columns="5" TabIndex="10301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV583" runat="server" Columns="5" TabIndex="10302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV584" runat="server" Columns="5" TabIndex="10303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV585" runat="server" Columns="5" TabIndex="10304" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV586" runat="server" Columns="5" TabIndex="10305" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc37" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV629" runat="server" Columns="4" TabIndex="11501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV640" runat="server" Columns="4" TabIndex="11502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV651" runat="server" Columns="4" TabIndex="11503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                            <asp:Label ID="lblEsc46" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV587" runat="server" Columns="5" TabIndex="10401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV588" runat="server" Columns="5" TabIndex="10402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV589" runat="server" Columns="5" TabIndex="10403" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV590" runat="server" Columns="5" TabIndex="10404" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV591" runat="server" Columns="5" TabIndex="10405" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc47" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV630" runat="server" Columns="4" TabIndex="11601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV641" runat="server" Columns="4" TabIndex="11602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV652" runat="server" Columns="4" TabIndex="11603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc56" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV592" runat="server" Columns="5" TabIndex="10501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV593" runat="server" Columns="5" TabIndex="10502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV594" runat="server" Columns="5" TabIndex="10503" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV595" runat="server" Columns="5" TabIndex="10504" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV596" runat="server" Columns="5" TabIndex="10505" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc57" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV631" runat="server" Columns="4" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV642" runat="server" Columns="4" TabIndex="11702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV653" runat="server" Columns="4" TabIndex="11703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc66" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV597" runat="server" Columns="5" TabIndex="10601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV598" runat="server" Columns="5" TabIndex="10602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV599" runat="server" Columns="5" TabIndex="10603" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV600" runat="server" Columns="5" TabIndex="10604" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV601" runat="server" Columns="5" TabIndex="10605" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc67" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV632" runat="server" Columns="4" TabIndex="11801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV643" runat="server" Columns="4" TabIndex="11802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV654" runat="server" Columns="4" TabIndex="11803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc76" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV602" runat="server" Columns="5" TabIndex="10701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV603" runat="server" Columns="5" TabIndex="10702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV604" runat="server" Columns="5" TabIndex="10703" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV605" runat="server" Columns="5" TabIndex="10704" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV606" runat="server" Columns="5" TabIndex="10705" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc77" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV633" runat="server" Columns="4" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV644" runat="server" Columns="4" TabIndex="11902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV655" runat="server" Columns="4" TabIndex="11903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc86" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV607" runat="server" Columns="5" TabIndex="10801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV608" runat="server" Columns="5" TabIndex="10802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV609" runat="server" Columns="5" TabIndex="10803" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV610" runat="server" Columns="5" TabIndex="10804" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV611" runat="server" Columns="5" TabIndex="10805" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc87" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV634" runat="server" Columns="4" TabIndex="12001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV645" runat="server" Columns="4" TabIndex="12002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV656" runat="server" Columns="4" TabIndex="12003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc96" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV612" runat="server" Columns="5" TabIndex="10901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV613" runat="server" Columns="5" TabIndex="10902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV614" runat="server" Columns="5" TabIndex="10903" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV615" runat="server" Columns="5" TabIndex="10904" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV616" runat="server" Columns="5" TabIndex="10905" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc97" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV635" runat="server" Columns="4" TabIndex="12101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV646" runat="server" Columns="4" TabIndex="12102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV657" runat="server" Columns="4" TabIndex="12103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc106" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV617" runat="server" Columns="5" TabIndex="11001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV618" runat="server" Columns="5" TabIndex="11002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV619" runat="server" Columns="5" TabIndex="11003" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV620" runat="server" Columns="5" TabIndex="11004" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV621" runat="server" Columns="5" TabIndex="11005" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblEsc107" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV636" runat="server" Columns="4" TabIndex="12201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV647" runat="server" Columns="4" TabIndex="12202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV658" runat="server" Columns="4" TabIndex="12203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblTotal62" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV622" runat="server" Columns="5" TabIndex="11101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV623" runat="server" Columns="5" TabIndex="11102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV624" runat="server" Columns="5" TabIndex="11103" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV625" runat="server" Columns="5" TabIndex="11104" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV626" runat="server" Columns="5" TabIndex="11105" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                            <td style="padding-left: 10px; text-align: center">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotal7" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV637" runat="server" Columns="4" TabIndex="12301" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV648" runat="server" Columns="4" TabIndex="12302" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV659" runat="server" Columns="4" TabIndex="12303" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="11" style="height: 4px; text-align: left">
                                </td>
                        </tr>
                    </table>
         </center>
        <center>
            &nbsp;</center>
        <center>
                                <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="8. Escriba la poblaci�n total atendida, desglos�ndola por nivel, servicio, edad cumplida a la fecha de llenado del cuestionario y sexo."
                                    Width="800px"></asp:Label>&nbsp;</center>
        <center>
            &nbsp;</center>
        <center>
            <table id="Table2"  >
                <tr>
                    <td style="text-align: center;">
                    </td>
                    <td colspan="2" style="text-align: center;" class="linaBajoAlto">
                                <asp:Label ID="lblInicial8" runat="server" CssClass="lblRojo" Text="EDUCACI�N INICIAL" ></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPreescolar8" runat="server" CssClass="lblRojo" Text="PREESCOLAR"
                                    ></asp:Label></td>
                            <td colspan="2" style="text-align: center;" class="linaBajoAlto">
                                &nbsp;<asp:Label ID="lblPrimaria8" runat="server" CssClass="lblRojo" Text="PRIMARIA"
                                    ></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblSecundaria8" runat="server" CssClass="lblRojo" Text="SECUNDARIA" ></asp:Label></td>
                    <td class="linaBajo" rowspan="2" style="text-align: center">
                                <asp:Label ID="lblTotal81" runat="server" CssClass="lblRojo" Text="TOTAL" Width="90px"></asp:Label></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="1" style="text-align: center">
                    </td>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombresEI8" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    ></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeresEI8" runat="server" CssClass="lblGrisTit" Text="MUJERES"
                                    ></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombresPre8" runat="server" CssClass="lblGrisTit" Text="HOMBRES" ></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeresPre8" runat="server" CssClass="lblGrisTit" Text="MUJERES" ></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombresPri8" runat="server" CssClass="lblGrisTit" Text="HOMBRES" ></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeresPri8" runat="server" CssClass="lblGrisTit" Text="MUJERES" ></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblHombresSec8" runat="server" CssClass="lblGrisTit" Text="HOMBRES" ></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblMujeresSec8" runat="server" CssClass="lblGrisTit" Text="MUJERES" ></asp:Label></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center; height: 30px;" class="linaBajoAlto">
                                <asp:Label ID="lbl45Dias" runat="server" CssClass="lblGrisTit" Text="45 d�as a 1 a�o" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV660" runat="server" Columns="5" TabIndex="20101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV661" runat="server" Columns="5" TabIndex="20102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV662" runat="server" Columns="5" TabIndex="20103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV663" runat="server" Columns="5" TabIndex="20104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV664" runat="server" Columns="5" TabIndex="20105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV665" runat="server" Columns="5" TabIndex="20106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV666" runat="server" Columns="5" TabIndex="20107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV667" runat="server" Columns="5" TabIndex="20108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV668" runat="server" Columns="5" TabIndex="20109" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lbl2A�os" runat="server" CssClass="lblGrisTit" Text="2 a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV669" runat="server" Columns="5" TabIndex="20201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV670" runat="server" Columns="5" TabIndex="20202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV671" runat="server" Columns="5" TabIndex="20203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV672" runat="server" Columns="5" TabIndex="20204" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV673" runat="server" Columns="5" TabIndex="20205" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV674" runat="server" Columns="5" TabIndex="20206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV675" runat="server" Columns="5" TabIndex="20207" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV676" runat="server" Columns="5" TabIndex="20208" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV677" runat="server" Columns="5" TabIndex="20209" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lbl3A�os" runat="server" CssClass="lblGrisTit" Text="3 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV678" runat="server" Columns="5" TabIndex="20301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV679" runat="server" Columns="5" TabIndex="20302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV680" runat="server" Columns="5" TabIndex="20303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV681" runat="server" Columns="5" TabIndex="20304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV682" runat="server" Columns="5" TabIndex="20305" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV683" runat="server" Columns="5" TabIndex="20306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV684" runat="server" Columns="5" TabIndex="20307" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV685" runat="server" Columns="5" TabIndex="20308" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV686" runat="server" Columns="5" TabIndex="20309" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lbl3A�os6Meses" runat="server" CssClass="lblGrisTit" Text="3 a�os 6 meses a 3 a�os 11 meses"
                                    Width="100px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV687" runat="server" Columns="5" TabIndex="20401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV688" runat="server" Columns="5" TabIndex="20402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV689" runat="server" Columns="5" TabIndex="20403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV690" runat="server" Columns="5" TabIndex="20404" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV691" runat="server" Columns="5" TabIndex="20405" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV692" runat="server" Columns="5" TabIndex="20406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV693" runat="server" Columns="5" TabIndex="20407" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV694" runat="server" Columns="5" TabIndex="20408" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV695" runat="server" Columns="5" TabIndex="20409" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lbl4A�os" runat="server" CssClass="lblGrisTit" Text="4 a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV696" runat="server" Columns="5" TabIndex="20501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV697" runat="server" Columns="5" TabIndex="20502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV698" runat="server" Columns="5" TabIndex="20503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV699" runat="server" Columns="5" TabIndex="20504" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV700" runat="server" Columns="5" TabIndex="20505" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV701" runat="server" Columns="5" TabIndex="20506" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV702" runat="server" Columns="5" TabIndex="20507" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV703" runat="server" Columns="5" TabIndex="20508" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV704" runat="server" Columns="5" TabIndex="20509" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lbl5A�os" runat="server" CssClass="lblGrisTit" Text="5 a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV705" runat="server" Columns="5" TabIndex="20601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV706" runat="server" Columns="5" TabIndex="20602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV707" runat="server" Columns="5" TabIndex="20603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV708" runat="server" Columns="5" TabIndex="20604" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV709" runat="server" Columns="5" TabIndex="20605" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV710" runat="server" Columns="5" TabIndex="20606" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV711" runat="server" Columns="5" TabIndex="20607" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV712" runat="server" Columns="5" TabIndex="20608" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV713" runat="server" Columns="5" TabIndex="20609" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lbl6A�os" runat="server" CssClass="lblGrisTit" Text="6 a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV714" runat="server" Columns="5" TabIndex="20701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV715" runat="server" Columns="5" TabIndex="20702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV716" runat="server" Columns="5" TabIndex="20703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV717" runat="server" Columns="5" TabIndex="20704" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV718" runat="server" Columns="5" TabIndex="20705" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV719" runat="server" Columns="5" TabIndex="20706" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV720" runat="server" Columns="5" TabIndex="20707" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV721" runat="server" Columns="5" TabIndex="20708" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV722" runat="server" Columns="5" TabIndex="20709" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lbl7A�os" runat="server" CssClass="lblGrisTit" Text="7 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV723" runat="server" Columns="5" TabIndex="20801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV724" runat="server" Columns="5" TabIndex="20802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV725" runat="server" Columns="5" TabIndex="20803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV726" runat="server" Columns="5" TabIndex="20804" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV727" runat="server" Columns="5" TabIndex="20805" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV728" runat="server" Columns="5" TabIndex="20806" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV729" runat="server" Columns="5" TabIndex="20807" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV730" runat="server" Columns="5" TabIndex="20808" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV731" runat="server" Columns="5" TabIndex="20809" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl8A�os" runat="server" CssClass="lblGrisTit" Text="8 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV732" runat="server" Columns="5" TabIndex="20901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV733" runat="server" Columns="5" TabIndex="20902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV734" runat="server" Columns="5" TabIndex="20903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV735" runat="server" Columns="5" TabIndex="20904" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV736" runat="server" Columns="5" TabIndex="20905" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV737" runat="server" Columns="5" TabIndex="20906" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV738" runat="server" Columns="5" TabIndex="20907" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV739" runat="server" Columns="5" TabIndex="20908" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV740" runat="server" Columns="5" TabIndex="20909" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl9A�os" runat="server" CssClass="lblGrisTit" Text="9 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV741" runat="server" Columns="5" TabIndex="21001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV742" runat="server" Columns="5" TabIndex="21002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV743" runat="server" Columns="5" TabIndex="21003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV744" runat="server" Columns="5" TabIndex="21004" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV745" runat="server" Columns="5" TabIndex="21005" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV746" runat="server" Columns="5" TabIndex="21006" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV747" runat="server" Columns="5" TabIndex="21007" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV748" runat="server" Columns="5" TabIndex="21008" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV749" runat="server" Columns="5" TabIndex="21009" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl10A�os" runat="server" CssClass="lblGrisTit" Text="10 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV750" runat="server" Columns="5" TabIndex="21101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV751" runat="server" Columns="5" TabIndex="21102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV752" runat="server" Columns="5" TabIndex="21103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV753" runat="server" Columns="5" TabIndex="21104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV754" runat="server" Columns="5" TabIndex="21105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV755" runat="server" Columns="5" TabIndex="21106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV756" runat="server" Columns="5" TabIndex="21107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV757" runat="server" Columns="5" TabIndex="21108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV758" runat="server" Columns="5" TabIndex="21109" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl11A�os" runat="server" CssClass="lblGrisTit" Text="11 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV759" runat="server" Columns="5" TabIndex="21201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV760" runat="server" Columns="5" TabIndex="21202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV761" runat="server" Columns="5" TabIndex="21203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV762" runat="server" Columns="5" TabIndex="21204" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV763" runat="server" Columns="5" TabIndex="21205" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV764" runat="server" Columns="5" TabIndex="21206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV765" runat="server" Columns="5" TabIndex="21207" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV766" runat="server" Columns="5" TabIndex="21208" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV767" runat="server" Columns="5" TabIndex="21209" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl12A�os" runat="server" CssClass="lblGrisTit" Text="12 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV768" runat="server" Columns="5" TabIndex="21301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV769" runat="server" Columns="5" TabIndex="21302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV770" runat="server" Columns="5" TabIndex="21303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV771" runat="server" Columns="5" TabIndex="21304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV772" runat="server" Columns="5" TabIndex="21305" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV773" runat="server" Columns="5" TabIndex="21306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV774" runat="server" Columns="5" TabIndex="21307" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV775" runat="server" Columns="5" TabIndex="21308" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV776" runat="server" Columns="5" TabIndex="21309" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl13A�os" runat="server" CssClass="lblGrisTit" Text="13 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV777" runat="server" Columns="5" TabIndex="21401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo" colspan="1">
                                <asp:TextBox ID="txtV778" runat="server" Columns="5" TabIndex="21402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV779" runat="server" Columns="5" TabIndex="21403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV780" runat="server" Columns="5" TabIndex="21404" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV781" runat="server" Columns="5" TabIndex="21405" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV782" runat="server" Columns="5" TabIndex="21406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV783" runat="server" Columns="5" TabIndex="21407" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV784" runat="server" Columns="5" TabIndex="21408" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV785" runat="server" Columns="5" TabIndex="21409" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl14A�os" runat="server" CssClass="lblGrisTit" Text="14 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV786" runat="server" Columns="5" TabIndex="21501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV787" runat="server" Columns="5" TabIndex="21502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV788" runat="server" Columns="5" TabIndex="21503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV789" runat="server" Columns="5" TabIndex="21504" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV790" runat="server" Columns="5" TabIndex="21505" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV791" runat="server" Columns="5" TabIndex="21506" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV792" runat="server" Columns="5" TabIndex="21507" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV793" runat="server" Columns="5" TabIndex="21508" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV794" runat="server" Columns="5" TabIndex="21509" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl15A�os" runat="server" CssClass="lblGrisTit" Text="15 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV795" runat="server" Columns="5" TabIndex="21601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV796" runat="server" Columns="5" TabIndex="21602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV797" runat="server" Columns="5" TabIndex="21603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV798" runat="server" Columns="5" TabIndex="21604" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV799" runat="server" Columns="5" TabIndex="21605" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV800" runat="server" Columns="5" TabIndex="21606" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV801" runat="server" Columns="5" TabIndex="21607" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV802" runat="server" Columns="5" TabIndex="21608" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV803" runat="server" Columns="5" TabIndex="21609" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl16A�os" runat="server" CssClass="lblGrisTit" Text="16 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV804" runat="server" Columns="5" TabIndex="21701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV805" runat="server" Columns="5" TabIndex="21702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV806" runat="server" Columns="5" TabIndex="21703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV807" runat="server" Columns="5" TabIndex="21704" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV808" runat="server" Columns="5" TabIndex="21705" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV809" runat="server" Columns="5" TabIndex="21706" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV810" runat="server" Columns="5" TabIndex="21707" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV811" runat="server" Columns="5" TabIndex="21708" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV812" runat="server" Columns="5" TabIndex="21709" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl17A�os" runat="server" CssClass="lblGrisTit" Text="17 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV813" runat="server" Columns="5" TabIndex="21801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV814" runat="server" Columns="5" TabIndex="21802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV815" runat="server" Columns="5" TabIndex="21803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV816" runat="server" Columns="5" TabIndex="21804" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV817" runat="server" Columns="5" TabIndex="21805" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV818" runat="server" Columns="5" TabIndex="21806" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV819" runat="server" Columns="5" TabIndex="21807" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV820" runat="server" Columns="5" TabIndex="21808" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV821" runat="server" Columns="5" TabIndex="21809" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl18A�os" runat="server" CssClass="lblGrisTit" Text="18 a�os" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV822" runat="server" Columns="5" TabIndex="21901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV823" runat="server" Columns="5" TabIndex="21902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV824" runat="server" Columns="5" TabIndex="21903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV825" runat="server" Columns="5" TabIndex="21904" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV826" runat="server" Columns="5" TabIndex="21905" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV827" runat="server" Columns="5" TabIndex="21906" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV828" runat="server" Columns="5" TabIndex="21907" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV829" runat="server" Columns="5" TabIndex="21908" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV830" runat="server" Columns="5" TabIndex="21909" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center" class="linaBajo">
                                <asp:Label ID="lbl19A�os" runat="server" CssClass="lblGrisTit" Text="19 o m�s a�os"
                                    Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV831" runat="server" Columns="5" TabIndex="22001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV832" runat="server" Columns="5" TabIndex="22002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV833" runat="server" Columns="5" TabIndex="22003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV834" runat="server" Columns="5" TabIndex="22004" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV835" runat="server" Columns="5" TabIndex="22005" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV836" runat="server" Columns="5" TabIndex="22006" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV837" runat="server" Columns="5" TabIndex="22007" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV838" runat="server" Columns="5" TabIndex="22008" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV839" runat="server" Columns="5" TabIndex="22009" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="linaBajo">
                                <asp:Label ID="lblSubtotal8" runat="server" CssClass="lblRojo" Text="SUBTOTALES" Width="90px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV840" runat="server" Columns="5" TabIndex="22101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV841" runat="server" Columns="5" TabIndex="22102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV842" runat="server" Columns="5" TabIndex="22103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV843" runat="server" Columns="5" TabIndex="22104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV844" runat="server" Columns="5" TabIndex="22105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV845" runat="server" Columns="5" TabIndex="22106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV846" runat="server" Columns="5" TabIndex="22107" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV847" runat="server" Columns="5" TabIndex="22108" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV848" runat="server" Columns="5" TabIndex="22109" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="1" style="text-align: center" class="linaBajo">
                                <asp:Label ID="lblTotal82" runat="server" CssClass="lblRojo" Text="TOTAL" Width="90px"></asp:Label></td>
                            <td style="text-align: center" colspan="2" class="linaBajo">
                                <asp:TextBox ID="txtV849" runat="server" Columns="5" TabIndex="22231" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="text-align: center" colspan="2" class="linaBajo">
                                <asp:TextBox ID="txtV850" runat="server" Columns="5" TabIndex="22232" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="text-align: center" colspan="2" class="linaBajo">
                                <asp:TextBox ID="txtV851" runat="server" Columns="5" TabIndex="22233" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="text-align: center" colspan="2" class="linaBajo">
                                <asp:TextBox ID="txtV852" runat="server" Columns="5" TabIndex="22234" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV853" runat="server" Columns="5" TabIndex="22235" CssClass="lblNegro" MaxLength="5"></asp:TextBox></td>
                    <td class="Orila" style="text-align: center">
                        &nbsp;</td>
                </tr>
            </table>
    
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('ASEC_911_USAER_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('ASEC_911_USAER_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal_911_USAER_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_USAER_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
<%--				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>--%>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
         
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
        
</asp:Content>
