<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.6C (Alumnos por Especialidad)" AutoEventWireup="true" CodeBehind="ACG_911_6C.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_6C.ACG_911_6C" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />

   
    <script type="text/javascript">
        var _enter=true;
    </script>
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>


    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
     <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>CAPACITACI�N PARA EL TRABAJO</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_6C',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('ACG_911_6C',true)"><a href="#" class="activo" title=""><span>ALUMNOS POR ESPECIALIDAD</span></a></li>
        <li onclick="openPage('AGT_911_6C',false)"><a href="#" title=""><span>TOTAL ALUMNOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

         <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


                    <table >
                        <tr>
                            <td colspan="11" style=" text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                    Text="I. ALUMNOS POR ESPECIALIDAD" ></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="11" style="text-align: left; padding-bottom: 20px;">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        
                                    
                                    Text="1. Escriba por cada especialidad impartida de julio del 2012 a junio del 2013, el n�mero progresivo, la clave de especialidad, nombre, grupos, el total de alumnos, desglos�ndolo por sexo, inscripci�n total, existencia. acreditados y rango de edad." Width="942px"
                       ></asp:Label>
                                <br />
                                <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Verifique que las sumas de los alumnos por rango de edad sea igual al total."></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="11" style="text-align: left">
                                <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True" Text="N�MERO PROGRESIVO"
                                    Width="155px"></asp:Label>
                                <asp:TextBox ID="txtVA0" runat="server" Columns="5" MaxLength="15" Width="24px" BackColor="Bisque" Font-Bold="True"  style="text-align: right" ReadOnly="True">0</asp:TextBox>
                                <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True" Text="CLAVE DE LA ESPECIALIDAD"
                                    Width="170px"></asp:Label>
                                <asp:TextBox ID="txtVA59" runat="server" Columns="9" MaxLength="9" TabIndex="10101" CssClass="lblNegro"></asp:TextBox>
                                <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Font-Bold="True" Text="NOMBRE DE LA ESPECIALIDAD"
                                    Width="177px"></asp:Label>
                                <asp:TextBox ID="txtVA1" runat="server" Columns="60" MaxLength="100" 
                                    TabIndex="10102" CssClass="lblNegro"></asp:TextBox>
                                <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                                    Width="38px"></asp:Label>
                                <asp:TextBox ID="txtVA56" runat="server" Columns="3" MaxLength="3" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
        </table>
        
       
            <table >
                <tr>
                <td>
                    </td>
                <td>
                    </td>
                <td style="vertical-align: bottom;">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="MENOS DE 15 A�OS" Width="90px"></asp:Label></td>
                <td style="vertical-align: bottom;">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15- 19 A�OS"
                        Width="90px"></asp:Label></td>
                <td style="vertical-align: bottom;">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="20-24 A�OS"
                        Width="90px"></asp:Label></td>
                <td style="vertical-align: bottom;">
                    <asp:Label ID="lblReprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="25-34 A�OS"
                        Width="90px"></asp:Label></td>
                <td style="vertical-align: bottom; text-align: center">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="35-44 A�OS"
                        Width="90px"></asp:Label></td>
                <td style="vertical-align: bottom; text-align: center">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="45-54 A�OS"
                        Width="90px"></asp:Label></td>
                <td style="vertical-align: bottom; text-align: center">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="55-64 A�OS"
                        Width="90px"></asp:Label></td>
                <td style="vertical-align: bottom; text-align: center">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="65 A�OS Y M�S"
                        Width="90px"></asp:Label></td>
                <td style="vertical-align: bottom; text-align: center">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
                </tr>
                <tr>
                <td rowspan="3">
                    <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        ></asp:Label></td>
                <td style="text-align: left;">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCION TOTAL"
                        Width="120px"></asp:Label></td>
                <td>
                                <asp:TextBox ID="txtVA2" runat="server" Columns="4" MaxLength="4" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                                <asp:TextBox ID="txtVA3" runat="server" Columns="4" MaxLength="4" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                                <asp:TextBox ID="txtVA4" runat="server" Columns="4" MaxLength="4" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                                <asp:TextBox ID="txtVA5" runat="server" Columns="4" MaxLength="4" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA6" runat="server" Columns="4" MaxLength="4" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA7" runat="server" Columns="4" MaxLength="4" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA8" runat="server" Columns="4" MaxLength="4" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA9" runat="server" Columns="4" MaxLength="4" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA10" runat="server" Columns="4" MaxLength="4" TabIndex="20109" CssClass="lblNegro"></asp:TextBox></td>
                </tr>
                <tr>
                <td style="text-align: left;">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="120px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtVA11" runat="server" Columns="4" MaxLength="4" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA12" runat="server" Columns="4" MaxLength="4" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA13" runat="server" Columns="4" MaxLength="4" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA14" runat="server" Columns="4" MaxLength="4" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA15" runat="server" Columns="4" MaxLength="4" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA16" runat="server" Columns="4" MaxLength="4" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA17" runat="server" Columns="4" MaxLength="4" TabIndex="20207" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA18" runat="server" Columns="4" MaxLength="4" TabIndex="20208" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA19" runat="server" Columns="4" MaxLength="4" TabIndex="20209" CssClass="lblNegro"></asp:TextBox></td>
                </tr>
                <tr>
                <td style="text-align: left;">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ACREDITADOS"
                        Width="120px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtVA20" runat="server" Columns="4" MaxLength="4" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA21" runat="server" Columns="4" MaxLength="4" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA22" runat="server" Columns="4" MaxLength="4" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA23" runat="server" Columns="4" MaxLength="4" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA24" runat="server" Columns="4" MaxLength="4" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA25" runat="server" Columns="4" MaxLength="4" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA26" runat="server" Columns="4" MaxLength="4" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA27" runat="server" Columns="4" MaxLength="4" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA28" runat="server" Columns="4" MaxLength="4" TabIndex="20309" CssClass="lblNegro"></asp:TextBox></td>
                </tr>
                <tr>
                <td rowspan="3">
                    <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        ></asp:Label></td>
                <td style="text-align: left">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCION TOTAL"
                        Width="120px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtVA29" runat="server" Columns="4" MaxLength="4" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA30" runat="server" Columns="4" MaxLength="4" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA31" runat="server" Columns="4" MaxLength="4" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA32" runat="server" Columns="4" MaxLength="4" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA33" runat="server" Columns="4" MaxLength="4" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA34" runat="server" Columns="4" MaxLength="4" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA35" runat="server" Columns="4" MaxLength="4" TabIndex="20407" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA36" runat="server" Columns="4" MaxLength="4" TabIndex="20408" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA37" runat="server" Columns="4" MaxLength="4" TabIndex="20409" CssClass="lblNegro"></asp:TextBox></td>
                </tr>
                <tr>
                <td style="text-align: left;">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="120px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtVA38" runat="server" Columns="4" MaxLength="4" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA39" runat="server" Columns="4" MaxLength="4" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA40" runat="server" Columns="4" MaxLength="4" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA41" runat="server" Columns="4" MaxLength="4" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA42" runat="server" Columns="4" MaxLength="4" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA43" runat="server" Columns="4" MaxLength="4" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA44" runat="server" Columns="4" MaxLength="4" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA45" runat="server" Columns="4" MaxLength="4" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA46" runat="server" Columns="4" MaxLength="4" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
                </tr>
                <tr>
                <td style="text-align: left;">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ACREDITADOS"
                        Width="120px"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtVA47" runat="server" Columns="4" MaxLength="4" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA48" runat="server" Columns="4" MaxLength="4" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA49" runat="server" Columns="4" MaxLength="4" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA50" runat="server" Columns="4" MaxLength="4" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA51" runat="server" Columns="4" MaxLength="4" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA52" runat="server" Columns="4" MaxLength="4" TabIndex="20606" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA53" runat="server" Columns="4" MaxLength="4" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA54" runat="server" Columns="4" MaxLength="4" TabIndex="20608" CssClass="lblNegro"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtVA55" runat="server" Columns="4" MaxLength="4" TabIndex="20609" CssClass="lblNegro"></asp:TextBox></td>
                </tr>
            </table>
       
        
            <table>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkEliminarCarrera" runat="server" OnClick="lnkEliminarCarrera_Click">Eliminar Carrera Actual</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                <td>
                    Carreras:
                </td>
                    <td style="font-weight: bold;">
                       <asp:DropDownList ID="ddlID_Carrera" runat="server"  onchange='OnChange(this);'>
                       </asp:DropDownList > de 
                       <asp:TextBox ID="lblUnodeN" runat="server" Columns="15" MaxLength="15" Width="24px" BackColor="Bisque" Font-Bold="True" ReadOnly="True">0</asp:TextBox>
                    </td>
                    <td id="td_Nuevo" runat="server"><span onclick = "navegarCarreras_new(-1);" >&nbsp;<a href="#">Agregar Otra <strong>*</strong></a></span></td>
                </tr>
            </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_6C',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_6C',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AGT_911_6C',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_6C',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
      <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
      
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
       <%-- <%=hidIdCtrl.ClientID %>--%>
       
         <script type="text/javascript" language="javascript">
                document.getElementById("ctl00_cphMainMaster_txtVA1").onkeypress = "";
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                var ID_Carrera =0; 
                
                 
               
                function OnChange(dropdown)
                {
                    var myindex  = dropdown.selectedIndex
                    var SelValue = dropdown.options[myindex].value
                    navegarCarreras_new(SelValue);
                    return true;
                }
                
                function navegarCarreras_new(id_carrera){
                     ID_Carrera = (id_carrera);
                     var ID_Carreratmp = ID_Carrera; 
                                        
                    if (id_carrera == -1)
                    {
                       var totalRegs = document.getElementById('<%=lblUnodeN.ClientID%>').value;
                       ID_Carrera = eval(totalRegs) + 1; 
                       alert(ID_Carrera);
                    }
                
                    openPage('ACG_911_6C');
                }
               
                function __ReceiveServerData_Variables(rValue){
                   if (rValue != null){
                        document.getElementById("divWait").style.visibility = "hidden";
                        var rows = rValue.split('|');
                        var vi = 1;
                        if (rValue != "")  LimpiarFallas();
                        var detalles="<ul>";
                        for(vi = 1; vi < rows.length; vi ++){
                            var res = rows[vi];
                            var colums = res.split('!');
                            var caja = document.getElementById("ctl00_cphMainMaster_txtV" + colums[0]);
                            if (caja!=null){
                                 caja.className = "txtVarFallas";
                            }
                            detalles = detalles + "<li>" + colums[1]+ "</li>";
                       }
                       if (CambiarPagina != "") {  // para que no se borre la informacion a menos que se mueva de la pagina
                            var obj = document.getElementById('divResultado');
                            obj.innerHTML =  detalles + "</ul>";
                       }
                       var Cambiar = "No";
                       if ( rValue != "" && CambiarPagina != "" ){
                          if(ir1)
                        {
                           
                            Cambiar = "Si";
                        }
                        else
                        {
                            alert("Se encontraron errores de captura\nDebe corregirlos para avanzar a la siguiente p"+'\u00e1'+"gina.");
                            Cambiar = "No";
                        }
                       }
                       else if(rValue == "" && CambiarPagina != ""){
                          Cambiar = "Si";
                       }
                       if (Cambiar == "Si"){
                             window.location.href=CambiarPagina+".aspx?idC=" + ID_Carrera;
                       } 
                   }
                }
                
                
               
 		Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
