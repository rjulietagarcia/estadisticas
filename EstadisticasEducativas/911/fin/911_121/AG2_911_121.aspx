<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AG2_911_121.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_121.AG2_911_121" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
<script type="text/javascript">
        MaxCol = 6;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    
    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PREESCOLAR IND�GENA</span></td>
        </tr>
        <tr><td>
            <asp:Label ID="Label20" runat="server" Text="FIN DE CURSOS 2012-2013"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
 
   <div id="menu" style="min-width:1100px;">
        <ul class="left">
            <li onclick="openPage('Identificacion_911_121',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
            <li onclick="openPage('AG1_911_121',true)"><a href="#" title="" ><span>1�</span></a></li>
            <li onclick="openPage('AG2_911_121',true)"><a href="#" title="" class="activo"><span>2�</span></a></li>
            <li onclick="openPage('AG3_911_121',false)"><a href="#" title=""><span>3�</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
            
        </ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
  

   <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
	<tr>
		<td class="EsqSupIzq"></td>
		<td class="RepSup"></td>
		<td class="EsqSupDer"></td>
	</tr>
	<tr>
		<td class="RepLatIzq"> </td>
	    <td>

            <table>
                <tr>
                    <td colspan="2" >
                        <asp:Label ID="Label19" runat="server" Text="2�" CssClass="lblGrisTit" Font-Size="16px"></asp:Label></td>
                   
                    <td style="width: 67px">
                        <asp:Label ID="Label14" runat="server" Text="3 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="Label15" runat="server" Text="4 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="Label16" runat="server" Text="5 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="Label17" runat="server" Text="6 a�os" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 67px">
                        <asp:Label ID="Label18" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="Label3" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; text-align: left;">
                        <asp:Label ID="Label12" runat="server" Text="INSCRIPCI�N TOTAL" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtV47" runat="server" Columns="4" MaxLength="4" TabIndex="10101"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV48" runat="server" Columns="4" MaxLength="4" TabIndex="10102"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV49" runat="server" Columns="4" MaxLength="4" TabIndex="10103"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV50" runat="server" Columns="4" MaxLength="4" TabIndex="10104"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV51" runat="server" Columns="4" MaxLength="4" TabIndex="10105"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left;">
                        <asp:Label ID="Label4" runat="server" Text="EXISTENCIA" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtV52" runat="server" Columns="4" MaxLength="4" TabIndex="10201"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV53" runat="server" Columns="4" MaxLength="4" TabIndex="10202"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV54" runat="server" Columns="4" MaxLength="4" TabIndex="10203"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV55" runat="server" Columns="4" MaxLength="4" TabIndex="10204"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV56" runat="server" Columns="4" MaxLength="4" TabIndex="10205"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left;">
                        <asp:Label ID="Label5" runat="server" Text="PROMOVIDOS" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtV57" runat="server" Columns="4" MaxLength="4" TabIndex="10301"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV58" runat="server" Columns="4" MaxLength="4" TabIndex="10302"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV59" runat="server" Columns="4" MaxLength="4" TabIndex="10303"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV60" runat="server" Columns="4" MaxLength="4" TabIndex="10304"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV61" runat="server" Columns="4" MaxLength="4" TabIndex="10305"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><br />
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="Label2" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; text-align: left;">
                        <asp:Label ID="Label6" runat="server" Text="INSCRIPCI�N TOTAL" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtV62" runat="server" Columns="4" MaxLength="4" TabIndex="10401"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV63" runat="server" Columns="4" MaxLength="4" TabIndex="10402"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV64" runat="server" Columns="4" MaxLength="4" TabIndex="10403"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV65" runat="server" Columns="4" MaxLength="4" TabIndex="10404"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV66" runat="server" Columns="4" MaxLength="4" TabIndex="10405"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left;">
                        <asp:Label ID="Label8" runat="server" Text="EXISTENCIA" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtV67" runat="server" Columns="4" MaxLength="4" TabIndex="10501"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV68" runat="server" Columns="4" MaxLength="4" TabIndex="10502"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV69" runat="server" Columns="4" MaxLength="4" TabIndex="10503"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV70" runat="server" Columns="4" MaxLength="4" TabIndex="10504"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV71" runat="server" Columns="4" MaxLength="4" TabIndex="10505"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left;">
                        <asp:Label ID="Label10" runat="server" Text="PROMOVIDOS" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtV72" runat="server" Columns="4" MaxLength="4" TabIndex="10601"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV73" runat="server" Columns="4" MaxLength="4" TabIndex="10602"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV74" runat="server" Columns="4" MaxLength="4" TabIndex="10603"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV75" runat="server" Columns="4" MaxLength="4" TabIndex="10604"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV76" runat="server" Columns="4" MaxLength="4" TabIndex="10605"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><br /></td>
                </tr>
                <tr>
                    <td rowspan="3" style="width: 75px">
                        <asp:Label ID="Label1" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                    <td style="width: 132px; height: 10px; text-align: left;">
                        <asp:Label ID="Label7" runat="server" Text="INSCRIPCI�N TOTAL" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td style="height: 10px">
                        <asp:TextBox ID="txtV77" runat="server" Columns="4" MaxLength="4" TabIndex="10701"></asp:TextBox></td>
                    <td style="height: 10px">
                        <asp:TextBox ID="txtV78" runat="server" Columns="4" MaxLength="4" TabIndex="10702"></asp:TextBox></td>
                    <td style="height: 10px">
                        <asp:TextBox ID="txtV79" runat="server" Columns="4" MaxLength="4" TabIndex="10703"></asp:TextBox></td>
                    <td style="height: 10px">
                        <asp:TextBox ID="txtV80" runat="server" Columns="4" MaxLength="4" TabIndex="10704"></asp:TextBox></td>
                    <td style="height: 10px">
                        <asp:TextBox ID="txtV81" runat="server" Columns="4" MaxLength="4" TabIndex="10705"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left;">
                        <asp:Label ID="Label9" runat="server" Text="EXISTENCIA" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtV82" runat="server" Columns="4" MaxLength="4" TabIndex="10801"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV83" runat="server" Columns="4" MaxLength="4" TabIndex="10802"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV84" runat="server" Columns="4" MaxLength="4" TabIndex="10803"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV85" runat="server" Columns="4" MaxLength="4" TabIndex="10804"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV86" runat="server" Columns="4" MaxLength="4" TabIndex="10805"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 132px; text-align: left;">
                        <asp:Label ID="Label11" runat="server" Text="PROMOVIDOS" Width="130px" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtV87" runat="server" Columns="4" MaxLength="4" TabIndex="10901"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV88" runat="server" Columns="4" MaxLength="4" TabIndex="10902"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV89" runat="server" Columns="4" MaxLength="4" TabIndex="10903"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV90" runat="server" Columns="4" MaxLength="4" TabIndex="10904"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="txtV91" runat="server" Columns="4" MaxLength="4" TabIndex="10905"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><br /></td>
                </tr>
                <tr>
                    <td rowspan="1" >
                    </td>
                    <td style=" text-align: left;">
                        <asp:Label ID="Label13" runat="server" Text="GRUPOS" CssClass="lblGrisTit"></asp:Label></td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:TextBox ID="txtV92" runat="server" Columns="4" MaxLength="2" TabIndex="11101"></asp:TextBox></td>
                </tr>
            </table>
            <hr />
            <table align="center">
                <tr>
                    <td style="height: 47px" ><span  onclick="openPage('AG1_911_121',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                    <td style="height: 47px" ><span  onclick="openPage('AG1_911_121',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                    <td style="width: 330px;">&nbsp;
                    </td>
                    <td style="height: 47px" ><span  onclick="openPage('AG3_911_121',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                    <td style="height: 47px" ><span  onclick="openPage('AG3_911_121',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                </tr>
            </table> 
            
            <div class="divResultado" id="divResultado"></div> 

            
		    </td>
	        <td class="RepLatDer">&nbsp;</td>
	    </tr>
	    <tr>
		    <td class="EsqInfIzq"></td>
		    <td class="RepInf"></td>
		    <td class="EsqInfDer"></td>
	    </tr>
	<!--</TBODY>-->
	</table> 
    </center>
          <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type = "hidden" runat = "server"/>
        
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

    <script type="text/javascript" language="javascript">
        var CambiarPagina = "";
        var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
        Disparador(<%=this.hidDisparador.Value %>); 
          
        GetTabIndexes();
    </script>
    <%--Agregado--%>
   
</asp:Content>
