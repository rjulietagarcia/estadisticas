<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.6(Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_6.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_6.AGD_911_6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


   <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
   <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
    <script type="text/javascript">
        var _enter=true;
    </script>
  <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 8;
        MaxRow = 23;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
<div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style=" padding-left:300px;">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
   
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_6',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_6',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_6',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_6',true)"><a href="#" title=""><span>3�</span></a></li>
        <li onclick="openPage('AGT_911_6',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_6',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_6',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
   <div class="balloonstyle" id="tooltipayuda">
    <p>Favor de ingresar la estad�stica de alumnos con capacidades y aptitudes sobresalientes, y alumnos que fueron atendidos por la Unidad de Servicios de Apoyo a la Educaci�n Regular (USAER).</p>
    <p>El resto de las variables son llenadas autom�ticamente.</p>
    <p>En caso de existir inconsistencias en los datos deber� realizar las modificaciones en el sistema de Control Escolar.</p>
    <p>En esta pantalla est�n disponibles los links para acceder al Control Escolar y realizar correcciones en los datos de los alumnos, si utiliza esta opci�n, una vez realizados los cambios, deber� regresar a la estad�stica y presionar clic en el bot�n de Actualizar Datos.</p>
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>   
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td style="vertical-align: top">
                    <table style="width: 366px">
                    <tr>
                        <td colspan="2" style="height: 44px; text-align: left;">
                            <asp:Label ID="lblTit2" runat="server" Text="2. De la existencia total, anote el n�mero de alumnos de nacionalidad extranjera, desglos�ndolo por sexo." CssClass="lblRojo"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table >
                                <tr>
                                    <td style="  text-align: left; width: 118px;">
                                    </td>
                                    <td  >
                                        <asp:Label ID="lblHombres2" runat="server" Text="HOMBRES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:Label ID="lblMujeres2" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:Label ID="lblTotal2" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;  width: 118px;">
                                        <asp:Label ID="lblEEUU" runat="server" Text="ESTADOS UNIDOS" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV302" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10101" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV303" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10102" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV304" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10103" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;  width: 118px;">
                                        <asp:Label ID="lblCanada" runat="server" Text="CANAD�" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV305" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10201" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV306" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10202" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV307" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10203" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;  width: 118px;">
                                        <asp:Label ID="lblCA" runat="server" Text="CENTROAM�RICA Y EL CARIBE" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV308" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10301" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV309" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10302" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV310" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10303" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;  width: 118px;">
                                        <asp:Label ID="lblSA" runat="server" Text="SUDAM�RICA" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV311" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10401" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV312" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10402" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV313" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10403" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;  width: 118px;">
                                        <asp:Label ID="lblAfrica" runat="server" Text="�FRICA" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV314" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10501" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV315" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10502" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV316" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10503" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;  width: 118px;">
                                        <asp:Label ID="lblAsia" runat="server" Text="ASIA" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV317" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10601" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV318" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10602" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV319" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10603" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;  width: 118px;">
                                        <asp:Label ID="lblEuropa" runat="server" Text="EUROPA" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV320" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10701" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV321" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10702" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV322" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10703" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left;  width: 118px;">
                                        <asp:Label ID="lblOceania" runat="server" Text="OCEAN�A" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV323" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10801" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV324" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10802" MaxLength="3"></asp:TextBox></td>
                                    <td  >
                                        <asp:TextBox ID="txtV325" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10803" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="  text-align: left; width: 118px;">
                                    </td>
                                    <td  >
                                    </td>
                                    <td  >
                                        <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                    <td  >
                                        <asp:TextBox ID="txtV326" runat="server" Columns="3" CssClass="lblNegro" TabIndex="10901" MaxLength="3"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
                <td style="vertical-align: top">
                    <table style="width: 366px">
                        <tr>
                            <td colspan="2" style="text-align: left; height: 44px;">
                                <asp:Label ID="lblTit4" runat="server" Text="4. De la existencia total, anote el n�mero de alumnos con discapacidad, desglos�ndolo por sexo." CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 100px">
                                <center>
                                <table style="width: 166px; text-align:center">
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblHombres4" runat="server" Text="HOMBRES" CssClass="lblGrisTit"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV330" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20401" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblMujeres4" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV331" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20501" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblTotal4" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV332" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20601" MaxLength="4"></asp:TextBox></td>
                                    </tr>
                                </table>
                                </center>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 366px">
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lblTit5" runat="server" Text="5. De la existencia total, anote el n�mero de alumnos con capacidades y aptitudes sobresalientes, desglos�ndolo por sexo." CssClass="lblRojo"></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                            <table style="width: 166px">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblHombres5" runat="server" Text="HOMBRES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV333" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20701" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblMujeres5" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV334" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20801" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblTotal5" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV335" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20901" MaxLength="4"></asp:TextBox></td>
                                </tr>
                            </table>
                            </center>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top"><table style="width: 366px">
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lblTit3" runat="server" CssClass="lblRojo" Text="3. De la existencia total, anote el n�mero de alumnos ind�genas, desglos�ndolo por sexo."></asp:Label><br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <center>
                            <table style="width: 166px">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV327" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20101" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblMujeres3" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV328" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20201" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblTotal3" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV329" runat="server" Columns="4" CssClass="lblNegro" TabIndex="20301" MaxLength="4"></asp:TextBox></td>
                                </tr>
                            </table>
                            </center>
                        </td>
                    </tr>
                </table>
                </td>
                <td style="vertical-align: top">
                <table style="width: 366px">
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lblTit6" runat="server" Text="6. De la existencia total, anote el n�mero de alumnos que fueron atendidos por la Unidad de Servicios de Apoyo a la Educaci�n Regular (USAER), desglos�ndolo por sexo." CssClass="lblRojo"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                            <table style="width: 166px">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblHombres6" runat="server" Text="HOMBRES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV336" runat="server" Columns="4" CssClass="lblNegro" TabIndex="21001" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblMujeres6" runat="server" Text="MUJERES" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV337" runat="server" Columns="4" CssClass="lblNegro" TabIndex="21101" MaxLength="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblTotal6" runat="server" Text="TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV338" runat="server" Columns="4" CssClass="lblNegro" TabIndex="21201" MaxLength="4"></asp:TextBox></td>
                                </tr>
                            </table>
                            </center>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" rowspan="1" style="vertical-align: top">
                    <asp:Label ID="lblTit7" runat="server" CssClass="lblRojo" Text="7. Anote el n�mero de alumnos que reprobaron asignaturas, desglos�ndolo seg�n el n�mero de asignaturas reprobadas y el grado."
                        Width="602px"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" rowspan="1" style="vertical-align: top">
                    <asp:Label ID="lblTit71" runat="server" CssClass="lblNegro" Font-Bold="True" Text="Nota:"></asp:Label>
                    <asp:Label ID="lblTit711" runat="server" CssClass="lblNegro" Font-Bold="False" Text="Verifique por grado, que el total de alumnos que reprobaron asignaturas m�s el total de aprobados sea igual a la existencia." Width="567px"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" rowspan="1" style="vertical-align: top">
                   
                    <center>
                    <asp:Label ID="lblTit7111" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="ASIGNATURAS REPROBADAS"></asp:Label>
                        </center></td>
            </tr>
            <tr>
                <td colspan="2" rowspan="1" style="vertical-align: top">
                            <center>
                            <table style="width: 374px">
                                <tr>
                                    <td style="text-align: left; height: 34px;">
                                        </td>
                                    <td style="height: 34px">
                                        <asp:Label ID="lbl7_1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="1"></asp:Label></td>
                                    <td style="height: 34px">
                                        <asp:Label ID="lbl7_2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="2"></asp:Label></td>
                                    <td style="height: 34px">
                                        <asp:Label ID="lbl7_3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="3"></asp:Label></td>
                                    <td style="height: 34px">
                                        <asp:Label ID="lbl7_4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="4"></asp:Label></td>
                                    <td style="height: 34px">
                                        <asp:Label ID="lbl7_5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="5"></asp:Label></td>
                                    <td style="height: 34px">
                                        <asp:Label ID="lbl7_6" runat="server" CssClass="lblNegro" Font-Bold="True" Text="6 y m�s"></asp:Label></td>
                                    <td style="height: 34px">
                                        <asp:Label ID="lbl7_T" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblPrimero" runat="server" CssClass="lblGrisTit" Text="PRIMERO"></asp:Label></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV339" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30101"></asp:TextBox></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV340" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30102"></asp:TextBox></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV341" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30103"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV342" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30104"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV343" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30105"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV344" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30106"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV345" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30107"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                       <center>
                                        <asp:Label ID="lblSegundo" runat="server" CssClass="lblGrisTit" Text="SEGUNDO"></asp:Label>
                                        </center></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV346" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30201"></asp:TextBox></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV347" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30202"></asp:TextBox></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV348" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30203"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV349" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30204"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV350" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30205"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV351" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30206"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV352" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30207"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <center>
                                        <asp:Label ID="lblTercero" runat="server" CssClass="lblGrisTit" Text="TERCERO"></asp:Label>
                                     </center>
                                     </td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV353" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30301"></asp:TextBox></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV354" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30302"></asp:TextBox></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV355" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30303"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV356" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30304"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV357" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30305"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV358" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30306"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV359" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30307"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblTotal7" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"></asp:Label></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV360" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30401"></asp:TextBox></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV361" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30402"></asp:TextBox></td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="txtV362" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30403"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV363" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30404"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV364" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30405"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV365" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30406"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV366" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                            TabIndex="30407"></asp:TextBox></td>
                                </tr>
                            </table>
                            </center>
                </td>
            </tr>
            <tr>
                <td colspan="2" rowspan="1" style="vertical-align: top; height: 36px">
                    <asp:Label ID="lblTit8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8. Anote el nombre de las asignaturas y el n�mero de alumnos, por grado, donde haya mayor �ndice de reprobaci�n (de mayor a menor frecuencia)." Width="630px"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" rowspan="1" style="vertical-align: top">
                    <center>
                    <table >
                        <tr>
                            <td style="width: 316px">
                                <table >
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="lblPrimero8" runat="server" CssClass="lblNegro" Font-Bold="True" Text="P R I M E R O"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAsignatura1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ASIGNATURA"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblAlumnos1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ALUMNOS"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtV367" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="30601"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV368" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="30602" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV369" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="30603"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtV370" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="30701"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV371" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="30702" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV372" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="30703"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtV373" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="30801"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV374" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="30802" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV375" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="30803"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtV376" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="30901"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV377" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="30902" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV378" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="30903"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtV379" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31001"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV380" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31002" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV381" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31003"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="lblSegundo8" runat="server" CssClass="lblNegro" Font-Bold="True" Text="S E G U N D O"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                        </td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblAsignatura2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ASIGNATURA"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblAlumnos2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ALUMNOS"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV382" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31101"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV383" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31102" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV384" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31103"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV385" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31201"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV386" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31202" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV387" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31203"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV388" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31301"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV389" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31302" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV390" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31303"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV391" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31401"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV392" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31402" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV393" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31403"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV394" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31501"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV395" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31502" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV396" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31503"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                        </td>
                                        <td style="text-align: left">
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: center">
                                            <asp:Label ID="lblTercero8" runat="server" CssClass="lblNegro" Font-Bold="True" Text="T E R C E R O"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                        </td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblAsignatura3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ASIGNATURA"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblAlumnos3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ALUMNOS"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV397" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31601"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV398" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31602" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV399" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31603"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV400" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31701"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV401" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31702" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV402" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31703"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV403" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31801"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV404" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31802" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV405" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31803"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV406" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="31901"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV407" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="31902" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV408" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="31903"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV409" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                                TabIndex="32001"></asp:TextBox></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="txtV410" runat="server" Columns="30" CssClass="lblNegro"
                                                MaxLength="30" TabIndex="32002" Width="156px"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV411" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="32003"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </center>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                </td>
                <td rowspan="1" style="vertical-align: top">
                </td>
            </tr>
        </table>
                 <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGT_911_6',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_6',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal_911_6',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_6',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>

        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
         <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
       
        
     
   <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
//                function ValidaTexto(obj){
//                    if(obj.value == ''){
//                       obj.value = '_';
//                    }
//                }    
//                function OpenPageCharged(ventana,IR){
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV368'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV371'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV374'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV377'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV380'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV383'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV386'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV389'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV392'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV395'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV398'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV401'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV404'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV407'));
//                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV410'));
//                    openPage(ventana,IR);
//                    
//                } 
 		Disparador(<%=hidDisparador.Value %>);
        GetTabIndexes();  
        </script> 
  
  
    
</asp:Content>

