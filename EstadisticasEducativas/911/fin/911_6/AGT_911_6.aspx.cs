using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;

using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.fin._911_6
{
    public partial class AGT_911_6 : System.Web.UI.Page, ICallbackEventHandler
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Valida numeros
                txtV220.Attributes["onkeypress"] = "return Num(event)";
                txtV221.Attributes["onkeypress"] = "return Num(event)";
                txtV222.Attributes["onkeypress"] = "return Num(event)";
                txtV223.Attributes["onkeypress"] = "return Num(event)";
                txtV224.Attributes["onkeypress"] = "return Num(event)";
                txtV225.Attributes["onkeypress"] = "return Num(event)";
                txtV226.Attributes["onkeypress"] = "return Num(event)";
                txtV227.Attributes["onkeypress"] = "return Num(event)";
                txtV228.Attributes["onkeypress"] = "return Num(event)";
                txtV229.Attributes["onkeypress"] = "return Num(event)";
                txtV230.Attributes["onkeypress"] = "return Num(event)";
                txtV231.Attributes["onkeypress"] = "return Num(event)";
                txtV232.Attributes["onkeypress"] = "return Num(event)";
                txtV233.Attributes["onkeypress"] = "return Num(event)";
                txtV234.Attributes["onkeypress"] = "return Num(event)";
                txtV235.Attributes["onkeypress"] = "return Num(event)";
                txtV236.Attributes["onkeypress"] = "return Num(event)";
                txtV237.Attributes["onkeypress"] = "return Num(event)";
                txtV238.Attributes["onkeypress"] = "return Num(event)";
                txtV239.Attributes["onkeypress"] = "return Num(event)";
                txtV240.Attributes["onkeypress"] = "return Num(event)";
                txtV241.Attributes["onkeypress"] = "return Num(event)";
                txtV242.Attributes["onkeypress"] = "return Num(event)";
                txtV243.Attributes["onkeypress"] = "return Num(event)";
                txtV244.Attributes["onkeypress"] = "return Num(event)";
                txtV245.Attributes["onkeypress"] = "return Num(event)";
                txtV246.Attributes["onkeypress"] = "return Num(event)";
                txtV247.Attributes["onkeypress"] = "return Num(event)";
                txtV248.Attributes["onkeypress"] = "return Num(event)";
                txtV249.Attributes["onkeypress"] = "return Num(event)";
                txtV250.Attributes["onkeypress"] = "return Num(event)";
                txtV251.Attributes["onkeypress"] = "return Num(event)";
                txtV252.Attributes["onkeypress"] = "return Num(event)";
                txtV253.Attributes["onkeypress"] = "return Num(event)";
                txtV254.Attributes["onkeypress"] = "return Num(event)";
                txtV255.Attributes["onkeypress"] = "return Num(event)";
                txtV256.Attributes["onkeypress"] = "return Num(event)";
                txtV257.Attributes["onkeypress"] = "return Num(event)";
                txtV258.Attributes["onkeypress"] = "return Num(event)";
                txtV259.Attributes["onkeypress"] = "return Num(event)";
                txtV260.Attributes["onkeypress"] = "return Num(event)";
                txtV261.Attributes["onkeypress"] = "return Num(event)";
                txtV262.Attributes["onkeypress"] = "return Num(event)";
                txtV263.Attributes["onkeypress"] = "return Num(event)";
                txtV264.Attributes["onkeypress"] = "return Num(event)";
                txtV265.Attributes["onkeypress"] = "return Num(event)";
                txtV266.Attributes["onkeypress"] = "return Num(event)";
                txtV267.Attributes["onkeypress"] = "return Num(event)";
                txtV268.Attributes["onkeypress"] = "return Num(event)";
                txtV269.Attributes["onkeypress"] = "return Num(event)";
                txtV270.Attributes["onkeypress"] = "return Num(event)";
                txtV271.Attributes["onkeypress"] = "return Num(event)";
                txtV272.Attributes["onkeypress"] = "return Num(event)";
                txtV273.Attributes["onkeypress"] = "return Num(event)";
                txtV274.Attributes["onkeypress"] = "return Num(event)";
                txtV275.Attributes["onkeypress"] = "return Num(event)";
                txtV276.Attributes["onkeypress"] = "return Num(event)";
                txtV277.Attributes["onkeypress"] = "return Num(event)";
                txtV278.Attributes["onkeypress"] = "return Num(event)";
                txtV279.Attributes["onkeypress"] = "return Num(event)";
                txtV280.Attributes["onkeypress"] = "return Num(event)";
                txtV281.Attributes["onkeypress"] = "return Num(event)";
                txtV282.Attributes["onkeypress"] = "return Num(event)";
                txtV283.Attributes["onkeypress"] = "return Num(event)";
                txtV284.Attributes["onkeypress"] = "return Num(event)";
                txtV285.Attributes["onkeypress"] = "return Num(event)";
                txtV286.Attributes["onkeypress"] = "return Num(event)";
                txtV287.Attributes["onkeypress"] = "return Num(event)";
                txtV288.Attributes["onkeypress"] = "return Num(event)";
                txtV289.Attributes["onkeypress"] = "return Num(event)";
                txtV290.Attributes["onkeypress"] = "return Num(event)";
                txtV291.Attributes["onkeypress"] = "return Num(event)";
                txtV292.Attributes["onkeypress"] = "return Num(event)";
                txtV293.Attributes["onkeypress"] = "return Num(event)";
                txtV294.Attributes["onkeypress"] = "return Num(event)";
                txtV295.Attributes["onkeypress"] = "return Num(event)";
                txtV296.Attributes["onkeypress"] = "return Num(event)";
                txtV297.Attributes["onkeypress"] = "return Num(event)";
                txtV298.Attributes["onkeypress"] = "return Num(event)";
                txtV299.Attributes["onkeypress"] = "return Num(event)";
                txtV300.Attributes["onkeypress"] = "return Num(event)";
                txtV301.Attributes["onkeypress"] = "return Num(event)";
                #endregion

                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
                Class911.ActualizaEncabezado(this.Page, controlDP);
                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion


                //Cargar los datos para la carga inicial
                if (hidListaTxtBoxs.Value == "")
                    hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);

                Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

                #region Escribir JS Call Back
                String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
                String callbackScript_Variables;
                callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
                #endregion

                this.hdnCct.Value = controlDP.Clave + " - \"" + controlDP.Nombre + "\"";
                this.hidDisparador.Value = Class911.TiempoAutoGuardado();

               // lnkModificacionDatosAl.NavigateUrl = Class911.Liga_ModificacionDatosAlumno(controlDP);
                //lnkControlEscolarABC.NavigateUrl = Class911.Liga_ControlEscolar(controlDP);
              //  lnkCalificaciones.NavigateUrl = Class911.Liga_Calificaciones(controlDP);

                if (controlDP.Estatus == 0)
                    pnlOficializado.Visible = false;
                else
                    pnlOficializado.Visible = true;
            }

        }


        // llamadas por detras(Call Backs) **********************

        protected string returnValue;

        public void RaiseCallbackEvent(String eventArgument)
        {
            returnValue = Class911.RaiseCallbackEvent(eventArgument, 1, HttpContext.Current);
        }
        public string GetCallbackResult()
        {
            return returnValue;
        }

        protected void lnkRefrescar_Click(object sender, EventArgs e)
        {
            ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);
            // Actualiza Info
            Class911.CargaInicialCuestionario(controlDP, 1);
            // Recarga datos
            hidListaTxtBoxs.Value = Class911.ListaCajas(this, controlDP);
            Class911.LlenarDatosDB11(this.Page, controlDP, 1, 0, hidListaTxtBoxs.Value);

            #region Volver a escribir JS Call Back
            String cbReference_Variables = Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveServerData_Variables", "context", "ErrorServidor", false);
            String callbackScript_Variables;
            callbackScript_Variables = "function CallServer_Variables(arg, context)" + "{ " + cbReference_Variables + ";}";
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "CallServer_Variables", callbackScript_Variables, true);
            #endregion

        }

        //*********************************


    }
}
