<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AGT_911_122.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_122.AGT_911_122" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
   
    <script type="text/javascript">
        var _enter=true;
    </script>
 
 
     <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 13;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header" style="text-align:center;">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PRIMARIA IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_122',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_122',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_122',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_122',true)"><a href="#" title=""><span>3�</span></a></li>
        <li onclick="openPage('AG4_911_122',true)"><a href="#" title=""><span>4�</span></a></li>
        <li onclick="openPage('AG5_911_122',true)"><a href="#" title=""><span>5�</span></a></li>
        <li onclick="openPage('AG6_911_122',true)"><a href="#" title=""><span>6�</span></a></li>
        <li onclick="openPage('AGT_911_122',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li>
        <%--<li onclick="openPage('AGD_911_122')"><a href="#" title=""><span>DESGLOSE</span></a></li>--%>
        <li onclick="openPage('Personal_911_122',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

         <table style="text-align:center">
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbl1" runat="server" Text="TOTAL" CssClass="lblRojo" Font-Size="25px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl_6" runat="server" Text="Menos de 6 a�os" CssClass="lblRojo" Width="48px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="Label1" runat="server" Text="6 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl7" runat="server" Text="7 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl8" runat="server" Text="8 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td style="width: 81px">
                                <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left; height: 32px;">
                                <asp:Label ID="lblInscripcionH" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV523" runat="server" Columns="3" ReadOnly="True" MaxLength="3" TabIndex="10101" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV524" runat="server" Columns="3" ReadOnly="True" TabIndex="10102" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV525" runat="server" Columns="3" ReadOnly="True" TabIndex="10103" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV526" runat="server" Columns="3" ReadOnly="True" TabIndex="10104" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV527" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10105" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV528" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10106" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV529" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10107" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV530" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10108" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV531" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10109" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV532" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10110" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px; height: 32px;">
                                <asp:TextBox ID="txtV533" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10111" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px; height: 32px;">
                                <asp:TextBox ID="txtV534" runat="server" Columns="4" ReadOnly="True" TabIndex="10112" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaH" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV535" runat="server" Columns="3" ReadOnly="True" TabIndex="10201" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV536" runat="server" Columns="3" ReadOnly="True" TabIndex="10202" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV537" runat="server" Columns="3" ReadOnly="True" TabIndex="10203" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV538" runat="server" Columns="3" ReadOnly="True" TabIndex="10204" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV539" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10205" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV540" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10206" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV541" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10207" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV542" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10208" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV543" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10209" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV544" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10210" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV545" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10211" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV546" runat="server" Columns="4" ReadOnly="True" TabIndex="10212" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosH" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV547" runat="server" Columns="3" ReadOnly="True" TabIndex="10301" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>                        
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV548" runat="server" Columns="3" ReadOnly="True" TabIndex="10302" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV549" runat="server" Columns="3" ReadOnly="True" TabIndex="10303" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV550" runat="server" Columns="3" ReadOnly="True" TabIndex="10304" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV551" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10305" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV552" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10306" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV553" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10307" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV554" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10308" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV555" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10309" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV556" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10310" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV557" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10311" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV558" runat="server" Columns="4" ReadOnly="True" TabIndex="10312" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                         </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td>
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionM" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV559" runat="server" Columns="3" ReadOnly="True" TabIndex="10401" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV560" runat="server" Columns="3" ReadOnly="True" TabIndex="10402" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV561" runat="server" Columns="3" ReadOnly="True" TabIndex="10403" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV562" runat="server" Columns="3" ReadOnly="True" TabIndex="10404" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV563" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10405" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV564" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10406" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV565" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10407" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV566" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10408" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV567" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10409" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV568" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10410" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV569" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10411" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV570" runat="server" Columns="4" ReadOnly="True" TabIndex="10412" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaM" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV571" runat="server" Columns="3" ReadOnly="True" TabIndex="10501" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV572" runat="server" Columns="3" ReadOnly="True" TabIndex="10502" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV573" runat="server" Columns="3" ReadOnly="True" TabIndex="10503" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV574" runat="server" Columns="3" ReadOnly="True" TabIndex="10504" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV575" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10505" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV576" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10506" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV577" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10507" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV578" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10508" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV579" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10509" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV580" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10510" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV581" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10511" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV582" runat="server" Columns="4" ReadOnly="True" TabIndex="10512" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosM" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV583" runat="server" Columns="3" ReadOnly="True" TabIndex="10601" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV584" runat="server" Columns="3" ReadOnly="True" TabIndex="10602" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV585" runat="server" Columns="3" ReadOnly="True" TabIndex="10603" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV586" runat="server" Columns="3" ReadOnly="True" TabIndex="10604" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV587" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10605" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV588" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10606" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV589" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10607" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV590" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10608" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV591" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10609" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV592" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10610" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV593" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10611" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV594" runat="server" Columns="4" ReadOnly="True" TabIndex="10612" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                    </table><br />
                </td>
            </tr>
            <tr>
                    <td style="height: 102px">
                    <table>
                        <tr>
                            <td rowspan="3" style="width: 75px">
                                <asp:Label ID="lblSubtotal" runat="server" Text="SUBTOTAL" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="INSCRIPCI�N TOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV595" runat="server" Columns="3" ReadOnly="True" TabIndex="10701" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV596" runat="server" Columns="3" ReadOnly="True" TabIndex="10702" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV597" runat="server" Columns="3" ReadOnly="True" TabIndex="10703" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV598" runat="server" Columns="3" ReadOnly="True" TabIndex="10704" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV599" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10705" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV600" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10706" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV601" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10707" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV602" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10708" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV603" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10709" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV604" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10710" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV605" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10711" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV606" runat="server" Columns="4" ReadOnly="True" TabIndex="10712" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaS" runat="server" Text="EXISTENCIA" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV607" runat="server" Columns="3" ReadOnly="True" TabIndex="10801" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV608" runat="server" Columns="3" ReadOnly="True" TabIndex="10802" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV609" runat="server" Columns="3" ReadOnly="True" TabIndex="10803" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV610" runat="server" Columns="3" ReadOnly="True" TabIndex="10804" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV611" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10805" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV612" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10806" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV613" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10807" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV614" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10808" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV615" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10809" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV616" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10810" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV617" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10811" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV618" runat="server" Columns="4" ReadOnly="True" TabIndex="10812" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblAprobadosS" runat="server" Text="APROBADOS" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV619" runat="server" Columns="3" ReadOnly="True" TabIndex="10901" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox>
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV620" runat="server" Columns="3" ReadOnly="True" TabIndex="10902" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV621" runat="server" Columns="3" ReadOnly="True" TabIndex="10903" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV622" runat="server" Columns="3" ReadOnly="True" TabIndex="10904" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV623" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10905" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV624" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10906" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV625" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10907" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV626" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10908" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV627" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10909" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV628" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10910" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV629" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10911" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV630" runat="server" Columns="4" ReadOnly="True" TabIndex="10912" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px; height: 16px">
                            </td>
                            <td style="width: 132px; height: 16px">
                                &nbsp;</td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                            <td style="width: 81px; height: 16px">
                            </td>
                            <td style="width: 67px; height: 16px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                            </td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblGrupos" runat="server" CssClass="lblGrisTit" Text="GRUPOS"></asp:Label></td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 67px">
                            </td>
                            <td style="width: 81px">
                            </td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV631" runat="server" Columns="2" ReadOnly="True" TabIndex="11101" CssClass="lblNegro" MaxLength="2">0</asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <!-- Tabla agregada -->
            <tr>
                <td>
                <br />
                        <table>
                            <tr>
                                <td style="text-align: left; height: 19px;" colspan="3" width="480">
                                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Text="3. Escriba el nombre de la lengua materna principal que se habla en la localidad."
                                        Width="480px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: left" width="160">
                                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Text="LENGUA MATERNA"></asp:Label></td>
                                <td style="width: 67px; text-align: left;" colspan="2">
                                    <asp:DropDownList ID="optV632" runat="server" TabIndex="11301" Width="248px" onchange="OnChange(this)" >
                                    <asp:ListItem Value="Seleccione una lengua materna"></asp:ListItem>
                                    <asp:ListItem Value="1 AKATEKO VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="2 AMUZGO V FAMILIA OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="3 AWAKATEKO VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="4 AYAPANECO IX FAM. MIXE-ZOQUE"></asp:ListItem>
                                    <asp:ListItem Value="5 CORA II FAMILIA YUTO-NAHUA "></asp:ListItem>
                                    <asp:ListItem Value="6 CUCUPA III FAM COCHIMI-YUMANA "></asp:ListItem>
                                    <asp:ListItem Value="7 CUICATECO V FAM OTO-MANGUE "></asp:ListItem>
                                    <asp:ListItem Value="8 CHATINO V FAMILIA OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="9 CHICHIMECO JONAZ V FAM OTO-M"></asp:ListItem>
                                    <asp:ListItem Value="10 CHINANTECO V FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="11 CHOCHOLTECO V FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="12 CHONTAL DE OAXACA X FAM CH-O"></asp:ListItem>
                                    <asp:ListItem Value="13 CHONTAL DE TABASCO VI FAM MAYA"></asp:ListItem>
                                    <asp:ListItem Value="14 CHUJ VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="15 CHOL VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="16 GUARIJIO II FAM YUTO-NAHUA "></asp:ListItem>
                                    <asp:ListItem Value="17 HUASTECO VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="18 HUAVE XI FAMILIA HUAVE "></asp:ListItem>
                                    <asp:ListItem Value="19 HUICHOL II FAM YUTO-NAHUA"></asp:ListItem>
                                    <asp:ListItem Value="20 IXCATECO V FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="21 IXIL VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="22 JAKALTEKO VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="23 KAQCHIKEL VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="24 KICKAPOO I FAMILIA ALGICA"></asp:ListItem>
                                    <asp:ListItem Value="25 KILIWA III FAM COCHIMI-YUMANA "></asp:ListItem>
                                    <asp:ListItem Value="26 KUMIAI III FAM COCHIMI-YUMANA "></asp:ListItem>
                                    <asp:ListItem Value="27 KUAHL III FAM COCHIMI-YUMANA"></asp:ListItem>
                                    <asp:ListItem Value="28 KICHE VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="29 LACANDON VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="30 MAM VI FAMILIA MAYA "></asp:ListItem>
                                    <asp:ListItem Value="31 MATLATZINCA V FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="32 MAYA VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="33 MAYO II FAMILIA YUTO-NAHUA "></asp:ListItem>
                                    <asp:ListItem Value="34 MAZAHUA V FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="35 MAZATECO V FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="36 MIXE IX FAMILIA MIXE-ZOQUE "></asp:ListItem>
                                    <asp:ListItem Value="37 MIXTECO V FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="38 NAHUATL II FAM YUTO-NAHUA"></asp:ListItem>
                                    <asp:ListItem Value="39 OLUTECO IX FAM MIXE-ZOQUE"></asp:ListItem>
                                    <asp:ListItem Value="40 OTOMI V FAM OTO-MANGUE "></asp:ListItem>
                                    <asp:ListItem Value="41 PAIPAI III FAM COCHIMI-YUMANA "></asp:ListItem>
                                    <asp:ListItem Value="42 PAME V FAMILIA OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="43 PAPAGO II FAMILIA YUTO-NAHUA"></asp:ListItem>
                                    <asp:ListItem Value="44 PIMA II FAMILIA YUTO-NAHUA "></asp:ListItem>
                                    <asp:ListItem Value="45 POPOLOCA V FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="46 POPOLUCA DE LA SIERRA IX FAM"></asp:ListItem>
                                    <asp:ListItem Value="47 QATOK VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="48 QANJOBAL VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="49 QEQCHIVI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="50 SAYULTECO IX FAM MIXE-ZOQUE"></asp:ListItem>
                                    <asp:ListItem Value="51 SERI IV FAMILIA SERI"></asp:ListItem>
                                    <asp:ListItem Value="52 TARAHUMARA II FAM YUTO-NAHUA"></asp:ListItem>
                                    <asp:ListItem Value="53 TARASCO VIII FAMILIA TARASCA"></asp:ListItem>
                                    <asp:ListItem Value="54 TEKO VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="55 TEPEHUA VII FAM TOTONACO-TEPE "></asp:ListItem>
                                    <asp:ListItem Value="56 TEPEHUANO DEL NORTE II FAM Y-N"></asp:ListItem>
                                    <asp:ListItem Value="57 TEPEHUANO DEL SUR II FAM Y-NA "></asp:ListItem>
                                    <asp:ListItem Value="58 TEXISTEPEQUE�O IX FAM MIXE-Z"></asp:ListItem>
                                    <asp:ListItem Value="59 TOJOLABAL VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="60 TOTONACO VII FAM TOTONACO-T"></asp:ListItem>
                                    <asp:ListItem Value="61 TRIQUI VI FAM OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="62 TLAHUICA V FMILIA OTO-MANGUE"></asp:ListItem>
                                    <asp:ListItem Value="63 TLAPANECO V FAMILIA OTO-M"></asp:ListItem>
                                    <asp:ListItem Value="64 TSELTAL VI FAMILIA MAYA"></asp:ListItem>
                                    <asp:ListItem Value="65 TSOTSIL VI FAMILIAMAYA "></asp:ListItem>
                                    <asp:ListItem Value="66 YAQUI II FAMILIA YUTO-NAHUA"></asp:ListItem>
                                    <asp:ListItem Value="67 ZAPOTECO V FAMILIA OTO-MANGUE "></asp:ListItem>
                                    <asp:ListItem Value="68 ZOQUE IX FAMILIA OTO-MANGUE"></asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtV632" runat="server" Columns="1" CssClass="lblNegro" MaxLength="30"
                                        ReadOnly="True"  Width="1px" style="visibility:hidden;">0</asp:TextBox>
                                    </td>
                            </tr>
                            <tr>
                                <td style="text-align: left" width="160">
                                </td>
                                <td colspan="2" style="width: 67px; text-align: center">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="width: 132px; height: 16px; text-align: left">
                                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Text="4. De la existencia total, escriba el n�mero de alumnos con discapacidad, desglos�ndolo por sexo."
                                        Width="480px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 72px; height: 16px; text-align: center;">
                                    <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                                <td style="height: 16px; text-align: center;" width="160">
                                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                                <td style="height: 16px; text-align: center;" width="160">
                                    <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Text="TOTAL"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 72px; text-align: center">
                                    <asp:TextBox ID="txtV633" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="20301">0</asp:TextBox></td>
                                <td style="text-align: center;" width="160">
                                    <asp:TextBox ID="txtV634" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="20302">0</asp:TextBox></td>
                                <td style="text-align: center;" width="160">
                                    <asp:TextBox ID="txtV635" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="20303">0</asp:TextBox></td>
                            </tr>
                            
                            <!-- Pregunta 5 -->
                           <%--<%-- <tr>
                                <td colspan="3" style="width: 132px; height: 16px; text-align: left">
                                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Text="5. Escriba el n�mero de directivos, docentes y promotores con grupo, por grado." Width="480px"></asp:Label>
                                    <br />
                                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Text="IMPORTANTE: Si un profesor atiende m�s de un grado, an�telo en el rubro correspondiente; el total debe coincidir con la suma de
                                    directivo con grupo m�s personal docente m�s promotores de la pregunta 1 de la secci�n II. PERSONAL POR FUNCI�N." Width="480px"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr style="text-align: left;">
                            <td></td>
                            <td><asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Text="PRIMERO"></asp:Label></td>
                            <td><asp:TextBox ID="txtV636" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="20501">0</asp:TextBox></td>
                            </tr>
                            <tr style="text-align: left;">
                            <td></td>
                            <td><asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Text="SEGUNDO"></asp:Label></td>
                            <td><asp:TextBox ID="txtV637" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="20601">0</asp:TextBox></td>
                            </tr>
                            <tr style="text-align: left;">
                            <td></td>
                            <td><asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Text="TERCERO"></asp:Label></td>
                            <td><asp:TextBox ID="txtV638" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="20701">0</asp:TextBox></td>
                            </tr>
                            <tr style="text-align: left;">
                            <td></td>
                            <td><asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Text="CUARTO"></asp:Label></td>
                            <td><asp:TextBox ID="txtV639" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="20801">0</asp:TextBox></td>
                            </tr>
                            <tr style="text-align: left;">
                            <td></td>
                            <td><asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Text="QUINTO"></asp:Label></td>
                            <td><asp:TextBox ID="txtV640" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="20901">0</asp:TextBox></td>
                            </tr>
                            <tr style="text-align: left;">
                            <td></td>
                            <td><asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Text="SEXTO"></asp:Label></td>
                            <td><asp:TextBox ID="txtV641" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="21001">0</asp:TextBox></td>
                            </tr>
                            <tr style="text-align: left;">
                            <td></td>
                            <td><asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Text="MAS DE UN GRADO"></asp:Label></td>
                            <td><asp:TextBox ID="txtV642" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="21101">0</asp:TextBox></td>
                            </tr>
                            <tr style="text-align: left;">
                            <td></td>
                            <td><asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Text="TOTAL"></asp:Label></td>
                            <td><asp:TextBox ID="txtV643" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                        ReadOnly="True" TabIndex="21201">0</asp:TextBox></td>
                            </tr>--%>
                            <!-- Fin pregunta 5 -->
                        </table>
                </td>
            </tr>
            <!-- Fin de la tabla agregada -->
            
        </table>
        <hr />
        
        
        
        <!-- Empiezan botones de navegaci�n -->
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG6_911_122',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG6_911_122',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                 <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal_911_122',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_122',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        <!-- Terminan botones de navegaci�n -->
          <div class="divResultado" id="divResultado"></div> 
            <%--cierre tabla de aqui--%>
       
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
   
                  <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
             <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
    <script type="text/javascript" >  
     function OnChange(dropdown)
                {
                    var myindex  = dropdown.selectedIndex
                    var SelValue = dropdown.options[myindex].value
                     if(myindex == 0)
                    {
                    document.getElementById('ctl00_cphMainMaster_txtV632').value  = '_';
                    }else{
                    document.getElementById('ctl00_cphMainMaster_txtV632').value  = SelValue;                    
                    }
                    return true;
                }
    </script>
    <script type="text/javascript"  language="javascript">
        var CambiarPagina = "";
        var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
    </script>
    <%--Agregado--%>
    <script type="text/javascript" >
        GetTabIndexes();
    </script>
    <%--Agregado--%>
   </asp:Content> 