<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.122(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_122.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_122.Personal_911_122" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 2;
        MaxRow = 19;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header" style="text-align:center;">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PRIMARIA IND�GENA</span></td>
        </tr>
        <tr><td>
            <asp:Label ID="Label1" runat="server" Text="FIN DE CURSOS 2012-2013"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_122',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a>
        </li><li onclick="openPage('AG1_911_122',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_122',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_122',true)"><a href="#" title=""><span>3�</span></a></li>
        <li onclick="openPage('AG4_911_122',true)"><a href="#" title=""><span>4�</span></a></li>
        <li onclick="openPage('AG5_911_122',true)"><a href="#" title=""><span>5�</span></a></li>
        <li onclick="openPage('AG6_911_122',true)"><a href="#" title=""><span>6�</span></a></li>
        <li onclick="openPage('AGT_911_122',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
        <%--<li onclick="openPage('AGD_911_122')"><a href="#" title=""><span>DESGLOSE</span></a></li>--%>
        <li onclick="openPage('Personal_911_122',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="openPage('Inmueble_911_122',false)"><a href="#" title=""><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div class="balloonstyle" id="tooltipayuda">
    <p>Favor de revisar la estad�stica de personal y en caso de observar informaci�n que no se apega a la realidad, realizar las adecuaciones en el m�dulo de Personal por Funci�n.</p>
    <p>En esta pantalla est� disponible un link para acceder al m�dulo de Personal por Funci�n  y realizar correcciones en la plantilla de personal, si utiliza esta opci�n, una vez realizados los cambios, deber� regresar a la estad�stica y presionar clic en el bot�n de Actualizar Datos.</p>
    <p>Una vez que haya revisado y aprobado los datos dar clic en la opci�n SIGUIENTE para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>    
        <br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 550px">
            <tr>
                <td>
                    <asp:Label ID="lblTitulo1" runat="server" CssClass="lblRojo" Text="Anote el n�mero de directivos con grupo y docentes con grupo, por grado."></asp:Label><br />
                    <br />
                    <asp:Label ID="lblTitulo2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="IMPORTANTE:"></asp:Label>
                    <asp:Label ID="lblTitulo3" runat="server" CssClass="lblRojo" Text="Si un profesor atiende m�s de un grado, an�telo en el rubro correspondiente; el total debe coincidir con la suma  de directivo con grupo m�s personal docente de la secci�n PERSONAL POR FUNCI�N."></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 192px">
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblPrimero1" runat="server" CssClass="lblGrisTit" Text="PRIMERO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV636" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10101" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblSegundo1" runat="server" CssClass="lblGrisTit" Text="SEGUNDO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV637" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10201" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblTercero1" runat="server" CssClass="lblGrisTit" Text="TERCERO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV638" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10301" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblCuarto1" runat="server" CssClass="lblGrisTit" Text="CUARTO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV639" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10401"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblQuinto1" runat="server" CssClass="lblGrisTit" Text="QUINTO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV640" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10501"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblSexto1" runat="server" CssClass="lblGrisTit" Text="SEXTO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV641" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10601"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblMasDeUnGrado" runat="server" CssClass="lblGrisTit" Text="M�S DE UN GRADO"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV642" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10701" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblGrisTit" Text="TOTAL"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV643" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10801" MaxLength="2"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <asp:Label ID="lblTitulo" runat="server" CssClass="lblRojo" Font-Size="16px" Text="II. PERSONAL POR FUNCI�N"></asp:Label><br />
        <br />
        <table style="width: 550px">
            <tr>
                <td>
                    <asp:Label ID="lblTitulo4" runat="server" CssClass="lblRojo" Text="1. Escriba el personal seg�n la funci�n que realiza, independientemente de su nombramiento, tipo y fuente de pago. Si una persona desempe�a dos o m�s funciones, an�tela en aqu�lla a la que dedique m�s tiempo."></asp:Label><br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 411px">
                        <tr>
                            <td style="text-align: left" colspan="2">
                                <asp:Label ID="lblPerDir" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PERSONAL DIRECTIVO"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left; padding-left: 15px;">
                                <asp:Label ID="lblPerDirCG" runat="server" CssClass="lblGrisTit" Text="CON GRUPO"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV644" runat="server" Columns="2" CssClass="lblNegro" TabIndex="10901" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left; padding-left: 15px;">
                                <asp:Label ID="lblPerDirSG" runat="server" CssClass="lblGrisTit" Text="SIN GRUPO"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV645" runat="server" Columns="2" CssClass="lblNegro" TabIndex="11001" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblPerDoc" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PERSONAL DOCENTE"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV646" runat="server" Columns="3" CssClass="lblNegro" TabIndex="11101" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblPromo" runat="server" CssClass="lblNegro" Font-Bold="True"  Text="PROMOTORES"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV647" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="11201"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblPerAdmAuxServ" runat="server" CssClass="lblNegro" Font-Bold="True"  Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV648" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="11301"></asp:TextBox></td>
                        </tr> 
                        <tr>
                            <td style="width: 519px; text-align: left">
                                <asp:Label ID="lblTotalPer" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL DE PERSONAL"></asp:Label></td>
                            <td style="width: 82px; text-align: right">
                                <asp:TextBox ID="txtV649" runat="server" Columns="4" CssClass="lblNegro" TabIndex="11401" MaxLength="4"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGT_911_122',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_122',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                 <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Inmueble_911_122',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Inmueble_911_122',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
      <div class="divResultado" id="divResultado"></div> 
            <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>



        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" />
        
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
         
</asp:Content>
