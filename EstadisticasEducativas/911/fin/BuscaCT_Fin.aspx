<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasicMenu.Master" AutoEventWireup="true" CodeBehind="BuscaCT_Fin.aspx.cs" Inherits="EstadisticasEducativas._911.fin.BuscaCT_Fin" Title="Estadística 911 - Inicio" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
    </asp:ScriptManagerProxy>
    
       <%-- <table style="width: 100%">
            <tr>
                <td style="font-weight: bold; font-size: 16px; color: #ffffff; font-family: Verdana, Tahoma, Arial;
                    height: 30px; background-color: #cc0000; text-align: center">
                                            Estadísticas Educativas 911</td>
            </tr>
        </table>--%>
  
        <br />
        <br />
        <center>
        <div style="text-align:center; width:100%">
             <center>
                
           <table class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"></td>
				    <td>
				        <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">										
								<tr>
									<td class="Titulo" style="FONT-WEIGHT: bold" align="center" >
                                        Fin de Cursos 
                                    </td>
								</tr>
						</table>
						<br />
				   <asp:UpdatePanel ID="upBusqueda" runat="server">
                        <ContentTemplate> 
                        
			            <table   border="0" cellpadding="0" cellspacing="2"  >
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label ID="Label1" runat="server" CssClass="lblEtiqueta" Font-Size="14px" Text="Ciclo Escolar:"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlCiclo" runat="server" Enabled="false">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                 <td align="left" style="width: 133px">
                                    <asp:Label ID="lblEntidad" runat="server" CssClass="lblEtiqueta" Font-Size="14px" Text="Entidad:"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                   <asp:DropDownList ID="ddlEntidad" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEntidad_SelectedIndexChanged">
                                   <asp:ListItem Value="0">Seleccione un Estado</asp:ListItem>
                                   </asp:DropDownList></td>
                            </tr> 
                             <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblRegion" runat="server" Text="Región:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                   <%-- <asp:TextBox   ID="txtRegion" runat="server" Width="55px" TabIndex="1"
                                        MaxLength="12"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="True" Enabled="false" >
                                            <asp:ListItem Value="0">Seleccione una Región</asp:ListItem>
                                    </asp:DropDownList>
                                        </td>
                            </tr>
                            
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label ID="lblNivel" runat="server"  Font-Size="14px" Text="Nivel:" CssClass="lblEtiqueta" ></asp:Label>
                                    </td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlNivel" runat="server" OnSelectedIndexChanged="ddlNivel_SelectedIndexChanged">
                                    <asp:ListItem Value="0">Seleccione un Nivel</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                           
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblZona" runat="server" Text="Zona:" Width="43px" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtZona" runat="server" Width="55px" TabIndex="2"
                                        MaxLength="3"></asp:TextBox></td>
                            </tr>
                            <tr id="trNiv">
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblClave" runat="server" Text="Centro de Trabajo:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtClaveCT" runat="server" Width="110px" TabIndex="3"
                                        MaxLength="10"></asp:TextBox>
                                    <asp:DropDownList ID="ddlCentroTrabajo" runat="server" Visible="False">
                                    </asp:DropDownList></td>
                            </tr>
                        </table>   
              </ContentTemplate>
                        </asp:UpdatePanel> 
                         
                        <br />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="67px" TabIndex="4" OnClick="btnBuscar_Click"   />
                      
                    
                        
                        </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
                <div>
          
            <br />
           <asp:UpdatePanel ID="upMsg" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ddlNivel" EventName="SelectedIndexChanged" />
                </Triggers>
                <ContentTemplate> 
                    <asp:Label ID="lblMsg" runat="server"  ></asp:Label>
              </ContentTemplate>
            </asp:UpdatePanel> 
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">

            <ProgressTemplate>

            <asp:Image ID="Image1" runat="server" ImageUrl="../../tema/images/loading.gif" />
            <!-- this is an animated progress gif that will be shown while progress delay -->

            </ProgressTemplate>
            </asp:UpdateProgress>
            
            </div>
            </center>
        </div>
        </center>
                    
        <br />
 
                <center>
                  <%--Seccion de llenado del gridView--%>
                  
                <div id = "up_container" style="text-align: center">
                   <center>
                   
                 
                   
                  <asp:UpdatePanel ID="update" runat="server" >  
                     <ContentTemplate>   
                   <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" 
                        PageSize="50" CellPadding="4" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="RowCommand"
                        OnSorting="GridView1_Sorting" EmptyDataText="Busqueda sin registros">
                        <PagerSettings PageButtonCount="20" />
                        <Columns>
 
                            <asp:ButtonField CommandName="cap" HeaderText="CAP" Text="&lt;img src='../../tema/images/iconEsc.gif' alt='Captura 911' style='border:0;'&gt;" />
                            
                            <asp:BoundField DataField="Id_CT" HeaderText="Id_CT" SortExpression="Id_CT" Visible="False">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ID_CCTNT"  SortExpression="ID_CCTNT" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID_CCTNT" runat="server" Text='<%# Bind("ID_CCTNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="CveCT" HeaderText="CveCT" SortExpression="CveCT">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomCT" HeaderText="NomCT" SortExpression="NomCT">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveTurno" HeaderText="CveTurno" SortExpression="CveTurno">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomTurno" Visible="false" HeaderText="Turno" SortExpression="NomTurno">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomRegion" HeaderText="Region" SortExpression="NomRegion">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveRegion" Visible="false" HeaderText="Region" SortExpression="CveRegion">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveZona" HeaderText="Zona" SortExpression="CveZona">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Calle" Visible="false" HeaderText="Calle" SortExpression="Calle">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Numero" Visible="false" HeaderText="Numero" SortExpression="Numero">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomColonia" Visible="false" HeaderText="Colonia" SortExpression="NomColonia">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomMunicipio" Visible="false" HeaderText="Municipio" SortExpression="NomMunicipio">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                   </ContentTemplate>  
                <Triggers>  
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />  
                    <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />

                </Triggers>  
              </asp:UpdatePanel>  
              <%--FIN DEL LLENADO DEL GRIDVIEW--%>
                   </center>
                   
                </div>
               <cc1:UpdatePanelAnimationExtender ID="upae" BehaviorID="animation" runat="server" TargetControlID="update">  
                     <Animations>  
         <OnUpdating>  
             <Sequence>  
                 
                 <ScriptAction Script="var b = $find('animation'); b._originalHeight = b._element.offsetHeight;" />  
                   
                
                 <Parallel duration="0">  
                     <EnableAction AnimationTarget="btnBuscar" Enabled="false" />  
                     
                     
                    
                 </Parallel>  
                 <StyleAction Attribute="overflow" Value="hidden" />  
                   
                
                 <Parallel duration=".25" Fps="30">  
                     
                         <%--<FadeOut AnimationTarget="up_container" minimumOpacity=".2" />  --%>
                       
                      
 
                 </Parallel>  
             </Sequence>  
         </OnUpdating>  
         <OnUpdated>  
             <Sequence>  
                
                 <Parallel duration=".25" Fps="30">  
                     
                         <%--<FadeIn AnimationTarget="up_container" minimumOpacity=".2" />  --%>
                     
                       
                     
                 </Parallel>  
                   
              
                 <Parallel duration="0">  
                     
                     <EnableAction AnimationTarget="btnBuscar" Enabled="true" />
                      
                     
                     
                 </Parallel>                              
             </Sequence>  
         </OnUpdated>  
     </Animations>  
</cc1:UpdatePanelAnimationExtender>
                </center>
         
      
                
                
                
                
                
                
                
                
          <%-- <asp:UpdatePanel ID="upGridView1" runat="server" >
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="Sorting" />
                <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="PageIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlNivel" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
            </Triggers>
            <ContentTemplate>
              
            </ContentTemplate>
        </asp:UpdatePanel>--%>
        <%--<script type="text/javascript" src="../../tema/js/wait.js"></script>--%>
        <%--<asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>     --%>       
                <%--<div id="wait" style="position:fixed; text-align:center">                
                    <img src="../../tema/images/indicator_mozilla_blu.gif" alt=''/>            
                </div>  --%>      
           <%-- </ProgressTemplate>        
        </asp:UpdateProgress>--%>
 
    <script type="text/javascript">
        function enter(e) 
        {
            if (_enter) 
            {
                if (event.keyCode==13)
                {
                    event.keyCode=9; 
                    return event.keyCode;
                }    
            }
        }
        
        
        
        
        function inicio()
        {
            document.getElementById("txtClaveCT").focus();
        }    
        
        function Num(evt){
       return  keyRestrict(evt,'1234567890');
    }
    
    function keyRestrict(e, validchars) {
         var key='', keychar='';
         key = (document.all) ? e.keyCode : e.which;
         if (key == null) return true;
         keychar = String.fromCharCode(key);
         keychar = keychar.toLowerCase();
         validchars = validchars.toLowerCase();
         if (validchars.indexOf(keychar) != -1)
          return true;
         if (  key==8 || key==127 || key==0)
          return true;
         return false;
    }
    function AbrirPDF() {
        var ruta = document.getElementById("ctl00_cphMainMaster_hidIdCtrl").value
        if (ruta != "") {
            window.open('../Reportes/Reporte.aspx?OCE=2&ctrl=' + ruta, '', 'width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668')
        }
    }
    
        var _enter=true;
        function Vent911(niv,subniv,cnt,cic)
        {
            var v911 = false;
            try{
                var Pagina="";
                if(niv==11 && subniv==1){	//	911_2	Preescolar General
                    Pagina="911_2/Identificacion_911_2.aspx";
                }
                if(niv==11 && subniv==3){	//	ECC_21	Preescolar CONAFE
                    Pagina="911_ECC_21/Identificacion_911_ECC_21.aspx";
                }
                if(niv==12 && subniv==4){	//	911_4	Primaria General
                    Pagina="911_4/Identificacion_911_4.aspx";
                }
                if(niv==12 && subniv==6){	//	ECC_22	Primaria CONAFE
                    Pagina="911_ECC_22/Identificacion_911_ECC_22.aspx";
                }
                if(niv==13 && subniv==7){	//	911_6	Secundaria
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==13 && subniv==8){	//	911_6	Secundaria 2
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==13 && subniv==9){	//	911_6	Secundaria 3
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==13 && subniv==10){	//	911_6	Telesecundaria
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==13 && subniv==11){	//	911_6	Secundaria 5
                    Pagina="911_6/Identificacion_911_6.aspx";
                }
                if(niv==22 && subniv==22){	//	911_8G	Bachillerato General
                    Pagina="911_8G/Identificacion_911_8G.aspx";
                }
                if(niv==22 && subniv==23){	//	911_8T	Bachillerato Tecnologico
                    Pagina="911_8T/Identificacion_911_8T.aspx";
                }
                if(niv==21 && subniv==0){	//	911_8P	Profesional Técnico
                    Pagina="911_8P/Identificacion_911_8P.aspx";
                }
                if(niv==51 && subniv==50){	//	CAM_2	CAM Educacion Especial
                    Pagina="911_CAM_2/Identificacion_911_CAM_2.aspx";
                }
                if(niv==51 && subniv==51){	//	USAER_2	USAER Unidad de Servicios de Apoyo a la Educación Regular
                    Pagina="911_USAER_2/Identificacion_911_USAER_2.aspx";
                }
                if(niv==42 && subniv==42){	//	EI_NE2	Inicial NO Escolarizada
                    Pagina="911_EI_NE_2/Identificacion_911_EI_NE_2.aspx";
                }
                if(niv==41 && subniv==40){	//	EI_2	Inicial Escolarizada
                    Pagina="911_EI_2/Identificacion_911_EI_2.aspx";
                }
                if(niv==61 && subniv==63){	//	911_6C	Formación para el Trabajo
                    Pagina="911_6C/Identificacion_911_6C.aspx";
                }
                Pagina=Pagina + "?cnt=" + cnt + "&cic=" + cic;
                window.open(Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
             
            }
            catch(err)
            {
                alert(err);
            } 
           
        }
        
    </script>
</asp:Content>
