<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="CAM-2(Desglose)" AutoEventWireup="true" CodeBehind="Desglose_CAM_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin.CAM_2.Desglose_CAM_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
<%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 25;
        MaxRow = 39;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
     <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N ESPECIAL - CENTRO DE ATENCI�N M�LTIPLE</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1500px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_CAM_2',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_CAM_2',true)"><a href="#" title=""><span>INICIAL Y PREESCOLAR</span></a></li>
        <li onclick="openPage('AG2_CAM_2',true)"><a href="#" title=""><span>PRIMARIA Y SECUNDARIA</span></a></li>
        <li onclick="openPage('AG3_CAM_2',true)"><a href="#" title=""><span>CAP. P/TRABAJO Y ATENCI�N COMP.</span></a></li>
        <li onclick="openPage('Desglose_CAM_2',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li>
        <li onclick="openPage('Personal1_CAM_2',false)"><a href="#" title=""><span>PERSONAL DOCENTE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
       <%-- <li onclick="openPage('Anexo')"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

          <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


            <table>
            <tr>
                <td colspan="14" nowrap="nowrap" style="height: 20px; text-align: justify">
                    <asp:Label ID="lblInstruccion7" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="7. Escriba la inscripci�n total, desglos�ndola por servicio o nivel, edad a la fecha del llenado del cuestionario y sexo."
                        Width="100%"></asp:Label></td>
                <td colspan="1" nowrap="nowrap" style="height: 20px; text-align: justify">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                </td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInicial7" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EDUCACI�N INICIAL"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblPreescolar7" runat="server" CssClass="lblNegro" Font-Bold="True"
                        Text="PREESCOLAR" Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblPrimaria7" runat="server" CssClass="lblNegro" Font-Bold="True" Text="PRIMARIA"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblSecundaria7" runat="server" CssClass="lblNegro" Font-Bold="True"
                        Text="SECUNDARIA" Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblCapacitacion7" runat="server" CssClass="lblNegro" Font-Bold="True"
                        Text="CAPACITACI�N LABORAL" Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblAtencion7" runat="server" CssClass="lblNegro" Font-Bold="True" Text="ATENCI�N COMPLEMENTARIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="Orila">
                    &nbsp;</td>
                <td class="linaBajo" style="text-align: center">
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom71" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj71" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom72" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj72" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom73" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj73" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom74" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj74" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom75" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj75" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom76" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj76" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTota72" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoAlto">
                    <asp:Label ID="lbl45a1" runat="server" CssClass="lblGrisTit" Text="45 d�as a 1 a�o"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV872" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV873" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV874" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV875" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV876" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV877" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV878" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV879" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV880" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV881" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10110"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV882" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10111"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV883" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10112"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV884" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10113"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl2a" runat="server" CssClass="lblGrisTit" Text="2 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV885" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV886" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV887" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV888" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV889" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV890" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV891" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV892" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV893" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV894" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10210"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV895" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10211"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV896" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10212"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV897" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10213"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lbl3a" runat="server" CssClass="lblGrisTit" Text="3 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV898" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV899" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV900" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV901" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV902" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV903" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV904" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV905" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV906" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV907" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10310"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV908" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10311"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV909" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10312"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV910" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10313"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl3a6m" runat="server" CssClass="lblGrisTit" Text="3 a�os 6 meses a 3 a�os 11 meses"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV911" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV912" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV913" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV914" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV915" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV916" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV917" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV918" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV919" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV920" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10410"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV921" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10411"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV922" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10412"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV923" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10413"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl4a" runat="server" CssClass="lblGrisTit" Text="4 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV924" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV925" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV926" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV927" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV928" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV929" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV930" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV931" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV932" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV933" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10510"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV934" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10511"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV935" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10512"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV936" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10513"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lbl5a" runat="server" CssClass="lblGrisTit" Text="5 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV937" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV938" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV939" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV940" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV941" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV942" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV943" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV944" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV945" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV946" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10610"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV947" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10611"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV948" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10612"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV949" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10613"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl6a" runat="server" CssClass="lblGrisTit" Text="6 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV950" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV951" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV952" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV953" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV954" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV955" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV956" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV957" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV958" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV959" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10710"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV960" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10711"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV961" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10712"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV962" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10713"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl7a" runat="server" CssClass="lblGrisTit" Text="7 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV963" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV964" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV965" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV966" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV967" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV968" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV969" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV970" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10808"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV971" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV972" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10810"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV973" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10811"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV974" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10812"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV975" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10813"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lbl8a" runat="server" CssClass="lblGrisTit" Text="8 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV976" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV977" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV978" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV979" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV980" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV981" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV982" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV983" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10908"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV984" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV985" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10910"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV986" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10911"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV987" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10912"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV988" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10913"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl9a" runat="server" CssClass="lblGrisTit" Text="9 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV989" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV990" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV991" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV992" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV993" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV994" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV995" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV996" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11008"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV997" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV998" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11010"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV999" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11011"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1000" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11012"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1001" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11013"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl10a" runat="server" CssClass="lblGrisTit" Text="10 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1002" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1003" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1004" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1005" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1006" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1007" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1008" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1009" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1010" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1011" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11110"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1012" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11111"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1013" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11112"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1014" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11113"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lbl11a" runat="server" CssClass="lblGrisTit" Text="11 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1015" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1016" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1017" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1018" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1019" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1020" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1021" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1022" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1023" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1024" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11210"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1025" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11211"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1026" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11212"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1027" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11213"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl12a" runat="server" CssClass="lblGrisTit" Text="12 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1028" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1029" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1030" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1031" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1032" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1033" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1034" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1035" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1036" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1037" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11310"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1038" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11311"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1039" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11312"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1040" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11313"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl13a" runat="server" CssClass="lblGrisTit" Text="13 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1041" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1042" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1043" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1044" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1045" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1046" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1047" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1048" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1049" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1050" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11410"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1051" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11411"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1052" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11412"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1053" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11413"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lbl14a" runat="server" CssClass="lblGrisTit" Text="14 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1054" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1055" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1056" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1057" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1058" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1059" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1060" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1061" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1062" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1063" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11510"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1064" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11511"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1065" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11512"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1066" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11513"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl15a" runat="server" CssClass="lblGrisTit" Text="15 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1067" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1068" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1069" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1070" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1071" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1072" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1073" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1074" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11608"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1075" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1076" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11610"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1077" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11611"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1078" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11612"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1079" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11613"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl16a" runat="server" CssClass="lblGrisTit" Text="16 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1080" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1081" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1082" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1083" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1084" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1085" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1086" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1087" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11708"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1088" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1089" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11710"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1090" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11711"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1091" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11712"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1092" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11713"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lbl17a" runat="server" CssClass="lblGrisTit" Text="17 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1093" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1094" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1095" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1096" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1097" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1098" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1099" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1100" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11808"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1101" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1102" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11810"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1103" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11811"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1104" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11812"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1105" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11813"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl18a" runat="server" CssClass="lblGrisTit" Text="18 a�os" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1106" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1107" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1108" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1109" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1110" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1111" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1112" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1113" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11908"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1114" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1115" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11910"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1116" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11911"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1117" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11912"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1118" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11913"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lbl19a" runat="server" CssClass="lblGrisTit" Text="19 o mas a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1119" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1120" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1121" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1122" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1123" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1124" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1125" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1126" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12008"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1127" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1128" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12010"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1129" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12011"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1130" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12012"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1131" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12013"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblSubtotales" runat="server" CssClass="lblGrisTit" Text="SUBTOTALES" Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1616" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12101"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1617" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1618" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1619" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1620" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1621" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1622" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1623" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1624" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1625" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12110"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1626" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12111"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1627" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="12112"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1628" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="12113"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblTota73" runat="server" CssClass="lblGrisTit" Text="TOTAL (Suma de HOM y MUJ)"
                        Width="90px"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1132" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                <td colspan="2" style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1133" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                <td colspan="2" style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1134" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
                <td colspan="2" style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1135" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
                <td colspan="2" style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1136" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20105"></asp:TextBox></td>
                <td colspan="2" style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1137" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20106"></asp:TextBox></td>
                <td colspan="1" style="text-align: center" class="linaBajoE">
                    <asp:TextBox ID="txtV1138" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20107"></asp:TextBox></td>
                <td class="Orila" colspan="1" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="14" nowrap="nowrap" style="height: 20px">
                </td>
                <td colspan="1" nowrap="nowrap" style="height: 20px">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="14">
                    <asp:Label ID="lblInstruccion8" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="8. Registre por nivel o servicio educativo el n�mero de alumnos beneficiados en educaci�n regular, y la cantidad de padres orientados y maestros asesorados, al inicio del ciclo escolar."
                        Width="100%"></asp:Label></td>
                <td colspan="1">
                </td>
            </tr>
            <tr>
                <td colspan="14">
                    <table>
                        <tbody>
                            <tr>
                                <td style="text-align: center">
                                </td>
                                <td style="text-align: center">
                                    <asp:Label ID="lblBeneficiados" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                        Text="ALUMNOS BENEFICIADOS" Width="200px"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:Label ID="lblOrientados" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                        Text="PADRES ORIENTADOS" Width="200px"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:Label ID="lblAsesorados" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                        Text="MAESTROS ASESORADOS" Width="200px"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblInicial8" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                        Text="EDUCACI�N INICIAL" Width="100%"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1139" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30101"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1140" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30102"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1141" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30103"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblPreescolar8" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                        Text="PREESCOLAR" Width="100%"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1142" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30201"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1143" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30202"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1144" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30203"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblPrimaria8" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                        Text="PRIMARIA" Width="100%"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1145" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30301"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1146" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30302"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1147" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30303"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblSecundaria8" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                        Text="SECUNDARIA" Width="100%"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1148" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30401"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1149" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30402"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1150" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30403"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblCapacitacion" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                        Text="CAPACITACI�N LABORAL" Width="100%"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1151" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30501"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1152" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30502"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1153" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30503"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: left">
                                    <asp:Label ID="lblTota8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                                        Width="100%"></asp:Label></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1154" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30601"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1155" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30602"></asp:TextBox></td>
                                <td style="text-align: center">
                                    <asp:TextBox ID="txtV1156" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30603"></asp:TextBox></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td colspan="1">
                </td>
            </tr>
        </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG3_CAM_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG3_CAM_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Personal1_CAM_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal1_CAM_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
        <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                              
 		Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
