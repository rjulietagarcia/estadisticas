<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="CAM-2(CAP. P/TRABAJO Y AT. COMP.)" AutoEventWireup="true" CodeBehind="AG3_CAM_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin.CAM_2.AG3_CAM_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <script type="text/javascript">
        var _enter=true;
    </script>
 <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 25;
        MaxRow = 39;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
      <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N ESPECIAL - CENTRO DE ATENCI�N M�LTIPLE</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1500px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_CAM_2',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_CAM_2',true)"><a href="#" title=""><span>INICIAL Y PREESCOLAR</span></a></li>
        <li onclick="openPage('AG2_CAM_2',true)"><a href="#" title=""><span>PRIMARIA Y SECUNDARIA</span></a></li>
        <li onclick="openPage('AG3_CAM_2',true)"><a href="#" title="" class="activo"><span>CAP. P/TRABAJO Y ATENCI�N COMP.</span></a></li>
        <li onclick="openPage('Desglose_CAM_2',false)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL DOCENTE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
       <%-- <li onclick="openPage('Anexo')"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

   <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td colspan="18" style="text-align: justify">
                    <asp:Label ID="lblInstruccion5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5. Escriba el n�mero de alumnos por sexo y el n�mero de grupos de capacitaci�n para el trabajo, de acuerdo con el taller al que asisten, seg�n los rubros que se indican. Si se imparten otros talleres, escr�balos en las l�neas; seg�n la tabla del reverso de la p�gina anterior."
                        Width="100%"></asp:Label></td>
                <td colspan="1" style="text-align: justify">
                </td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center">
                </td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionI5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N INICIAL"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionT5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100px"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblBajas5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="BAJAS"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblExistencia5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblEgresados5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EGRESADOS"
                        Width="100%"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTermino5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="T�RMINO DE ATENCI�N"
                        Width="100px"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    &nbsp;<asp:Label ID="lblContinuan5" runat="server" CssClass="lblNegro" Font-Bold="True"
                        Text="CONTIN�AN CON ATENCI�N" Width="100px"></asp:Label></td>
                <td colspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblIntegrados5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="INTEGRADOS A EDUCACI�N REGULAR"
                        Width="100px"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblGrupos" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
                <td class="Orila" rowspan="2" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center">
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom53" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj53" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom54" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj54" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom55" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj55" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom56" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj56" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom57" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:Label ID="lblMuj57" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:Label ID="lblHom58" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="lblMuj58" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left; height: 26px;" class="linaBajoAlto">
                    <asp:Label ID="lblCocina" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="COCINA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV353" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajo">
                    <asp:TextBox ID="txtV354" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV355" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajo">
                    <asp:TextBox ID="txtV356" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV357" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajo">
                    <asp:TextBox ID="txtV358" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV359" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajo">
                    <asp:TextBox ID="txtV360" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV361" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10109"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajo">
                    <asp:TextBox ID="txtV362" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10110"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV363" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10111"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajo">
                    <asp:TextBox ID="txtV364" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10112"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV365" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10113"></asp:TextBox></td>
                <td style="text-align: center; height: 26px; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV366" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10114"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV367" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10115"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;" class="linaBajo">
                    <asp:TextBox ID="txtV368" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10116"></asp:TextBox>
                </td>
                <td style="text-align: center; height: 26px;" class="linaBajoV">
                    <asp:TextBox ID="txtV369" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10117"></asp:TextBox></td>
                <td class="Orila" style="height: 26px; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblCorte" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CORTE Y CONFECCI�N"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV370" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV371" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV372" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV373" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV374" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV375" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV376" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV377" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV378" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV379" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10210"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV380" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10211"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV381" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10212"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV382" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10213"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV383" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10214"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV384" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10215"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV385" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10216"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV386" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10217"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblArtesanias" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ARTESAN�AS"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV387" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV388" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV389" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV390" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV391" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV392" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV393" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV394" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV395" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV396" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10310"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV397" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10311"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV398" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10312"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV399" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10313"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajoS">
                    <asp:TextBox ID="txtV400" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10314"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV401" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10315"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV402" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10316"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV403" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10317"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblElectricidad" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ELECTRICIDAD"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV404" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV405" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV406" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV407" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV408" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV409" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV410" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV411" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV412" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV413" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10410"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV414" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10411"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV415" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10412"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV416" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10413"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV417" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10414"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV418" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10415"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV419" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10416"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV420" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10417"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblTejido" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TEJIDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV421" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV422" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV423" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV424" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV425" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV426" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV427" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV428" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV429" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV430" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10510"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV431" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10511"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV432" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10512"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV433" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10513"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV434" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10514"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV435" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10515"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV436" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10516"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV437" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10517"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajoS">
                    <asp:Label ID="lblCarpinteria" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CARPINTER�A"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV438" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV439" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV440" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV441" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV442" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV443" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV444" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV445" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV446" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV447" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10610"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV448" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10611"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV449" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10612"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV450" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10613"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajoS">
                    <asp:TextBox ID="txtV451" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10614"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV452" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10615"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV453" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10616"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV454" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10617"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblHerreria" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HERRER�A"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV455" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV456" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV457" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV458" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV459" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV460" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV461" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV462" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV463" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV464" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10710"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV465" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10711"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV466" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10712"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV467" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10713"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV468" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10714"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV469" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10715"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV470" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10716"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV471" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10717"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left" class="linaBajo">
                    <asp:Label ID="lblBelleza" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="BELLEZA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV472" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV473" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV474" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV475" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV476" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV477" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV478" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV479" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10808"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV480" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV481" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10810"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV482" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10811"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV483" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10812"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV484" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10813"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV485" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10814"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV486" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10815"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV487" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10816"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV488" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10817"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:DropDownList ID="optV489" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV489')" Width="200px"></asp:DropDownList>
                    <asp:TextBox Width="1px" ID="txtV489" runat="server" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV490" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV491" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV492" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV493" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV494" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV495" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV496" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10908"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV497" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV498" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10910"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV499" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10911"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV500" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10912"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV501" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10913"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV502" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10914"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajoS">
                    <asp:TextBox ID="txtV503" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10915"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV504" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10916"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV505" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10917"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV506" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10918"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:DropDownList ID="optV507" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV507')" Width="200px"></asp:DropDownList>
                    <asp:TextBox Width="1px" ID="txtV507" runat="server"  style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV508" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV509" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV510" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV511" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV512" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV513" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV514" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11008"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV515" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV516" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11010"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV517" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11011"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV518" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11012"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV519" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11013"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV520" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11014"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV521" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11015"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV522" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11016"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV523" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11017"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV524" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11018"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:DropDownList ID="optV525" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV525')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV525" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV526" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV527" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV528" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV529" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV530" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV531" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV532" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV533" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV534" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11110"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV535" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11111"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV536" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11112"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV537" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11113"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV538" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11114"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV539" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11115"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV540" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11116"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV541" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11117"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV542" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11118"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:DropDownList ID="optV543" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV543')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV543" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV544" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV545" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV546" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV547" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV548" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV549" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV550" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV551" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV552" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11210"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV553" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11211"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV554" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11212"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV555" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11213"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV556" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11214"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajoS">
                    <asp:TextBox ID="txtV557" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11215"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV558" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11216"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV559" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11217"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV560" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11218"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:DropDownList ID="optV561" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV561')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV561" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV562" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV563" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV564" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV565" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV566" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV567" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV568" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV569" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV570" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11310"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV571" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11311"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV572" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11312"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV573" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11313"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV574" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11314"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV575" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11315"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV576" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11316"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV577" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11317"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV578" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11318"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:DropDownList ID="optV579" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV579')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV579" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV580" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV581" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV582" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV583" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV584" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV585" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV586" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11408"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV587" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11409"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV588" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11410"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV589" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11411"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV590" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11412"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV591" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11413"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV592" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11414"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV593" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11415"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV594" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11416"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV595" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11417"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV596" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11418"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:DropDownList ID="optV597" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV597')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV597" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV598" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV599" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV600" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV601" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV602" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV603" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV604" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11508"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV605" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11509"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV606" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11510"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV607" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11511"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV608" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11512"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV609" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11513"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV610" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11514"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajoS">
                    <asp:TextBox ID="txtV611" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11515"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV612" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11516"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV613" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11517"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV614" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11518"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:DropDownList ID="optV615" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV615')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV615" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV616" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV617" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV618" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11604"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV619" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11605"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV620" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11606"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV621" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11607"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV622" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11608"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV623" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11609"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV624" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11610"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV625" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11611"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV626" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11612"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV627" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11613"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV628" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11614"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV629" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11615"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV630" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11616"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV631" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11617"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV632" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11618"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:DropDownList ID="optV633" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV633')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV633" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV634" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV635" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV636" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11704"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV637" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11705"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV638" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11706"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV639" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11707"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV640" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11708"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV641" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11709"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV642" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11710"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV643" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11711"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV644" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11712"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV645" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11713"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV646" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11714"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV647" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11715"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV648" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11716"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV649" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11717"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV650" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11718"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:DropDownList ID="optV1507" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV1507')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV1507" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1508" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1509" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1510" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11804"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1511" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11805"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1512" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11806"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1513" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11807"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1514" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11808"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1515" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11809"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1516" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11810"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1517" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11811"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1518" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11812"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1519" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11813"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1520" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11814"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajoS">
                    <asp:TextBox ID="txtV1521" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11815"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1522" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11816"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1523" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11817"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1524" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11818"></asp:TextBox></td>
                     <td class="Orila" style="text-align: center">
                         &nbsp;</td>
            </tr>    
            <tr>
                <td style="text-align: center" class="linaBajo">
                    <asp:DropDownList ID="optV1525" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV1525')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV1525" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1526" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1527" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1528" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11904"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1529" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11905"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1530" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11906"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1531" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11907"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1532" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11908"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1533" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11909"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1534" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11910"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1535" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11911"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1536" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11912"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1537" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11913"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1538" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11914"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV1539" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11915"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1540" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11916"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1541" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11917"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1542" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11918"></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
                <td style="text-align: center"></td>
               </tr>     
            <tr>
               <td style="text-align: center" class="linaBajo">
                    <asp:DropDownList ID="optV1543" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV1543')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV1543" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1544" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1545" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1546" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12004"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1547" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12005"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1548" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12006"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1549" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12007"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1550" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12008"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1551" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12009"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1552" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12010"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1553" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12011"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1554" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12012"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1555" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12013"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1556" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12014"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV1557" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12015"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1558" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12016"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1559" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12017"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1560" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12018"></asp:TextBox></td>
                   <td class="Orila" style="text-align: center">
                       &nbsp;</td>
            </tr>    
            <tr>
                <td style="text-align: center" class="linaBajoS">
                    <asp:DropDownList ID="optV1561" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV1561')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV1561" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1562" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1563" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12103"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1564" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12104"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1565" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12105"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1566" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12106"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1567" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12107"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1568" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12108"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1569" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12109"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1570" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12110"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1571" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12111"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1572" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12112"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1573" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12113"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1574" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12114"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajoS">
                    <asp:TextBox ID="txtV1575" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12115"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1576" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12116"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1577" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12117"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV1578" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12118"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>     
            <tr>
                <td style="text-align: center;width:220px;" class="linaBajo" >
                    <asp:DropDownList ID="optV1579" runat="server" onchange="OnChange(this,'ctl00_cphMainMaster_txtV1579')" Width="200px"></asp:DropDownList>
                    <asp:TextBox ID="txtV1579" runat="server" Width="1px" style="visibility:hidden;"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1580" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12202"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1581" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12203"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1582" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12204"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1583" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12205"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1584" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12206"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1585" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12207"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1586" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12208"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1587" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12209"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1588" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12210"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1589" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12211"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1590" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12212"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1591" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12213"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1592" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12214"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV1593" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12215"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1594" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12216"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV1595" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12217"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV1596" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12218"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV651" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12301"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV652" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12302"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV653" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12303"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV654" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12304"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV655" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12305"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV656" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12306"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV657" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12307"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV658" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12308"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV659" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12309"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV660" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12310"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV661" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12311"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV662" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12312"></asp:TextBox>&nbsp;</td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV663" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12313"></asp:TextBox></td>
                <td style="text-align: center; width: 57px;" class="linaBajo">
                    <asp:TextBox ID="txtV664" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12314"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV665" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12315"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV666" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12316"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoV">
                    <asp:TextBox ID="txtV667" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12317"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
        </table>
        <br />
    
   
    
    <table>
        <tr>
            <td colspan="4" style="height: 53px; text-align: justify">
                <asp:Label ID="lblInstruccion5b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro de inscripcion total, escriba el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblSituacion5" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center; width: 53px;">
                <asp:Label ID="lblTotal52" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV668" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV669" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12502"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV670" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12503"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblVisual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV671" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV672" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12602"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV673" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12603"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV674" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV675" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12702"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV676" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12703"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblAuditiva" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV677" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV678" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12802"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV679" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12803"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV680" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV681" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12902"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV682" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12903"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV683" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13001"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV684" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13002"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV685" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13003"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="text-align: left">
                <asp:Label ID="lblSobresalientes" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV686" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV687" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13102"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV688" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13103"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblOtros5" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV689" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV690" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13202"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV691" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13203"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblTotal53" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV692" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV693" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13302"></asp:TextBox></td>
            <td style="text-align: center; width: 53px;">
                <asp:TextBox ID="txtV694" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="13303"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td colspan="12" style="text-align: center">
                <asp:Label ID="lblInstruccion6" runat="server" CssClass="lblRojo" Font-Bold="True"
                    Text="6. Escriba por nivel o servicio educativo el n�mero total de alumnos que recibieron atenci�n complementaria durante el ciclo escolar, por grado y sexo, seg�n los rubros que se indican."
                    Width="100%"></asp:Label></td>
            <td colspan="1" style="text-align: center">
            </td>
        </tr>
        <tr>
            <td style="width: 97px; text-align: center">
            </td>
            <td style="text-align: center">
            </td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblTotalAtendidos6" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL DE ALUMNOS ATENDIDOS"
                    Width="150px"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblBajas6" runat="server" CssClass="lblNegro" Font-Bold="True" Text="BAJAS"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblExistencia6" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EXISTENCIA"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblContinuan6" runat="server" CssClass="lblNegro" Font-Bold="True" Text="CONTIN�AN EN APOYO"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                <asp:Label ID="lblTerminaron6" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TERMINARON APOYO"
                    Width="100%"></asp:Label></td>
            <td class="Orila" colspan="1" style="text-align: center">
                &nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 97px; text-align: center">
            </td>
            <td style="text-align: center">
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom61" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj61" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom62" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj62" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom63" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj63" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom64" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj64" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblHom65" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblMuj65" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 97px; text-align: left" class="linaBajoAlto">
                <asp:Label ID="lblInicial6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INICIAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoAlto">
                &nbsp;</td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV695" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV696" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV697" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV698" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV699" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20105"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV700" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20106"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV701" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20107"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV702" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20108"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV703" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20109"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV704" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20110"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 97px; text-align: left" class="linaBajoS" rowspan="3">
                <asp:Label ID="lblPreescolar6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PREESCOLAR"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblPre1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV705" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV706" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV707" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV708" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20204"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV709" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20205"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV710" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20206"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV711" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20207"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV712" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20208"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV713" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20209"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV714" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20210"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblPre2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV715" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV716" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV717" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV718" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20304"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV719" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20305"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV720" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20306"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV721" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20307"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV722" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20308"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV723" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20309"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV724" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20310"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajoS">
                <asp:Label ID="lblPre3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV725" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV726" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV727" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV728" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20404"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV729" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20405"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV730" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20406"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV731" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20407"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV732" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20408"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV733" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20409"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV734" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20410"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 97px; text-align: left" class="linaBajoS" rowspan="6">
                <asp:Label ID="lblPrimaria6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMARIA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblPri1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV735" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV736" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV737" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20503"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV738" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20504"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV739" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20505"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV740" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20506"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV741" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20507"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV742" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20508"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV743" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20509"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV744" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20510"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblPri2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV745" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV746" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV747" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20603"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV748" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20604"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV749" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20605"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV750" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20606"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV751" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20607"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV752" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20608"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV753" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20609"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV754" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20610"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblPri3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV755" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV756" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20702"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV757" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20703"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV758" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20704"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV759" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20705"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV760" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20706"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV761" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20707"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV762" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20708"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV763" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20709"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV764" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20710"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblPri4o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV765" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV766" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20802"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV767" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20803"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV768" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20804"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV769" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20805"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV770" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20806"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV771" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20807"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV772" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20808"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV773" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20809"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV774" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20810"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblPri5o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV775" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20901"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV776" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20902"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV777" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20903"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV778" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20904"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV779" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20905"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV780" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20906"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV781" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20907"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV782" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20908"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV783" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20909"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV784" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="20910"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajoS">
                <asp:Label ID="lblPri6o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="6o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV785" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21001"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV786" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21002"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV787" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21003"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV788" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21004"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV789" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21005"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV790" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21006"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV791" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21007"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV792" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21008"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV793" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21009"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV794" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21010"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 97px; text-align: left" class="linaBajoS" rowspan="3">
                <asp:Label ID="lblSecundaria6" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                    Text="SECUNDARIA" Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblSec1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV795" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21101"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV796" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21102"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV797" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21103"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV798" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21104"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV799" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21105"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV800" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21106"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV801" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21107"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV802" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21108"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV803" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21109"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV804" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21110"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajo">
                <asp:Label ID="lblSec2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV805" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21201"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV806" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21202"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV807" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21203"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV808" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21204"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV809" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21205"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV810" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21206"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV811" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21207"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV812" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21208"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV813" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21209"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajo">
                <asp:TextBox ID="txtV814" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21210"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center" class="linaBajoS">
                <asp:Label ID="lblSec3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                    Width="100%"></asp:Label>
            </td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV815" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21301"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV816" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21302"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV817" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21303"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV818" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21304"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV819" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21305"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV820" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21306"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV821" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21307"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV822" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21308"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV823" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21309"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV824" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21310"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 97px; text-align: left" class="linaBajoS">
                <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CAPACITACI�N LABORAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoS">
                &nbsp;</td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV825" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21401"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV826" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21402"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV827" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21403"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV828" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21404"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV829" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21405"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV830" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21406"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV831" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21407"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV832" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21408"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV833" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21409"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV834" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21410"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 97px; text-align: left" class="linaBajoS">
                <asp:Label ID="lblTotal61" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center" class="linaBajoS">
                &nbsp;</td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV835" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21501"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV836" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21502"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV837" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21503"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV838" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="21504"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV839" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21505"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV840" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21506"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV841" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21507"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV842" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21508"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV843" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21509"></asp:TextBox></td>
            <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV844" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21510"></asp:TextBox></td>
            <td class="Orila" style="text-align: center">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td colspan="4" style="text-align: justify">
                <asp:Label ID="lblInstruccion6b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro total de alumnos atendidos, escriba el n�mero de alumnos con alguna discapacidad o con capacidades y aptitudes sobresalientes, seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblSituacion6" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal62" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblCeguera6" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV845" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30101"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV846" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30102"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV847" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30103"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblVisual6" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD VISUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV848" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30201"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV849" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30202"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV850" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30203"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblSordera6" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV851" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30301"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV852" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30302"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV853" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30303"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblAuditiva6" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD AUDITIVA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV854" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30401"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV855" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30402"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV856" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30403"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblMotriz6" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV857" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30501"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV858" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30502"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV859" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30503"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblIntelectual6" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV860" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30601"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV861" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30602"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV862" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30603"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="nowrap" style="text-align: left">
                <asp:Label ID="lblSobresalientes6" runat="server" CssClass="lblGrisTit" Text="CAPACIDADES Y APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV863" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30701"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV864" runat="server" Columns="4" MaxLength="2" CssClass="lblNegro" TabIndex="30702"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV865" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30703"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblOtros6" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV866" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30801"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV867" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30802"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV868" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30803"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblTotal63" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV869" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30901"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV870" runat="server" Columns="4" MaxLength="3" CssClass="lblNegro" TabIndex="30902"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV871" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="30903" ></asp:TextBox></td>
        </tr>
    </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG2_CAM_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG2_CAM_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Desglose_CAM_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Desglose_CAM_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }
                
                 function OnChange(dropdown,idtxt)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    if(myindex == 0)
                    {
                    document.getElementById(idtxt).value  = '_';
                    }else{
                    document.getElementById(idtxt).value  = SelValue;                    
                    }
                    
                  return true;
                }

                                    
 		Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
