<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="CAM-2(Personal)" AutoEventWireup="true" CodeBehind="Personal1_CAM_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin.CAM_2.Personal1_CAM_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <script type="text/javascript">
        var _enter=true;
    </script>
<%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 25;
        MaxRow = 39;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
      <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N ESPECIAL - CENTRO DE ATENCI�N M�LTIPLE</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1500px;">
      <ul class="left">
        <li onclick="OpenPageCharged('Identificacion_911_CAM_2',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="OpenPageCharged('AG1_CAM_2',true)"><a href="#" title=""><span>INICIAL Y PREESCOLAR</span></a></li>
        <li onclick="OpenPageCharged('AG2_CAM_2',true)"><a href="#" title=""><span>PRIMARIA Y SECUNDARIA</span></a></li>
        <li onclick="OpenPageCharged('AG3_CAM_2',true)"><a href="#" title=""><span>CAP. P/TRABAJO Y ATENCI�N COMP.</span></a></li>
        <li onclick="OpenPageCharged('Desglose_CAM_2',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="OpenPageCharged('Personal1_CAM_2',true)"><a href="#" title="" class="activo"><span>PERSONAL DOCENTE</span></a></li>
        <li onclick="OpenPageCharged('Personal2_CAM_2',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
      <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
       <%-- <li onclick="openPage('Anexo')"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>--%>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
 
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td colspan="2" rowspan="1" valign="top">
                    <asp:Label ID="lblPERSONAL" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="II. PERSONAL POR FUNCI�N" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td rowspan="1" valign="top">
                    <table style="width: 320px">
                        <tr>
                            <td>
                                <asp:Label ID="lblDIRECTOR" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DIRECTOR"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="1. Marque la situaci�n del Director" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblConGrupo" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="CON GRUPO" Width="100%"></asp:Label></td>
                                        <td>
                                            &nbsp;<asp:RadioButton ID="optV1157" runat="server" GroupName="Situacion1"  />
                                            <asp:TextBox ID="txtV1157" runat="server" Width="20px" style="visibility:hidden"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblSinGrupo" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="SIN GRUPO" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:RadioButton ID="optV1158" runat="server" GroupName="Situacion1"  />
                                            <asp:TextBox ID="txtV1158" runat="server" Width="20px" style="visibility:hidden"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: justify">
                                <asp:Label ID="lblInstruccion2" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="2. Escriba la clave y el nombre de su nivel m�ximo de estudios y de su especialidad, de acuerdo con las tablas de nivel m�ximo de estudios y de especialidad que est�n en el reverso de la p�gina anterior."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td colspan="2" style="text-align: left">
                                            <asp:Label ID="lblNivelMax" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="NIVEL M�XIMO DE ESTUDIOS" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV1159" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"  Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="txtV1160" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                                        <td style="width: 186px; text-align: left">
                                            <asp:DropDownList ID="cboV1160" runat="server">
                                            </asp:DropDownList>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left">
                                            <asp:Label ID="lblEspecialidad" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="ESPECIALIDAD" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV1161" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro"  Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="txtV1162" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  Width="2px" style="visibility:hidden;"></asp:TextBox></td>
                                        <td style="width: 186px; text-align: left">
                                            <asp:DropDownList ID="cboV1162" runat="server" >
                                            </asp:DropDownList>
                                            </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblOtra" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRA"
                                                Width="100%"></asp:Label></td>
                                        <td colspan="2">
                                            <asp:Label ID="lblEspecifique" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="ESPECIFIQUE:" Width="100%"></asp:Label></td>
                                        <td style="width: 186px">
                                            <asp:TextBox ID="txtV1597" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccion3" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="3. Indique el sexo del director." Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblHombre" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRE"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            &nbsp;<asp:RadioButton ID="optV1163" runat="server" GroupName="Sexo" />
                                            <asp:TextBox ID="txtV1163" runat="server" Width="20px" style="visibility:hidden"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblMujer" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJER"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:RadioButton ID="optV1164" runat="server" GroupName="Sexo"/>
                                            <asp:TextBox ID="txtV1164" runat="server" Width="20px" style="visibility:hidden"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblInstruccion4" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="4. Marque su situaci�n acad�mica" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td colspan="3" style="height: 26px; text-align: left">
                                            <asp:Label ID="lblPosgrado" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="POSGRADO" Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 62px; height: 20px">
                                        </td>
                                        <td style="width: 69px; height: 20px">
                                            <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="POSGRADO"
                                                Width="100%"></asp:Label></td>
                                        <td style="height: 20px; text-align: left">
                                            <asp:RadioButton ID="optV1165" runat="server" GroupName="Situacion4"/>
                                           <asp:TextBox ID="txtV1165" runat="server" Width="20px" style="visibility:hidden"></asp:TextBox> 
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 62px; height: 20px">
                                        </td>
                                        <td style="width: 69px; height: 20px">
                                        </td>
                                        <td style="height: 20px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: left">
                                            <asp:Label ID="lblLicenciatura4" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="LICENCIATURA" Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblTitulado" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="TITULADO" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:RadioButton ID="optV1166" runat="server" GroupName="Situacion4"/>
                                            <asp:TextBox ID="txtV1166" runat="server" Width="20px" style="visibility:hidden"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblNoTitulado" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="NO TITULADO" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:RadioButton ID="optV1167" runat="server" GroupName="Situacion4"  />
                                            <asp:TextBox ID="txtV1167" runat="server" Width="20px" style="visibility:hidden"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblEstudiante" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                Text="ESTUDIANTE" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:RadioButton ID="optV1168" runat="server" GroupName="Situacion4"  />
                                            <asp:TextBox ID="txtV1168" runat="server" Width="20px" style="visibility:hidden"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: right">
                    <table>
                        <tr>
                            <td colspan="9" style="text-align: left">
                                <asp:Label ID="lblDOCENTE" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DOCENTE"
                                    Width="100%"></asp:Label></td>
                            <td colspan="1" style="text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: justify">
                                <asp:Label ID="lblInstruccion5" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="5. Registre la cantidad de personal docente por formaci�n, seg�n su situaci�n acad�mica y sexo."
                                    Width="100%"></asp:Label></td>
                            <td colspan="1" style="text-align: justify">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: justify">
                                &nbsp;</td>
                            <td colspan="1" style="text-align: justify">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                            </td>
                            <td colspan="8" style="text-align: center">
                                <asp:Label ID="lblLicenciatura51" runat="server" CssClass="lblRojo" Font-Bold="True"
                                    Text="L I C E N C I A T U R A" Width="100%"></asp:Label></td>
                            <td colspan="1" style="text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                            </td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblTitulados51" runat="server" CssClass="lblNegro" Font-Bold="True"
                                    Text="TITULADOS" Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblNoTitulados51" runat="server" CssClass="lblNegro" Font-Bold="True"
                                    Text="NO TITULADOS" Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblEstudiantes51" runat="server" CssClass="lblNegro" Font-Bold="True"
                                    Text="ESTUDIANTES" Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblTotal51" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                            <td class="Orila" colspan="1" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblFormacion" runat="server" CssClass="lblRojo" Font-Bold="True" Text="FORMACI�N"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:Label ID="lblHom51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:Label ID="lblMuj51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:Label ID="lblHom52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:Label ID="lblMuj52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:Label ID="lblHom53" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:Label ID="lblMuj53" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:Label ID="lblHom54" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:Label ID="lblMuj54" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblMental" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN DEFICIENCIA MENTAL"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1169" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1170" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1171" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1172" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1173" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1174" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1175" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1176" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblAudicion" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="EN AUDICI�N Y LENGUAJE" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1177" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1178" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1179" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1180" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1181" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1182" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1183" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1184" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblAprendizaje" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="EN APRENDIZAJE" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1185" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1186" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1187" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1188" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1189" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1190" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1191" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1192" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblVisual" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN DEFICIENCIA VISUAL"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1193" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1194" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1195" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1196" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1197" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1198" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1199" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1200" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblLocomotor" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="EN APARATO LOCOMOTOR" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1201" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1202" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1203" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1204" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1205" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1206" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1207" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1208" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblInadaptacion" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="EN INADAPTACI�N SOCIAL" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1209" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1210" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1211" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1212" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1213" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1214" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1215" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1216" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblPedagogo" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="PEDAGOGO" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1217" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1218" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1219" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1220" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1221" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1222" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1223" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1224" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblPsicologo" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="PSIC�LOGO" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1225" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1226" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1227" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1228" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1229" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10805"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1230" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10806"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1231" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10807"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1232" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10808"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblMaestroPre" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="MAESTRO DE PREESCOLAR" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1233" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1234" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1235" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1236" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1237" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1238" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1239" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1240" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10908"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblMaestroPri" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="MAESTRO DE PRIMARIA" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1241" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1242" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1243" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1244" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1245" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1246" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1247" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1248" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblIntegracionE" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="EN INTEGRACI�N EDUCATIVA" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1249" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1250" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1251" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1252" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1253" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1254" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1255" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1256" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11108"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblMaestroTaller" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="MAESTRO DE TALLER" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1257" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1258" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1259" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1260" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1261" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1262" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1263" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1264" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11208"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblInstructor" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                    Text="INSTRUCTOR" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1265" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1266" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1267" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1268" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1269" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1270" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1271" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1272" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11308"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="10" class="linaBajo" style="text-align: left; height:30px;">
                                 <asp:Label ID="lblInstruccion5b" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Si existe personal docente con formaci�n diferente de las especialidades, registre la cantidad de personal de acuerdo con su situaci�n acad�mica y sexo."
                                    Width="600px"></asp:Label>
                           </td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:TextBox ID="txtV1273" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1274" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11402"></asp:TextBox>
                            </td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1275" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1276" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1277" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1278" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1279" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1280" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11408"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1281" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11409"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajoS">
                                <asp:TextBox ID="txtV1282" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1283" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1284" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1285" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1286" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1287" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1288" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1289" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11508"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1290" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11509"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:TextBox ID="txtV1291" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1292" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1293" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1294" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11604"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1295" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11605"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1296" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11606"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1297" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11607"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1298" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11608"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1299" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11609"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:TextBox ID="txtV1300" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo" colspan="">
                                <asp:TextBox ID="txtV1301" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1302" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1303" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11704"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1304" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11705"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1305" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11706"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1306" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11707"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1307" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11708"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1308" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11709"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajoS">
                                <asp:TextBox ID="txtV1309" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1310" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1311" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1312" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11804"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1313" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11805"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1314" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11806"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1315" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11807"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1316" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11808"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1317" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11809"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:TextBox ID="txtV1318" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1319" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1320" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1321" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11904"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1322" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11905"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1323" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11906"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1324" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="11907"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1325" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11908"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1326" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11909"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:TextBox ID="txtV1327" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1328" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1329" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1330" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12004"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1331" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12005"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1332" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12006"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1333" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12007"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1334" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12008"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1335" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12009"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajoS">
                                <asp:TextBox ID="txtV1598" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="12101"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1599" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1600" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12103"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1601" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12104"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1602" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12105"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1603" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12106"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1604" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12107"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoE">
                                <asp:TextBox ID="txtV1605" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12108"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV1606" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12109"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajo">
                                <asp:TextBox ID="txtV1607" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="12201"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1608" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12202"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1609" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12203"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1610" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12204"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1611" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12205"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1612" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12206"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1613" runat="server" Columns="3" MaxLength="2" CssClass="lblNegro" TabIndex="12207"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoV">
                                <asp:TextBox ID="txtV1614" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12208"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV1615" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12209"></asp:TextBox></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td colspan="3" class="linaBajo">
                                <asp:Label ID="lblTotal53" runat="server" CssClass="lblGrisTit" Text="TOTAL (Suma de HOM y MUJ)"
                                    Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV1336" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12301"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('Desglose_CAM_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Desglose_CAM_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="OpenPageCharged('Personal2_CAM_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Personal2_CAM_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                   
                function PintaOPTs(){
                     marcar('V1157');
                     marcar('V1158');
                     marcar('V1163');
                     marcar('V1164');
                     marcar('V1165');
                     marcar('V1166');
                     marcar('V1167');
                     marcar('V1168');
                }  
                
                function marcar(variable){
                     var txtv = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                     if (txtv != null) {
                         var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                         if (txtv.value == 'X'){
                             chk.checked = true;
                         } else {
                             chk.checked = false;
                         } 
                     }
                }
                
                function OPTs2Txt(){
                     marcarTXT('V1157');
                     marcarTXT('V1158');
                     marcarTXT('V1163');
                     marcarTXT('V1164');
                     marcarTXT('V1165');
                     marcarTXT('V1166');
                     marcarTXT('V1167');
                     marcarTXT('V1168');
                }    
                function marcarTXT(variable){
                     //alert(variable);
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         
                         var txt = document.getElementById('ctl00_cphMainMaster_txt'+variable);
                         if (chk.checked)
                         {
                             txt.value = 'X';
                            // alert(txt.value);
                             }
                         else
                             txt.value = '_';
                     }
                }
                
                 function SleccionarCBOs(){
                    var opcion = document.getElementById('ctl00_cphMainMaster_txtV1159').value;
                    if (opcion == "") opcion = 0;
                    document.getElementById('ctl00_cphMainMaster_cboV1160').options[opcion].selected = true;
                    //document.getElementById('txtV857').value = document.getElementById('cboV857').options[opcion].text;
                     
                    var opcion2 = document.getElementById('ctl00_cphMainMaster_txtV1161').value;
                     if (opcion2 == "") opcion2 = 0;
                    document.getElementById('ctl00_cphMainMaster_cboV1162').options[opcion2].selected = true;
                    //document.getElementById('txtV859').value = document.getElementById('cboV859').options[opcion].text;
                }
                function cboChangeV1160(obj){
                        if (obj!= null){
                       	    var texto 
   	                        var indice = obj.selectedIndex ;
   	                        var valor = obj.options[indice].value ;
   	                        var textoEscogido = obj.options[indice].text ;
   	                        document.getElementById('ctl00_cphMainMaster_txtV1159').value = valor;
   	                        document.getElementById('ctl00_cphMainMaster_txtV1160').value = textoEscogido;
   	                    }
                }
                
                function cboChangeV1162(obj){
                        if (obj!= null){
                       	    var texto 
   	                        var indice = obj.selectedIndex ;
   	                        var valor = obj.options[indice].value ;
   	                        var textoEscogido = obj.options[indice].text ;
   	                        document.getElementById('ctl00_cphMainMaster_txtV1161').value = valor;
   	                        document.getElementById('ctl00_cphMainMaster_txtV1162').value = textoEscogido;
   	                    }
                }
                
                
                function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }    
                var ir1;
                function OpenPageCharged(ventana,ir){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1160'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1162'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1597'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1273'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1282'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1291'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1300'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1309'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1318'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1327'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1598'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV1607'));
                    ir1=ir;
                    openPage(ventana,ir1);
                } 
                PintaOPTs();
                SleccionarCBOs();                                               
 		Disparador(<%=hidDisparador.Value %>);
            GetTabIndexes();
        </script> 
</asp:Content>
