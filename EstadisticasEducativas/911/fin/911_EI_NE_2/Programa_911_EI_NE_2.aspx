<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="EI-NE2(Programa)" AutoEventWireup="true" CodeBehind="Programa_911_EI_NE_2.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_EI_NE_2.Programa_911_EI_NE_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />

    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 8;
        MaxRow = 20;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>


    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">


    <table style=" padding-left:200px;">
        <tr><td><span>EDUCACI�N INICIAL NO ESCOLARIZADA</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_EI_NE_2',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Grupos_911_EI_NE_2',true)"><a href="#" title=""><span>GRUPOS</span></a></li>
        <li onclick="openPage('AG_911_EI_NE_2',true)"><a href="#" title=""><span>NI�OS</span></a></li>
        <li onclick="openPage('Personal_911_EI_NE_2',true)"><a href="#" title=""><span>PADRES DE FAMILIA Y PERSONAL</span></a></li>
        <li onclick="openPage('Programa_911_EI_NE_2',true)"><a href="#" title="" class="activo"><span>PROGRAMA</span></a></li>
        <li onclick="openPage('Espacios_911_EI_NE_2',false)"><a href="#" title=""><span>ESPACIOS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
        </ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

        <center>
       
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td style="width: 340px; text-align: center;">
                    <table id="TABLE1" style="text-align: center" >
                        <tr>
                            <td colspan="8" rowspan="1" style=" width: 215px; text-align: left">
                                <asp:Label ID="lblPrograma" runat="server" CssClass="lblRojo" Font-Bold="True" Text="V. AVANCE DE PROGRAMA"
                                    Width="250px" Font-Size="16px"></asp:Label><br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba el n�mero de grupos seg�n el grado de avance del programa, por delegaci�n o municipio y localidad o colonia."
                                    Width="770px"></asp:Label><br />
                            </td>
                            <td colspan="1" rowspan="1" style="width: 215px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" class="linaBajoAlto">
                                &nbsp;</td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblInvestigacion" runat="server" CssClass="lblRojo" Text="INVESTIGACI�N DE CAMPO" Width="100px"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblComites" runat="server" CssClass="lblRojo" Text="FORMACI�N DE COMIT�S" Width="100px"></asp:Label></td>
                            <td rowspan="1" class="linaBajoAlto">
                                <asp:Label ID="lblReclutamiento" runat="server" CssClass="lblRojo" Text="RECLUTAMIENTO DE EDUCADORES COMUNITARIOS" Width="100px"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblCapacitacion" runat="server" CssClass="lblRojo" Text="CAPACITACI�N DE EDUCADORES COMUNITARIOS" Width="100px"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblGpoDePadres" runat="server" CssClass="lblRojo" Text="FORMACI�N DE GRUPOS DE PADRES"
                                    Width="100px"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblSesionCap" runat="server" CssClass="lblRojo" Text="SESI�N DE CAPACITACI�N PERMANENTE" Width="100px"></asp:Label></td>
                            <td class="linaBajoAlto">
                                <asp:Label ID="lblClausura" runat="server" CssClass="lblRojo" Text="CLAUSURA" Width="100px"></asp:Label></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr1" runat="server">
                            <td class="linaBajo">
                                <asp:Label ID="lblLoc1" runat="server" CssClass="lblGrisTit" Text="1" Height="17px" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV303" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV304" runat="server" Columns="3" TabIndex="10102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV305" runat="server" Columns="3" TabIndex="10103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV306" runat="server" Columns="3" TabIndex="10104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV307" runat="server" Columns="3" TabIndex="10105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV308" runat="server" Columns="3" TabIndex="10106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV309" runat="server" Columns="3" TabIndex="10107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr2" runat="server">
                            <td class="linaBajo">
                                <asp:Label ID="lblLoc2" runat="server" CssClass="lblGrisTit" Height="17px" Text="2" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV310" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV311" runat="server" Columns="3" TabIndex="10202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV312" runat="server" Columns="3" TabIndex="10203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV313" runat="server" Columns="3" TabIndex="10204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV314" runat="server" Columns="3" TabIndex="10205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV315" runat="server" Columns="3" TabIndex="10206" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV316" runat="server" Columns="3" TabIndex="10207" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr3" runat="server">
                            <td class="linaBajo">
                            <asp:Label ID="lblLoc3" runat="server" CssClass="lblGrisTit" Text="3" Height="17px" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV317" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV318" runat="server" Columns="3" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV319" runat="server" Columns="3" TabIndex="10303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV320" runat="server" Columns="3" TabIndex="10304" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV321" runat="server" Columns="3" TabIndex="10305" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV322" runat="server" Columns="3" TabIndex="10306" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV323" runat="server" Columns="3" TabIndex="10307" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr4" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc4" runat="server" CssClass="lblGrisTit" Height="17px" Text="4" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV324" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV325" runat="server" Columns="3" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV326" runat="server" Columns="3" TabIndex="10403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV327" runat="server" Columns="3" TabIndex="10404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV328" runat="server" Columns="3" TabIndex="10405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV329" runat="server" Columns="3" TabIndex="10406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV330" runat="server" Columns="3" TabIndex="10407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr5" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc5" runat="server" CssClass="lblGrisTit" Height="17px" Text="5" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV331" runat="server" Columns="3" TabIndex="10501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV332" runat="server" Columns="3" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV333" runat="server" Columns="3" TabIndex="10503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV334" runat="server" Columns="3" TabIndex="10504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV335" runat="server" Columns="3" TabIndex="10505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV336" runat="server" Columns="3" TabIndex="10506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV337" runat="server" Columns="3" TabIndex="10507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr6" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc6" runat="server" CssClass="lblGrisTit" Height="17px" Text="6" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV338" runat="server" Columns="3" TabIndex="10601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV339" runat="server" Columns="3" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV340" runat="server" Columns="3" TabIndex="10603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV341" runat="server" Columns="3" TabIndex="10604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV342" runat="server" Columns="3" TabIndex="10605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV343" runat="server" Columns="3" TabIndex="10606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV344" runat="server" Columns="3" TabIndex="10607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr7" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc7" runat="server" CssClass="lblGrisTit" Height="17px" Text="7" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV345" runat="server" Columns="3" TabIndex="10701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV346" runat="server" Columns="3" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV347" runat="server" Columns="3" TabIndex="10703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV348" runat="server" Columns="3" TabIndex="10704" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV349" runat="server" Columns="3" TabIndex="10705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV350" runat="server" Columns="3" TabIndex="10706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV351" runat="server" Columns="3" TabIndex="10707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr8" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc8" runat="server" CssClass="lblGrisTit" Height="17px" Text="8"
                                    Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV352" runat="server" Columns="3" TabIndex="10801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV353" runat="server" Columns="3" TabIndex="10802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV354" runat="server" Columns="3" TabIndex="10803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV355" runat="server" Columns="3" TabIndex="10804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV356" runat="server" Columns="3" TabIndex="10805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV357" runat="server" Columns="3" TabIndex="10806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV358" runat="server" Columns="3" TabIndex="10807" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr9" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc9" runat="server" CssClass="lblGrisTit" Height="17px" Text="9" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV359" runat="server" Columns="3" TabIndex="10901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV360" runat="server" Columns="3" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV361" runat="server" Columns="3" TabIndex="10903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV362" runat="server" Columns="3" TabIndex="10904" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV363" runat="server" Columns="3" TabIndex="10905" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV364" runat="server" Columns="3" TabIndex="10906" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV365" runat="server" Columns="3" TabIndex="10907" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr10" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc10" runat="server" CssClass="lblGrisTit" Height="17px" Text="10" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV366" runat="server" Columns="3" TabIndex="11001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV367" runat="server" Columns="3" TabIndex="11002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV368" runat="server" Columns="3" TabIndex="11003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV369" runat="server" Columns="3" TabIndex="11004" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV370" runat="server" Columns="3" TabIndex="11005" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV371" runat="server" Columns="3" TabIndex="11006" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV372" runat="server" Columns="3" TabIndex="11007" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="tr11" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc11" runat="server" CssClass="lblGrisTit" Height="17px" Text="11" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV373" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV374" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV375" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV376" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV377" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV378" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV379" runat="server" Columns="3" TabIndex="11107" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr12" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc12" runat="server" CssClass="lblGrisTit" Height="17px" Text="12" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV380" runat="server" Columns="3" TabIndex="11201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV381" runat="server" Columns="3" TabIndex="11202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV382" runat="server" Columns="3" TabIndex="11203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV383" runat="server" Columns="3" TabIndex="11204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV384" runat="server" Columns="3" TabIndex="11205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV385" runat="server" Columns="3" TabIndex="11206" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV386" runat="server" Columns="3" TabIndex="11207" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr13" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc13" runat="server" CssClass="lblGrisTit" Height="17px" Text="13" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV387" runat="server" Columns="3" TabIndex="11301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV388" runat="server" Columns="3" TabIndex="11302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV389" runat="server" Columns="3" TabIndex="11303" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV390" runat="server" Columns="3" TabIndex="11304" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV391" runat="server" Columns="3" TabIndex="11305" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV392" runat="server" Columns="3" TabIndex="11306" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV393" runat="server" Columns="3" TabIndex="11307" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr14" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc14" runat="server" CssClass="lblGrisTit" Height="17px" Text="14" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV394" runat="server" Columns="3" TabIndex="11401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV395" runat="server" Columns="3" TabIndex="11402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV396" runat="server" Columns="3" TabIndex="11403" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV397" runat="server" Columns="3" TabIndex="11404" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV398" runat="server" Columns="3" TabIndex="11405" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV399" runat="server" Columns="3" TabIndex="11406" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV400" runat="server" Columns="3" TabIndex="11407" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr15" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc15" runat="server" CssClass="lblGrisTit" Text="15" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV401" runat="server" Columns="3" TabIndex="11501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV402" runat="server" Columns="3" TabIndex="11502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV403" runat="server" Columns="3" TabIndex="11503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV404" runat="server" Columns="3" TabIndex="11504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV405" runat="server" Columns="3" TabIndex="11505" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV406" runat="server" Columns="3" TabIndex="11506" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV407" runat="server" Columns="3" TabIndex="11507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr16" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc16" runat="server" CssClass="lblGrisTit" Text="16" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV408" runat="server" Columns="3" TabIndex="11601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV409" runat="server" Columns="3" TabIndex="11602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV410" runat="server" Columns="3" TabIndex="11603" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV411" runat="server" Columns="3" TabIndex="11604" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV412" runat="server" Columns="3" TabIndex="11605" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV413" runat="server" Columns="3" TabIndex="11606" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV414" runat="server" Columns="3" TabIndex="11607" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr17" runat="server">
                            <td rowspan="1" class="linaBajo">
                            <asp:Label ID="lblLoc17" runat="server" CssClass="lblGrisTit" Text="17" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV705" runat="server" Columns="3" TabIndex="11701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV706" runat="server" Columns="3" TabIndex="11702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV707" runat="server" Columns="3" TabIndex="11703" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV708" runat="server" Columns="3" TabIndex="11704" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV709" runat="server" Columns="3" TabIndex="11705" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV710" runat="server" Columns="3" TabIndex="11706" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV711" runat="server" Columns="3" TabIndex="11707" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr id="tr18" runat="server">
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblLoc18" runat="server" CssClass="lblGrisTit" Text="18" Width="67px"></asp:Label></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV712" runat="server" Columns="3" TabIndex="11801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV713" runat="server" Columns="3" TabIndex="11802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV714" runat="server" Columns="3" TabIndex="11803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV715" runat="server" Columns="3" TabIndex="11804" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV716" runat="server" Columns="3" TabIndex="11805" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV717" runat="server" Columns="3" TabIndex="11806" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="linaBajoS">
                                <asp:TextBox ID="txtV718" runat="server" Columns="3" TabIndex="11807" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" class="linaBajo">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV415" runat="server" Columns="3" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV416" runat="server" Columns="3" TabIndex="11902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV417" runat="server" Columns="3" TabIndex="11903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV418" runat="server" Columns="3" TabIndex="11904" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV419" runat="server" Columns="3" TabIndex="11905" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV420" runat="server" Columns="3" TabIndex="11906" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV421" runat="server" Columns="3" TabIndex="11907" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td class="Orila">
                                &nbsp;</td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
   
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal_911_EI_NE_2',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_EI_NE_2',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Espacios_911_EI_NE_2',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Espacios_911_EI_NE_2',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

    
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
       
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
