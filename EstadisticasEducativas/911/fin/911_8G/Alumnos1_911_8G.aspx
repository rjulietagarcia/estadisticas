<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8G(Alumnos)" AutoEventWireup="true" CodeBehind="Alumnos1_911_8G.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_8G.Alumnos1_911_8G" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/balloontip.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="/../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 16;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

     <div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">


    <table style=" padding-left:200px;">
        <tr><td><span>BACHILLERATO GENERAL</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8G',true)" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos1_911_8G',true)"><a href="#" title="" class="activo"><span>ALUMNOS Y GRUPOS</span></a></li>
        <li onclick="openPage('Alumnos2_911_8G',false)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL Y PLANTELES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
       <center>
   

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table >
            <tr>
                <td colspan="2" valign="top">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="OBSERVACI�N: INCLUYA EN TODAS LAS PREGUNTAS DE ESTA SECCI�N A LOS ALUMNOS Y GRUPOS DE LOS PLANTELES DE EXTENSI�N O M�DULOS."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <asp:Label ID="Label35" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                        Text="I. ALUMNOS Y GRUPOS" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <table style="width: 600px">
                        <tr>
                            <td>
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba el total de alumnos desglosando la inscripci�n total, la existencia, los aprobados en todas las asignaturas y los reprobados de una a cinco asignaturas, seg�n el sexo."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                    <table>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRADO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SEMESTRE"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblSexo" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SEXO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblNegro" Font-Bold="True"
                        Text="INSCRIPCI�N TOTAL" Width="100px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblNegro" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblNegro" Font-Bold="True" Text="APROBADOS EN TODAS LAS ASIGNATURAS"
                        Width="100px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblReprobados" runat="server" CssClass="lblNegro" Font-Bold="True" Text="REPROBADOS DE 1 A 5 ASIGNATURAS"
                        Width="95px"></asp:Label></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 Y 2"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV1" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV2" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV3" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV4" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV5" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV6" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV7" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV8" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
            </tr><tr>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 Y 4"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV9" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV10" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV11" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV12" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV13" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV14" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV15" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV16" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 Y 6"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV17" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV18" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV19" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV20" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV21" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV22" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV23" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV24" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblNegro" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 Y 8"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV25" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV26" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV27" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV28" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblMuj4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV29" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV30" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV31" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV32" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV33" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV34" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV35" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV36" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="lblInstruccion2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. De la existencia total, escriba el n�mero de alumnos de tercer grado, seg�n el �rea de estudios o capacitaci�n que cursen, por ejemplo: Bachillerato en ciencias fisico-matem�ticas y naturales, ciencias sociales y humanidades; iniciaci�n a la inform�tica o a la pr�ctica docente, etc�tera."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td rowspan="2" style="text-align: center">
                    <asp:Label ID="lbl29" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NOMBRE DEL �REA"
                        Width="100%"></asp:Label></td>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="lblAlumnos" runat="server" CssClass="lblNegro" Font-Bold="True" Text="A L U M N O S"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="text-align: center">
                    <asp:Label ID="lblHombres2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblMujeres2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblTotal2a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblnum1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV37" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV38" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV39" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV40" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblnum2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV41" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV42" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV43" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV44" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblnum3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV45" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV46" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV47" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV48" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblnum4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV49" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV50" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV51" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV52" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblnum5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV53" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV54" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV55" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV56" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblnum6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="6"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV57" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV58" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV59" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV60" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                    <asp:Label ID="lblTotal2b" runat="server" CssClass="lblNegro" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV61" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11661"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table style="width: 500px">
                        <tr>
                            <td>
                    <asp:Label ID="lblInstruccion3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. De la existencia total, escriba el n�mero de alumnos de nacionalidad extranjera, deslos�ndolo por sexo."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblTotal3a" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEU" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ESTADOS UNIDOS"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV62" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20101" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV63" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20102" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV64" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20103" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCANADA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CANAD�"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV65" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20201" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV66" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20202" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV67" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20203" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCAYC" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CENTROAM�RICA Y EL CARIBE"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV68" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20301" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV69" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20302" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV70" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20303" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblSUDAM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUDAM�RICA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV71" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20401" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV72" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20402" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV73" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20403" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblAFRICA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="�FRICA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV74" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20501" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV75" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20502" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV76" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20503" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblASIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ASIA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV77" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20601" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV78" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20602" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV79" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20603" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEUROPA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EUROPA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV80" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20701" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV81" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20702" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV82" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20703" ></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblOCEANIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OCEAN�A"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV83" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20801" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV84" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20802" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV85" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20803" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblTotal3b" runat="server" CssClass="lblNegro" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV86" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="20931" ></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                    <asp:Label ID="lblInstruccion4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Escriba, por grado, el n�mero de grupos existentes."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblGRUPOS" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o. (1o. y 2o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV87" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21001" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem2y3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o. (3o. y 4o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV88" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21101" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem4y5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o. (5o. y 6o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV89" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21201" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o. (7o. y 8o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV90" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21301" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTotal4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV91" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21401" ></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_8G',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_8G',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Alumnos2_911_8G',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Alumnos2_911_8G',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        
        </center>
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

        
       <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                function Guines(){
                     Establecer('ctl00_cphMainMaster_txtV37');
                     Establecer('ctl00_cphMainMaster_txtV41');
                     Establecer('ctl00_cphMainMaster_txtV45');
                     Establecer('ctl00_cphMainMaster_txtV49');
                     Establecer('ctl00_cphMainMaster_txtV53');
                     Establecer('ctl00_cphMainMaster_txtV57');
                }     
                function Establecer(ID){
                    var valor = document.getElementById(ID).value;
                    if (valor == '' || valor == '0')
                        document.getElementById(ID).value = '_';
                }
                Guines();         
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>

