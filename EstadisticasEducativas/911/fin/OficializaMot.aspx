﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OficializaMot.aspx.cs" Inherits="EstadisticasEducativas._911.fin.OficializaMot" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Oficialización con motivo </title>
<link href="../../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../../tema/css/estilo.css" type="text/css" rel="stylesheet">
    <link href="../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
     <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <link href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script> 
 
    <style type="text/css"  >
       
       .Bordes1{
         border-right: #990066 1px solid; border-top: #990066 1px solid; border-left: #990066 1px solid; border-bottom: #990066 1px solid;
         background-color: olive;
       }
       .Bordes2{
         border-right: #990066 1px solid; border-top: #990066 1px solid; border-left: #990066 1px solid; border-bottom: #990066 1px solid;
         background-color: #777777;
       }
    </style>
</head>
<body>
    <form id="fOficializa" runat="server">
    <div id="Cuerpo">
     
   
     
 
   <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>
            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

 
                        <table id="Table2" cellspacing="0" cellpadding="0" width="440" border="0"> 
                                <tr> 
                                    <td> 
                                    </td> 
                                </tr> 
                                <tr> 
                                    <td> 
                                        <table style="width: 620px">
                    <tr>
                        <td colspan="2" style="height: 28px">
                            <br />
                            <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Text="MOTIVO DE NO OFICIALIZAR" Font-Size="16pt"></asp:Label></td>
                    </tr>
                    <tr>
                        <td  colspan="2" >
                           </td>
                           
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;<asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="Declaro los datos ingresados en el cuestionario como verídicos y finalizo la captura de los mismos." Width="264px"></asp:Label></td>
                        <td>
                            <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Text="No fue posible contestar el cuestionario por el siguiente motivo:"
                                Width="222px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Button ID="cmdOficializar" runat="server" CssClass="botones2" Text="Oficializar" OnClick="cmdOficializar_Click" /></td>
                        <td>
                            <asp:DropDownList ID="ddlMotivos" runat="server" Width="228px" CssClass="lblNegro">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Text="Generar comprobante de captura."></asp:Label></td>
                        <td>
                            <asp:Button ID="cmdGenerarComprobante" runat="server" CssClass="botones2" Text="Aceptar" OnClick="cmdGenerarComprobante_Click" /></td>
                    </tr>
                     
                  
                    
                    <tr>
                        <td style="width: 310px">
                            <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Text="Imprimir solo el formato del cuestionario."></asp:Label></td>
                        <td>
                            <asp:Button ID="cmdImprimircuestionario" runat="server" CssClass="botones2" Text="Aceptar" OnClick="cmdImprimircuestionario_Click" /></td>
                    </tr>
                </table>
                                    </td> 
                                </tr> 
                                <tr> 
                                    <td> 
                                    </td> 
                                </tr> 
                            </table> 
            <div > 
                <asp:Label ID="lblResultado" runat="server"></asp:Label>
            </div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


            
            <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
            <input id="hidIdCtrl" type="hidden" runat= "server" />
            <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
            <script type="text/javascript">
                function openPage(page){window.location.href=page+".aspx";}
            </script> 
        
         <asp:Table runat="server" ID="table1" Visible="False">
             <asp:TableRow ID="TableRow1" runat="server">
                 <asp:TableCell ID="TableCell1" runat="server" CssClass="Bordes1">Base de Datos</asp:TableCell>
                 <asp:TableCell ID="TableCell2" runat="server" CssClass="Bordes2">Web Service</asp:TableCell>
             </asp:TableRow> 
          </asp:Table>
         
        <center>
            &nbsp;</center>
        <center>
         <asp:Table ID="tblDatos" runat="server" Visible="False" >    
             <asp:TableRow ID="TableRow2" runat="server">
                 <asp:TableCell ID="TableCell3" runat="server" CssClass="BordesX">NoVar</asp:TableCell>
                 <asp:TableCell ID="TableCell4" runat="server" CssClass="BordesX">Valor</asp:TableCell>
             </asp:TableRow>
         </asp:Table>
            &nbsp;</center>
        <center>
            <asp:Table ID="tblFallas" runat="server" Visible="False" >
                <asp:TableRow ID="TableRow3" runat="server">
                    <asp:TableCell ID="TableCell5" runat="server" CssClass="BordesX">NoVar</asp:TableCell>
                    <asp:TableCell ID="TableCell6" runat="server" CssClass="BordesX">Descripción</asp:TableCell>
                    <asp:TableCell ID="TableCell7" runat="server" CssClass="BordesX">Inconsitencia</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </center>
         <asp:HiddenField ID="hidRuta" runat="server" />
          <asp:Literal ID="js" runat="server"></asp:Literal>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
           
         
 <script language = "javascript" type="text/javascript">
       // motivadoOficializar();
           function AbrirPDF(){
               var ruta = document.getElementById("hidRuta").value
               if (ruta != ""){
                    window.open (ruta,'','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668')
               }
           }
           AbrirPDF();
        </script>
    
    </div>
    </form>
</body>
</html>
