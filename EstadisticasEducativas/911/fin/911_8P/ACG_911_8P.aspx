﻿ <%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.8P (Alumnos por Carrera)" AutoEventWireup="true" CodeBehind="ACG_911_8P.aspx.cs" Inherits="EstadisticasEducativas._911.fin._911_8P.ACG_911_8P" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <%--<link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
  
    <script type="text/javascript">
        var _enter=true;
    </script>
    <%--<style type="text/css">
    .linaBajo{
       text-align: center; 
       width: 110px; 
       border-bottom:solid 1px;
       border-left:solid 1px;
    }
    .linaBajoAlto{
       text-align: center; 
       width: 110px; 
       border-bottom:solid 1px;
       border-top:solid 1px;
       border-left:solid 1px;
    }
    .Orila{
       border-left:solid 1px;
    }
    </style>--%>
    
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 16;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1300px; height:65px;">
    <div id="header">


    <table style=" padding-left:300px;">
        <tr><td><span>PROFESIONAL TÉCNICO MEDIO</span></td>
        </tr>
        <tr><td><span>2012-2013</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>

    
    <div id="menu" style="min-width:1300px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_8P',true)"><a href="#" title=""><span>IDENTIFICACIÓN</span></a></li>
        <li onclick="openPage('ACG_911_8P',true)"><a href="#" title="" class="activo"><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('AGD_911_8P',false)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>1°, 2° y 3°</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>4°, 5° y TOTAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROCEIES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>INMUEBLE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACIÓN</span></a></li></ul>
    </div>
<br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

         <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

          
                    <table style="width: 830px">
                        <tr>
                            <td colspan="7" style="padding-bottom: 10px; text-align: left">
                                <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="16px"
                                    Text="I. ALUMNOS POR CARRERA Y GRUPOS" Width="100%"></asp:Label></td>
                            <td colspan="1" style="padding-bottom: 10px; text-align: left;">
                            </td>
                            <td colspan="1" style="padding-bottom: 10px; width: 198px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" style="text-align: left">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba por carrera el número de alumnos. Desglose la inscripción total, la existencia, los aprobados en todas las asignaturas y los reprobados de una a cinco asignaturas, según el sexo."
                        Width="100%"></asp:Label></td>
                            <td colspan="1" style="text-align: left;">
                            </td>
                            <td colspan="1" style="width: 198px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" style="height: 19px; text-align: left">
                            </td>
                            <td colspan="1" style="height: 19px; text-align: left;">
                            </td>
                            <td colspan="1" style="width: 198px; height: 19px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 19px; text-align: left">
                                <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Nombre de la carrera"
                                    Width="140px"></asp:Label></td>
                            <td colspan="5" style="height: 19px; text-align: left">
                                &nbsp;<asp:TextBox ID="txtVA1" runat="server" Columns="30" MaxLength="30" TabIndex="10101" CssClass="lblNegro"></asp:TextBox>
                                &nbsp; &nbsp; &nbsp;
                                <asp:Label id="Label10" runat="server" Text="ID" Font-Bold="True" CssClass="lblRojo"></asp:Label>
                                <asp:TextBox ID="txtVA0" runat="server" BackColor="Bisque" Columns="5" Font-Bold="True"
                                    MaxLength="5" ReadOnly="True" Style="text-align: right" Width="24px">0</asp:TextBox></td>
                            <td colspan="1" style="height: 19px; text-align: left;">
                            </td>
                            <td colspan="1" style="width: 198px; height: 19px; text-align: left">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 110px; text-align: left">
                                <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="CLAVE"
                                    Width="60px"></asp:Label></td>
                            <td colspan="" style="width: 110px; text-align: center">
                                <asp:TextBox ID="txtVA2" runat="server" Columns="8" MaxLength="9" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 110px; text-align: right">
                                &nbsp;<asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DURACIÓN"
                                    Width="60px"></asp:Label></td>
                            <td colspan="2" style="width: 110px; text-align: left">
                                <asp:TextBox ID="txtVA3" runat="server" Columns="30" MaxLength="30" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 110px; text-align: right">
                                <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ÁREA DE ESTUDIO"
                                    Width="105px"></asp:Label></td>
                            <td style="text-align: left" colspan="3">
                                <asp:TextBox ID="txtVA4" runat="server" Columns="1" MaxLength="1" TabIndex="10303" CssClass="lblNegro"></asp:TextBox>
                                <asp:DropDownList ID="ddl_Areas" runat="server" onchange="OnChange_Area(this)">
                                </asp:DropDownList></td>
                        </tr>
            <tr >
                <td class="linaBajoAlto">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                        Width="60px"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEMESTRES"
                        Width="80px"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblSexo" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEXO"
                        Width="80px"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="INSCRIPCIÓN TOTAL" Width="100px"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EXISTENCIA"
                        Width="100px"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="APROBADOS EN TODAS LAS ASIGNATURAS"
                        Width="100px"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblReprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="REPROBADOS DE 1 A 5 ASIGNATURAS"
                        Width="100px"></asp:Label></td>
                <td class="Orila">&nbsp;
                   
                </td>
                <td style="width: 198px">
                </td>
            </tr>
            <tr >
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 Y 2"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA5" runat="server" Columns="5" MaxLength="5" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA6" runat="server" Columns="5" MaxLength="5" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA7" runat="server" Columns="5" MaxLength="5" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA8" runat="server" Columns="5" MaxLength="5" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td style="width: 198px">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA9" runat="server" Columns="5" MaxLength="5" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA10" runat="server" Columns="5" MaxLength="5" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA11" runat="server" Columns="5" MaxLength="5" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA12" runat="server" Columns="5" MaxLength="5" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td style="width: 198px">
                </td>
            </tr><tr>
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 Y 4"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA13" runat="server" Columns="5" MaxLength="5" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA14" runat="server" Columns="5" MaxLength="5" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA15" runat="server" Columns="5" MaxLength="5" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA16" runat="server" Columns="5" MaxLength="5" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                    </td>
                <td style="width: 198px">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA17" runat="server" Columns="5" MaxLength="5" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA18" runat="server" Columns="5" MaxLength="5" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA19" runat="server" Columns="5" MaxLength="5" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA20" runat="server" Columns="5" MaxLength="5" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td style="width: 198px">
                </td>
            </tr>
            <tr>
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 Y 6"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA21" runat="server" Columns="5" MaxLength="5" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA22" runat="server" Columns="5" MaxLength="5" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA23" runat="server" Columns="5" MaxLength="5" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA24" runat="server" Columns="5" MaxLength="5" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td style="width: 198px">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA25" runat="server" Columns="5" MaxLength="5" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA26" runat="server" Columns="5" MaxLength="5" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA27" runat="server" Columns="5" MaxLength="5" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA28" runat="server" Columns="5" MaxLength="5" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td style="width: 198px">
                </td>
            </tr>
            <tr>
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td rowspan="2" class="linaBajo">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 Y 8"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lbl6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA29" runat="server" Columns="5" MaxLength="5" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA30" runat="server" Columns="5" MaxLength="5" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA31" runat="server" Columns="5" MaxLength="5" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA32" runat="server" Columns="5" MaxLength="5" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td style="width: 198px">
                </td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMuj4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA33" runat="server" Columns="5" MaxLength="5" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA34" runat="server" Columns="5" MaxLength="5" TabIndex="11202" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA35" runat="server" Columns="5" MaxLength="5" TabIndex="11203" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA36" runat="server" Columns="5" MaxLength="5" TabIndex="11204" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td style="width: 198px">
                </td>
            </tr>
                        <tr>
                            <td rowspan="2" class="linaBajo">
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5o."
                                    Width="100%"></asp:Label></td>
                            <td rowspan="2" class="linaBajo">
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="9 Y 10"
                                    Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRES"
                                    Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtVA37" runat="server" Columns="5" MaxLength="5" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtVA38" runat="server" Columns="5" MaxLength="5" TabIndex="11302" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtVA39" runat="server" Columns="5" MaxLength="5" TabIndex="11303" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtVA40" runat="server" Columns="5" MaxLength="5" TabIndex="11304" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                            <td style="width: 198px">
                            </td>
                        </tr>
                        <tr>
                            <td class="linaBajo">
                                <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                                    Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtVA41" runat="server" Columns="5" MaxLength="5" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtVA42" runat="server" Columns="5" MaxLength="5" TabIndex="11402" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtVA43" runat="server" Columns="5" MaxLength="5" TabIndex="11403" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtVA44" runat="server" Columns="5" MaxLength="5" TabIndex="11404" CssClass="lblNegro"></asp:TextBox></td>
                            <td class="Orila">&nbsp;
                            </td>
                            <td style="width: 198px">
                            </td>
                        </tr>
            <tr>
                <td colspan="3" style="text-align: center;" class="linaBajo">
                    <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA45" runat="server" Columns="5" MaxLength="5" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA46" runat="server" Columns="5" MaxLength="5" TabIndex="11502" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA47" runat="server" Columns="5" MaxLength="5" TabIndex="11503" CssClass="lblNegro"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtVA48" runat="server" Columns="5" MaxLength="5" TabIndex="11504" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila">&nbsp;
                </td>
                <td style="width: 198px">
                </td>
            </tr>
        </table>
        
          <table>
          <tr>
                    <td>
                        <asp:LinkButton ID="lnkEliminarCarrera" runat="server" OnClick="lnkEliminarCarrera_Click">Eliminar Carrera Actual</asp:LinkButton>
                    </td>
                </tr>
                <tr><td>
                    Carreras:
                </td>
                    <td style="font-weight: bold;">
                       <asp:DropDownList ID="ddlID_Carrera" runat="server"  onchange='OnChange(this);'>
                       </asp:DropDownList > de 
                       <asp:TextBox ID="lblUnodeN" runat="server" Columns="5" MaxLength="5" Width="24px" BackColor="Bisque" Font-Bold="True" ReadOnly="True">0</asp:TextBox>
                    </td>
                    <td id="td_Nuevo" runat="server"><span onclick = "navegarCarreras_new(-1);" >&nbsp;<a href="#">Agregar Otra <strong>*</strong></a></span></td>
                </tr>
            </table>
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_911_8P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir página previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_911_8P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
               <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('AGD_911_8P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_8P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir página siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando información por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
               
                    
      
        
              <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                var ID_Carrera =0; 
                
                
                
                function OnChange_Area(dropdown)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    var SelText = dropdown.options[myindex].text;
                    document.getElementById("ctl00_cphMainMaster_txtVA4").value = SelValue;
                                      
                    return true;
                }
                function OnChange(dropdown)
                {
                    var myindex  = dropdown.selectedIndex
                    var SelValue = dropdown.options[myindex].value
                    navegarCarreras_new(SelValue);
                    return true;
                }
                
                function navegarCarreras_new(id_carrera){
                     ID_Carrera = (id_carrera);
                     var ID_Carreratmp = ID_Carrera; 
                                        
                    if (id_carrera == -1)
                    {
                       var totalRegs = document.getElementById('<%=lblUnodeN.ClientID%>').value;
                       ID_Carrera = eval(totalRegs) + 1; 
                       alert(ID_Carrera);
                    }
                
                    openPage('ACG_911_8P');
                }
               
              
                 function __ReceiveServerData_Variables(rValue){
                   if (rValue != null){
                        document.getElementById("divWait").style.visibility = "hidden";
                        var rows = rValue.split('|');
                        var vi = 1;
                        if (rValue != "")  LimpiarFallas();
                        var detalles="<ul>";
                        for(vi = 1; vi < rows.length; vi ++){
                            var res = rows[vi];
                            var colums = res.split('!');
                            var caja = document.getElementById("ctl00_cphMainMaster_txtV" + colums[0]);
                          
                            if (caja!=null){
                                 caja.className = "txtVarFallas";
                            }
                            detalles = detalles + "<li>" + colums[1]+ "</li>";
                       }
                       if (CambiarPagina != "") {  // para que no se borre la informacion a menos que se mueva de la pagina
                            var obj = document.getElementById('divResultado');
                            obj.innerHTML =  detalles + "</ul>";
                       }
                       var Cambiar = "No";
                       if ( rValue != "" && CambiarPagina != "" ){
                          
                                 if(ir1)
                        {
                           
                            Cambiar = "Si";
                        }
                        else
                        {
                            alert("Se encontraron errores de captura\nDebe corregirlos para avanzar a la siguiente p"+'\u00e1'+"gina.");
                            Cambiar = "No";
                        }
                       }
                       else if(rValue == "" && CambiarPagina != ""){
                          Cambiar = "Si";
                       }
                       if (Cambiar == "Si"){
                             window.location.href=CambiarPagina+".aspx?idC=" + ID_Carrera;
                       } 
                   }
                }

     

             
               
                function OcultarNuevoRegistro(){
                   if (document.getElementById("txtVA6").disabled== true){
                      document.getElementById("NuevoReg").style.display = "none";
                   }
                }
             
                              
    
             
             
                 
               
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
        <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
        
</asp:Content>
