<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="ECC-22(2� Nivel)" AutoEventWireup="true" CodeBehind="Alumnos2_ECC_22.aspx.cs" Inherits="EstadisticasEducativas._911.fin.ECC_22.Alumnos2_ECC_22" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>

    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 28;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>

    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">


    <table style="min-width: 1100px; height: 65px; ">
        <tr><td><span>EDUCACI�N COMUNITARIA RURAL PRIMARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_ECC_22')" style="text-align: left"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Alumnos1_ECC_22')"><a href="#" title="" ><span>PRIMER NIVEL</span></a></li><li onclick="openPage('Alumnos2_ECC_22')"><a href="#" title="" class="activo"><span>SEGUNDO NIVEL</span></a></li><li onclick="openPage('Alumnos3_ECC_22')"><a href="#" title=""><span>TERCER NIVEL</span></a></li><li onclick="openPage('Anexo_ECC_22')"><a href="#" title=""><span>ANEXO</span></a></li><li onclick="openPage('Oficializar_911_ECC_22')"><a href="#" title=""><span>OFICIALIZACI�N</span></a></li></ul>
    </div><br />
<br />
<br />
 <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td colspan="15">
                    <asp:Label ID="lblTituloTabla" runat="server" CssClass="lblNegro" Text="Estad�stica de alumnos por nivel, ciclo (a�o), sexo, inscripci�n total, existencia, aprobados y edad"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 87px;">
                </td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblmenos6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl6a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl7a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl8a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl9a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl10a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl11a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl12a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl13a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl14a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lbl15a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 y m�s a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1er ciclo (1er a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblHombresC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left;" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="120px"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV328" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV329" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV330" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV331" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV332" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV333" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV334" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV335" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV336" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10109"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV337" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10110"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV338" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10111"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV339" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10112"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV340" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV341" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV342" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV343" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV344" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV345" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV346" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV347" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV348" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10209"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV349" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10210"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV350" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10211"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV351" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10212"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS">
                    <asp:Label ID="lblAprobadosHC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV352" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV353" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV354" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV355" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV356" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV357" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV358" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV359" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV360" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10309"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV361" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10310"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV362" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10311"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV363" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10312"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblMujeresC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV364" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV365" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV366" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV367" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV368" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV369" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV370" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV371" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV372" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10409"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV373" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10410"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV374" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10411"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV375" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10412"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV376" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV377" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV378" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV379" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV380" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV381" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV382" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV383" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV384" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10509"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV385" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10510"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV386" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10511"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV387" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10512"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV388" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV389" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV390" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV391" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV392" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV393" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV394" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV395" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV396" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10609"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV397" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10610"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV398" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10611"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV399" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10612"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left;" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV400" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV401" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV402" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV403" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV404" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV405" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV406" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV407" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV408" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10709"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV409" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10710"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV410" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10711"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV411" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10712"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV412" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV413" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV414" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV415" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10804"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV416" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10805"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV417" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10806"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV418" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10807"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV419" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10808"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV420" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10809"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV421" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10810"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV422" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10811"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV423" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10812"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
           
            <tr>
                <td style="text-align: left;" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC1" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV424" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV425" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV426" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV427" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10904"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV428" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10905"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV429" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10906"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV430" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10907"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV431" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10908"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV432" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10909"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV433" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10910"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV434" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10911"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV435" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10912"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
             <tr>
                <td></td>
                <td></td>
                <td></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label9" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 y m�s a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2do ciclo (2do a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblHombresC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left;" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV436" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV437" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV438" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV439" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11004"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV440" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11005"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV441" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11006"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV442" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11007"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV443" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11008"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV444" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11009"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV445" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11010"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV446" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11011"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV447" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11012"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV448" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV449" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV450" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV451" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11104"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV452" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11105"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV453" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11106"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV454" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11107"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV455" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11108"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV456" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11109"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV457" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11110"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV458" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11111"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV459" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11112"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS">
                    <asp:Label ID="lblAprobadosHC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV460" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV461" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV462" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV463" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11204"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV464" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11205"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV465" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11206"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV466" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11207"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV467" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11208"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV468" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11209"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV469" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11210"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV470" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11211"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV471" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11212"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblMujeresC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV472" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV473" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV474" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV475" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11304"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV476" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11305"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV477" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11306"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV478" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11307"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV479" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11308"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV480" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11309"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV481" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11310"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV482" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11311"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV483" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11312"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV484" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV485" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV486" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV487" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11404"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV488" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11405"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV489" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11406"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV490" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11407"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV491" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11408"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV492" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11409"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV493" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11410"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV494" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11411"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV495" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11412"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV496" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV497" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV498" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV499" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11504"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV500" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11505"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV501" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11506"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV502" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11507"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV503" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11508"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV504" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11509"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV505" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11510"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV506" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11511"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV507" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11512"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left;" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV508" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV509" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV510" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV511" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11604"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV512" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11605"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV513" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11606"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV514" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11607"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV515" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11608"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV516" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11609"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV517" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11610"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV518" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11611"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV519" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11612"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV520" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11701"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV521" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11702"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV522" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11703"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV523" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11704"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV524" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11705"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV525" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11706"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV526" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11707"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV527" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11708"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV528" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11709"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV529" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11710"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV530" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11711"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV531" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11712"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV532" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11801"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV533" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11802"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV534" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11803"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV535" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11804"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV536" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11805"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV537" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11806"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV538" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11807"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV539" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11808"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV540" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11809"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV541" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11810"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV542" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11811"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV543" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11812"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
             <tr>
                <td>
                </td>
                <td >
                </td>
                <td></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Menos de 6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label16" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label18" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Text="11 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Font-Bold="True" Text="12 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Font-Bold="True" Text="13 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label22" runat="server" CssClass="lblRojo" Font-Bold="True" Text="14 a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label23" runat="server" CssClass="lblRojo" Font-Bold="True" Text="15 y m�s a�os"
                        Width="60px"></asp:Label></td>
                <td style="vertical-align: bottom;" class="linaBajoAlto">
                    <asp:Label ID="Label24" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="60px"></asp:Label></td>
                <td class="Orila" style="vertical-align: bottom; text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblCiclo3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3er ciclo (3er a�o)"
                        Width="70px"></asp:Label></td>
                <td rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblHombresC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionHC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV544" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11901"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV545" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11902"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV546" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11903"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV547" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11904"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV548" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11905"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV549" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11906"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV550" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11907"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV551" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11908"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV552" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11909"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV553" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11910"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV554" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11911"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV555" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11912"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
           
            <tr>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblExistenciaHC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV556" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12001"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV557" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12002"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV558" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12003"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV559" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12004"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV560" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12005"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV561" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12006"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV562" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12007"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV563" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12008"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV564" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12009"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV565" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12010"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV566" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12011"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV567" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12012"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS">
                    <asp:Label ID="lblAprobadosHC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV568" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12101"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV569" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12102"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV570" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12103"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV571" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12104"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV572" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12105"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV573" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12106"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV574" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12107"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV575" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12108"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV576" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12109"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV577" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12110"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV578" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12111"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV579" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12112"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo">
                    <asp:Label ID="lblMujeresC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblInscripcionMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV580" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12201"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV581" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12202"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV582" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12203"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV583" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12204"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV584" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12205"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV585" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12206"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV586" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12207"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV587" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12208"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV588" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12209"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV589" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12210"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV590" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12211"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV591" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12212"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo">
                    <asp:Label ID="lblExistenciaMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV592" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12301"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV593" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12302"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV594" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12303"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV595" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12304"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV596" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12305"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV597" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12306"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV598" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12307"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV599" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12308"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV600" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12309"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV601" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12310"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV602" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12311"></asp:TextBox></td>
                <td class="linaBajo">
                    <asp:TextBox ID="txtV603" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12312"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS">
                    <asp:Label ID="lblAprobadosMC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV604" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12401"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV605" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12402"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV606" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12403"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV607" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12404"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV608" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12405"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV609" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12406"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV610" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12407"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV611" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12408"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV612" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12409"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV613" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12410"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV614" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12411"></asp:TextBox></td>
                <td class="linaBajoS">
                    <asp:TextBox ID="txtV615" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12412"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td rowspan="3" class="linaBajo Sombreado">
                    <asp:Label ID="lblSubtotalC3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: left;" class="linaBajo Sombreado">
                    <asp:Label ID="lblInscripcionSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSCRIPCI�N TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV616" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12501"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV617" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12502"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV618" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12503"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV619" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12504"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV620" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12505"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV621" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12506"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV622" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12507"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV623" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12508"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV624" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12509"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV625" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12510"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV626" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12511"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV627" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12512"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajo Sombreado">
                    <asp:Label ID="lblExistenciaSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENCIA"
                        Width="100%"></asp:Label></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV628" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12601"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV629" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12602"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV630" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12603"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV631" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12604"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV632" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12605"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV633" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12606"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV634" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12607"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV635" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12608"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV636" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12609"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV637" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12610"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV638" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12611"></asp:TextBox></td>
                <td class="linaBajo Sombreado">
                    <asp:TextBox ID="txtV639" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12612"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: left;" class="linaBajoS Sombreado">
                    <asp:Label ID="lblAprobadosSC3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="APROBADOS"
                        Width="100%"></asp:Label></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV640" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12701"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV641" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12702"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV642" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12703"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV643" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12704"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV644" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12705"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV645" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12706"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV646" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12707"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV647" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12708"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV648" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12709"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV649" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12710"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV650" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12711"></asp:TextBox></td>
                <td class="linaBajoS Sombreado">
                    <asp:TextBox ID="txtV651" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="12712"></asp:TextBox></td>
                <td class="Orila">
                    &nbsp;
                </td>
            </tr>
        </table>
        
       <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Alumnos1_ECC_22')"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Alumnos1_ECC_22')"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                    </td>
                <td ><span  onclick="openPage('Alumnos3_ECC_22')"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Alumnos3_ECC_22')"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
          
        </script>
        
    <%--Agregado--%>
    <script type="text/javascript">
        GetTabIndexes();
    </script>
    <%--Agregado--%>
         
</asp:Content>
