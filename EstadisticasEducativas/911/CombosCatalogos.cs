using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace EstadisticasEducativas._911
{
    public class CombosCatalogos
    {
        #region metodos para hardcodear combos
        
        public  void llenadropsLenguaMaterna(DropDownList drop)
        {
            drop.Items.Add(new ListItem("Seleccione una lengua materna","0"));
            drop.Items.Add(new ListItem("1 AKATEKO VI FAMILIA MAYA", "1"));
            drop.Items.Add(new ListItem("2 AMUZGO V FAMILIA OTO-MANGUE", "2"));
            drop.Items.Add(new ListItem("3 AWAKATEKO VI FAMILIA MAYA", "3"));
            drop.Items.Add(new ListItem("4 AYAPANECO IX FAM. MIXE-ZOQUE", "4"));
            drop.Items.Add(new ListItem("5 CORA II FAMILIA YUTO-NAHUA", "5"));
            drop.Items.Add(new ListItem("6 CUCAP� III FAM COCHIMI-YUMANA", "6"));
            drop.Items.Add(new ListItem("7 CUICATECO V FAM OTO-MANGUE", "7"));
            drop.Items.Add(new ListItem("8 CHATINO V FAMILIA OTO-MANGUE", "8"));
            drop.Items.Add(new ListItem("9 CHICHIMECO JONAZ V FAM OTO-M", "9"));
            drop.Items.Add(new ListItem("10 CHINANTECO V FAM OTO-MANGUE", "10"));
            drop.Items.Add(new ListItem("11 CHOCHOLTECO V FAM OTO-MANGUE", "11"));
            drop.Items.Add(new ListItem("12 CHONTAL DE OAXACA X FAM CH-O", "12"));
            drop.Items.Add(new ListItem("13 CHONTAL DE TABASCO VI FAM MAYA", "13"));
            drop.Items.Add(new ListItem("14 CHUJ VI FAMILIA MAYA", "14"));
            drop.Items.Add(new ListItem("15 CH'OL VI FAMILIA MAYA", "15"));
            drop.Items.Add(new ListItem("16 GUARIJIO II FAM YUTO-NAHUA", "16"));
            drop.Items.Add(new ListItem("17 HUASTECO VI FAMILIA MAYA", "17"));
            drop.Items.Add(new ListItem("18 HUAVE XI FAMILIA HUAVE", "18"));
            drop.Items.Add(new ListItem("19 HUICHOL II FAM YUTO-NAHUA", "19"));
            drop.Items.Add(new ListItem("20 IXCATECO V FAM OTO-MANGUE", "20"));
            drop.Items.Add(new ListItem("21 IXIL VI FAMILIA MAYA", "21"));
            drop.Items.Add(new ListItem("22 JAKALTEKO VI FAMILIA MAYA", "22"));
            drop.Items.Add(new ListItem("23 KAQCHIKEL VI FAMILIA MAYA", "23"));
            drop.Items.Add(new ListItem("24 KICKAPOO I FAMILIA ALGICA", "24"));
            drop.Items.Add(new ListItem("25 KILIWA III FAM COCHIMI-YUMANA", "25"));
            drop.Items.Add(new ListItem("26 KUMIAI III FAM COCHIMI-YUMANA", "26"));
            drop.Items.Add(new ListItem("27 KU'AHL III FAM COCHIMI-YUMANA", "27"));
            drop.Items.Add(new ListItem("28 K'ICHE' VI FAMILIA MAYA", "28"));
            drop.Items.Add(new ListItem("29 LACANDON VI FAMILIA MAYA", "29"));
            drop.Items.Add(new ListItem("30 MAM VI FAMILIA MAYA", "30"));
            drop.Items.Add(new ListItem("31 MATLATZINCA V FAM OTO-MANGUE", "31"));
            drop.Items.Add(new ListItem("32 MAYA VI FAMILIA MAYA", "32"));
            drop.Items.Add(new ListItem("33 MAYO II FAMILIA YUTO-NAHUA", "33"));
            drop.Items.Add(new ListItem("34 MAZAHUA V FAM OTO-MANGUE", "34"));
            drop.Items.Add(new ListItem("35 MAZATECO V FAM OTO-MANGUE", "35"));
            drop.Items.Add(new ListItem("36 MIXE IX FAMILIA MIXE-ZOQUE", "36"));
            drop.Items.Add(new ListItem("37 MIXTECO V FAM OTO-MANGUE", "37"));
            drop.Items.Add(new ListItem("38 NAHUATL II FAM YUTO-NAHUA", "38"));
            drop.Items.Add(new ListItem("39 OLUTECO IX FAM MIXE-ZOQUE", "39"));
            drop.Items.Add(new ListItem("40 OTOMI V FAM OTO-MANGUE", "40"));
            drop.Items.Add(new ListItem("41 PAIPAI III FAM COCHIMI-YUMANA", "41"));
            drop.Items.Add(new ListItem("42 PAME V FAMILIA OTO-MANGUE", "42"));
            drop.Items.Add(new ListItem("43 PAPAGO II FAMILIA YUTO-NAHUA", "43"));
            drop.Items.Add(new ListItem("44 PIMA II FAMILIA YUTO-NAHUA", "44"));
            drop.Items.Add(new ListItem("45 POPOLOCA V FAM OTO-MANGUE", "45"));
            drop.Items.Add(new ListItem("46 POPOLUCA DE LA SIERRA IX FAMILIA MIXE-ZOQUE", "46"));
            drop.Items.Add(new ListItem("47 QATO'K VI FAMILIA MAYA", "47"));
            drop.Items.Add(new ListItem("48 Q'ANJOB'AL VI FAMILIA MAYA", "48"));
            drop.Items.Add(new ListItem("49 Q'EQCHI' VI FAMILIA MAYA", "49"));
            drop.Items.Add(new ListItem("50 SAYULTECO IX FAM MIXE-ZOQUE", "50"));
            drop.Items.Add(new ListItem("51 SERI IV FAMILIA SERI", "51"));
            drop.Items.Add(new ListItem("52 TARAHUMARA II FAM YUTO-NAHUA", "52"));
            drop.Items.Add(new ListItem("53 TARASCO VIII FAMILIA TARASCA", "53"));
            drop.Items.Add(new ListItem("54 TEKO VI FAMILIA MAYA", "54"));
            drop.Items.Add(new ListItem("55 TEPEHUA VII FAM TOTONACO-TEPE", "55"));
            drop.Items.Add(new ListItem("56 TEPEHUANO DEL NORTE II FAM Y-N", "56"));
            drop.Items.Add(new ListItem("57 TEPEHUANO DEL SUR II FAM Y-NA", "57"));
            drop.Items.Add(new ListItem("58 TEXISTEPEQUE�O IX FAM MIXE-Z", "58"));
            drop.Items.Add(new ListItem("59 TOJOLABAL VI FAMILIA MAYA", "59"));
            drop.Items.Add(new ListItem("60 TOTONACO VII FAM TOTONACO-T", "60"));
            drop.Items.Add(new ListItem("61 TRIQUI VI FAM OTO-MANGUE", "61"));
            drop.Items.Add(new ListItem("62 TLAHUICA V FMILIA OTO-MANGUE", "62"));
            drop.Items.Add(new ListItem("63 TLAPANECO V FAMILIA OTO-M", "63"));
            drop.Items.Add(new ListItem("64 TSELTAL VI FAMILIA MAYA", "64"));
            drop.Items.Add(new ListItem("65 TSOTSIL VI FAMILIA MAYA", "65"));
            drop.Items.Add(new ListItem("66 YAQUI II FAMILIA YUTO-NAHUA", "66"));
            drop.Items.Add(new ListItem("67 ZAPOTECO V FAMILIA OTO-MANGUE", "67"));
            drop.Items.Add(new ListItem("68 ZOQUE IX FAMILIA MIXE-ZOQUE", "68"));
        }
        
        public  void llenadropsEspecialidades(DropDownList drop)
        {
            drop.Items.Add(new ListItem("Seleccione una especialidad"));
            drop.Items.Add(new ListItem("1 PSIC�LOGO"));
            drop.Items.Add(new ListItem("2 PEDAGOGO"));
            drop.Items.Add(new ListItem("3 DEFICIENCIA MENTAL"));
            drop.Items.Add(new ListItem("4 AUDICI�N Y LENGUAJE"));
            drop.Items.Add(new ListItem("5 PROBLEMAS DE APRENDIZAJE"));
            drop.Items.Add(new ListItem("6 INADAPTADO SOCIAL"));
            drop.Items.Add(new ListItem("7 MAESTRO DE PRIMARIA"));
            drop.Items.Add(new ListItem("8 APARATO LOCOMOTOR"));
            drop.Items.Add(new ListItem("9 DEFICIENCIA VISUAL"));
            drop.Items.Add(new ListItem("10 COMUNICAC�N HUMANA"));
            drop.Items.Add(new ListItem("11 NORMAL SUPERIOR"));
            drop.Items.Add(new ListItem("12 SEXOLOG�A"));
            drop.Items.Add(new ListItem("13 ADMINISTRACI�N EDUCATIVA"));
            drop.Items.Add(new ListItem("14 ODONTOLOG�A"));
            drop.Items.Add(new ListItem("15 AUTISMO"));
            drop.Items.Add(new ListItem("16 EDUCACI�N ESPECIAL"));
            drop.Items.Add(new ListItem("17 INOVACIONES EDUCATIVAS"));
            drop.Items.Add(new ListItem("18 INTERVENCION TEMPRANA"));
            drop.Items.Add(new ListItem("19 MATEM�TICAS"));
            drop.Items.Add(new ListItem("20 SOCIOLOG�A"));
            drop.Items.Add(new ListItem("21 INGENIERO MEC�NICO"));
            drop.Items.Add(new ListItem("22 ENSE�ANZA SUPERIOR"));
            drop.Items.Add(new ListItem("23 EDUCACI�N"));
        }

        public void llenadropsTalleres(DropDownList drop)
        {
            drop.Items.Add(new ListItem("Seleccione un taller","0"));
            drop.Items.Add(new ListItem("SERIGRAF�A","1"));
            drop.Items.Add(new ListItem("REPOSTER�A", "2"));
            drop.Items.Add(new ListItem("PANADER�A", "3"));
            drop.Items.Add(new ListItem("INSTRUMENTOS MUSICALES", "4"));
            drop.Items.Add(new ListItem("SERVICIOS GENERALES", "5"));
            drop.Items.Add(new ListItem("CARRERA T�CNICA MESOTERAPIA", "6"));
            drop.Items.Add(new ListItem("MAQUILA", "7"));
            drop.Items.Add(new ListItem("TAPICER�A", "8"));
            drop.Items.Add(new ListItem("REPARACI�N DE CALZADO", "9"));
            drop.Items.Add(new ListItem("AUXILIAR EDUCATIVO", "10"));
            drop.Items.Add(new ListItem("ENCUADERNACI�N", "11"));
            drop.Items.Add(new ListItem("CER�MICA", "12"));
            drop.Items.Add(new ListItem("CARRERA T�CNICA EN MUSICA", "13"));
            drop.Items.Add(new ListItem("ALIMENTACI�N", "14"));
            drop.Items.Add(new ListItem("M�QUINAS Y HERRAMIENTAS", "15"));
            drop.Items.Add(new ListItem("HOJALATER�A Y PINTURA", "16"));
            drop.Items.Add(new ListItem("M�LTIPLE", "17"));
            drop.Items.Add(new ListItem("ORGANIZACI�N", "18"));
            drop.Items.Add(new ListItem("PROPED�UTICO", "19"));
            drop.Items.Add(new ListItem("AUXILIAR EMPRESARIAL", "20"));
            drop.Items.Add(new ListItem("MANUALIDADES", "21"));
            drop.Items.Add(new ListItem("JUGETER�A", "22"));
            drop.Items.Add(new ListItem("MENSAJER�A", "23"));
            drop.Items.Add(new ListItem("BRIGADA DE LIMPIEZA", "24"));
        }

        public void LlenarMotivos(DropDownList drop)
        {
            //ddlMotivos.Items.Add(new ListItem("Seleccionar ...", "0"));
            drop.Items.Add(new ListItem("1.- La escuela est� en proceso de clausura", "1"));
            drop.Items.Add(new ListItem("2.- Falta de personal Docente", "2"));
            drop.Items.Add(new ListItem("3.- Falta de alumnos", "3"));
            drop.Items.Add(new ListItem("4.- Incumplimiento del Director", "4"));
            drop.Items.Add(new ListItem("5.- Escuelas de nueva creaci�n", "5"));
            drop.Items.Add(new ListItem("6.- Causa administrativa", "6"));
            drop.Items.Add(new ListItem("7.- No corresponde la fecha de levantamiento con el inicio de cursos de la escuela", "7"));
            drop.Items.Add(new ListItem("8.- Compactaci�n de turno", "8"));
            drop.Items.Add(new ListItem("9.- Cambio de turno", "9"));
            //10.- Cerrado
        }

        public void LlenardropsAreas(DropDownList drop)
        {
           
            drop.Items.Add(new ListItem("A.. C. Agropecuarias", "A"));
            drop.Items.Add(new ListItem("B.. C. de la Salud", "B"));
            drop.Items.Add(new ListItem("C.. C. Naturales y Exactas", "C"));
            drop.Items.Add(new ListItem("D.. C. Sociales y Admivas", "D"));
            drop.Items.Add(new ListItem("E.. Educaci�n y Humanidades", "E"));
            drop.Items.Add(new ListItem("F.. Ingenier�a y Tecnolog�a", "F"));
            drop.Items.Add(new ListItem("Otros", "O"));
            
        }

        #endregion
    }
}
