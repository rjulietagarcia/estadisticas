<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AG3_911_112.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_112.AG3_911_112" Title="911.112(3�)" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1250px; height:65px;">
    <div id="header" style="text-align:center;">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PRIMARIA IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1250px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_112',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_911_112',true)"><a href="#" title=""><span>1�</span></a></li><li onclick="openPage('AG2_911_112',true)"><a href="#" title=""><span>2�</span></a></li><li onclick="openPage('AG3_911_112',true)"><a href="#" title="" class="activo"><span>3�</span></a></li><li onclick="openPage('AG4_911_112',false)"><a href="#" title=""><span>4�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>5�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>6�</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>TOTAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
        <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
           
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
           
         <table style="text-align:center; width: 900px;">
            <tr>
                    <td>
                    <table>
                    <tr>
                                    <td colspan="12" style="padding-bottom: 20px; text-align:center">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblRojo" 
                                        Text="Estad�stica de alumnos por grado, sexo, nuevo ingreso, repetidores y edad"
                                            ></asp:Label>
                                    </td>
                                </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbl1" runat="server" Text="3�" CssClass="lblRojo" Font-Size="25px"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl7" runat="server" Text="7 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl8" runat="server" Text="8 a�os" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl13" runat="server" CssClass="lblRojo" Text="13 a�os"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lbl14" runat="server" CssClass="lblRojo" Text="14 a�os"></asp:Label></td>
                            <td style="width: 81px">
                                <asp:Label ID="lbl15" runat="server" CssClass="lblRojo" Text="15 a�os y m�s"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:Label ID="lblTotal" runat="server" Text="TOTAL" CssClass="lblRojo"></asp:Label></td>
                        </tr>
                        <tr>
                            <td rowspan="2" style="width: 75px">
                                <asp:Label ID="lblHombres" runat="server" Text="HOMBRES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionH" runat="server" Text="NUEVO INGRESO" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV117" runat="server" Columns="3" ReadOnly="True" TabIndex="10101" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV118" runat="server" Columns="3" ReadOnly="True" TabIndex="10102" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV119" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10103" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV120" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10104" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV121" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10105" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV122" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10106" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV123" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10107" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV124" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10108" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV125" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10109" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV126" runat="server" Columns="4" ReadOnly="True" TabIndex="10110" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblExistenciaH" runat="server" Text="REPETIDORES" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV127" runat="server" Columns="3" ReadOnly="True" TabIndex="10201" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV128" runat="server" Columns="3" ReadOnly="True" TabIndex="10202" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV129" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10203" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV130" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10204" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV131" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10205" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV132" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10206" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV133" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10207" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV134" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10208" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV135" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10209" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV136" runat="server" Columns="4" ReadOnly="True" TabIndex="10210" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="12" rowspan="1">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" style="width: 75px">
                                <asp:Label ID="lblMujeres" runat="server" Text="MUJERES" CssClass="lblRojo"></asp:Label></td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionM" runat="server" Text="NUEVO INGRESO" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV137" runat="server" Columns="3" ReadOnly="True" TabIndex="10301" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV138" runat="server" Columns="3" ReadOnly="True" TabIndex="10302" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV139" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10303" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV140" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10304" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV141" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10305" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV142" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10306" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV143" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10307" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV144" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10308" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV145" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10309" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV146" runat="server" Columns="4" ReadOnly="True" TabIndex="10310" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                                <asp:Label ID="lblExistenciaM" runat="server" Text="REPETIDORES" CssClass="lblGrisTit"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV147" runat="server" Columns="3" ReadOnly="True" TabIndex="10401" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV148" runat="server" Columns="3" ReadOnly="True" TabIndex="10402" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV149" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10403" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV150" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10404" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV151" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10405" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV152" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10406" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV153" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10407" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV154" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10408" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV155" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10409" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV156" runat="server" Columns="4" ReadOnly="True" TabIndex="10410" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="12">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 75px">
                            </td>
                            <td style="width: 132px; text-align: left">
                                <asp:Label ID="lblInscripcionS" runat="server" Text="SUBTOTAL" CssClass="lblGrisTit"></asp:Label></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV157" runat="server" Columns="3" ReadOnly="True" TabIndex="10501" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV158" runat="server" Columns="3" ReadOnly="True" TabIndex="10502" MaxLength="3" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV159" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10503" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV160" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10504" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV161" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10505" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV162" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10506" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV163" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10507" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV164" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10508" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 81px">
                                <asp:TextBox ID="txtV165" runat="server" Columns="3" MaxLength="3" ReadOnly="True"
                                    TabIndex="10509" Width="40px" CssClass="lblNegro">0</asp:TextBox></td>
                            <td style="width: 67px">
                                <asp:TextBox ID="txtV166" runat="server" Columns="4" ReadOnly="True" TabIndex="10510" CssClass="lblNegro" MaxLength="4">0</asp:TextBox></td>
                        </tr>
                        <tr></tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG2_911_112',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG2_911_112',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                 <td style="width: 330px; ">&nbsp;
                </td>
                <td ><span  onclick="openPage('AG4_911_112',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG4_911_112',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>

         <div class="divResultado" id="divResultado"></div> 
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

        
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center>
         <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                <div class="fondoDegradado" style="width: 100%; height: 100%; position:fixed; background: #000000 url(../../../tema/images/loading2.gif) no-repeat center center; top:0%; left:0%; text-align:center;  filter:Alpha(Opacity=60); opacity:.6;">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
    <script type="text/javascript" language="javascript">
        MaxCol = 11;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
        
        var CambiarPagina = "";
        var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
        Disparador(<%=this.hidDisparador.Value %>);
        GetTabIndexes();
    </script>
    <%--Agregado--%> 
</asp:Content>
