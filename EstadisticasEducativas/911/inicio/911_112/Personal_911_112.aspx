<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.112(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_112.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_112.Personal_911_112" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    
    
    <div id="logo"></div>
    <div style="min-width:1250px; height:65px;">
    <div id="header">
    <table style="width:100%">
        
        <tr><td><span>EDUCACI�N PRIMARIA IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1250px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_112',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_911_112',true)"><a href="#" title=""><span>1�</span></a></li><li onclick="openPage('AG2_911_112',true)"><a href="#" title=""><span>2�</span></a></li><li onclick="openPage('AG3_911_112',true)"><a href="#" title=""><span>3�</span></a></li><li onclick="openPage('AG4_911_112',true)"><a href="#" title=""><span>4�</span></a></li><li onclick="openPage('AG5_911_112',true)"><a href="#" title=""><span>5�</span></a></li><li onclick="openPage('AG6_911_112',true)"><a href="#" title=""><span>6�</span></a></li><li onclick="openPage('AGT_911_112',true)"><a href="#" title="" ><span>TOTAL</span></a></li><li onclick="openPage('AGD_911_112',true)"><a href="#" title="" ><span>DESGLOSE</span></a></li><li onclick="openPage('Personal_911_112',true)"><a href="#" class="activo" title=""><span>PERSONAL</span></a></li><li onclick="openPage('CMYA_911_112',false)"><a href="#" title="" ><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
   <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>  
           

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>



    <div>
    
        <table>
            <tr>
                <td style="width: 340px; text-align: center;">
                    <table id="TABLE1" style="text-align: center; width: 748px;" >
                        <tr>
                            <td colspan="18" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="III. PERSONAL POR FUNCI�N"
                                    Width="250px" Font-Size="16px"></asp:Label><br />
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="18" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de personal que realiza funciones de directivo (con y sin grupo), docente, docente especial (profesor de educaci�n f�sica, actividades art�sticas, tecnol�gicas y de idiomas), y administrativo, auxiliar y de servicios, independientemente de su nombramiento, tipo y fuente de pago, desgl�selos seg�n su funci�n, nivel m�ximo de estudios y sexo."
                                    Width="1195px"></asp:Label><br />
                                <br />
                                <asp:Label ID="lblNota1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Notas:"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="a) Si una persona desempe�a dos o m�s funciones an�tela en aquella a la que dedique m�s tiempo"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="lblNota3" runat="server" CssClass="lblRojo" Text="b) Si en la tabla corresponde al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente o en OTROS"
                                    Width="1195px"></asp:Label><br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="18" rowspan="1">
                                <table align="center">
                                      <tr>
                            <td colspan="1" rowspan="3" style="width: 227px;"  >
                                <asp:Label ID="lblNivelEd" runat="server" CssClass="lblRojo" Text="NIVEL EDUCATIVO" Width="120px"></asp:Label></td>
                            <td colspan="4" rowspan="" style="text-align: center;"  >
                                <asp:Label ID="lblPersDir" runat="server" CssClass="lblRojo" Text="PERSONAL DIRECTIVO" Width="141px"></asp:Label></td>
                            <td colspan="2" rowspan="2" style="text-align: center"  >
                                <asp:Label ID="lblPersDocente" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE" Width="85px"></asp:Label></td>
                            
                            <td colspan="2" rowspan="2" style="text-align: center"  >
                                <asp:Label ID="lblDocIdiom" runat="server" CssClass="lblRojo" Text="PROMOTORES" Width="95px"></asp:Label></td>
                            <td colspan="2" rowspan="2" style="text-align: center; height: 70px;"  >
                                <asp:Label ID="lblPersAdmin" runat="server" CssClass="lblRojo" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS" Height="60px" Width="95px"></asp:Label>
                            </td>
                            <td   colspan="1" rowspan="2">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center"  >
                                <asp:Label ID="lblPersDirCG" runat="server" CssClass="lblRojo" Text="CON GRUPO" Width="70px"></asp:Label></td>
                            <td colspan="2" style="text-align: center"  >
                                <asp:Label ID="lblPersDirSG" runat="server" CssClass="lblRojo" Text="SIN GRUPO" Width="70px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblPersDirCGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblPersDirCGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblPersDirSGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblPersDirSGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblPersDocenteH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblPersDocenteM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblDocIdiomH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblDocIdiomM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblPersAdminH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="height: 28px; width: 70px;"  >
                                <asp:Label ID="lblPersAdminM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="height: 28px"  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblPrimInc" runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV444" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10101"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV445" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10102"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV446" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10103"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV447" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10104"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV448" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10105"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV449" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10106"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV450" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10107"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV451" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10108"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV452" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10109"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV453" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10110"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="PRIMARIA TERMINADA"></asp:Label></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV454" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10201"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV455" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10202"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV456" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10203"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV457" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10204"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV458" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10205"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV459" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="10206"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV460" runat="server" Columns="2" TabIndex="10207" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV461" runat="server" Columns="2" TabIndex="10208" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV462" runat="server" Columns="2" TabIndex="10209" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV463" runat="server" Columns="2" TabIndex="10210" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 227px;"  >
                            <asp:Label ID="lblSecInc" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td style="height: 36px; text-align: center;"  >
                                <asp:TextBox ID="txtV464" runat="server" Columns="2" TabIndex="10301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center;"  >
                                <asp:TextBox ID="txtV465" runat="server" Columns="2" TabIndex="10302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV466" runat="server" Columns="2" TabIndex="10303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV467" runat="server" Columns="2" TabIndex="10304" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV468" runat="server" Columns="2" TabIndex="10305" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV469" runat="server" Columns="2" TabIndex="10306" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV470" runat="server" Columns="2" TabIndex="10307" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV471" runat="server" Columns="2" TabIndex="10308" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV472" runat="server" Columns="2" TabIndex="10309" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 36px; text-align: center"  >
                                <asp:TextBox ID="txtV473" runat="server" Columns="2" TabIndex="10310" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblSecTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="SECUNDARIA TERMINADA"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV474" runat="server" Columns="2" TabIndex="10401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV475" runat="server" Columns="2" TabIndex="10402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV476" runat="server" Columns="2" TabIndex="10403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV477" runat="server" Columns="2" TabIndex="10404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV478" runat="server" Columns="2" TabIndex="10405" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV479" runat="server" Columns="2" TabIndex="10406" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV480" runat="server" Columns="2" TabIndex="10407" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV481" runat="server" Columns="2" TabIndex="10408" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV482" runat="server" Columns="2" TabIndex="10409" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV483" runat="server" Columns="2" TabIndex="10410" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblProfTec" runat="server" CssClass="lblGrisTit" Height="17px" Text="PROFESIONAL T�CNICO"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV484" runat="server" Columns="2" TabIndex="10501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV485" runat="server" Columns="2" TabIndex="10502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV486" runat="server" Columns="2" TabIndex="10503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV487" runat="server" Columns="2" TabIndex="10504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV488" runat="server" Columns="2" TabIndex="10505" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV489" runat="server" Columns="2" TabIndex="10506" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV490" runat="server" Columns="2" TabIndex="10507" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV491" runat="server" Columns="2" TabIndex="10508" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV492" runat="server" Columns="2" TabIndex="10509" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV493" runat="server" Columns="2" TabIndex="10510" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblBachInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO INCOMPLETO"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV494" runat="server" Columns="2" TabIndex="10601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV495" runat="server" Columns="2" TabIndex="10602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV496" runat="server" Columns="2" TabIndex="10603" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV497" runat="server" Columns="2" TabIndex="10604" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV498" runat="server" Columns="2" TabIndex="10605" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV499" runat="server" Columns="2" TabIndex="10606" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV500" runat="server" Columns="2" TabIndex="10607" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV501" runat="server" Columns="2" TabIndex="10608" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV502" runat="server" Columns="2" TabIndex="10609" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV503" runat="server" Columns="2" TabIndex="10610" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblBachTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO TERMINADO"></asp:Label></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV504" runat="server" Columns="2" TabIndex="10701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV505" runat="server" Columns="2" TabIndex="10702" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV506" runat="server" Columns="2" TabIndex="10703" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV507" runat="server" Columns="2" TabIndex="10704" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV508" runat="server" Columns="2" TabIndex="10705" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV509" runat="server" Columns="2" TabIndex="10706" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV510" runat="server" Columns="2" TabIndex="10707" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV511" runat="server" Columns="2" TabIndex="10708" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV512" runat="server" Columns="2" TabIndex="10709" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV513" runat="server" Columns="2" TabIndex="10710" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblNormPreeInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR INCOMPLETA"
                                    Width="215px"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV514" runat="server" Columns="2" TabIndex="10801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV515" runat="server" Columns="2" TabIndex="10802" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV516" runat="server" Columns="2" TabIndex="10803" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV517" runat="server" Columns="2" TabIndex="10804" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV518" runat="server" Columns="2" TabIndex="10805" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV519" runat="server" Columns="2" TabIndex="10806" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV520" runat="server" Columns="2" TabIndex="10807" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV521" runat="server" Columns="2" TabIndex="10808" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV522" runat="server" Columns="2" TabIndex="10809" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV523" runat="server" Columns="2" TabIndex="10810" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblNormPreeTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR TERMINADA"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV524" runat="server" Columns="2" TabIndex="10901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV525" runat="server" Columns="2" TabIndex="10902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV526" runat="server" Columns="2" TabIndex="10903" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV527" runat="server" Columns="2" TabIndex="10904" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV528" runat="server" Columns="2" TabIndex="10905" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV529" runat="server" Columns="2" TabIndex="10906" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV530" runat="server" Columns="2" TabIndex="10907" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV531" runat="server" Columns="2" TabIndex="10908" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV532" runat="server" Columns="2" TabIndex="10909" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV533" runat="server" Columns="2" TabIndex="10910" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblNormPrimInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA INCOMPLETA"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV534" runat="server" Columns="2" TabIndex="11001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV535" runat="server" Columns="2" TabIndex="11002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV536" runat="server" Columns="2" TabIndex="11003" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV537" runat="server" Columns="2" TabIndex="11004" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV538" runat="server" Columns="2" TabIndex="11005" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV539" runat="server" Columns="2" TabIndex="11006" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV540" runat="server" Columns="2" TabIndex="11007" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV541" runat="server" Columns="2" TabIndex="11008" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV542" runat="server" Columns="2" TabIndex="11009" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV543" runat="server" Columns="2" TabIndex="11010" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblNormPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA TERMINADA"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV544" runat="server" Columns="2" TabIndex="11101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV545" runat="server" Columns="2" TabIndex="11102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV546" runat="server" Columns="2" TabIndex="11103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV547" runat="server" Columns="2" TabIndex="11104" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV548" runat="server" Columns="2" TabIndex="11105" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV549" runat="server" Columns="2" TabIndex="11106" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV550" runat="server" Columns="2" TabIndex="11107" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV551" runat="server" Columns="2" TabIndex="11108" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV552" runat="server" Columns="2" TabIndex="11109" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV553" runat="server" Columns="2" TabIndex="11110" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblNormSupInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR INCOMPLETA"></asp:Label></td>
                            <td style="height: 15px; text-align: center;"  >
                                <asp:TextBox ID="txtV554" runat="server" Columns="2" TabIndex="11201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center;"  >
                                <asp:TextBox ID="txtV555" runat="server" Columns="2" TabIndex="11202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center"  >
                                <asp:TextBox ID="txtV556" runat="server" Columns="2" TabIndex="11203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center"  >
                                <asp:TextBox ID="txtV557" runat="server" Columns="2" TabIndex="11204" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center"  >
                                <asp:TextBox ID="txtV558" runat="server" Columns="2" TabIndex="11205" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center"  >
                                <asp:TextBox ID="txtV559" runat="server" Columns="2" TabIndex="11206" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center"  >
                                <asp:TextBox ID="txtV560" runat="server" Columns="2" TabIndex="11207" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center"  >
                                <asp:TextBox ID="txtV561" runat="server" Columns="2" TabIndex="11208" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center"  >
                                <asp:TextBox ID="txtV562" runat="server" Columns="2" TabIndex="11209" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 15px; text-align: center"  >
                                <asp:TextBox ID="txtV563" runat="server" Columns="2" TabIndex="11210" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblNormSupPas" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, PASANTE"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV564" runat="server" Columns="2" TabIndex="11301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV565" runat="server" Columns="2" TabIndex="11302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV566" runat="server" Columns="2" TabIndex="11303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV567" runat="server" Columns="2" TabIndex="11304" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV568" runat="server" Columns="2" TabIndex="11305" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV569" runat="server" Columns="2" TabIndex="11306" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV570" runat="server" Columns="2" TabIndex="11307" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV571" runat="server" Columns="2" TabIndex="11308" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV572" runat="server" Columns="2" TabIndex="11309" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV573" runat="server" Columns="2" TabIndex="11310" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblNormSupTit" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, TITULADO"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV574" runat="server" Columns="2" TabIndex="11401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV575" runat="server" Columns="2" TabIndex="11402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV576" runat="server" Columns="2" TabIndex="11403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV577" runat="server" Columns="2" TabIndex="11404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV578" runat="server" Columns="2" TabIndex="11405" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV579" runat="server" Columns="2" TabIndex="11406" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV580" runat="server" Columns="2" TabIndex="11407" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV581" runat="server" Columns="2" TabIndex="11408" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV582" runat="server" Columns="2" TabIndex="11409" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV583" runat="server" Columns="2" TabIndex="11410" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblLicInc" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA INCOMPLETA"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV584" runat="server" Columns="2" TabIndex="11501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV585" runat="server" Columns="2" TabIndex="11502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV586" runat="server" Columns="2" TabIndex="11503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV587" runat="server" Columns="2" TabIndex="11504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV588" runat="server" Columns="2" TabIndex="11505" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV589" runat="server" Columns="2" TabIndex="11506" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV590" runat="server" Columns="2" TabIndex="11507" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV591" runat="server" Columns="2" TabIndex="11508" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV592" runat="server" Columns="2" TabIndex="11509" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV593" runat="server" Columns="2" TabIndex="11510" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblLicPas" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, PASANTE"></asp:Label></td>
                            <td style="height: 12px; text-align: center;"  >
                                <asp:TextBox ID="txtV594" runat="server" Columns="2" TabIndex="11601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center;"  >
                                <asp:TextBox ID="txtV595" runat="server" Columns="2" TabIndex="11602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center"  >
                                <asp:TextBox ID="txtV596" runat="server" Columns="2" TabIndex="11603" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center"  >
                                <asp:TextBox ID="txtV597" runat="server" Columns="2" TabIndex="11604" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center"  >
                                <asp:TextBox ID="txtV598" runat="server" Columns="2" TabIndex="11605" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center"  >
                                <asp:TextBox ID="txtV599" runat="server" Columns="2" TabIndex="11606" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center"  >
                                <asp:TextBox ID="txtV600" runat="server" Columns="2" TabIndex="11607" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center"  >
                                <asp:TextBox ID="txtV601" runat="server" Columns="2" TabIndex="11608" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center"  >
                                <asp:TextBox ID="txtV602" runat="server" Columns="2" TabIndex="11609" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 12px; text-align: center"  >
                                <asp:TextBox ID="txtV603" runat="server" Columns="2" TabIndex="11610" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                            <asp:Label ID="lblLicTit" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV604" runat="server" Columns="2" TabIndex="11701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV605" runat="server" Columns="2" TabIndex="11702" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV606" runat="server" Columns="2" TabIndex="11703" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV607" runat="server" Columns="2" TabIndex="11704" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV608" runat="server" Columns="2" TabIndex="11705" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV609" runat="server" Columns="2" TabIndex="11706" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV610" runat="server" Columns="2" TabIndex="11707" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV611" runat="server" Columns="2" TabIndex="11708" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV612" runat="server" Columns="2" TabIndex="11709" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV613" runat="server" Columns="2" TabIndex="11710" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblMaestInc" runat="server" CssClass="lblGrisTit" Text="MAESTR�A INCOMPLETA"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV614" runat="server" Columns="2" TabIndex="11801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV615" runat="server" Columns="2" TabIndex="11802" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV616" runat="server" Columns="2" TabIndex="11803" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV617" runat="server" Columns="2" TabIndex="11804" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV618" runat="server" Columns="2" TabIndex="11805" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV619" runat="server" Columns="2" TabIndex="11806" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV620" runat="server" Columns="2" TabIndex="11807" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV621" runat="server" Columns="2" TabIndex="11808" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV622" runat="server" Columns="2" TabIndex="11809" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV623" runat="server" Columns="2" TabIndex="11810" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblMaestGrad" runat="server" CssClass="lblGrisTit" Text="MAESTR�A, GRADUADO"></asp:Label></td>
                            <td style="height: 21px; text-align: center;"  >
                                <asp:TextBox ID="txtV624" runat="server" Columns="2" TabIndex="11901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center;"  >
                                <asp:TextBox ID="txtV625" runat="server" Columns="2" TabIndex="11902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center"  >
                                <asp:TextBox ID="txtV626" runat="server" Columns="2" TabIndex="11903" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center"  >
                                <asp:TextBox ID="txtV627" runat="server" Columns="2" TabIndex="11904" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center"  >
                                <asp:TextBox ID="txtV628" runat="server" Columns="2" TabIndex="11905" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center"  >
                                <asp:TextBox ID="txtV629" runat="server" Columns="2" TabIndex="11906" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center"  >
                                <asp:TextBox ID="txtV630" runat="server" Columns="2" TabIndex="11907" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center"  >
                                <asp:TextBox ID="txtV631" runat="server" Columns="2" TabIndex="11908" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center"  >
                                <asp:TextBox ID="txtV632" runat="server" Columns="2" TabIndex="11909" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 21px; text-align: center"  >
                                <asp:TextBox ID="txtV633" runat="server" Columns="2" TabIndex="11910" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblDocInc" runat="server" CssClass="lblGrisTit" Text="DOCTORADO INCOMPLETO"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV634" runat="server" Columns="2" TabIndex="12001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV635" runat="server" Columns="2" TabIndex="12002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV636" runat="server" Columns="2" TabIndex="12003" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV637" runat="server" Columns="2" TabIndex="12004" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV638" runat="server" Columns="2" TabIndex="12005" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV639" runat="server" Columns="2" TabIndex="12006" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV640" runat="server" Columns="2" TabIndex="12007" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV641" runat="server" Columns="2" TabIndex="12008" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV642" runat="server" Columns="2" TabIndex="12009" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV643" runat="server" Columns="2" TabIndex="12010" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblDocGrad" runat="server" CssClass="lblGrisTit" Text="DOCTORADO, GRADUADO"></asp:Label></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV644" runat="server" Columns="2" TabIndex="12101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV645" runat="server" Columns="2" TabIndex="12102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV646" runat="server" Columns="2" TabIndex="12103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV647" runat="server" Columns="2" TabIndex="12104" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV648" runat="server" Columns="2" TabIndex="12105" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV649" runat="server" Columns="2" TabIndex="12106" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV650" runat="server" Columns="2" TabIndex="12107" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV651" runat="server" Columns="2" TabIndex="12108" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV652" runat="server" Columns="2" TabIndex="12109" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV653" runat="server" Columns="2" TabIndex="12110" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: justify; width: 227px;"  >
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS*"></asp:Label>
                                <br />
                                <asp:Label ID="lblEspecifique" runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE"></asp:Label></td>
                            <td style="height: 26px; text-align: center" colspan="10">
                            </td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 227px;"  >
                                <asp:TextBox ID="txtV654" Columns="30" MaxLength="30" runat="server" Width="200px"></asp:TextBox>
                                     
                            </td>
                            <td style="height: 26px; text-align: center;"  >
                                <asp:TextBox ID="txtV655" runat="server" Columns="2" TabIndex="12201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV656" runat="server" Columns="2" TabIndex="12202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV657" runat="server" Columns="2" TabIndex="12203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV658" runat="server" Columns="2" TabIndex="12204" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV659" runat="server" Columns="2" TabIndex="12205" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV660" runat="server" Columns="2" TabIndex="12206" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV661" runat="server" Columns="2" TabIndex="12207" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV662" runat="server" Columns="2" TabIndex="12208" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV663" runat="server" Columns="2" TabIndex="12209" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV664" runat="server" Columns="2" TabIndex="12210" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 227px;"  >
                                <asp:TextBox ID="txtV665" Columns="30" MaxLength="30" runat="server" Width="200px"></asp:TextBox>   
                            </td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV666" runat="server" Columns="2" TabIndex="12301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV667" runat="server" Columns="2" TabIndex="12302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV668" runat="server" Columns="2" TabIndex="12303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV669" runat="server" Columns="2" TabIndex="12304" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV670" runat="server" Columns="2" TabIndex="12305" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV671" runat="server" Columns="2" TabIndex="12306" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV672" runat="server" Columns="2" TabIndex="12307" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV673" runat="server" Columns="2" TabIndex="12308" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV674" runat="server" Columns="2" TabIndex="12309" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 49px; text-align: center"  >
                                <asp:TextBox ID="txtV675" runat="server" Columns="2" TabIndex="12310" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 227px;"  >
                                <asp:TextBox ID="txtV676" Columns="30" MaxLength="30" runat="server" Width="200px"></asp:TextBox>   
                            </td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV677" runat="server" Columns="2" TabIndex="12401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV678" runat="server" Columns="2" TabIndex="12402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV679" runat="server" Columns="2" TabIndex="12403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV680" runat="server" Columns="2" TabIndex="12404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV681" runat="server" Columns="2" TabIndex="12405" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV682" runat="server" Columns="2" TabIndex="12406" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV683" runat="server" Columns="2" TabIndex="12407" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV684" runat="server" Columns="2" TabIndex="12408" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV685" runat="server" Columns="2" TabIndex="12409" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center"  >
                                <asp:TextBox ID="txtV686" runat="server" Columns="2" TabIndex="12410" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 227px"  >
                                <asp:Label ID="lblSubtotales" runat="server" CssClass="lblGrisTit" Text="SUBTOTALES"></asp:Label></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV687" runat="server" Columns="2" TabIndex="12501" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV688" runat="server" Columns="2" TabIndex="12502" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV689" runat="server" Columns="2" TabIndex="12503" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV690" runat="server" Columns="2" TabIndex="12504" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV691" runat="server" Columns="2" TabIndex="12505" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV692" runat="server" Columns="2" TabIndex="12506" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV693" runat="server" Columns="2" TabIndex="12507" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV694" runat="server" Columns="2" TabIndex="12508" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV695" runat="server" Columns="2" TabIndex="12509" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px"  >
                                <asp:TextBox ID="txtV696" runat="server" Columns="2" TabIndex="12510" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td  >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="10" rowspan="1" style="text-align: right">
                                &nbsp;<asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL PERSONAL (Suma de subtotales)"
                                    Width="275px"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center" align="center"  >
                                <asp:TextBox ID="txtV697" runat="server" Columns="2" TabIndex="20101" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td align="center"  >
                                &nbsp;</td>
                        </tr>
                                </table>
                            </td>
                        </tr>
                      
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGD_911_112',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_112',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                    &nbsp;
                 </td>
                <td ><span  onclick="openPage('CMYA_911_112',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('CMYA_911_112',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                 var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                 
 		      Disparador(<%=hidDisparador.Value %>);
                      
                        MaxCol = 18;
                        MaxRow = 28;
                        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                        GetTabIndexes();
        </script> 
</asp:Content>
