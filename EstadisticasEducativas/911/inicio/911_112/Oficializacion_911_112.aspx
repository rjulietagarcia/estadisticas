<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.112(Oficializaci�n)" AutoEventWireup="true" CodeBehind="Oficializacion_911_112.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_112.Oficializacion_911_112" %>

 
  <asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
     <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 12;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
    <div id="logo"></div>
    <div style="min-width:1250px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr><td><span>EDUCACI�N PRIMARIA IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1250px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_112',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_112',true)"><a href="#" title=""><span>1�</span></a></li>
        <li onclick="openPage('AG2_911_112',true)"><a href="#" title=""><span>2�</span></a></li>
        <li onclick="openPage('AG3_911_112',true)"><a href="#" title=""><span>3�</span></a></li>
        <li onclick="openPage('AG4_911_112',true)"><a href="#" title=""><span>4�</span></a></li>
        <li onclick="openPage('AG5_911_112',true)"><a href="#" title=""><span>5�</span></a></li>
        <li onclick="openPage('AG6_911_112',true)"><a href="#" title=""><span>6�</span></a></li>
        <li onclick="openPage('AGT_911_112',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_112',true)"><a href="#" title="" ><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_112',true)"><a href="#"  title=""><span>PERSONAL</span></a></li>
        <li onclick="OpenPage('CMyA_911_112',true)"><a href="#" title="" ><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
        <li onclick="openPage('Anexo_911_112',true)"><a href="#" title="" ><span>ANEXO</span></a></li>
        <li onclick="openPage('Oficializacion_911_112',true)"><a href="#" title="" class="activo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <br /> 
    <div id="tooltipayuda" class="balloonstyle">
    <p>En la secci�n de observaciones favor de referir las correcciones que deban realizarse a los datos de identificaci�n del plantel y notificar alguna queja, inquietud o sugerencia para mejorar el proceso del levantamiento estad�stico.</p>
    <p>Si la escuela no generar� estad�sticas educativas favor de indicar el motivo y dar clic en el bot�n de OFICIALIZAR.</p>
    <p>Una vez concluido el trabajo de revisar y complementar la informaci�n se procede a oficializar la Estad�stica Educativa presionado un clic en el bot�n OFICIALIZAR.</p>
    <p>El sistema realizar� una serie de validaciones para detectar inconsistencias en los datos y si la informaci�n es v�lida se dar� por concluido el levantamiento estad�stico, y se habilitar�n las opciones para generar el comprobante de captura y la impresi�n del cuestionario lleno.</p>
    </div>
    <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
     <center>
      
                <table  class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0"> 
                    <tr> 
                        <td class="EsqSupIzq" style="width: 15px"> 
                        </td> 
                        <td class="RepSup"> 
                        </td> 
                        <td class="EsqSupDer"> 
                        </td> 
                    </tr> 
                    <tr> 
                        <td class="RepLatIzq" style="width: 15px"> 
                        </td> 
                        <td> 
                            <table id="Table2" cellspacing="0" cellpadding="0" width="440" border="0"> 
                                <tr> 
                                    <td> 
                                    </td> 
                                </tr> 
                                <tr> 
                                    <td> 
                                        <table style="width: 620px">
                    <tr>
                        <td colspan="2" style="height: 28px">
                            <br />
                            <asp:Label ID="Label2" runat="server"  Text="OFICIALIZAR LA INFORMACI�N ESTAD�STICA" Font-Size="16pt"></asp:Label></td>
                    </tr>
                    <tr>
                        <td  colspan="2" >
                            <table style="width:90%;">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="Label7" runat="server" Text="Observaciones:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtObservaciones" runat="server" Height="74px" MaxLength="1000" TextMode="MultiLine" Width="97%"></asp:TextBox>
                                        <br />
                                        <asp:Button ID="cmdGuardarObs" runat="server"   OnClick="cmdGuardarObs_Click"
                                            Text="Guardar Observacion" /></td>
                                </tr>
                            </table>
                           </td>
                           
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;<asp:Label ID="Label1" runat="server" Text="Declaro los datos ingresados en el cuestionario como ver�dicos y finalizo la captura de los mismos." Width="264px"></asp:Label></td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="No fue posible contestar el cuestionario por el siguiente motivo:"
                                Width="222px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Button ID="cmdOficializar" runat="server"   Text="Oficializar" OnClick="cmdOficializar_Click" /></td>
                        <td>
                            <asp:DropDownList ID="ddlMotivos" runat="server" Width="228px"  >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Label ID="Label4" runat="server" Text="Generar comprobante de captura."></asp:Label></td>
                        <td>
                            <asp:Button ID="cmdGenerarComprobante" runat="server"   Text="Aceptar" OnClick="cmdGenerarComprobante_Click" /></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Label ID="Label5" runat="server" Text="Imprimir el cuestionario lleno."></asp:Label></td>
                        <td>
                            <asp:Button ID="cmdImprimirCuestionario_Lleno" runat="server"   Text="Aceptar" OnClick="cmdImprimirCuestionario_Lleno_Click" /></td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            &nbsp;</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 310px">
                            <asp:Label ID="Label6" runat="server" Text="Imprimir solo el formato del cuestionario."></asp:Label></td>
                        <td>
                            <asp:Button ID="cmdImprimircuestionario" runat="server"   Text="Aceptar" OnClick="cmdImprimircuestionario_Click" /></td>
                    </tr>
                </table>
                                    </td> 
                                </tr> 
                                <tr> 
                                    <td> 
                                    </td> 
                                </tr> 
                            </table> 
                        </td> 
                        <td class="RepLatDer"> 
                        </td> 
                    </tr> 
                    <tr> 
                        <td class="EsqInfIzq" style="height: 18px; width: 15px;"> 
                        </td> 
                        <td class="RepInf" style="height: 18px"> 
                        </td> 
                        <td class="EsqInfDer" style="height: 18px"> 
                        </td> 
                    </tr> 
                </table>        
    
                      
            
            <div > 
                <asp:Label ID="lblResultado" runat="server"></asp:Label>
            </div>
            
            <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
            <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
            <input id="hidIdCtrl" type="hidden" runat= "server" />
            <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
            <script type="text/javascript">
                function openPage(page){window.location.href=page+".aspx";}
            </script> 
        
         <asp:Table runat="server" ID="table1" Visible="False">
             <asp:TableRow ID="TableRow1" runat="server">
                 <asp:TableCell ID="TableCell1" runat="server" CssClass="Bordes1">Base de Datos</asp:TableCell>
                 <asp:TableCell ID="TableCell2" runat="server" CssClass="Bordes2">Web Service</asp:TableCell>
             </asp:TableRow> 
          </asp:Table>
        </center>
        <center>
            &nbsp;</center>
        <center>
         <asp:Table ID="tblDatos" runat="server" Visible="False" >    
             <asp:TableRow ID="TableRow2" runat="server">
                 <asp:TableCell ID="TableCell3" runat="server" CssClass="BordesX">NoVar</asp:TableCell>
                 <asp:TableCell ID="TableCell4" runat="server" CssClass="BordesX">Valor</asp:TableCell>
             </asp:TableRow>
         </asp:Table>
            &nbsp;</center>
        <center>
            <asp:Table ID="tblFallas" runat="server" Visible="False" >
                <asp:TableRow ID="TableRow3" runat="server">
                    <asp:TableCell ID="TableCell5" runat="server" CssClass="BordesX">NoVar</asp:TableCell>
                    <asp:TableCell ID="TableCell6" runat="server" CssClass="BordesX">Descripci�n</asp:TableCell>
                    <asp:TableCell ID="TableCell7" runat="server" CssClass="BordesX">Inconsitencia</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </center>
        <asp:HiddenField ID="hidRuta" runat="server" />
        <script language = "javascript" type="text/javascript">
           function AbrirPDF(){
               var ruta = document.getElementById("ctl00_cphMainMaster_hidRuta").value
               if (ruta != ""){
                    window.open (ruta,'','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668')
               }
           }
           AbrirPDF();
           
        </script>

</asp:Content>