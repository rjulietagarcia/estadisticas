<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.1(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_1.Personal_911_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td>
                <span>EDUCACI�N PREESCOLAR</span>
            </td>
        </tr>
        <tr>
            <td>
                <span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span>
            </td>
        </tr>
        <tr>
            <td>
                <span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span>
            </td>
        </tr>
    </table>
    </div></div>
    <div id="menu" style="min-width:1000px; width:expression(document.body.clientWidth < 1001? '1000px': 'auto' );">
      <ul class="left">
        <li onclick="OpenPageCharged('Identificacion_911_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="OpenPageCharged('Alumnos_911_1',true)"><a href="#" title=""><span>ALUMNOS</span></a></li>
        <li onclick="OpenPageCharged('Personal_911_1',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="OpenPageCharged('CMYA_911_1',false)"><a href="#" title=""><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>  

        <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

    <center>
    
     
      <!--Tabla Fondo-->
      <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
			    <td>   
 					 <!--Contenido dentro del area de trabajo-->
    
                 <table>
                        <tr>
                            <td colspan="3" style="text-align:justify">
                                <asp:Label Font-Size="16px" ID="lblPERSONAL" runat="server" CssClass="lblGrisTit"  Text="II. PERSONAL POR FUNCI�N"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                                <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de personal que realiza funciones de directivo (con y sin grupo), docente, docente especial (profesor de educaci�n f�sica, actividades art�sticas, y de idiomas), y administrativo, auxiliar y de servicios, independientemente de su nombramiento, tipo y fuente de pago, desgl�selos seg�n su funci�n, nivel m�ximo de estudios y sexo."
                                    Width="1200px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                                <asp:Label ID="lblNotas" runat="server" CssClass="lblRojo" Text="Notas:" Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                                <asp:Label ID="lblInstruccion1a" runat="server" CssClass="lblRojo" Text="a) Si una persona desempe�a dos o m�s funciones an�tela en aquella a la que dedique m�s tiempo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: left">
                                <asp:Label ID="lblInstruccion1b" runat="server" CssClass="lblRojo" Text="b) Si en la tabla correspondiente al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente en Otros."
                                    Width="100%"></asp:Label></td>
                        </tr>
                     <tr>
                         <td colspan="3" style="text-align: left">
                         </td>
                     </tr>
                        <tr>
                            <td colspan="3" style="text-align: center">
                    <table>
                        <tr>
                            <td rowspan="3">
                                <asp:Label ID="lblNivelEd" runat="server" CssClass="lblRojo" Text="NIVEL EDUCATIVO"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td colspan="4" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPDirectivo" runat="server" CssClass="lblRojo" Text="PERSONAL DIRECTIVO"
                                    Width="300px" Font-Bold="True"></asp:Label></td>
                            <td colspan="2" rowspan="2" class="linaBajoAlto">
                                <asp:Label ID="lblDocente" runat="server" CssClass="lblRojo" Height="17px" Text="PERSONAL DOCENTE"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td colspan="6" style="text-align: center" class="linaBajoAlto">
                                <asp:Label ID="lblPDocenteEspecial" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE ESPECIAL"
                                    Width="300px" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: center" colspan="2" rowspan="2" class="linaBajoAlto">
                                <asp:Label ID="lblPAdministrativo" runat="server" CssClass="lblRojo" Height="50px" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS"
                                    Width="100px" Font-Bold="True"></asp:Label></td>
                            <td class="Orila" colspan="1" rowspan="2" style="width: 4px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center; height: 70px;" class="linaBajo">
                                <asp:Label ID="lblConGrupo" runat="server" CssClass="lblNegro" Height="17px" Text="CON GRUPO"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td colspan="2" style="text-align: center; height: 70px;" class="linaBajo">
                                <asp:Label ID="lblSinGrupo" runat="server" CssClass="lblNegro" Height="17px" Text="SIN GRUPO"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td colspan="2" style="text-align: center; height: 70px;" class="linaBajo">
                                <asp:Label ID="lblProfEd" runat="server" CssClass="lblNegro" Height="50px" Text="PROFESORES DE EDUCACI�N"
                                    Width="87px" Font-Bold="True"></asp:Label></td>
                            <td colspan="2" style="text-align: center; height: 70px;" class="linaBajo">
                                <asp:Label ID="lblProfArt" runat="server" CssClass="lblNegro" Height="50px" Text="PROFESORES DE ACTIVIDADES ART�STICAS"
                                    Width="100px" Font-Bold="True"></asp:Label></td>
                            <td colspan="2" style="text-align: center; height: 70px;" class="linaBajo">
                                <asp:Label ID="lblProfIdioma" runat="server" CssClass="lblNegro" Height="50px" Text="PROFESORES DE IDIOMAS"
                                    Width="80px" Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="linaBajo">
                                <asp:Label ID="lblPersDirCGH" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="HOMBRES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblPersDirCGM" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="MUJERES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblPersDirSGH" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="HOMBRES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblPersDirSGM" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="MUJERES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblPersDocenteH" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="HOMBRES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblPersDocenteM" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="MUJERES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblDocEdFisH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblDocEdFisM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblDocActArtH" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="HOMBRES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblDocActArtM" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="MUJERES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblDocIdiomH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblDocIdiomM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                <asp:Label ID="lblPersAdminH" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="HOMBRES" Width="100%"></asp:Label></td>
                            <td class="linaBajo">
                                 <asp:Label ID="lblPersAdminM" runat="server" CssClass="lblGrisTit" Height="17px"
                                     Text="MUJERES" Width="100%"></asp:Label></td>
                            <td class="Orila" style="width: 4px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left" class="linaBajoAlto">
                                <asp:Label ID="lblPrimInc" runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA" Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV132" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10101"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV133" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10102"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV134" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10103"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV135" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10104"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV136" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10105"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV137" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10106"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV138" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10107"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV139" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10108"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV140" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10109"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV141" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10110"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV142" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10111"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV143" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10112"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV144" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10113"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV145" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10114"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px; text-align: center">
                                &nbsp;</td>
                         </tr>
                              <tr>
                                  <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblPrimTerm" runat="server" CssClass="lblGrisTit" Text="PRIMARIA TERMINADA" Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV146" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10201"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV147" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10202"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV148" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10203"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV149" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10204"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV150" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10205"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV151" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10206"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV152" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10207"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV153" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10208"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV154" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10209"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV155" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10210"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV156" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10211"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV157" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10212"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV158" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10213"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV159" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10214"></asp:TextBox></td>
                                  <td class="Orila" style="width: 4px; text-align: center">
                                      &nbsp;</td>
                         </tr>
                              <tr>
                                  <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblSecInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="SECUNDARIA INCOMPLETA"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV160" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10301"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV161" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10302"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV162" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10303"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV163" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10304"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV164" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10305"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV165" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10306"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV166" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10307"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV167" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10308"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV168" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10309"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV169" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10310"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV170" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10311"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV171" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10312"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV172" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10313"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV173" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10314"></asp:TextBox></td>
                                  <td class="Orila" style="width: 4px; text-align: center">
                                      &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left; height: 25px;" class="linaBajo">
                                <asp:Label ID="lblSecTerm" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA TERMINADA"
                                    Width="200px"></asp:Label></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV174" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10401"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV175" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10402"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV176" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10403"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV177" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10404"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV178" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10405"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV179" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10406"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV180" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10407"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV181" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10408"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV182" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10409"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV183" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10410"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV184" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10411"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV185" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10412"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV186" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10413"></asp:TextBox></td>
                            <td style="text-align: center; height: 25px;" class="linaBajo">
                                <asp:TextBox ID="txtV187" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10414"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; height: 25px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left; height: 30px;" class="linaBajo">
                                <asp:Label ID="lblProfTec" runat="server" CssClass="lblGrisTit" Text="PROFESIONAL T�CNICO"
                                    Width="200px"></asp:Label></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV188" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10501"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV189" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10502"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV190" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10503"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV191" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10504"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV192" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10505"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV193" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10506"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV194" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10507"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV195" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10508"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV196" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10509"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV197" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10510"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV198" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10511"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV199" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10512"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV200" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10513"></asp:TextBox></td>
                            <td style="text-align: center; height: 30px;" class="linaBajo">
                                <asp:TextBox ID="txtV201" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10514"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; height: 30px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblBachInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO INCOMPLETO"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV202" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10601"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV203" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10602"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV204" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10603"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV205" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10604"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV206" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10605"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV207" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10606"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV208" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10607"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV209" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10608"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV210" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10609"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV211" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10610"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV212" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10611"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV213" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10612"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV214" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10613"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV215" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10614"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblBachTerm" runat="server" CssClass="lblGrisTit" Text="BACHILLERATO TERMINADO"
                                    Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV216" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10701"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV217" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10702"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV218" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10703"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV219" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10704"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV220" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10705"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV221" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10706"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV222" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10707"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV223" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10708"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV224" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10709"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV225" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10710"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV226" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10711"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV227" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10712"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV228" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10713"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV229" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10714"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left; height: 14px;" class="linaBajo">
                                <asp:Label ID="lblNormPreeInc" runat="server" CssClass="lblGrisTit"
                                    Text="NORMAL PREESCOLAR INCOMPLETA" Width="200px"></asp:Label></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV230" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10801"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV231" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10802"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV232" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10803"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV233" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10804"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV234" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10805"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV235" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10806"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV236" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10807"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV237" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10808"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV238" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10809"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV239" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10810"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV240" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10811"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV241" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10812"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV242" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10813"></asp:TextBox></td>
                            <td style="text-align: center; height: 14px;" class="linaBajo">
                                <asp:TextBox ID="txtV243" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10814"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; height: 14px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblNormPreeTerm" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="NORMAL PREESCOLAR TERMINADA" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV244" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10901"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV245" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10902"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV246" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10903"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV247" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10904"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV248" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10905"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV249" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="10906"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV250" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10907"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV251" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10908"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV252" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10909"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV253" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10910"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV254" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10911"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV255" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="10912"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV256" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10913"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV257" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="10914"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPrimInc" runat="server" CssClass="lblGrisTit"
                                    Text="NORMAL PRIMARIA INCOMPLETA" Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV258" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11001"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV259" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11002"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV260" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11003"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV261" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11004"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV262" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11005"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV263" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11006"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV264" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11007"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV265" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11008"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV266" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11009"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV267" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11010"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV268" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11011"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV269" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11012"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV270" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11013"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV271" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11014"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormPrimTerm" runat="server" CssClass="lblGrisTit"
                                    Text="NORMAL PRIMARIA TERMINADA" Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV272" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11201"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV273" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11202"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV274" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11203"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV275" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11204"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV276" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11205"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV277" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11206"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV278" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11207"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV279" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11208"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV280" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11209"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV281" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11210"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV282" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11211"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV283" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11212"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV284" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11213"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV285" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11214"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblNormSupInc" runat="server" CssClass="lblGrisTit" Height="17px"
                                    Text="NORMAL SUPERIOR INCOMPLETA" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV286" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11301"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV287" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11302"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV288" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11303"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV289" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11304"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV290" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11305"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV291" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11306"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV292" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11307"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV293" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11308"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV294" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11309"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV295" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11310"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV296" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11311"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV297" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11312"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV298" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11313"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV299" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11314"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormSupPas" runat="server" CssClass="lblGrisTit"
                                    Text="NORMAL SUPERIOR, PASANTE" Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV300" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11401"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV301" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11402"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV302" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11403"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV303" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11404"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV304" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11405"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV305" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11406"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV306" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11407"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV307" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11408"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV308" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11409"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV309" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11410"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV310" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11411"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV311" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11412"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV312" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11413"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV313" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11414"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblNormSupTit" runat="server" CssClass="lblGrisTit"
                                    Text="NORMAL SUPERIOR, TITULADO" Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV314" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11501"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV315" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11502"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV316" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11503"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV317" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11504"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV318" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11505"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV319" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11506"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV320" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11507"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV321" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11508"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV322" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11509"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV323" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11510"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV324" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11511"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV325" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11512"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV326" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11513"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV327" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11514"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left; height: 10px;" class="linaBajoS">
                                <asp:Label ID="lblLicInc" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA INCOMPLETA" Width="100%"></asp:Label></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV328" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11601"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV329" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11602"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV330" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11603"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV331" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11604"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV332" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11605"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV333" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11606"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV334" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11607"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV335" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11608"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV336" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11609"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV337" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11610"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV338" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11611"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV339" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11612"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV340" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11613"></asp:TextBox></td>
                            <td style="text-align: center; height: 10px;" class="linaBajoS">
                                <asp:TextBox ID="txtV341" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11614"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; height: 10px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblLicPas" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, PASANTE"
                                    Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV342" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11701"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV343" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11702"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV344" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11703"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV345" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11704"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV346" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11705"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV347" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11706"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV348" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11707"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV349" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11708"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV350" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11709"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV351" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11710"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV352" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11711"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV353" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11712"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV354" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11713"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV355" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11714"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblLicTit" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO"
                                    Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV356" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11801"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV357" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11802"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV358" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11803"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV359" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11804"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV360" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11805"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV361" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11806"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV362" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11807"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV363" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11808"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV364" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11809"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV365" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11810"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV366" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11811"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV367" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11812"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV368" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11813"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV369" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11814"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblMaestInc" runat="server" CssClass="lblGrisTit" Text="MAESTR�A INCOMPLETA"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV370" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11901"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV371" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11902"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV372" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11903"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV373" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11904"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV374" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11905"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV375" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="11906"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV376" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11907"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV377" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11908"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV378" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11909"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV379" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11910"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV380" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11911"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV381" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="11912"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV382" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11913"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV383" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="11914"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblMaestGrad" runat="server" CssClass="lblGrisTit" Text="MAESTR�A, GRADUADO"
                                    Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV384" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12001"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV385" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12002"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV386" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12003"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV387" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12004"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV388" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12005"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV389" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12006"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV390" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12007"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV391" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12008"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV392" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12009"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV393" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12010"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV394" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12011"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV395" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12012"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV396" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12013"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV397" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12014"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajo">
                                <asp:Label ID="lblDocInc" runat="server" CssClass="lblGrisTit" Text="DOCTORADO INCOMPLETO"
                                    Width="200px"></asp:Label></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV398" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12101"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV399" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12102"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV400" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12103"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV401" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12104"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV402" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12105"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV403" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12106"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV404" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12107"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV405" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12108"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV406" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12109"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV407" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12110"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV408" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12111"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV409" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12112"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV410" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12113"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV411" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12114"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                                <tr>
                                    <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblDocGrad" runat="server" CssClass="lblGrisTit" Text="DOCTORADO, GRADUADO"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV412" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12201"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV413" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12202"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV414" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12203"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV415" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12204"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV416" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12205"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV417" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12206"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV418" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12207"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV419" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12208"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV420" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12209"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV421" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12210"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV422" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12211"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV423" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12212"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV424" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12213"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV425" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12214"></asp:TextBox></td>
                                    <td class="Orila" style="width: 4px; text-align: center">
                                        &nbsp;</td>
                         </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS*" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 4px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblEspecifique" runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE:" Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="text-align: center">
                            </td>
                            <td style="width: 4px; text-align: center">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="linaBajoAlto">
                                <asp:TextBox ID="txtV426" runat="server" Columns="35" CssClass="lblNegro" MaxLength="30" TabIndex="12301"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV427" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12302"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV428" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12303"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV429" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12304"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV430" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12305"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV431" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12306"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV432" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12307"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV433" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12308"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV434" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12309"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV435" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12310"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV436" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12311"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV437" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12312"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV438" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12313"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV439" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12314"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoAlto">
                                <asp:TextBox ID="txtV440" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12315"></asp:TextBox></td>
                            <td class="Orila" style="width: 4px; text-align: center">
                                &nbsp;</td>
                        </tr>
                         <tr>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV441" runat="server" Columns="35" CssClass="lblNegro" MaxLength="30" TabIndex="12401"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV442" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12402"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV443" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12403"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV444" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12404"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV445" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12405"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV446" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12406"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV447" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12407"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV448" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12408"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV449" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12409"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV450" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12410"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV451" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12411"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV452" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12412"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV453" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12413"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV454" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12414"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV455" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12415"></asp:TextBox></td>
                             <td class="Orila" style="width: 4px; text-align: center">
                                 &nbsp;</td>
                        </tr>
                         <tr>
                            <td class="linaBajo">
                                <asp:TextBox ID="txtV456" runat="server" Columns="35" CssClass="lblNegro" MaxLength="30" TabIndex="12501"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV457" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12502"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV458" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12503"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV459" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12504"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV460" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12505"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV461" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12506"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV462" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3" TabIndex="12507"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV463" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12508"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV464" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12509"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV465" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12510"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV466" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12511"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV467" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12512"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV468" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1" TabIndex="12513"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV469" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12514"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajo">
                                <asp:TextBox ID="txtV470" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12515"></asp:TextBox></td>
                             <td class="Orila" style="width: 4px; text-align: center">
                                 &nbsp;</td>
                        </tr>
                         <tr>
                         <td style="text-align: left" class="linaBajoS">
                                <asp:Label ID="lblSubtotales" runat="server" CssClass="lblGrisTit" Text="SUBTOTALES" Width="100%"></asp:Label></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV471" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12601"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV472" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12602"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV473" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12603"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV474" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12604"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV475" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4" TabIndex="12605"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV476" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4" TabIndex="12606"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV477" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12607"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV478" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12608"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV479" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12609"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV480" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12610"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV481" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12611"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV482" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12612"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV483" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12613"></asp:TextBox></td>
                            <td style="text-align: center" class="linaBajoS">
                                <asp:TextBox ID="txtV484" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2" TabIndex="12614"></asp:TextBox></td>
                             <td class="Orila" style="width: 4px; text-align: center">
                                 &nbsp;</td>
                            
                        </tr>
                        <tr>
                            <td style="height: 25px; text-align: left;">
                            </td>
                            <td style="height: 25px; text-align: center;">
                            </td>
                            <td style="height: 25px">
                            </td>
                            <td style="height: 25px">
                            </td>
                            <td style="height: 25px; text-align: center;">
                            </td>
                            <td style="height: 25px">
                            </td>
                            <td style="height: 25px">
                            </td>
                            <td style="height: 25px">
                            </td>
                            <td style="height: 25px">
                            </td>
                            <td colspan="4" style="height: 25px; text-align: center;">
                                <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Text="TOTALE DE PERSONAL (Suma de subtotales)"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="height: 25px; text-align: center">
                                <asp:TextBox ID="txtV485" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4" TabIndex="12701"></asp:TextBox></td>
                            <td colspan="1" style="width: 4px; height: 25px; text-align: center">
                                &nbsp;</td>
                        </tr>
                              
                  
                         
                         
                    </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center" valign="top">
                                <table>
                                    <tr>
                                        <td style="text-align: justify">
                                            <asp:Label ID="lblInstrucciones2" runat="server" CssClass="lblRojo" Text="2. Escriba la cantidad de personal administrativo, auxiliar y de servicios seg�n su funci�n y sexo. (El total debe coincidir con el total del rubro de personal administrativo, auxiliar y de servicios reportado en el punto 1 de esta secci�n )" Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <table>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                            <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:Label ID="lblTotal2" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblAdministrativo" runat="server" CssClass="lblGrisTit" Text="ADMINISTRATIVO"
                                                Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV486" runat="server" Columns="2" TabIndex="20101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV487" runat="server" Columns="2" TabIndex="20102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV488" runat="server" Columns="2" TabIndex="20103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblIntendente" runat="server" CssClass="lblGrisTit" Text="ASISTENTE DE SERVICIOS (INTENDENTE)"
                                                Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV489" runat="server" Columns="2" TabIndex="20201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV490" runat="server" Columns="2" TabIndex="20202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV491" runat="server" Columns="2" TabIndex="20203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblConserje" runat="server" CssClass="lblGrisTit" Text="ASISTENTE DE SERVICIOS Y MANTENIMIENTO (CONSERJE)"
                                                Width="200px"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV492" runat="server" Columns="2" TabIndex="20301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV493" runat="server" Columns="2" TabIndex="20302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV494" runat="server" Columns="2" TabIndex="20303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblCocinera" runat="server" CssClass="lblGrisTit" Text="COCINERA"
                                                Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV495" runat="server" Columns="2" TabIndex="20401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV496" runat="server" Columns="2" TabIndex="20402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV497" runat="server" Columns="2" TabIndex="20403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblAuxiliarCocina" runat="server" CssClass="lblGrisTit" Text="AUXILIAR DE COCINA"
                                                Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV498" runat="server" Columns="2" TabIndex="20501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV499" runat="server" Columns="2" TabIndex="20502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV500" runat="server" Columns="2" TabIndex="20503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblOtros2" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV501" runat="server" Columns="2" TabIndex="20601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV502" runat="server" Columns="2" TabIndex="20602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV503" runat="server" Columns="2" TabIndex="20603" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="Label51" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV504" runat="server" Columns="2" TabIndex="20701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV505" runat="server" Columns="2" TabIndex="20702" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                    <td>
                                            <asp:TextBox ID="txtV506" runat="server" Columns="2" TabIndex="20703" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="text-align: center" valign="top">
                                <table>
                                    <tr>
                                        <td style="text-align: justify">
                                            <asp:Label ID="lblInstrucciones3" runat="server" CssClass="lblRojo" Text="3. Escriba la cantidad de personal docente m�s directivo con grupo, por grado."
                                                Width="265px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: justify">
                                            <asp:Label ID="lblInstrucciones3I" runat="server" CssClass="lblRojo" Text="IMPORTANTE: Si un profesor atiende m�s de un grado, an�telo en el rubro correspondiente; el total debe de coincidir con la suma de directivo con grupo m�s personal docente de la pregunta 1 de esta secci�n."
                                                Width="265px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <table>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblPrimero" runat="server" CssClass="lblGrisTit" Text="PRIMERO" Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV507" runat="server" Columns="4" TabIndex="30101" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblSegundo" runat="server" CssClass="lblGrisTit" Text="SEGUNDO" Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV508" runat="server" Columns="4" TabIndex="30201" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblTercero" runat="server" CssClass="lblGrisTit" Text="TERCERO" Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV509" runat="server" Columns="4" TabIndex="30301" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; height: 26px;">
                                            <asp:Label ID="lblMasUnGrado" runat="server" CssClass="lblGrisTit" Text="M�S DE UN GRADO"
                                                Width="100%"></asp:Label></td>
                                                    <td style="height: 26px">
                                            <asp:TextBox ID="txtV510" runat="server" Columns="4" TabIndex="30401" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblTotal3" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV511" runat="server" Columns="4" TabIndex="30501" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="text-align: center" valign="top">
                                <table>
                                    <tr>
                                        <td style="text-align: justify">
                                            <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="4. Escriba la cantidad de horas impartidas a la semana por el personal docente especial en el centro de trabajo."
                                                Width="330px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <table>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblHorasEdFis" runat="server" CssClass="lblGrisTit" Text="Profesores de educaci�n f�sica"
                                                Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV512" runat="server" Columns="3" TabIndex="030601" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblHorasActArt" runat="server" CssClass="lblGrisTit" Text="Profesores de m�sica, acompa�amiento musical, danza, etc�tera (actividades art�sticas)."
                                                Width="290px"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV513" runat="server" Columns="3" TabIndex="030701" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left">
                                            <asp:Label ID="lblHorasIdiom" runat="server" CssClass="lblGrisTit" Text="Profesores de idiomas."
                                                Width="100%"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV514" runat="server" Columns="3" TabIndex="030801" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <table align="center">
                        <tr>
                            <td ><span  onclick="OpenPageCharged('Alumnos_911_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                            <td ><span  onclick="OpenPageCharged('Alumnos_911_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                            <td style="width: 330px;">
                            &nbsp;
                            </td>
                            <td ><span  onclick="OpenPageCharged('CMYA_911_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                            <td ><span  onclick="OpenPageCharged('CMYA_911_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                        </tr>
                    </table>
                    
                    <div id="divResultado" class="divResultado" ></div>
                    
                     <!--/Contenido dentro del area de trabajo-->
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
         </table>
         <!--/Tabla Fondo-->

        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 16;
                MaxRow = 31;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                Disparador(<%=this.hidDisparador.Value %>);  
                function OpenPageCharged(ventana,bool){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV426'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV441'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV456'));
                    openPage(ventana,bool);
                }
                function ValidaTexto(obj){
                    if(obj.value == ''){
                       obj.value = '_';
                    }
                }                            
        </script> 
        <script type="text/javascript">
        
    </script>
</center> 
</asp:Content> 
