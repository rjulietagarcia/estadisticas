<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Inicial y Preescolar" AutoEventWireup="true" CodeBehind="AG1_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.AG1_CAM_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N ESPECIAL"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table></div></div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_CAM_1',true)"><a href="#" title="" class="activo"><span>INICIAL Y PREE</span></a></li>
        <li onclick="openPage('AG2_CAM_1',false)"><a href="#" title="" ><span>PRIM Y SEC</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL(CONT)</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAG Y AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
           
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


    <table style="width: 1000px">
        <tr>
            <td colspan="2" style="text-align:justify">
                <asp:Label ID="lblALUMNOS" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                    Text="I. ALUMNOS Y GRUPOS" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="lblINICIAL" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EDUCACI�N INICIAL"
                    Width="100%"></asp:Label></td>
            <td>
                <asp:Label ID="lblPREESCOLAR" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EDUCACI�N PREESCOLAR"
                    Width="100%"></asp:Label></td>
        </tr>
            <tr>
                <td valign="top">

    <table>
        <tr>
            <td colspan="6" style="text-align: justify">
                <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True" 
                Text="1. Escriba el n�mero de alumnos inscritos, por sexo, 
                y el n�mero de grupos de educaci�n inicial, seg�n los rubros que se indican."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="6" style="height: 20px; text-align: center">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblNuevoIngreso1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NUEVO INGRESO"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblReIngreso1" runat="server" CssClass="lblNegro" Font-Bold="True" Text="REINGRESO"
                    Width="100%"></asp:Label></td>
            <td rowspan="2" style="text-align: center">
                <asp:Label ID="lblTotal11" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                    Width="65px"></asp:Label></td>
            <td rowspan="2" style="text-align: center">
                <asp:Label ID="lblGrupos1" runat="server" CssClass="lblNegro" Font-Bold="True"  Text="GRUPOS" Width="70px"></asp:Label></td>
        </tr>
    <tr>
    <td style="text-align: center"><asp:Label ID="lblHom11" runat="server" CssClass="lblGrisTit" Font-Bold="True"  Text="HOM" Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:Label ID="lblMuj11" runat="server" CssClass="lblGrisTit" Font-Bold="True"  Text="MUJ" Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:Label ID="lblHom12" runat="server" CssClass="lblGrisTit" Font-Bold="True"  Text="HOM" Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:Label ID="lblMuj12" runat="server" CssClass="lblGrisTit" Font-Bold="True"  Text="MUJ" Width="100%"></asp:Label></td>
    </tr>
    <tr>
    <td style="text-align: center"><asp:TextBox ID="txtV1" runat="server" Columns="2" MaxLength="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV2" runat="server" Columns="2" MaxLength="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV3" runat="server" Columns="2" MaxLength="2" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV4" runat="server" Columns="2" MaxLength="2" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV5" runat="server" Columns="3" MaxLength="3" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV6" runat="server" Columns="2" MaxLength="2" TabIndex="10106" CssClass="lblNegro"></asp:TextBox></td>  
    </tr>
    </table>
                </td>
                <td>
     <table>
         <tr>
             <td colspan="7" rowspan="1" style="text-align: justify">
                 <asp:Label ID="lblInstruccion2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Escriba el n�mero de alumnos inscritos, por sexo, y el n�mero de grupos de educaci�n preescolar, seg�n los rubros que se indican."
                     Width="450px"></asp:Label></td>
         </tr>
         <tr>
             <td colspan="7" nowrap="nowrap" rowspan="1" style="height: 20px; text-align: center">
             </td>
         </tr>
         <tr>
             <td style="text-align: center">
             </td>
             <td colspan="2" style="text-align: center">
                 <asp:Label ID="lblNuevoIngreso2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NUEVO INGRESO"
                     Width="100%"></asp:Label></td>
             <td colspan="2" style="text-align: center">
                 <asp:Label ID="lblReIngreso2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="REINGRESO"
                     Width="100%"></asp:Label></td>
             <td rowspan="2" style="text-align: center">
                 <asp:Label ID="lblTotal21" runat="server" CssClass="lblNegro" Font-Bold="True"  Text="TOTAL" Width="100%"></asp:Label></td>
             <td rowspan="2" style="text-align: center">
                 <asp:Label ID="lblGrupos2" runat="server" CssClass="lblNegro" Font-Bold="True"  Text="GRUPOS" Width="100%"></asp:Label></td>
         </tr>
    <tr>
        <td style="text-align: center">
        </td>
    <td style="text-align: center"><asp:Label ID="lblHom21" runat="server" CssClass="lblGrisTit" Font-Bold="True"  Text="HOM" Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:Label ID="lblMuj21" runat="server" CssClass="lblGrisTit" Font-Bold="True"  Text="MUJ" Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:Label ID="lblHom22" runat="server" CssClass="lblGrisTit" Font-Bold="True"  Text="HOM" Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:Label ID="lblMuj22" runat="server" CssClass="lblGrisTit" Font-Bold="True"  Text="MUJ" Width="100%"></asp:Label></td>
    </tr>
    <tr>
    <td><asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o." Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:TextBox ID="txtV34" runat="server" Columns="2" MaxLength="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV35" runat="server" Columns="2" MaxLength="2" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV36" runat="server" Columns="2" MaxLength="2" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV37" runat="server" Columns="2" MaxLength="2" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV38" runat="server" Columns="3" MaxLength="3" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV39" runat="server" Columns="2" MaxLength="2" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>              
    </tr>
    <tr>
    <td><asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o." Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:TextBox ID="txtV40" runat="server" Columns="2" MaxLength="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV41" runat="server" Columns="2" MaxLength="2" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV42" runat="server" Columns="2" MaxLength="2" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV43" runat="server" Columns="2" MaxLength="2" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV44" runat="server" Columns="3" MaxLength="3" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV45" runat="server" Columns="2" MaxLength="2" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>              
    </tr>
    <tr>
    <td><asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o." Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:TextBox ID="txtV46" runat="server" Columns="2" MaxLength="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV47" runat="server" Columns="2" MaxLength="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV48" runat="server" Columns="2" MaxLength="2" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV49" runat="server" Columns="2" MaxLength="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV50" runat="server" Columns="3" MaxLength="3" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV51" runat="server" Columns="2" MaxLength="2" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>              
    </tr>
    <tr>
    <td><asp:Label ID="lblTotal22" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
    <td style="text-align: center"><asp:TextBox ID="txtV52" runat="server" Columns="2" MaxLength="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV53" runat="server" Columns="2" MaxLength="2" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV54" runat="server" Columns="2" MaxLength="2" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV55" runat="server" Columns="2" MaxLength="2" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV56" runat="server" Columns="3" MaxLength="3" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
    <td style="text-align: center"><asp:TextBox ID="txtV57" runat="server" Columns="2" MaxLength="2" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>              
    </tr>
    </table>
                </td>
            </tr>
            <tr>
                <td>
    <table>
        <tr>
            <td colspan="4" style="text-align: justify">
                <asp:Label ID="lblInstruccion1b" runat="server" CssClass="lblRojo" 
                Text="De los alumnos reportados en el rubro anterior, escriba la cantidad de alumnos con 
                discapacidad, aptitudes sobresalientes u otras condiciones, 
                seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
             <td style="text-align: center">
                <asp:Label ID="lblSituacion1" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal12" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblCeguera1" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
          <td style="text-align: center">
                <asp:TextBox ID="txtV7" runat="server" Columns="3" MaxLength="3" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
           <td style="text-align: center">
                <asp:TextBox ID="txtV8" runat="server" Columns="3" MaxLength="3" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
           <td style="text-align: center">
                <asp:TextBox ID="txtV9" runat="server" Columns="4" MaxLength="4" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
           <td style="text-align:justify">
                <asp:Label ID="lblVisual1" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                    Width="100%"></asp:Label></td>
           <td style="text-align: center">
                <asp:TextBox ID="txtV10" runat="server" Columns="3" MaxLength="3" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV11" runat="server" Columns="3" MaxLength="3" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
         <td style="text-align: center">
                <asp:TextBox ID="txtV12" runat="server" Columns="4" MaxLength="4" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
             <td style="text-align:justify">
                <asp:Label ID="lblSordera1" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
           <td style="text-align: center">
                <asp:TextBox ID="txtV13" runat="server" Columns="3" MaxLength="3" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV14" runat="server" Columns="3" MaxLength="3" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
           <td style="text-align: center">
                <asp:TextBox ID="txtV15" runat="server" Columns="4" MaxLength="4" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
           <td style="text-align:justify">
                <asp:Label ID="lblAuditiva1" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                    Width="100%"></asp:Label></td>
         <td style="text-align: center">
                <asp:TextBox ID="txtV16" runat="server" Columns="3" MaxLength="3" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
          <td style="text-align: center">
                <asp:TextBox ID="txtV17" runat="server" Columns="3" MaxLength="3" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
           <td style="text-align: center">
                <asp:TextBox ID="txtV18" runat="server" Columns="4" MaxLength="4" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
           <td style="text-align:justify">
                <asp:Label ID="lblMotriz1" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
          <td style="text-align: center">
                <asp:TextBox ID="txtV19" runat="server" Columns="3" MaxLength="3" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
          <td style="text-align: center">
                <asp:TextBox ID="txtV20" runat="server" Columns="3" MaxLength="3" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
          <td style="text-align: center">
                <asp:TextBox ID="txtV21" runat="server" Columns="4" MaxLength="4" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
           <td style="text-align:justify">
                <asp:Label ID="lblIntelectual1" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
          <td style="text-align: center">
                <asp:TextBox ID="txtV22" runat="server" Columns="3" MaxLength="3" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
           <td style="text-align: center">
                <asp:TextBox ID="txtV23" runat="server" Columns="3" MaxLength="3" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
         <td style="text-align: center">
                <asp:TextBox ID="txtV24" runat="server" Columns="4" MaxLength="4" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="noWrap" style="text-align:justify">
                <asp:Label ID="lblSobresalientes1" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV25" runat="server" Columns="3" MaxLength="3" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV26" runat="server" Columns="3" MaxLength="3" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV27" runat="server" Columns="4" MaxLength="4" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
           <td style="text-align:justify">
                <asp:Label ID="lblOtros1" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="100%"></asp:Label></td>
        <td style="text-align: center">
                <asp:TextBox ID="txtV28" runat="server" Columns="3" MaxLength="3" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV29" runat="server" Columns="3" MaxLength="3" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
          <td style="text-align: center">
                <asp:TextBox ID="txtV30" runat="server" Columns="4" MaxLength="4" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
          <td>
                <asp:Label ID="lblTotal13" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
           <td style="text-align: center">
                <asp:TextBox ID="txtV31" runat="server" Columns="3" MaxLength="3" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
         <td style="text-align: center">
                <asp:TextBox ID="txtV32" runat="server" Columns="3" MaxLength="3" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
        <td style="text-align: center">
                <asp:TextBox ID="txtV33" runat="server" Columns="4" MaxLength="4" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                </td>
                <td>
    <table>
        <tr>
            <td colspan="4" style="text-align: justify">
                <asp:Label ID="lblInstruccion2b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba la cantidad de alumnos con 
                discapacidad, aptitudes sobresalientes u otras condiciones, 
                seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
          <td style="text-align: center">
                <asp:Label ID="lblSituacion2" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
             <td style="text-align: center">
                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal23" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblCeguera2" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV58" runat="server" Columns="3" MaxLength="3" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV59" runat="server" Columns="3" MaxLength="3" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV60" runat="server" Columns="4" MaxLength="4" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblVisual2" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV61" runat="server" Columns="3" MaxLength="3" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV62" runat="server" Columns="3" MaxLength="3" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV63" runat="server" Columns="4" MaxLength="4" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblSordera2" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV64" runat="server" Columns="3" MaxLength="3" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV65" runat="server" Columns="3" MaxLength="3" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV66" runat="server" Columns="4" MaxLength="4" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblAuditiva2" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV67" runat="server" Columns="3" MaxLength="3" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV68" runat="server" Columns="3" MaxLength="3" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV69" runat="server" Columns="4" MaxLength="4" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblMotriz2" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV70" runat="server" Columns="3" MaxLength="3" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV71" runat="server" Columns="3" MaxLength="3" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV72" runat="server" Columns="4" MaxLength="4" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblIntelectual2" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV73" runat="server" Columns="3" MaxLength="3" TabIndex="21001" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV74" runat="server" Columns="3" MaxLength="3" TabIndex="21002" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV75" runat="server" Columns="4" MaxLength="4" TabIndex="21003" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td nowrap="noWrap" style="text-align:justify">
                <asp:Label ID="lblSobresalientes2" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV76" runat="server" Columns="3" MaxLength="3" TabIndex="21101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV77" runat="server" Columns="3" MaxLength="3" TabIndex="21102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV78" runat="server" Columns="4" MaxLength="4" TabIndex="21103" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblOtros2" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV79" runat="server" Columns="3" MaxLength="3" TabIndex="21201" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV80" runat="server" Columns="3" MaxLength="3" TabIndex="21202" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV81" runat="server" Columns="4" MaxLength="4" TabIndex="21203" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTotal24" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV82" runat="server" Columns="3" MaxLength="3" TabIndex="21301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV83" runat="server" Columns="3" MaxLength="3" TabIndex="21302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV84" runat="server" Columns="4" MaxLength="4" TabIndex="21303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('AG2_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG2_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 20;
                MaxRow = 20;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
