<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Personal" AutoEventWireup="true" CodeBehind="Personal1_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.Personal1_CAM_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <div id="logo"></div>
    <div style="min-width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server" Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N ESPECIAL"></asp:Label></td>
        </tr>
         <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div></div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span>INICIAL Y PREE</span></a></li><li onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><span>PRIM Y SEC</span></a></li><li onclick="openPage('AG3_CAM_1',true)"><a href="#" title=""><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li><li onclick="openPage('Desgolse_CAM_1',true)"><a href="#" title=""><span>DESGLOSE</span></a></li><li onclick="openPage('Personal1_CAM_1',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li><li onclick="openPage('Personal2_CAM_1',false)"><a href="#" title="" ><span>PERSONAL(CONT)</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAG Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 1000px">
            <tr>
                <td colspan="2" rowspan="1" valign="top" style="padding-bottom: 20px; text-align: left">
                    <asp:Label ID="lblPERSONAL" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                        Text="II. PERSONAL POR FUNCI�N" Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td rowspan="1" valign="top">
                    <table style="width: 320px">
                        <tr>
                            <td style="text-align: left">
                    <asp:Label ID="lblDIRECTOR" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DIRECTOR"
                        Width="100%" Font-Size="11pt"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Marque la situaci�n del Director"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblConGrupo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CON GRUPO"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            &nbsp;<asp:TextBox ID="txtV791" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:RadioButton ID="optV791" onclick = "OPTs2Txt();" runat="server" GroupName="Situacion1"  TabIndex="10101" onkeydown="return Arrows(event,this. TabIndex)"  /></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblSinGrupo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SIN GRUPO"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV792" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:RadioButton ID="optV792" onclick = "OPTs2Txt();" runat="server" GroupName="Situacion1"  TabIndex="10201" onkeydown="return Arrows(event,this. TabIndex)" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInstruccion2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. Escriba la clave y el nombre de su nivel m�ximo de estudios y de su especialidad, de acuerdo con las tablas de nivel m�ximo de estudios y de especialidad que est�n en el reverso de la p�gina anterior."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td colspan="2" style="text-align: left">
                                            <asp:Label ID="lblNivelMax" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NIVEL M�XIMO DE ESTUDIOS"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV793" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV794" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:DropDownList ID="ddl_Nivel" onchange="OnChange_Nivel(this)" runat="server" Width="150px">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left">
                                            <asp:Label ID="lblEspecialidad" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ESPECIALIDAD"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV795" runat="server" Columns="1" MaxLength="1" CssClass="lblNegro" Enabled="False"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV796" runat="server" Columns="1" MaxLength="30" CssClass="lblNegro" style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:DropDownList ID="ddl_Especialidad" onchange="OnChange_Especialidad(this)" runat="server" Width="150px">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblOtra" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OTRA"
                                                Width="100%"></asp:Label></td>
                                        <td colspan="2">
                                            <asp:Label ID="lblEspecifique" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ESPECIFIQUE:"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV797" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro"  TabIndex="10301" ></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInstruccion3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Indique el sexo del director."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblHombre" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOMBRE"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            &nbsp;<asp:TextBox ID="txtV798" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:RadioButton ID="optV798" onclick = "OPTs2Txt();" runat="server" GroupName="Sexo"  TabIndex="10401" onkeydown="return Arrows(event,this. TabIndex)"/></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblMujer" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJER"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV799" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:RadioButton ID="optV799" onclick = "OPTs2Txt();" runat="server" GroupName="Sexo"  TabIndex="10501" onkeydown="return Arrows(event,this. TabIndex)"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInstruccion4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Marque su situaci�n acad�mica"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 20px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblNegro" runat="server" CssClass="lblNegro" Font-Bold="True" Text="POSGRADO"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtV800" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:RadioButton ID="optV800" onclick = "OPTs2Txt();" runat="server" GroupName="Situacion4"  TabIndex="10601" onkeydown="return Arrows(event,this. TabIndex)"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left">
                                            <asp:Label ID="lblLicenciatura4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="LICENCIATURA"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblTitulado" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TITULADO"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV801" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:RadioButton ID="optV801" onclick = "OPTs2Txt();" runat="server" GroupName="Situacion4"  TabIndex="10701" onkeydown="return Arrows(event,this. TabIndex)"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblNoTitulado" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NO TITULADO"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV802" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:RadioButton ID="optV802" onclick = "OPTs2Txt();" runat="server" GroupName="Situacion4"  TabIndex="10801" onkeydown="return Arrows(event,this. TabIndex)"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblEstudiante" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ESTUDIANTE"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV803" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px;"></asp:TextBox>
                                            <asp:RadioButton ID="optV803" onclick = "OPTs2Txt();" runat="server" GroupName="Situacion4"  TabIndex="10901" onkeydown="return Arrows(event,this. TabIndex)" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: right; padding-left: 20px;">
                    <table>
                        <tr>
                            <td colspan="9" style="text-align: left">
                    <asp:Label ID="lblDOCENTE" runat="server" CssClass="lblRojo" Font-Bold="True" Text="DOCENTE"
                        Width="100%" Font-Size="11pt"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: justify">
                                <asp:Label ID="lblInstruccion5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5. Registre la cantidad de personal docente por formaci�n, seg�n su situaci�n acad�mica y sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: justify">
                                </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                            </td>
                            <td colspan="8" style="text-align: center">
                                <asp:Label ID="lblLicenciatura51" runat="server" CssClass="lblRojo" Font-Bold="True" Text="L I C E N C I A T U R A"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                            </td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblTitulados51" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TITULADOS"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblNoTitulados51" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NO TITULADOS"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblEstudiantes51" runat="server" CssClass="lblNegro" Font-Bold="True" Text="ESTUDIANTES"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblTotal51" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblFormacion" runat="server" CssClass="lblRojo" Font-Bold="True" Text="FORMACI�N"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHom51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMuj51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHom52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMuj52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHom53" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMuj53" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHom54" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMuj54" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblMental" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN DEFICIENCIA MENTAL"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV804" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV805" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV806" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV807" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV808" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20105"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV809" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20106"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV810" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20107"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV811" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20108"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblAudicion" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN AUDICI�N Y LENGUAJE"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV812" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV813" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV814" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV815" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20204"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV816" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20205"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV817" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20206"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV818" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20207"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV819" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20208"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblAprendizaje" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN APRENDIZAJE"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV820" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV821" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV822" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV823" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20304"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV824" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20305"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV825" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20306"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV826" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20307"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV827" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20308"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblVisual" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN DEFICIENCIA VISUAL"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV828" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV829" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV830" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV831" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20404"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV832" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20405"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV833" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20406"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV834" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20407"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV835" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20408"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblLocomotor" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN APARATO LOCOMOTOR"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV836" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV837" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV838" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20503"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV839" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20504"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV840" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20505"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV841" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20506"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV842" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20507"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV843" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20508"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInadaptacion" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN INADAPTACI�N SOCIAL"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV844" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV845" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV846" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20603"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV847" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20604"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV848" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20605"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV849" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20606"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV850" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20607"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV851" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20608"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblPedagogo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PEDAGOGO"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV852" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV853" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20702"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV854" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20703"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV855" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20704"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV856" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20705"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV857" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20706"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV858" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20707"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV859" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20708"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblPsicologo" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PSIC�LOGO"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV860" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV861" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20802"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV862" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20803"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV863" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20804"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV864" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20805"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV865" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20806"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV866" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20807"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV867" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20808"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblMaestroPre" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO DE PREESCOLAR"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV868" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20901"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV869" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20902"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV870" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20903"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV871" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20904"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV872" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20905"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV873" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20906"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV874" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20907"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV875" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20908"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblMaestroPri" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO DE PRIMARIA"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV876" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21001"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV877" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21002"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV878" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21003"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV879" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21004"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV880" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21005"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV881" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21006"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV882" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21007"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV883" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21008"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblIntegracionE" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN INTEGRACI�N EDUCATIVA"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV884" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21101"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV885" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21102"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV886" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21103"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV887" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21104"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV888" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21105"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV889" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21106"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV890" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21107"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV891" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21108"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblMaestroTaller" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO DE TALLER"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV892" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21201"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV893" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21202"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV894" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21203"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV895" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21204"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV896" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21205"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV897" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21206"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV898" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21207"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV899" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21208"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblInstructor" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="INSTRUCTOR"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV900" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21301"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV901" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21302"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV902" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21303"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV903" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21304"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV904" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21305"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV905" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21306"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV906" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21307"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV907" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21308"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="9" style="text-align: left">
                                <br />
                                <asp:Label ID="lblInstruccion5b" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Si existe personal docente con formaci�n diferente de las especialidades, registre la cantidad de personal de acuerdo con su situaci�n acad�mica y sexo."
                                    Width="600px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                            </td>
                            <td colspan="8" style="text-align: center">
                                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="L I C E N C I A T U R A"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                            </td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="Label2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TITULADOS"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="Label3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NO TITULADOS"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="Label4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="ESTUDIANTES"
                                    Width="100%"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="Label5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="FORMACI�N"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV908" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21401"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV909" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21402"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV910" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21403"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV911" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21404"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV912" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21405"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV913" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21406"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV914" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="21407"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV915" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21408"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV916" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21409"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV917" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21501"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV918" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21502"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV919" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21503"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV920" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21504"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV921" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21505"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV922" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21506"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV923" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="21507"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV924" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21508"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV925" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21509"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV926" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21601"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV927" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21602"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV928" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21603"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV929" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21604"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV930" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21605"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV931" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21606"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV932" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="21607"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV933" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21608"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV934" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21609"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV935" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21701"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV936" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21702"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV937" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21703"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV938" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21704"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV939" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21705"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV940" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21706"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV941" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="21707"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV942" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21708"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV943" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21709"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV944" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21801"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV945" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21802"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV946" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21803"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV947" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21804"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV948" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21805"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV949" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21806"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV950" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="21807"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV951" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21808"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV952" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21809"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV953" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21901"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV954" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21902"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV955" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21903"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV956" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21904"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV957" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21905"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV958" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="21906"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV959" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="21907"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV960" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21908"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV961" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="21909"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtV962" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="22001"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV963" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="22002"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV964" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="22003"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV965" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="22004"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV966" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="22005"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV967" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="22006"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV968" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="22007"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV969" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22008"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV970" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22009"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td colspan="4">
                                <asp:Label ID="lblTotal53" runat="server" CssClass="lblRojo" Text="TOTAL (Suma de HOM y MUJ)"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV971" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22101"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('Desgolse_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Desgolse_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="OpenPageCharged('Personal2_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Personal2_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
       <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 15;
                MaxRow = 25;
                 TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                     function OPTs2Txt(){
                     marcarTXT('791');
                     marcarTXT('792'); 
                     marcarTXT('798');
                     marcarTXT('799');
                     marcarTXT('800');
                     marcarTXT('801');
                     marcarTXT('802');
                     marcarTXT('803');       
                } 
                function PintaOPTs(){
                     marcar('791');
                     marcar('792'); 
                     marcar('798');
                     marcar('799');
                     marcar('800');
                     marcar('801');
                     marcar('802');
                     marcar('803');       
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                 PintaOPTs();
                 function ValidaTexto(obj){
                    if(obj.value.toString() == ''){
                       obj.value = '_';
                    }
                }    
                function OpenPageCharged(ventana){
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV794'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV796'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV797'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV908'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV917'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV926'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV935'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV944'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV953'));
                    ValidaTexto(document.getElementById('ctl00_cphMainMaster_txtV962'));
                    openPage(ventana);
                    
                } 
                
              function OnChange_Nivel(dropdown)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    var SelText = dropdown.options[myindex].text;
                    document.getElementById("ctl00_cphMainMaster_txtV793").value = SelValue;
                    var txt = document.getElementById("ctl00_cphMainMaster_txtV794");
                    txt.value = SelText;
                  
                    return true;
                }
              function OnChange_Especialidad(dropdown)
                {
                    var myindex  = dropdown.selectedIndex;
                    var SelValue = dropdown.options[myindex].value;
                    var SelText = dropdown.options[myindex].text;
                    document.getElementById("ctl00_cphMainMaster_txtV795").value = SelValue;
                    var txt = document.getElementById("ctl00_cphMainMaster_txtV796");
                    txt.value = SelText;
                  
                    return true;
                } 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
