<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Personal" AutoEventWireup="true" CodeBehind="Personal2_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.Personal2_CAM_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <div id="logo"></div>
    <div style="width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N ESPECIAL"></asp:Label></td>
        </tr>
         <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div>
    </div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span>INICIAL Y PREE</span></a></li><li onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><span>PRIM Y SEC</span></a></li><li onclick="openPage('AG3_CAM_1',true)"><a href="#" title=""><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li><li onclick="openPage('Desgolse_CAM_1',true)"><a href="#" title=""><span>DESGLOSE</span></a></li><li onclick="openPage('Personal1_CAM_1',true)"><a href="#" title=""><span>PERSONAL</span></a></li><li onclick="openPage('Personal2_CAM_1',true)"><a href="#" title="" class="activo"><span>PERSONAL(CONT)</span></a></li><li onclick="openPage('CMYA_CAM_1',false)"><a href="#" title="" ><span>CARRERA MAG Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0"  >
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer" style="width:1px"></td>
				</tr>
				<tr style="width:1200px">
					<td class="RepLatIzq"></td>
				    <td style="width:1200px">
            <%--a aqui--%>
            <div style="width:1200px">
                        <table >
                            <tr>
                                <td valign="top">
                                    <table width="600px">
                                        <tr>
                                            <td colspan="9">
                                                <asp:Label ID="Label34" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PERSONAL PARADOCENTE"
                                                    ></asp:Label>
                                                    <br />
                                                    <asp:Label ID="Label334" runat="server" CssClass="lblRojo" Font-Bold="True" Text="6. Registre la cantidad de personal paradocente por formaci�n, seg�n su situaci�n acad�mica y sexo."
                                                    ></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                
                                            </td>
                                            <td colspan="8">
                                                <asp:Label ID="Label261" runat="server" CssClass="lblRojo" Font-Bold="True" Text="L I C E N C I A T U R A"
                                                    ></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                            </td>
                                            <td colspan="2"  >
                                                <asp:Label ID="Label262" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TITULADOS"
                                                     ></asp:Label></td>
                                            <td colspan="2"  >
                                                <asp:Label ID="Label263" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NO TITULADOS"
                                                     ></asp:Label></td>
                                            <td colspan="2"  >
                                                <asp:Label ID="Label264" runat="server" CssClass="lblNegro" Font-Bold="True" Text="ESTUDIANTES"
                                                     ></asp:Label></td>
                                            <td colspan="2"  >
                                                <asp:Label ID="Label265" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                                                     ></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <asp:Label ID="Label266" runat="server" CssClass="lblRojo" Font-Bold="True" Text="FORMACI�N"
                                                     ></asp:Label></td>
                                            <td  >
                                                <asp:Label ID="Label267" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                                     ></asp:Label></td>
                                            <td  >
                                                <asp:Label ID="Label268" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                                     ></asp:Label></td>
                                            <td  >
                                                <asp:Label ID="Label269" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                                     ></asp:Label></td>
                                            <td  >
                                                <asp:Label ID="Label270" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                                     ></asp:Label></td>
                                            <td  >
                                                <asp:Label ID="Label271" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                                     ></asp:Label></td>
                                            <td  >
                                                <asp:Label ID="Label272" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                                     ></asp:Label></td>
                                            <td  >
                                                <asp:Label ID="Label273" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                                                     ></asp:Label></td>
                                            <td  >
                                                <asp:Label ID="Label274" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                                                     ></asp:Label></td>
                                        </tr>
                                        <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label275" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO DE LENGUAJE"
                         ></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV972" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV973" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV974" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV975" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10104"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV976" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10105"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV977" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10106"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV978" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10107"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV979" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10108"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label276" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERAPISTA F�SICO"
                         ></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV980" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV981" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV982" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV983" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10204"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV984" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10205"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV985" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10206"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV986" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10207"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV987" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10208"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label277" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PSIC�LOGO"
                         ></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV988" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV989" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV990" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV991" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10304"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV992" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10305"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV993" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10306"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV994" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10307"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV995" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10308"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label278" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TRABAJADOR SOCIAL"
                         ></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV996" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV997" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV998" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV999" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10404"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1000" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10405"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1001" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10406"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1002" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10407"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1003" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10408"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label279" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M�DICO"
                         ></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV1004" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1005" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1006" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1007" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10504"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1008" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10505"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1009" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10506"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1010" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10507"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1011" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10508"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label280" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO DE EDUCACI�N F�SICA"
                         ></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV1012" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1013" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1014" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1015" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10604"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1016" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10605"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1017" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10606"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1018" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10607"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1019" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10608"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label281" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MAESTRO ESPECIALISTA"
                         ></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV1020" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1021" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1022" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1023" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10704"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1024" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10705"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1025" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10706"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV1026" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10707"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1027" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10708"></asp:TextBox></td>
            </tr>
                                        <tr>
                    <td colspan="9"  >
                    <asp:Label ID="Label282" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Escriba la especialidad del maestro."
                         ></asp:Label>
                    <asp:TextBox ID="txtV1028" runat="server" Columns="50" MaxLength="30" CssClass="lblNegro" TabIndex="10801"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="9"  >
                    <asp:Label ID="Label335" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Si existe personal paradocente con formaci�n diferente de las especialidades, registre la cantidad de personal de acuerdo con su situaci�n acad�mica y sexo."
                         ></asp:Label></td>
                </tr>
                <tr>
                    <td  >
                    </td>
                    <td colspan="8">
                        <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="L I C E N C I A T U R A"
                             ></asp:Label></td>
                </tr>
                <tr>
                    <td  >
                    </td>
                    <td colspan="2">
                        <asp:Label ID="Label2" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TITULADOS"
                             ></asp:Label></td>
                    <td colspan="2">
                        <asp:Label ID="Label3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NO TITULADOS"
                             ></asp:Label></td>
                    <td colspan="2">
                        <asp:Label ID="Label4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="ESTUDIANTES"
                             ></asp:Label></td>
                    <td colspan="2">
                        <asp:Label ID="Label5" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                             ></asp:Label></td>
                </tr>
                <tr>
                    <td  >
                    </td>
                    <td>
                        <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                             ></asp:Label></td>
                    <td>
                        <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                             ></asp:Label></td>
                    <td>
                        <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                             ></asp:Label></td>
                    <td>
                        <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                             ></asp:Label></td>
                    <td>
                        <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                             ></asp:Label></td>
                    <td>
                        <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                             ></asp:Label></td>
                    <td>
                        <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                             ></asp:Label></td>
                    <td  >
                        <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                             ></asp:Label></td>
                </tr>
                <tr>
                    <td  >
                    <asp:TextBox ID="txtV1029" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1030" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1031" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20103"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1032" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20104"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1033" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20105"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1034" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20106"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1035" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="20107"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1036" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20108"></asp:TextBox></td>
                    <td  >
                    <asp:TextBox ID="txtV1037" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20109"></asp:TextBox></td>
                </tr>
                <tr>
                    <td  >
                    <asp:TextBox ID="txtV1038" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20201"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1039" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20202"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1040" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1041" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20204"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1042" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20205"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1043" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20206"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1044" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="20207"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1045" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20208"></asp:TextBox></td>
                    <td  >
                    <asp:TextBox ID="txtV1046" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20209"></asp:TextBox></td>
                </tr>
                <tr>
                    <td  >
                    <asp:TextBox ID="txtV1047" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1048" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1049" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1050" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20304"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1051" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20305"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1052" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20306"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1053" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="20307"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1054" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20308"></asp:TextBox></td>
                    <td  >
                    <asp:TextBox ID="txtV1055" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20309"></asp:TextBox></td>
                </tr>
                <tr>
                    <td  >
                    <asp:TextBox ID="txtV1056" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1057" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1058" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1059" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20404"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1060" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20405"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1061" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20406"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1062" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="20407"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1063" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20408"></asp:TextBox></td>
                    <td  >
                    <asp:TextBox ID="txtV1064" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20409"></asp:TextBox></td>
                </tr>
                <tr>
                    <td  >
                    <asp:TextBox ID="txtV1065" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1066" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1067" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20503"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1068" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20504"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1069" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20505"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1070" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20506"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1071" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="20507"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1072" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20508"></asp:TextBox></td>
                    <td  >
                    <asp:TextBox ID="txtV1073" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20509"></asp:TextBox></td>
                </tr>
                <tr>
                    <td  >
                    <asp:TextBox ID="txtV1074" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1075" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1076" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20603"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1077" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20604"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1078" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20605"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1079" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20606"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1080" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="20607"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1081" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20608"></asp:TextBox></td>
                    <td  >
                    <asp:TextBox ID="txtV1082" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20609"></asp:TextBox></td>
                </tr>
                <tr>
                    <td  >
                    <asp:TextBox ID="txtV1083" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1084" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20702"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1085" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20703"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1086" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20704"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1087" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20705"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1088" runat="server" Columns="2" MaxLength="2" Rows="2" CssClass="lblNegro" TabIndex="20706"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1089" runat="server" Columns="2" MaxLength="3" Rows="2" CssClass="lblNegro" TabIndex="20707"></asp:TextBox></td>
                    <td>
                    <asp:TextBox ID="txtV1090" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20708"></asp:TextBox></td>
                    <td  >
                    <asp:TextBox ID="txtV1091" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20709"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="9" style="text-align: right">
                    <asp:Label ID="Label296" runat="server" CssClass="lblGrisTit" Text="TOTAL (Suma de HOM y MUJ)"
                         ></asp:Label>
                    <asp:TextBox ID="txtV1092" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20801"></asp:TextBox>
                    </td>
                </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="400px">
                                        <tr>
                                            <td colspan="4">
                                        <asp:Label ID="Label35" runat="server" CssClass="lblRojo" Font-Bold="True" Text="PERSONAL ADMINISTRATIVO"
                                             ></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" >
                                        <asp:Label ID="Label336" runat="server" CssClass="lblRojo" Font-Bold="True" Text="7. Registre la cantidad de personal administrativo por sexo, seg�n la funci�n que desempe�e."
                                             ></asp:Label></td>
                                        </tr>
                                        <tr>
                <td>
                    <asp:Label ID="Label297" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                          Font-Bold="True"></asp:Label></td>
                <td  >
                    <asp:Label ID="Label298" runat="server" CssClass="lblGrisTit" Text="HOMBRES"  ></asp:Label></td>
                <td  >
                    <asp:Label ID="Label299" runat="server" CssClass="lblGrisTit" Text="MUJERES"  ></asp:Label></td>
                <td  >
                    <asp:Label ID="Label300" runat="server" CssClass="lblGrisTit" Text="TOTAL"  ></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label301" runat="server" CssClass="lblGrisTit" Text="NI�ERA"  ></asp:Label></td>
                <td  >
                    <asp:TextBox ID="txtV1093" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20901"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1094" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20902"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1095" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="20903"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label302" runat="server" CssClass="lblGrisTit" Text="AUXILIAR ADMINISTRATIVO"
                         ></asp:Label></td>
                <td  >
                    <asp:TextBox ID="txtV1096" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21001"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1097" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21002"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1098" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21003"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label303" runat="server" CssClass="lblGrisTit" Text="SECRETARIA"  ></asp:Label></td>
                <td  >
                    <asp:TextBox ID="txtV1099" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21101"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1100" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21102"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1101" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21103"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label304" runat="server" CssClass="lblGrisTit" Text="VIGILANTE"  ></asp:Label></td>
                <td  >
                    <asp:TextBox ID="txtV1102" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21201"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1103" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21202"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1104" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label305" runat="server" CssClass="lblGrisTit" Text="ASISTENTE DE SERVICIOS (INTENDENTE)"
                         ></asp:Label></td>
                <td  >
                    <asp:TextBox ID="txtV1105" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21301"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1106" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21302"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1107" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: justify"  >
                    <asp:Label ID="Label306" runat="server" CssClass="lblGrisTit" Text="CHOFER"  ></asp:Label></td>
                <td  >
                    <asp:TextBox ID="txtV1108" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21401"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1109" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21402"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1110" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21403"></asp:TextBox></td>
            </tr>
             <tr>
                <td colspan="4"  >
                <br />
                    <asp:Label ID="Label337" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Si existe personal administrativo que desempe�e labores diferentes de las que est�n se�aladas, describalo y registre su n�mero."
                         ></asp:Label></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1111" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21501"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1112" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21502"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1113" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21503"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1114" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21504"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1115" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21601"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1116" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21602"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1117" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21603"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1118" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21604"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1119" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21701"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1120" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21702"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1121" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21703"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1122" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21704"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1123" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21801"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1124" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21802"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1125" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21803"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1126" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21804"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1127" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="21901"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1128" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21902"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1129" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21903"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1130" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="21904"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1131" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="22001"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1132" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22002"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1133" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22003"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1134" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22004"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1135" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="22101"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1136" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22102"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1137" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22103"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1138" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22104"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1139" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="22201"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1140" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22202"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1141" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22203"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1142" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22204"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1143" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="22301"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1144" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22302"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1145" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22303"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1146" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22304"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    <asp:TextBox ID="txtV1147" runat="server" Columns="30" MaxLength="30" CssClass="lblNegro" TabIndex="22401"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1148" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22402"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1149" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22403"></asp:TextBox></td>
                <td  >
                    <asp:TextBox ID="txtV1150" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22404"></asp:TextBox></td>
            </tr>
            <tr>
                
                <td colspan="4" >
                    <asp:Label ID="Label307" runat="server" CssClass="lblGrisTit" Text="TOTAL"  ></asp:Label>
                    <asp:TextBox ID="txtV1151" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="22501"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4">
                    <table>
                        <tr>
                        <td  >
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label308" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL DE PERSONAL (Considere al personal reportado en los apartados 1, 5, 6 y 7.)"
                                     ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1152" runat="server" Columns="4" MaxLength="4" CssClass="lblNegro" TabIndex="22601"></asp:TextBox></td>
                        </tr>
                    </table>
                        </td>
                    </tr>
                    <tr>
                        <td  >
                    <asp:Label ID="Label340" runat="server" CssClass="lblRojo" Font-Bold="True" Text="8. Del total del personal que labora en el centro, registre la cantidad de personal con licencia limitada."
                         ></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: right" >
                    <asp:TextBox ID="txtV1153" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22701"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td  >
                    <asp:Label ID="Label342" runat="server" CssClass="lblRojo" Font-Bold="True" Text="9. Del total del personal que labora en el centro, registre la cantidad de personal comisionado."
                         ></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"  >
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label338" runat="server" CssClass="lblGrisTit" Text="INTERNO"  ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1154" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22801"></asp:TextBox></td>
                            <td nowrap="nowrap"  >
                            </td>
                            <td>
                                <asp:Label ID="Label339" runat="server" CssClass="lblGrisTit" Text="EXTERNO"  ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1155" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="22901"></asp:TextBox></td>
                        </tr>
                    </table>
                        </td>
                    </tr>
                    <tr>
                        <td  >
                    <asp:Label ID="Label341" runat="server" CssClass="lblRojo" Font-Bold="True" Text="10. Del total del personal que labora en el centro, registre la cantidad de personal becado."
                         ></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: right"  >
                    <asp:TextBox ID="txtV1156" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="23001"></asp:TextBox></td>
                    </tr>
                    </table>
                </td>
            </tr>
                                    </table>    
                                </td>
                            </tr>
                        </table>

        
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal1_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal1_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('CMYA_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('CMYA_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        <div style="width:900px"  id="divResultado" class="divResultado"  ></div>
        </div>
        
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer" ></td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 15;
                MaxRow = 41;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                  
                   
              
               
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
