<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Primaria y Ssecundaria" AutoEventWireup="true" CodeBehind="AG2_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.AG2_CAM_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  Font-Bold="True"
                    Font-Size="14px" Text="EDUCACION ESPECIAL"></asp:Label></td>
        </tr>
         <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table></div></div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span>INICIAL Y PREE</span></a></li>
        <li onclick="openPage('AG2_CAM_1',true)"><a href="#" title="" class="activo"><span>PRIM Y SEC</span></a></li>
        <li onclick="openPage('AG3_CAM_1',false)"><a href="#" title="" ><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL(CONT)</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAG Y AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
           
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


    <table style="width: 1000px">
        <tr>
            <td valign="top">
                <asp:Label ID="lblPRIMARIA" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EDUCACI�N PRIMARIA"
                    Width="100%"></asp:Label></td>
            <td valign="top">
                <asp:Label ID="lblSECUNDARIA" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EDUCACI�N SECUNDARIA"
                    Width="100%"></asp:Label></td>
        </tr>
            <tr>
                <td valign="top">
    <table>
        <tr>
            <td colspan="7" style="text-align: justify">
                <asp:Label ID="lblInstruccion3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Escriba el n�mero de alumnos inscritos, por sexo, y el n�mero de grupos de educaci�n primaria, seg�n los rubros que se indican."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="7" nowrap="nowrap" style="height: 20px; text-align: center">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblNuevoIngreso3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NUEVO INGRESO"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblReingreso3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="REINGRESO"
                    Width="100%"></asp:Label></td>
            <td rowspan="2" style="text-align: center">
                <asp:Label ID="lblTotal31" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td rowspan="2" style="text-align: center">
                <asp:Label ID="lblGrupos3" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: center">
            </td>
            <td style="text-align: center">
                <asp:Label ID="lblHom31" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMuj31" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHom32" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMuj32" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl31o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV85" runat="server" Columns="2" MaxLength="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV86" runat="server" Columns="2" MaxLength="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV87" runat="server" Columns="2" MaxLength="2" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV88" runat="server" Columns="2" MaxLength="2" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV89" runat="server" Columns="3" MaxLength="3" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV90" runat="server" Columns="2" MaxLength="2" TabIndex="10106" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl32o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV91" runat="server" Columns="2" MaxLength="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV92" runat="server" Columns="2" MaxLength="2" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV93" runat="server" Columns="2" MaxLength="2" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV94" runat="server" Columns="2" MaxLength="2" TabIndex="10204" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV95" runat="server" Columns="3" MaxLength="3" TabIndex="10205" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV96" runat="server" Columns="2" MaxLength="2" TabIndex="10206" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl33o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV97" runat="server" Columns="2" MaxLength="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV98" runat="server" Columns="2" MaxLength="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV99" runat="server" Columns="2" MaxLength="2" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV100" runat="server" Columns="2" MaxLength="2" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV101" runat="server" Columns="3" MaxLength="3" TabIndex="10305" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV102" runat="server" Columns="2" MaxLength="2" TabIndex="10306" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl34o" runat="server" CssClass="lblGrisTit" Text="4o." Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV103" runat="server" Columns="2" MaxLength="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV104" runat="server" Columns="2" MaxLength="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV105" runat="server" Columns="2" MaxLength="2" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV106" runat="server" Columns="2" MaxLength="2" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV107" runat="server" Columns="3" MaxLength="3" TabIndex="10405" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV108" runat="server" Columns="2" MaxLength="2" TabIndex="10406" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
         <tr>
            <td>
                <asp:Label ID="lbl35o" runat="server" CssClass="lblGrisTit" Text="5o." Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV109" runat="server" Columns="2" MaxLength="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV110" runat="server" Columns="2" MaxLength="2" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV111" runat="server" Columns="2" MaxLength="2" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV112" runat="server" Columns="2" MaxLength="2" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV113" runat="server" Columns="3" MaxLength="3" TabIndex="10505" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV114" runat="server" Columns="2" MaxLength="2" TabIndex="10506" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
         <tr>
            <td>
                <asp:Label ID="lbl36o" runat="server" CssClass="lblGrisTit" Text="6o." Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV115" runat="server" Columns="2" MaxLength="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV116" runat="server" Columns="2" MaxLength="2" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV117" runat="server" Columns="2" MaxLength="2" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV118" runat="server" Columns="2" MaxLength="2" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV119" runat="server" Columns="3" MaxLength="3" TabIndex="10605" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV120" runat="server" Columns="2" MaxLength="2" TabIndex="10606" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
         <tr>
            <td>
                <asp:Label ID="lblTotal32" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV121" runat="server" Columns="2" MaxLength="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV122" runat="server" Columns="2" MaxLength="2" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV123" runat="server" Columns="2" MaxLength="2" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV124" runat="server" Columns="2" MaxLength="2" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV125" runat="server" Columns="3" MaxLength="3" TabIndex="10705" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV126" runat="server" Columns="2" MaxLength="2" TabIndex="10706" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                </td>
                <td valign="top">
    <table>
        <tr>
            <td colspan="7" style="text-align: justify">
                <asp:Label ID="lblInstruccion4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Escriba el n�mero de alumnos inscritos, por sexo, y el n�mero de grupos de educaci�n secundaria, seg�n los rubros que se indican."
                    Width="450px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="7" nowrap="nowrap" style="height: 20px; text-align: center">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblNuevoIngreso4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="NUEVO INGRESO"
                    Width="100%"></asp:Label></td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblReingreso4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="REINGRESO"
                    Width="100%"></asp:Label></td>
            <td rowspan="2" style="text-align: center">
                <asp:Label ID="lblTotal41" runat="server" CssClass="lblNegro" Font-Bold="True" Text="TOTAL"
                    Width="100%"></asp:Label></td>
            <td rowspan="2" style="text-align: center">
                <asp:Label ID="lblGrupos4" runat="server" CssClass="lblNegro" Font-Bold="True" Text="GRUPOS"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: center">
            </td>
            <td style="text-align: center">
                <asp:Label ID="lblHom41" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMuj41" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHom42" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="HOM"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMuj42" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJ"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl41o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV154" runat="server" Columns="2" MaxLength="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV155" runat="server" Columns="2" MaxLength="2" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV156" runat="server" Columns="2" MaxLength="2" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV157" runat="server" Columns="2" MaxLength="2" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV158" runat="server" Columns="3" MaxLength="3" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV159" runat="server" Columns="2" MaxLength="2" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl42o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV160" runat="server" Columns="2" MaxLength="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV161" runat="server" Columns="2" MaxLength="2" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV162" runat="server" Columns="2" MaxLength="2" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV163" runat="server" Columns="2" MaxLength="2" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV164" runat="server" Columns="3" MaxLength="3" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV165" runat="server" Columns="2" MaxLength="2" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl43o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV166" runat="server" Columns="2" MaxLength="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV167" runat="server" Columns="2" MaxLength="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV168" runat="server" Columns="2" MaxLength="2" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV169" runat="server" Columns="2" MaxLength="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV170" runat="server" Columns="3" MaxLength="3" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV171" runat="server" Columns="2" MaxLength="2" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTotal42" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV172" runat="server" Columns="2" MaxLength="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV173" runat="server" Columns="2" MaxLength="2" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV174" runat="server" Columns="2" MaxLength="2" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV175" runat="server" Columns="2" MaxLength="2" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV176" runat="server" Columns="3" MaxLength="3" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV177" runat="server" Columns="2" MaxLength="2" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                </td>
            </tr>
            <tr>
                <td>
    <table>
        <tr>
            <td colspan="4" style="text-align: justify">
                <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba la cantidad de alumnos con 
                discapacidad, aptitudes sobresalientes u otras condiciones, 
                seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label>
                    </td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblSituacion3" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal33" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblCeguera3" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV127" runat="server" Columns="3" MaxLength="3" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV128" runat="server" Columns="3" MaxLength="3" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV129" runat="server" Columns="4" MaxLength="4" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblVisual3" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV130" runat="server" Columns="3" MaxLength="3" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV131" runat="server" Columns="3" MaxLength="3" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV132" runat="server" Columns="4" MaxLength="4" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblSordera3" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV133" runat="server" Columns="3" MaxLength="3" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV134" runat="server" Columns="3" MaxLength="3" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV135" runat="server" Columns="4" MaxLength="4" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblAuditiva3" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV136" runat="server" Columns="3" MaxLength="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV137" runat="server" Columns="3" MaxLength="3" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV138" runat="server" Columns="4" MaxLength="4" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblMotriz3" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV139" runat="server" Columns="3" MaxLength="3" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV140" runat="server" Columns="3" MaxLength="3" TabIndex="11202" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV141" runat="server" Columns="4" MaxLength="4" TabIndex="11203" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblIntelectual3" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV142" runat="server" Columns="3" MaxLength="3" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV143" runat="server" Columns="3" MaxLength="3" TabIndex="11302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV144" runat="server" Columns="4" MaxLength="4" TabIndex="11303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblSobresalientes3" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV145" runat="server" Columns="3" MaxLength="3" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV146" runat="server" Columns="3" MaxLength="3" TabIndex="11402" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV147" runat="server" Columns="4" MaxLength="4" TabIndex="11403" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblOtros3" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV148" runat="server" Columns="3" MaxLength="3" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV149" runat="server" Columns="3" MaxLength="3" TabIndex="11502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV150" runat="server" Columns="4" MaxLength="4" TabIndex="11503" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTotal34" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV151" runat="server" Columns="3" MaxLength="3" TabIndex="11601" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV152" runat="server" Columns="3" MaxLength="3" TabIndex="11602" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV153" runat="server" Columns="4" MaxLength="4" TabIndex="11603" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                </td>
                <td>
    <table>
        <tr>
            <td colspan="4" style="text-align:justify">
                <asp:Label ID="lblInstruccion2b" runat="server" CssClass="lblRojo" Text="De los alumnos reportados en el rubro anterior, escriba la cantidad de alumnos con 
                discapacidad, aptitudes sobresalientes u otras condiciones, 
                seg�n las definiciones establecidas en el glosario."
                    Width="450px"></asp:Label>
                    
                    </td>
        </tr>
        <tr>
            <td colspan="4" nowrap="nowrap" style="height: 20px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center">
                <asp:Label ID="lblSituacion4" runat="server" CssClass="lblNegro" Text="SITUACI�N DEL ALUMNO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal43" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblCeguera4" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV178" runat="server" Columns="3" MaxLength="3" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV179" runat="server" Columns="3" MaxLength="3" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV180" runat="server" Columns="4" MaxLength="4" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblVisual4" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV181" runat="server" Columns="3" MaxLength="3" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV182" runat="server" Columns="3" MaxLength="3" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV183" runat="server" Columns="4" MaxLength="4" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblSordera4" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV184" runat="server" Columns="3" MaxLength="3" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV185" runat="server" Columns="3" MaxLength="3" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV186" runat="server" Columns="4" MaxLength="4" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblAuditiva4" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV187" runat="server" Columns="3" MaxLength="3" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV188" runat="server" Columns="3" MaxLength="3" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV189" runat="server" Columns="4" MaxLength="4" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblMotriz4" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV190" runat="server" Columns="3" MaxLength="3" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV191" runat="server" Columns="3" MaxLength="3" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV192" runat="server" Columns="4" MaxLength="4" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblIntelectual4" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV193" runat="server" Columns="3" MaxLength="3" TabIndex="21001" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV194" runat="server" Columns="3" MaxLength="3" TabIndex="21002" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV195" runat="server" Columns="4" MaxLength="4" TabIndex="21003" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblSobresalientes4" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV196" runat="server" Columns="3" MaxLength="3" TabIndex="21101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV197" runat="server" Columns="3" MaxLength="3" TabIndex="21102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV198" runat="server" Columns="4" MaxLength="4" TabIndex="21103" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align:justify">
                <asp:Label ID="lblOtros4" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV199" runat="server" Columns="3" MaxLength="3" TabIndex="21201" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV200" runat="server" Columns="3" MaxLength="3" TabIndex="21202" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV201" runat="server" Columns="4" MaxLength="4" TabIndex="21203" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTotal44" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV202" runat="server" Columns="3" MaxLength="3" TabIndex="21301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV203" runat="server" Columns="3" MaxLength="3" TabIndex="21302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV204" runat="server" Columns="4" MaxLength="4" TabIndex="21303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('AG3_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AG3_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 20;
                MaxRow = 20;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
