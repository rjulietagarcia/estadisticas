<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Carrera Magisterial y Aulas" AutoEventWireup="true" CodeBehind="CMYA_CAM_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.CAM_1.CMYA_CAM_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    <div id="logo"></div>
    <div style="min-width:1550px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  Font-Bold="True"
                    Font-Size="14px" Text="EDUCACI�N ESPECIAL"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table></div></div>
    <div id="menu" style="min-width:1550px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_CAM_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_CAM_1',true)"><a href="#" title=""><span>INICIAL Y PREE</span></a></li>
        <li onclick="openPage('AG2_CAM_1',true)"><a href="#" title=""><span>PRIM Y SEC</span></a></li>
        <li onclick="openPage('AG3_CAM_1',true)"><a href="#" title=""><span>CAP P/TRAB Y ATENCI�N COMPL</span></a></li>
        <li onclick="openPage('Desgolse_CAM_1',true)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal1_CAM_1',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('Personal2_CAM_1',true)"><a href="#" title=""><span>PERSONAL(CONT)</span></a></li>
        <li onclick="openPage('CMYA_CAM_1',true)"><a href="#" title="" class="activo"><span>CARRERA MAG Y AULAS</span></a></li>
        <li onclick="openPage('Gasto_CAM_1',false)"><a href="#" title="" ><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


    <div>
        &nbsp;&nbsp;
        <table style="width: 800px">
            <tr>
                <td style="width: 400px">
                    <asp:Label ID="lblCarrera" runat="server" CssClass="lblRojo" Font-Size="16px" Text="III. CARRERA MAGISTERIAL" Width="100%"></asp:Label></td>
                <td nowrap="nowrap" style="width: 50px">
                </td>
                <td>
                    <asp:Label ID="Label344" runat="server" CssClass="lblRojo" Font-Size="16px" Text="IV. AULAS"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 400px; text-align: justify">
                    <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de profesores que se encuentran en el programa de carrera magisterial"
                        Width="100%"></asp:Label></td>
                <td nowrap="nowrap" style="width: 50px; text-align: justify">
                </td>
                <td style="text-align: justify">
                    <asp:Label ID="Label343" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de aulas, cub�culos y talleres existentes y en uso."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 400px; text-align: right; height: 26px;">
                    <asp:TextBox ID="txtV1157" runat="server" Columns="3" MaxLength="3" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                <td nowrap="nowrap" style="width: 50px; height: 26px;">
                </td>
                <td style="height: 26px">
                </td>
            </tr>
            <tr>
                <td style="width: 400px; text-align: justify">
                    <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Desglose la cantidad anotada en el inciso anterior, seg�n la vertiente y el nivel en que se encuentran los profesores."
                        Width="100%"></asp:Label></td>
                <td nowrap="nowrap" style="width: 50px">
                </td>
                <td rowspan="2" style="text-align: center" valign="top">
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="Label313" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EXISTENTES"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:Label ID="Label314" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EN USO"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label310" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="AULAS"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV1176" runat="server" Columns="2" MaxLength="2" TabIndex="12001" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV1177" runat="server" Columns="2" MaxLength="2" TabIndex="12002" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label311" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUB�CULOS"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV1178" runat="server" Columns="2" MaxLength="2" TabIndex="12101" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV1179" runat="server" Columns="2" MaxLength="2" TabIndex="12102" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label312" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TALLERES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV1180" runat="server" Columns="2" MaxLength="2" TabIndex="12201" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV1181" runat="server" Columns="2" MaxLength="2" TabIndex="12202" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 400px; text-align: center">
                    <table>
                        <tr>
                            <td rowspan="6" style="width: 176px; text-align: left" valign="top">
                                <asp:Label ID="lbl1aVertiente" runat="server" CssClass="lblNegro" Height="21px" Text="1a. VERTIENTE"
                                    Width="100%" Font-Bold="True"></asp:Label>
                                <asp:Label ID="lbl1aVertiente2" runat="server" CssClass="lblNegro" Height="21px" Text="(Profesores frente a grupo)"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelA1" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1158" runat="server" Columns="3" MaxLength="3" TabIndex="10201" CssClass="lblNegro"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelB1" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1159" runat="server" Columns="3" MaxLength="3" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelBC1" runat="server" CssClass="lblGrisTit" Text="Nivel BC"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1160" runat="server" Columns="3" MaxLength="3" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelC1" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1161" runat="server" Columns="3" MaxLength="3" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelD1" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1162" runat="server" Columns="3" MaxLength="3" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelE1" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1163" runat="server" Columns="3" MaxLength="3" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 176px; height: 25px; text-align: left" valign="top">
                            </td>
                            <td nowrap="nowrap" style="height: 25px">
                            </td>
                            <td style="height: 25px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="6" style="width: 176px; text-align: left" valign="top">
                                <asp:Label ID="lbl2aVertiente" runat="server" CssClass="lblNegro" Height="21px" Text="2a. VERTIENTE"
                                    Width="100%" Font-Bold="True"></asp:Label>
                                <asp:Label ID="lbl2aVertiente2" runat="server" CssClass="lblNegro" Height="21px" Text="(Docentes en funciones directivas y de supervisi�n)"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelA2" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1164" runat="server" Columns="3" MaxLength="3" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelB2" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1165" runat="server" Columns="3" MaxLength="3" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelBC2" runat="server" CssClass="lblGrisTit" Text="Nivel BC"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1166" runat="server" Columns="3" MaxLength="3" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelC2" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1167" runat="server" Columns="3" MaxLength="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelD2" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1168" runat="server" Columns="3" MaxLength="3" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelE2" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1169" runat="server" Columns="3" MaxLength="3" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 176px; height: 25px; text-align: left" valign="top">
                            </td>
                            <td nowrap="nowrap" style="height: 25px">
                            </td>
                            <td style="height: 25px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="6" style="width: 176px; text-align: left" valign="top">
                                <asp:Label ID="lbl3aVertiente" runat="server" CssClass="lblNegro" Height="21px" Text="3a. VERTIENTE"
                                    Width="100%" Font-Bold="True"></asp:Label>
                                <asp:Label ID="lbl3aVertiente2" runat="server" CssClass="lblNegro" Height="21px" Text="(Docentes en actividades t�cno-pedag�gicas)"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelA3" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1170" runat="server" Columns="3" MaxLength="3" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelB3" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1171" runat="server" Columns="3" MaxLength="3" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelBC3" runat="server" CssClass="lblGrisTit" Text="Nivel BC"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1172" runat="server" Columns="3" MaxLength="3" TabIndex="11601" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelC3" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1173" runat="server" Columns="3" MaxLength="3" TabIndex="11701" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelD3" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1174" runat="server" Columns="3" MaxLength="3" TabIndex="11801" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNivelE3" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV1175" runat="server" Columns="3" MaxLength="3" TabIndex="11901" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td nowrap="nowrap" style="width: 50px; text-align: center" valign="top">
                </td>
            </tr>
        </table>
    
    </div>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal2_CAM_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal2_CAM_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Gasto_CAM_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Gasto_CAM_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 3;
                MaxRow = 23;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
