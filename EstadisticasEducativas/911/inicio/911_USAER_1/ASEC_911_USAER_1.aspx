<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="USAER-1(Alumnos Secundaria)" AutoEventWireup="true" CodeBehind="ASEC_911_USAER_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_USAER_1.ASEC_911_USAER_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
  
<div id="logo"></div>
    <div style="min-width:1380px; height:65px;">
    <div id="header">
    
    <table style="width:100%">
       <tr><td><span>EDUCACI�N ESPECIAL</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    
    </div></div>
    <div id="menu" style="min-width:1380px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_USAER_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AE_911_USAER_1',true)"><a href="#" title=""><span>ALUMNOS POR CT</span></a></li>
        <li onclick="openPage('AINI_911_USAER_1',true)"><a href="#" title=""><span>INICIAL</span></a></li>
        <li onclick="openPage('APRE_911_USAER_1',true)"><a href="#" title=""><span>PREESCOLAR</span></a></li>
        <li onclick="openPage('APRIM_911_USAER_1',true)"><a href="#" title=""><span>PRIMARIA</span></a></li>
        <li onclick="openPage('ASEC_911_USAER_1',true)"><a href="#" title="" class="activo"><span>SECUNDARIA</span></a></li>
        <li onclick="openPage('AGD_911_USAER_1',false)"><a href="#" title=""><span>DESGLOSE</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        
        <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td>
                    <table id="TABLE1" style="text-align: center;">
                        <tr>
                            <td colspan="16" rowspan="1" style="text-align: left; padding-bottom: 10px;">
                                <asp:Label ID="lbl5" runat="server" CssClass="lblRojo" Text="5. Escriba la poblaci�n total con necesidades educativas especiales atendida en secundaria, por escuela, grado y sexo."
                                    Width="750px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="10" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblSecundaria" runat="server" CssClass="lblRojo" Text="SECUNDARIA" Width="500px"></asp:Label>
                                </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td colspan="1" rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td colspan="6" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPobGrado" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL POR GRADO"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" rowspan="1" style="padding-bottom: 10px; text-align: center">
                                <asp:Label ID="lblPoblacion" runat="server" CssClass="lblRojo" Text="POBLACI�N TOTAL (SUMA)"
                                    Width="200px"></asp:Label></td>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                                </td>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                            <td rowspan="1" style="padding-bottom: 10px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="" style="text-align: center; height: 3px; width: 67px;">
                                <asp:Label ID="lblEscuela" runat="server" CssClass="lblRojo" Text="ESCUELA" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="2">
                                <asp:Label ID="lblPrimero" runat="server" CssClass="lblRojo" Text="1�" Width="67px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblSegundo" runat="server" CssClass="lblRojo" Text="2�" Width="67px"></asp:Label></td>
                            <td style="text-align: center" colspan="2">
                                <asp:Label ID="lblTercero" runat="server" CssClass="lblRojo" Text="3�" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;" colspan="3">
                                &nbsp;</td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES"
                                    Width="67px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMujeres1" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center;">
                                &nbsp;<asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;">
                                <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px; height: 26px;">
                                <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresTot" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujresTot" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal51" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; text-align: center">
                                </td>
                            <td style="width: 67px; text-align: center">
                                </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                            <td style="width: 67px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc1" runat="server" CssClass="lblGrisTit" Text="1" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV726" runat="server" Columns="2" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV727" runat="server" Columns="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV728" runat="server" Columns="2" TabIndex="10103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV729" runat="server" Columns="2" TabIndex="10104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV730" runat="server" Columns="2" TabIndex="10105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV731" runat="server" Columns="2" TabIndex="10106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV732" runat="server" Columns="3" TabIndex="10107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV733" runat="server" Columns="3" TabIndex="10108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV734" runat="server" Columns="3" TabIndex="10109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:Label ID="lblEsc2" runat="server" CssClass="lblGrisTit" Text="2" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV735" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV736" runat="server" Columns="2" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV737" runat="server" Columns="2" TabIndex="10203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV738" runat="server" Columns="2" TabIndex="10204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV739" runat="server" Columns="2" TabIndex="10205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV740" runat="server" Columns="2" TabIndex="10206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV741" runat="server" Columns="3" TabIndex="10207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV742" runat="server" Columns="3" TabIndex="10208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV743" runat="server" Columns="3" TabIndex="10209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                            <asp:Label ID="lblEsc3" runat="server" CssClass="lblGrisTit" Text="3" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV744" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV745" runat="server" Columns="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV746" runat="server" Columns="2" TabIndex="10303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV747" runat="server" Columns="2" TabIndex="10304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV748" runat="server" Columns="2" TabIndex="10305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV749" runat="server" Columns="2" TabIndex="10306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV750" runat="server" Columns="3" TabIndex="10307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV751" runat="server" Columns="3" TabIndex="10308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV752" runat="server" Columns="3" TabIndex="10309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                            <asp:Label ID="lblEsc4" runat="server" CssClass="lblGrisTit" Text="4" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV753" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV754" runat="server" Columns="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV755" runat="server" Columns="2" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV756" runat="server" Columns="2" TabIndex="10404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV757" runat="server" Columns="2" TabIndex="10405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV758" runat="server" Columns="2" TabIndex="10406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV759" runat="server" Columns="3" TabIndex="10407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV760" runat="server" Columns="3" TabIndex="10408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV761" runat="server" Columns="3" TabIndex="10409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc5" runat="server" CssClass="lblGrisTit" Text="5" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV762" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV763" runat="server" Columns="2" TabIndex="10502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV764" runat="server" Columns="2" TabIndex="10503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV765" runat="server" Columns="2" TabIndex="10504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV766" runat="server" Columns="2" TabIndex="10505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV767" runat="server" Columns="2" TabIndex="10506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV768" runat="server" Columns="3" TabIndex="10507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV769" runat="server" Columns="3" TabIndex="10508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV770" runat="server" Columns="3" TabIndex="10509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc6" runat="server" CssClass="lblGrisTit" Text="6" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV771" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV772" runat="server" Columns="2" TabIndex="10602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV773" runat="server" Columns="2" TabIndex="10603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV774" runat="server" Columns="2" TabIndex="10604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV775" runat="server" Columns="2" TabIndex="10605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV776" runat="server" Columns="2" TabIndex="10606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV777" runat="server" Columns="3" TabIndex="10607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV778" runat="server" Columns="3" TabIndex="10608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV779" runat="server" Columns="3" TabIndex="10609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc7" runat="server" CssClass="lblGrisTit" Text="7" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV780" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV781" runat="server" Columns="2" TabIndex="10702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV782" runat="server" Columns="2" TabIndex="10703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV783" runat="server" Columns="2" TabIndex="10704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV784" runat="server" Columns="2" TabIndex="10705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV785" runat="server" Columns="2" TabIndex="10706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV786" runat="server" Columns="3" TabIndex="10707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV787" runat="server" Columns="3" TabIndex="10708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV788" runat="server" Columns="3" TabIndex="10709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc8" runat="server" CssClass="lblGrisTit" Text="8" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV789" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV790" runat="server" Columns="2" TabIndex="10802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV791" runat="server" Columns="2" TabIndex="10803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV792" runat="server" Columns="2" TabIndex="10804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV793" runat="server" Columns="2" TabIndex="10805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV794" runat="server" Columns="2" TabIndex="10806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV795" runat="server" Columns="3" TabIndex="10807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV796" runat="server" Columns="3" TabIndex="10808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV797" runat="server" Columns="3" TabIndex="10809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblEsc9" runat="server" CssClass="lblGrisTit" Text="9" Width="20px"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV798" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV799" runat="server" Columns="2" TabIndex="10902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center;">
                                <asp:TextBox ID="txtV800" runat="server" Columns="2" TabIndex="10903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV801" runat="server" Columns="2" TabIndex="10904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV802" runat="server" Columns="2" TabIndex="10905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV803" runat="server" Columns="2" TabIndex="10906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV804" runat="server" Columns="3" TabIndex="10907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV805" runat="server" Columns="3" TabIndex="10908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV806" runat="server" Columns="3" TabIndex="10909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center; height: 26px;">
                                <asp:Label ID="lblEsc10" runat="server" CssClass="lblGrisTit" Text="10" Width="20px"></asp:Label></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV807" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV808" runat="server" Columns="2" TabIndex="11002" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV809" runat="server" Columns="2" TabIndex="11003" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV810" runat="server" Columns="2" TabIndex="11004" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV811" runat="server" Columns="2" TabIndex="11005" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV812" runat="server" Columns="2" TabIndex="11006" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV813" runat="server" Columns="3" TabIndex="11007" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV814" runat="server" Columns="3" TabIndex="11008" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV815" runat="server" Columns="3" TabIndex="11009" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="text-align: center;">
                                <asp:Label ID="lblTotal52" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV816" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV817" runat="server" Columns="3" TabIndex="11102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV818" runat="server" Columns="3" TabIndex="11103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV819" runat="server" Columns="3" TabIndex="11104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                <asp:TextBox ID="txtV820" runat="server" Columns="3" TabIndex="11105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV821" runat="server" Columns="3" TabIndex="11106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV822" runat="server" Columns="4" TabIndex="11107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV823" runat="server" Columns="4" TabIndex="11108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV824" runat="server" Columns="4" TabIndex="11109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px; text-align: center; width: 67px;">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" 
                                Text="De los alumnos reportados en el rubro anterior, 
                                escriba la cantidad de alumnos 
                                con discapacidad, aptitudes sobresalientes u otras condiciones, 
                                seg�n las definiciones establecidas en el glosario."
                                    Width="1136px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: center">
                                <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNOS"
                                    Width="200px"></asp:Label></td>
                            <td colspan="3" style="text-align: center">
                                <asp:Label ID="lblAlumnos" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="200px"></asp:Label></td>
                            <td colspan="10" style="text-align: center">
                                &nbsp;<asp:Label ID="lblAlumnosEsc" runat="server" CssClass="lblRojo" Text="ALUMNOS POR ESCUELA"
                                    Width="200px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 26px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblHombresAl" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblMujeresAl" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblTotal53" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc12" runat="server" CssClass="lblGrisTit" Text="1" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc22" runat="server" CssClass="lblGrisTit" Text="2" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc32" runat="server" CssClass="lblGrisTit" Text="3" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc42" runat="server" CssClass="lblGrisTit" Text="4" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc52" runat="server" CssClass="lblGrisTit" Text="5" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc62" runat="server" CssClass="lblGrisTit" Text="6" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc72" runat="server" CssClass="lblGrisTit" Text="7" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc82" runat="server" CssClass="lblGrisTit" Text="8" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc92" runat="server" CssClass="lblGrisTit" Text="9" Width="67px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:Label ID="lblEsc102" runat="server" CssClass="lblGrisTit" Text="10" Width="67px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left; height: 26px;">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV825" runat="server" Columns="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV826" runat="server" Columns="2" TabIndex="20102" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV827" runat="server" Columns="3" TabIndex="20103" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV828" runat="server" Columns="2" TabIndex="20104" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV829" runat="server" Columns="2" TabIndex="20105" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV830" runat="server" Columns="2" TabIndex="20106" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV831" runat="server" Columns="2" TabIndex="20107" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV832" runat="server" Columns="2" TabIndex="20108" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV833" runat="server" Columns="2" TabIndex="20109" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV834" runat="server" Columns="2" TabIndex="20110" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV835" runat="server" Columns="2" TabIndex="20111" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV836" runat="server" Columns="2" TabIndex="20112" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV837" runat="server" Columns="2" TabIndex="20113" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV838" runat="server" Columns="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV839" runat="server" Columns="2" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV840" runat="server" Columns="3" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV841" runat="server" Columns="2" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV842" runat="server" Columns="2" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV843" runat="server" Columns="2" TabIndex="20206" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV844" runat="server" Columns="2" TabIndex="20207" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV845" runat="server" Columns="2" TabIndex="20208" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV846" runat="server" Columns="2" TabIndex="20209" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV847" runat="server" Columns="2" TabIndex="20210" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV848" runat="server" Columns="2" TabIndex="20211" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV849" runat="server" Columns="2" TabIndex="20212" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV850" runat="server" Columns="2" TabIndex="20213" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV851" runat="server" Columns="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV852" runat="server" Columns="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV853" runat="server" Columns="3" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV854" runat="server" Columns="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV855" runat="server" Columns="2" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV856" runat="server" Columns="2" TabIndex="20306" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV857" runat="server" Columns="2" TabIndex="20307" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV858" runat="server" Columns="2" TabIndex="20308" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV859" runat="server" Columns="2" TabIndex="20309" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV860" runat="server" Columns="2" TabIndex="20310" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV861" runat="server" Columns="2" TabIndex="20311" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV862" runat="server" Columns="2" TabIndex="20312" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV863" runat="server" Columns="2" TabIndex="20313" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV864" runat="server" Columns="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV865" runat="server" Columns="2" TabIndex="20402" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV866" runat="server" Columns="3" TabIndex="20403" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV867" runat="server" Columns="2" TabIndex="20404" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV868" runat="server" Columns="2" TabIndex="20405" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV869" runat="server" Columns="2" TabIndex="20406" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV870" runat="server" Columns="2" TabIndex="20407" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV871" runat="server" Columns="2" TabIndex="20408" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV872" runat="server" Columns="2" TabIndex="20409" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV873" runat="server" Columns="2" TabIndex="20410" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV874" runat="server" Columns="2" TabIndex="20411" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV875" runat="server" Columns="2" TabIndex="20412" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV876" runat="server" Columns="2" TabIndex="20413" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV877" runat="server" Columns="2" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV878" runat="server" Columns="2" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV879" runat="server" Columns="3" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV880" runat="server" Columns="2" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV881" runat="server" Columns="2" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV882" runat="server" Columns="2" TabIndex="20506" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV883" runat="server" Columns="2" TabIndex="20507" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV884" runat="server" Columns="2" TabIndex="20508" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV885" runat="server" Columns="2" TabIndex="20509" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV886" runat="server" Columns="2" TabIndex="20510" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV887" runat="server" Columns="2" TabIndex="20511" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV888" runat="server" Columns="2" TabIndex="20512" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV889" runat="server" Columns="2" TabIndex="20513" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV890" runat="server" Columns="2" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV891" runat="server" Columns="2" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV892" runat="server" Columns="3" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV893" runat="server" Columns="2" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV894" runat="server" Columns="2" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV895" runat="server" Columns="2" TabIndex="20606" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV896" runat="server" Columns="2" TabIndex="20607" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV897" runat="server" Columns="2" TabIndex="20608" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV898" runat="server" Columns="2" TabIndex="20609" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV899" runat="server" Columns="2" TabIndex="20610" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV900" runat="server" Columns="2" TabIndex="20611" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV901" runat="server" Columns="2" TabIndex="20612" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV902" runat="server" Columns="2" TabIndex="20613" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                                    Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV903" runat="server" Columns="2" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV904" runat="server" Columns="2" TabIndex="20702" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV905" runat="server" Columns="3" TabIndex="20703" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV906" runat="server" Columns="2" TabIndex="20704" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV907" runat="server" Columns="2" TabIndex="20705" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV908" runat="server" Columns="2" TabIndex="20706" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV909" runat="server" Columns="2" TabIndex="20707" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV910" runat="server" Columns="2" TabIndex="20708" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV911" runat="server" Columns="2" TabIndex="20709" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV912" runat="server" Columns="2" TabIndex="20710" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV913" runat="server" Columns="2" TabIndex="20711" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV914" runat="server" Columns="2" TabIndex="20712" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV915" runat="server" Columns="2" TabIndex="20713" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV916" runat="server" Columns="2" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV917" runat="server" Columns="2" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV918" runat="server" Columns="3" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV919" runat="server" Columns="2" TabIndex="20804" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV920" runat="server" Columns="2" TabIndex="20805" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV921" runat="server" Columns="2" TabIndex="20806" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV922" runat="server" Columns="2" TabIndex="20807" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV923" runat="server" Columns="2" TabIndex="20808" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV924" runat="server" Columns="2" TabIndex="20809" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV925" runat="server" Columns="2" TabIndex="20810" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV926" runat="server" Columns="2" TabIndex="20811" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV927" runat="server" Columns="2" TabIndex="20812" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV928" runat="server" Columns="2" TabIndex="20813" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="1" style="text-align: left">
                                <asp:Label ID="lblTotal54" runat="server" CssClass="lblRojo" Text="TOTAL" Width="200px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV929" runat="server" Columns="3" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV930" runat="server" Columns="3" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV931" runat="server" Columns="3" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV932" runat="server" Columns="3" TabIndex="20904" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV933" runat="server" Columns="3" TabIndex="20905" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV934" runat="server" Columns="3" TabIndex="20906" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV935" runat="server" Columns="3" TabIndex="20907" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV936" runat="server" Columns="3" TabIndex="20908" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV937" runat="server" Columns="3" TabIndex="20909" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV938" runat="server" Columns="3" TabIndex="20910" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV939" runat="server" Columns="3" TabIndex="20911" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV940" runat="server" Columns="3" TabIndex="20912" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV941" runat="server" Columns="3" TabIndex="20913" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="1" style="width: 67px; height: 14px; text-align: left">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                            <td style="width: 67px; height: 14px; text-align: center">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('APRIM_911_USAER_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('APRIM_911_USAER_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('AGD_911_USAER_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('AGD_911_USAER_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />   
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          MaxCol = 17;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
         GetTabIndexes();
        </script> 
             
</asp:Content>
