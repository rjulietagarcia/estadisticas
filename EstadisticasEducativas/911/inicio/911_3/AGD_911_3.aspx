<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.3(Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_3.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_3.AGD_911_3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
          
  <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header"> 
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="EDUCACI�N PRIMARIA"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div>
    </div>
    <div id="menu" style="min-width:1400px; width:expression(document.body.clientWidth < 1401? '1400px': 'auto' );">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_3',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('AG1_911_3',true)"><a href="#" title=""><span>1�, 2� y 3�</span></a></li>
        <li onclick="openPage('AG4_911_3',true)"><a href="#" title=""><span>4�, 5� y 6�</span></a></li>
        <li onclick="openPage('AGT_911_3',true)"><a href="#" title=""><span>TOTAL</span></a></li>
        <li onclick="openPage('AGD_911_3',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li>
        <li onclick="openPage('Personal_911_3',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
    <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center> 
    
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 100%; text-align: center">
            <tr>
                <td style="width: 100%; text-align: center">
                    <table id="TABLE1" style="text-align: center">
                        <tr>
                            <td colspan="5" rowspan="1" style="vertical-align: top; text-align: center">
                                <table style="width: 455px; text-align: center">
                                    <tr>
                                        <td colspan="6" style="padding-bottom: 10px; width: 3px; text-align: justify">
                                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Del total de alumnos reportados en primer grado, registre la cantidad de los que cursaron educaci�n preescolar, seg�n los a�os cursados y tipo de ingreso, desglos�ndola por sexo." Width="494px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="padding-bottom: 10px; width: 150px; text-align: center">
                                        </td>
                                        <td style="padding-bottom: 10px; width: 67px; text-align: center">
                                        </td>
                                        <td style="padding-bottom: 10px; width: 67px; text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblHombres2" runat="server" CssClass="lblRojo" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td style="padding-bottom: 10px; width: 67px; text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblMujeres2" runat="server" CssClass="lblRojo" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td style="padding-bottom: 10px; width: 67px; text-align: center" class="linaBajoAlto Orila">
                                            <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                            <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 150px;" rowspan="2" class="linaBajoAlto">
                                            <asp:Label ID="lblUnA�o" runat="server" CssClass="lblRojo" Text="UN A�O CURSADO"
                                                Width="150px"></asp:Label></td>
                                        <td style="width: 67px; text-align: left" class="linaBajoAlto">
                                            <asp:Label ID="lblNvoIngreso1A" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV349" runat="server" Columns="3" TabIndex="10101" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV350" runat="server" Columns="3" TabIndex="10102" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV351" runat="server" Columns="3" TabIndex="10103" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblRepetidores1A" runat="server" CssClass="lblGrisTit" Text="REPETIDORES" Width="100px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV352" runat="server" Columns="3" TabIndex="10201" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV353" runat="server" Columns="3" TabIndex="10202" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV354" runat="server" Columns="3" TabIndex="10203" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="vertical-align: middle; text-align: left; width: 150px;" class="linaBajo">
                                            <asp:Label ID="lblDosA�os" runat="server" CssClass="lblRojo" Text="DOS A�OS CURSADOS"
                                                Width="150px"></asp:Label>
                                            </td>
                                        <td style="width: 67px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblNvoIngreso2A" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV355" runat="server" Columns="3" TabIndex="10301" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV356" runat="server" Columns="3" TabIndex="10302" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV357" runat="server" Columns="3" TabIndex="10303" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblRepetidores2A" runat="server" CssClass="lblGrisTit" Text="REPETIDORES" Width="100px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV358" runat="server" Columns="3" TabIndex="10401" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV359" runat="server" Columns="3" TabIndex="10402" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV360" runat="server" Columns="3" TabIndex="10403" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="vertical-align: middle; text-align: left; width: 150px;" class="linaBajo">
                                            <asp:Label ID="lblTresA�os" runat="server" CssClass="lblRojo" Text="TRES A�OS CURSADOS"
                                                Width="150px"></asp:Label>
                                            </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblNvoIngreso3A" runat="server" CssClass="lblGrisTit" Text="NUEVO INGRESO" Width="100px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV361" runat="server" Columns="3" TabIndex="10501" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV362" runat="server" Columns="3" TabIndex="10502" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV363" runat="server" Columns="3" TabIndex="10503" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblRepetidores3A" runat="server" CssClass="lblGrisTit" Text="REPETIDORES" Width="100px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV364" runat="server" Columns="3" TabIndex="10601" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV365" runat="server" Columns="3" TabIndex="10602" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV366" runat="server" Columns="3" TabIndex="10603" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="vertical-align: middle; width: 150px; text-align: right">
                                        </td>
                                        <td colspan="" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblTotal22" runat="server" CssClass="lblRojo" Text="TOTAL" Width="100px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV367" runat="server" Columns="3" TabIndex="10701" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV368" runat="server" Columns="3" TabIndex="10702" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV369" runat="server" Columns="3" TabIndex="10703" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                            <br />
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: center">
                                            <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: center">
                                            <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: center">
                                            <asp:Label ID="lblTotal3" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3. Escriba el n�mero de ni�os ind�genas, desglos�ndolo por sexo."
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                            <asp:TextBox ID="txtV370" runat="server" Columns="3" TabIndex="10801" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                            <asp:TextBox ID="txtV371" runat="server" Columns="3" TabIndex="10802" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                            <asp:TextBox ID="txtV372" runat="server" Columns="3" TabIndex="10803" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="lbl4" runat="server" CssClass="lblRojo" Text="4. Escriba el n�mero de alumnos de nacionalidad extranjera, desglos�ndolo por sexo."
                                                Width="470px"></asp:Label><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblHombres4" runat="server" CssClass="lblRojo" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: center" class="linaBajoAlto">
                                            <asp:Label ID="lblMujeres4" runat="server" CssClass="lblRojo" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: center" class="linaBajoAlto Orila">
                                            <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajoAlto">
                                            <asp:Label ID="lblEEUU" runat="server" CssClass="lblGrisTit" Text="ESTADOS UNIDOS"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV373" runat="server" Columns="3" TabIndex="10901" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV374" runat="server" Columns="3" TabIndex="10902" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV375" runat="server" Columns="3" TabIndex="10903" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblCanada" runat="server" CssClass="lblGrisTit" Text="CANAD�" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV376" runat="server" Columns="3" TabIndex="11001" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV377" runat="server" Columns="3" TabIndex="11002" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV378" runat="server" Columns="3" TabIndex="11003" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblCA" runat="server" CssClass="lblGrisTit" Text="CENTROAM�RICA Y EL CARIBE"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV379" runat="server" Columns="3" TabIndex="11101" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV380" runat="server" Columns="3" TabIndex="11102" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV381" runat="server" Columns="3" TabIndex="11103" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblSA" runat="server" CssClass="lblGrisTit" Text="SUDAM�RICA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV382" runat="server" Columns="3" TabIndex="11201" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV383" runat="server" Columns="3" TabIndex="11202" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV384" runat="server" Columns="3" TabIndex="11203" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblAfrica" runat="server" CssClass="lblGrisTit" Text="�FRICA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV385" runat="server" Columns="3" TabIndex="11301" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV386" runat="server" Columns="3" TabIndex="11302" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV387" runat="server" Columns="3" TabIndex="11303" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblAsia" runat="server" CssClass="lblGrisTit" Text="ASIA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV388" runat="server" Columns="3" TabIndex="11401" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV389" runat="server" Columns="3" TabIndex="11402" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV390" runat="server" Columns="3" TabIndex="11403" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblEuropa" runat="server" CssClass="lblGrisTit" Text="EUROPA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV391" runat="server" Columns="3" TabIndex="11501" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV392" runat="server" Columns="3" TabIndex="11502" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV393" runat="server" Columns="3" TabIndex="11503" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblOceania" runat="server" CssClass="lblGrisTit" Text="OCEAN�A" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV394" runat="server" Columns="3" TabIndex="11601" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV395" runat="server" Columns="3" TabIndex="11602" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV396" runat="server" Columns="3" TabIndex="11603" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblTotal42" runat="server" CssClass="lblRojo" Text="TOTAL" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV397" runat="server" Columns="3" TabIndex="11701" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV398" runat="server" Columns="3" TabIndex="11702" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV399" runat="server" Columns="3" TabIndex="11703" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <br />
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center">
                                            <asp:Label ID="lblHombres5" runat="server" CssClass="lblRojo" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center">
                                            <asp:Label ID="lblMujeres5" runat="server" CssClass="lblRojo" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center">
                                            <asp:Label ID="lblTotal5" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                      
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: justify">
                                            <asp:Label ID="lbl5" runat="server" CssClass="lblRojo" Text="5. Escriba el n�mero de alumnos atendidos por la Unidad de Servicios de Apoyo a la Educaci�n Regular (USAER), desglos�ndolo por sexo."
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                            <asp:TextBox ID="txtV400" runat="server" Columns="3" TabIndex="11801" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                            <asp:TextBox ID="txtV401" runat="server" Columns="3" TabIndex="11802" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                            <asp:TextBox ID="txtV402" runat="server" Columns="3" TabIndex="11803" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                       
                                    </tr>
                                    <tr>
                                        <td colspan="5" rowspan="1" style="vertical-align: middle; width: 150px; text-align: justify">
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" 
                                            Text="
                                                6. Escriba la cantidad de alumnos con alguna discapacidad, aptitudes sobresalientes u otras condiciones, desglos�ndolos por sexo. 
                                                    Enseguida registre el n�mero de alumnos con Necesidades Educativas Especiales (NEE), independientemente de que presenten o no alguna discapacidad."
                                                Width="470px"></asp:Label><br />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="2" style="padding-bottom: 10px; vertical-align: middle;
                                            width: 150px; text-align: center">
                                            <asp:Label ID="lblSituacion" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNO"
                                                Width="250px"></asp:Label></td>
                                        <td colspan="3" rowspan="1" style="padding-bottom: 10px; vertical-align: middle;
                                            width: 150px; text-align: center" class="linaBajoAlto Orila">
                                            <asp:Label ID="lblAlumnos6" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="201px"></asp:Label></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:Label ID="lblHombres6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:Label ID="lblMujeres6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:Label ID="lblTotal6" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajoAlto">
                                            <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV403" runat="server" Columns="3" TabIndex="11901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV404" runat="server" Columns="3" TabIndex="11902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV405" runat="server" Columns="3" TabIndex="11903" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV406" runat="server" Columns="3" TabIndex="12001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV407" runat="server" Columns="3" TabIndex="12002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV408" runat="server" Columns="3" TabIndex="12003" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV409" runat="server" Columns="3" TabIndex="12101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV410" runat="server" Columns="3" TabIndex="12102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV411" runat="server" Columns="3" TabIndex="12103" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV412" runat="server" Columns="3" TabIndex="12201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV413" runat="server" Columns="3" TabIndex="12202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV414" runat="server" Columns="3" TabIndex="12203" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV415" runat="server" Columns="3" TabIndex="12301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV416" runat="server" Columns="3" TabIndex="12302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV417" runat="server" Columns="3" TabIndex="12303" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV418" runat="server" Columns="3" TabIndex="12401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV419" runat="server" Columns="3" TabIndex="12402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV420" runat="server" Columns="3" TabIndex="12403" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                                                Width="265px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV421" runat="server" Columns="3" TabIndex="12501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV422" runat="server" Columns="3" TabIndex="12502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV423" runat="server" Columns="3" TabIndex="12503" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV424" runat="server" Columns="3" TabIndex="12601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV425" runat="server" Columns="3" TabIndex="12602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV426" runat="server" Columns="3" TabIndex="12603" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblTotal62" runat="server" CssClass="lblRojo" Text="TOTAL" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV427" runat="server" Columns="3" TabIndex="12701" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV428" runat="server" Columns="3" TabIndex="12702" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV429" runat="server" Columns="3" TabIndex="12703" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left" class="linaBajo">
                                            <asp:Label ID="lblNEE" runat="server" CssClass="lblRojo" Text="NECESIDADES EDUCATIVAS ESPECIALES"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV430" runat="server" Columns="3" TabIndex="12801" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo">
                                            <asp:TextBox ID="txtV431" runat="server" Columns="3" TabIndex="12802" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" class="linaBajo Orila">
                                            <asp:TextBox ID="txtV432" runat="server" Columns="3" TabIndex="12803" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td class="Orila">&nbsp;
                                            </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        <hr />
        <table>
            <tr>
                <td ><span  onclick="openPage('AGT_911_3',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_3',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                    &nbsp;
                </td>
                <td ><span  onclick="openPage('Personal_911_3',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_3',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado"  ></div>


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center>
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                 MaxCol = 5;
                MaxRow = 29;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
