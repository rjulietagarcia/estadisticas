<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="ECC-12(Alumnos)" AutoEventWireup="true" CodeBehind="Alumnos_ECC_12.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.AP_ECC_12.Alumnos_ECC_12" %>

 <asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server"> 

   
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 17;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
 
<div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">

    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  
                      Text="EDUCACI�N  PRIMARIA CONAFE"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table>
    </div></div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_ECC_12',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Alumnos_ECC_12',true)"><a href="#" title="" class="activo"><span>ALUMNOS</span></a></li><li onclick="openPage('Anexo_ECC_12',false)"><a href="#" title=""><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
     <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
<center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 594px">
            <tr>
                <td colspan="3" style="width: 898px;text-align:center">
                    <asp:Label ID="Label36" runat="server" CssClass="lblRojo" Font-Size="16px" 
                    Text="IMPORTANTE: AL CONTESTAR ESTE CUESTIONARIO NO CONSIDERE LA INFORMACI�N DE PREESCOLAR"
                        Width="100%"></asp:Label></td>
                
            </tr>
            <tr>
                <td style="width: 898px;text-align:justify">
                    <asp:Label ID="lblALUMNOS" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="I. ALUMNOS"
                        Width="100%"></asp:Label></td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 898px">
                    <asp:Label ID="lblInstrucci�n1" runat="server" CssClass="lblRojo" Text="1. Seleccione el tipo de servicio."
                        Width="100%"></asp:Label></td>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblInstrucci�n2" runat="server" CssClass="lblRojo" Text="2. �El proyecto es de aulas compartidas?"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 898px">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 559px; text-align: left">
                                <asp:Label ID="lblComunitario" runat="server" CssClass="lblGrisTit" 
                                    Text="Cursos comunitarios" Width="100%"></asp:Label></td>
                            <td>
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="rbtV1" runat="server" GroupName="servicio"  />
                                <asp:TextBox ID="txtV1"   style="visibility:hidden;" runat="server" Columns="1" Height="1px" Width="1px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 559px; text-align: left">
                                <asp:Label ID="lblPAEPI" runat="server" CssClass="lblGrisTit" Text="Proyecto de Atenci�n Educativa a la Poblaci�n Ind�gena (PAEPI)"></asp:Label></td>
                            <td>
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="rbtV2" runat="server" GroupName="servicio"  />
                                <asp:TextBox ID="txtV2"  style="visibility:hidden;" runat="server" Columns="1" Height="1px" Width="1px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 559px; text-align: left">
                                <asp:Label ID="lblPAEPIAM" runat="server" CssClass="lblGrisTit" Text="Proyecto de Atenci�n Educativa a la Poblaci�n Infantil Agr�cola Migrante (PAEPIAM)"></asp:Label></td>
                            <td>
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="rbtV3" runat="server" GroupName="servicio"  />
                                <asp:TextBox ID="txtV3"  style="visibility:hidden;" runat="server" Columns="1" Height="1px" Width="1px"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                    <table style="width: 418px">
                        <tr>
                            <td >
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="rbtV410" runat="server" GroupName="compartidas" Text="S�"  />
                                <asp:TextBox ID="txtV410"  style="visibility:hidden;" runat="server" Columns="1" Height="1px" Width="1px"></asp:TextBox></td>
                            <td >
                                <asp:RadioButton onclick = "OPTs2Txt();" ID="rbtV411" runat="server" GroupName="compartidas" Text="NO"  />
                                <asp:TextBox ID="txtV411"  style="visibility:hidden;" runat="server" Columns="1" Height="1px" Width="1px"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblInstrucci�n3" runat="server" CssClass="lblRojo" Text="3. Escriba el total de alumnos inscritos a partir de la fecha de inicio de cursos, sumando las altas y restando las bajas hasta el 31 de octubre, desglos�ndolo por nivel, ciclo, sexo y edad. Verifique que la suma de los subtotales de los alumnos por edad sea igual al total."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3">
        <table>
            <tr>
                <td style="width: 29px">
                    -</td>
                <td style="width: 181px">
                </td>
                <td>
                </td>
                <td style="text-align: center; width: 83px;" class="linaBajoAlto">
                    <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Text="Menos de 6 a�os"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit"  Text="6 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit"  Text="7 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit"  Text="8 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit"  Text="9 a�os"
                        Width="100%"></asp:Label></td>
                <td style="width: 79px; text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit"  Text="10 a�os"
                        Width="100%"></asp:Label></td>
                <td style="width: 79px; text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit"  Text="11 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit"  Text="12 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit"  Text="13 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit"  Text="14 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit"  Text="15 a�os y m�s"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTotal3" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 29px;" rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblNivel1" runat="server" CssClass="lblRojo"  Text="1er. NIVEL"
                        Width="100%"></asp:Label></td>
                <td style="width: 181px;" rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblciclo11" runat="server" CssClass="lblNegro"  Text="1er. ciclo (1er. a�o)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="lblHombres11" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label>
                    </td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV4" runat="server" TabIndex="10101" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV5" runat="server" TabIndex="10102" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV6" runat="server" TabIndex="10103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV7" runat="server" TabIndex="10104" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV8" runat="server" TabIndex="10105" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV9" runat="server" TabIndex="10106" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV10" runat="server" TabIndex="10107" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV11" runat="server" TabIndex="10108" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV12" runat="server" TabIndex="10109" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV13" runat="server" TabIndex="10110" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV14" runat="server" TabIndex="10111" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV15" runat="server" TabIndex="10112" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;" class="linaBajo">
                    <asp:Label ID="lblMujeres11" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV16" runat="server" TabIndex="10201" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV17" runat="server" TabIndex="10202" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV18" runat="server" TabIndex="10203" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV19" runat="server" TabIndex="10204" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV20" runat="server" TabIndex="10205" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 12px; width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV21" runat="server" TabIndex="10206" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 12px; width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV22" runat="server" TabIndex="10207" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV23" runat="server" TabIndex="10208" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 12px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV24" runat="server" TabIndex="10209" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 12px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV25" runat="server" TabIndex="10210" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 12px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV26" runat="server" TabIndex="10211" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV27" runat="server" TabIndex="10212" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:Label ID="lblSTotal11" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV28" runat="server" TabIndex="10301" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV29" runat="server" TabIndex="10302" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV30" runat="server" TabIndex="10303" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV31" runat="server" TabIndex="10304" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV32" runat="server" TabIndex="10305" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 6px; width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV33" runat="server" TabIndex="10306" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 6px; width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV34" runat="server" TabIndex="10307" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV35" runat="server" TabIndex="10308" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 6px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV36" runat="server" TabIndex="10309" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 6px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV37" runat="server" TabIndex="10310" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="height: 6px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV38" runat="server" TabIndex="10311" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV39" runat="server" TabIndex="10312" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 181px" rowspan="3" class="linaBajo">
                    <asp:Label ID="lblciclo12" runat="server" CssClass="lblNegro"  Text="2o. ciclo (2o. a�o)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres12" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV40" runat="server" TabIndex="10401" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV41" runat="server" TabIndex="10402" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV42" runat="server" TabIndex="10403" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV43" runat="server" TabIndex="10404" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV44" runat="server" TabIndex="10405" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV45" runat="server" TabIndex="10406" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV46" runat="server" TabIndex="10407" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV47" runat="server" TabIndex="10408" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV48" runat="server" TabIndex="10409" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV49" runat="server" TabIndex="10410" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV50" runat="server" TabIndex="10411" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV51" runat="server" TabIndex="10412" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres12" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV52" runat="server" TabIndex="10501" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV53" runat="server" TabIndex="10502" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV54" runat="server" TabIndex="10503" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV55" runat="server" TabIndex="10504" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV56" runat="server" TabIndex="10505" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV57" runat="server" TabIndex="10506" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV58" runat="server" TabIndex="10507" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV59" runat="server" TabIndex="10508" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV60" runat="server" TabIndex="10509" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV61" runat="server" TabIndex="10510" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV62" runat="server" TabIndex="10511" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV63" runat="server" TabIndex="10512" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblSTotal12" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV64" runat="server" TabIndex="10601" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV65" runat="server" TabIndex="10602" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV66" runat="server" TabIndex="10603" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV67" runat="server" TabIndex="10604" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV68" runat="server" TabIndex="10605" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV69" runat="server" TabIndex="10606" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV70" runat="server" TabIndex="10607" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV71" runat="server" TabIndex="10608" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV72" runat="server" TabIndex="10609" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV73" runat="server" TabIndex="10610" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                <asp:TextBox ID="txtV74" runat="server" TabIndex="10611" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                <asp:TextBox ID="txtV75" runat="server" TabIndex="10612" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 181px" rowspan="3" class="linaBajo">
                    <asp:Label ID="lblciclo13" runat="server" CssClass="lblNegro"  Text="3er. ciclo o m�s (3er. a�o o m�s)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres13" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                    <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV76" runat="server" TabIndex="10701" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV77" runat="server" TabIndex="10702" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV78" runat="server" TabIndex="10703" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV79" runat="server" TabIndex="10704" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV80" runat="server" TabIndex="10705" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV81" runat="server" TabIndex="10706" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV82" runat="server" TabIndex="10707" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV83" runat="server" TabIndex="10708" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV84" runat="server" TabIndex="10709" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV85" runat="server" TabIndex="10710" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV86" runat="server" TabIndex="10711" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV87" runat="server" TabIndex="10712" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres13" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV88" runat="server" TabIndex="10801" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV89" runat="server" TabIndex="10802" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV90" runat="server" TabIndex="10803" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV91" runat="server" TabIndex="10804" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV92" runat="server" TabIndex="10805" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV93" runat="server" TabIndex="10806" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV94" runat="server" TabIndex="10807" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV95" runat="server" TabIndex="10808" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV96" runat="server" TabIndex="10809" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV97" runat="server" TabIndex="10810" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV98" runat="server" TabIndex="10811" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV99" runat="server" TabIndex="10812" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblSTotal13" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV100" runat="server" TabIndex="10901" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV101" runat="server" TabIndex="10902" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV102" runat="server" TabIndex="10903" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV103" runat="server" TabIndex="10904" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV104" runat="server" TabIndex="10905" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV105" runat="server" TabIndex="10906" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV106" runat="server" TabIndex="10907" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV107" runat="server" TabIndex="10908" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV108" runat="server" TabIndex="10909" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV109" runat="server" TabIndex="10910" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV110" runat="server" TabIndex="10911" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV111" runat="server" TabIndex="10912" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
              <tr>
                <td style="width: 29px">
                    -</td>
                <td style="width: 181px">
                </td>
                <td>
                </td>
                <td style="text-align: center; width: 83px;" class="linaBajoAlto">
                    &nbsp;<asp:Label ID="Label48" runat="server" CssClass="lblGrisTit" Text="Menos de 6 a�os"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit"  Text="6 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit"  Text="7 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit"  Text="8 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit"  Text="9 a�os"
                        Width="100%"></asp:Label></td>
                <td style="width: 79px; text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit"  Text="10 a�os"
                        Width="100%"></asp:Label></td>
                <td style="width: 79px; text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit"  Text="11 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label19" runat="server" CssClass="lblGrisTit"  Text="12 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit"  Text="13 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit"  Text="14 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label22" runat="server" CssClass="lblGrisTit"  Text="15 a�os y m�s"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label23" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                        Width="100%"></asp:Label></td>
                  <td class="Orila" style="text-align: center">
                      &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 29px" rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblNivel2" runat="server" CssClass="lblRojo"  Text="2o. NIVEL"
                        Width="100%"></asp:Label></td>
                <td style="width: 181px" rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblciclo21" runat="server" CssClass="lblNegro"  Text="1er. ciclo (1er. a�o)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblHombres21" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV112" runat="server" TabIndex="11001" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV113" runat="server" TabIndex="11002" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV114" runat="server" TabIndex="11003" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV115" runat="server" TabIndex="11004" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV116" runat="server" TabIndex="11005" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV117" runat="server" TabIndex="11006" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV118" runat="server" TabIndex="11007" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV119" runat="server" TabIndex="11008" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV120" runat="server" TabIndex="11009" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV121" runat="server" TabIndex="11010" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV122" runat="server" TabIndex="11011" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV123" runat="server" TabIndex="11012" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres21" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV124" runat="server" TabIndex="11101" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV125" runat="server" TabIndex="11102" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV126" runat="server" TabIndex="11103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV127" runat="server" TabIndex="11104" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV128" runat="server" TabIndex="11105" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV129" runat="server" TabIndex="11106" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV130" runat="server" TabIndex="11107" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV131" runat="server" TabIndex="11108" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV132" runat="server" TabIndex="11109" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV133" runat="server" TabIndex="11110" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV134" runat="server" TabIndex="11111" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV135" runat="server" TabIndex="11112" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblSTotal21" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV136" runat="server" TabIndex="11201" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV137" runat="server" TabIndex="11202" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV138" runat="server" TabIndex="11203" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV139" runat="server" TabIndex="11204" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV140" runat="server" TabIndex="11205" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV141" runat="server" TabIndex="11206" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV142" runat="server" TabIndex="11207" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV143" runat="server" TabIndex="11208" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV144" runat="server" TabIndex="11209" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV145" runat="server" TabIndex="11210" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV146" runat="server" TabIndex="11211" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV147" runat="server" TabIndex="11212" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 181px" rowspan="3" class="linaBajo">
                    <asp:Label ID="lblciclo22" runat="server" CssClass="lblNegro"  Text="2o. ciclo (2o. a�o)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres22" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV148" runat="server" TabIndex="11301" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV149" runat="server" TabIndex="11302" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV150" runat="server" TabIndex="11303" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV151" runat="server" TabIndex="11304" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV152" runat="server" TabIndex="11305" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV153" runat="server" TabIndex="11306" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV154" runat="server" TabIndex="11307" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV155" runat="server" TabIndex="11308" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV156" runat="server" TabIndex="11309" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV157" runat="server" TabIndex="11310" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV158" runat="server" TabIndex="11311" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV159" runat="server" TabIndex="11312" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres22" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV160" runat="server" TabIndex="11401" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV161" runat="server" TabIndex="11402" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV162" runat="server" TabIndex="11403" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV163" runat="server" TabIndex="11404" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV164" runat="server" TabIndex="11405" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV165" runat="server" TabIndex="11406" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV166" runat="server" TabIndex="11407" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV167" runat="server" TabIndex="11408" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV168" runat="server" TabIndex="11409" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV169" runat="server" TabIndex="11410" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV170" runat="server" TabIndex="11411" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV171" runat="server" TabIndex="11412" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblSTotal22" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV172" runat="server" TabIndex="11501" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV173" runat="server" TabIndex="11502" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV174" runat="server" TabIndex="11503" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV175" runat="server" TabIndex="11504" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV176" runat="server" TabIndex="11505" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV177" runat="server" TabIndex="11506" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV178" runat="server" TabIndex="11507" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV179" runat="server" TabIndex="11508" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV180" runat="server" TabIndex="11509" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV181" runat="server" TabIndex="11510" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV182" runat="server" TabIndex="11511" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV183" runat="server" TabIndex="11512" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 181px" rowspan="3" class="linaBajo">
                    <asp:Label ID="lblciclo23" runat="server" CssClass="lblNegro"  Text="3er. ciclo o m�s (3er. a�o o m�s)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres23" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV184" runat="server" TabIndex="11601" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV185" runat="server" TabIndex="11602" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox>
                </td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV186" runat="server" TabIndex="11603" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV187" runat="server" TabIndex="11604" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV188" runat="server" TabIndex="11605" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV189" runat="server" TabIndex="11606" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV190" runat="server" TabIndex="11607" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV191" runat="server" TabIndex="11608" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV192" runat="server" TabIndex="11609" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV193" runat="server" TabIndex="11610" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV194" runat="server" TabIndex="11611" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV195" runat="server" TabIndex="11612" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres23" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV196" runat="server" TabIndex="11701" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV197" runat="server" TabIndex="11702" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV198" runat="server" TabIndex="11703" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV199" runat="server" TabIndex="11704" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV200" runat="server" TabIndex="11705" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV201" runat="server" TabIndex="11706" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV202" runat="server" TabIndex="11707" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV203" runat="server" TabIndex="11708" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV204" runat="server" TabIndex="11709" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV205" runat="server" TabIndex="11710" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV206" runat="server" TabIndex="11711" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV207" runat="server" TabIndex="11712" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblSTotal23" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV208" runat="server" TabIndex="11801" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV209" runat="server" TabIndex="11802" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV210" runat="server" TabIndex="11803" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV211" runat="server" TabIndex="11804" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV212" runat="server" TabIndex="11805" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV213" runat="server" TabIndex="11806" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV214" runat="server" TabIndex="11807" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV215" runat="server" TabIndex="11808" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV216" runat="server" TabIndex="11809" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV217" runat="server" TabIndex="11810" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV218" runat="server" TabIndex="11811" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV219" runat="server" TabIndex="11812" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
              <tr>
                <td style="width: 29px">
                    -</td>
                <td style="width: 181px">
                </td>
                <td>
                </td>
                <td style="text-align: center; width: 83px;" class="linaBajoAlto">
                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Text="Menos de 6 a�os"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit"  Text="6 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit"  Text="7 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit"  Text="8 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label28" runat="server" CssClass="lblGrisTit"  Text="9 a�os"
                        Width="100%"></asp:Label></td>
                <td style="width: 79px; text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit"  Text="10 a�os"
                        Width="100%"></asp:Label></td>
                <td style="width: 79px; text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label30" runat="server" CssClass="lblGrisTit"  Text="11 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label31" runat="server" CssClass="lblGrisTit"  Text="12 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label32" runat="server" CssClass="lblGrisTit"  Text="13 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label33" runat="server" CssClass="lblGrisTit"  Text="14 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label34" runat="server" CssClass="lblGrisTit"  Text="15 a�os y m�s"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label35" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                        Width="100%"></asp:Label></td>
                  <td class="Orila" style="text-align: center">
                      &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 29px" rowspan="9" class="linaBajoAlto">
                    <asp:Label ID="lblNivel3" runat="server" CssClass="lblRojo"  Text="3er. NIVEL"
                        Width="100%"></asp:Label></td>
                <td style="width: 181px" rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblciclo31" runat="server" CssClass="lblNegro"  Text="1er. ciclo (1er. a�o)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblHombres31" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV220" runat="server" TabIndex="11901" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV221" runat="server" TabIndex="11902" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV222" runat="server" TabIndex="11903" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV223" runat="server" TabIndex="11904" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV224" runat="server" TabIndex="11905" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV225" runat="server" TabIndex="11906" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV226" runat="server" TabIndex="11907" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV227" runat="server" TabIndex="11908" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV228" runat="server" TabIndex="11909" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV229" runat="server" TabIndex="11910" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV230" runat="server" TabIndex="11911" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV231" runat="server" TabIndex="11912" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres31" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV232" runat="server" TabIndex="12001" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV233" runat="server" TabIndex="12002" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV234" runat="server" TabIndex="12003" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV235" runat="server" TabIndex="12004" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV236" runat="server" TabIndex="12005" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV237" runat="server" TabIndex="12006" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV238" runat="server" TabIndex="12007" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV239" runat="server" TabIndex="12008" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV240" runat="server" TabIndex="12009" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV241" runat="server" TabIndex="12010" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV242" runat="server" TabIndex="12011" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV243" runat="server" TabIndex="12012" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblSTotal31" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV244" runat="server" TabIndex="12101" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV245" runat="server" TabIndex="12102" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV246" runat="server" TabIndex="12103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV247" runat="server" TabIndex="12104" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV248" runat="server" TabIndex="12105" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV249" runat="server" TabIndex="12106" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV250" runat="server" TabIndex="12107" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV251" runat="server" TabIndex="12108" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV252" runat="server" TabIndex="12109" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV253" runat="server" TabIndex="12110" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV254" runat="server" TabIndex="12111" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV255" runat="server" TabIndex="12112" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 181px" rowspan="3" class="linaBajo">
                    <asp:Label ID="lblciclo32" runat="server" CssClass="lblNegro"  Text="2o. ciclo (2o. a�o)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres32" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV256" runat="server" TabIndex="12201" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV257" runat="server" TabIndex="12202" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV258" runat="server" TabIndex="12203" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV259" runat="server" TabIndex="12204" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV260" runat="server" TabIndex="12205" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox>&nbsp;
                </td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV261" runat="server" TabIndex="12206" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV262" runat="server" TabIndex="12207" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV263" runat="server" TabIndex="12208" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV264" runat="server" TabIndex="12209" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV265" runat="server" TabIndex="12210" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV266" runat="server" TabIndex="12211" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV267" runat="server" TabIndex="12212" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres32" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV268" runat="server" TabIndex="12301" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV269" runat="server" TabIndex="12302" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV270" runat="server" TabIndex="12303" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV271" runat="server" TabIndex="12304" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV272" runat="server" TabIndex="12305" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV273" runat="server" TabIndex="12306" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV274" runat="server" TabIndex="12307" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV275" runat="server" TabIndex="12308" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV276" runat="server" TabIndex="12309" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV277" runat="server" TabIndex="12310" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV278" runat="server" TabIndex="12311" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV279" runat="server" TabIndex="12312" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblSTotal32" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV280" runat="server" TabIndex="12401" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV281" runat="server" TabIndex="12402" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV282" runat="server" TabIndex="12403" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV283" runat="server" TabIndex="12404" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV284" runat="server" TabIndex="12405" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV285" runat="server" TabIndex="12406" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV286" runat="server" TabIndex="12407" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV287" runat="server" TabIndex="12408" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV288" runat="server" TabIndex="12409" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV289" runat="server" TabIndex="12410" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV290" runat="server" TabIndex="12411" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV291" runat="server" TabIndex="12412" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 181px" rowspan="3" class="linaBajo">
                    <asp:Label ID="lblciclo33" runat="server" CssClass="lblNegro"  Text="3er. ciclo o m�s (3er. a�o o m�s)"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblHombres33" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV292" runat="server" TabIndex="12501" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV293" runat="server" TabIndex="12502" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV294" runat="server" TabIndex="12503" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV295" runat="server" TabIndex="12504" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV296" runat="server" TabIndex="12505" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV297" runat="server" TabIndex="12506" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV298" runat="server" TabIndex="12507" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV299" runat="server" TabIndex="12508" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV300" runat="server" TabIndex="12509" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV301" runat="server" TabIndex="12510" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV302" runat="server" TabIndex="12511" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV303" runat="server" TabIndex="12512" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeres33" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV304" runat="server" TabIndex="12601" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV305" runat="server" TabIndex="12602" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV306" runat="server" TabIndex="12603" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV307" runat="server" TabIndex="12604" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV308" runat="server" TabIndex="12605" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV309" runat="server" TabIndex="12606" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV310" runat="server" TabIndex="12607" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV311" runat="server" TabIndex="12608" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV312" runat="server" TabIndex="12609" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV313" runat="server" TabIndex="12610" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV314" runat="server" TabIndex="12611" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV315" runat="server" TabIndex="12612" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblSTotal33" runat="server" CssClass="lblGrisTit"  Text="SUBTOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV316" runat="server" TabIndex="12701" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV317" runat="server" TabIndex="12702" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV318" runat="server" TabIndex="12703" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV319" runat="server" TabIndex="12704" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV320" runat="server" TabIndex="12705" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV321" runat="server" TabIndex="12706" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV322" runat="server" TabIndex="12707" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV323" runat="server" TabIndex="12708" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV324" runat="server" TabIndex="12709" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV325" runat="server" TabIndex="12710" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV326" runat="server" TabIndex="12711" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV327" runat="server" TabIndex="12712" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
              <tr>
                <td style="width: 29px">
                    -</td>
                <td style="width: 181px">
                </td>
                <td>
                </td>
                <td style="text-align: center; width: 83px;" class="linaBajoAlto">
                    <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Text="Menos de 6 a�os"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label37" runat="server" CssClass="lblGrisTit"  Text="6 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label38" runat="server" CssClass="lblGrisTit"  Text="7 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label39" runat="server" CssClass="lblGrisTit"  Text="8 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label40" runat="server" CssClass="lblGrisTit"  Text="9 a�os"
                        Width="100%"></asp:Label></td>
                <td style="width: 79px; text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label41" runat="server" CssClass="lblGrisTit"  Text="10 a�os"
                        Width="100%"></asp:Label></td>
                <td style="width: 79px; text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label42" runat="server" CssClass="lblGrisTit"  Text="11 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label43" runat="server" CssClass="lblGrisTit"  Text="12 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label44" runat="server" CssClass="lblGrisTit"  Text="13 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label45" runat="server" CssClass="lblGrisTit"  Text="14 a�os"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center;" class="linaBajoAlto">
                    <asp:Label ID="Label46" runat="server" CssClass="lblGrisTit"  Text="15 a�os y m�s"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="Label47" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                        Width="100%"></asp:Label></td>
                  <td class="Orila" style="text-align: center">
                      &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 29px">
                </td>
                <td style="width: 181px" rowspan="3" class="linaBajoAlto">
                    <asp:Label ID="lblTotal1" runat="server" CssClass="lblNegro"  Text="TOTAL"
                        Width="100%" Font-Bold="True"></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblHombresT" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV328" runat="server" TabIndex="12801" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV329" runat="server" TabIndex="12802" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV330" runat="server" TabIndex="12803" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV331" runat="server" TabIndex="12804" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV332" runat="server" TabIndex="12805" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV333" runat="server" TabIndex="12806" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV334" runat="server" TabIndex="12807" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV335" runat="server" TabIndex="12808" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox>
                </td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV336" runat="server" TabIndex="12809" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV337" runat="server" TabIndex="12810" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV338" runat="server" TabIndex="12811" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV339" runat="server" TabIndex="12812" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 29px">
                </td>
                <td class="linaBajo">
                    <asp:Label ID="lblMujeresT" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajo">
                    <asp:TextBox ID="txtV340" runat="server" TabIndex="12901" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV341" runat="server" TabIndex="12902" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV342" runat="server" TabIndex="12903" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV343" runat="server" TabIndex="12904" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV344" runat="server" TabIndex="12905" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV345" runat="server" TabIndex="12906" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV346" runat="server" TabIndex="12907" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV347" runat="server" TabIndex="12908" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV348" runat="server" TabIndex="12909" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV349" runat="server" TabIndex="12910" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV350" runat="server" TabIndex="12911" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV351" runat="server" TabIndex="12912" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 29px">
                </td>
                <td class="linaBajoS">
                    <asp:Label ID="lblTotal2" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center; width: 83px;" class="linaBajoS">
                    <asp:TextBox ID="txtV352" runat="server" TabIndex="13001" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV353" runat="server" TabIndex="13002" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV354" runat="server" TabIndex="13003" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV355" runat="server" TabIndex="13004" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV356" runat="server" TabIndex="13005" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV357" runat="server" TabIndex="13006" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="width: 79px; text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV358" runat="server" TabIndex="13007" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV359" runat="server" TabIndex="13008" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV360" runat="server" TabIndex="13009" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV361" runat="server" TabIndex="13010" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV362" runat="server" TabIndex="13011" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV363" runat="server" TabIndex="13012" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;
                </td>
            </tr>
        </table>
                </td>
            </tr>
            <tr>
                <td style="width: 898px;text-align:justify;">
                    <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="4. Del total de alumnos inscritos al primer nivel, registre la cantidad de los que cursaron educaci�n preescolar, seg�n los a�os cursados desglos�ndola por sexo."
                        Width="100%"></asp:Label></td>
                <td style="text-align: justify">
                    &nbsp;</td>
                <td style="text-align:justify">
                    <asp:Label ID="lblInstrucci�n6" runat="server" CssClass="lblRojo" Text="6. Escriba la cantidad de alumnos con Necesidades Educativas Especiales (NEE), independientemente de que presenten o no alguna discapacidad, desglos�ndola por sexo."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 898px; text-align: center;"><table style="width: 365px; height: 1px">
                    <tr>
                        <td style="text-align: left; height: 19px;">
                        </td>
                        <td style="height: 19px">
                            <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                                Width="100%"></asp:Label></td>
                        <td style="height: 19px">
                            <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                                Width="100%"></asp:Label></td>
                        <td style="height: 19px">
                            <asp:Label ID="lblTotal4" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="height: 19px; text-align: left">
                            <asp:Label ID="lblCursado1" runat="server" CssClass="lblGrisTit"  Text="UN A�O CURSADO"
                                Width="100%"></asp:Label></td>
                        <td style="height: 19px">
                            <asp:TextBox ID="txtV364" runat="server" TabIndex="20101" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        <td style="height: 19px">
                            <asp:TextBox ID="txtV365" runat="server" TabIndex="20102" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        <td style="height: 19px">
                            <asp:TextBox ID="txtV366" runat="server" TabIndex="20103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: left; height: 26px;">
                            <asp:Label ID="lblCursado2" runat="server" CssClass="lblGrisTit"  Text="DOS A�OS CURSADOS"
                                Width="100%"></asp:Label></td>
                        <td style="height: 26px">
                            <asp:TextBox ID="txtV367" runat="server" TabIndex="20201" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        <td style="height: 26px">
                            <asp:TextBox ID="txtV368" runat="server" TabIndex="20202" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        <td style="height: 26px">
                            <asp:TextBox ID="txtV369" runat="server" TabIndex="20203" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <asp:Label ID="lblCursado3" runat="server" CssClass="lblGrisTit"  Text="TRES A�OS CURSADOS"
                                Width="100%"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtV370" runat="server" TabIndex="20301" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="txtV371" runat="server" TabIndex="20302" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        <td>
                            <asp:TextBox ID="txtV372" runat="server" TabIndex="20303" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="text-align: right; height: 26px;">
                            <asp:Label ID="lblTotalT4" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                                Width="100%"></asp:Label></td>
                        <td style="height: 26px">
                            <asp:TextBox ID="txtV373" runat="server" TabIndex="20401" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        <td style="height: 26px">
                            <asp:TextBox ID="txtV374" runat="server" TabIndex="20402" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        <td style="height: 26px">
                            <asp:TextBox ID="txtV375" runat="server" TabIndex="20403" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                    </tr>
                </table>
                </td>
                <td style="text-align: center" valign="top">
                    &nbsp;</td>
                <td style="text-align: center" valign="top">
                    <table style="width: 393px">
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblHombres6" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMujeres6" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                                    Width="100%"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotal6" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV403" runat="server" TabIndex="30101" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV404" runat="server" TabIndex="30102" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center; height: 26px;">
                                <asp:TextBox ID="txtV405" runat="server" TabIndex="30103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 898px; height: 18px;text-align:justify">
                    <asp:Label ID="lblInstrucci�n5" runat="server" CssClass="lblRojo" Text="5. Escriba la cantidad de alumnos con discapacidad, aptitudes sobresalientes u otras condiciones, desglos�ndolos por sexo."
                        Width="100%"></asp:Label></td>
                <td style="height: 18px; text-align: justify">
                    &nbsp;</td>
                <td style="height: 18px;text-align:justify">
                    <asp:Label ID="lblPERSONAL" runat="server" CssClass="lblGrisTit" Text="II. PERSONAL"
                        Width="100%" Font-Size="16px"></asp:Label><br />
                    <asp:Label ID="lblinstructores" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de instructores comunitarios desglos�ndola por sexo."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 898px; height: 18px; text-align: center;">
                    <table style="width: 365px; height: 1px">
                        <tr>
                            <td style="text-align: left"></td>
                            <td colspan="3" style="text-align:center">
                                <asp:Label ID="Label50" runat="server" CssClass="lblGrisTit"  Text="A L U M N O S"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblSituaci�n" runat="server" CssClass="lblNegro"  Text="SITUACI�N DEL ALUMNO"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblHombres5" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblMujeres5" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:Label ID="lblTotal5" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 19px; text-align: left">
                                <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit"  Text="CEGUERA"
                                    Width="100%"></asp:Label></td>
                            <td style="height: 19px">
                                <asp:TextBox ID="txtV376" runat="server" TabIndex="20501" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 19px">
                                <asp:TextBox ID="txtV377" runat="server" TabIndex="20502" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 19px">
                                <asp:TextBox ID="txtV378" runat="server" TabIndex="20503" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblVisual" runat="server" CssClass="lblGrisTit"  Text="BAJA VISI�N"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV379" runat="server" TabIndex="20601" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV380" runat="server" TabIndex="20602" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV381" runat="server" TabIndex="20603" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit"  Text="SORDERA"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV382" runat="server" TabIndex="20701" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV383" runat="server" TabIndex="20702" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV384" runat="server" TabIndex="20703" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblAuditiva" runat="server" CssClass="lblGrisTit"  Text="HIPOACUSIA"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV385" runat="server" TabIndex="20801" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV386" runat="server" TabIndex="20802" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV387" runat="server" TabIndex="20803" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 19px; text-align: left">
                                <asp:Label ID="lblMotriz" runat="server" CssClass="lblGrisTit"  Text="DISCAPACIDAD MOTRIZ"
                                    Width="100%"></asp:Label></td>
                            <td style="height: 19px">
                                <asp:TextBox ID="txtV388" runat="server" TabIndex="20901" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 19px">
                                <asp:TextBox ID="txtV389" runat="server" TabIndex="20902" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 19px">
                                <asp:TextBox ID="txtV390" runat="server" TabIndex="20903" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblIntelectual" runat="server" CssClass="lblGrisTit" 
                                    Text="DISCAPACIDAD INTELECTUAL" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV391" runat="server" TabIndex="21001" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV392" runat="server" TabIndex="21002" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV393" runat="server" TabIndex="21003" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left; height: 35px;">
                                <asp:Label ID="lblCAS" runat="server" CssClass="lblGrisTit"  Text="APTITUDES SOBRESALIENTES"
                                    Width="100%"></asp:Label></td>
                            <td style="height: 35px">
                                <asp:TextBox ID="txtV394" runat="server" TabIndex="21101" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 35px">
                                <asp:TextBox ID="txtV395" runat="server" TabIndex="21102" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 35px">
                                <asp:TextBox ID="txtV396" runat="server" TabIndex="21103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="height: 26px; text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit"  Text="OTRAS CONDICIONES"
                                    Width="100%"></asp:Label></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtV397" runat="server" TabIndex="21201" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtV398" runat="server" TabIndex="21202" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtV399" runat="server" TabIndex="21203" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Label ID="lblTotalGeneral" runat="server" CssClass="lblGrisTit" 
                                    Text="TOTAL" Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV400" runat="server" TabIndex="21301" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV401" runat="server" TabIndex="21302" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV402" runat="server" TabIndex="21303" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
                <td style="height: 18px; text-align: center" valign="top">
                    &nbsp;</td>
                <td style="height: 18px; text-align: center;" valign="top">
                    <table style="width: 393px">
                        <tr>
                            <td style="text-align: center">
                                <asp:Label ID="lblHombresP" runat="server" CssClass="lblGrisTit"  Text="HOMBRES"
                                    Width="100%"></asp:Label></td>
                            <td style="text-align: center">
                                <asp:Label ID="lblMujeresP" runat="server" CssClass="lblGrisTit"  Text="MUJERES"
                                    Width="100%"></asp:Label>
                            </td>
                            <td style="text-align: center">
                                <asp:Label ID="lblTotalP" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV406" runat="server" TabIndex="30201" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV407" runat="server" TabIndex="30202" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="text-align: center">
                                <asp:TextBox ID="txtV408" runat="server" TabIndex="30203" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_ECC_12',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_ECC_12',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Anexo_ECC_12',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_ECC_12',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                 var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		   
 		        
 		        function OPTs2Txt(){
                     marcarTXT('1');
                     marcarTXT('2');
                     marcarTXT('3');
                     
                     marcarTXT('410');
                     marcarTXT('411');
                } 
                function PintaOPTs(){
                     marcar('1');
                     marcar('2');
                     marcar('3');
                     marcar('410');
                     marcar('411');
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_rbtV'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_rbtV'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                
              PintaOPTs();
              Disparador(<%=hidDisparador.Value %>);
           GetTabIndexes();
        </script> 
</asp:Content>
