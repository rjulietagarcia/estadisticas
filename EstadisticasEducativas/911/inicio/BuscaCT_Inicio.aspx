<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasicMenu.Master" AutoEventWireup="true" CodeBehind="BuscaCT_Inicio.aspx.cs" Inherits="EstadisticasEducativas._911.inicio.BuscaCT_Inicio" Title="Busqueda Centros de Trabajo"  %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    
       
        <br />
        <br />
        <center>
        <div style="text-align:center; width:100%">
             <center>
                
           <table class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"></td>
				    <td>
				     
				        <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">										
								<tr>
									<td class="Titulo" style="FONT-WEIGHT: bold" align="center" >
                                        Inicio de Cursos 
                                    </td>
								</tr>
						</table>
						<br />
						
				    <asp:UpdatePanel ID="upBusqueda" runat="server">
                        <ContentTemplate>
                        
			            <table   border="0" cellpadding="0" cellspacing="2"  >
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label ID="Label1" runat="server" CssClass="lblEtiqueta" Font-Size="14px" Text="Ciclo Escolar:"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlCiclo" runat="server" Enabled="false">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                 <td align="left" style="width: 133px">
                                    <asp:Label ID="lblEntidad" runat="server" CssClass="lblEtiqueta" Font-Size="14px" Text="Entidad:"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlEntidad" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEntidad_SelectedIndexChanged">
                                        <asp:ListItem Value="0">Seleccione un Estado</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                           <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblRegion" runat="server" Text="Regi�n:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <%--<asp:TextBox   ID="txtRegion" runat="server" Width="55px" TabIndex="1"
                                        MaxLength="12"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlRegion" runat="server" Enabled="false" >
                                            <asp:ListItem Value="0">Seleccione una Regi�n</asp:ListItem>
                                          
                                    </asp:DropDownList>
                                        </td>
                            </tr> 
                           <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label ID="lblNivel" runat="server"  Font-Size="14px" Text="Nivel:" CssClass="lblEtiqueta" ></asp:Label>
                                    </td>
                                <td align="left" style="width: 120px">
                                    <asp:DropDownList ID="ddlNivel" runat="server" OnSelectedIndexChanged="ddlNivel_SelectedIndexChanged">
                                    </asp:DropDownList></td>
                            </tr>
                            
                            <tr>
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblZona" runat="server" Text="Zona:" Width="43px" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtZona" runat="server" Width="55px" TabIndex="2"
                                        MaxLength="3"></asp:TextBox></td>
                            </tr>
                            <tr id="trNiv">
                                <td align="left" style="width: 133px">
                                    <asp:Label  ID="lblClave" runat="server" Text="Centro de Trabajo:" Font-Size="14px" CssClass="lblEtiqueta"></asp:Label></td>
                                <td align="left" style="width: 120px">
                                    <asp:TextBox   ID="txtClaveCT" runat="server" Width="110px" TabIndex="3"
                                        MaxLength="10"></asp:TextBox>
                                    <asp:DropDownList ID="ddlCentroTrabajo" runat="server" Visible="False">
                                    </asp:DropDownList></td>
                            </tr>
                        </table>   
                   </ContentTemplate>
                        </asp:UpdatePanel>
                         
                        <br />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="67px" TabIndex="4" OnClick="btnBuscar_Click"   /><br />
                        </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
               <div>
          
            <br />
           <asp:UpdatePanel ID="upMsg" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ddlNivel" EventName="SelectedIndexChanged" />
                </Triggers>
                <ContentTemplate> 
                    <asp:Label ID="lblMsg" runat="server"  ></asp:Label>
              </ContentTemplate>
            </asp:UpdatePanel> 
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">

            <ProgressTemplate>

            <asp:Image ID="Image1" runat="server" ImageUrl="../../tema/images/loading.gif" />
            <!-- this is an animated progress gif that will be shown while progress delay -->

            </ProgressTemplate>
            </asp:UpdateProgress>
            
            </div>
            </center>
        </div>
        </center>
        <br />
     
                <center>
                
                <div style="text-align: center">
                   <center>
                   <asp:UpdatePanel ID="update" runat="server" >  
                     <ContentTemplate> 
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        PageSize="50" CellPadding="4" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="RowCommand"
                        OnSorting="GridView1_Sorting" EmptyDataText="Busqueda sin registros">
                        <PagerSettings PageButtonCount="20" />
                        <Columns>

<%--                            <asp:TemplateField>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ItemTemplate>
                                    <b>
                                        <a href='javascript:Vent911(<%# Eval("id_nivel") %>,<%# Eval("id_subnivel") %>,<%# Eval("ID_CCTNT") %>,5);'><img id="img911" style="border:0px;" alt="" src="../../tema/images/iconEsc.gif" /></a>
                                    </b>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:ButtonField CommandName="cap" HeaderText="CAP" Text="&lt;img src='../../tema/images/iconEsc.gif' alt='Captura 911' style='border:0;'&gt;" />
                            <asp:BoundField DataField="Id_CT" HeaderText="Id_CT" SortExpression="Id_CT" Visible="False">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ID_CCTNT"  SortExpression="ID_CCTNT" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID_CCTNT" runat="server" Text='<%# Bind("ID_CCTNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="CveCT" HeaderText="CveCT" SortExpression="CveCT">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomCT" HeaderText="NomCT" SortExpression="NomCT">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveTurno" HeaderText="CveTurno" SortExpression="CveTurno">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomTurno" HeaderText="Turno" SortExpression="NomTurno">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveRegion" HeaderText="Region" SortExpression="CveRegion">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CveZona" HeaderText="Zona" SortExpression="CveZona">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Calle" HeaderText="Calle" SortExpression="Calle" Visible="false">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Numero" HeaderText="Numero" SortExpression="Numero" Visible="false">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomColonia" HeaderText="Colonia" SortExpression="NomColonia" Visible="false">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NomMunicipio" HeaderText="Municipio" SortExpression="NomMunicipio" Visible="false">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                     </ContentTemplate>  
                <Triggers>  
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />  
                    <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />

                </Triggers>  
              </asp:UpdatePanel>  
                    </center>
                    
                </div>
                
                   <cc1:UpdatePanelAnimationExtender ID="upae" BehaviorID="animation" runat="server" TargetControlID="update">  
                     <Animations>  
         <OnUpdating>  
             <Sequence>  
                 
                 <ScriptAction Script="var b = $find('animation'); b._originalHeight = b._element.offsetHeight;" />  
                   
                
                 <Parallel duration="0">  
                     <EnableAction AnimationTarget="btnBuscar" Enabled="false" />  
                     
                     
                    
                 </Parallel>  
                 <StyleAction Attribute="overflow" Value="hidden" />  
                   
                
                 <Parallel duration=".25" Fps="30">  
                     
                         <%--<FadeOut AnimationTarget="up_container" minimumOpacity=".2" />  --%>
                       
                      
 
                 </Parallel>  
             </Sequence>  
         </OnUpdating>  
         <OnUpdated>  
             <Sequence>  
                
                 <Parallel duration=".25" Fps="30">  
                     
                         <%--<FadeIn AnimationTarget="up_container" minimumOpacity=".2" />  --%>
                     
                       
                     
                 </Parallel>  
                   
              
                 <Parallel duration="0">  
                     
                     <EnableAction AnimationTarget="btnBuscar" Enabled="true" />
                      
                     
                     
                 </Parallel>                              
             </Sequence>  
         </OnUpdated>  
     </Animations>  
</cc1:UpdatePanelAnimationExtender>
                </center>
        

       


    <script type="text/javascript">
        function enter(e) 
        {
            if (_enter) 
            {
                if (event.keyCode==13)
                {
                    event.keyCode=9; 
                    return event.keyCode;
                }    
            }
        }
        function inicio()
        {
            document.getElementById("txtClaveCT").focus();
        }    
        function Num(evt) {
            var nav4 = window.Event ? true : false;
	        var key = nav4 ? evt.which : evt.keyCode;
            return (key <= 13 || (key >= 48 && key <= 57));
        }
    
        var _enter=true;
        function Vent911(niv,subniv,cnt,cic)
        {
            var v911 = false;
            try{
//               v911 = window.open ("", "ventana911", "width=1,height=1,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
//               v911.close();   
                //v911 = window.open(Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
                var Pagina="";
                if(niv==41 && subniv==40){	//Inicial Escolarizado Cuestionario 15
                    Pagina="EI_1/Identificacion_EI_1.aspx";
                }
                if(niv==11 && subniv==1){	//911_2	Preescolar General 19
                    Pagina="911_1/Identificacion_911_1.aspx";
                }
                if(niv==11 && subniv==3){	//ECC_21	Preescolar CONAFE 20
                    Pagina="ECC_11/Identificacion_ECC_11.aspx";
                }
                if(niv==12 && subniv==4){	//911_4	Primaria General 22
                    Pagina="911_3/Identificacion_911_3.aspx";
                }
                if(niv==12 && subniv==6){	//ECC_22	Primaria CONAFE 23
                    Pagina="ECC_12/Identificacion_ECC_12.aspx";
                }
                if(niv==13 && subniv==7){	//911_6	Secundaria 25
                    Pagina="911_5/Identificacion_911_5.aspx";
                }
                if(niv==13 && subniv==8){	//911_6	Secundaria 2 25
                    Pagina="911_5/Identificacion_911_5.aspx";
                }
                if(niv==13 && subniv==9){	//911_6	Secundaria 3 25
                    Pagina="911_5/Identificacion_911_5.aspx";
                }
                if(niv==13 && subniv==10){	//911_6	Telesecundaria 25
                    Pagina="911_5/Identificacion_911_5.aspx";
                }
                if(niv==13 && subniv==11){	//911_6	Secundaria 5 25
                    Pagina="911_5/Identificacion_911_5.aspx";
                }
                if(niv==22 && subniv==22){	//911_8G	Bachillerato General 26
                    Pagina="911_7G/Identificacion_911_7G.aspx";
                }
                if(niv==22 && subniv==23){	//911_8T	Bachillerato Tecnologico 27
                    Pagina="911_7T/Identificacion_911_7T.aspx";
                }
                if(niv==21){                //911_8P	Profesional T�cnico 28
                    Pagina="911_7P/Identificacion_911_7P.aspx";
                }
                if(niv==51 && subniv==50){	//CAM_2	CAM Educacion Especial 18
                    Pagina="CAM_1/Identificacion_CAM_1.aspx";
                }
                if(niv==51 && subniv==51){	//USAER_2	USAER Unidad de Servicios de Apoyo a la Educaci�n Regular 17
                    Pagina="911_USAER_1/Identificacion_911_USAER_1.aspx";
                }
                if(niv==42 && subniv==42){	//EI_NE2	Inicial NO Escolarizada 16
                    Pagina="EI_NE1/Identificacion_EI_NE1.aspx";
                }
                Pagina=Pagina + "?cnt=" + cnt + "&cic=" + cic;
                window.open(Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
               
                //v911.location=Pagina;
                //v911.focus();
              
            }
            catch(err)
            {
                alert(err);
            } 
           
          //  v911 = window.open (Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
            //v911 = window.open (Pagina, "ventana911", "");
        }
        
        //v911 = window.open ("", "ventana911", "width=1,height=1,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
     

    </script>
</asp:Content>
