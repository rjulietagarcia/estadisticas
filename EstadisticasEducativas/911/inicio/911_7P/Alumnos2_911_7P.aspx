<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7G(Alumnos por edad)" AutoEventWireup="true" CodeBehind="Alumnos2_911_7P.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7P.Alumnos2_911_7P" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
   <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" Text="PROFESIONAL T�CNICO MEDIO"  Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table>
    </div></div>
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7P',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Carreras_911_7P',true)"><a href="#" title=""><span>ALUMNOS POR CARRERA</span></a></li><li onclick="openPage('Alumnos1_911_7P',true)"><a href="#" title=""><span>TOTAL</span></a></li><li onclick="openPage('Alumnos2_911_7P',true)"><a href="#" title="" class="activo"><span>ALUMNOS POR EDAD</span></a></li><li onclick="openPage('Egresados_911_7P',false)"><a href="#" title="" ><span>EGRESADOS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PROCEIES</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
   <br /><br /><br />
            <asp:Panel ID="pnlOficializado" runat="server"  CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
          
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq" style="width: 1px"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq">&nbsp; </td>
				    <td>
            <%--a aqui--%>


        &nbsp;
        <table>
            <tr>
                <td colspan="16" style="text-align:justify">
                    <asp:Label ID="lblALUMNOSPOREDAD" runat="server" CssClass="lblGriTit" 
                        Font-Bold="True" Font-Size="16px"
                        Text="II. ALUMNOS DE NUEVO INGRESO Y REPETIDORES, POR EDAD" ></asp:Label></td>
                <td colspan="1">
                </td>
            </tr>
            <tr>
                <td colspan="16" style="text-align:justify">
                    <asp:Label ID="lblInstruccion1" runat="server"   CssClass="lblRojo" Font-Bold="True" Text="1. Escriba el total de alumnos inscritos a partir de la fecha de inicio de cursos, sumando las altas y restando las bajas hasta el 30 de septiembre, y desgl�selo por grado, sexo, nuevo ingreso, repetidores y edad. Verifique que la suma de los subtotales de alumnos por edad sea igual al total."
                        Width="1200px"></asp:Label></td>
                <td colspan="1">
                </td>
            </tr>
            <tr>
                <td colspan="16" style="text-align: left; ; padding-top: 10px;">
                    <asp:Label ID="lbltabla" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="Nota: No utilice las �reas sombreadas."></asp:Label></td>
                <td colspan="1" style="padding-top: 10px; text-align: left">
                </td>
            </tr>
            <tr>
                <td rowspan="1">
                </td>
                <td rowspan="1">
                </td>
                <td>
                </td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl14a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="14 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl15a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="15 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl16a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="16 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl17a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl18a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl19a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl20a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl21a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl22a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl23a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lbl24a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        ></asp:Label></td>
                <td style="text-align: center; width: 72px;" class="linaBajoAlto">
                    <asp:Label ID="lbl25a" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="25 a�os y mas" Width="68px"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTotal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="5" style="text-align: center" bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1o. (sem. 1 y 2)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2" bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lblHom1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        ></asp:Label></td>
                <td bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcion1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV52" runat="server" Columns="4" MaxLength="4" TabIndex="10501" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV53" runat="server" Columns="4" MaxLength="4" TabIndex="10502" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV54" runat="server" Columns="4" MaxLength="4" TabIndex="10503" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV55" runat="server" Columns="4" MaxLength="4" TabIndex="10504" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV56" runat="server" Columns="4" MaxLength="4" TabIndex="10505" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV57" runat="server" Columns="4" MaxLength="4" TabIndex="10506" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV58" runat="server" Columns="4" MaxLength="4" TabIndex="10507" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV59" runat="server" Columns="4" MaxLength="4" TabIndex="10508" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV60" runat="server" Columns="4" MaxLength="4" TabIndex="10509" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV61" runat="server" Columns="4" MaxLength="4" TabIndex="10510" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV62" runat="server" Columns="4" MaxLength="4" TabIndex="10511" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV63" runat="server" Columns="4" MaxLength="4" TabIndex="10512" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV64" runat="server" Columns="4" MaxLength="4" TabIndex="10513" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="1" style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblExistencia1oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtVDis1" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV65" runat="server" Columns="4" MaxLength="4" TabIndex="10601" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV66" runat="server" Columns="4" MaxLength="4" TabIndex="10602" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV67" runat="server" Columns="4" MaxLength="4" TabIndex="10603" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV68" runat="server" Columns="4" MaxLength="4" TabIndex="10604" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV69" runat="server" Columns="4" MaxLength="4" TabIndex="10605" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV70" runat="server" Columns="4" MaxLength="4" TabIndex="10606" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV71" runat="server" Columns="4" MaxLength="4" TabIndex="10607" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV72" runat="server" Columns="4" MaxLength="4" TabIndex="10608" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV73" runat="server" Columns="4" MaxLength="4" TabIndex="10609" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV74" runat="server" Columns="4" MaxLength="4" TabIndex="10610" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV75" runat="server" Columns="4" MaxLength="4" TabIndex="10611" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV76" runat="server" Columns="4" MaxLength="4" TabIndex="10612" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblMuj1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        ></asp:Label></td>
                <td bordercolor="#00ab00" class="linaBajo">
                    <asp:Label ID="lblInscripcion1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV77" runat="server" Columns="4" MaxLength="4" TabIndex="10701" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV78" runat="server" Columns="4" MaxLength="4" TabIndex="10702" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV79" runat="server" Columns="4" MaxLength="4" TabIndex="10703" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV80" runat="server" Columns="4" MaxLength="4" TabIndex="10704" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV81" runat="server" Columns="4" MaxLength="4" TabIndex="10705" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV82" runat="server" Columns="4" MaxLength="4" TabIndex="10706" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV83" runat="server" Columns="4" MaxLength="4" TabIndex="10707" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV84" runat="server" Columns="4" MaxLength="4" TabIndex="10708" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV85" runat="server" Columns="4" MaxLength="4" TabIndex="10709" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV86" runat="server" Columns="4" MaxLength="4" TabIndex="10710" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV87" runat="server" Columns="4" MaxLength="4" TabIndex="10711" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV88" runat="server" Columns="4" MaxLength="4" TabIndex="10712" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV89" runat="server" Columns="4" MaxLength="4" TabIndex="10713" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblExistencia1oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtVDis2" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV90" runat="server" Columns="4" MaxLength="4" TabIndex="10801" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV91" runat="server" Columns="4" MaxLength="4" TabIndex="10802" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV92" runat="server" Columns="4" MaxLength="4" TabIndex="10803" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV93" runat="server" Columns="4" MaxLength="4" TabIndex="10804" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV94" runat="server" Columns="4"  MaxLength="4" TabIndex="10805" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV95" runat="server" Columns="4" MaxLength="4" TabIndex="10806" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV96" runat="server" Columns="4" MaxLength="4" TabIndex="10807" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV97" runat="server" Columns="4" MaxLength="4" TabIndex="10808" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV98" runat="server" Columns="4" MaxLength="4" TabIndex="10809" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV99" runat="server" Columns="4" MaxLength="4" TabIndex="10810" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV100" runat="server" Columns="4" MaxLength="4" TabIndex="10811" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV101" runat="server" Columns="4" MaxLength="4" TabIndex="10812" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="" class="linaBajoS">
                    <asp:Label ID="lblAprobados1oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV102" runat="server" Columns="4" MaxLength="4" TabIndex="10901" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV103" runat="server" Columns="4" MaxLength="4" TabIndex="10902" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV104" runat="server" Columns="4" MaxLength="4" TabIndex="10903" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV105" runat="server" Columns="4" MaxLength="4" TabIndex="10904" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV106" runat="server" Columns="4" MaxLength="4" TabIndex="10905" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV107" runat="server" Columns="4" MaxLength="4" TabIndex="10906" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV108" runat="server" Columns="4" MaxLength="4" TabIndex="10907" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV109" runat="server" Columns="4" MaxLength="4" TabIndex="10908" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV110" runat="server" Columns="4" MaxLength="4" TabIndex="10909" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV111" runat="server" Columns="4" MaxLength="4" TabIndex="10910" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV112" runat="server" Columns="4" MaxLength="4" TabIndex="10911" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV113" runat="server" Columns="4" MaxLength="4" TabIndex="10912" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV114" runat="server" Columns="4" MaxLength="4" TabIndex="10913" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
                        <tr>
                <td rowspan="1">
                </td>
                <td rowspan="1">
                </td>
                <td>
                </td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="14 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="15 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="16 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label13" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label14" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label19" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        ></asp:Label></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:Label ID="Label20" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="25 a�os y mas"
                        Width="68px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label21" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="5" bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2o. (sem. 3 y 4)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2" style="text-align: center" bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lblHom2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcion2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtVDis21" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV115" runat="server" Columns="4" MaxLength="4" TabIndex="11101" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV116" runat="server" Columns="4" MaxLength="4" TabIndex="11102" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV117" runat="server" Columns="4" MaxLength="4" TabIndex="11103" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV118" runat="server" Columns="4" MaxLength="4" TabIndex="11104" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV119" runat="server" Columns="4" MaxLength="4" TabIndex="11105" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV120" runat="server" Columns="4" MaxLength="4" TabIndex="11106" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV121" runat="server" Columns="4" MaxLength="4" TabIndex="11107" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV122" runat="server" Columns="4" MaxLength="4" TabIndex="11108" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV123" runat="server" Columns="4" MaxLength="4" TabIndex="11109" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV124" runat="server" Columns="4" MaxLength="4" TabIndex="11110" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV125" runat="server" Columns="4" MaxLength="4" TabIndex="11111" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV126" runat="server" Columns="4" MaxLength="4" TabIndex="11112" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblExistencia2oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtVDis22" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV127" runat="server" Columns="4" MaxLength="4" TabIndex="11201" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV128" runat="server" Columns="4" MaxLength="4" TabIndex="11202" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV129" runat="server" Columns="4" MaxLength="4" TabIndex="11203" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV130" runat="server" Columns="4" MaxLength="4" TabIndex="11204" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV131" runat="server" Columns="4" MaxLength="4" TabIndex="11205" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV132" runat="server" Columns="4" MaxLength="4" TabIndex="11206" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV133" runat="server" Columns="4" MaxLength="4" TabIndex="11207" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV134" runat="server" Columns="4" MaxLength="4" TabIndex="11208" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV135" runat="server" Columns="4" MaxLength="4" TabIndex="11209" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV136" runat="server" Columns="4" MaxLength="4" TabIndex="11210" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV137" runat="server" Columns="4" MaxLength="4" TabIndex="11211" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV138" runat="server" Columns="4" MaxLength="4" TabIndex="11212" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblMuj2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        ></asp:Label></td>
                <td bordercolor="#00ab00">
                    <asp:Label ID="lblInscripcion2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtVDis23" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV139" runat="server" Columns="4" MaxLength="4" TabIndex="11301" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV140" runat="server" Columns="4" MaxLength="4" TabIndex="11302" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV141" runat="server" Columns="4" MaxLength="4" TabIndex="11303" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV142" runat="server" Columns="4" MaxLength="4" TabIndex="11304" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV143" runat="server" Columns="4" MaxLength="4" TabIndex="11305" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV144" runat="server" Columns="4" MaxLength="4" TabIndex="11306" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV145" runat="server" Columns="4" MaxLength="4" TabIndex="11307" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV146" runat="server" Columns="4" MaxLength="4" TabIndex="11308" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV147" runat="server" Columns="4" MaxLength="4" TabIndex="11309" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV148" runat="server" Columns="4" MaxLength="4" TabIndex="11310" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV149" runat="server" Columns="4" MaxLength="4" TabIndex="11311" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV150" runat="server" Columns="4" MaxLength="4" TabIndex="11312" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
             <tr>
                <td bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblExistencia2oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtVDis24" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV151" runat="server" Columns="4" MaxLength="4" TabIndex="11401" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV152" runat="server" Columns="4" MaxLength="4" TabIndex="11402" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV153" runat="server" Columns="4" MaxLength="4" TabIndex="11403" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV154" runat="server" Columns="4" MaxLength="4" TabIndex="11404" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV155" runat="server" Columns="4" MaxLength="4" TabIndex="11405" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV156" runat="server" Columns="4" MaxLength="4" TabIndex="11406" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV157" runat="server" Columns="4" MaxLength="4" TabIndex="11407" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV158" runat="server" Columns="4" MaxLength="4" TabIndex="11408" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV159" runat="server" Columns="4" MaxLength="4" TabIndex="11409" ></asp:TextBox></td>
                 <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV160" runat="server" Columns="4" MaxLength="4" TabIndex="11410" ></asp:TextBox></td>
                 <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV161" runat="server" Columns="4" MaxLength="4" TabIndex="11411" ></asp:TextBox></td>
                 <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV162" runat="server" Columns="4" MaxLength="4" TabIndex="11412" ></asp:TextBox></td>
                 <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr> 
             <tr>
                <td colspan="2" style="" class="linaBajoS">
                    <asp:Label ID="lblAprobados2oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis25" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV163" runat="server" Columns="4" MaxLength="4" TabIndex="11501" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV164" runat="server" Columns="4" MaxLength="4" TabIndex="11502" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV165" runat="server" Columns="4" MaxLength="4" TabIndex="11503" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV166" runat="server" Columns="4" MaxLength="4" TabIndex="11504" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV167" runat="server" Columns="4" MaxLength="4" TabIndex="11505" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV168" runat="server" Columns="4" MaxLength="4" TabIndex="11506" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV169" runat="server" Columns="4" MaxLength="4" TabIndex="11507" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV170" runat="server" Columns="4" MaxLength="4" TabIndex="11508" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV171" runat="server" Columns="4" MaxLength="4" TabIndex="11509" ></asp:TextBox></td>
                 <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV172" runat="server" Columns="4" MaxLength="4" TabIndex="11510" ></asp:TextBox></td>
                 <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV173" runat="server" Columns="4" MaxLength="4" TabIndex="11511" ></asp:TextBox></td>
                 <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV174" runat="server" Columns="4" MaxLength="4" TabIndex="11512" ></asp:TextBox></td>
                 <td class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
                        <tr>
                <td rowspan="1">
                </td>
                <td rowspan="1">
                </td>
                <td>
                </td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label22" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="14 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label23" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="15 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="16 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label28" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label29" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label30" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label31" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label32" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        ></asp:Label></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:Label ID="Label74" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="25 a�os y mas"
                        Width="68px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label34" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
            </tr>

             <tr>
                 <td rowspan="5" bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3o. (sem. 5 y 6)"
                        Width="40px"></asp:Label></td>
                 <td rowspan="2" bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lblHom3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        ></asp:Label></td>
                <td bordercolor="#00ab00" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcion3oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtVDis31" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtVDis32" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV175" runat="server" Columns="4" MaxLength="4" TabIndex="11701" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV176" runat="server" Columns="4" MaxLength="4" TabIndex="11702" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV177" runat="server" Columns="4" MaxLength="4" TabIndex="11703" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV178" runat="server" Columns="4" MaxLength="4" TabIndex="11704" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV179" runat="server" Columns="4" MaxLength="4" TabIndex="11705" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV180" runat="server" Columns="4" MaxLength="4" TabIndex="11706" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV181" runat="server" Columns="4" MaxLength="4" TabIndex="11707" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV182" runat="server" Columns="4" MaxLength="4" TabIndex="11708" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV183" runat="server" Columns="4" MaxLength="4" TabIndex="11709" ></asp:TextBox></td>
                 <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV184" runat="server" Columns="4" MaxLength="4" TabIndex="11710" ></asp:TextBox></td>
                 <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV185" runat="server" Columns="4" MaxLength="4" TabIndex="11711" ></asp:TextBox></td>
                 <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
             <tr>
                <td bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblExistencia3oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                 <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtVDis33" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtVDis34" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV186" runat="server" Columns="4" MaxLength="4" TabIndex="11801" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV187" runat="server" Columns="4" MaxLength="4" TabIndex="11802" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV188" runat="server" Columns="4" MaxLength="4" TabIndex="11803" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV189" runat="server" Columns="4" MaxLength="4" TabIndex="11804" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV190" runat="server" Columns="4" MaxLength="4" TabIndex="11805" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV191" runat="server" Columns="4" MaxLength="4" TabIndex="11806" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV192" runat="server" Columns="4" MaxLength="4" TabIndex="11807" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV193" runat="server" Columns="4" MaxLength="4" TabIndex="11808" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV194" runat="server" Columns="4" MaxLength="4" TabIndex="11809" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV195" runat="server" Columns="4" MaxLength="4" TabIndex="11810" ></asp:TextBox></td>
                 <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV196" runat="server" Columns="4" MaxLength="4" TabIndex="11811" ></asp:TextBox></td>
                 <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                     &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblMuj3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        ></asp:Label></td>
                <td bordercolor="#00ab00">
                    <asp:Label ID="lblInscripcion3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtVDis35" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtVDis36" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV197" runat="server" Columns="4" MaxLength="4" TabIndex="11901" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV198" runat="server" Columns="4" MaxLength="4" TabIndex="11902" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV199" runat="server" Columns="4" MaxLength="4" TabIndex="11903" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV200" runat="server" Columns="4" MaxLength="4" TabIndex="11904" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV201" runat="server" Columns="4" MaxLength="4" TabIndex="11905" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV202" runat="server" Columns="4" MaxLength="4" TabIndex="11906" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV203" runat="server" Columns="4" MaxLength="4" TabIndex="11907" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV204" runat="server" Columns="4" MaxLength="4" TabIndex="11908" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV205" runat="server" Columns="4" MaxLength="4" TabIndex="11909" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV206" runat="server" Columns="4" MaxLength="4" TabIndex="11910" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajo">
                    <asp:TextBox ID="txtV207" runat="server" Columns="4" MaxLength="4" TabIndex="11911" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:Label ID="lblExistencia3oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtVDis37" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtVDis38" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV208" runat="server" Columns="4" MaxLength="4" TabIndex="12001" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV209" runat="server" Columns="4" MaxLength="4" TabIndex="12002" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV210" runat="server" Columns="4" MaxLength="4" TabIndex="12003" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV211" runat="server" Columns="4" MaxLength="4" TabIndex="12004" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV212" runat="server" Columns="4" MaxLength="4" TabIndex="12005" ></asp:TextBox></td>
                <td style="text-align: center;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV213" runat="server" Columns="4" MaxLength="4" TabIndex="12006" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV214" runat="server" Columns="4" MaxLength="4" TabIndex="12007" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV215" runat="server" Columns="4" MaxLength="4" TabIndex="12008" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV216" runat="server" Columns="4" MaxLength="4" TabIndex="12009" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV217" runat="server" Columns="4" MaxLength="4" TabIndex="12010" ></asp:TextBox></td>
                <td style="text-align: center" bordercolor="#00ab00" class="linaBajoS">
                    <asp:TextBox ID="txtV218" runat="server" Columns="4" MaxLength="4" TabIndex="12011" ></asp:TextBox></td>
                <td bordercolor="#00ab00" class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="" class="linaBajoS">
                    <asp:Label ID="lblAprobados3oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis39" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis40" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV219" runat="server" Columns="4" MaxLength="4" TabIndex="12101" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV220" runat="server" Columns="4" MaxLength="4" TabIndex="12102" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV221" runat="server" Columns="4" MaxLength="4" TabIndex="12103" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV222" runat="server" Columns="4" MaxLength="4" TabIndex="12104" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV223" runat="server" Columns="4" MaxLength="4" TabIndex="12105" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV224" runat="server" Columns="4" MaxLength="4" TabIndex="12106" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV225" runat="server" Columns="4" MaxLength="4" TabIndex="12107" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV226" runat="server" Columns="4" MaxLength="4" TabIndex="12108" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV227" runat="server" Columns="4" MaxLength="4" TabIndex="12109" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV228" runat="server" Columns="4" MaxLength="4" TabIndex="12110" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV229" runat="server" Columns="4" MaxLength="4" TabIndex="12111" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
                        <tr>
                <td rowspan="1">
                </td>
                <td rowspan="1">
                </td>
                <td>
                </td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label35" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="14 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label36" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="15 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label37" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="16 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label38" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label39" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label40" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label41" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label42" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label43" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label44" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label45" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        ></asp:Label></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    &nbsp;<asp:Label ID="Label33" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="25 a�os y mas" Width="68px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label47" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
            </tr>

            <tr>
                <td rowspan="5" class="linaBajoAlto">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4o. (sem. 7 y 8)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblHom4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblInscripcion4oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis41" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis42" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis43" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV230" runat="server" Columns="4" MaxLength="4" TabIndex="12301" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV231" runat="server" Columns="4" MaxLength="4" TabIndex="12302" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV232" runat="server" Columns="4" MaxLength="4" TabIndex="12303" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV233" runat="server" Columns="4" MaxLength="4" TabIndex="12304" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV234" runat="server" Columns="4" MaxLength="4" TabIndex="12305" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV235" runat="server" Columns="4" MaxLength="4" TabIndex="12306" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV236" runat="server" Columns="4" MaxLength="4" TabIndex="12307" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV237" runat="server" Columns="4" MaxLength="4" TabIndex="12308" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:TextBox ID="txtV238" runat="server" Columns="4" MaxLength="4" TabIndex="12309" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV239" runat="server" Columns="4" MaxLength="4" TabIndex="12310" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblExistencia4oH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis44" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis45" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis46" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV240" runat="server" Columns="4" MaxLength="4" TabIndex="12401" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV241" runat="server" Columns="4" MaxLength="4" TabIndex="12402" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV242" runat="server" Columns="4" MaxLength="4" TabIndex="12403" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV243" runat="server" Columns="4" MaxLength="4" TabIndex="12404" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV244" runat="server" Columns="4" MaxLength="4" TabIndex="12405" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV245" runat="server" Columns="4" MaxLength="4" TabIndex="12406" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV246" runat="server" Columns="4" MaxLength="4" TabIndex="12407" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV247" runat="server" Columns="4" MaxLength="4" TabIndex="12408" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV248" runat="server" Columns="4" MaxLength="4" TabIndex="12409" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV249" runat="server" Columns="4" MaxLength="4" TabIndex="12410" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center" class="linaBajoS">
                    <asp:Label ID="lblMuj4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        ></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblInscripcion4oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis47" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis48" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis49" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV250" runat="server" Columns="4" MaxLength="4" TabIndex="12501" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV251" runat="server" Columns="4" MaxLength="4" TabIndex="12502" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV252" runat="server" Columns="4" MaxLength="4" TabIndex="12503" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV253" runat="server" Columns="4" MaxLength="4" TabIndex="12504" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV254" runat="server" Columns="4" MaxLength="4" TabIndex="12505" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV255" runat="server" Columns="4" MaxLength="4" TabIndex="12506" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV256" runat="server" Columns="4" MaxLength="4" TabIndex="12507" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV257" runat="server" Columns="4" MaxLength="4" TabIndex="12508" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:TextBox ID="txtV258" runat="server" Columns="4" MaxLength="4" TabIndex="12509" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV259" runat="server" Columns="4" MaxLength="4" TabIndex="12510" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblExistencia4oM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis50" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis51" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis52" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV260" runat="server" Columns="4" MaxLength="4" TabIndex="12601" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV261" runat="server" Columns="4" MaxLength="4" TabIndex="12602" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV262" runat="server" Columns="4" MaxLength="4" TabIndex="12603" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV263" runat="server" Columns="4" MaxLength="4" TabIndex="12604" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV264" runat="server" Columns="4" MaxLength="4" TabIndex="12605" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV265" runat="server" Columns="4" MaxLength="4" TabIndex="12606" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV266" runat="server" Columns="4" MaxLength="4" TabIndex="12607" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV267" runat="server" Columns="4" MaxLength="4" TabIndex="12608" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV268" runat="server" Columns="4" MaxLength="4" TabIndex="12609" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV269" runat="server" Columns="4" MaxLength="4" TabIndex="12610" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="" class="linaBajoS">
                    <asp:Label ID="lblAprobados4oM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis53" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis54" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis55" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV270" runat="server" Columns="4" MaxLength="4" TabIndex="12701" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV271" runat="server" Columns="4" MaxLength="4" TabIndex="12702" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV272" runat="server" Columns="4" MaxLength="4" TabIndex="12703" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV273" runat="server" Columns="4" MaxLength="4" TabIndex="12704" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV274" runat="server" Columns="4" MaxLength="4" TabIndex="12705" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV275" runat="server" Columns="4" MaxLength="4" TabIndex="12706" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV276" runat="server" Columns="4" MaxLength="4" TabIndex="12707" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV277" runat="server" Columns="4" MaxLength="4" TabIndex="12708" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV278" runat="server" Columns="4" MaxLength="4" TabIndex="12709" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV279" runat="server" Columns="4" MaxLength="4" TabIndex="12710" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
                        <tr>
                <td rowspan="1">
                </td>
                <td rowspan="1">
                </td>
                <td>
                </td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label48" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="14 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label49" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="15 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label50" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="16 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label51" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label52" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label53" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label54" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label55" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label56" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label57" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label58" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        ></asp:Label></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:Label ID="Label46" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="25 a�os y mas"
                        Width="68px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label60" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
            </tr>

            <tr>
                <td rowspan="5" class="linaBajoAlto">
                    <asp:Label ID="Label8" runat="server" CssClass="lblRojo" Font-Bold="True" Text="5o. (sem. 9 y 10)"
                        Width="40px"></asp:Label></td>
                <td rowspan="2" class="linaBajoAlto">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        ></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis60" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis61" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis62" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis63" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV280" runat="server" Columns="4" MaxLength="4" TabIndex="12901" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV281" runat="server" Columns="4" MaxLength="4" TabIndex="12902" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV282" runat="server" Columns="4" MaxLength="4" TabIndex="12903" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV283" runat="server" Columns="4" MaxLength="4" TabIndex="12904" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV284" runat="server" Columns="4" MaxLength="4" TabIndex="12905" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV285" runat="server" Columns="4" MaxLength="4" TabIndex="12906" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV286" runat="server" Columns="4" MaxLength="4" TabIndex="12907" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:TextBox ID="txtV287" runat="server" Columns="4" MaxLength="4" TabIndex="12908" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV288" runat="server" Columns="4" MaxLength="4" TabIndex="12909" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis64" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis65" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis66" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis67" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV289" runat="server" Columns="4" MaxLength="4" TabIndex="13001" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV290" runat="server" Columns="4" MaxLength="4" TabIndex="13002" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV291" runat="server" Columns="4" MaxLength="4" TabIndex="13003" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV292" runat="server" Columns="4" MaxLength="4" TabIndex="13004" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV293" runat="server" Columns="4" MaxLength="4" TabIndex="13005" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV294" runat="server" Columns="4" MaxLength="4" TabIndex="13006" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV295" runat="server" Columns="4" MaxLength="4" TabIndex="13007" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV296" runat="server" Columns="4" MaxLength="4" TabIndex="13008" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV297" runat="server" Columns="4" MaxLength="4" TabIndex="13009" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" class="linaBajoS">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        ></asp:Label></td>
                <td rowspan="1" class="linaBajo">
                    <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis68" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis69" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis70" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtVDis71" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV298" runat="server" Columns="4" MaxLength="4" TabIndex="13101" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV299" runat="server" Columns="4" MaxLength="4" TabIndex="13102" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV300" runat="server" Columns="4" MaxLength="4" TabIndex="13103" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV301" runat="server" Columns="4" MaxLength="4" TabIndex="13104" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV302" runat="server" Columns="4" MaxLength="4" TabIndex="13105" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV303" runat="server" Columns="4" MaxLength="4" TabIndex="13106" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV304" runat="server" Columns="4" MaxLength="4" TabIndex="13107" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:TextBox ID="txtV305" runat="server" Columns="4" MaxLength="4" TabIndex="13108" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV306" runat="server" Columns="4" MaxLength="4" TabIndex="13109" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="1" class="linaBajoS">
                    <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td rowspan="1" style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis72" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis73" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis74" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis75" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV307" runat="server" Columns="4" MaxLength="4" TabIndex="13201" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV308" runat="server" Columns="4" MaxLength="4" TabIndex="13202" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV309" runat="server" Columns="4" MaxLength="4" TabIndex="13203" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV310" runat="server" Columns="4" MaxLength="4" TabIndex="13204" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV311" runat="server" Columns="4" MaxLength="4" TabIndex="13205" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV312" runat="server" Columns="4" MaxLength="4" TabIndex="13206" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV313" runat="server" Columns="4" MaxLength="4" TabIndex="13207" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV314" runat="server" Columns="4" MaxLength="4" TabIndex="13208" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV315" runat="server" Columns="4" MaxLength="4" TabIndex="13209" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" rowspan="1" style="" class="linaBajoS">
                    <asp:Label ID="Label7" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SUBTOTAL"
                        ></asp:Label></td>
                <td rowspan="1" style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis76" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis77" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis78" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis79" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV316" runat="server" Columns="4" MaxLength="4" TabIndex="13301" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV317" runat="server" Columns="4" MaxLength="4" TabIndex="13302" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV318" runat="server" Columns="4" MaxLength="4" TabIndex="13303" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV319" runat="server" Columns="4" MaxLength="4" TabIndex="13304" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV320" runat="server" Columns="4" MaxLength="4" TabIndex="13305" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV321" runat="server" Columns="4" MaxLength="4" TabIndex="13306" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV322" runat="server" Columns="4" MaxLength="4" TabIndex="13307" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV323" runat="server" Columns="4" MaxLength="4" TabIndex="13308" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV324" runat="server" Columns="4" MaxLength="4" TabIndex="13309" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
                        <tr>
                <td rowspan="1">
                </td>
                <td rowspan="1">
                </td>
                <td>
                </td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label61" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="14 a�os y menos"
                        Width="50px"></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label62" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="15 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label63" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="16 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label64" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="17 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label65" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="18 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label66" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="19 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label67" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="20 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label68" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="21 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label69" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="22 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label70" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="23 a�os"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label71" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="24 a�os"
                        ></asp:Label></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:Label ID="Label72" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="25 a�os y mas"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:Label ID="Label73" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                            <td class="Orila" style="text-align: center">
                                &nbsp;</td>
            </tr>

            <tr>
                <td rowspan="5" style="text-align: center" class="linaBajoAlto">
                    <asp:Label ID="lblTotalT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                <td rowspan="2" class="linaBajoAlto">
                    <asp:Label ID="lblHomT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        ></asp:Label></td>
                <td class="linaBajoAlto">
                    <asp:Label ID="lblInscripcionTH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV325" runat="server" Columns="4" MaxLength="4" TabIndex="13501" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV326" runat="server" Columns="4" MaxLength="4" TabIndex="13502" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV327" runat="server" Columns="4" MaxLength="4" TabIndex="13503" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV328" runat="server" Columns="4" MaxLength="4" TabIndex="13504" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV329" runat="server" Columns="4" MaxLength="4" TabIndex="13505" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV330" runat="server" Columns="4" MaxLength="4" TabIndex="13506" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV331" runat="server" Columns="4" MaxLength="4" TabIndex="13507" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV332" runat="server" Columns="4" MaxLength="4" TabIndex="13508" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV333" runat="server" Columns="4" MaxLength="4" TabIndex="13509" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV334" runat="server" Columns="4" MaxLength="4" TabIndex="13510" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV335" runat="server" Columns="4" MaxLength="4" TabIndex="13511" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:TextBox ID="txtV336" runat="server" Columns="4" MaxLength="4" TabIndex="13512" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV337" runat="server" Columns="4" MaxLength="4" TabIndex="13513" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="linaBajoS">
                    <asp:Label ID="lblExistenciaTH" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtVDis80" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4"  ReadOnly="true"  MaxLength="4" Enabled="false" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV338" runat="server" Columns="4" MaxLength="4" TabIndex="13601" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV339" runat="server" Columns="4" MaxLength="4" TabIndex="13602" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV340" runat="server" Columns="4" MaxLength="4" TabIndex="13603" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV341" runat="server" Columns="4" MaxLength="4" TabIndex="13604" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV342" runat="server" Columns="4" MaxLength="4" TabIndex="13605" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV343" runat="server" Columns="4" MaxLength="4" TabIndex="13606" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV344" runat="server" Columns="4" MaxLength="4" TabIndex="13607" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV345" runat="server" Columns="4" MaxLength="4" TabIndex="13608" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV346" runat="server" Columns="4" MaxLength="4" TabIndex="13609" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV347" runat="server" Columns="4" MaxLength="4" TabIndex="13610" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV348" runat="server" Columns="4" MaxLength="4" TabIndex="13611" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV349" runat="server" Columns="4" MaxLength="4" TabIndex="13612" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td rowspan="2" class="linaBajoS">
                    <asp:Label ID="lblMujT" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        ></asp:Label></td>
                <td class="linaBajo">
                    <asp:Label ID="lblInscripcionTM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="NUEVO INGRESO"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV350" runat="server" Columns="4" MaxLength="4" TabIndex="13701" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV351" runat="server" Columns="4" MaxLength="4" TabIndex="13702" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV352" runat="server" Columns="4" MaxLength="4" TabIndex="13703" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV353" runat="server" Columns="4" MaxLength="4" TabIndex="13704" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV354" runat="server" Columns="4" MaxLength="4" TabIndex="13705" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV355" runat="server" Columns="4" MaxLength="4" TabIndex="13706" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV356" runat="server" Columns="4" MaxLength="4" TabIndex="13707" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV357" runat="server" Columns="4" MaxLength="4" TabIndex="13708" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV358" runat="server" Columns="4" MaxLength="4" TabIndex="13709" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV359" runat="server" Columns="4" MaxLength="4" TabIndex="13710" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajo">
                    <asp:TextBox ID="txtV360" runat="server" Columns="4" MaxLength="4" TabIndex="13711" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajo">
                    <asp:TextBox ID="txtV361" runat="server" Columns="4" MaxLength="4" TabIndex="13712" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajo">
                    <asp:TextBox ID="txtV362" runat="server" Columns="4" MaxLength="4" TabIndex="13713" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="height: 30px" class="linaBajoS">
                    <asp:Label ID="lblExistenciaTM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="REPETIDORES"
                        ></asp:Label></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtVDis81" runat="server" BackColor="Silver" BorderColor="Silver"
                        BorderStyle="Solid" Columns="4" ReadOnly="true" Enabled="false"  MaxLength="4" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV363" runat="server" Columns="4" MaxLength="4" TabIndex="13801" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV364" runat="server" Columns="4" MaxLength="4" TabIndex="13802" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV365" runat="server" Columns="4" MaxLength="4" TabIndex="13803" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV366" runat="server" Columns="4" MaxLength="4" TabIndex="13804" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV367" runat="server" Columns="4" MaxLength="4" TabIndex="13805" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV368" runat="server" Columns="4" MaxLength="4" TabIndex="13806" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV369" runat="server" Columns="4" MaxLength="4" TabIndex="13807" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV370" runat="server" Columns="4" MaxLength="4" TabIndex="13808" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV371" runat="server" Columns="4" MaxLength="4" TabIndex="13809" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV372" runat="server" Columns="4" MaxLength="4" TabIndex="13810" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV373" runat="server" Columns="4" MaxLength="4" TabIndex="13811" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV374" runat="server" Columns="4" MaxLength="4" TabIndex="13812" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="linaBajoS">
                    <asp:Label ID="lblAprobadosTM" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        ></asp:Label></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV375" runat="server" Columns="4" MaxLength="4" TabIndex="13901" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV376" runat="server" Columns="4" MaxLength="4" TabIndex="13902" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV377" runat="server" Columns="4" MaxLength="4" TabIndex="13903" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV378" runat="server" Columns="4" MaxLength="4" TabIndex="13904" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV379" runat="server" Columns="4" MaxLength="4" TabIndex="13905" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV380" runat="server" Columns="4" MaxLength="4" TabIndex="13906" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV381" runat="server" Columns="4" MaxLength="4" TabIndex="13907" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV382" runat="server" Columns="4" MaxLength="4" TabIndex="13908" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV383" runat="server" Columns="4" MaxLength="4" TabIndex="13909" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV384" runat="server" Columns="4" MaxLength="4" TabIndex="13910" ></asp:TextBox></td>
                <td style="text-align: center;" class="linaBajoS">
                    <asp:TextBox ID="txtV385" runat="server" Columns="4" MaxLength="4" TabIndex="13911" ></asp:TextBox></td>
                <td style="text-align: center; width: 72px;" class="linaBajoS">
                    <asp:TextBox ID="txtV386" runat="server" Columns="4" MaxLength="4" TabIndex="13912" ></asp:TextBox></td>
                <td style="text-align: center" class="linaBajoS">
                    <asp:TextBox ID="txtV387" runat="server" Columns="4" MaxLength="4" TabIndex="13913" ></asp:TextBox></td>
                <td class="Orila" style="text-align: center">
                    &nbsp;
                </td>
            </tr>
            
          </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Alumnos1_911_7P',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Alumnos1_911_7P',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Egresados_911_7P',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Egresados_911_7P',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>
           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq" ></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 14;
                MaxRow = 60;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
          
        </script> 
</asp:Content>
