<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7T(Total)" AutoEventWireup="true" CodeBehind="Alumnos1_911_7T.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7T.Alumnos1_911_7T" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   <script type="text/javascript">
        MaxCol = 15;
        MaxRow = 28;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>

    
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
    <table style="width:100%">
        
        <tr><td><span>BACHILLERATO TECNOL�GICO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
            <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title="" ><span>IDENTIFICACI�N</span></a></li>
            <li onclick="openPage('Carreras_911_7T',true)"><a href="#" title="" ><span>ALUMNOS POR CARRERA</span></a></li>
            <li onclick="openPage('Alumnos1_911_7T',true)"><a href="#" title="" class="activo"><span>TOTAL</span></a></li>
            <li onclick="openPage('Egresados_911_7T',false)"><a href="#" title="" ><span>EGRESADOS</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ALUMNOS POR EDAD</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PLANTELES</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 650px">
           
            
            <tr>
                <td style="text-align: center">
                    <table style="width: 600px">
                        <tr>
                            <td style="text-align: left">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="2. TOTAL DE ALUMNOS (Suma de las carreras o especialidades)."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                    <table>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblGrado" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRADO"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblSemestre" runat="server" CssClass="lblRojo" Font-Bold="True" Text="SEMESTRE"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl1o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1 Y 2"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV1" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV2" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV3" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl2o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl3y4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3 Y 4"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV4" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV5" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV6" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl3o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl5y6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="5 Y 6"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV7" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV8" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV9" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl4o" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o."
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="7 Y 8"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV10" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV11" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV12" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Label ID="lblTOTAL1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV13" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV14" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV15" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                    <asp:Label ID="lblInstruccion3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="3. Escriba el n�mero de alumnos de nacionalidad extranjera, deslos�ndolo por sexo."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblHombres3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="HOMBRES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblMujeres3" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:Label ID="lblTotal3a" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEU" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ESTADOS UNIDOS"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV16" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20101"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV17" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20102"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV18" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                        TabIndex="20103"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCANADA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CANAD�"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV19" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                        TabIndex="20201"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV20" runat="server" Columns="3" CssClass="lblNegro" MaxLength="3"
                        TabIndex="20202"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV21" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblCAYC" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CENTROAM�RICA Y EL CARIBE"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV22" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20301"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV23" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20302"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV24" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblSUDAM" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SUDAM�RICA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV25" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20401"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV26" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20402"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV27" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblAFRICA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="�FRICA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV28" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20501"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV29" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20502"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV30" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20503"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblASIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ASIA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV31" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20601"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV32" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20602"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV33" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20603"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblEUROPA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="EUROPA"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV34" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20701"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV35" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20702"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV36" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20703"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblOCEANIA" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="OCEAN�A"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV37" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20801"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV38" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20802" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV39" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20803" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTotal3b" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV40" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20901" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV41" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20902" ></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="txtV42" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="20903" ></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                    <asp:Label ID="lblInstruccion4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="4. Escriba, por grado, el n�mero de grupos existentes."
                        Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
        <table>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblGRUPOS" runat="server" CssClass="lblRojo" Font-Bold="True" Text="GRUPOS"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="1o. (1o. y 2o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV43" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30101" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem2y3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="2o. (3o. y 4o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV44" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30201" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem4y5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="3o. (5o. y 6o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV45" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30301" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSem7y8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="4o. (7o. y 8o. semestres)"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV46" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30401" ></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV47" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="30501" ></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Carreras_911_7T',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Carreras_911_7T',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;</td>
                <td ><span  onclick="openPage('Egresados_911_7T',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Egresados_911_7T',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />                
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		    Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
