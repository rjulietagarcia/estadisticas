<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="Aulas_911_7T.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_7T.Aulas_911_7T" Title="911.7T(Aulas)" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

  
   <script type="text/javascript">
        MaxCol = 18;
        MaxRow = 28;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
 
    
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
    <table style="width:100%">
        
        <tr><td><span>BACHILLERATO TECNOL�GICO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7T',true)"><a href="#" title="" ><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Carreras_911_7T',true)"><a href="#" title="" ><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('Alumnos1_911_7T',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
        <li onclick="openPage('Egresados_911_7T',true)"><a href="#" title="" ><span>EGRESADOS</span></a></li>
        <li onclick="openPage('Alumnos2_911_7T',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Planteles_911_7T',true)"><a href="#" title=""  ><span>PLANTELES</span></a></li>
        <li onclick="openPage('Personal_911_7T',true)"><a href="#" title="" ><span>PERSONAL</span></a></li>
        <li onclick="openPage('Aulas_911_7T',true)"><a href="#" title="" class="activo"><span>AULAS</span></a></li>
        <li onclick="openPage('Gasto_911_7T',false)"><a href="#" title="" ><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
  
<center>  
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

<table>
    <tr>
                            <td style="padding-bottom: 10px; padding-top: 10px; text-align: left">
                                <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                                    Text="VI. AULAS" Width="100%"></asp:Label></td>
                        </tr>
    <tr>
        <td style="text-align: left">
            <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. Escriba el n�mero de aulas por grado y tipo, y el n�mero de talleres y laboratorios existentes en el plantel."
                Width="100%"></asp:Label></td>
    </tr>
    <tr>
        <td style="padding-bottom: 10px; padding-top: 10px; text-align: left">
            <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="Nota:"
                Width="100%"></asp:Label>
            <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="a) El reporte de aulas debe ser por turno."
                Width="100%"></asp:Label>
            <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="b) Si un aula se utiliza para dar clases a m�s de un grado, an�tela en el rubro correspondiente."
                Width="100%"></asp:Label></td>
    </tr>
    <tr>
        <td style="text-align: center"><table>
            <tr>
                <td style="text-align: center; padding-bottom: 10px; vertical-align: bottom; height: 19px;">
                    <asp:Label ID="Label6" runat="server" CssClass="lblRojo" Font-Bold="True" Text="AULAS"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; padding-bottom: 10px; vertical-align: bottom; height: 19px;">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="70px"></asp:Label></td>
                <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="70px"></asp:Label></td>
                <td style="text-align: center; padding-bottom: 10px; vertical-align: bottom; height: 19px;">
                    <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="70px"></asp:Label></td>
                <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="70px"></asp:Label></td>
                <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                    <asp:Label ID="Label12" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="M�S DE UN GRADO"
                        Width="70px"></asp:Label></td>
                <td style="padding-bottom: 10px; vertical-align: bottom; height: 19px; text-align: center">
                    <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="70px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left; width: 45px;">
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EXISTENTES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center; width: 71px;">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV819" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20101"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left; width: 45px;">
                    <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="EN USO"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; height: 26px;">
                    <asp:TextBox ID="txtV820" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20201"></asp:TextBox></td>
                <td style="text-align: center; width: 71px; height: 26px;">
                    <asp:TextBox ID="txtV821" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20202"></asp:TextBox></td>
                <td style="text-align: center; height: 26px;">
                    <asp:TextBox ID="txtV822" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20203"></asp:TextBox></td>
                <td style="height: 26px; text-align: center">
                    <asp:TextBox ID="txtV823" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20204"></asp:TextBox></td>
                <td style="height: 26px; text-align: center">
                    <asp:TextBox ID="txtV824" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20205"></asp:TextBox></td>
                <td style="height: 26px; text-align: center">
                    <asp:TextBox ID="txtV825" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20206"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="7" style="padding-bottom: 10px; padding-top: 10px; text-align: left">
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="De las aulas reportadas en uso, indique el n�mero de las adaptadas."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left; width: 45px;">
                    <asp:Label ID="Label15" runat="server" CssClass="lblRojo" Font-Bold="True" Text="ADAPTADAS"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV826" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20301"></asp:TextBox></td>
                <td style="text-align: center; width: 71px;">
                    <asp:TextBox ID="txtV827" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20302"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV828" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20303"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV829" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20304"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV830" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20305"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV831" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20306"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left; width: 45px;">
                    <asp:Label ID="Label17" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TALLERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center; width: 71px;">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV832" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20401"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 45px; text-align: left" colspan="1" rowspan="2">
                    <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Text="LABORATORIOS"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center; width: 71px;">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                </td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV833" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5"
                        TabIndex="20501"></asp:TextBox></td>
            </tr>
        </table>
        </td>
    </tr>
</table>

   
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal_911_7T',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_7T',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp; </td>
                <td ><span  onclick="openPage('Gasto_911_7T',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Gasto_911_7T',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                 var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
                
 		      Disparador(<%=hidDisparador.Value %>);
                      GetTabIndexes();
        </script> 
</asp:Content>
