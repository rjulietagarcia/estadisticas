<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7T(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_7T.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7T.Personal_911_7T" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

  
   <script type="text/javascript">
        MaxCol = 18;
        MaxRow = 28;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
 
    
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
    <table style="width:100%">
        
        <tr><td><span>BACHILLERATO TECNOL�GICO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_7T',true)"><a href="#" title="" ><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Carreras_911_7T',true)"><a href="#" title="" ><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('Alumnos1_911_7T',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
        <li onclick="openPage('Egresados_911_7T',true)"><a href="#" title="" ><span>EGRESADOS</span></a></li>
        <li onclick="openPage('Alumnos2_911_7T',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="openPage('Planteles_911_7T',true)"><a href="#" title=""  ><span>PLANTELES</span></a></li>
        <li onclick="openPage('Personal_911_7T',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
        <li onclick="openPage('Aulas_911_7T',false)"><a href="#" title="" ><span>AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
  
<center>  
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>



    <div>
    <br/><br/>
        <table>
            <tr>
                <td style="width: 340px; text-align: center;">
                    <table id="TABLE1" style="text-align: center" >
                        <tr>
                            <td colspan="17" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="IV. PERSONAL POR FUNCI�N"
                                    Width="250px" Font-Size="16px"></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="17" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <table style="text-align: center">
                                    <tr>
                                        <td colspan="1" style="padding-bottom: 20px; text-align: left">
                                            <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="1. Sume el personal directivo con grupo, personal docente y personal docente especial, y an�telo seg�n el tiempo que dedica a la funci�n acad�mica."
                                                Width="700px"></asp:Label><br />
                                            <asp:Label ID="lblNota4" runat="server" CssClass="lblRojo" Text="NOTA: Si en la instituci�n no se utiliza el t�rmino tres cuartos de tiempo, no lo considere."
                                                Width="700px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="1" style="padding-bottom: 20px; text-align: left">
                                            <table>
                                                <tr>
                                                    <td>
                                            <asp:Label ID="lblTiempoCompleto" runat="server" CssClass="lblGrisTit" Text="TIEMPO COMPLETO"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV410" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="10101"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                            <asp:Label ID="lblTiempo3Cuartos" runat="server" CssClass="lblGrisTit" Text="TRES CUARTOS DE TIEMPO"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV411" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="10201"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                            <asp:Label ID="lblMedioTiempo" runat="server" CssClass="lblGrisTit" Text="MEDIO TIEMPO"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV412" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="10301"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                            <asp:Label ID="lblPorHoras" runat="server" CssClass="lblGrisTit" Text="POR HORAS"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV413" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                                TabIndex="10401"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                            <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label></td>
                                                    <td>
                                            <asp:TextBox ID="txtV414" runat="server" Columns="4" CssClass="lblNegro" MaxLength="4"
                                                TabIndex="10501"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: text-top; text-align: left">
                                            <br />
                                            <asp:Label ID="lblNotaTotal4" runat="server" CssClass="lblGrisTit" Text="(Este total debe de coincidir con la suma de personal directivo con grupo, personal docente y personal especial reportado en la pregunta 2 de esta secci�n.)"
                                                Width="700px"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="17" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="2. Escriba la cantidad de personal que realiza funciones de directivo (con y sin grupo), docente, docente especial (profesor de educaci�n f�sica, actividades art�sticas, tecnol�gicas y de idiomas), y administrativo, auxiliar y de servicios, independientemente de su nombramiento, tipo y fuente de pago, desgl�selos seg�n su funci�n, nivel m�ximo de estudios y sexo."
                                    Width="1195px"></asp:Label><br />
                                <br />
                                <asp:Label ID="lblNota1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Notas:"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="a) Si una persona desempe�a dos o m�s funciones an�tela en aquella a la que dedique m�s tiempo"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="lblNota3" runat="server" CssClass="lblRojo" Text="b) Si en la tabla corresponde al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente o en OTROS"
                                    Width="1195px"></asp:Label><br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="2" style="width: 215px; height: 3px; text-align: center; padding-right: 5px; padding-left: 5px;">
                                <asp:Label ID="lblNivelEd" runat="server" CssClass="lblRojo" Text="NIVEL EDUCATIVO" Width="200px"></asp:Label></td>
                            <td colspan="4" rowspan="" style="text-align: center;">
                                <asp:Label ID="lblPersDir" runat="server" CssClass="lblRojo" Text="PERSONAL DIRECTIVO" Width="141px"></asp:Label></td>
                            <td colspan="2" rowspan="2" style="text-align: center">
                                <asp:Label ID="lblPersDocente" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE" Width="85px"></asp:Label></td>
                            <td style="border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;" colspan="8" rowspan="">
                                <asp:Label ID="lblPersDocEsp" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE ESPECIAL"
                                    Width="254px"></asp:Label></td>
                            <td colspan="2" rowspan="2" style="text-align: center">
                                <asp:Label ID="lblPersAdmin" runat="server" CssClass="lblRojo" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS" Height="54px" Width="95px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblPersDirCG" runat="server" CssClass="lblRojo" Text="CON GRUPO" Width="70px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblPersDirSG" runat="server" CssClass="lblRojo" Text="SIN GRUPO" Width="70px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblDocEdFis" runat="server" CssClass="lblRojo" Text="PROFESORES DE EDUCACI�N F�SICA" Width="110px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblDocActArt" runat="server" CssClass="lblRojo" Text="PROFESORES DE ACTIVIDADES ART�STICAS"
                                    Width="100px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblDocActTec" runat="server" CssClass="lblRojo" Text="PROFESORES DE ACTIVIDADES TECNOL�GICAS" Width="100px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblDocIdiom" runat="server" CssClass="lblRojo" Text="PROFESORES DE IDIOMAS" Width="95px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="padding-right: 5px; width: 215px">
                            </td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDirCGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 66px; height: 26px">
                                <asp:Label ID="lblPersDirCGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDirSGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDirSGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDocenteH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDocenteM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocEdFisH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocEdFisM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocActArtH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 62px; height: 26px">
                                <asp:Label ID="lblDocActArtM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocActTecH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocActTecM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 59px; height: 26px">
                                <asp:Label ID="lblDocIdiomH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 54px; height: 26px">
                                <asp:Label ID="lblDocIdiomM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 54px; height: 26px">
                                <asp:Label ID="lblPersAdminH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 54px; height: 26px">
                                <asp:Label ID="lblPersAdminM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="padding-right: 5px; width: 215px; height: 36px; text-align: left">
                                <asp:Label ID="lblPrimInc" runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td style="width: 62px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV415" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20101"></asp:TextBox></td>
                            <td style="width: 67px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV416" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20102"></asp:TextBox></td>
                            <td style="width: 67px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV417" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20103"></asp:TextBox></td>
                            <td style="width: 59px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV418" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20104"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV419" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="20105"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV420" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="20106"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV421" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20107"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV422" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20108"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV423" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20109"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV424" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20110"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV425" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20111"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV426" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20112"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV427" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20113"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV428" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20114"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV429" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20115"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV430" runat="server" Columns="3" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20116"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="padding-right: 5px; width: 215px; height: 36px; text-align: left">
                                <asp:Label ID="lblPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="PRIMARIA TERMINADA"></asp:Label></td>
                            <td style="width: 62px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV431" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20201"></asp:TextBox></td>
                            <td style="width: 67px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV432" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20202"></asp:TextBox></td>
                            <td style="width: 67px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV433" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20203"></asp:TextBox></td>
                            <td style="width: 59px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV434" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20204"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV435" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="20205"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV436" runat="server" Columns="4" CssClass="lblNegro" MaxLength="3"
                                    TabIndex="20206"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV437" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20207"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV438" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20208"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV439" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20209"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV440" runat="server" Columns="2" CssClass="lblNegro" MaxLength="1"
                                    TabIndex="20210"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV441" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20211"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV442" runat="server" Columns="2" CssClass="lblNegro" MaxLength="2"
                                    TabIndex="20212"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV443" runat="server" Columns="2" TabIndex="20213" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV444" runat="server" Columns="2" TabIndex="20214" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV445" runat="server" Columns="3" TabIndex="20215" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV446" runat="server" Columns="3" TabIndex="20216" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 215px; padding-right: 5px; height: 36px; text-align: left;">
                            <asp:Label ID="lblSecInc" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td style="width: 54px; height: 36px; text-align: center;">
                                <asp:TextBox ID="txtV447" runat="server" Columns="2" TabIndex="20301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center;">
                                <asp:TextBox ID="txtV448" runat="server" Columns="2" TabIndex="20302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV449" runat="server" Columns="2" TabIndex="20303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV450" runat="server" Columns="2" TabIndex="20304" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV451" runat="server" Columns="4" TabIndex="20305" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV452" runat="server" Columns="4" TabIndex="20306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV453" runat="server" Columns="2" TabIndex="20307" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV454" runat="server" Columns="2" TabIndex="20308" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV455" runat="server" Columns="2" TabIndex="20309" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV456" runat="server" Columns="2" TabIndex="20310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV457" runat="server" Columns="2" TabIndex="20311" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV458" runat="server" Columns="2" TabIndex="20312" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV459" runat="server" Columns="2" TabIndex="20313" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV460" runat="server" Columns="2" TabIndex="20314" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV461" runat="server" Columns="3" TabIndex="20315" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 36px; text-align: center">
                                <asp:TextBox ID="txtV462" runat="server" Columns="3" TabIndex="20316" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblSecTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="SECUNDARIA TERMINADA"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV463" runat="server" Columns="2" TabIndex="20401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV464" runat="server" Columns="2" TabIndex="20402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV465" runat="server" Columns="2" TabIndex="20403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV466" runat="server" Columns="2" TabIndex="20404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV467" runat="server" Columns="4" TabIndex="20405" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV468" runat="server" Columns="4" TabIndex="20406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV469" runat="server" Columns="2" TabIndex="20407" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV470" runat="server" Columns="2" TabIndex="20408" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV471" runat="server" Columns="2" TabIndex="20409" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV472" runat="server" Columns="2" TabIndex="20410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV473" runat="server" Columns="2" TabIndex="20411" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV474" runat="server" Columns="2" TabIndex="20412" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV475" runat="server" Columns="2" TabIndex="20413" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV476" runat="server" Columns="2" TabIndex="20414" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV477" runat="server" Columns="3" TabIndex="20415" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV478" runat="server" Columns="3" TabIndex="20416" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 215px; margin-right: 5px; text-align: left;">
                                <asp:Label ID="lblProfTec" runat="server" CssClass="lblGrisTit" Height="17px" Text="PROFESIONAL T�CNICO"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV479" runat="server" Columns="2" TabIndex="20501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV480" runat="server" Columns="2" TabIndex="20502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV481" runat="server" Columns="2" TabIndex="20503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV482" runat="server" Columns="2" TabIndex="20504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV483" runat="server" Columns="4" TabIndex="20505" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV484" runat="server" Columns="4" TabIndex="20506" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV485" runat="server" Columns="2" TabIndex="20507" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV486" runat="server" Columns="2" TabIndex="20508" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV487" runat="server" Columns="2" TabIndex="20509" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV488" runat="server" Columns="2" TabIndex="20510" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV489" runat="server" Columns="2" TabIndex="20511" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV490" runat="server" Columns="2" TabIndex="20512" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV491" runat="server" Columns="2" TabIndex="20513" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV492" runat="server" Columns="2" TabIndex="20514" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV493" runat="server" Columns="3" TabIndex="20515" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV494" runat="server" Columns="3" TabIndex="20516" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblBachInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO INCOMPLETO"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV495" runat="server" Columns="2" TabIndex="20601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV496" runat="server" Columns="2" TabIndex="20602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV497" runat="server" Columns="2" TabIndex="20603" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV498" runat="server" Columns="2" TabIndex="20604" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV499" runat="server" Columns="4" TabIndex="20605" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV500" runat="server" Columns="4" TabIndex="20606" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV501" runat="server" Columns="2" TabIndex="20607" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV502" runat="server" Columns="2" TabIndex="20608" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV503" runat="server" Columns="2" TabIndex="20609" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV504" runat="server" Columns="2" TabIndex="20610" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV505" runat="server" Columns="2" TabIndex="20611" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV506" runat="server" Columns="2" TabIndex="20612" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV507" runat="server" Columns="2" TabIndex="20613" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV508" runat="server" Columns="2" TabIndex="20614" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV509" runat="server" Columns="3" TabIndex="20615" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV510" runat="server" Columns="3" TabIndex="20616" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblBachTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO TERMINADO"></asp:Label></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV511" runat="server" Columns="2" TabIndex="20701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV512" runat="server" Columns="2" TabIndex="20702" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV513" runat="server" Columns="2" TabIndex="20703" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV514" runat="server" Columns="2" TabIndex="20704" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV515" runat="server" Columns="4" TabIndex="20705" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV516" runat="server" Columns="4" TabIndex="20706" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV517" runat="server" Columns="2" TabIndex="20707" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV518" runat="server" Columns="2" TabIndex="20708" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV519" runat="server" Columns="2" TabIndex="20709" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV520" runat="server" Columns="2" TabIndex="20710" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV521" runat="server" Columns="2" TabIndex="20711" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV522" runat="server" Columns="2" TabIndex="20712" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV523" runat="server" Columns="2" TabIndex="20713" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV524" runat="server" Columns="2" TabIndex="20714" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV525" runat="server" Columns="3" TabIndex="20715" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV526" runat="server" Columns="3" TabIndex="20716" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormPreeInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR INCOMPLETA"
                                    Width="215px"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV527" runat="server" Columns="2" TabIndex="20801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV528" runat="server" Columns="2" TabIndex="20802" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV529" runat="server" Columns="2" TabIndex="20803" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV530" runat="server" Columns="2" TabIndex="20804" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV531" runat="server" Columns="4" TabIndex="20805" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV532" runat="server" Columns="4" TabIndex="20806" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV533" runat="server" Columns="2" TabIndex="20807" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV534" runat="server" Columns="2" TabIndex="20808" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV535" runat="server" Columns="2" TabIndex="20809" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV536" runat="server" Columns="2" TabIndex="20810" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV537" runat="server" Columns="2" TabIndex="20811" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV538" runat="server" Columns="2" TabIndex="20812" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV539" runat="server" Columns="2" TabIndex="20813" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV540" runat="server" Columns="2" TabIndex="20814" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV541" runat="server" Columns="3" TabIndex="20815" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV542" runat="server" Columns="3" TabIndex="20816" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormPreeTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR TERMINADA"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV543" runat="server" Columns="2" TabIndex="20901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV544" runat="server" Columns="2" TabIndex="20902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV545" runat="server" Columns="2" TabIndex="20903" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV546" runat="server" Columns="2" TabIndex="20904" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV547" runat="server" Columns="4" TabIndex="20905" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV548" runat="server" Columns="4" TabIndex="20906" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV549" runat="server" Columns="2" TabIndex="20907" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV550" runat="server" Columns="2" TabIndex="20908" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV551" runat="server" Columns="2" TabIndex="20909" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV552" runat="server" Columns="2" TabIndex="20910" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV553" runat="server" Columns="2" TabIndex="20911" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV554" runat="server" Columns="2" TabIndex="20912" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV555" runat="server" Columns="2" TabIndex="20913" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV556" runat="server" Columns="2" TabIndex="20914" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV557" runat="server" Columns="3" TabIndex="20915" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV558" runat="server" Columns="3" TabIndex="20916" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormPrimInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA INCOMPLETA"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV559" runat="server" Columns="2" TabIndex="21001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV560" runat="server" Columns="2" TabIndex="21002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV561" runat="server" Columns="2" TabIndex="21003" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV562" runat="server" Columns="2" TabIndex="21004" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV563" runat="server" Columns="4" TabIndex="21005" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV564" runat="server" Columns="4" TabIndex="21006" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV565" runat="server" Columns="2" TabIndex="21007" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV566" runat="server" Columns="2" TabIndex="21008" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV567" runat="server" Columns="2" TabIndex="21009" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV568" runat="server" Columns="2" TabIndex="21010" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV569" runat="server" Columns="2" TabIndex="21011" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV570" runat="server" Columns="2" TabIndex="21012" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV571" runat="server" Columns="2" TabIndex="21013" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV572" runat="server" Columns="2" TabIndex="21014" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV573" runat="server" Columns="3" TabIndex="21015" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV574" runat="server" Columns="3" TabIndex="21016" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA TERMINADA"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV575" runat="server" Columns="2" TabIndex="21101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV576" runat="server" Columns="2" TabIndex="21102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV577" runat="server" Columns="2" TabIndex="21103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV578" runat="server" Columns="2" TabIndex="21104" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV579" runat="server" Columns="4" TabIndex="21105" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV580" runat="server" Columns="4" TabIndex="21106" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV581" runat="server" Columns="2" TabIndex="21107" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV582" runat="server" Columns="2" TabIndex="21108" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV583" runat="server" Columns="2" TabIndex="21109" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV584" runat="server" Columns="2" TabIndex="21110" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV585" runat="server" Columns="2" TabIndex="21111" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV586" runat="server" Columns="2" TabIndex="21112" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV587" runat="server" Columns="2" TabIndex="21113" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV588" runat="server" Columns="2" TabIndex="21114" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV589" runat="server" Columns="3" TabIndex="21115" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV590" runat="server" Columns="3" TabIndex="21116" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 15px; text-align: left">
                                <asp:Label ID="lblNormSupInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR INCOMPLETA"></asp:Label></td>
                            <td style="width: 54px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV591" runat="server" Columns="2" TabIndex="21201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV592" runat="server" Columns="2" TabIndex="21202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV593" runat="server" Columns="2" TabIndex="21203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV594" runat="server" Columns="2" TabIndex="21204" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV595" runat="server" Columns="4" TabIndex="21205" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV596" runat="server" Columns="4" TabIndex="21206" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV597" runat="server" Columns="2" TabIndex="21207" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV598" runat="server" Columns="2" TabIndex="21208" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV599" runat="server" Columns="2" TabIndex="21209" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV600" runat="server" Columns="2" TabIndex="21210" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV601" runat="server" Columns="2" TabIndex="21211" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV602" runat="server" Columns="2" TabIndex="21212" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV603" runat="server" Columns="2" TabIndex="21213" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV604" runat="server" Columns="2" TabIndex="21214" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV605" runat="server" Columns="3" TabIndex="21215" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV606" runat="server" Columns="3" TabIndex="21216" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormSupPas" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, PASANTE"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV607" runat="server" Columns="2" TabIndex="21301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV608" runat="server" Columns="2" TabIndex="21302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV609" runat="server" Columns="2" TabIndex="21303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV610" runat="server" Columns="2" TabIndex="21304" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV611" runat="server" Columns="4" TabIndex="21305" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV612" runat="server" Columns="4" TabIndex="21306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV613" runat="server" Columns="2" TabIndex="21307" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV614" runat="server" Columns="2" TabIndex="21308" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV615" runat="server" Columns="2" TabIndex="21309" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV616" runat="server" Columns="2" TabIndex="21310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV617" runat="server" Columns="2" TabIndex="21311" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV618" runat="server" Columns="2" TabIndex="21312" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV619" runat="server" Columns="2" TabIndex="21313" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV620" runat="server" Columns="2" TabIndex="21314" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV621" runat="server" Columns="3" TabIndex="21315" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV622" runat="server" Columns="3" TabIndex="21316" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormSupTit" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, TITULADO"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV623" runat="server" Columns="2" TabIndex="21401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV624" runat="server" Columns="2" TabIndex="21402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV625" runat="server" Columns="2" TabIndex="21403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV626" runat="server" Columns="2" TabIndex="21404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV627" runat="server" Columns="4" TabIndex="21405" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV628" runat="server" Columns="4" TabIndex="21406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV629" runat="server" Columns="2" TabIndex="21407" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV630" runat="server" Columns="2" TabIndex="21408" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV631" runat="server" Columns="2" TabIndex="21409" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV632" runat="server" Columns="2" TabIndex="21410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV633" runat="server" Columns="2" TabIndex="21411" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV634" runat="server" Columns="2" TabIndex="21412" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV635" runat="server" Columns="2" TabIndex="21413" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV636" runat="server" Columns="2" TabIndex="21414" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV637" runat="server" Columns="3" TabIndex="21415" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV638" runat="server" Columns="3" TabIndex="21416" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblLicInc" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA INCOMPLETA"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV639" runat="server" Columns="2" TabIndex="21501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV640" runat="server" Columns="2" TabIndex="21502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV641" runat="server" Columns="2" TabIndex="21503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV642" runat="server" Columns="2" TabIndex="21504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV643" runat="server" Columns="4" TabIndex="21505" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV644" runat="server" Columns="4" TabIndex="21506" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV645" runat="server" Columns="2" TabIndex="21507" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV646" runat="server" Columns="2" TabIndex="21508" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV647" runat="server" Columns="2" TabIndex="21509" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV648" runat="server" Columns="2" TabIndex="21510" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV649" runat="server" Columns="2" TabIndex="21511" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV650" runat="server" Columns="2" TabIndex="21512" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV651" runat="server" Columns="2" TabIndex="21513" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV652" runat="server" Columns="2" TabIndex="21514" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV653" runat="server" Columns="3" TabIndex="21515" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV654" runat="server" Columns="3" TabIndex="21516" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 12px; text-align: left">
                                <asp:Label ID="lblLicPas" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, PASANTE"></asp:Label></td>
                            <td style="width: 54px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV655" runat="server" Columns="2" TabIndex="21601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV656" runat="server" Columns="2" TabIndex="21602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV657" runat="server" Columns="2" TabIndex="21603" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV658" runat="server" Columns="2" TabIndex="21604" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV659" runat="server" Columns="4" TabIndex="21605" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV660" runat="server" Columns="4" TabIndex="21606" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV661" runat="server" Columns="2" TabIndex="21607" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV662" runat="server" Columns="2" TabIndex="21608" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV663" runat="server" Columns="2" TabIndex="21609" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV664" runat="server" Columns="2" TabIndex="21610" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV665" runat="server" Columns="2" TabIndex="21611" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV666" runat="server" Columns="2" TabIndex="21612" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV667" runat="server" Columns="2" TabIndex="21613" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV668" runat="server" Columns="2" TabIndex="21614" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV669" runat="server" Columns="3" TabIndex="21615" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV670" runat="server" Columns="3" TabIndex="21616" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                            <asp:Label ID="lblLicTit" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV671" runat="server" Columns="2" TabIndex="21701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV672" runat="server" Columns="2" TabIndex="21702" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV673" runat="server" Columns="2" TabIndex="21703" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV674" runat="server" Columns="2" TabIndex="21704" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV675" runat="server" Columns="4" TabIndex="21705" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV676" runat="server" Columns="4" TabIndex="21706" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV677" runat="server" Columns="2" TabIndex="21707" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV678" runat="server" Columns="2" TabIndex="21708" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV679" runat="server" Columns="2" TabIndex="21709" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV680" runat="server" Columns="2" TabIndex="21710" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV681" runat="server" Columns="2" TabIndex="21711" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV682" runat="server" Columns="2" TabIndex="21712" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV683" runat="server" Columns="2" TabIndex="21713" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV684" runat="server" Columns="2" TabIndex="21714" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV685" runat="server" Columns="3" TabIndex="21715" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV686" runat="server" Columns="3" TabIndex="21716" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblMaestInc" runat="server" CssClass="lblGrisTit" Text="MAESTR�A INCOMPLETA"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV687" runat="server" Columns="2" TabIndex="21801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV688" runat="server" Columns="2" TabIndex="21802" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV689" runat="server" Columns="2" TabIndex="21803" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV690" runat="server" Columns="2" TabIndex="21804" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV691" runat="server" Columns="4" TabIndex="21805" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV692" runat="server" Columns="4" TabIndex="21806" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV693" runat="server" Columns="2" TabIndex="21807" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV694" runat="server" Columns="2" TabIndex="21808" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV695" runat="server" Columns="2" TabIndex="21809" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV696" runat="server" Columns="2" TabIndex="21810" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV697" runat="server" Columns="2" TabIndex="21811" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV698" runat="server" Columns="2" TabIndex="21812" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV699" runat="server" Columns="2" TabIndex="21813" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV700" runat="server" Columns="2" TabIndex="21814" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV701" runat="server" Columns="3" TabIndex="21815" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV702" runat="server" Columns="3" TabIndex="21816" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 21px; text-align: left">
                                <asp:Label ID="lblMaestGrad" runat="server" CssClass="lblGrisTit" Text="MAESTR�A, GRADUADO"></asp:Label></td>
                            <td style="width: 54px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV703" runat="server" Columns="2" TabIndex="21901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV704" runat="server" Columns="2" TabIndex="21902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV705" runat="server" Columns="2" TabIndex="21903" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV706" runat="server" Columns="2" TabIndex="21904" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV707" runat="server" Columns="4" TabIndex="21905" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV708" runat="server" Columns="4" TabIndex="21906" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV709" runat="server" Columns="2" TabIndex="21907" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV710" runat="server" Columns="2" TabIndex="21908" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV711" runat="server" Columns="2" TabIndex="21909" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV712" runat="server" Columns="2" TabIndex="21910" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV713" runat="server" Columns="2" TabIndex="21911" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV714" runat="server" Columns="2" TabIndex="21912" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV715" runat="server" Columns="2" TabIndex="21913" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV716" runat="server" Columns="2" TabIndex="21914" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV717" runat="server" Columns="3" TabIndex="21915" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV718" runat="server" Columns="3" TabIndex="21916" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblDocInc" runat="server" CssClass="lblGrisTit" Text="DOCTORADO INCOMPLETO"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV719" runat="server" Columns="2" TabIndex="22001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV720" runat="server" Columns="2" TabIndex="22002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV721" runat="server" Columns="2" TabIndex="22003" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV722" runat="server" Columns="2" TabIndex="22004" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV723" runat="server" Columns="4" TabIndex="22005" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV724" runat="server" Columns="4" TabIndex="22006" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV725" runat="server" Columns="2" TabIndex="22007" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV726" runat="server" Columns="2" TabIndex="22008" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV727" runat="server" Columns="2" TabIndex="22009" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV728" runat="server" Columns="2" TabIndex="22010" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV729" runat="server" Columns="2" TabIndex="22011" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV730" runat="server" Columns="2" TabIndex="22012" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV731" runat="server" Columns="2" TabIndex="22013" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV732" runat="server" Columns="2" TabIndex="22014" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV733" runat="server" Columns="3" TabIndex="22015" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV734" runat="server" Columns="3" TabIndex="22016" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblDocGrad" runat="server" CssClass="lblGrisTit" Text="DOCTORADO, GRADUADO"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV735" runat="server" Columns="2" TabIndex="22101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV736" runat="server" Columns="2" TabIndex="22102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV737" runat="server" Columns="2" TabIndex="22103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV738" runat="server" Columns="2" TabIndex="22104" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV739" runat="server" Columns="4" TabIndex="22105" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV740" runat="server" Columns="4" TabIndex="22106" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV741" runat="server" Columns="2" TabIndex="22107" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV742" runat="server" Columns="2" TabIndex="22108" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV743" runat="server" Columns="2" TabIndex="22109" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV744" runat="server" Columns="2" TabIndex="22110" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV745" runat="server" Columns="2" TabIndex="22111" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV746" runat="server" Columns="2" TabIndex="22112" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV747" runat="server" Columns="2" TabIndex="22113" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV748" runat="server" Columns="2" TabIndex="22114" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV749" runat="server" Columns="3" TabIndex="22115" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV750" runat="server" Columns="3" TabIndex="22116" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS*"></asp:Label>
                                <br />
                                <asp:Label ID="lblEspecifique" runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                            <td style="width: 54px; height: 26px; text-align: center">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtV751" runat="server" Columns="30" TabIndex="22201" MaxLength="30" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV752" runat="server" Columns="2" TabIndex="22202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV753" runat="server" Columns="2" TabIndex="22203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV754" runat="server" Columns="2" TabIndex="22204" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV755" runat="server" Columns="2" TabIndex="22205" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV756" runat="server" Columns="4" TabIndex="22206" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV757" runat="server" Columns="4" TabIndex="22207" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV758" runat="server" Columns="2" TabIndex="22208" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV759" runat="server" Columns="2" TabIndex="22209" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV760" runat="server" Columns="2" TabIndex="22210" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV761" runat="server" Columns="2" TabIndex="22211" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV762" runat="server" Columns="2" TabIndex="22212" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV763" runat="server" Columns="2" TabIndex="22213" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV764" runat="server" Columns="2" TabIndex="22214" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV765" runat="server" Columns="2" TabIndex="22215" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV766" runat="server" Columns="3" TabIndex="22216" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV767" runat="server" Columns="3" TabIndex="22217" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtV768" runat="server" Columns="30" TabIndex="22301" MaxLength="30" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV769" runat="server" Columns="2" TabIndex="22302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV770" runat="server" Columns="2" TabIndex="22303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV771" runat="server" Columns="2" TabIndex="22304" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV772" runat="server" Columns="2" TabIndex="22305" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV773" runat="server" Columns="4" TabIndex="22306" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV774" runat="server" Columns="4" TabIndex="22307" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV775" runat="server" Columns="2" TabIndex="22308" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV776" runat="server" Columns="2" TabIndex="22309" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV777" runat="server" Columns="2" TabIndex="22310" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV778" runat="server" Columns="2" TabIndex="22311" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV779" runat="server" Columns="2" TabIndex="22312" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV780" runat="server" Columns="2" TabIndex="22313" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV781" runat="server" Columns="2" TabIndex="22314" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV782" runat="server" Columns="2" TabIndex="22315" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV783" runat="server" Columns="3" TabIndex="22316" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV784" runat="server" Columns="3" TabIndex="22317" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtV785" runat="server" Columns="30" TabIndex="22401" MaxLength="30" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV786" runat="server" Columns="2" TabIndex="22402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV787" runat="server" Columns="2" TabIndex="22403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV788" runat="server" Columns="2" TabIndex="22404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV789" runat="server" Columns="2" TabIndex="22405" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV790" runat="server" Columns="4" TabIndex="22406" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV791" runat="server" Columns="4" TabIndex="22407" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV792" runat="server" Columns="2" TabIndex="22408" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV793" runat="server" Columns="2" TabIndex="22409" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV794" runat="server" Columns="2" TabIndex="22410" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV795" runat="server" Columns="2" TabIndex="22411" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV796" runat="server" Columns="2" TabIndex="22412" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV797" runat="server" Columns="2" TabIndex="22413" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV798" runat="server" Columns="2" TabIndex="22414" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV799" runat="server" Columns="2" TabIndex="22415" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV800" runat="server" Columns="3" TabIndex="22416" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV801" runat="server" Columns="3" TabIndex="22417" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 54px; height: 26px">
                                <asp:Label ID="lblSubtotales" runat="server" CssClass="lblGrisTit" Text="SUBTOTALES"></asp:Label></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV802" runat="server" Columns="2" TabIndex="22501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV803" runat="server" Columns="2" TabIndex="22502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV804" runat="server" Columns="2" TabIndex="22503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV805" runat="server" Columns="2" TabIndex="22504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV806" runat="server" Columns="4" TabIndex="22505" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV807" runat="server" Columns="4" TabIndex="22506" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV808" runat="server" Columns="2" TabIndex="22507" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV809" runat="server" Columns="2" TabIndex="22508" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV810" runat="server" Columns="2" TabIndex="22509" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV811" runat="server" Columns="2" TabIndex="22510" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV812" runat="server" Columns="2" TabIndex="22511" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV813" runat="server" Columns="2" TabIndex="22512" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV814" runat="server" Columns="2" TabIndex="22513" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV815" runat="server" Columns="2" TabIndex="22514" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV816" runat="server" Columns="3" TabIndex="22515" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="width: 54px; height: 26px">
                                <asp:TextBox ID="txtV817" runat="server" Columns="3" TabIndex="22516" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="16" rowspan="1" style="text-align: right">
                                &nbsp;<asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL PERSONAL (Suma de subtotales)"
                                    Width="275px"></asp:Label></td>
                            <td style="width: 54px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV818" runat="server" Columns="4" TabIndex="30101" MaxLength="4" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('Planteles_911_7T',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Planteles_911_7T',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp; </td>
                <td ><span  onclick="OpenPageCharged('Aulas_911_7T',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('Aulas_911_7T',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                 var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                 function OpenPageCharged(pagina){
                    Guion('751');
                    Guion('768');
                    Guion('785');
                    openPage(pagina);
                }
                
                function Guion(variable){
                    var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                    if (txtv != null){
                       if (txtv.value == "")
                          txtv.value = "_";  
                    }
                }
 		      Disparador(<%=hidDisparador.Value %>);
                      GetTabIndexes();
        </script> 
</asp:Content>
