<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.7T(Egresados)" AutoEventWireup="true" CodeBehind="Egresados_911_7T.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_7T.Egresados_911_7T" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


   <script type="text/javascript">
        MaxCol = 5;
        MaxRow = 28;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>

    
    <div id="logo"></div>
    <div style="min-width:1400px; height:65px;">
    <div id="header">
    <table style="width:100%">
        
        <tr><td><span>BACHILLERATO TECNOL�GICO</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1400px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title="" ><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Carreras_911_7T',true)"><a href="#" title="" ><span>ALUMNOS POR CARRERA</span></a></li>
        <li onclick="openPage('Alumnos1_911_7T',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
        <li onclick="openPage('Egresados_911_7T',true)"><a href="#" title=""  class="activo"><span>EGRESADOS</span></a></li>
        <li onclick="openPage('Alumnos2_911_7T',true)"><a href="#" title=""><span>ALUMNOS POR EDAD</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PLANTELES</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>PERSONAL</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>AULAS</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    
  <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

  <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>



        <table style="width: 400px" align="center">
            <tr>
                <td colspan="2" valign="top" style="text-align:justify">
                    <asp:Label ID="Label35" runat="server" CssClass="lblGrisTit" Font-Bold="True" Font-Size="16px"
                        Text="II. EGRESADOS, REPROBADOS Y REGULARIZADOS DEL CICLO ANTERIOR." Width="400px"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <asp:Label ID="Label1" runat="server" CssClass="lblRojo" Font-Bold="True" Font-Size="12px"
                        Text="(CONSIDERE LA INFORMACI�N REPORTADA EN EL FIN DE CURSOS ANTERIOR)" Width="400px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <table>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="1. Escriba la cantidad de alumnos egresados del ciclo escolar anterior (incluya los regularizados hasta el 30 de Septiembre de este a�o), desglos�ndola por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center">
                    <asp:Label ID="lblInscripcionT" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblExistencia" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;">
                    <asp:Label ID="lblAprobados" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lbl1y2" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="ALUMNOS EGRESADOS"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV48" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV49" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10102"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV50" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10103"></asp:TextBox></td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="2. Escriba la cantidad de alumnos, por grado, que reprobaron de 1 a 5 asignaturas el ciclo escolar anterior, desglos�ndola por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center">
                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="Label4" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;">
                    <asp:Label ID="Label5" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV51" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV52" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10202"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV53" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV54" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV55" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10302"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV56" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV57" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV58" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10402"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV59" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV60" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV61" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10502"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV62" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10503"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center">
                    <asp:Label ID="Label10" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV63" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10601"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV64" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10602"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV65" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10603"></asp:TextBox></td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label11" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="3. De los alumnos reportados en el punto anterior registre el n�mero de alumnos que se regularizaron (aprobaron mediante ex�menes extraordinarios todas las asignaturas que adeudaban) al 30 de Septiembre del presente a�o, desglos�ndolo por sexo."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center">
                    <asp:Label ID="Label12" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="Label13" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;">
                    <asp:Label ID="Label14" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label15" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV66" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10701"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV67" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10702"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV68" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10703"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label16" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV69" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10801"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV70" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10802"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV71" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10803"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV72" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10901"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV73" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10902"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV74" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="10903"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label18" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV75" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11001"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV76" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11002"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV77" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11003"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center">
                    <asp:Label ID="Label19" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV78" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11101"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV79" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11102"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV80" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11103"></asp:TextBox></td>
            </tr>
                        <tr>
                            <td colspan="4" style="text-align: justify; padding-bottom: 10px; padding-top: 10px;">
                    <asp:Label ID="Label20" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="4. De los alumnos reportados en la pregunta 2, escriba la cantidad de ellos que est�n inscritos en el presente ciclo escolar y contin�an como irregulares (adeudan asignaturas), seg�n sexo y el grado que cursan actualmente."
                        Width="370px"></asp:Label></td>
                        </tr>
            <tr>
                <td style="text-align: center">
                    </td>
                <td style="text-align: center">
                    <asp:Label ID="Label21" runat="server" CssClass="lblRojo" Font-Bold="True"
                        Text="HOMBRES" Width="90px"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="Label22" runat="server" CssClass="lblRojo" Font-Bold="True" Text="MUJERES"
                        Width="90px"></asp:Label></td>
                <td style="text-align: center; width: 93px;">
                    <asp:Label ID="Label23" runat="server" CssClass="lblRojo" Font-Bold="True" Text="TOTAL"
                        Width="90px"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="PRIMERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV81" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11201"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV82" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11202"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV83" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11203"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="SEGUNDO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV84" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11301"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV85" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11302"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV86" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11303"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label26" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="TERCERO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV87" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11401"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV88" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11402"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV89" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11403"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="CUARTO"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV90" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11501"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV91" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11502"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV92" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11503"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="1" style="text-align: center">
                    <asp:Label ID="Label28" runat="server" CssClass="lblRojo" Font-Bold="True" Text="T O T A L"
                        Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV93" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11601"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV94" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11602"></asp:TextBox></td>
                <td style="text-align: center; width: 93px;">
                    <asp:TextBox ID="txtV95" runat="server" Columns="5" MaxLength="5" CssClass="lblNegro" TabIndex="11603"></asp:TextBox></td>
            </tr>                        
        </table>
       
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Alumnos1_911_7T',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Alumnos1_911_7T',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp; </td>
                <td ><span  onclick="openPage('Alumnos2_911_7T',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Alumnos2_911_7T',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		Disparador(<%=hidDisparador.Value %>);
        GetTabIndexes();
        </script> 
</asp:Content>
