<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="LMD_911_111.aspx.cs" Inherits="EstadisticasEducativas._911.inicio._911_111.LMD_911_111" Title="911.111(Lengua Materna)" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
<div id="logo"></div>
    <div style="min-width:1100px; height:65px;">
    <div id="header">
    <table style="width:100%;">
        <tr><td><span>EDUCACI�N PREESCOLAR IND�GENA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_111',true)">  <a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('AYG_911_111',true)">             <a href="#" title="" ><span>ALUMNOS Y GRUPOS</span></a></li><li onclick="openPage('LMD_911_111',true)">             <a href="#" title="" class="activo"><span>LENGUA MATERNA</span></a></li><li onclick="openPage('Personal_911_111',false)">       <a href="#" title="" ><span>PERSONAL POR FUNCION</span></a></li><li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>CARRERA Y AULAS</span></a></li><li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>ANEXO</span></a></li><li onclick="denyPage()">                               <a href="#" title="" class="inactivo"><span>OFICIALIZACION</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br />
        <br />
        <br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
        <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"></td>
				    <td>   
                        <table align="center" style="width:600px;" >
                            <tr>
                                <td style="text-align:justify;">
                            <asp:Label ID="Label25" Font-Size="16px" runat="server" CssClass="lblGrisTit" Text="II. LENGUA MATERNA"></asp:Label><br />
                                </td>
                            </tr>
                            <tr>
                                <td  style="text-align:justify;">
                                    &nbsp;<asp:Label ID="lblInstrucion1" runat="server" CssClass="lblRojo" Text="1. <i>Lengua materna</i>. Escriba el nombre de las lenguas maternas habladas por los docentes y la cantidad de maestros que hablan cada una de ellas." ></asp:Label></td>
                            </tr>
                             
                            <tr>
                                <td >
                                <table>
                            <tr>
                                <td  >
                                    <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit" Text="Clave" Width="100%"></asp:Label></td>
                                <td   style="width: 384px">
                                    <asp:Label ID="lbl3a" runat="server" CssClass="lblGrisTit" Text="Lengua Materna" Width="100%"></asp:Label></td>
                                <td  >
                                    <asp:Label ID="lbl4a" runat="server" CssClass="lblGrisTit" Text="N�mero de maestros" Width="100%"></asp:Label></td>
                                <td  >&nbsp;</td>
                            </tr>
                            <tr>
                                <td   >
                                    <asp:TextBox ReadOnly="true"  ID="txtV95" runat="server" Columns="2"  MaxLength="2" TabIndex="10101" CssClass="lblNegro" ></asp:TextBox></td>
                                <td    style="width: 384px">
                                    <asp:TextBox ID="txtV96" runat="server" ReadOnly="True"  Width="5px" style="visibility:hidden;" >0</asp:TextBox>
                                    <asp:DropDownList ID="optV96" runat="server"    Width="340px"  onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV96','ctl00_cphMainMaster_txtV95')">
                                    </asp:DropDownList>
                                    
                                    </td>
                                <td   >
                                    <asp:TextBox ID="txtV97" runat="server" Columns="2"  MaxLength="2" TabIndex="10102" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >&nbsp;</td>
                            </tr>
                            <tr>
                                <td   >
                                    <asp:TextBox ReadOnly="true" ID="txtV98" runat="server" Columns="2"  MaxLength="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
                                <td   style="width: 384px">
                                    <asp:TextBox ID="txtV99" runat="server" ReadOnly="True"  Width="5px" style="visibility:hidden;" >0</asp:TextBox>
                                    <asp:DropDownList ID="optV99" runat="server"    Width="340px"  onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV99','ctl00_cphMainMaster_txtV98')">
                                    </asp:DropDownList>
                                    
                                    </td>
                                <td  >
                                    <asp:TextBox ID="txtV100" runat="server" Columns="2"  MaxLength="2" TabIndex="10202" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >&nbsp;</td>
                            </tr>
                            <tr>
                                <td   >
                                    <asp:TextBox ReadOnly="true" ID="txtV101" runat="server" Columns="2"  MaxLength="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
                                <td    style="width: 384px">
                                    <asp:TextBox ID="txtV102" runat="server" ReadOnly="True"  Width="5px" style="visibility:hidden;" >0</asp:TextBox>
                                    <asp:DropDownList ID="optV102" runat="server"    Width="340px"  onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV102','ctl00_cphMainMaster_txtV101')">
                                    </asp:DropDownList>
                                    
                                    </td>
                                <td   >
                                    <asp:TextBox ID="txtV103" runat="server" Columns="2"  MaxLength="2" TabIndex="10302" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >&nbsp;</td>
                            </tr>
                            <tr>
                                <td   >
                                    <asp:TextBox ReadOnly="true" ID="txtV104" runat="server" Columns="2"  MaxLength="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
                                <td   style="width: 384px">
                                    <asp:TextBox ID="txtV105" runat="server" ReadOnly="True"  Width="5px" style="visibility:hidden;" >0</asp:TextBox>
                                    <asp:DropDownList ID="optV105" runat="server"   Width="340px"  onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV105','ctl00_cphMainMaster_txtV104')">
                                    </asp:DropDownList>
                                    
                                    </td>
                                <td  >
                                    <asp:TextBox ID="txtV106" runat="server" Columns="2"  MaxLength="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                                <td  >&nbsp;</td>
                            </tr>
                        </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:justify;">
                                    <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Text="2. <i>Lengua(s) de la comunidad</i>. Escriba el nombre de la(s) lengua(s) materna(s) que se habla(n) en la comunidad."></asp:Label></td>
                            </tr>
                            <tr>
                                <td><table>
                                    <tr>
                                        <td  >
                                            <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Text="Clave" Width="100%"></asp:Label></td>
                                        <td   style="width: 405px">
                                            <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Text="Lengua Materna"
                                                Width="100%"></asp:Label></td>
                                        <td  >
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td   >
                                            <asp:TextBox ReadOnly="true" ID="txtV107" runat="server" Columns="2"  MaxLength="2" CssClass="lblNegro" 
                                                TabIndex="10501"></asp:TextBox></td>
                                        <td    style="width: 405px">
                                            <asp:TextBox ID="txtV108" runat="server" ReadOnly="True" Width="5px" style="visibility:hidden;">0</asp:TextBox>
                                            <asp:DropDownList ID="optV108" runat="server"    Width="340px"  onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV108','ctl00_cphMainMaster_txtV107')">
                                            </asp:DropDownList>
                                            
                                        </td>
                                        <td  >
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td   >
                                            <asp:TextBox ReadOnly="true" ID="txtV109" runat="server"  CssClass="lblNegro" Columns="2"  MaxLength="2"
                                                TabIndex="10601"></asp:TextBox></td>
                                        <td   style="width: 405px">
                                            <asp:TextBox ID="txtV110" runat="server" ReadOnly="True" Width="5px" style="visibility:hidden;">0</asp:TextBox>
                                            <asp:DropDownList ID="optV110" runat="server"    Width="340px"  onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV110','ctl00_cphMainMaster_txtV109')">
                                            </asp:DropDownList>
                                            
                                        </td>
                                        <td  >
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td   >
                                            <asp:TextBox ReadOnly="true" ID="txtV111" runat="server" Columns="2"  MaxLength="2" CssClass="lblNegro" 
                                                TabIndex="10701"></asp:TextBox></td>
                                        <td    style="width: 405px">
                                            <asp:TextBox ID="txtV112" runat="server" ReadOnly="True" Width="5px" style="visibility:hidden;">0</asp:TextBox>
                                            <asp:DropDownList ID="optV112" runat="server"   Width="340px"  onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV112','ctl00_cphMainMaster_txtV111')">
                                            </asp:DropDownList>
                                            
                                        </td>
                                        <td  >
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td   >
                                            <asp:TextBox ID="txtV113" runat="server" Columns="2"  MaxLength="2" CssClass="lblNegro" 
                                                TabIndex="10801"></asp:TextBox></td>
                                        <td   style="width: 405px">
                                            <asp:TextBox ID="txtV114" runat="server" ReadOnly="True" Width="5px" style="visibility:hidden;">0</asp:TextBox>
                                            <asp:DropDownList ID="optV114" runat="server"    Width="340px"  onchange="OnChangeDropXid(this,'ctl00_cphMainMaster_txtV114','ctl00_cphMainMaster_txtV113')">
                                            </asp:DropDownList>
                                            
                                        </td>
                                        <td  >
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:justify;">
                                    <asp:Label ID="Label3" runat="server" CssClass="lblRojo" Text="3. <i>Dominio de la lengua</i>. Escriba la cantidad de personal que habla, lee y escribe la lengua materna de la comunidad."></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 274px">
                                        <tr>
                                            <td align="left" style="width: 146px">
                                            </td>
                                            <td style="width: 41px">
                                                <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Text="Habla" Width="100%"></asp:Label></td>
                                            <td style="width: 40px">
                                                <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Text="Lee" Width="100%"></asp:Label></td>
                                            <td>
                                                <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Text="Escribe" Width="100%"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 146px">
                                                <asp:Label ID="Label52" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                                    Text="DIRECTOR CON GRUPO" Width="99%"></asp:Label></td>
                                            <td style="width: 41px">
                                                <asp:TextBox ID="txtV115" runat="server" MaxLength="2" TabIndex="10901" Width="26px"></asp:TextBox></td>
                                            <td style="width: 40px">
                                                <asp:TextBox ID="txtV116" runat="server" MaxLength="2" TabIndex="10902" Width="26px"></asp:TextBox></td>
                                            <td>
                                                <asp:TextBox ID="txtV117" runat="server" MaxLength="2" TabIndex="10903" Width="26px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 146px">
                                                <asp:Label ID="Label53" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                                    Text="DIRECTOR SIN GRUPO" Width="96%"></asp:Label></td>
                                            <td style="width: 41px">
                                                <asp:TextBox ID="txtV118" runat="server" MaxLength="2" TabIndex="11001" Width="26px"></asp:TextBox></td>
                                            <td style="width: 40px">
                                                <asp:TextBox ID="txtV119" runat="server" MaxLength="2" TabIndex="11002" Width="26px"></asp:TextBox></td>
                                            <td>
                                                <asp:TextBox ID="txtV120" runat="server" MaxLength="2" TabIndex="11003" Width="26px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 146px">
                                                <asp:Label ID="Label54" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                                    Text="DOCENTE" Width="53%"></asp:Label></td>
                                            <td style="width: 41px">
                                                <asp:TextBox ID="txtV121" runat="server" MaxLength="2" TabIndex="11101" Width="26px"></asp:TextBox></td>
                                            <td style="width: 40px">
                                                <asp:TextBox ID="txtV122" runat="server" MaxLength="2" TabIndex="11102" Width="26px"></asp:TextBox></td>
                                            <td>
                                                <asp:TextBox ID="txtV123" runat="server" MaxLength="2" TabIndex="11103" Width="26px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 146px">
                                                <asp:Label ID="Label55" runat="server" CssClass="lblRojo" Font-Bold="True" Height="14px"
                                                    Text="PROMOTOR" Width="58%"></asp:Label></td>
                                            <td style="width: 41px">
                                                <asp:TextBox ID="txtV124" runat="server" MaxLength="2" TabIndex="11201" Width="26px"></asp:TextBox></td>
                                            <td style="width: 40px">
                                                <asp:TextBox ID="txtV125" runat="server" MaxLength="2" TabIndex="11202" Width="26px"></asp:TextBox></td>
                                            <td>
                                                <asp:TextBox ID="txtV126" runat="server" MaxLength="2" TabIndex="11203" Width="26px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 146px">
                                                <asp:Label ID="Label56" runat="server" CssClass="lblGrisTit" Font-Bold="True" Height="14px"
                                                    Text="TOTAL" Width="38%"></asp:Label></td>
                                            <td style="width: 41px">
                                                <asp:TextBox ID="txtV127" runat="server" Columns="2"  MaxLength="2" TabIndex="11301" Width="26px"></asp:TextBox></td>
                                            <td style="width: 40px">
                                                <asp:TextBox ID="txtV128" runat="server" Columns="2"  MaxLength="2" TabIndex="11302" Width="26px"></asp:TextBox></td>
                                            <td>
                                                <asp:TextBox ID="txtV129" runat="server" Columns="2"  MaxLength="2" TabIndex="11303" Width="26px"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </table>
                        <hr />
                        <table align="center">
                            <tr>
                                <td ><span  onclick="openPage('AYG_911_111',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                                <td ><span  onclick="openPage('AYG_911_111',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                                <td style="width: 330px;">
                                   &nbsp;
                                </td>
                                <td ><span  onclick="openPage('Personal_911_111',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                                <td ><span  onclick="openPage('Personal_911_111',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
                            </tr>
                        </table>
                        
                        <div id="divResultado" class="divResultado" ></div>
                       
                        <br />
                        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
                        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
                        <input id="hidIdCtrl" type="hidden" runat= "server" />
                        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
                        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
                          <div id="divWait" style="width: 100%; height: 100%; position:fixed; top:0; left:0;" >
                                <div class="fondoDegradado">
                                 <br /><br /><br /><br />
                                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;background:#000;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                                 Este proceso puede tardar....</span>
                                 <br />
                                </div>
                         </div>
                           <asp:Panel ID="pnlFallas" runat="server">
                           </asp:Panel>
                           
                    <!--/Contenido dentro del area de trabajo-->
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			</table>
			
        <script type="text/javascript" language="javascript">
                MaxCol = 10;
                MaxRow = 30;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		        Disparador(<%=hidDisparador.Value %>);
 		        
 		       
        </script>
    </center>
</asp:Content>
