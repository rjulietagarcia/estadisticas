<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Gasto" AutoEventWireup="true" CodeBehind="Gasto_EI_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.EI_1.Gasto_EI_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   
    
<div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  
                      Text="EDUCACI�N INICIAL"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table></div></div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_EI_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Poblacion_EI_1',true)"><a href="#" title=""><span>ALUMNOS</span></a></li>
        <li onclick="openPage('Personal_EI_1',true)"><a href="#" title=""><span>PERSONAL</span></a></li>
        <li onclick="openPage('CMYA_EI_1',true)"><a href="#" title=""><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
        <li onclick="openPage('Gasto_EI_1',true)"><a href="#" title="" class="activo"><span>GASTO</span></a></li>
        <li onclick="openPage('Anexo_EI_1',false)"><a href="#" title=""><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title=""><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

<center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

        <table >
                            <tr>
                                <td style="width: 813px;text-align:justify">
                                    <asp:Label ID="lblGASTOS" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="V. GASTOS DE LAS FAMILIAS EN EDUCACI�N"
                                        Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify; width: 813px;">
                                    <asp:Label ID="lblInstruccionA" runat="server" CssClass="lblRojo" Text="a) La informaci�n de esta secci�n ser� utilizada exclusivamente para obtener indicadores de gasto educativo."
                                        Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify; width: 813px;">
                                    <asp:Label ID="lblInstruccionB" runat="server" CssClass="lblRojo" Text="b) El punto n�mero 1 deber� ser constestado por las escuelas de todos sostenimientos. El punto n�mero 2 �nicamente por las escuelas con sostenimiento particular."></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify; width: 813px;">
                                    <asp:Label ID="lblInstruccionC" runat="server" CssClass="lblRojo" Text="c) Presente las cifras en pesos; no utilice decimales."
                                        Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify; width: 813px;">
                                    <asp:Label ID="lblInstruccionD" runat="server" CssClass="lblRojo" Text="d) Para contestar, considere las definiciones siguientes. Si no cuenta con cantidades exactas, d� una aproximaci�n de las mismas."
                                        Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify; width: 813px;">
                                    <asp:Label ID="lblDefinicion1" runat="server" CssClass="lblGrisTit" Height="68px"
                                        Text="GASTO PROMEDIO ANUAL. Es el monto promedio de dinero que gasta cada alumno (o los padres del alumno) en un determinado concepto, durante el ciclo escolar. Se aplica a los siguientes conceptos: inscripci�n, paquetes de �tiles y libros (cuando �stos se soliciten) y uniformes. Asimismo, se aplican a cuotas que requieran un desembolso para las familias; por ejemplo, las aportaciones a la asociaci�n de padres de familia o alguna ayuda para el arreglo de la escuela o para equipar laboratorios y talleres, etc�tera."></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify; width: 813px;">
                                    <asp:Label ID="lblDefinicion2" runat="server" CssClass="lblGrisTit" Text="GASTO PROMEDIO MENSUAL. Es el monto promedio de dinero que gasta cada alumno (o los padres del alumno) en septiembre por conceptos de colegiatura y(o) transporte escolar en escuelas particulares. Es el resultado de dividir el total de los ingresos de la escuela en septiembre entre el total de alumnos. Por ejemplo: si el ingreso de la escuela por colegiaturas pagadas por las familias durante septiembre fue de $15,000 y el n�mero de alumnos es de 100, el gasto promedio mensual en colegiaturas es de $150, cantidad que se reportar� en el rubro correspondiente."></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 813px;text-align:justify">
                                    <asp:Label ID="lblInciso1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="1. ESCUELAS DE TODOS LOS SOSTENIMIENTOS"
                                        Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: justify; width: 813px;">
                                    <asp:Label ID="lblInstruccion1" runat="server" CssClass="lblRojo" Font-Bold="True"
                                        Text="En el caso de escuelas particulares, considere los gastos y compras que los alumnos y(o) padres de familia hacen directamente en la instituci�n, as� como fuera de ella."></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 813px;">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto11" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="GASTO PROMEDIO ANUAL EN EL PAQUETE DE �TILES Y LIBROS QUE SE SUGIERE ADQUIERA EL ALUMNO"
                                                    Width="600px"></asp:Label></td>
                                            <td style="text-align: right; width: 140px;">
                                                <asp:TextBox ID="txtV419" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="10101"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto12" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="GASTO PROMEDIO ANUAL EN UNIFORMES QUE SE SUGIERE ADQUIERA EL ALUMNO" Width="100%"></asp:Label></td>
                                            <td style="text-align: right; width: 140px;">
                                                <asp:TextBox ID="txtV420" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="10201"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto13" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="GASTO PROMEDIO ANUAL EN CUOTAS" Width="100%"></asp:Label></td>
                                            <td style="text-align: right; width: 140px;">
                                                <asp:TextBox ID="txtV421" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="10301"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 813px;text-align:justify">
                                    <asp:Label ID="lblInciso2" runat="server" CssClass="lblRojo" Font-Bold="True" Text="2. ESCUELAS PARTICULARES"
                                        Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 813px;">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto21" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="GASTO PROMEDIO ANUAL EN INSCRIPCI�N" Width="100%"></asp:Label></td>
                                            <td colspan="2" style="text-align: right">
                                                <asp:TextBox ID="txtV422" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="10401"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto22" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="GASTO PROMEDIO MENSUAL EN COLEGIATURA" Width="100%"></asp:Label></td>
                                            <td colspan="2" style="text-align: right">
                                                <asp:TextBox ID="txtV423" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="10501"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto23" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="N�MERO DE MENSUALIDADES QUE SE PAGAN" Width="100%"></asp:Label></td>
                                            <td colspan="2" style="text-align: right">
                                                <asp:TextBox ID="txtV424" runat="server" Columns="5" CssClass="lblNegro" MaxLength="2"
                                                    TabIndex="10601"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto24" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="�LA ESCUELA OFRECE SERVICIO DE TRANSPORTE ESCOLAR, PROPIO O CONCESIONADO?"
                                                    Width="580px"></asp:Label></td>
                                            <td style="text-align:right">
                                                <asp:TextBox ID="txtV425"  style="visibility:hidden;" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2"
                                                    ></asp:TextBox>
                                                    <asp:TextBox ID="txtV426"  style="visibility:hidden;" runat="server" Columns="1" CssClass="lblNegro" MaxLength="2"
                                                    ></asp:TextBox>
                                                <asp:RadioButton  onclick = "OPTs2Txt();" ID="opt425" runat="server" GroupName="TRANSPORTE" Text="SI"   />
                                                <asp:RadioButton  onclick = "OPTs2Txt();"  ID="opt426" runat="server" GroupName="TRANSPORTE" Text="NO"   />
                                                </td>
                                            
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 813px;text-align:justify">
                                    <asp:Label ID="lblInstrucion2" runat="server" CssClass="lblRojo" Font-Bold="True"
                                        Text="Si la respuesta es S�, conteste lo siguiente:" Width="100%"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="text-align: center; width: 813px;">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="height: 23px; text-align: left">
                                                &nbsp;</td>
                                            <td style="height: 23px; text-align: left">
                                                <asp:Label ID="lblGasto25" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="GASTO PROMEDIO MENSUAL EN SERVICIO DE TRANSPORTE" Width="600px"></asp:Label></td>
                                            <td style="height: 23px; text-align: right; width: 145px;">
                                                <asp:TextBox ID="txtV427" runat="server" Columns="5" CssClass="lblNegro" MaxLength="4" TabIndex="10701"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                &nbsp;</td>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto26" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="N�MERO DE MENSUALIDADES QUE SE PAGAN" Width="100%"></asp:Label></td>
                                            <td style="text-align: right; width: 145px;">
                                                <asp:TextBox ID="txtV428" runat="server" Columns="5" CssClass="lblNegro" MaxLength="2" TabIndex="10801"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left">
                                                &nbsp;</td>
                                            <td style="text-align: left">
                                                <asp:Label ID="lblGasto27" runat="server" CssClass="lblGrisTit" Font-Bold="True"
                                                    Text="N�MERO DE ALUMNOS QUE UTILIZAN EL SERVICIO." Width="100%"></asp:Label></td>
                                            <td style="text-align: right; width: 145px;">
                                                <asp:TextBox ID="txtV429" runat="server" Columns="5" CssClass="lblNegro" MaxLength="5" TabIndex="10901"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; width: 813px;">
                                </td>
                            </tr>
                        </table>
        
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('CMYA_EI_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('CMYA_EI_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Anexo_EI_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_EI_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado"  class="divResultado"  ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                MaxCol = 20;
                MaxRow = 20;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                  function OPTs2Txt(){
                     marcarTXT('425');
                     marcarTXT('426');
                } 
                function PintaOPTs(){
                     marcar('425');
                     marcar('426');
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_opt'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                
              PintaOPTs();
                Disparador(<%=this.hidDisparador.Value %>);
        </script> 
</asp:Content>
