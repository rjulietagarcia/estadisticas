<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Poblacion" AutoEventWireup="true" CodeBehind="Poblacion_EI_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.EI_1.Poblacion_EI_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

 
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 17;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
   
<div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server"  
                      Text="EDUCACI�N INICIAL"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table>
    </div></div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_EI_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Poblacion_EI_1',true)"><a href="#" title="" class="activo"><span>ALUMNOS</span></a></li><li onclick="openPage('Personal_EI_1',false)"><a href="#" title=""><span>PERSONAL</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
     <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

<center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 1204px">
            <tr>
                <td style="width: 400px">
                    <table style="width: 400px">
                        <tr>
                            <td style="text-align: justify; width: 400px;">
                                <asp:Label ID="lblIncisoI" runat="server" CssClass="lblGrisTit" Text="I. INFORMACI�N GENERAL"
                                    Width="100%" Font-Size="16px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 400px">
                                <asp:Label ID="lblInstruccionI1" runat="server" CssClass="lblRojo" Text="1. Marque las secciones que atiende el CENDI."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 400px;" valign="bottom">
                                <table style="width: 350px">
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblLactantesI" runat="server" CssClass="lblGrisTit" Height="17px" Text="LACTANTES"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblMaternalesI" runat="server" CssClass="lblGrisTit" Height="17px"
                                                Text="MATERNALES" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblPreescolar" runat="server" CssClass="lblGrisTit" Height="17px"
                                                Text="PREESCOLAR" Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:CheckBox ID="optV1" onclick ="OPTs2Txt()" runat="server" Text=" " />
                                            <asp:TextBox ID="txtV1" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px"></asp:TextBox>
                                            </td>
                                        <td style="text-align: center">
                                            <asp:CheckBox ID="optV2" onclick ="OPTs2Txt()" runat="server" Text=" " />
                                            <asp:TextBox ID="txtV2" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:CheckBox ID="optV3" onclick ="OPTs2Txt()" runat="server" Text=" " />
                                            <asp:TextBox ID="txtV3" runat="server" Columns="1" CssClass="lblNegro" MaxLength="1"
                                                style="visibility:hidden; width:1px"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 400px;">
                                <asp:Label ID="lblInstruccionI2" runat="server" CssClass="lblRojo" Text="2. Escriba la cantidad de solicitudes de inscripci�n pendientes."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; width: 400px;">
                                <asp:TextBox ID="txtV4" runat="server" Columns="3" MaxLength="3" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 400px;">
                                <asp:Label ID="lblInstruccionI3" runat="server" CssClass="lblRojo" Text="3. De la existencia, escriba el n�mero de los alumnos atendidos por la Unidad de Servicios de Apoyo a la Educaci�n Regular (USAER), desglos�ndolos por sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 400px;" valign="middle">
                                <table style="width: 350px">
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label1" runat="server" CssClass="lblGrisTit"  Text="NI�OS"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label2" runat="server" CssClass="lblGrisTit"  Text="NI�AS"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblTotal3" runat="server" CssClass="lblGrisTit"  Text="TOTAL"
                                                    Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV5" runat="server" TabIndex="10201" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV6" runat="server"  TabIndex="10202" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="text-align: center">
                                            <asp:TextBox ID="txtV7" runat="server" TabIndex="10203" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 400px;">
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" 
                                Text="4. De la existencia, 
                                escriba la cantidad de alumnos 
                                con discapacidad, aptitudes sobresalientes u otras condiciones, 
                                desglos�ndolos por sexo."
                                    ></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 400px;" valign="middle">
                                <table style="width: 365px; height: 1px">
                                    <tr>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblSituaci�n" runat="server" CssClass="lblNegro" Font-Bold="True"
                                                Height="17px" Text="SITUACI�N DEL ALUMNO" Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="lblNi�os4" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�OS"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�AS"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblTotal4" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                                                Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px; text-align: left">
                                            <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Height="17px" Text="CEGUERA"
                                                Width="100%"></asp:Label></td>
                                        <td style="height: 19px">
                                            <asp:TextBox ID="txtV8" runat="server" TabIndex="10301" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="height: 19px">
                                            <asp:TextBox ID="txtV9" runat="server" TabIndex="10302" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="height: 19px">
                                            <asp:TextBox ID="txtV10" runat="server" TabIndex="10303" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblVisual" runat="server" CssClass="lblGrisTit" Height="17px" Text="BAJA VISI�N"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV11" runat="server" Columns="3" TabIndex="10401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV12" runat="server" Columns="3" MaxLength="2" TabIndex="10402" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV13" runat="server" Columns="3" MaxLength="3" TabIndex="10403" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Height="17px" Text="SORDERA"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV14" runat="server" TabIndex="10501" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV15" runat="server" TabIndex="10502" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV16" runat="server" TabIndex="10503" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblAuditiva" runat="server" CssClass="lblGrisTit" Height="17px" Text="HIPOACUSIA"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV17" runat="server" TabIndex="10601" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV18" runat="server" TabIndex="10602" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV19" runat="server" TabIndex="10603" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px; text-align: left">
                                            <asp:Label ID="lblMotriz" runat="server" CssClass="lblGrisTit" Height="17px" Text="DISCAPACIDAD MOTRIZ"
                                                Width="100%"></asp:Label></td>
                                        <td style="height: 19px">
                                            <asp:TextBox ID="txtV20" runat="server" TabIndex="10701" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="height: 19px">
                                            <asp:TextBox ID="txtV21" runat="server" TabIndex="10702" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="height: 19px">
                                            <asp:TextBox ID="txtV22" runat="server" TabIndex="10703" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblIntelectual" runat="server" CssClass="lblGrisTit" Height="17px"
                                                Text="DISCAPACIDAD INTELECTUAL" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV23" runat="server" TabIndex="10801" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV24" runat="server" TabIndex="10802" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV25" runat="server" TabIndex="10803" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblOtra" runat="server" CssClass="lblGrisTit" Height="17px" Text="OTRAS CONDICIONES"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV26" runat="server" TabIndex="10901" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV27" runat="server" TabIndex="10902" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV28" runat="server" TabIndex="10903" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblCAS" runat="server" CssClass="lblGrisTit" Height="100%" Text="APTITUDES SOBRESALIENTES"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV29" runat="server" TabIndex="11001" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV30" runat="server" TabIndex="11002" Columns="3" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV31" runat="server" TabIndex="11003" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblTotalT4" runat="server" CssClass="lblGrisTit" Height="17px"
                                                Text="TOTAL" Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV32" runat="server" TabIndex="11101" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV33" runat="server" TabIndex="11102" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV34" runat="server" TabIndex="11103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: justify; width: 400px;">
                                <asp:Label ID="lblInstruccionI5" runat="server" CssClass="lblRojo" Text="5. Escriba, por sexo, el n�mero de alumnos con necesidades Educativas Especiales (NEE), independientemente de que presenten o no alguna discapacidad."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 400px;" valign="middle"><table style="width: 350px">
                                <tr>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblNi�os5" runat="server"   CssClass="lblGrisTit"  Text="NI�OS"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblNi�as5" runat="server"   CssClass="lblGrisTit"  Text="NI�AS"
                                                Width="100%"></asp:Label></td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblTotal5" runat="server"   CssClass="lblGrisTit"  Text="TOTAL"
                                                Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center" valign="middle">
                                        <asp:TextBox ID="txtV35" runat="server" Columns="2" TabIndex="11201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    <td style="text-align: center" valign="middle">
                                        <asp:TextBox ID="txtV36" runat="server" Columns="2" TabIndex="11202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    <td style="text-align: center" valign="middle">
                                        <asp:TextBox ID="txtV37" runat="server" Columns="3" TabIndex="11203" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: left; width: 700px;" valign="top">
                    <table style="width: 95px">
                        <tr>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="lblIncisoII" runat="server" CssClass="lblGrisTit" Text="II. POBLACI�N INFANTIL ATENDIDA EN LAS SECCIONES DE LACTANTES Y MATERNALES"
                                    Width="100%" Font-Size="16px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left; height: 19px;">
                                <asp:Label ID="lblInstruccionII1" runat="server" CssClass="lblRojo" Text="1. Desglose la existencia de los alumnos en lactantes y maternales, por estrato de edad y sexo."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="lblInstruccionII2" runat="server" CssClass="lblRojo" Text="2. Grupos: Escriba, por secci�n, la cantidad de grupos existenctes, seg�n el estrato de edad."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="lblImportante" runat="server" CssClass="lblRojo" Text="Importante: Consulte la tabla de estratos de edad en la secci�n de glosario."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 177px; text-align: center;">
                                <table>
                                    <tr>
                                        <td style="height: 19px">
                                        </td>
                                        <td colspan="4" style="height: 19px; text-align: center">
                                <asp:Label ID="lblLactantesII" runat="server" CssClass="lblNegro"  Text="LACTANTES"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px">
                                        </td>
                                        <td colspan="4" style="text-align: center; height: 19px;">
                                            <asp:Label ID="lblEstratoL" runat="server" CssClass="lblNegro" Height="17px" Text="ESTRATO DE EDAD"
                                                Width="100%" Font-Bold="True"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Height="17px" Text="1"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Height="17px" Text="2"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Height="17px" Text="3"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label11" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                                                Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblNi�osII1" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�OS"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV38" runat="server" Columns="3" TabIndex="20101" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV39" runat="server" Columns="3" TabIndex="20102" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV40" runat="server" Columns="3" TabIndex="20103" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV41" runat="server" Columns="3" TabIndex="20104" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblNi�asII1" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�AS"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV42" runat="server" Columns="3" TabIndex="20201" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV43" runat="server" Columns="3" TabIndex="20202" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV44" runat="server" Columns="3" TabIndex="20203" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV45" runat="server" Columns="3" TabIndex="20204" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblSubtotalII1" runat="server" CssClass="lblGrisTit" Height="17px" Text="SUBTOTAL"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                            <asp:TextBox ID="txtV46" runat="server" Columns="3" TabIndex="20301" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV47" runat="server" Columns="3" TabIndex="20302" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV48" runat="server" Columns="3" TabIndex="20303" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                            <asp:TextBox ID="txtV49" runat="server" Columns="3" TabIndex="20304" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblGruposII1" runat="server" CssClass="lblGrisTit" Height="17px" Text="GRUPOS"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: right">
                                            <asp:TextBox ID="txtV50" runat="server" Columns="3" TabIndex="20401" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="text-align: right">
                                            <asp:TextBox ID="txtV51" runat="server" Columns="3" TabIndex="20402" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="text-align: right">
                                            <asp:TextBox ID="txtV52" runat="server" Columns="3" TabIndex="20403" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="text-align: right">
                                            <asp:TextBox ID="txtV53" runat="server" Columns="3" TabIndex="20404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="height: 177px; width: 305px; text-align: center;">
                                <table>
                                    <tr>
                                        <td style="height: 19px">
                                        </td>
                                        <td colspan="4" style="height: 19px; text-align: center">
                                <asp:Label ID="lblMaternalesII" runat="server" CssClass="lblNegro"   Text="MATERNAL"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 19px">
                                        </td>
                                        <td colspan="4" style="text-align: center; height: 19px;">
                                            <asp:Label ID="lblEstratoM" runat="server" CssClass="lblNegro" Height="17px" Text="ESTRATO DE EDAD"
                                                Width="100%" Font-Bold="True"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td style="width: 57px; text-align: center">
                                            <asp:Label ID="Label24" runat="server" CssClass="lblGrisTit" Height="17px" Text="1"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label25" runat="server" CssClass="lblGrisTit" Height="17px" Text="2"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Height="17px" Text="3"
                                                Width="100%"></asp:Label></td>
                                        <td style="text-align: center">
                                            <asp:Label ID="Label50" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                                                Width="100%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px; text-align: left">
                                            <asp:Label ID="lblNi�osII2" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�OS"
                                                Width="100%"></asp:Label></td>
                                        <td style="height: 26px; width: 57px;">
                                        <asp:TextBox ID="txtV54" runat="server" Columns="3" TabIndex="20501" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="height: 26px">
                                        <asp:TextBox ID="txtV55" runat="server" Columns="3" TabIndex="20502" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="height: 26px">
                                        <asp:TextBox ID="txtV56" runat="server" Columns="3" TabIndex="20503" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td style="height: 26px">
                                        <asp:TextBox ID="txtV57" runat="server" Columns="3" TabIndex="20504" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblNi�asII2" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�AS"
                                                Width="100%"></asp:Label></td>
                                        <td style="width: 57px">
                                        <asp:TextBox ID="txtV58" runat="server" Columns="3" TabIndex="20601" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV59" runat="server" Columns="3" TabIndex="20602" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV60" runat="server" Columns="3" TabIndex="20603" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV61" runat="server" Columns="3" TabIndex="20604" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblSubtotalII2" runat="server" CssClass="lblGrisTit" Height="17px" Text="SUBTOTAL"
                                                Width="100%"></asp:Label></td>
                                        <td style="width: 57px">
                                        <asp:TextBox ID="txtV62" runat="server" Columns="3" TabIndex="20701" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV63" runat="server" Columns="3" TabIndex="20702" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV64" runat="server" Columns="3" TabIndex="20703" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV65" runat="server" Columns="3" TabIndex="20704" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblGruposII2" runat="server" CssClass="lblGrisTit" Height="17px" Text="GRUPOS"
                                                Width="100%"></asp:Label></td>
                                        <td>
                                        <asp:TextBox ID="txtV66" runat="server" Columns="3" TabIndex="20801" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV67" runat="server" Columns="3" TabIndex="20802" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV68" runat="server" Columns="3" TabIndex="20803" MaxLength="1" CssClass="lblNegro"></asp:TextBox></td>
                                        <td>
                                        <asp:TextBox ID="txtV69" runat="server" Columns="3" TabIndex="20804" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="lblIncisoIII" runat="server" CssClass="lblGrisTit" Text="III. POBLACI�N INFANTIL ATENDIDA EN LAS SECCION DE PREESCOLAR"
                                    Width="100%" Font-Size="16px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="lblInstruccionIII1" runat="server" CssClass="lblRojo" Text="1. Escriba la existencia de los alumnos, por edad y sexo, atendidos en la secci�n de preescolar, y el n�mero de grupos, por grado. Verifique que la suma de los subtotales de los alumnos por edad, sea igual al total."
                                    Width="700px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <table style="width: 106px">
                        <tr>
                            <td>
                    <table style="width: 83px">
                        <tr>
                            <td style="width: 3px; height: 17px;">
                            </td>
                            <td style="height: 17px">
                            </td>
                            <td style="height: 17px">
                                <asp:Label ID="lbl3a" runat="server" CssClass="lblGrisTit" Height="17px" Text="3 a�os"
                                    Width="80px"></asp:Label></td>
                            <td style="height: 17px">
                                <asp:Label ID="lbl4a" runat="server" CssClass="lblGrisTit" Height="17px" Text="4 a�os"
                                    Width="80px"></asp:Label></td>
                            <td style="height: 17px">
                                <asp:Label ID="lbl5a" runat="server" CssClass="lblGrisTit" Height="17px" Text="5 a�os"
                                    Width="80px"></asp:Label></td>
                            <td style="height: 17px">
                                <asp:Label ID="lbl6a" runat="server" CssClass="lblGrisTit" Height="17px" Text="6 a�os"
                                    Width="80px"></asp:Label></td>
                            <td style="height: 17px">
                                <asp:Label ID="lblTotalIII3" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                                    Width="80px"></asp:Label></td>
                            <td style="height: 17px">
                                <asp:Label ID="Label45" runat="server" CssClass="lblGrisTit" Height="17px" Text="GRUPOS"
                                    Width="80px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 3px; text-align: left" rowspan="3">
                                <asp:Label ID="lbl1o" runat="server" CssClass="lblNegro" Height="1px" Text="1o."
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNi�osIII1" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�OS"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV70" runat="server" TabIndex="30101" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV71" runat="server" TabIndex="30102" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV72" runat="server" TabIndex="30103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV73" runat="server" TabIndex="30104" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV74" runat="server" TabIndex="30105" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNi�asIII1" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�AS"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV75" runat="server" TabIndex="30201" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV76" runat="server" TabIndex="30202" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV77" runat="server" TabIndex="30203" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV78" runat="server" TabIndex="30204" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV79" runat="server" TabIndex="30205" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblStotalIII1" runat="server" CssClass="lblGrisTit" Height="17px" Text="SUBTOTAL"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV80" runat="server" TabIndex="30301" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV81" runat="server" TabIndex="30302" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV82" runat="server" TabIndex="30303" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV83" runat="server" TabIndex="30304" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV84" runat="server" TabIndex="30305" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV85" runat="server" TabIndex="30306" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 3px; text-align: left" rowspan="3">
                                <asp:Label ID="lbl2o" runat="server" CssClass="lblNegro" Height="17px" Text="2o."
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNi�osIII2" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�OS"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV86" runat="server" TabIndex="30401" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV87" runat="server" TabIndex="30402" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV88" runat="server" TabIndex="30403" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV89" runat="server" TabIndex="30404" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV90" runat="server" TabIndex="30405" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNi�asIII2" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�AS"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV91" runat="server" TabIndex="30501" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV92" runat="server" TabIndex="30502" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV93" runat="server" TabIndex="30503" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV94" runat="server" TabIndex="30504" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV95" runat="server" TabIndex="30505" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblStotalIII2" runat="server" CssClass="lblGrisTit" Height="17px" Text="SUBTOTAL"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV96" runat="server" TabIndex="30601" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV97" runat="server" TabIndex="30602" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV98" runat="server" TabIndex="30603" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV99" runat="server" TabIndex="30604" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV100" runat="server" TabIndex="30605" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV101" runat="server" TabIndex="30606" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 3px; text-align: left" rowspan="3">
                                <asp:Label ID="Label48" runat="server" CssClass="lblNegro" Height="17px" Text="3o."
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNi�osIII3" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�OS"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV102" runat="server" TabIndex="30701" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV103" runat="server" TabIndex="30702" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV104" runat="server" TabIndex="30703" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV105" runat="server" TabIndex="30704" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV106" runat="server" TabIndex="30705" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblNi�asIII3" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�AS"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV107" runat="server" TabIndex="30801" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV108" runat="server" TabIndex="30802" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV109" runat="server" TabIndex="30803" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV110" runat="server" TabIndex="30804" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV111" runat="server" TabIndex="30805" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblStotalIII3" runat="server" CssClass="lblGrisTit" Height="17px" Text="SUBTOTAL"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV112" runat="server" TabIndex="30901" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV113" runat="server" TabIndex="30902" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV114" runat="server" TabIndex="30903" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV115" runat="server" TabIndex="30904" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV116" runat="server" TabIndex="30905" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV117" runat="server" TabIndex="30906" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 3px; text-align: left" rowspan="3">
                                <asp:Label ID="lblTotalIII1" runat="server" CssClass="lblNegro" Height="17px" Text="TOTAL"
                                    Width="100%" Font-Bold="True"></asp:Label></td>
                            <td style="text-align: left">
                                <asp:Label ID="lblNi�osIIIt" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�OS"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV118" runat="server" TabIndex="31001" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV119" runat="server" TabIndex="31002" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV120" runat="server" TabIndex="31003" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV121" runat="server" TabIndex="31004" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV122" runat="server" TabIndex="31005" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; height: 26px;">
                                <asp:Label ID="lblNi�asIIIt" runat="server" CssClass="lblGrisTit" Height="17px" Text="NI�AS"
                                    Width="100%"></asp:Label></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtV123" runat="server" TabIndex="31101" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtV124" runat="server" TabIndex="31102" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtV125" runat="server" TabIndex="31103" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtV126" runat="server" TabIndex="31104" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px">
                                <asp:TextBox ID="txtV127" runat="server" TabIndex="31105" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td style="height: 26px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <asp:Label ID="lblTotalIII2" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                                    Width="100%"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtV128" runat="server" TabIndex="31201" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV129" runat="server" TabIndex="31202" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV130" runat="server" TabIndex="31203" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV131" runat="server" TabIndex="31204" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV132" runat="server" TabIndex="31205" Columns="3" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                            <td>
                                <asp:TextBox ID="txtV133" runat="server" TabIndex="31206" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_EI_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_EI_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Personal_EI_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_EI_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                function OPTs2Txt(){
                     marcarTXT('1');
                     marcarTXT('2'); 
                     marcarTXT('3');
                } 
                function PintaOPTs(){
                     marcar('1');
                     marcar('2'); 
                     marcar('3');
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                 PintaOPTs();
 		Disparador(<%=hidDisparador.Value %>);
          
        </script> 
        <script type="text/javascript">
         GetTabIndexes();
         </script>
</asp:Content>
