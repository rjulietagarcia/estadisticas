<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Personal" AutoEventWireup="true" CodeBehind="Personal_EI_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.EI_1.Personal_EI_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

   
     <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 17;
        MaxRow = 38;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
   
<div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                <asp:Label ID="lblNivel" runat="server" 
                    Text="EDUCACI�N INICIAL"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table></div></div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_EI_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Poblacion_EI_1',true)"><a href="#" title=""><span>ALUMNOS</span></a></li><li onclick="openPage('Personal_EI_1',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li><li onclick="openPage('CMYA_EI_1',false)"><a href="#" title=""><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
     <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

<center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>

        
        <table style="width: 1000px">
        <tr>
            <td>
                <table style="width: 450px">
                    <tr>
                        <td style="text-align:justify;">
                            <asp:Label ID="lblIncisoI" runat="server" CssClass="lblGrisTit" Text="IV. PERSONAL POR FUNCI�N"
                                Width="100%" Font-Size="16px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: justify">
                            <asp:Label ID="lblInstuccion1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de personal de servicio pedag�gico, desglos�ndolo por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
    <table style="width: 100%">
        <tr>
            <td>
            </td>
            <td style="text-align: center">
                    <asp:Label ID="lblHombres1" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                    <asp:Label ID="lblMujeres11" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                    <asp:Label ID="lblTotal11" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                    <asp:Label ID="lblPedagogica" runat="server" CssClass="lblGrisTit" Text="JEFE DEL �REA PEDAG�GICA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                    <asp:TextBox ID="txtV134" runat="server" TabIndex="10101" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                    <asp:TextBox ID="txtV135" runat="server" TabIndex="10102" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                    <asp:TextBox ID="txtV136" runat="server" TabIndex="10103"  Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; height: 25px;">
                    <asp:Label ID="lblMusical" runat="server" CssClass="lblGrisTit" Text="PROFESORES DE ENSE�ANZA MUSICAL" Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 25px;">
                    <asp:TextBox ID="txtV137" runat="server" TabIndex="10201" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; height: 25px;">
                    <asp:TextBox ID="txtV138" runat="server" TabIndex="10202" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                    <asp:TextBox ID="txtV139" runat="server" TabIndex="10203" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                    <asp:Label ID="lblEdFisica" runat="server" CssClass="lblGrisTit" Text="PROFESORES DE EDUCACI�N F�SICA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                    <asp:TextBox ID="txtV140" runat="server" TabIndex="10301" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                    <asp:TextBox ID="txtV141" runat="server" TabIndex="10302" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                    <asp:TextBox ID="txtV142" runat="server" TabIndex="10303" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                    </td>
            <td colspan="2">
                    <asp:Label ID="lblStotal1" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL (1)" Width="100%"></asp:Label></td>
            <td style="text-align: right">
                    <asp:TextBox ID="txtV143" runat="server" TabIndex="10401" Columns="2" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: justify">
                            <asp:Label ID="lblInstuccion2" runat="server" CssClass="lblRojo" Text="2. Divida a los puericultores por secci�n y estrato de edad, desglos�ndolos por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
    <table style="width: 100%">
        <tr>
            <td colspan="1" style="text-align: center; width: 135px;">
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblLactantes2" runat="server" CssClass="lblNegro" Text="LACTANTES" Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblMaternales2" runat="server" CssClass="lblNegro" Text="MATERNALES" Width="100%" Font-Bold="True"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 135px">
            </td>
            <td style="width: 3px; text-align: center;">
                <asp:Label ID="lblHombres21" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres21" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres22" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres22" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal21" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 135px; text-align: center">
                <asp:Label ID="lbl21" runat="server" CssClass="lblGrisTit" Text="1" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV144" runat="server" Columns="2"  TabIndex="10501" MaxLength="2" CssClass="lblNegro" ></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV145" runat="server" Columns="2" TabIndex="10502" MaxLength="2" CssClass="lblNegro" ></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV146" runat="server" Columns="2" TabIndex="10503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV147" runat="server" Columns="2" TabIndex="10504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV148" runat="server" Columns="2" TabIndex="10505" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 135px; text-align: center">
                <asp:Label ID="lbl22" runat="server" CssClass="lblGrisTit" Text="2" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV149" runat="server" Columns="2" TabIndex="10601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV150" runat="server" Columns="2" TabIndex="10602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV151" runat="server" Columns="2" TabIndex="10603" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV152" runat="server" Columns="2" TabIndex="10604" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV153" runat="server" Columns="2" TabIndex="10605" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 135px; text-align: center">
                <asp:Label ID="lbl23" runat="server" CssClass="lblGrisTit" Text="3" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV154" runat="server" Columns="2" TabIndex="10701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV155" runat="server" Columns="2" TabIndex="10702" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV156" runat="server" Columns="2" TabIndex="10703" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV157" runat="server" Columns="2" TabIndex="10704" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV158" runat="server" Columns="2" TabIndex="10705" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 135px">
            </td>
            <td style="width: 3px">
            </td>
            <td>
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblStotal2" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL (2)"
                    Width="100%"></asp:Label></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV159" runat="server" Columns="2" TabIndex="10801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: justify">
                            <asp:Label ID="lblInstuccion3" runat="server" CssClass="lblRojo" Text="3. Separe a los educadores por secci�n y estrato de edad, desglos�ndolos por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
    <table style="width: 100%">
        <tr>
            <td colspan="1" style="text-align: center">
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblLactantes3" runat="server" CssClass="lblNegro" Text="LACTANTES" Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblMaternales3" runat="server" CssClass="lblNegro" Text="MATERNALES" Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblPreescolar3" runat="server" CssClass="lblNegro" Text="PREESCOLAR" Width="100%" Font-Bold="True"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 3px; height: 17px;">
            </td>
            <td style="width: 3px; text-align: center; height: 17px;">
                <asp:Label ID="lblHombres31" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 17px;">
                <asp:Label ID="lblMujeres31" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 17px;">
                <asp:Label ID="lblHombres32" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 17px;">
                <asp:Label ID="lblMujeres32" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 17px;">
                <asp:Label ID="lblHombres33" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 17px;">
                <asp:Label ID="lblMujeres33" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center; height: 17px;">
                <asp:Label ID="lblTotal31" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 3px; text-align: center">
                <asp:Label ID="lbl31" runat="server" CssClass="lblGrisTit" Text="1" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV160" runat="server" Columns="2" TabIndex="10901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV161" runat="server" Columns="2" TabIndex="10902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV162" runat="server" Columns="2" TabIndex="10903" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV163" runat="server" Columns="2" TabIndex="10904" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV164" runat="server" Columns="2" TabIndex="10905" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV165" runat="server" Columns="2" TabIndex="10906" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV166" runat="server" Columns="2" TabIndex="10907" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 3px; text-align: center">
                <asp:Label ID="lbl32" runat="server" CssClass="lblGrisTit" Text="2" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV167" runat="server" Columns="2" TabIndex="11001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV168" runat="server" Columns="2" TabIndex="11002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV169" runat="server" Columns="2" TabIndex="11003" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV170" runat="server" Columns="2" TabIndex="11004" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV171" runat="server" Columns="2" TabIndex="11005" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV172" runat="server" Columns="2" TabIndex="11006" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV173" runat="server" Columns="2" TabIndex="11007" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 3px; text-align: center">
                <asp:Label ID="lbl33" runat="server" CssClass="lblGrisTit" Text="3" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV174" runat="server" Columns="2" TabIndex="11101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV175" runat="server" Columns="2" TabIndex="11102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV176" runat="server" Columns="2" TabIndex="11103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV177" runat="server" Columns="2" TabIndex="11104" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV178" runat="server" Columns="2" TabIndex="11105" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV179" runat="server" Columns="2" TabIndex="11106" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV180" runat="server" Columns="2" TabIndex="11107" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblStotal3" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL (3)"
                    Width="100%"></asp:Label></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV181" runat="server" Columns="2" TabIndex="11201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: justify">
                            <asp:Label ID="lblInstuccion4" runat="server" CssClass="lblRojo" Text="4. Separe a los asistentes educativos por secci�n y estrato de edad, desglos�ndolos por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
    <table style="width: 100%">
        <tr>
            <td colspan="1" style="text-align: center">
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblLactantes4" runat="server" CssClass="lblNegro" Text="LACTANTES" Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblMaternales4" runat="server" CssClass="lblNegro" Text="MATERNALES" Width="100%" Font-Bold="True"></asp:Label></td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblPreescolar4" runat="server" CssClass="lblNegro" Text="PREESCOLAR" Width="100%" Font-Bold="True"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 3px">
            </td>
            <td style="width: 3px; text-align: center;">
                <asp:Label ID="lblHombres41" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres41" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres42" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres42" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres43" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres43" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal41" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 3px; text-align: center">
                <asp:Label ID="lbl41" runat="server" CssClass="lblGrisTit" Text="1" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV182" runat="server" Columns="2" TabIndex="11301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV183" runat="server" Columns="2" TabIndex="11302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV184" runat="server" Columns="2" TabIndex="11303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV185" runat="server" Columns="2" TabIndex="11304" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV186" runat="server" Columns="2" TabIndex="11305" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV187" runat="server" Columns="2" TabIndex="11306" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV188" runat="server" Columns="2" TabIndex="11307" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 3px; text-align: center;">
                <asp:Label ID="lbl42" runat="server" CssClass="lblGrisTit" Text="2" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV189" runat="server" Columns="2" TabIndex="11401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center;">
                <asp:TextBox ID="txtV190" runat="server" Columns="2" TabIndex="11402"  MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center;">
                <asp:TextBox ID="txtV191" runat="server" Columns="2" TabIndex="11403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center;">
                <asp:TextBox ID="txtV192" runat="server" Columns="2" TabIndex="11404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center;">
                <asp:TextBox ID="txtV193" runat="server" Columns="2" TabIndex="11405" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center;">
                <asp:TextBox ID="txtV194" runat="server" Columns="2" TabIndex="11406" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right;">
                <asp:TextBox ID="txtV195" runat="server" Columns="2" TabIndex="11407" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 3px; text-align: center">
                <asp:Label ID="lbl43" runat="server" CssClass="lblGrisTit" Text="3" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV196" runat="server" Columns="2" TabIndex="11501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV197" runat="server" Columns="2" TabIndex="11502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV198" runat="server" Columns="2" TabIndex="11503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV199" runat="server" Columns="2" TabIndex="11504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV200" runat="server" Columns="2" TabIndex="11505" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV201" runat="server" Columns="2" TabIndex="11506" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: right">
                <asp:TextBox ID="txtV202" runat="server" Columns="2" TabIndex="11507" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td colspan="2" style="text-align: center;">
                <asp:Label ID="lblStotal4" runat="server" CssClass="lblGrisTit" Text="SUBTOTAL (4)"
                    Width="100%"></asp:Label></td>
            <td style="text-align: right;">
                <asp:TextBox ID="txtV203" runat="server" Columns="2" TabIndex="11601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            <table>
                                <tr>
                                    <td style="text-align: center">
                                        <asp:Label ID="lblTotalG1" runat="server" CssClass="lblGrisTit" Text="TOTAL (1)" Width="100px"></asp:Label></td>
                                    <td style="text-align: right">
                                        <asp:TextBox ID="txtV204" runat="server" Columns="3" TabIndex="11701" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: justify">
                            <asp:Label ID="lblInstuccion5" runat="server" CssClass="lblRojo" Text="5. Escriba la cantidad de personal directivo y administrativo, desglos�ndolo por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
    <table style="width: 100%">
        <tr>
            <td style="width: 318px">
            </td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres51" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres51" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal51" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 318px;">
                <asp:Label ID="lblDirectores" runat="server" CssClass="lblGrisTit" Text="DIRECTORES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV205" runat="server" Columns="2" TabIndex="11801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV206" runat="server" Columns="2" TabIndex="11802" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV207" runat="server" Columns="2" TabIndex="11803" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 318px;">
                <asp:Label ID="lblSubDirectores" runat="server" CssClass="lblGrisTit" Text="SUBDIRECTORES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV208" runat="server" Columns="2" TabIndex="11901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV209" runat="server" Columns="2" TabIndex="11902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV210" runat="server" Columns="2" TabIndex="11903" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 318px;">
                <asp:Label ID="lblSecretarias" runat="server" CssClass="lblGrisTit" Text="SECRETARIAS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV211" runat="server" Columns="2" TabIndex="12001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV212" runat="server" Columns="2" TabIndex="12002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV213" runat="server" Columns="2" TabIndex="12003" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 318px;">
                <asp:Label ID="lblAdmyAux" runat="server" CssClass="lblGrisTit" Text="ADMINISTRATIVO Y AUXILIAR"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV214" runat="server" Columns="2" TabIndex="12101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV215" runat="server" Columns="2" TabIndex="12102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV216" runat="server" Columns="2" TabIndex="12103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><table>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="lblTotalG2" runat="server" CssClass="lblGrisTit" Text="TOTAL (2)" Width="100px"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtV217" runat="server" Columns="2" TabIndex="12201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: justify">
                            <asp:Label ID="lblInstuccion6" runat="server" CssClass="lblRojo" Text="6. Escriba la cantidad de personal directivo y administrativo, desglos�ndolo por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
    <table style="width: 100%">
        <tr>
            <td style="width: 313px">
            </td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres61" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres61" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal61" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 313px;">
                <asp:Label ID="lblMedicos" runat="server" CssClass="lblGrisTit" Text="M�DICOS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV218" runat="server" Columns="2" TabIndex="12301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV219" runat="server" Columns="2" TabIndex="12302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV220" runat="server" Columns="2" TabIndex="12303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 313px;">
                <asp:Label ID="lblOdontologos" runat="server" CssClass="lblGrisTit" Text="ODONT�LOGOS"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV221" runat="server" Columns="2" TabIndex="12401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV222" runat="server" Columns="2" TabIndex="12402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV223" runat="server" Columns="2" TabIndex="12403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 313px;">
                <asp:Label ID="lblpsicologos" runat="server" CssClass="lblGrisTit" Text="PSIC�LOGOS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV224" runat="server" Columns="2" TabIndex="12501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV225" runat="server" Columns="2" TabIndex="12502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV226" runat="server" Columns="2" TabIndex="12503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 313px;">
                <asp:Label ID="lblenfermeras" runat="server" CssClass="lblGrisTit" Text="ENFERMERAS"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV227" runat="server" Columns="2" TabIndex="12601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV228" runat="server" Columns="2" TabIndex="12602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV229" runat="server" Columns="2" TabIndex="12603" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 313px;">
                <asp:Label ID="lblTsociales" runat="server" CssClass="lblGrisTit" Text="TRABAJADORAS SOCIALES"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV230" runat="server" Columns="2" TabIndex="12701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV231" runat="server" Columns="2" TabIndex="12702" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV232" runat="server" Columns="2" TabIndex="12703" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><table>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="lblTotalG3" runat="server" CssClass="lblGrisTit" Text="TOTAL (3)" Width="100px"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtV233" runat="server" Columns="2" TabIndex="12801" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: justify">
                            <asp:Label ID="lblInstuccion7" runat="server" CssClass="lblRojo" Text="7. Escriba la cantidad de personal de servicio de nutrici�n, desglos�ndolo por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
    <table style="width: 100%">
        <tr>
            <td style="width: 316px">
            </td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres71" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres71" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal71" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 316px;">
                <asp:Label ID="lblEconomas" runat="server" CssClass="lblGrisTit" Text="EC�NOMAS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV234" runat="server" Columns="2" TabIndex="12901" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV235" runat="server" Columns="2" TabIndex="12902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV236" runat="server" Columns="2" TabIndex="12903" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 316px;">
                <asp:Label ID="lblNutriologos" runat="server" CssClass="lblGrisTit" Text="NUTRI�LOGOS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV237" runat="server" Columns="2" TabIndex="13001" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV238" runat="server" Columns="2" TabIndex="13002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV239" runat="server" Columns="2" TabIndex="13003" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 316px;">
                <asp:Label ID="lblDietistas" runat="server" CssClass="lblGrisTit" Text="DIETISTAS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV240" runat="server" Columns="2" TabIndex="13101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV241" runat="server" Columns="2" TabIndex="13102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV242" runat="server" Columns="2" TabIndex="13103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 316px;">
                <asp:Label ID="lblCocineros" runat="server" CssClass="lblGrisTit" Text="COCINEROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV243" runat="server" Columns="2" TabIndex="13201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV244" runat="server" Columns="2" TabIndex="13202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV245" runat="server" Columns="2" TabIndex="13203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 316px;">
                <asp:Label ID="lblAuxCocina" runat="server" CssClass="lblGrisTit" Text="AUXILIARES DE COCINA"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV246" runat="server" Columns="2" TabIndex="13301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV247" runat="server" Columns="2" TabIndex="13302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV248" runat="server" Columns="2" TabIndex="13303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 316px;">
                <asp:Label ID="lblEAlmacen" runat="server" CssClass="lblGrisTit" Text="ENCARGADOS DE ALMAC�N"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV249" runat="server" Columns="2" TabIndex="13401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV250" runat="server" Columns="2" TabIndex="13402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV251" runat="server" Columns="2" TabIndex="13403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 316px;">
                <asp:Label ID="lblEBancoLeche" runat="server" CssClass="lblGrisTit" Text="ENCARGADOS DEL BANCO DE LECHE"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV252" runat="server" Columns="2" TabIndex="13501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV253" runat="server" Columns="2" TabIndex="13502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV254" runat="server" Columns="2" TabIndex="13503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 316px;">
                <asp:Label ID="lblAuxBancoLeche" runat="server" CssClass="lblGrisTit" Text="AUXILIARES DEL BANCO DE LECHE"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV255" runat="server" Columns="2" TabIndex="13601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV256" runat="server" Columns="2" TabIndex="13602" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV257" runat="server" Columns="2" TabIndex="13603" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><table>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="lblTotalG4" runat="server" CssClass="lblGrisTit" Text="TOTAL (4)" Width="100px"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtV258" runat="server" Columns="2" TabIndex="13701" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table style="width: 450px">
                    <tr>
                        <td style="width: 446px;text-align: justify" >
                            <asp:Label ID="lblInstuccion8" runat="server" CssClass="lblRojo" Text="8. Escriba la cantidad de personal de servicios generales, desglos�ndolo por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: center">
    <table>
        <tr>
            <td>
            </td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres81" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres81" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal81" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblAuxIntendencia" runat="server" CssClass="lblGrisTit" Text="AUXILIARES DE INTENDENCIA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV259" runat="server" Columns="2" TabIndex="20101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV260" runat="server" Columns="2" TabIndex="20102" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV261" runat="server" Columns="2" TabIndex="20103" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblLavanderos" runat="server" CssClass="lblGrisTit" Text="LAVANDEROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV262" runat="server" Columns="2" TabIndex="20201" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV263" runat="server" Columns="2" TabIndex="20202" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV264" runat="server" Columns="2" TabIndex="20203" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblVigilantes" runat="server" CssClass="lblGrisTit" Text="VIGILANTES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV265" runat="server" Columns="2" TabIndex="20301" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV266" runat="server" Columns="2" TabIndex="20302" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV267" runat="server" Columns="2" TabIndex="20303" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblAuxMantenimiento" runat="server" CssClass="lblGrisTit" Text="AUXILIARES DE MANTENIMIENTO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV268" runat="server" Columns="2" TabIndex="20401" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV269" runat="server" Columns="2" TabIndex="20402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV270" runat="server" Columns="2" TabIndex="20403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblConserjes" runat="server" CssClass="lblGrisTit" Text="ASISTENTES DE SERVICIOS DE MANTENIMIENTO (CONSERJES)"
                    Width="225px"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV271" runat="server" Columns="2" TabIndex="20501" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV272" runat="server" Columns="2" TabIndex="20502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV273" runat="server" Columns="2" TabIndex="20503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: right;"><table>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="lblTotalG5" runat="server" CssClass="lblGrisTit" Text="TOTAL (5)" Width="100px"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtV274" runat="server" Columns="2" TabIndex="20601" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 446px;text-align: justify">
                            <asp:Label ID="lblInstuccion9" runat="server" CssClass="lblRojo" Text="9. Registre al personal con otro tipo de funciones, desglos�ndolo por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: justify;">
                            <asp:Label ID="lblInstuccion9b" runat="server" CssClass="lblRojo" Text="Desglose al personal que no se consider� en los rubros del 1 al 8 seg�n la actividad que desempe�a."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: center">
    <table>
        <tr>
            <td>
            </td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres91" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres91" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblTotal91" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtV275" runat="server" Columns="32" TabIndex="20701" MaxLength="32" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV276" runat="server" Columns="2" TabIndex="20702" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV277" runat="server" Columns="2" TabIndex="20703" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV278" runat="server" Columns="2" TabIndex="20704" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtV279" runat="server" Columns="32" TabIndex="20801" MaxLength="32" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV280" runat="server" Columns="2" TabIndex="20802" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV281" runat="server" Columns="2" TabIndex="20803" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV282" runat="server" Columns="2" TabIndex="20804" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtV283" runat="server" Columns="32" TabIndex="20901" MaxLength="32" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV284" runat="server" Columns="2" TabIndex="20902" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV285" runat="server" Columns="2" TabIndex="20903" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV286" runat="server" Columns="2" TabIndex="20904" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtV287" runat="server" Columns="32" TabIndex="21001" MaxLength="32" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV288" runat="server" Columns="2" TabIndex="21002" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV289" runat="server" Columns="2" TabIndex="21003" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV290" runat="server" Columns="2" TabIndex="21004" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: right;"><table>
                            <tr>
                                <td style="text-align: center">
                                    <asp:Label ID="lblTotalG6" runat="server" CssClass="lblGrisTit" Text="TOTAL (6)" Width="100px"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtV291" runat="server" Columns="2" TabIndex="21101" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: right">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSumeTotales" runat="server" CssClass="lblGrisTit" Text="Sume los totales del 1 al 6."
                                            Width="200px"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblTotales" runat="server" CssClass="lblGrisTit" Text="TOTAL DE PERSONAL"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV292" runat="server" Columns="3" TabIndex="21201" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: justify">
                            <asp:Label ID="lblInstuccion10" runat="server" CssClass="lblRojo" Text="10. Desglose el total del personal, seg�n su nivel m�ximo de estudios y sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: justify;">
                            <asp:Label ID="lblInstuccion10nota" runat="server" CssClass="lblRojo" Text="NOTA: Si en la tabla correspondiente al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente o en Otros."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 446px; text-align: center">
    <table>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNivelEd" runat="server" CssClass="lblNegro" Text="NIVEL EDUCATIVO"
                    Width="100%" Font-Bold="True"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblHombres101" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:Label ID="lblMujeres101" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="100%"></asp:Label></td>
            <td style="text-align: center; width: 57px;">
                <asp:Label ID="lblTotal101" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblPrimariaI" runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV293" runat="server" Columns="2" MaxLength="2" TabIndex="21301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV294" runat="server" Columns="2" MaxLength="2" TabIndex="21302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV295" runat="server" Columns="2" MaxLength="2" TabIndex="21303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblPrimariaT" runat="server" CssClass="lblGrisTit" Text="PRIMARIA TERMINADA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV296" runat="server" Columns="2" MaxLength="2" TabIndex="21401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV297" runat="server" Columns="2" MaxLength="2" TabIndex="21402" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV298" runat="server" Columns="2" MaxLength="2" TabIndex="21403" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblSecundariaI" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA INCOMPLETA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV299" runat="server" Columns="2" MaxLength="2" TabIndex="21501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV300" runat="server" Columns="2" MaxLength="2" TabIndex="21502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV301" runat="server" Columns="2" MaxLength="2" TabIndex="21503" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblSecundariaT" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA TERMINADA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV302" runat="server" Columns="2" MaxLength="2" TabIndex="21601" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV303" runat="server" Columns="2" MaxLength="2" TabIndex="21602" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV304" runat="server" Columns="2" MaxLength="2" TabIndex="21603" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblProfesionalTec" runat="server" CssClass="lblGrisTit" Text="PROFESIONAL T�CNICO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV305" runat="server" Columns="2" MaxLength="2" TabIndex="21701" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV306" runat="server" Columns="2" MaxLength="2" TabIndex="21702" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV307" runat="server" Columns="2" MaxLength="2" TabIndex="21703" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblBachilleratoI" runat="server" CssClass="lblGrisTit" Text="BACHILLERATO INCOMPLETO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV308" runat="server" Columns="2" MaxLength="2" TabIndex="21801" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV309" runat="server" Columns="2" MaxLength="2" TabIndex="21802" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV310" runat="server" Columns="2" MaxLength="2" TabIndex="21803" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblBachilleratoT" runat="server" CssClass="lblGrisTit" Text="BACHILLERATO TERMINADO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV311" runat="server" Columns="2" MaxLength="2" TabIndex="21901" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV312" runat="server" Columns="2" MaxLength="2" TabIndex="21902" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV313" runat="server" Columns="2" MaxLength="2" TabIndex="21903" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNormalPreI" runat="server" CssClass="lblGrisTit" Text="NORMAL PREESCOLAR INCOMPLETA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV314" runat="server" Columns="2" MaxLength="2" TabIndex="22001" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV315" runat="server" Columns="2" MaxLength="2" TabIndex="22002" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV316" runat="server" Columns="2" MaxLength="2" TabIndex="22003" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNormalPreT" runat="server" CssClass="lblGrisTit" Text="NORMAL PREESCOLAR TERMINADA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV317" runat="server" Columns="2" MaxLength="2" TabIndex="22101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV318" runat="server" Columns="2" MaxLength="2" TabIndex="22102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV319" runat="server" Columns="2" MaxLength="2" TabIndex="22103" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNormalPriI" runat="server" CssClass="lblGrisTit" Text="NORMAL PRIMARIA INCOMPLETA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV320" runat="server" Columns="2" MaxLength="2" TabIndex="22201" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV321" runat="server" Columns="2" MaxLength="2" TabIndex="22202" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV322" runat="server" Columns="2" MaxLength="2" TabIndex="22203" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNormalPriT" runat="server" CssClass="lblGrisTit" Text="NORMAL PRIMARIA TERMINADA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV323" runat="server" Columns="2" MaxLength="2" TabIndex="22301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV324" runat="server" Columns="2" MaxLength="2" TabIndex="22302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV325" runat="server" Columns="2" MaxLength="2" TabIndex="22303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNormalSupI" runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR INCOMPLETA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV326" runat="server" Columns="2" MaxLength="2" TabIndex="22401" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV327" runat="server" Columns="2" MaxLength="2" TabIndex="22402" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV328" runat="server" Columns="2" MaxLength="2" TabIndex="22403" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNormalSupP" runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR, PASANTE" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV329" runat="server" Columns="2" MaxLength="2" TabIndex="22501" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV330" runat="server" Columns="2" MaxLength="2" TabIndex="22502" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV331" runat="server" Columns="2" MaxLength="2" TabIndex="22503" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblNormalSupT" runat="server" CssClass="lblGrisTit" Text="NORMAL SUPERIOR, TITULADO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV332" runat="server" Columns="2" MaxLength="2" TabIndex="22601" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV333" runat="server" Columns="2" MaxLength="2" TabIndex="22602" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV334" runat="server" Columns="2" MaxLength="2" TabIndex="22603" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblLicenciaturaI" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA INCOMPLETA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV335" runat="server" Columns="2" MaxLength="2" TabIndex="22701" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV336" runat="server" Columns="2" MaxLength="2" TabIndex="22702" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV337" runat="server" Columns="2" MaxLength="2" TabIndex="22703" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblLicenciaturaP" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, PASANTE" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV338" runat="server" Columns="2" MaxLength="2" TabIndex="22801" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV339" runat="server" Columns="2" MaxLength="2" TabIndex="22802" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV340" runat="server" Columns="2" MaxLength="2" TabIndex="22803" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblLicenciaturaT" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV341" runat="server" Columns="2" MaxLength="2" TabIndex="22901" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV342" runat="server" Columns="2" MaxLength="2" TabIndex="22902" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV343" runat="server" Columns="2" MaxLength="2" TabIndex="22903" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblMaestriaI" runat="server" CssClass="lblGrisTit" Text="MAESTR�A INCOMPLETA" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV344" runat="server" Columns="2" MaxLength="2" TabIndex="23001" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV345" runat="server" Columns="2" MaxLength="2" TabIndex="23002" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV346" runat="server" Columns="2" MaxLength="2" TabIndex="23003" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="MaestriaT" runat="server" CssClass="lblGrisTit" Text="MAESTR�A, GRADUADO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV347" runat="server" Columns="2" MaxLength="2" TabIndex="23101" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV348" runat="server" Columns="2" MaxLength="2" TabIndex="23102" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV349" runat="server" Columns="2" MaxLength="2" TabIndex="23103" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="DoctoradoI" runat="server" CssClass="lblGrisTit" Text="DOCTORADO INCOMPLETO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV350" runat="server" Columns="2" MaxLength="2" TabIndex="23201" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV351" runat="server" Columns="2" MaxLength="2" TabIndex="23202" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV352" runat="server" Columns="2" MaxLength="2" TabIndex="23203" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblDoctoradoT" runat="server" CssClass="lblGrisTit" Text="DOCTORADO, GRADUADO" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV353" runat="server" Columns="2" MaxLength="2" TabIndex="23301" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV354" runat="server" Columns="2" MaxLength="2" TabIndex="23302" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center; width: 57px;">
                <asp:TextBox ID="txtV355" runat="server" Columns="2" MaxLength="2" TabIndex="23303" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblOtros10" runat="server" CssClass="lblGrisTit" Text="OTROS" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                </td>
            <td style="text-align: center">
                </td>
            <td style="text-align: center; width: 57px;">
                </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblEspecifique10" runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE:"
                    Width="100%"></asp:Label></td>
            <td style="text-align: center">
            </td>
            <td style="text-align: center">
            </td>
            <td style="width: 57px; text-align: center">
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:TextBox ID="txtV356" runat="server" Columns="30" TabIndex="23401" MaxLength="32" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV357" runat="server" Columns="2" TabIndex="23402" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV358" runat="server" Columns="2" TabIndex="23403" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="width: 57px; text-align: center">
                <asp:TextBox ID="txtV359" runat="server" Columns="2" TabIndex="23404" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:TextBox ID="txtV360" runat="server" Columns="30" TabIndex="23501" MaxLength="32" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV361" runat="server" Columns="2" TabIndex="23502" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV362" runat="server" Columns="2" TabIndex="23503" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
            <td style="width: 57px; text-align: center">
                <asp:TextBox ID="txtV363" runat="server" Columns="2" TabIndex="23504" MaxLength="2" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:Label ID="lblTotal102" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="100%"></asp:Label></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV364" runat="server" Columns="3" TabIndex="23601" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
            <td style="text-align: center">
                <asp:TextBox ID="txtV365" runat="server" Columns="3" TabIndex="23602" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
            <td style="width: 57px; text-align: center">
                <asp:TextBox ID="txtV366" runat="server" Columns="3" TabIndex="23603" MaxLength="3" CssClass="lblNegro"></asp:TextBox></td>
        </tr>
    </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Poblacion_EI_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Poblacion_EI_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                </td>
                <td ><span  onclick="openPage('CMYA_EI_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('CMYA_EI_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div style="width:900px" id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer"></td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                    
                
 		        Disparador(<%=hidDisparador.Value %>);
 		        GetTabIndexes();
          
        </script> 
</asp:Content>
