<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="Carrera Magisterial" AutoEventWireup="true" CodeBehind="CMYA_EI_1.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.EI_1.CMYA_EI_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">

    
     <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 17;
        MaxRow = 31;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
   
<div id="logo"></div>
    <div style="min-width:1000px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr>
            <td style="text-align:center">
                &nbsp;<asp:Label ID="lblNivel" runat="server" 
                      Text="EDUCACI�N INICIAL"></asp:Label></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>

    </table></div></div>
    <div id="menu" style="min-width:1000px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_EI_1',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li><li onclick="openPage('Poblacion_EI_1',true)"><a href="#" title=""><span>ALUMNOS</span></a></li><li onclick="openPage('Personal_EI_1',true)"><a href="#" title=""><span>PERSONAL</span></a></li><li onclick="openPage('CMYA_EI_1',true)"><a href="#" title="" class="activo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li><li onclick="openPage('Gasto_EI_1',false)"><a href="#" title=""><span>GASTO</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>ANEXO</span></a></li><li onclick="denyPage()"><a href="#" title=""><span>OFICIALIZACI�N</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
     <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>

<center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        &nbsp;<table style="width: 899px">
            <tr>
                <td valign="top">
                    <table style="width: 400px">
                        <tr>
                            <td valign="top" style="text-align:justify">
                                <asp:Label ID="lblCARRERA" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="V. CARRERA MAGISTERIAL"
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: justify">
                                <asp:Label ID="lblInstruccionIII1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de profesores que se encuentran en el programa de carrera magisterial."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:TextBox ID="txtV367" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: justify">
                                <asp:Label ID="lblInstruccionIII2" runat="server" CssClass="lblRojo" Text="2. Desglose la cantidad anotada en el inciso anterior, seg�n la vertiente y el nivel en que se encuentra el personal."
                                    Width="100%"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
        <table>
            <tr>
                <td rowspan="1">
                    <br />
                    <br />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td rowspan="6">
                    &nbsp;<table>
            <tr>
                <td>
                    <asp:Label ID="lbl1aVertiente" runat="server" CssClass="lblNegro" Height="21px" Text="1a. VERTIENTE"
                        Width="100%" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td>
        <asp:Label ID="lbl1aVertiente2" runat="server" CssClass="lblNegro" Height="21px" Text="(Profesores frente a grupo)"
            Width="180px" Font-Bold="True"></asp:Label></td>
                            
            </tr>
        </table>
                </td>
                <td>
                    <asp:Label ID="lblNivelA1" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV368" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblNivelB1" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV369" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
        <asp:Label ID="lblNivelBC1" runat="server" CssClass="lblGrisTit" Text="Nivel BC"
            Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV370" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label ID="lblNivelC1" runat="server" CssClass="lblGrisTit"
                Text="Nivel C" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV371" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label ID="lblNivelD1" runat="server"
                    CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV372" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label ID="lblNivelE1"
                        runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV373" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="1">
                    <br />
                    <br />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td rowspan="6">
                    <table>
            <tr>
                <td>
        <asp:Label ID="lbl2aVertiente" runat="server" CssClass="lblNegro"
                Height="21px" Text="2a. VERTIENTE" Width="100%" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td style="height: 41px">
  
    <asp:Label ID="lbl2aVertiente2"
                    runat="server" CssClass="lblNegro" Height="46px" Text="(Docentes en funciones directivas y de supervisi�n)"
                    Width="180px" Font-Bold="True"></asp:Label></td>
            </tr>
        </table>
  
    </td>
                <td>
                <asp:Label
                            ID="lblNivelA2" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV374" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                ID="lblNivelB2" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV375" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                    ID="lblNivelBC2" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV376" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                        ID="lblNivelC2" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV377" runat="server" Columns="2" TabIndex="11101" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                            ID="lblNivelD2" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV378" runat="server" Columns="2" TabIndex="11201" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                                ID="lblNivelE2" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV379" runat="server" Columns="2" TabIndex="11301" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td rowspan="1">
                    <br />
                    <br />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td rowspan="6">
                    &nbsp;<table>
                        <tr>
                            <td>
        <asp:Label ID="lbl3aVertiente" runat="server" CssClass="lblNegro"
                        Height="21px" Text="3a. VERTIENTE" Width="100%" Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lbl3aVertiente2"
                            runat="server" CssClass="lblNegro" Height="43px" Text="(Docentes en actividades t�cnico-pedag�gicas)"
                            Width="180px" Font-Bold="True"></asp:Label></td>
                        </tr>
                    </table>
                </td>
                <td>
                <asp:Label
                                                    ID="lblNivelA3" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV380" runat="server" Columns="2" TabIndex="11401" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                                        ID="lblNivelB3" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV381" runat="server" Columns="2" TabIndex="11501" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                                            ID="lblNivelBC3" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV382" runat="server" Columns="2" TabIndex="11601" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                                                ID="lblNivelC3" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV383" runat="server" Columns="2" TabIndex="11701" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                                                    ID="lblNivelD3" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV384" runat="server" Columns="2" TabIndex="11801" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                <asp:Label
                                                                        ID="lblNivelE3" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="100%"></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtV385" runat="server" Columns="2" TabIndex="11901" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: center" valign="top"><table style="width: 400px">
                    <tr>
                        <td style="text-align: left">
                            <asp:Label ID="lblAULAS" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="VI. AULAS"
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <asp:Label ID="lblInstruccionVI1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de aulas por servicio atendido."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <asp:Label ID="lblNOTAS" runat="server" CssClass="lblRojo" Text="NOTAS:" Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <asp:Label ID="lblInstruccionVI1a" runat="server" CssClass="lblRojo" Text="a) El informe de aulas debe ser por turno."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <asp:Label ID="lblInstruccionVI1b" runat="server" CssClass="lblRojo" Text="b) Si un aula se utiliza para atender a m�s de un estrato/grado, an�tela en el rubro correspondiente."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                    
                        <td style="text-align: center">
                        <br />
                            <asp:Label ID="lblLACTANTES" runat="server" CssClass="lblRojo" Text="LACTANTES" Width="100%"></asp:Label>
                            <hr />
                            </td>
                            
                    </tr>
                    <tr>
                        <td style="text-align: right">
        <table>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblAulasLac" runat="server" CssClass="lblNegro" Text="AULAS" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl1Lac" runat="server" CssClass="lblNegro" Text="1" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl2Lac" runat="server" CssClass="lblNegro" Text="2" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl3Lac" runat="server" CssClass="lblNegro" Text="3" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblEstratoLac" runat="server" CssClass="lblNegro" Text="M�S DE UN ESTRATO"
                        Width="130px" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalLac" runat="server" CssClass="lblNegro" Text="TOTAL" Width="100%" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblExistentesLac" runat="server" CssClass="lblGrisTit" Text="EXISTENTES" Width="100%"></asp:Label></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td style="width: 3px">
                </td>
                <td style="width: 3px">
                </td>
                <td style="width: 3px">
                </td>
                <td style="width: 3px; text-align: center">
                    <asp:TextBox ID="txtV386" runat="server" Columns="2" TabIndex="20101" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblUsoLac" runat="server" CssClass="lblGrisTit" Text="EN USO" Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV387" runat="server" Columns="2" TabIndex="20201" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV388" runat="server" Columns="2" TabIndex="20202" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV389" runat="server" Columns="2" TabIndex="20203" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV390" runat="server" Columns="2" TabIndex="20204" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV391" runat="server" Columns="2" TabIndex="20205" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: left">
                    <asp:Label ID="lblInstruccionLac" runat="server" CssClass="lblGrisTit" Text="De las aulas reportadas en uso, indique la cantidad de las adaptadas."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblAdaptadasLac" runat="server" CssClass="lblGrisTit" Text="ADAPTADAS" Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV392" runat="server" Columns="2" TabIndex="20301" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV393" runat="server" Columns="2" TabIndex="20302" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV394" runat="server" Columns="2" TabIndex="20303" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV395" runat="server" Columns="2" TabIndex="20304" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV396" runat="server" Columns="2" TabIndex="20305" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
        </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                        <br />
                            <asp:Label ID="lblMATERNALES" runat="server" CssClass="lblRojo" Text="MATERNALES" Width="100%"></asp:Label>
                            <hr />
                            </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
        <table>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblAulasMat" runat="server" CssClass="lblNegro" Text="AULAS" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl1Mat" runat="server" CssClass="lblNegro" Text="1" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl2Mat" runat="server" CssClass="lblNegro" Text="2" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl3Mat" runat="server" CssClass="lblNegro" Text="3" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblEstratoMat" runat="server" CssClass="lblNegro" Text="M�S DE UN ESTRATO"
                        Width="130px" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalMat" runat="server" CssClass="lblNegro" Text="TOTAL" Width="100%" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label17" runat="server" CssClass="lblGrisTit" Text="EXISTENTES" Width="100%"></asp:Label></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 3px; text-align: center">
                    <asp:TextBox ID="txtV397" runat="server" Columns="2" TabIndex="20401" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblUsoMat" runat="server" CssClass="lblGrisTit" Text="EN USO" Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV398" runat="server" Columns="2" TabIndex="20501" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV399" runat="server" Columns="2" TabIndex="20502" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV400" runat="server" Columns="2" TabIndex="20503" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV401" runat="server" Columns="2" TabIndex="20504" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV402" runat="server" Columns="2" TabIndex="20505" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: left">
                    <asp:Label ID="lblInstruccionMat" runat="server" CssClass="lblGrisTit" Text="De las aulas reportadas en uso, indique la cantidad de las adaptadas."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblAdaptadasMat" runat="server" CssClass="lblGrisTit" Text="ADAPTADAS" Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV403" runat="server" Columns="2" TabIndex="20601" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV404" runat="server" Columns="2" TabIndex="20602" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV405" runat="server" Columns="2" TabIndex="20603" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV406" runat="server" Columns="2" TabIndex="20604" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV407" runat="server" Columns="2" TabIndex="20605" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
        </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                        <br />
                            <asp:Label ID="lblPREESCOLAR" runat="server" CssClass="lblRojo" Text="PREESCOLAR" Width="100%"></asp:Label>
                            <hr />
                            </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
        <table>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblAulasPre" runat="server" CssClass="lblNegro" Text="AULAS" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl1Pre" runat="server" CssClass="lblNegro" Text="1" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl2Pre" runat="server" CssClass="lblNegro" Text="2" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lbl3Pre" runat="server" CssClass="lblNegro" Text="3" Width="100%" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblEstratoPre" runat="server" CssClass="lblNegro" Text="M�S DE UN ESTRATO"
                        Width="130px" Font-Bold="True"></asp:Label></td>
                <td style="text-align: center">
                    <asp:Label ID="lblTotalPre" runat="server" CssClass="lblNegro" Text="TOTAL" Width="100%" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label27" runat="server" CssClass="lblGrisTit" Text="EXISTENTES" Width="100%"></asp:Label></td>
                <td style="width: 3px; text-align: center">
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td style="width: 3px; text-align: center">
                    <asp:TextBox ID="txtV408" runat="server" Columns="2" TabIndex="20701" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblUsoPre" runat="server" CssClass="lblGrisTit" Text="EN USO" Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV409" runat="server" Columns="2" TabIndex="20801" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV410" runat="server" Columns="2" TabIndex="20802" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV411" runat="server" Columns="2" TabIndex="20803" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV412" runat="server" Columns="2" TabIndex="20804" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV413" runat="server" Columns="2" TabIndex="20805" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: left">
                    <asp:Label ID="lblInstruccionPre" runat="server" CssClass="lblGrisTit" Text="De las aulas reportadas en uso, indique la cantidad de las adaptadas."
                        Width="100%"></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="lblAdaptadasPre" runat="server" CssClass="lblGrisTit" Text="ADAPTADAS" Width="100%"></asp:Label></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV414" runat="server" Columns="2" TabIndex="20901" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV415" runat="server" Columns="2" TabIndex="20902" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV416" runat="server" Columns="2" TabIndex="20903" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV417" runat="server" Columns="2" TabIndex="20904" CssClass="lblNegro"></asp:TextBox></td>
                <td style="text-align: center">
                    <asp:TextBox ID="txtV418" runat="server" Columns="2" TabIndex="20905" CssClass="lblNegro"></asp:TextBox></td>
            </tr>
        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal_EI_1',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal_EI_1',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Gasto_EI_1',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Gasto_EI_1',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>

       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
<%--                    <img src="../../../tema/images/loading2.gif" alt="Procesando" style="filter:Alpha(Opacity=40);opacity:.4;"/>
--%>                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
 		Disparador(<%=hidDisparador.Value %>);
               GetTabIndexes();   
        </script> 
</asp:Content> 
