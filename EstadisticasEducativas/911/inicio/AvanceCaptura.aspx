<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignMenu.Master" AutoEventWireup="true" CodeBehind="AvanceCaptura.aspx.cs" Inherits="EstadisticasEducativas._911.inicio.AvanceCaptura" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<asp:Content id="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">  

    <script type="text/javascript" language="javascript">
        var _enter=true;
        function Vent911(Pagina)
        {
            var v911 = false;
            try{
               v911 = window.open (Pagina, "ventana911", "width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668");
                v911.location=Pagina;
                v911.focus();
            }
            catch(err)
            {
                alert(err);
            } 
        }
    </script>

    <script type="text/javascript">
        function enter(e) 
        {
            if (_enter) 
            {
                if (event.keyCode==13)
                {
                    event.keyCode=9; 
                    return event.keyCode;
                }    
            }
        }
        function Num(evt){    
            
           return  keyRestrict(evt,'1234567890');
        }
    </script>


<%--        <table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0">
            <tr>
                <td width="100%" align="center">
                    <table id="Table4" cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr>
                            <td>
                                <img alt="" src="../../tema/images/head_lg_nl.jpg" /></td>
                            <td>
                                <img alt="" src="../../tema/images/banner_nl.jpg" /></td>
                            <td>
                                <img alt="" style="width: 181px; height: 97px" height="97" src="../../tema/images/SE.jpg"
                                    width="181" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr>
                <td style="font-weight: bold; font-size: 16px; color: #ffffff; font-family: Verdana, Tahoma, Arial;
                    height: 30px; background-color: #cc0000; text-align: center">
                                            Estad�sticas Educativas 911</td>
            </tr>
        </table>--%>
        <br />
        <center>
        <div style="text-align:center; width:100%">
             <center>
            <asp:Label ID="lblCiclo" CssClass="titulopagina" runat="server" Font-Size="20px">Avance de captura -Inicio de Cursos 2009-2010</asp:Label><br />
            <br />
            <%--<asp:UpdatePanel ID="upBusqueda" runat="server">
                <ContentTemplate>--%>
            <table  border="0" cellpadding="0" cellspacing="2" style="width: 310px">
                <tr>
                    <td align="left" style="width: 106px">
                        <asp:Label ID="lblNivel" runat="server" CssClass="lblRojo" Font-Size="14px" Text="Nivel:"></asp:Label></td>
                    <td align="left" style="width: 120px">
                        <asp:DropDownList ID="ddlNivel" runat="server" AutoPostBack="True" CssClass="lblNegro"
                            Font-Size="14px" OnSelectedIndexChanged="ddlNivel_SelectedIndexChanged">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="left" style="width: 106px">
                        <asp:Label CssClass="lblRojo" ID="lblRegion" runat="server" Text="Regi�n:" Font-Size="14px"></asp:Label></td>
                    <td align="left" style="width: 120px">
                        <asp:TextBox CssClass="lblNegro" ID="txtRegion" runat="server" Width="55px" TabIndex="1"
                            MaxLength="12" Font-Size="14px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="left" style="width: 106px">
                        <asp:Label CssClass="lblRojo" ID="lblZona" runat="server" Text="Zona:" Width="43px" Font-Size="14px"></asp:Label></td>
                    <td align="left" style="width: 120px">
                        <asp:TextBox CssClass="lblNegro" ID="txtZona" runat="server" Width="55px" TabIndex="2"
                            MaxLength="3" Font-Size="14px"></asp:TextBox></td>
                </tr>
                <tr id="trNiv">
                    <td align="left" style="width: 106px">
                        <asp:Label CssClass="lblRojo" ID="lblClave" runat="server" Text="Centro de Trabajo:" Font-Size="14px" Width="144px"></asp:Label></td>
                    <td align="left" style="width: 120px">
                        <asp:TextBox CssClass="lblNegro" ID="txtClaveCT" runat="server" Width="110px" TabIndex="3"
                            MaxLength="10" Font-Size="14px"></asp:TextBox>
                        <asp:DropDownList ID="ddlCentroTrabajo" runat="server" Visible="False">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td align="left" style="width: 106px">
                        <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Font-Size="14px" Text="Estatus:" Width="132px"></asp:Label></td>
                    <td align="left" style="width: 120px">
                        <asp:DropDownList ID="ddlEstatus" runat="server">
                        </asp:DropDownList></td>
                </tr>
            </table>
         <%--       </ContentTemplate>
            </asp:UpdatePanel>--%>
                 <asp:CheckBox ID="chkPaginado" runat="server" Checked="True" CssClass="lblGrisTit"
                     Text="Paginado" /></center>
            <center>
            <br />
            &nbsp;&nbsp;
            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="67px" TabIndex="4" OnClick="btnBuscar_Click" CssClass="botones2" />
                <asp:Button ID="btnExportar" runat="server" Text="Exportar a Excel" Width="115px" TabIndex="4" OnClick="btnExportar_Click" CssClass="botones2" />
                <asp:Button ID="btnRecargarPagina" runat="server" OnClick="btnRecargarPagina_Click"
                    Text="Rec" Width="13px" /><br />
            <br />
<%--            <asp:UpdatePanel ID="upMsg" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ddlNivel" EventName="SelectedIndexChanged" />
                </Triggers>
                <ContentTemplate>--%>
                    <asp:Label ID="lblMsg" runat="server" CssClass="msg"></asp:Label>
<%--                </ContentTemplate>
            </asp:UpdatePanel>--%>
            </center>
        </div>
        </center>
        <br />
<%--        <asp:UpdatePanel ID="upGridView1" runat="server" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnBuscar" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="Sorting" />
                <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="PageIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlNivel" EventName="SelectedIndexChanged" />
            </Triggers>
            <ContentTemplate>--%>
                <center>
                <div style="text-align: center">
                   <center>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Font-Names="Arial"
                        Font-Size="11px" OnRowCreated="GridView1_RowCreated" AllowPaging="True" AllowSorting="True"
                        PageSize="50" CellPadding="4" OnPageIndexChanging="GridView1_PageIndexChanging"
                        OnSorting="GridView1_Sorting" OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                        ForeColor="#333333" EmptyDataText="No se han enontrado registros" OnRowCommand="RowCommand">
                        <Columns>
                            <asp:ButtonField CommandName="cc" HeaderText="CC" Text="&lt;img src='../../tema/images/Hoja.gif' alt='Comprobante de captura' style='border:0;'&gt;" />
                            <asp:ButtonField CommandName="frm" HeaderText="FRM" Text="&lt;img src='../../tema/images/Archivo.gif' alt='Cuestionario lleno' width=25 height=25  style='border:0;'&gt;" />
                            <asp:ButtonField CommandName="des" HeaderText="DES" Text="&lt;img src='../../tema/images/setting.gif' alt='Desoficializar' width=28 height=28 style='border:0;'&gt;" />
                            <asp:ButtonField CommandName="cap" HeaderText="CAP" Text="&lt;img src='../../tema/images/iconEsc.gif' alt='Captura 911' style='border:0;'&gt;" />
                        
                    <%--    CompCaptura = "<img src='../../tema/images/Hoja.gif' id='imgCC' alt='Comprobante de captura' style='cursor: hand; border:0px' onclick=AbrirPDF('Fin/" + Class911.GenerarComprobante_PDF(int.Parse(dr["ID_CONTROL"].ToString().Trim())).Trim() + "',1) />";
                    Cuestionario = "<img src='../../tema/images/Archivo.gif' id='imgCLL' alt='Cuestionario lleno' style='cursor: hand; border:0px; width: 30px; height: 30px;' onclick=AbrirPDF('Fin/" + Class911.GenerarCuestionario_PDF(int.Parse(dr["ID_CONTROL"].ToString().Trim())).Trim() + "',2) />";
                    Desoficializar = "<img src='../../tema/images/setting.gif' id='imgDeso' alt='Desoficializar' style='cursor: hand; border:0px; width: 30px; height: 30px;' onclick=AbrirPDF('Fin/" + Class911.Desoficializar(int.Parse(dr["ID_CONTROL"].ToString().Trim())).Trim() + "',3) />";
                    Detalle = "<img src='../../tema/images/iconEsc.gif' id='imgDetalle' alt='Captura 911' style='cursor: hand; border:0px' onclick=Vent911(" + dr["ID_CENTROTRABAJO"].ToString() + "," + dr["Id_INMUEBLE"].ToString() + ",'" + dr["CCT"].ToString().Trim() + "'," + dr["ID_TURNO"].ToString().Trim() + ") />";
                    --%>
<%--                            <asp:TemplateField HeaderText="CC" SortExpression="ID_CENTROTRABAJO">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "CompCaptura")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FRM" SortExpression="ID_CENTROTRABAJO">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "Cuestionario")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DES" SortExpression="ID_CENTROTRABAJO">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "Desoficializar")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CAP" SortExpression="ID_CENTROTRABAJO">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "Detalle")%>
                                </ItemTemplate>
                            </asp:TemplateField>--%>                            
                            <asp:BoundField DataField="ID_CONTROL" HeaderText="ID_CONTROL" SortExpression="ID_CONTROL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_CENTROTRABAJO" HeaderText="ID_CENTROTRABAJO" SortExpression="ID_CENTROTRABAJO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_INMUEBLE" HeaderText="INMUEBLE" SortExpression="ID_INMUEBLE">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="ID_CCTNT"  SortExpression="ID_CCTNT" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID_CCTNT" runat="server" Text='<%# Bind("ID_CCTNT") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="CCT" HeaderText="CCT" SortExpression="CCT">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_TURNO" HeaderText="TURNO" SortExpression="ID_TURNO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TELEFONO" HeaderText="TELEFONO" SortExpression="TELEFONO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="REGION" HeaderText="REGION" SortExpression="REGION">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_ZONA" HeaderText="ID_ZONA" SortExpression="ID_ZONA">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ZONA" HeaderText="ZONA" SortExpression="ZONA">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_NIVEL" HeaderText="ID_NIVEL" SortExpression="ID_NIVEL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NIVEL" HeaderText="NIVEL" SortExpression="NIVEL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_SUBNIVEL" HeaderText="ID_SUBNIVEL" SortExpression="ID_SUBNIVEL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SUBNIVEL" HeaderText="SUBNIVEL" SortExpression="SUBNIVEL">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ID_SOSTENIMIENTO" HeaderText="ID_SOSTENIMIENTO" SortExpression="ID_SOSTENIMIENTO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="SOSTENIMIENTO" HeaderText="SOSTENIMIENTO" SortExpression="SOSTENIMIENTO">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ESTATUS" HeaderText="ESTATUS" SortExpression="ESTATUS">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle BackColor="#CC0000" Font-Bold="True" ForeColor="White" />
                        <RowStyle Font-Names="Arial" Font-Size="10px" BackColor="White" ForeColor="#333333" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" Height="20px" CssClass="lblRojo" />
                        <AlternatingRowStyle BackColor="#E0E0E0" Font-Names="Arial" Font-Size="10px" ForeColor="#333333" />
                        <PagerSettings PageButtonCount="20" />
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    </asp:GridView>
                    </center>
                </div>
                </center>
    <%--        </ContentTemplate>
        </asp:UpdatePanel>--%>
        <%--<script type="text/javascript" src="../../tema/js/wait.js"></script>--%>
        <%--<asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>            
                <div id="wait" style="position:fixed; text-align:center">                
                    <img src="../../tema/images/indicator_mozilla_blu.gif" alt=''/>            
                </div>        
            </ProgressTemplate>        
        </asp:UpdateProgress>--%>

        <asp:HiddenField ID="hidRuta" runat="server" />
        <script language = "javascript" type="text/javascript">
           function AbrirPDF(ruta,tipo){
               if (ruta != ""){
                    if(tipo==3) {
                        if(confirm("�Esta seguro de desoficializar la estad�stica?")) {
                            window.open(ruta,'','width=100,height=100,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=300, left=200, xscreen=100, yscreen=100')
                        }
                    }
                    else
                    {
                        window.open(ruta,'','width=924,height=668,status=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no, top=10, left=200, xscreen=924, yscreen=668')
                    }
               }
           }
        </script>
        
</asp:Content>


