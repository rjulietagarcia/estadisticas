<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.5(Personal)" AutoEventWireup="true" CodeBehind="Personal_911_5.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_5.Personal_911_5" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
   
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 20;
        MaxRow = 26;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
            <li onclick="openPage('AG1_911_5',true)"><a href="#" title=""><span>1�</span></a></li>
            <li onclick="openPage('AG2_911_5',true)"><a href="#" title=""><span>2�</span></a></li>
            <li onclick="openPage('AG3_911_5',true)"><a href="#" title=""  ><span>3�</span></a></li>
            <li onclick="openPage('AGT_911_5',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
            <li onclick="openPage('AGD_911_5',true)"><a href="#" title="" ><span>DESGLOSE</span></a></li>
            <li onclick="openPage('Personal_911_5',true)"><a href="#" title="" class="activo"><span>PERSONAL</span></a></li>
            <li onclick="openPage('CMYA_911_5',false)"><a href="#" title="" ><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
        </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
         <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        

    <center>
            

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table>
            <tr>
                <td style="width: 340px; text-align: center;">
                    <table id="TABLE1" style="text-align: center" >
                       
                        <tr>
                            <td colspan="15" rowspan="1" style="padding-right: 5px; padding-left: 5px; width: 215px;
                                height: 3px; text-align: left">
                                <asp:Label ID="lblPersonal" runat="server" CssClass="lblGrisTit" Font-Bold="True" Text="II. PERSONAL POR FUNCI�N"
                                    Width="250px" Font-Size="16px"></asp:Label><br />
                                <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de personal que realiza funciones de directivo (con y sin grupo), docente, docente especial (profesor de educaci�n f�sica, actividades art�sticas, tecnol�gicas y de idiomas), y administrativo, auxiliar y de servicios, independientemente de su nombramiento, tipo y fuente de pago, desgl�selos seg�n su funci�n, nivel m�ximo de estudios y sexo."
                                    Width="1195px"></asp:Label><br />
                                <br />
                                <asp:Label ID="lblNota1" runat="server" CssClass="lblRojo" Font-Bold="True" Text="Notas:"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="a) Si una persona desempe�a dos o m�s funciones an�tela en aquella a la que dedique m�s tiempo"
                                    Width="1195px"></asp:Label><br />
                                <asp:Label ID="lblNota3" runat="server" CssClass="lblRojo" Text="b) Si en la tabla corresponde al NIVEL EDUCATIVO no se encuentra el nivel requerido, an�telo en el NIVEL que considere equivalente o en OTROS"
                                    Width="1195px"></asp:Label><br />
                                    <asp:Label ID="lblNota4" runat="server" CssClass="lblRojo" Text="c) Considere exclusivamente al personal que labora en el turno al que se refiere este cuestionario, independientemente de que labore en otro turno."
                                    Width="1195px"></asp:Label><br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1" rowspan="2" style="width: 215px; height: 3px; text-align: center; padding-right: 5px; padding-left: 5px;">
                                <asp:Label ID="lblNivelEd" runat="server" CssClass="lblRojo" Text="NIVEL EDUCATIVO" Width="200px"></asp:Label></td>
                            <td colspan="4" rowspan="" style="text-align: center;">
                                <asp:Label ID="lblPersDir" runat="server" CssClass="lblRojo" Text="PERSONAL DIRECTIVO" Width="141px"></asp:Label></td>
                            <td colspan="2" rowspan="2" style="text-align: center">
                                <asp:Label ID="lblPersDocente" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE" Width="85px"></asp:Label></td>
                            <td style="border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;" colspan="6" rowspan="">
                                <asp:Label ID="lblPersDocEsp" runat="server" CssClass="lblRojo" Text="PERSONAL DOCENTE ESPECIAL"
                                    Width="254px"></asp:Label></td>
                            <td colspan="2" rowspan="2" style="text-align: center">
                                <asp:Label ID="lblPersAdmin" runat="server" CssClass="lblRojo" Text="PERSONAL ADMINISTRATIVO, AUXILIAR Y DE SERVICIOS" Height="54px" Width="95px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblPersDirCG" runat="server" CssClass="lblRojo" Text="CON GRUPO" Width="70px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblPersDirSG" runat="server" CssClass="lblRojo" Text="SIN GRUPO" Width="70px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblDocEdFis" runat="server" CssClass="lblRojo" Text="PROFESORES DE EDUCACI�N F�SICA" Width="110px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblDocActArt" runat="server" CssClass="lblRojo" Text="PROFESORES DE ACTIVIDADES ART�STICAS"
                                    Width="100px"></asp:Label></td>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="lblDocActTec" runat="server" CssClass="lblRojo" Text="PROFESORES DE ACTIVIDADES TECNOL�GICAS" Width="100px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="padding-right: 5px; width: 215px">
                            </td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDirCGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDirCGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDirSGH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDirSGM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDocenteH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersDocenteM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocEdFisH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocEdFisM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocActArtH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocActArtM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocActTecH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblDocActTecM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersAdminH" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                            <td style="width: 67px; height: 26px">
                                <asp:Label ID="lblPersAdminM" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 215px; padding-right: 5px; height: 26px; text-align: left;">
                                <asp:Label ID="lblPrimInc" runat="server" CssClass="lblGrisTit" Text="PRIMARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV270" runat="server" Columns="2" TabIndex="10101" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV271" runat="server" Columns="2" TabIndex="10102" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV272" runat="server" Columns="2" TabIndex="10103" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV273" runat="server" Columns="2" TabIndex="10104" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV274" runat="server" Columns="4" TabIndex="10105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV275" runat="server" Columns="4" TabIndex="10106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV276" runat="server" Columns="2" TabIndex="10107" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV277" runat="server" Columns="2" TabIndex="10108" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV278" runat="server" Columns="2" TabIndex="10109" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV279" runat="server" Columns="2" TabIndex="10110" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV280" runat="server" Columns="2" TabIndex="10111" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV281" runat="server" Columns="2" TabIndex="10112" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV282" runat="server" Columns="3" TabIndex="10113" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV283" runat="server" Columns="3" TabIndex="10114" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="PRIMARIA TERMINADA"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV284" runat="server" Columns="2" TabIndex="10201" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV285" runat="server" Columns="2" TabIndex="10202" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV286" runat="server" Columns="2" TabIndex="10203" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV287" runat="server" Columns="2" TabIndex="10204" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV288" runat="server" Columns="4" TabIndex="10205" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV289" runat="server" Columns="4" TabIndex="10206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV290" runat="server" Columns="2" TabIndex="10207" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV291" runat="server" Columns="2" TabIndex="10208" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV292" runat="server" Columns="2" TabIndex="10209" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV293" runat="server" Columns="2" TabIndex="10210" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV294" runat="server" Columns="2" TabIndex="10211" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV295" runat="server" Columns="2" TabIndex="10212" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV296" runat="server" Columns="3" TabIndex="10213" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV297" runat="server" Columns="3" TabIndex="10214" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 215px; margin-right: 5px; text-align: left;">
                            <asp:Label ID="lblSecInc" runat="server" CssClass="lblGrisTit" Text="SECUNDARIA INCOMPLETA" Height="17px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV298" runat="server" Columns="2" TabIndex="10301" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV299" runat="server" Columns="2" TabIndex="10302" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV300" runat="server" Columns="2" TabIndex="10303" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV301" runat="server" Columns="2" TabIndex="10304" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV302" runat="server" Columns="4" TabIndex="10305" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV303" runat="server" Columns="4" TabIndex="10306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV304" runat="server" Columns="2" TabIndex="10307" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV305" runat="server" Columns="2" TabIndex="10308" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV306" runat="server" Columns="2" TabIndex="10309" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV307" runat="server" Columns="2" TabIndex="10310" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV308" runat="server" Columns="2" TabIndex="10311" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV309" runat="server" Columns="2" TabIndex="10312" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV310" runat="server" Columns="3" TabIndex="10313" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV311" runat="server" Columns="3" TabIndex="10314" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblSecTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="SECUNDARIA TERMINADA"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV312" runat="server" Columns="2" TabIndex="10401" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV313" runat="server" Columns="2" TabIndex="10402" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV314" runat="server" Columns="2" TabIndex="10403" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV315" runat="server" Columns="2" TabIndex="10404" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV316" runat="server" Columns="4" TabIndex="10405" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV317" runat="server" Columns="4" TabIndex="10406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV318" runat="server" Columns="2" TabIndex="10407" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV319" runat="server" Columns="2" TabIndex="10408" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV320" runat="server" Columns="2" TabIndex="10409" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV321" runat="server" Columns="2" TabIndex="10410" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV322" runat="server" Columns="2" TabIndex="10411" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV323" runat="server" Columns="2" TabIndex="10412" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV324" runat="server" Columns="3" TabIndex="10413" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV325" runat="server" Columns="3" TabIndex="10414" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblProfTec" runat="server" CssClass="lblGrisTit" Height="17px" Text="PROFESIONAL T�CNICO"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV326" runat="server" Columns="2" TabIndex="10501" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV327" runat="server" Columns="2" TabIndex="10502" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV328" runat="server" Columns="2" TabIndex="10503" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV329" runat="server" Columns="2" TabIndex="10504" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV330" runat="server" Columns="4" TabIndex="10505" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV331" runat="server" Columns="4" TabIndex="10506" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV332" runat="server" Columns="2" TabIndex="10507" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV333" runat="server" Columns="2" TabIndex="10508" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV334" runat="server" Columns="2" TabIndex="10509" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV335" runat="server" Columns="2" TabIndex="10510" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV336" runat="server" Columns="2" TabIndex="10511" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV337" runat="server" Columns="2" TabIndex="10512" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV338" runat="server" Columns="3" TabIndex="10513" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV339" runat="server" Columns="3" TabIndex="10514" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblBachInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO INCOMPLETO"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV340" runat="server" Columns="2" TabIndex="10601" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV341" runat="server" Columns="2" TabIndex="10602" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV342" runat="server" Columns="2" TabIndex="10603" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV343" runat="server" Columns="2" TabIndex="10604" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV344" runat="server" Columns="4" TabIndex="10605" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV345" runat="server" Columns="4" TabIndex="10606" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV346" runat="server" Columns="2" TabIndex="10607" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV347" runat="server" Columns="2" TabIndex="10608" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV348" runat="server" Columns="2" TabIndex="10609" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV349" runat="server" Columns="2" TabIndex="10610" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV350" runat="server" Columns="2" TabIndex="10611" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV351" runat="server" Columns="2" TabIndex="10612" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV352" runat="server" Columns="3" TabIndex="10613" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV353" runat="server" Columns="3" TabIndex="10614" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 27px; text-align: left">
                                <asp:Label ID="lblBachTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="BACHILLERATO TERMINADO"></asp:Label></td>
                            <td style="width: 67px; height: 27px; text-align: center">
                                <asp:TextBox ID="txtV354" runat="server" Columns="2" TabIndex="10701" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV355" runat="server" Columns="2" TabIndex="10702" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV356" runat="server" Columns="2" TabIndex="10703" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV357" runat="server" Columns="2" TabIndex="10704" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV358" runat="server" Columns="4" TabIndex="10705" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV359" runat="server" Columns="4" TabIndex="10706" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV360" runat="server" Columns="2" TabIndex="10707" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV361" runat="server" Columns="2" TabIndex="10708" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV362" runat="server" Columns="2" TabIndex="10709" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV363" runat="server" Columns="2" TabIndex="10710" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV364" runat="server" Columns="2" TabIndex="10711" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV365" runat="server" Columns="2" TabIndex="10712" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV366" runat="server" Columns="3" TabIndex="10713" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 27px; text-align: center;">
                                <asp:TextBox ID="txtV367" runat="server" Columns="3" TabIndex="10714" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormPreeInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR INCOMPLETA"
                                    Width="215px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV368" runat="server" Columns="2" TabIndex="10801" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV369" runat="server" Columns="2" TabIndex="10802" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV370" runat="server" Columns="2" TabIndex="10803" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV371" runat="server" Columns="2" TabIndex="10804" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV372" runat="server" Columns="4" TabIndex="10805" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV373" runat="server" Columns="4" TabIndex="10806" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV374" runat="server" Columns="2" TabIndex="10807" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV375" runat="server" Columns="2" TabIndex="10808" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV376" runat="server" Columns="2" TabIndex="10809" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV377" runat="server" Columns="2" TabIndex="10810" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV378" runat="server" Columns="2" TabIndex="10811" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV379" runat="server" Columns="2" TabIndex="10812" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV380" runat="server" Columns="3" TabIndex="10813" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV381" runat="server" Columns="3" TabIndex="10814" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormPreeTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PREESCOLAR TERMINADA"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV382" runat="server" Columns="2" TabIndex="10901" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV383" runat="server" Columns="2" TabIndex="10902" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV384" runat="server" Columns="2" TabIndex="10903" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV385" runat="server" Columns="2" TabIndex="10904" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV386" runat="server" Columns="4" TabIndex="10905" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV387" runat="server" Columns="4" TabIndex="10906" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV388" runat="server" Columns="2" TabIndex="10907" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV389" runat="server" Columns="2" TabIndex="10908" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV390" runat="server" Columns="2" TabIndex="10909" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV391" runat="server" Columns="2" TabIndex="10910" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV392" runat="server" Columns="2" TabIndex="10911" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV393" runat="server" Columns="2" TabIndex="10912" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV394" runat="server" Columns="3" TabIndex="10913" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV395" runat="server" Columns="3" TabIndex="10914" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 15px; text-align: left">
                                <asp:Label ID="lblNormPrimInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA INCOMPLETA"></asp:Label></td>
                            <td style="width: 67px; height: 15px; text-align: center">
                                <asp:TextBox ID="txtV396" runat="server" Columns="2" TabIndex="11001" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV397" runat="server" Columns="2" TabIndex="11002" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV398" runat="server" Columns="2" TabIndex="11003" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV399" runat="server" Columns="2" TabIndex="11004" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV400" runat="server" Columns="4" TabIndex="11005" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV401" runat="server" Columns="4" TabIndex="11006" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV402" runat="server" Columns="2" TabIndex="11007" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV403" runat="server" Columns="2" TabIndex="11008" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV404" runat="server" Columns="2" TabIndex="11009" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV405" runat="server" Columns="2" TabIndex="11010" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV406" runat="server" Columns="2" TabIndex="11011" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV407" runat="server" Columns="2" TabIndex="11012" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV408" runat="server" Columns="3" TabIndex="11013" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 15px; text-align: center;">
                                <asp:TextBox ID="txtV409" runat="server" Columns="3" TabIndex="11014" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormPrimTerm" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL PRIMARIA TERMINADA"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV410" runat="server" Columns="2" TabIndex="11101" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV411" runat="server" Columns="2" TabIndex="11102" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV412" runat="server" Columns="2" TabIndex="11103" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV413" runat="server" Columns="2" TabIndex="11104" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV414" runat="server" Columns="4" TabIndex="11105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV415" runat="server" Columns="4" TabIndex="11106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV416" runat="server" Columns="2" TabIndex="11107" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV417" runat="server" Columns="2" TabIndex="11108" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV418" runat="server" Columns="2" TabIndex="11109" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV419" runat="server" Columns="2" TabIndex="11110" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV420" runat="server" Columns="2" TabIndex="11111" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV421" runat="server" Columns="2" TabIndex="11112" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV422" runat="server" Columns="3" TabIndex="11113" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV423" runat="server" Columns="3" TabIndex="11114" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormSupInc" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR INCOMPLETA"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV424" runat="server" Columns="2" TabIndex="11201" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV425" runat="server" Columns="2" TabIndex="11202" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV426" runat="server" Columns="2" TabIndex="11203" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV427" runat="server" Columns="2" TabIndex="11204" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV428" runat="server" Columns="4" TabIndex="11205" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV429" runat="server" Columns="4" TabIndex="11206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV430" runat="server" Columns="2" TabIndex="11207" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV431" runat="server" Columns="2" TabIndex="11208" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV432" runat="server" Columns="2" TabIndex="11209" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV433" runat="server" Columns="2" TabIndex="11210" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV434" runat="server" Columns="2" TabIndex="11211" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV435" runat="server" Columns="2" TabIndex="11212" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV436" runat="server" Columns="3" TabIndex="11213" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV437" runat="server" Columns="3" TabIndex="11214" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblNormSupPas" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, PASANTE"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV438" runat="server" Columns="2" TabIndex="11301" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV439" runat="server" Columns="2" TabIndex="11302" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV440" runat="server" Columns="2" TabIndex="11303" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV441" runat="server" Columns="2" TabIndex="11304" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV442" runat="server" Columns="4" TabIndex="11305" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV443" runat="server" Columns="4" TabIndex="11306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV444" runat="server" Columns="2" TabIndex="11307" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV445" runat="server" Columns="2" TabIndex="11308" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV446" runat="server" Columns="2" TabIndex="11309" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV447" runat="server" Columns="2" TabIndex="11310" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV448" runat="server" Columns="2" TabIndex="11311" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV449" runat="server" Columns="2" TabIndex="11312" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV450" runat="server" Columns="3" TabIndex="11313" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV451" runat="server" Columns="3" TabIndex="11314" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 12px; text-align: left">
                                <asp:Label ID="lblNormSupTit" runat="server" CssClass="lblGrisTit" Height="17px" Text="NORMAL SUPERIOR, TITULADO"></asp:Label></td>
                            <td style="width: 67px; height: 12px; text-align: center">
                                <asp:TextBox ID="txtV452" runat="server" Columns="2" TabIndex="11401" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV453" runat="server" Columns="2" TabIndex="11402" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV454" runat="server" Columns="2" TabIndex="11403" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV455" runat="server" Columns="2" TabIndex="11404" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV456" runat="server" Columns="4" TabIndex="11405" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV457" runat="server" Columns="4" TabIndex="11406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV458" runat="server" Columns="2" TabIndex="11407" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV459" runat="server" Columns="2" TabIndex="11408" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV460" runat="server" Columns="2" TabIndex="11409" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV461" runat="server" Columns="2" TabIndex="11410" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV462" runat="server" Columns="2" TabIndex="11411" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV463" runat="server" Columns="2" TabIndex="11412" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV464" runat="server" Columns="3" TabIndex="11413" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 12px; text-align: center;">
                                <asp:TextBox ID="txtV465" runat="server" Columns="3" TabIndex="11414" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblLicInc" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA INCOMPLETA"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV466" runat="server" Columns="2" TabIndex="11501" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV467" runat="server" Columns="2" TabIndex="11502" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV468" runat="server" Columns="2" TabIndex="11503" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV469" runat="server" Columns="2" TabIndex="11504" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV470" runat="server" Columns="4" TabIndex="11505" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV471" runat="server" Columns="4" TabIndex="11506" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV472" runat="server" Columns="2" TabIndex="11507" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV473" runat="server" Columns="2" TabIndex="11508" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV474" runat="server" Columns="2" TabIndex="11509" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV475" runat="server" Columns="2" TabIndex="11510" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV476" runat="server" Columns="2" TabIndex="11511" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV477" runat="server" Columns="2" TabIndex="11512" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV478" runat="server" Columns="3" TabIndex="11513" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV479" runat="server" Columns="3" TabIndex="11514" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblLicPas" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, PASANTE"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV480" runat="server" Columns="2" TabIndex="11601" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV481" runat="server" Columns="2" TabIndex="11602" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV482" runat="server" Columns="2" TabIndex="11603" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV483" runat="server" Columns="2" TabIndex="11604" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV484" runat="server" Columns="4" TabIndex="11605" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV485" runat="server" Columns="4" TabIndex="11606" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV486" runat="server" Columns="2" TabIndex="11607" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV487" runat="server" Columns="2" TabIndex="11608" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV488" runat="server" Columns="2" TabIndex="11609" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV489" runat="server" Columns="2" TabIndex="11610" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV490" runat="server" Columns="2" TabIndex="11611" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV491" runat="server" Columns="2" TabIndex="11612" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV492" runat="server" Columns="3" TabIndex="11613" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV493" runat="server" Columns="3" TabIndex="11614" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 21px; text-align: left">
                            <asp:Label ID="lblLicTit" runat="server" CssClass="lblGrisTit" Text="LICENCIATURA, TITULADO"></asp:Label></td>
                            <td style="width: 67px; height: 21px; text-align: center">
                                <asp:TextBox ID="txtV494" runat="server" Columns="2" TabIndex="11701" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV495" runat="server" Columns="2" TabIndex="11702" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV496" runat="server" Columns="2" TabIndex="11703" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV497" runat="server" Columns="2" TabIndex="11704" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV498" runat="server" Columns="4" TabIndex="11705" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV499" runat="server" Columns="4" TabIndex="11706" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV500" runat="server" Columns="2" TabIndex="11707" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV501" runat="server" Columns="2" TabIndex="11708" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV502" runat="server" Columns="2" TabIndex="11709" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV503" runat="server" Columns="2" TabIndex="11710" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV504" runat="server" Columns="2" TabIndex="11711" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV505" runat="server" Columns="2" TabIndex="11712" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV506" runat="server" Columns="3" TabIndex="11713" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 21px; text-align: center;">
                                <asp:TextBox ID="txtV507" runat="server" Columns="3" TabIndex="11714" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblMaestInc" runat="server" CssClass="lblGrisTit" Text="MAESTR�A INCOMPLETA"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV508" runat="server" Columns="2" TabIndex="11801" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV509" runat="server" Columns="2" TabIndex="11802" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV510" runat="server" Columns="2" TabIndex="11803" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV511" runat="server" Columns="2" TabIndex="11804" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV512" runat="server" Columns="4" TabIndex="11805" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV513" runat="server" Columns="4" TabIndex="11806" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV514" runat="server" Columns="2" TabIndex="11807" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV515" runat="server" Columns="2" TabIndex="11808" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV516" runat="server" Columns="2" TabIndex="11809" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV517" runat="server" Columns="2" TabIndex="11810" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV518" runat="server" Columns="2" TabIndex="11811" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV519" runat="server" Columns="2" TabIndex="11812" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV520" runat="server" Columns="3" TabIndex="11813" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV521" runat="server" Columns="3" TabIndex="11814" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 17px; text-align: left">
                                <asp:Label ID="lblMaestGrad" runat="server" CssClass="lblGrisTit" Text="MAESTR�A, GRADUADO"></asp:Label></td>
                            <td style="width: 67px; height: 17px; text-align: center">
                                <asp:TextBox ID="txtV522" runat="server" Columns="2" TabIndex="11901" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV523" runat="server" Columns="2" TabIndex="11902" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV524" runat="server" Columns="2" TabIndex="11903" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV525" runat="server" Columns="2" TabIndex="11904" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV526" runat="server" Columns="4" TabIndex="11905" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV527" runat="server" Columns="4" TabIndex="11906" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV528" runat="server" Columns="2" TabIndex="11907" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV529" runat="server" Columns="2" TabIndex="11908" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV530" runat="server" Columns="2" TabIndex="11909" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV531" runat="server" Columns="2" TabIndex="11910" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV532" runat="server" Columns="2" TabIndex="11911" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV533" runat="server" Columns="2" TabIndex="11912" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV534" runat="server" Columns="3" TabIndex="11913" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 17px; text-align: center;">
                                <asp:TextBox ID="txtV535" runat="server" Columns="3" TabIndex="11914" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblDocInc" runat="server" CssClass="lblGrisTit" Text="DOCTORADO INCOMPLETO"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV536" runat="server" Columns="2" TabIndex="12001" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV537" runat="server" Columns="2" TabIndex="12002" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV538" runat="server" Columns="2" TabIndex="12003" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV539" runat="server" Columns="2" TabIndex="12004" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV540" runat="server" Columns="4" TabIndex="12005" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV541" runat="server" Columns="4" TabIndex="12006" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV542" runat="server" Columns="2" TabIndex="12007" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV543" runat="server" Columns="2" TabIndex="12008" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV544" runat="server" Columns="2" TabIndex="12009" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV545" runat="server" Columns="2" TabIndex="12010" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV546" runat="server" Columns="2" TabIndex="12011" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV547" runat="server" Columns="2" TabIndex="12012" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV548" runat="server" Columns="3" TabIndex="12013" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV549" runat="server" Columns="3" TabIndex="12014" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:Label ID="lblDocGrad" runat="server" CssClass="lblGrisTit" Text="DOCTORADO, GRADUADO"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV550" runat="server" Columns="2" TabIndex="12101" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV551" runat="server" Columns="2" TabIndex="12102" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV552" runat="server" Columns="2" TabIndex="12103" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV553" runat="server" Columns="2" TabIndex="12104" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV554" runat="server" Columns="4" TabIndex="12105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV555" runat="server" Columns="4" TabIndex="12106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV556" runat="server" Columns="2" TabIndex="12107" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV557" runat="server" Columns="2" TabIndex="12108" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV558" runat="server" Columns="2" TabIndex="12109" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV559" runat="server" Columns="2" TabIndex="12110" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV560" runat="server" Columns="2" TabIndex="12111" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV561" runat="server" Columns="2" TabIndex="12112" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV562" runat="server" Columns="3" TabIndex="12113" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV563" runat="server" Columns="3" TabIndex="12114" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 15px; text-align: left">
                                <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Text="OTROS*"></asp:Label><br />
                                &nbsp;<asp:Label ID="lblEspecifique" runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE"></asp:Label></td>
                            <td style="width: 67px; height: 15px; text-align: center; vertical-align: top;">
                            </td>
                            <td style="width: 67px; height: 15px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 15px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 15px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 15px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 15px; vertical-align: top; text-align: center;">
                            </td>
                            <td style="width: 67px; height: 15px">
                            </td>
                            <td style="width: 67px; height: 15px">
                            </td>
                            <td style="width: 67px; height: 15px">
                            </td>
                            <td style="width: 67px; height: 15px">
                            </td>
                            <td style="width: 67px; height: 15px">
                            </td>
                            <td style="width: 67px; height: 15px">
                            </td>
                            <td style="width: 67px; height: 15px">
                            </td>
                            <td style="width: 67px; height: 15px">
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV564" runat="server" Columns="30" TabIndex="12201" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV565" runat="server" Columns="2" TabIndex="12202" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV566" runat="server" Columns="2" TabIndex="12203" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV567" runat="server" Columns="2" TabIndex="12204" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV568" runat="server" Columns="2" TabIndex="12205" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV569" runat="server" Columns="4" TabIndex="12206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV570" runat="server" Columns="4" TabIndex="12207" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV571" runat="server" Columns="2" TabIndex="12208" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV572" runat="server" Columns="2" TabIndex="12209" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV573" runat="server" Columns="2" TabIndex="12210" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV574" runat="server" Columns="2" TabIndex="12211" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV575" runat="server" Columns="2" TabIndex="12212" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV576" runat="server" Columns="2" TabIndex="12213" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV577" runat="server" Columns="3" TabIndex="12214" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV578" runat="server" Columns="3" TabIndex="12215" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 26px; text-align: left">
                                <asp:TextBox ID="txtV579" runat="server" Columns="30" TabIndex="12301" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV580" runat="server" Columns="2" TabIndex="12302" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV581" runat="server" Columns="2" TabIndex="12303" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV582" runat="server" Columns="2" TabIndex="12304" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV583" runat="server" Columns="2" TabIndex="12305" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV584" runat="server" Columns="4" TabIndex="12306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV585" runat="server" Columns="4" TabIndex="12307" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV586" runat="server" Columns="2" TabIndex="12308" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV587" runat="server" Columns="2" TabIndex="12309" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV588" runat="server" Columns="2" TabIndex="12310" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV589" runat="server" Columns="2" TabIndex="12311" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV590" runat="server" Columns="2" TabIndex="12312" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px; text-align: center;">
                                <asp:TextBox ID="txtV591" runat="server" Columns="2" TabIndex="12313" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV592" runat="server" Columns="3" TabIndex="12314" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 26px">
                                <asp:TextBox ID="txtV593" runat="server" Columns="3" TabIndex="12315" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; height: 30px; text-align: left">
                                <asp:TextBox ID="txtV594" runat="server" Columns="30" TabIndex="12401" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center">
                                <asp:TextBox ID="txtV595" runat="server" Columns="2" TabIndex="12402" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV596" runat="server" Columns="2" TabIndex="12403" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV597" runat="server" Columns="2" TabIndex="12404" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV598" runat="server" Columns="2" TabIndex="12405" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV599" runat="server" Columns="4" TabIndex="12406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV600" runat="server" Columns="4" TabIndex="12407" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV601" runat="server" Columns="2" TabIndex="12408" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV602" runat="server" Columns="2" TabIndex="12409" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV603" runat="server" Columns="2" TabIndex="12410" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV604" runat="server" Columns="2" TabIndex="12411" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV605" runat="server" Columns="2" TabIndex="12412" CssClass="lblNegro" MaxLength="1"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV606" runat="server" Columns="2" TabIndex="12413" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV607" runat="server" Columns="3" TabIndex="12414" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; height: 30px; text-align: center;">
                                <asp:TextBox ID="txtV608" runat="server" Columns="3" TabIndex="12415" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td rowspan="1" style="width: 215px; text-align: left">
                                <asp:Label ID="lblSubtotales" runat="server" CssClass="lblGrisTit" Text="SUBTOTALES"></asp:Label></td>
                            <td style="width: 67px; text-align: center">
                                <asp:TextBox ID="txtV609" runat="server" Columns="2" TabIndex="12501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV610" runat="server" Columns="2" TabIndex="12502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV611" runat="server" Columns="2" TabIndex="12503" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px;">
                                <asp:TextBox ID="txtV612" runat="server" Columns="2" TabIndex="12504" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px;">
                                <asp:TextBox ID="txtV613" runat="server" Columns="4" TabIndex="12505" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV614" runat="server" Columns="4" TabIndex="12506" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV615" runat="server" Columns="2" TabIndex="12507" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV616" runat="server" Columns="2" TabIndex="12508" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV617" runat="server" Columns="2" TabIndex="12509" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV618" runat="server" Columns="2" TabIndex="12510" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV619" runat="server" Columns="2" TabIndex="12511" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV620" runat="server" Columns="2" TabIndex="12512" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV621" runat="server" Columns="3" TabIndex="12513" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                            <td style="width: 67px; text-align: center;">
                                <asp:TextBox ID="txtV622" runat="server" Columns="3" TabIndex="12514" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="14" rowspan="1" style="text-align: right">
                                <asp:Label ID="lblTotal" runat="server" CssClass="lblRojo" Text="TOTAL PERSONAL (Suma de subtotales)"
                                    Width="275px"></asp:Label></td>
                            <td style="width: 67px; height: 26px; text-align: center">
                                <asp:TextBox ID="txtV623" runat="server" Columns="4" TabIndex="12601" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="5" rowspan="1" style="text-align: center; vertical-align: top;">
                                <br />
                                <br />
                                <br />
                                <br />
                                <table style="width: 455px; text-align: center">
                                    <tr>
                                        <td colspan="4" style="padding-bottom: 20px; text-align: left">
                                            <asp:Label ID="lblInstrucciones2" runat="server" CssClass="lblRojo" Text="2. Escriba la cantidad de personal administrativo, auxiliar y de servicios seg�n su funci�n y sexo. (El total debe coincidir con el total del rubro de personal administrativo, auxiliar y de servicios reportado en el punto 1 de esta secci�n )"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 10px; width: 3px">
                                        </td>
                                        <td style="padding-bottom: 10px; width: 67px">
                                            <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES"></asp:Label></td>
                                        <td style="padding-bottom: 10px; width: 67px">
                                            <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES"></asp:Label></td>
                                        <td style="padding-bottom: 10px; width: 67px">
                                            <asp:Label ID="lblTotal2" runat="server" CssClass="lblGrisTit" Text="TOTAL"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblSecretarios" runat="server" CssClass="lblGrisTit" Text="SECRETARIAS" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV624" runat="server" Columns="3" TabIndex="20101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV625" runat="server" Columns="3" TabIndex="20102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV626" runat="server" Columns="3" TabIndex="20103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblAdjuntos" runat="server" CssClass="lblGrisTit" Text="AUXILIAR DE INTENDENCIA" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV627" runat="server" Columns="3" TabIndex="20201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV628" runat="server" Columns="3" TabIndex="20202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV629" runat="server" Columns="3" TabIndex="20203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblAdjCambAct" runat="server" CssClass="lblGrisTit" Text="VIGILANTE"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV630" runat="server" Columns="3" TabIndex="20301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV631" runat="server" Columns="3" TabIndex="20302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV632" runat="server" Columns="3" TabIndex="20303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblProyectos" runat="server" CssClass="lblGrisTit" Text="PREFECTO" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV633" runat="server" Columns="3" TabIndex="20401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV634" runat="server" Columns="3" TabIndex="20402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV635" runat="server" Columns="3" TabIndex="20403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblEspeciales" runat="server" CssClass="lblGrisTit" Text="TRABAJO SOCIAL" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV636" runat="server" Columns="3" TabIndex="20501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV637" runat="server" Columns="3" TabIndex="20502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV638" runat="server" Columns="3" TabIndex="20503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblIntendentes" runat="server" CssClass="lblGrisTit" Text="ORIENTADOR VOCACIONAL" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV639" runat="server" Columns="3" TabIndex="20601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV640" runat="server" Columns="3" TabIndex="20602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV641" runat="server" Columns="3" TabIndex="20603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblConserjes" runat="server" CssClass="lblGrisTit" Text="M�DICO ESCOLAR" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV642" runat="server" Columns="3" TabIndex="20701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV643" runat="server" Columns="3" TabIndex="20702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV644" runat="server" Columns="3" TabIndex="20703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblOtros2" runat="server" CssClass="lblGrisTit" Text="AUXILIARES ADMINISTRATIVOS" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV645" runat="server" Columns="3" TabIndex="20801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV646" runat="server" Columns="3" TabIndex="20802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV647" runat="server" Columns="3" TabIndex="20803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblAuxContables" runat="server" CssClass="lblGrisTit" Text="AUXILIARES CONTABLES (CONTRALOR)" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV648" runat="server" Columns="3" TabIndex="20901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV649" runat="server" Columns="3" TabIndex="20902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV650" runat="server" Columns="3" TabIndex="20903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblAlmacenista" runat="server" CssClass="lblGrisTit" Text="ALMACENISTA" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV651" runat="server" Columns="3" TabIndex="21001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV652" runat="server" Columns="3" TabIndex="21002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV653" runat="server" Columns="3" TabIndex="21003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblCoordActTec" runat="server" CssClass="lblGrisTit" Text="COORDINADORES DE ACTIVIDADES TECNOL�GICAS"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV654" runat="server" Columns="3" TabIndex="21101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV655" runat="server" Columns="3" TabIndex="21102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV656" runat="server" Columns="3" TabIndex="21103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblCoordAcrAcad" runat="server" CssClass="lblGrisTit" Text="COORDINADORES DE ACTIVIDADES ACAD�MICAS"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV657" runat="server" Columns="3" TabIndex="21201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV658" runat="server" Columns="3" TabIndex="21202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV659" runat="server" Columns="3" TabIndex="21203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblTecMtto" runat="server" CssClass="lblGrisTit" Text="T�CNICOS EN MANTENIMIENTO"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV660" runat="server" Columns="3" TabIndex="21301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV661" runat="server" Columns="3" TabIndex="21302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV662" runat="server" Columns="3" TabIndex="21303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblAsistMtto" runat="server" CssClass="lblGrisTit" Text="ASISTENTE DE SERVICIOS DE MANTENIMIENTO (CONSERJE)"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV663" runat="server" Columns="3" TabIndex="21401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV664" runat="server" Columns="3" TabIndex="21402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV665" runat="server" Columns="3" TabIndex="21403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblJefeServAdmin" runat="server" CssClass="lblGrisTit" Text="JEFE DE SERVICIOS ADMINISTRATIVOS"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV666" runat="server" Columns="3" TabIndex="21501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV667" runat="server" Columns="3" TabIndex="21502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV668" runat="server" Columns="3" TabIndex="21503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblBibliotecario" runat="server" CssClass="lblGrisTit" Text="BIBLIOTECARIO"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV669" runat="server" Columns="3" TabIndex="21601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV670" runat="server" Columns="3" TabIndex="21602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV671" runat="server" Columns="3" TabIndex="21603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblAyudLab" runat="server" CssClass="lblGrisTit" Text="AYUDANTES DE LABORATORIO"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV672" runat="server" Columns="3" TabIndex="21701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV673" runat="server" Columns="3" TabIndex="21702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV674" runat="server" Columns="3" TabIndex="21703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblOtros3" runat="server" CssClass="lblGrisTit" Text="OTROS*" Width="250px"></asp:Label>
                                            <asp:Label ID="lblEspecifique3" runat="server" CssClass="lblGrisTit" Text="*ESPECIFIQUE"
                                                Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left; height: 26px;">
                                            <asp:TextBox ID="txtV675" runat="server" Columns="30" TabIndex="21801" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center; height: 26px;">
                                            <asp:TextBox ID="txtV676" runat="server" Columns="3" TabIndex="21802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center; height: 26px;">
                                            <asp:TextBox ID="txtV677" runat="server" Columns="3" TabIndex="21803" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center; height: 26px;">
                                            <asp:TextBox ID="txtV678" runat="server" Columns="3" TabIndex="21804" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:TextBox ID="txtV679" runat="server" Columns="30" TabIndex="21901" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV680" runat="server" Columns="3" TabIndex="21902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV681" runat="server" Columns="3" TabIndex="21903" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV682" runat="server" Columns="3" TabIndex="21904" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:TextBox ID="txtV683" runat="server" Columns="30" TabIndex="22001" CssClass="lblNegro" MaxLength="30"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV684" runat="server" Columns="3" TabIndex="22002" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV685" runat="server" Columns="3" TabIndex="22003" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV686" runat="server" Columns="3" TabIndex="22004" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 3px; text-align: left">
                                            <asp:Label ID="lblTotal3" runat="server" CssClass="lblRojo" Text="TOTAL" Width="250px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV687" runat="server" Columns="3" TabIndex="22101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV688" runat="server" Columns="3" TabIndex="22102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV689" runat="server" Columns="3" TabIndex="22103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="6" rowspan="1" style="text-align: center; padding-right: 10px; padding-left: 10px; vertical-align: top;">
                                <br />
                                <br />
                                <br />
                                <br />
                                <table style="text-align: center; width: 5px;">
                                    <tr>
                                        <td colspan="2" style="padding-bottom: 20px; text-align: left">
                                            <asp:Label ID="lblInstrucciones3" runat="server" CssClass="lblRojo" Text="3. Escriba la cantidad de horas impartidas a la semana por el personal docente especial en el centro de trabajo."
                                                Width="350px"></asp:Label><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left">
                                            <asp:Label ID="lblProfEdFis" runat="server" CssClass="lblGrisTit" Text="Profesores de educaci�n f�sica"
                                                Width="280px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV690" runat="server" Columns="3" TabIndex="30101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left">
                                            <asp:Label ID="lblProfMusica" runat="server" CssClass="lblGrisTit" Text="Profesores de m�sica, danza, etc�tera (actividades art�sticas)."
                                                Width="280px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV691" runat="server" Columns="3" TabIndex="30201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 67px; text-align: left">
                                            <asp:Label ID="lblHorasActTec" runat="server" CssClass="lblGrisTit" Text="Profesores de actividades tecnol�gicas."
                                                Width="280px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV692" runat="server" Columns="3" TabIndex="30301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                            <td colspan="4" rowspan="1" style="text-align: center; vertical-align: top;">
                                <br />
                                <br />
                                <br />
                                &nbsp;<table style="text-align: center">
                                    <tr>
                                        <td colspan="2" style="padding-bottom: 20px; text-align: left">
                                            <asp:Label ID="lblInstrucciones4" runat="server" CssClass="lblRojo" Text="4. Sume el personal directivo con grupo, personal docente y personal docente especial, y an�telo seg�n el tiempo que dedica a la funci�n acad�mica."
                                                Width="270px"></asp:Label><br />
                                            <asp:Label ID="lblNota5" runat="server" CssClass="lblRojo" Text="NOTA: Si en la instituci�n no se utiliza el t�rmino tres cuartos de tiempo, no lo considere."
                                                Width="270px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblTiempoCompleto" runat="server" CssClass="lblGrisTit" Text="TIEMPO COMPLETO"></asp:Label></td>
                                        <td style="text-align: right">
                                            <asp:TextBox ID="txtV693" runat="server" Columns="3" TabIndex="30401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblTiempo3Cuartos" runat="server" CssClass="lblGrisTit" Text="TRES CUARTOS DE TIEMPO"></asp:Label></td>
                                        <td style="text-align: right">
                                            <asp:TextBox ID="txtV694" runat="server" Columns="3" TabIndex="30501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblMedioTiempo" runat="server" CssClass="lblGrisTit" Text="MEDIO TIEMPO"></asp:Label></td>
                                        <td style="text-align: right">
                                            <asp:TextBox ID="txtV695" runat="server" Columns="3" TabIndex="30601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; height: 26px;">
                                            <asp:Label ID="lblPorHoras" runat="server" CssClass="lblGrisTit" Text="POR HORAS"></asp:Label></td>
                                        <td style="text-align: right; height: 26px;">
                                            <asp:TextBox ID="txtV696" runat="server" Columns="3" TabIndex="30701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Label ID="lblTotal4" runat="server" CssClass="lblRojo" Text="TOTAL"></asp:Label>
                                            <asp:Label ID="lblNotaTotal4" runat="server" CssClass="lblGrisTit" Text="(Este total debe de coincidir con la suma de personal directivo con grupo, personal docente y personal especial reportado en la pregunta 1 de esta secci�n.)"></asp:Label></td>
                                        <td style="text-align: right; vertical-align: top;">
                                            <asp:TextBox ID="txtV697" runat="server" Columns="3" TabIndex="30801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
  
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="OpenPageCharged('AGD_911_5',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="OpenPageCharged('AGD_911_5',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;</td>
                <td ><span  onclick="OpenPageCharged('CMYA_911_5',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="OpenPageCharged('CMYA_911_5',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado"></div>


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                function OpenPageCharged(pagina){
                    Guion('564');
                    Guion('579');
                    Guion('594');
                    Guion('675');
                    Guion('679');
                    Guion('683');
                    openPage(pagina);
                }
                
                function Guion(variable){
                    var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                    if (txtv != null){
                       if (txtv.value == "")
                          txtv.value = "_";  
                    }
                }
 		        Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
