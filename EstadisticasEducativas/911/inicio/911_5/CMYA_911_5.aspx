<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.3(Carrera Magisteria y Aulas)" AutoEventWireup="true" CodeBehind="CMYA_911_5.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_5.CMYA_911_5" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
   
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 20;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
            <li onclick="openPage('AG1_911_5',true)"><a href="#" title=""><span>1�</span></a></li>
            <li onclick="openPage('AG2_911_5',true)"><a href="#" title=""><span>2�</span></a></li>
            <li onclick="openPage('AG3_911_5',true)"><a href="#" title=""  ><span>3�</span></a></li>
            <li onclick="openPage('AGT_911_5',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
            <li onclick="openPage('AGD_911_5',true)"><a href="#" title="" ><span>DESGLOSE</span></a></li>
            <li onclick="openPage('Personal_911_5',true)"><a href="#" title="" ><span>PERSONAL</span></a></li>
            <li onclick="openPage('CMYA_911_5',true)"><a href="#" title="" class="activo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
            <li onclick="openPage('Gasto_911_5',false)"><a href="#" title="" ><span>GASTO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
        </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
   <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
        <center>
            

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 100%; text-align: center">
            <tr>
                <td style="width: 100%; text-align: center">
                    <table align="center" id="TABLE1" style="text-align: center">
                        <tr>
                            <td colspan="5" rowspan="1" style="vertical-align: top; text-align: center">
                                <table style="width: 455px; text-align: center">
                                
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 20px; text-align:justify">
                                            <asp:Label ID="lblCarrera" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="III. CARRERA MAGISTERIAL"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; text-align: left">
                                            <asp:Label ID="lbl1" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de profesores que se encuentran en el programa de carrera magisterial"
                                                Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; text-align: right">
                                            <asp:TextBox ID="txtV698" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; text-align: left">
                                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Desglose la cantidad anotada en el inciso anterior, seg�n la vertiente y el nivel en que se encuentran los profesores." Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl1aVertiente" runat="server" CssClass="lblRojo" Text="1a. VERTIENTE"
                                                Width="160px" Height="21px"></asp:Label>
                                            <br />
                                            <asp:Label ID="lbl1aVertiente2" runat="server"
                                                    CssClass="lblRojo" Height="21px" Text="(Profesores frente a grupo)" Width="160px"></asp:Label></td>
                                        <td style="text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA1" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV699" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB1" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV700" runat="server" Columns="3" TabIndex="10301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC1" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV701" runat="server" Columns="3" TabIndex="10401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC1" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV702" runat="server" Columns="3" TabIndex="10501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD1" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV703" runat="server" Columns="3" TabIndex="10601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE1" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV704" runat="server" Columns="3" TabIndex="10701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="vertical-align: top; text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl2aVertiente" runat="server" CssClass="lblRojo" Height="21px" Text="2a. VERTIENTE"
                                                Width="160px"></asp:Label>
                                            <asp:Label ID="lbl2aVertiente2" runat="server" CssClass="lblRojo" Height="21px" Text="(Docentes en funciones directivas y de supervisi�n)"
                                                Width="160px"></asp:Label></td>
                                        <td style="text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA2" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV705" runat="server" Columns="3" TabIndex="10801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB2" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV706" runat="server" Columns="3" TabIndex="10901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC2" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV707" runat="server" Columns="3" TabIndex="11001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC2" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV708" runat="server" Columns="3" TabIndex="11101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD2" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV709" runat="server" Columns="3" TabIndex="11201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE2" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV710" runat="server" Columns="3" TabIndex="11301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="vertical-align: top; text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" rowspan="6" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl3aVertiente" runat="server" CssClass="lblRojo" Height="21px" Text="3a. VERTIENTE"
                                                Width="160px"></asp:Label>
                                            <asp:Label ID="lbl3aVertiente2" runat="server" CssClass="lblRojo" Height="21px" Text="(Docentes en actividades t�cno-pedag�gicas)"
                                                Width="160px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelA3" runat="server" CssClass="lblGrisTit" Text="Nivel A" Width="150px"></asp:Label></td>
                                        <td style="text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV711" runat="server" Columns="3" TabIndex="11401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelB3" runat="server" CssClass="lblGrisTit" Text="Nivel B" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV712" runat="server" Columns="3" TabIndex="11501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelBC3" runat="server" CssClass="lblGrisTit" Text="Nivel BC" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV713" runat="server" Columns="3" TabIndex="11601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelC3" runat="server" CssClass="lblGrisTit" Text="Nivel C" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV714" runat="server" Columns="3" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelD3" runat="server" CssClass="lblGrisTit" Text="Nivel D" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV715" runat="server" Columns="3" TabIndex="11801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left" colspan="3">
                                            <asp:Label ID="lblNivelE3" runat="server" CssClass="lblGrisTit" Text="Nivel E" Width="150px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: right" colspan="2">
                                            <asp:TextBox ID="txtV716" runat="server" Columns="3" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="padding-bottom: 20px; text-align:justify">
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Label ID="lblAulas" runat="server" CssClass="lblGrisTit" Font-Size="16px" Text="IV. AULAS"
                                                Width="150px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 50px;
                                            text-align: left">
                                            <asp:Label ID="lblInstrucciones1" runat="server" CssClass="lblRojo" Text="1. Registre el n�mero de aulas por grado, seg�n su tipo."
                                                Width="450px"></asp:Label>
                                            <asp:Label ID="lblNota1" runat="server" CssClass="lblRojo" Text="Notas:" Width="450px"></asp:Label>
                                            <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" Text="a) El reporte de aulas debe ser por turno."
                                                Width="450px"></asp:Label>
                                            <asp:Label ID="lblNota3" runat="server" CssClass="lblRojo" Text="b) Si un aula se utiliza para impartir clases a m�s de un grado, an�tela en el rubro correspondiente"
                                                Width="450px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lblAulas1" runat="server" CssClass="lblRojo" Text="AULAS" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom;
                                            text-align: center" colspan="2">
                                            <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Text="1�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom;
                                            text-align: center" colspan="2">
                                            <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Text="2�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom;
                                            text-align: center" colspan="2">
                                            <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Text="3�" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lblMasUnGdo" runat="server" CssClass="lblGrisTit" Text="M�S DE UN GRADO"
                                                Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: bottom; width: 50px;
                                            text-align: center">
                                            <asp:Label ID="lblTotal1" runat="server" CssClass="lblRojo" Text="TOTAL" Width="50px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:Label ID="lblExistentes" runat="server" CssClass="lblGrisTit" Text="EXISTENTES" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                            <asp:TextBox ID="txtV717" runat="server" Columns="2" TabIndex="12001" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:Label ID="lblEnUso" runat="server" CssClass="lblGrisTit" Text="EN USO" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV718" runat="server" Columns="2" TabIndex="20101" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV719" runat="server" Columns="2" TabIndex="20102" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV720" runat="server" Columns="2" TabIndex="20103" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV721" runat="server" Columns="2" TabIndex="20104" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                            <asp:TextBox ID="txtV722" runat="server" Columns="2" TabIndex="20105" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 50px;
                                            padding-top: 10px; text-align: left">
                                            <asp:Label ID="lblNota4" runat="server" CssClass="lblRojo" Text="De las reportadas en uso, indique el n�mero de las adaptadas."
                                                Width="450px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="" rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:Label ID="lblAdaptadas" runat="server" CssClass="lblGrisTit" Text="ADAPTADAS" Width="50px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV723" runat="server" Columns="2" TabIndex="20201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV724" runat="server" Columns="2" TabIndex="20202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV725" runat="server" Columns="2" TabIndex="20203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: left">
                                            <asp:TextBox ID="txtV726" runat="server" Columns="2" TabIndex="20204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 50px; text-align: right">
                                            <asp:TextBox ID="txtV727" runat="server" Columns="2" TabIndex="20205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Personal_911_5',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_5',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;
                </td>
                <td ><span  onclick="openPage('Gasto_911_5',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Gasto_911_5',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado"></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
        </center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />               
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>

           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
