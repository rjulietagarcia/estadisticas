<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="911.5(Desglose)" AutoEventWireup="true" CodeBehind="AGD_911_5.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio._911_5.AGD_911_5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
  
    <%--Agregado--%>
    <script language="javascript" type="text/javascript"  src="../../../tema/js/ArrowsHandler.js"></script>
    <script type="text/javascript">
        MaxCol = 13;
        MaxRow = 23;
        TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
    </script>
    <%--Agregado--%>
    
    <div id="logo"></div>
    <div style="min-width:1200px; height:65px;">
    <div id="header">
    <table style="width:100%">
        <tr><td><span>EDUCACI�N SECUNDARIA</span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCiclo" runat="server"></asp:Label></span></td>
        </tr>
        <tr><td><span><asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label></span></td>
        </tr>
    </table></div>
    </div>
    
    <div id="menu" style="min-width:1200px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_911_5',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
            <li onclick="openPage('AG1_911_5',true)"><a href="#" title=""><span>1�</span></a></li>
            <li onclick="openPage('AG2_911_5',true)"><a href="#" title=""><span>2�</span></a></li>
            <li onclick="openPage('AG3_911_5',true)"><a href="#" title=""  ><span>3�</span></a></li>
            <li onclick="openPage('AGT_911_5',true)"><a href="#" title="" ><span>TOTAL</span></a></li>
            <li onclick="openPage('AGD_911_5',true)"><a href="#" title="" class="activo"><span>DESGLOSE</span></a></li>
            <li onclick="openPage('Personal_911_5',false)"><a href="#" title=""><span>PERSONAL</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>CARRERA MAGISTERIAL Y AULAS</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>GASTO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>ANEXO</span></a></li>
            <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
    <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div>
       <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
   <center> 
            

  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


        <table style="width: 100%; text-align: center">
            <tr>
                <td style="width: 100%; text-align: center">
                    <table id="TABLE1" style="text-align: center">
                        <tr>
                            <td colspan="5" rowspan="1" style="vertical-align: top; text-align: center; width: 1013px;">
                                <table style="width: 455px; text-align: center">
                                    <tr>
                                        <td style="padding-bottom: 10px; width: 150px; text-align: left">
                                        </td>
                                        <td style="padding-bottom: 10px; width: 150px; text-align: left">
                                        </td>
                                        <td style="padding-bottom: 10px; width: 67px; text-align: center">
                                            <asp:Label ID="lblHombres2" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td style="padding-bottom: 10px; width: 67px; text-align: center">
                                            <asp:Label ID="lblMujeres2" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td style="padding-bottom: 10px; width: 65px; text-align: center">
                                            <asp:Label ID="lblTotal2" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td colspan="1" style="padding-right: 20px; padding-left: 20px; padding-bottom: 10px;
                                            width: 150px; text-align: left">
                                        </td>
                                        <td colspan="8" style="text-align: left; vertical-align: top;" rowspan="2">
                                            <asp:Label ID="lblAviso" runat="server" CssClass="lblRojo" Text="DE LA PREGUNTA 7 A LA 11 CONSIDERE LA INFORMACI�N REPORTADA EN EL FIN DE CURSOS ANTERIOR." Width="550px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="padding-bottom: 10px; width: 150px; text-align: left" colspan="2">
                                            <asp:Label ID="lbl2" runat="server" CssClass="lblRojo" Text="2. Escriba, por sexo, la cantidad de alumnos ind�genas."
                                                Width="300px"></asp:Label></td>
                                        <td style="padding-bottom: 10px; width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV166" runat="server" Columns="3" TabIndex="10101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td style="padding-bottom: 10px; width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV167" runat="server" Columns="3" TabIndex="10102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td style="padding-bottom: 10px; width: 65px; text-align: center">
                                            <asp:TextBox ID="txtV168" runat="server" Columns="4" TabIndex="10103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td style="padding-bottom: 10px; width: 150px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: middle; width: 150px;" colspan="5">
                                            </td>
                                        <td style="width: 150px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 150px; text-align: left">
                                            </td>
                                        <td style="width: 150px; text-align: center">
                                            </td>
                                        <td style="width: 67px; text-align: center; padding-bottom: 10px;">
                                            <asp:Label ID="lblHombres3" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center; padding-bottom: 10px;">
                                            <asp:Label ID="lblMujeres3" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td style="padding-bottom: 10px; width: 65px; text-align: center">
                                            <asp:Label ID="lblTotal3" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:Label ID="lblHombres7" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:Label ID="lblMujeres7" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:Label ID="lblTotal7" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl3" runat="server" CssClass="lblRojo" Text="3. Escriba la cantidad de alumnos que son atendidos por la Unidad de Servicios de Apoyo a la Educaci�n Regular (USAER), desglos�ndola por sexo."
                                                Width="300px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV169" runat="server" Columns="3" TabIndex="10201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV170" runat="server" Columns="3" TabIndex="10202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV171" runat="server" Columns="4" TabIndex="10203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left">
                                        </td>
                                        <td colspan="5" rowspan="1" style="vertical-align: top; text-align: left">
                                            <asp:Label ID="lbl7" runat="server" CssClass="lblRojo" Text="7. Escriba la cantidad de alumnos egresados del ciclo escolar anterior (incluya los regularizados hasta el 30 de septiembre de este a�o), desglos�ndola por sexo."
                                                Width="325px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV229" runat="server" Columns="4" TabIndex="12201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV230" runat="server" Columns="4" TabIndex="12202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV231" runat="server" Columns="4" TabIndex="12203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: left; width: 150px;" colspan="5">
                                            &nbsp;</td>
                                        <td style="width: 150px; text-align: left">
                                            </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                        <td style="width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: top; text-align: left; padding-bottom: 10px; width: 150px;" colspan="5">
                                            <asp:Label ID="lbl4" runat="server" CssClass="lblRojo" Text="4. Escriba la cantidad de alumnos con discapacidad, aptitudes sobresalientes u otras condiciones, desglos�ndolos por sexo."
                                                Width="495px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            </td>
                                        <td style="width: 67px; text-align: left" colspan="8">
                                            <asp:Label ID="lbl8" runat="server" CssClass="lblRojo" Text="8. Escriba la cantidad de alumnos por grado que reprobaron de 1 a 5 materias del ciclo escolar anterior (consulte la informaci�n reportada en el fin de cursos del ciclo escolar anterior), desglos�ndola por sexo."
                                                Width="550px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="2" style="vertical-align: middle; text-align: center">
                                            <asp:Label ID="lblSituacion4" runat="server" CssClass="lblRojo" Text="SITUACI�N DEL ALUMNO"
                                                Width="250px"></asp:Label></td>
                                        <td colspan="3" rowspan="1" style="width: 67px; text-align: center">
                                            <asp:Label ID="lblAlumnos4" runat="server" CssClass="lblRojo" Text="ALUMNOS" Width="201px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left; width: 67px;">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left; width: 67px;">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left; width: 67px;">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left; width: 67px;">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left; width: 67px;">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left; width: 67px;">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: left; width: 67px;">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td style="width: 65px; text-align: center">
                                            <asp:Label ID="lblTotal4" runat="server" CssClass="lblGrisTit" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td style="width: 67px; text-align: center">
                                            </td>
                                        <td style="text-align: center; vertical-align: top;" colspan="2">
                                            <asp:Label ID="lblPrimero8" runat="server" CssClass="lblRojo" Text="PRIMERO" Width="67px"></asp:Label></td>
                                        <td style="text-align: center; vertical-align: top;" colspan="2">
                                            <asp:Label ID="lblSegundo8" runat="server" CssClass="lblRojo" Text="SEGUNDO" Width="67px"></asp:Label></td>
                                        <td style="text-align: center; vertical-align: top;" colspan="2">
                                            <asp:Label ID="lblTercero8" runat="server" CssClass="lblRojo" Text="TERCERO" Width="67px"></asp:Label></td>
                                        <td style="text-align: center; vertical-align: top;" colspan="2">
                                            <asp:Label ID="lblTotal8" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: middle; text-align: left; margin-bottom: 20px; width: 150px;" colspan="2">
                                            <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Text="CEGUERA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: middle; text-align: center; margin-bottom: 20px; width: 67px;">
                                            <asp:TextBox ID="txtV172" runat="server" Columns="4" TabIndex="10301" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: middle; text-align: center; margin-bottom: 20px; width: 67px;">
                                            <asp:TextBox ID="txtV173" runat="server" Columns="4" TabIndex="10302" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: middle; text-align: center; margin-bottom: 20px; width: 65px;">
                                            <asp:TextBox ID="txtV174" runat="server" Columns="4" TabIndex="10303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres81" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres81" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres82" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres82" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres83" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres83" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left; margin-bottom: 20px;">
                                            <asp:Label ID="lblDiscVisual" runat="server" CssClass="lblGrisTit" Text="BAJA VISI�N"
                                                Width="250px"></asp:Label></td>
                                        <td colspan="" rowspan="1" style="vertical-align: middle; width: 67px; text-align: center; margin-bottom: 20px;">
                                            <asp:TextBox ID="txtV175" runat="server" Columns="4" TabIndex="10401" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: middle; text-align: center; margin-bottom: 20px; width: 67px;">
                                            <asp:TextBox ID="txtV176" runat="server" Columns="4" TabIndex="10402" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: middle; text-align: center; margin-bottom: 20px; width: 65px;">
                                            <asp:TextBox ID="txtV177" runat="server" Columns="4" TabIndex="10403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                            </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV232" runat="server" Columns="4" TabIndex="20101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV233" runat="server" Columns="4" TabIndex="20102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV234" runat="server" Columns="4" TabIndex="20103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV235" runat="server" Columns="4" TabIndex="20104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV236" runat="server" Columns="4" TabIndex="20105" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV237" runat="server" Columns="4" TabIndex="20106" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV238" runat="server" Columns="4" TabIndex="20107" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                            <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Text="SORDERA" Width="250px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV178" runat="server" Columns="4" TabIndex="10501" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV179" runat="server" Columns="4" TabIndex="10502" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 65px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV180" runat="server" Columns="4" TabIndex="10503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                            <asp:Label ID="lblDiscAuditiva" runat="server" CssClass="lblGrisTit" Text="HIPOACUSIA"
                                                Width="250px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV181" runat="server" Columns="4" TabIndex="10601" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV182" runat="server" Columns="4" TabIndex="10602" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 65px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV183" runat="server" Columns="4" TabIndex="10603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td colspan="8" rowspan="3" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                            <asp:Label ID="lbl9" runat="server" CssClass="lblRojo" Text="9. De los alumnos reportados en el punto anterior, registre el n�mero de alumnos por grado que se regularizaron (aprobaron mediante ex�menes extraordinarios todas las materias que adeudaban) hasta el 30 de septiembre de este a�o, desglos�ndolo por sexo."
                                                Width="500px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                            <asp:Label ID="lblDiscMotriz" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD MOTRIZ"
                                                Width="250px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV184" runat="server" Columns="4" TabIndex="10701" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV185" runat="server" Columns="4" TabIndex="10702" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 65px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV186" runat="server" Columns="4" TabIndex="10703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                            <asp:Label ID="lblDiscIntelectual" runat="server" CssClass="lblGrisTit" Text="DISCAPACIDAD INTELECTUAL"
                                                Width="250px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV187" runat="server" Columns="4" TabIndex="10801" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV188" runat="server" Columns="4" TabIndex="10802" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 65px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV189" runat="server" Columns="4" TabIndex="10803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                            <asp:Label ID="lblOtraDisc" runat="server" CssClass="lblGrisTit" Text="OTRAS CONDICIONES" Width="250px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV190" runat="server" Columns="4" TabIndex="10901" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV191" runat="server" Columns="4" TabIndex="10902" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 65px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV192" runat="server" Columns="4" TabIndex="10903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: left">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left; margin-bottom: 20px;">
                                            <asp:Label ID="lblCapSobresalientes" runat="server" CssClass="lblGrisTit" Text="APTITUDES SOBRESALIENTES"
                                                Width="265px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: middle; text-align: center; margin-bottom: 20px; width: 67px;">
                                            <asp:TextBox ID="txtV193" runat="server" Columns="4" TabIndex="11001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: middle; text-align: center; margin-bottom: 20px; width: 67px;">
                                            <asp:TextBox ID="txtV194" runat="server" Columns="4" TabIndex="11002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: middle; text-align: center; margin-bottom: 20px; width: 65px;">
                                            <asp:TextBox ID="txtV195" runat="server" Columns="4" TabIndex="11003" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:Label ID="lblPrimero9" runat="server" CssClass="lblRojo" Text="PRIMERO" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:Label ID="lblSegundo9" runat="server" CssClass="lblRojo" Text="SEGUNDO" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:Label ID="lblTercero9" runat="server" CssClass="lblRojo" Text="TERCERO" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:Label ID="lblTotal9" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                            <asp:Label ID="lblTotal42" runat="server" CssClass="lblRojo" Text="TOTAL" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV196" runat="server" Columns="4" TabIndex="11101" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV197" runat="server" Columns="4" TabIndex="11102" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td rowspan="1" style="margin-bottom: 20px; vertical-align: middle; width: 65px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV198" runat="server" Columns="4" TabIndex="11103" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres91" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres91" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres92" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres92" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres93" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres93" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV239" runat="server" Columns="4" TabIndex="20201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV240" runat="server" Columns="4" TabIndex="20202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV241" runat="server" Columns="4" TabIndex="20203" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV242" runat="server" Columns="4" TabIndex="20204" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV243" runat="server" Columns="4" TabIndex="20205" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV244" runat="server" Columns="4" TabIndex="20206" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV245" runat="server" Columns="4" TabIndex="20207" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres5" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres5" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center; width: 65px;">
                                            <asp:Label ID="lblTotal5" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left; height: 71px;">
                                            <asp:Label ID="lbl5" runat="server" CssClass="lblRojo" Text="5. Escriba la cantidad de alumnos con Necesidades Educativas Especiales (NEE), independientemente de que presenten o no alguna discapacidad, desglos�ndola por sexo."
                                                Width="300px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center; height: 71px;">
                                            <asp:TextBox ID="txtV199" runat="server" Columns="4" TabIndex="11201" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px; height: 71px;">
                                            <asp:TextBox ID="txtV200" runat="server" Columns="4" TabIndex="11202" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px; height: 71px;">
                                            <asp:TextBox ID="txtV201" runat="server" Columns="4" TabIndex="11203" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; height: 71px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: left; width: 67px; height: 71px;" colspan="8">
                                            <asp:Label ID="lbl10" runat="server" CssClass="lblRojo" Text="10. Registre la cantidad de alumnos de 3er. grado reportados en el punto anterior, desglos�ndola por sexo y edad."
                                                Width="550px"></asp:Label>
                                            <asp:Label ID="lblNota10" runat="server" CssClass="lblRojo" Text="Nota: La edad de los alumnos regularizados deber� ser la que se report� para ellos al inicio de cursos del ciclo escolar anterior (al 1� de septiembre)."
                                                Width="550px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left; padding-bottom: 10px;">
                                            <asp:Label ID="lbl6" runat="server" CssClass="lblRojo" Text="6. Escriba el n�mero de alumnos de nacionalidad extranjera, desglos�ndolo por sexo."
                                                Width="495px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lbl13a�os10" runat="server" CssClass="lblRojo" Text="13 a�os" Width="67px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lbl14a�os10" runat="server" CssClass="lblRojo" Text="14 a�os" Width="67px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lbl15a�os10" runat="server" CssClass="lblRojo" Text="15 a�os" Width="67px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lbl16a�os10" runat="server" CssClass="lblRojo" Text="16 a�os" Width="67px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lbl17a�os10" runat="server" CssClass="lblRojo" Text="17 a�os" Width="67px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lbl18a�os10" runat="server" CssClass="lblRojo" Text="18 a�os y m�s" Width="67px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lblTotal10" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: left">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:Label ID="lblHombres6" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 67px;
                                            text-align: center">
                                            <asp:Label ID="lblMujeres6" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 65px;
                                            text-align: center">
                                            <asp:Label ID="lblTotal6" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: middle; width: 150px;
                                            text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px;
                                            text-align: center">
                                            <asp:Label ID="lblHombres10" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV246" runat="server" Columns="4" TabIndex="20301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV247" runat="server" Columns="4" TabIndex="20302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV248" runat="server" Columns="4" TabIndex="20303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV249" runat="server" Columns="4" TabIndex="20304" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV250" runat="server" Columns="4" TabIndex="20305" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px;
                                            text-align: center">
                                            <asp:TextBox ID="txtV251" runat="server" Columns="4" TabIndex="20306" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV252" runat="server" Columns="4" TabIndex="20307" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblEEUU" runat="server" CssClass="lblGrisTit" Text="ESTADOS UNIDOS"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV202" runat="server" Columns="4" TabIndex="11301" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV203" runat="server" Columns="4" TabIndex="11302" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV204" runat="server" Columns="4" TabIndex="11303" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres10" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV253" runat="server" Columns="4" TabIndex="20401" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV254" runat="server" Columns="4" TabIndex="20402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV255" runat="server" Columns="4" TabIndex="20403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV256" runat="server" Columns="4" TabIndex="20404" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV257" runat="server" Columns="4" TabIndex="20405" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV258" runat="server" Columns="4" TabIndex="20406" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV259" runat="server" Columns="4" TabIndex="20407" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblCanada" runat="server" CssClass="lblGrisTit" Text="CANAD�" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV205" runat="server" Columns="4" TabIndex="11401" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV206" runat="server" Columns="4" TabIndex="11402" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV207" runat="server" Columns="4" TabIndex="11403" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblCA" runat="server" CssClass="lblGrisTit" Text="CENTROAM�RICA Y EL CARIBE"
                                                Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV208" runat="server" Columns="4" TabIndex="11501" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV209" runat="server" Columns="4" TabIndex="11502" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV210" runat="server" Columns="4" TabIndex="11503" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="2" style="vertical-align: top; text-align: left; width: 67px;" colspan="8">
                                            <asp:Label ID="lbl11" runat="server" CssClass="lblRojo" Text="11. De los alumnos reportados en la pregunta 8, escriba la cantidad de ellos que est�n inscritos en el presente ciclo escolar y contin�an como irregulares (adeudan asignaturas), seg�n sexo y el grado que cursan actualmente."
                                                Width="550px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblSA" runat="server" CssClass="lblGrisTit" Text="SUDAM�RICA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV211" runat="server" Columns="4" TabIndex="11601" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV212" runat="server" Columns="4" TabIndex="11602" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV213" runat="server" Columns="4" TabIndex="11603" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblAfrica" runat="server" CssClass="lblGrisTit" Text="�FRICA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV214" runat="server" Columns="4" TabIndex="11701" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV215" runat="server" Columns="4" TabIndex="11702" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV216" runat="server" Columns="4" TabIndex="11703" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblAsia" runat="server" CssClass="lblGrisTit" Text="ASIA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV217" runat="server" Columns="4" TabIndex="11801" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV218" runat="server" Columns="4" TabIndex="11802" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV219" runat="server" Columns="4" TabIndex="11803" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:Label ID="lblSegundo11" runat="server" CssClass="lblRojo" Text="SEGUNDO" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:Label ID="lblTercero11" runat="server" CssClass="lblRojo" Text="TERCERO" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:Label ID="lblTotal11" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblEuropa" runat="server" CssClass="lblGrisTit" Text="EUROPA" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV220" runat="server" Columns="4" TabIndex="11901" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV221" runat="server" Columns="4" TabIndex="11902" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV222" runat="server" Columns="4" TabIndex="11903" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres111" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres111" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblHombres112" runat="server" CssClass="lblGrisTit" Text="HOMBRES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:Label ID="lblMujeres112" runat="server" CssClass="lblGrisTit" Text="MUJERES" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblOceania" runat="server" CssClass="lblGrisTit" Text="OCEAN�A" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV223" runat="server" Columns="4" TabIndex="12001" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV224" runat="server" Columns="4" TabIndex="12002" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV225" runat="server" Columns="4" TabIndex="12003" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV260" runat="server" Columns="4" TabIndex="30101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV261" runat="server" Columns="4" TabIndex="30102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV262" runat="server" Columns="4" TabIndex="30103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV263" runat="server" Columns="4" TabIndex="30104" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center" colspan="2">
                                            <asp:TextBox ID="txtV264" runat="server" Columns="4" TabIndex="30105" CssClass="lblNegro" MaxLength="4"></asp:TextBox></td>
                                        <td colspan="1" rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="vertical-align: middle; width: 150px; text-align: left">
                                            <asp:Label ID="lblTotal62" runat="server" CssClass="lblRojo" Text="TOTAL" Width="250px"></asp:Label></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV226" runat="server" Columns="4" TabIndex="12101" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                            <asp:TextBox ID="txtV227" runat="server" Columns="4" TabIndex="12102" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 65px;">
                                            <asp:TextBox ID="txtV228" runat="server" Columns="4" TabIndex="12103" CssClass="lblNegro" MaxLength="3"></asp:TextBox></td>
                                        <td rowspan="1" style="vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; text-align: center; width: 67px;">
                                        </td>
                                        <td rowspan="1" style="vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: middle;
                                            width: 150px; text-align: center">
                                            </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center; width: 67px;">
                                            </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center; width: 67px;">
                                            </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center; width: 65px;">
                                            </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: left; width: 67px;" colspan="8">
                                            <asp:Label ID="lblAviso2" runat="server" CssClass="lblRojo" Text="La siguiente pregunta �nicamente deber� ser contestada por TELESECUNDARIAS."
                                                Width="550px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: middle;
                                            width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 65px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td colspan="8" rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px;
                                            text-align: left">
                                            <asp:Label ID="lbl12" runat="server" CssClass="lblRojo" Text="12. Escriba, por grado, el n�mero de directivos con grupo y docentes."
                                                Width="550px"></asp:Label>
                                            <asp:Label ID="lblNota12" runat="server" CssClass="lblRojo" Text="IMPORTANTE: Si un profesor atiende m�s de un grado, an�telo en el rubro correspondiente; el total debe coincidir con la suma de directivo con grupo m�s personal docentede la pregunta 1 de la secci�n PERSONAL POR FUNCI�N."
                                                Width="550px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: middle;
                                            width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 65px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: middle;
                                            width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 65px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lblPrimero12" runat="server" CssClass="lblGrisTit" Text="PRIMERO" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lblSegundo12" runat="server" CssClass="lblGrisTit" Text="SEGUNDO" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lblTercero12" runat="server" CssClass="lblGrisTit" Text="TERCERO" Width="67px"></asp:Label></td>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center">
                                            <asp:Label ID="lblMasUnGdo12" runat="server" CssClass="lblGrisTit" Text="M�S DE UN GRADO"
                                                Width="110px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                            <asp:Label ID="lblTotal12" runat="server" CssClass="lblRojo" Text="TOTAL" Width="67px"></asp:Label></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: middle;
                                            width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 65px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV265" runat="server" Columns="2" TabIndex="30201" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV266" runat="server" Columns="2" TabIndex="30202" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV267" runat="server" Columns="2" TabIndex="30203" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: top; text-align: center">
                                            <asp:TextBox ID="txtV268" runat="server" Columns="2" TabIndex="30204" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                            <asp:TextBox ID="txtV269" runat="server" Columns="2" TabIndex="30205" CssClass="lblNegro" MaxLength="2"></asp:TextBox></td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="padding-bottom: 10px; vertical-align: middle;
                                            width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 65px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 150px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                        <td rowspan="1" style="padding-bottom: 10px; vertical-align: top; width: 67px; text-align: center">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('AGT_911_5',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('AGT_911_5',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">&nbsp;</td>
                <td ><span  onclick="openPage('Personal_911_5',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Personal_911_5',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>


           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />                 
                 <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		        Disparador(<%=hidDisparador.Value %>);
          GetTabIndexes();
        </script> 
</asp:Content>
