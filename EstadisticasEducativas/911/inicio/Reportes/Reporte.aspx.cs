using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsEstadisticasEducativas;
using EstadisticasEducativas._911.fin.Reportes;

using System.Data.SqlClient;
using Mx.Gob.Nl.Educacion;
using SEroot.WsSESeguridad;

namespace EstadisticasEducativas._911.inicio.Reportes
{
    public partial class Reporte : System.Web.UI.Page
    {
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);

                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
                int PDF = 0;
                if (Request.Params["OCE"] != null)
                {
                    int.TryParse(Request.Params["OCE"], out PDF);
                }
                //  1->Comprobante     2->Vacio            3->Lleno              4-> Desoficializar
                byte[] reporte = null;
                switch (PDF)
                {
                    case 1:
                        reporte = wsEstadisticas.ImprimeComprobanteCaptura911(controlDP);
                        break;
                    case 2:
                        reporte = wsEstadisticas.ImprimeCuestionario911(controlDP, false);
                        // ImprimirQuiestionario(controlDP, false);
                        break;
                    case 3:
                        reporte = wsEstadisticas.ImprimeCuestionario911(controlDP, true);
                        //ImprimirQuiestionario(controlDP, true);
                        break;
                    case 4:
                        Label1.Visible = false;
                        controlDP.Estatus = 0;
                        wsEstadisticas.Oficializar_Cuestionario(controlDP, int.Parse(User.Identity.Name.Split('|')[0]));
                        Response.Write("La estad�stica fue desoficializada");
                        break;
                    default:
                        Response.Write("no se localiz� tipo de documento a imprimir");
                        break;
                }
                if (reporte != null)
                    ImprimeReporte(reporte);
                else
                    throw new Exception("Ocurri� un error al obtener el Reporte");

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message + " \\n " + ex.StackTrace);
            }
        }


       

        protected void DoDeleteFile()
        {
            if (Session["lGUID"] != null)
            {
                string lGUID = Session["lGUID"].ToString();
                if (System.IO.File.Exists(Server.MapPath("~/PDFReports/EE" + lGUID + ".pdf")))
                    System.IO.File.Delete(Server.MapPath("~/PDFReports/EE" + lGUID + ".pdf"));
            }
        }

        private void ImprimeReporte(byte[] aArchivo)
        {
            DoDeleteFile();
            System.Guid lGUID = Guid.NewGuid();
            System.IO.FileStream lFileStream = new System.IO.FileStream(Server.MapPath("~/PDFReports/EE" + lGUID.ToString() + ".pdf"), System.IO.FileMode.CreateNew);
            try
            {
                lFileStream.Write(aArchivo, 0, aArchivo.Length - 1);
            }
            finally
            {
                lFileStream.Flush();
                lFileStream.Close();
            }
            Session["lGUID"] = lGUID.ToString();
            Response.Redirect("~/PDFReports/EE" + lGUID.ToString() + ".pdf");
        }

       




        #region Old para borrarse
        /*

        
        protected UsuarioSeDP usr;
        protected CcntFiltrosQryDP cctSeleccionado;

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                #region Carga Parametros Y Hidden's
                usr = SeguridadSE.GetUsuario(HttpContext.Current);

                cctSeleccionado = usr.Selecciones.CentroTrabajoSeleccionado;


                int id_CT = 0;
                int id_Inm = 0;
                string cct = "";
                int tur = 0;

                id_CT = cctSeleccionado.CentrotrabajoId;
                id_Inm = cctSeleccionado.InmuebleId;
                cct = cctSeleccionado.Clavecct;
                tur = cctSeleccionado.TurnoId;

                ControlDP controlDP = Class911.GetControlSeleccionado(HttpContext.Current);

                hidIdCtrl.Value = controlDP.ID_Control.ToString();
                #endregion

                ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
                int PDF = 0;
                if (Request.Params["OCE"] != null)
                {
                    int.TryParse(Request.Params["OCE"], out PDF);
                }
                //  1->Comprobante     2->Vacio            3->Lleno              4-> Desoficializar
                switch (PDF)
                {
                    case 1:
                        ImprimirConstanciaOficializacion(controlDP);
                        break;
                    case 2:
                        ImprimirQuiestionario(controlDP, false);
                        break;
                    case 3:
                        ImprimirQuiestionario(controlDP, true);
                        break;
                    case 4:
                        Label1.Visible = false;
                        controlDP.Estatus = 0;
                        wsEstadisticas.Oficializar_Cuestionario(controlDP, int.Parse(User.Identity.Name.Split('|')[0]));
                        Response.Write("La estad�stica fue desoficializada");
                        break;
                    default:
                        Response.Write("no se localiz� tipo de documento a imprimir");
                        break;
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message + " \\n " + ex.StackTrace);
            }
        }
        private void ImprimirConstanciaOficializacion(ControlDP controlDP)
        {
            switch (controlDP.ID_Cuestionario)
            {
                //case 1:
                //    pdf_911_2_Oficializar(controlDP);
                //    break;
                //case 2:
                //    pdf_911_4_Oficializar(controlDP);
                //    break;
                //case 3:
                //    pdf_911_6_Oficializar(controlDP);
                //    break;
                //case 4:
                //    break;
                //case 5:
                //    break;
                //case 6:
                //    break;
                //case 7:
                //    pdf_ECC_21_Oficializar(controlDP);
                //    break;
                //case 8:
                //    pdf_ECC_22_Oficializar(controlDP);
                //    break;
                case 15:
                    pdf_911_EI_1_Oficializar(controlDP);
                    break;
                case 16:
                    pdf_911_EI_NE_1_Oficializar(controlDP);
                    break;
                case 19:
                    pdf_911_1_Oficializar(controlDP);
                    break;
                case 22:
                    pdf_911_3_Oficializar(controlDP);
                    break;
                case 25:
                    pdf_911_5_Oficializar(controlDP);
                    break;
                case 26:
                    pdf_911_7G_Oficializar(controlDP);
                    break;
                case 27:
                    pdf_911_7T_Oficializar(controlDP);
                    break;
                case 28:
                    pdf_911_7P_Oficializar(controlDP);
                    break;
                case 17:
                    pdf_911_USAER_1_Oficializar(controlDP);
                    break;
                case 18:
                    pdf_911_CAM_1_Oficializar(controlDP);
                    break;
                case 20:
                    pdf_911_ECC11_Oficializar(controlDP);
                    break;
                case 23:
                    pdf_911_ECC12_Oficializar(controlDP);
                    break;
            }

        }
        private void ImprimirQuiestionario(ControlDP controlDP, bool Lleno)
        {
            switch (controlDP.ID_Cuestionario)
            {
                //case 1:
                //    pdf_911_2(controlDP, Lleno);
                //    break;
                //case 2:
                //    pdf_911_4(controlDP, Lleno);
                //    break;
                //case 3:
                //    pdf_911_6(controlDP, Lleno);
                //    break;
                //case 4:
                //    break;
                //case 5:
                //    break;
                //case 6:
                //    break;
                //case 7:
                //    pdf_ECC_21(controlDP, Lleno);
                //    break;
                //case 8:
                //    pdf_ECC_22(controlDP, Lleno);
                //    break;
                case 15:
                    pdf_911_EI_1(controlDP, Lleno);
                    break;
                case 16:
                    pdf_911_EI_NE_1(controlDP, Lleno);
                    break;
                case 19:
                    pdf_911_1(controlDP, Lleno);
                    break;
                case 22:
                    pdf_911_3(controlDP, Lleno);
                    break;
                case 25:
                    pdf_911_5(controlDP, Lleno);
                    break;
                case 26:
                    pdf_911_7G(controlDP, Lleno);
                    break;
                case 27:
                    pdf_911_7T(controlDP, Lleno);
                    break;
                case 28:
                    pdf_911_7P(controlDP, Lleno);
                    break;
                case 17:
                    pdf_911_USAER_1(controlDP, Lleno);
                    break;
                case 18:
                    pdf_911_CAM_1(controlDP, Lleno);
                    break;
                case 20:
                    pdf_911_ECC11(controlDP, Lleno);
                    break;
                case 23:
                    pdf_911_ECC12(controlDP, Lleno);
                    break;
            }
        }

        #region PDF 911.1
        private void pdf_911_1(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_1 Ds911_1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");//
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_1.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO

            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, "); 
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, "); 
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, "); 
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, "); 
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, "); 
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_1.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_PREESCOLAR_G_Inicio
            sqlQuery.Append(" SELECT ID_Control ,ID_CicloEscolar, ");
            sqlQuery.Append(" v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31,  ");
            sqlQuery.Append(" v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59,  ");
            sqlQuery.Append(" v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89,  ");
            sqlQuery.Append(" v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101 ,v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114,  ");
            sqlQuery.Append(" v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125 ,v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138,  ");
            sqlQuery.Append(" v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149 ,v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162,  ");
            sqlQuery.Append(" v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173 ,v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187,  ");
            sqlQuery.Append(" v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197 ,v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212,  ");
            sqlQuery.Append(" v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221 ,v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237,  ");
            sqlQuery.Append(" v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245 ,v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262,  ");
            sqlQuery.Append(" v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269 ,v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287,  ");
            sqlQuery.Append(" v288 ,v289 ,v290 ,v291 ,v292 ,v293 ,v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312,  ");
            sqlQuery.Append(" v313 ,v314 ,v315 ,v316 ,v317 ,v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337,  ");
            sqlQuery.Append(" v338 ,v339 ,v340 ,v341 ,v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362,  ");
            sqlQuery.Append(" v363 ,v364 ,v365 ,v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388,  ");
            sqlQuery.Append(" v389 ,v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412,  ");
            sqlQuery.Append(" v413 ,v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429 ,v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436,  ");
            sqlQuery.Append(" v437 ,v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447 ,v448 ,v449 ,v450 ,v451 ,v452 ,v453 ,v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460,  ");
            sqlQuery.Append(" v461 ,v462 ,v463 ,v464 ,v465 ,v466 ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477 ,v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484,  ");
            sqlQuery.Append(" v485 ,v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501 ,v502 ,v503 ,v504 ,v505 ,v506 ,v507 ,v508,  ");
            sqlQuery.Append(" v509 ,v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523 ,v524 ,v525 ,v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532,  ");
            sqlQuery.Append(" v533 ,v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542 ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549 ,v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556,  ");
            sqlQuery.Append(" Descripcion  FROM sep_911.dbo.vw_Preesc_G_Inicio  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_1.vw_Preesc_G_Inicio);
            #endregion

            classRpts.LoadReporte("911_1.rpt");
            classRpts.DsDatos = Ds911_1;


            byte[] PDF_byte = classRpts.GenerarEnMemoria(1,0);

            ImprimeReporte(PDF_byte);
        }
 
        private void pdf_911_1_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_1 Ds911_1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_1.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_PREESCOLAR_G_F
            sqlQuery.Append(" SELECT ID_Control ,ID_CicloEscolar, ");
            sqlQuery.Append(" v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31,  ");
            sqlQuery.Append(" v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59,  ");
            sqlQuery.Append(" v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89,  ");
            sqlQuery.Append(" v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101 ,v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114,  ");
            sqlQuery.Append(" v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125 ,v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138,  ");
            sqlQuery.Append(" v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149 ,v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162,  ");
            sqlQuery.Append(" v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173 ,v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187,  ");
            sqlQuery.Append(" v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197 ,v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212,  ");
            sqlQuery.Append(" v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221 ,v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237,  ");
            sqlQuery.Append(" v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245 ,v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262,  ");
            sqlQuery.Append(" v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269 ,v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287,  ");
            sqlQuery.Append(" v288 ,v289 ,v290 ,v291 ,v292 ,v293 ,v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312,  ");
            sqlQuery.Append(" v313 ,v314 ,v315 ,v316 ,v317 ,v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337,  ");
            sqlQuery.Append(" v338 ,v339 ,v340 ,v341 ,v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362,  ");
            sqlQuery.Append(" v363 ,v364 ,v365 ,v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388,  ");
            sqlQuery.Append(" v389 ,v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412,  ");
            sqlQuery.Append(" v413 ,v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429 ,v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436,  ");
            sqlQuery.Append(" v437 ,v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447 ,v448 ,v449 ,v450 ,v451 ,v452 ,v453 ,v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460,  ");
            sqlQuery.Append(" v461 ,v462 ,v463 ,v464 ,v465 ,v466 ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477 ,v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484,  ");
            sqlQuery.Append(" v485 ,v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501 ,v502 ,v503 ,v504 ,v505 ,v506 ,v507 ,v508,  ");
            sqlQuery.Append(" v509 ,v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523 ,v524 ,v525 ,v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532,  ");
            sqlQuery.Append(" v533 ,v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542 ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549 ,v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556,  ");
            sqlQuery.Append(" Descripcion  FROM sep_911.dbo.vw_Preesc_G_Inicio  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_1.vw_Preesc_G_Inicio);
            #endregion

            classRpts.LoadReporte("estadisticaPre.rpt");
            classRpts.DsDatos = Ds911_1;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0,0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF 911.3
        private void pdf_911_3(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_3 Ds911_3 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_3();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_3.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_3.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Primaria_G_Inicio
            sqlQuery.Append("   SELECT ID_Control ,ID_CicloEscolar ,Descripcion ,v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15,  ");
            sqlQuery.Append(" v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44,  ");
            sqlQuery.Append(" v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73,  ");
            sqlQuery.Append(" v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101,  ");
            sqlQuery.Append(" v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125,  ");
            sqlQuery.Append(" v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149,  ");
            sqlQuery.Append(" v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162 ,v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173,  ");
            sqlQuery.Append(" v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197,  ");
            sqlQuery.Append(" v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221,  ");
            sqlQuery.Append(" v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237 ,v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245,  ");
            sqlQuery.Append(" v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269,  ");
            sqlQuery.Append(" v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293,  ");
            sqlQuery.Append(" v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312 ,v313 ,v314 ,v315 ,v316 ,v317,  ");
            sqlQuery.Append(" v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341,  ");
            sqlQuery.Append(" v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365,  ");
            sqlQuery.Append(" v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389,  ");
            sqlQuery.Append(" v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412 ,v413,  ");
            sqlQuery.Append(" v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429 ,v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436 ,v437,  ");
            sqlQuery.Append(" v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447 ,v448 ,v449 ,v450 ,v451 ,v452 ,v453 ,v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460 ,v461,  ");
            sqlQuery.Append(" v462 ,v463 ,v464 ,v465 ,v466 ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477 ,v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484 ,v485,  ");
            sqlQuery.Append(" v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501 ,v502 ,v503 ,v504 ,v505 ,v506 ,v507 ,v508 ,v509,  ");
            sqlQuery.Append(" v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523 ,v524 ,v525 ,v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532 ,v533,  ");
            sqlQuery.Append(" v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542 ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549 ,v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556 ,v557,  ");
            sqlQuery.Append(" v558 ,v559 ,v560 ,v561 ,v562 ,v563 ,v564 ,v565 ,v566 ,v567 ,v568 ,v569 ,v570 ,v571 ,v572 ,v573 ,v574 ,v575 ,v576 ,v577 ,v578 ,v579 ,v580 ,v581,  ");
            sqlQuery.Append(" v582 ,v583 ,v584 ,v585 ,v586 ,v587 ,v588 ,v589 ,v590 ,v591 ,v592 ,v593 ,v594 ,v595 ,v596 ,v597 ,v598 ,v599 ,v600 ,v601 ,v602 ,v603 ,v604 ,v605,  ");
            sqlQuery.Append(" v606 ,v607 ,v608 ,v609 ,v610 ,v611 ,v612 ,v613 ,v614 ,v615 ,v616 ,v617 ,v618 ,v619 ,v620 ,v621 ,v622 ,v623 ,v624 ,v625 ,v626 ,v627 ,v628 ,v629,  ");
            sqlQuery.Append(" v630 ,v631 ,v632 ,v633 ,v634 ,v635 ,v636 ,v637 ,v638 ,v639 ,v640 ,v641 ,v642 ,v643 ,v644 ,v645 ,v646 ,v647 ,v648 ,v649 ,v650 ,v651 ,v652 ,v653,  ");
            sqlQuery.Append(" v654 ,v655 ,v656 ,v657 ,v658 ,v659 ,v660 ,v661 ,v662 ,v663 ,v664 ,v665 ,v666 ,v667 ,v668 ,v669 ,v670 ,v671 ,v672 ,v673 ,v674 ,v675 ,v676 ,v677,  ");
            sqlQuery.Append(" v678 ,v679 ,v680 ,v681 ,v682 ,v683 ,v684 ,v685 ,v686 ,v687 ,v688 ,v689 ,v690 ,v691 ,v692 ,v693 ,v694 ,v695 ,v696 ,v697 ,v698 ,v699 ,v700 ,v701,  ");
            sqlQuery.Append(" v702 ,v703 ,v704 ,v705 ,v706 ,v707 ,v708 ,v709 ,v710 ,v711 ,v712 ,v713 ,v714 ,v715 ,v716 ,v717 ,v718 ,v719 ,v720 ,v721 ,v722 ,v723 ,v724 ,v725,  ");
            sqlQuery.Append(" v726 ,v727 ,v728 ,v729 ,v730 ,v731 ,v732 ,v733 ,v734 ,v735 ,v736 ,v737 ,v738 ,v739 ,v740 ,v741 ,v742 ,v743 ,v744 ,v745 ,v746 ,v747 ,v748 ,v749,  ");
            sqlQuery.Append(" v750 ,v751 ,v752 ,v753 ,v754 ,v755 ,v756 ,v757 ,v758 ,v759 ,v760 ,v761 ,v762 ,v763 ,v764 ,v765 ,v766 ,v767 ,v768 ,v769 ,v770 ,v771 ,v772 ,v773,  ");
            sqlQuery.Append(" v774 ,v775 ,v776 ,v777 ,v778 ,v779 ,v780 ,v781 ,v782 ,v783 ,v784 ,v785 ,v786 ,v787 ,v788 ,v789 ,v790 ,v791 ,v792 ,v793 ,v794 ,v795 ,v796 ,v797,  ");
            sqlQuery.Append(" v798 ,v799 ,v800 ,v801 ,v802 ,v803 ,v804 ,v805 ,v806 ,v807 ,v808 ,v809 ,v810 ,v811 ,v812 ,v813 ,v814 ,v815 ,v816 ,v817 ,v818 ,v819 ,v820 ,v821,  ");
            sqlQuery.Append(" v822 ,v823 ,v824 ,v825 ,v826 ,v827 ,v828 ,v829 ,v830 ,v831 ,v832 ,v833 ,v834 ,v835 ,v836 ,v837 ,v838 ,v839 ,v840 ,v841 ,v842 ,v843 ,v844 ,v845,  ");
            sqlQuery.Append(" v846 ,v847 ,v848 ,v849 ,v850 ,v851 ,v852 ,v853 ,v854 ,v855 ,v856 ,v857 ,v858 ,v859 ,v860 ,v861 ,v862 ,v863 ,v864 ,v865 ,v866 ,v867 ,v868 ,v869,  ");
            sqlQuery.Append(" v870 ,v871 ,v872 ,v873 ,v874 ,v875 ,v876 ,v877 ,v878 ,v879 ,v880 ,v881 ,v882 ,v883 ,v884 ,v885 ,v886 ,v887 ,v888 ,v889 ,v890 ,v891 ,v892 ,v893,  ");
            sqlQuery.Append(" v894 ,v895 ,v896 ,v897 ,v898 ,v899 ,v900 ,v901 ,v902 ,v903 ,v904 ,v905 ,v906 ,v907 ,v908 ,v909 ,v910 ,v911 ,v912 ,v913 ,v914 ,v915 ,v916 ,v917,  ");
            sqlQuery.Append(" v918 ,v919 ,v920 ,v921 ,v922 ,v923  ");
            sqlQuery.Append(" FROM sep_911.dbo.VW_Primaria_G_Inicio  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_3.VW_Primaria_G_Inicio);
            #endregion

            classRpts.LoadReporte("911_3.rpt");
            
            classRpts.DsDatos = Ds911_3;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(1,0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_3_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_3 Ds911_3 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_3();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_3.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Primaria_G_Inicio
            sqlQuery.Append("   SELECT ID_Control ,ID_CicloEscolar ,Descripcion ,v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15,  ");
            sqlQuery.Append(" v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44,  ");
            sqlQuery.Append(" v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73,  ");
            sqlQuery.Append(" v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101,  ");
            sqlQuery.Append(" v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125,  ");
            sqlQuery.Append(" v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149,  ");
            sqlQuery.Append(" v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162 ,v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173,  ");
            sqlQuery.Append(" v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197,  ");
            sqlQuery.Append(" v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221,  ");
            sqlQuery.Append(" v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237 ,v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245,  ");
            sqlQuery.Append(" v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269,  ");
            sqlQuery.Append(" v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293,  ");
            sqlQuery.Append(" v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312 ,v313 ,v314 ,v315 ,v316 ,v317,  ");
            sqlQuery.Append(" v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341,  ");
            sqlQuery.Append(" v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365,  ");
            sqlQuery.Append(" v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389,  ");
            sqlQuery.Append(" v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412 ,v413,  ");
            sqlQuery.Append(" v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429 ,v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436 ,v437,  ");
            sqlQuery.Append(" v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447 ,v448 ,v449 ,v450 ,v451 ,v452 ,v453 ,v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460 ,v461,  ");
            sqlQuery.Append(" v462 ,v463 ,v464 ,v465 ,v466 ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477 ,v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484 ,v485,  ");
            sqlQuery.Append(" v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501 ,v502 ,v503 ,v504 ,v505 ,v506 ,v507 ,v508 ,v509,  ");
            sqlQuery.Append(" v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523 ,v524 ,v525 ,v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532 ,v533,  ");
            sqlQuery.Append(" v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542 ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549 ,v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556 ,v557,  ");
            sqlQuery.Append(" v558 ,v559 ,v560 ,v561 ,v562 ,v563 ,v564 ,v565 ,v566 ,v567 ,v568 ,v569 ,v570 ,v571 ,v572 ,v573 ,v574 ,v575 ,v576 ,v577 ,v578 ,v579 ,v580 ,v581,  ");
            sqlQuery.Append(" v582 ,v583 ,v584 ,v585 ,v586 ,v587 ,v588 ,v589 ,v590 ,v591 ,v592 ,v593 ,v594 ,v595 ,v596 ,v597 ,v598 ,v599 ,v600 ,v601 ,v602 ,v603 ,v604 ,v605,  ");
            sqlQuery.Append(" v606 ,v607 ,v608 ,v609 ,v610 ,v611 ,v612 ,v613 ,v614 ,v615 ,v616 ,v617 ,v618 ,v619 ,v620 ,v621 ,v622 ,v623 ,v624 ,v625 ,v626 ,v627 ,v628 ,v629,  ");
            sqlQuery.Append(" v630 ,v631 ,v632 ,v633 ,v634 ,v635 ,v636 ,v637 ,v638 ,v639 ,v640 ,v641 ,v642 ,v643 ,v644 ,v645 ,v646 ,v647 ,v648 ,v649 ,v650 ,v651 ,v652 ,v653,  ");
            sqlQuery.Append(" v654 ,v655 ,v656 ,v657 ,v658 ,v659 ,v660 ,v661 ,v662 ,v663 ,v664 ,v665 ,v666 ,v667 ,v668 ,v669 ,v670 ,v671 ,v672 ,v673 ,v674 ,v675 ,v676 ,v677,  ");
            sqlQuery.Append(" v678 ,v679 ,v680 ,v681 ,v682 ,v683 ,v684 ,v685 ,v686 ,v687 ,v688 ,v689 ,v690 ,v691 ,v692 ,v693 ,v694 ,v695 ,v696 ,v697 ,v698 ,v699 ,v700 ,v701,  ");
            sqlQuery.Append(" v702 ,v703 ,v704 ,v705 ,v706 ,v707 ,v708 ,v709 ,v710 ,v711 ,v712 ,v713 ,v714 ,v715 ,v716 ,v717 ,v718 ,v719 ,v720 ,v721 ,v722 ,v723 ,v724 ,v725,  ");
            sqlQuery.Append(" v726 ,v727 ,v728 ,v729 ,v730 ,v731 ,v732 ,v733 ,v734 ,v735 ,v736 ,v737 ,v738 ,v739 ,v740 ,v741 ,v742 ,v743 ,v744 ,v745 ,v746 ,v747 ,v748 ,v749,  ");
            sqlQuery.Append(" v750 ,v751 ,v752 ,v753 ,v754 ,v755 ,v756 ,v757 ,v758 ,v759 ,v760 ,v761 ,v762 ,v763 ,v764 ,v765 ,v766 ,v767 ,v768 ,v769 ,v770 ,v771 ,v772 ,v773,  ");
            sqlQuery.Append(" v774 ,v775 ,v776 ,v777 ,v778 ,v779 ,v780 ,v781 ,v782 ,v783 ,v784 ,v785 ,v786 ,v787 ,v788 ,v789 ,v790 ,v791 ,v792 ,v793 ,v794 ,v795 ,v796 ,v797,  ");
            sqlQuery.Append(" v798 ,v799 ,v800 ,v801 ,v802 ,v803 ,v804 ,v805 ,v806 ,v807 ,v808 ,v809 ,v810 ,v811 ,v812 ,v813 ,v814 ,v815 ,v816 ,v817 ,v818 ,v819 ,v820 ,v821,  ");
            sqlQuery.Append(" v822 ,v823 ,v824 ,v825 ,v826 ,v827 ,v828 ,v829 ,v830 ,v831 ,v832 ,v833 ,v834 ,v835 ,v836 ,v837 ,v838 ,v839 ,v840 ,v841 ,v842 ,v843 ,v844 ,v845,  ");
            sqlQuery.Append(" v846 ,v847 ,v848 ,v849 ,v850 ,v851 ,v852 ,v853 ,v854 ,v855 ,v856 ,v857 ,v858 ,v859 ,v860 ,v861 ,v862 ,v863 ,v864 ,v865 ,v866 ,v867 ,v868 ,v869,  ");
            sqlQuery.Append(" v870 ,v871 ,v872 ,v873 ,v874 ,v875 ,v876 ,v877 ,v878 ,v879 ,v880 ,v881 ,v882 ,v883 ,v884 ,v885 ,v886 ,v887 ,v888 ,v889 ,v890 ,v891 ,v892 ,v893,  ");
            sqlQuery.Append(" v894 ,v895 ,v896 ,v897 ,v898 ,v899 ,v900 ,v901 ,v902 ,v903 ,v904 ,v905 ,v906 ,v907 ,v908 ,v909 ,v910 ,v911 ,v912 ,v913 ,v914 ,v915 ,v916 ,v917,  ");
            sqlQuery.Append(" v918 ,v919 ,v920 ,v921 ,v922 ,v923  ");
            sqlQuery.Append(" FROM sep_911.dbo.VW_Primaria_G_Inicio  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_3.VW_Primaria_G_Inicio);
            #endregion

            classRpts.LoadReporte("estadisticaPrim.rpt");
            classRpts.DsDatos = Ds911_3;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0,0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF 911.5
        private void pdf_911_5(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_5 Ds911_5 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_5();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_5.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_5.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Secundaria_G_Inicio
            sqlQuery.Append("   SELECT ID_Control ,Estatus ,Descripcion ,ID_CicloEscolar v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8,  ");
            sqlQuery.Append(" v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36,  ");
            sqlQuery.Append(" v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60 ,v61 ,v62 ,v63 ,v64,  ");
            sqlQuery.Append(" v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92,  ");
            sqlQuery.Append(" v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101 ,v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117,  ");
            sqlQuery.Append(" v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125 ,v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141,  ");
            sqlQuery.Append(" v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149 ,v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162 ,v163 ,v164 ,v165,  ");
            sqlQuery.Append(" v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173 ,v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189,  ");
            sqlQuery.Append(" v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197 ,v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213,  ");
            sqlQuery.Append(" v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221 ,v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237,  ");
            sqlQuery.Append(" v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245 ,v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261,  ");
            sqlQuery.Append(" v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269 ,v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285,  ");
            sqlQuery.Append(" v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293 ,v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309,  ");
            sqlQuery.Append(" v310 ,v311 ,v312 ,v313 ,v314 ,v315 ,v316 ,v317 ,v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333,  ");
            sqlQuery.Append(" v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341 ,v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357,  ");
            sqlQuery.Append(" v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365 ,v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381,  ");
            sqlQuery.Append(" v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389 ,v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405,  ");
            sqlQuery.Append(" v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412 ,v413 ,v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429,  ");
            sqlQuery.Append(" v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436 ,v437 ,v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447 ,v448 ,v449 ,v450 ,v451 ,v452 ,v453,  ");
            sqlQuery.Append(" v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460 ,v461 ,v462 ,v463 ,v464 ,v465 ,v466 ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477,  ");
            sqlQuery.Append(" v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484 ,v485 ,v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501,  ");
            sqlQuery.Append(" v502 ,v503 ,v504 ,v505 ,v506 ,v507 ,v508 ,v509 ,v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523 ,v524 ,v525,  ");
            sqlQuery.Append(" v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532 ,v533 ,v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542 ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549,  ");
            sqlQuery.Append(" v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556 ,v557 ,v558 ,v559 ,v560 ,v561 ,v562 ,v563 ,v564 ,v565 ,v566 ,v567 ,v568 ,v569 ,v570 ,v571 ,v572 ,v573,  ");
            sqlQuery.Append(" v574 ,v575 ,v576 ,v577 ,v578 ,v579 ,v580 ,v581 ,v582 ,v583 ,v584 ,v585 ,v586 ,v587 ,v588 ,v589 ,v590 ,v591 ,v592 ,v593 ,v594 ,v595 ,v596 ,v597,  ");
            sqlQuery.Append(" v598 ,v599 ,v600 ,v601 ,v602 ,v603 ,v604 ,v605 ,v606 ,v607 ,v608 ,v609 ,v610 ,v611 ,v612 ,v613 ,v614 ,v615 ,v616 ,v617 ,v618 ,v619 ,v620 ,v621,  ");
            sqlQuery.Append(" v622 ,v623 ,v624 ,v625 ,v626 ,v627 ,v628 ,v629 ,v630 ,v631 ,v632 ,v633 ,v634 ,v635 ,v636 ,v637 ,v638 ,v639 ,v640 ,v641 ,v642 ,v643 ,v644 ,v645,  ");
            sqlQuery.Append(" v646 ,v647 ,v648 ,v649 ,v650 ,v651 ,v652 ,v653 ,v654 ,v655 ,v656 ,v657 ,v658 ,v659 ,v660 ,v661 ,v662 ,v663 ,v664 ,v665 ,v666 ,v667 ,v668 ,v669,  ");
            sqlQuery.Append(" v670 ,v671 ,v672 ,v673 ,v674 ,v675 ,v676 ,v677 ,v678 ,v679 ,v680 ,v681 ,v682 ,v683 ,v684 ,v685 ,v686 ,v687 ,v688 ,v689 ,v690 ,v691 ,v692 ,v693,  ");
            sqlQuery.Append(" v694 ,v695 ,v696 ,v697 ,v698 ,v699 ,v700 ,v701 ,v702 ,v703 ,v704 ,v705 ,v706 ,v707 ,v708 ,v709 ,v710 ,v711 ,v712 ,v713 ,v714 ,v715 ,v716 ,v717,  ");
            sqlQuery.Append(" v718 ,v719 ,v720 ,v721 ,v722 ,v723 ,v724 ,v725 ,v726 ,v727 ,v728 ,v729 ,v730 ,v731 ,v732 ,v733 ,v734 ,v735 ,v736 ,v737 ,v738 ,v739 ");
            sqlQuery.Append(" FROM sep_911.dbo.VW_Secundaria_G_Inicio  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_5.VW_Secundaria_G_Inicio);
            #endregion

            classRpts.LoadReporte("911_5.rpt");
            classRpts.DsDatos = Ds911_5;


            byte[] PDF_byte = classRpts.GenerarEnMemoria(1,0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_5_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_5 Ds911_5 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_5();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_5.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Secundaria_G_Inicio
            sqlQuery.Append("   SELECT ID_Control ,Estatus ,Descripcion ,ID_CicloEscolar v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8,  ");
            sqlQuery.Append(" v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36,  ");
            sqlQuery.Append(" v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60 ,v61 ,v62 ,v63 ,v64,  ");
            sqlQuery.Append(" v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92,  ");
            sqlQuery.Append(" v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101 ,v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117,  ");
            sqlQuery.Append(" v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125 ,v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141,  ");
            sqlQuery.Append(" v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149 ,v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162 ,v163 ,v164 ,v165,  ");
            sqlQuery.Append(" v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173 ,v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189,  ");
            sqlQuery.Append(" v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197 ,v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213,  ");
            sqlQuery.Append(" v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221 ,v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237,  ");
            sqlQuery.Append(" v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245 ,v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261,  ");
            sqlQuery.Append(" v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269 ,v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285,  ");
            sqlQuery.Append(" v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293 ,v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309,  ");
            sqlQuery.Append(" v310 ,v311 ,v312 ,v313 ,v314 ,v315 ,v316 ,v317 ,v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333,  ");
            sqlQuery.Append(" v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341 ,v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357,  ");
            sqlQuery.Append(" v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365 ,v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381,  ");
            sqlQuery.Append(" v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389 ,v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405,  ");
            sqlQuery.Append(" v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412 ,v413 ,v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429,  ");
            sqlQuery.Append(" v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436 ,v437 ,v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447 ,v448 ,v449 ,v450 ,v451 ,v452 ,v453,  ");
            sqlQuery.Append(" v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460 ,v461 ,v462 ,v463 ,v464 ,v465 ,v466 ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477,  ");
            sqlQuery.Append(" v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484 ,v485 ,v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501,  ");
            sqlQuery.Append(" v502 ,v503 ,v504 ,v505 ,v506 ,v507 ,v508 ,v509 ,v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523 ,v524 ,v525,  ");
            sqlQuery.Append(" v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532 ,v533 ,v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542 ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549,  ");
            sqlQuery.Append(" v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556 ,v557 ,v558 ,v559 ,v560 ,v561 ,v562 ,v563 ,v564 ,v565 ,v566 ,v567 ,v568 ,v569 ,v570 ,v571 ,v572 ,v573,  ");
            sqlQuery.Append(" v574 ,v575 ,v576 ,v577 ,v578 ,v579 ,v580 ,v581 ,v582 ,v583 ,v584 ,v585 ,v586 ,v587 ,v588 ,v589 ,v590 ,v591 ,v592 ,v593 ,v594 ,v595 ,v596 ,v597,  ");
            sqlQuery.Append(" v598 ,v599 ,v600 ,v601 ,v602 ,v603 ,v604 ,v605 ,v606 ,v607 ,v608 ,v609 ,v610 ,v611 ,v612 ,v613 ,v614 ,v615 ,v616 ,v617 ,v618 ,v619 ,v620 ,v621,  ");
            sqlQuery.Append(" v622 ,v623 ,v624 ,v625 ,v626 ,v627 ,v628 ,v629 ,v630 ,v631 ,v632 ,v633 ,v634 ,v635 ,v636 ,v637 ,v638 ,v639 ,v640 ,v641 ,v642 ,v643 ,v644 ,v645,  ");
            sqlQuery.Append(" v646 ,v647 ,v648 ,v649 ,v650 ,v651 ,v652 ,v653 ,v654 ,v655 ,v656 ,v657 ,v658 ,v659 ,v660 ,v661 ,v662 ,v663 ,v664 ,v665 ,v666 ,v667 ,v668 ,v669,  ");
            sqlQuery.Append(" v670 ,v671 ,v672 ,v673 ,v674 ,v675 ,v676 ,v677 ,v678 ,v679 ,v680 ,v681 ,v682 ,v683 ,v684 ,v685 ,v686 ,v687 ,v688 ,v689 ,v690 ,v691 ,v692 ,v693,  ");
            sqlQuery.Append(" v694 ,v695 ,v696 ,v697 ,v698 ,v699 ,v700 ,v701 ,v702 ,v703 ,v704 ,v705 ,v706 ,v707 ,v708 ,v709 ,v710 ,v711 ,v712 ,v713 ,v714 ,v715 ,v716 ,v717,  ");
            sqlQuery.Append(" v718 ,v719 ,v720 ,v721 ,v722 ,v723 ,v724 ,v725 ,v726 ,v727 ,v728 ,v729 ,v730 ,v731 ,v732 ,v733 ,v734 ,v735 ,v736 ,v737 ,v738 ,v739  ");
            sqlQuery.Append(" FROM sep_911.dbo.VW_Secundaria_G_Inicio  "); 
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_5.VW_Secundaria_G_Inicio);
            #endregion

            classRpts.LoadReporte("estadisticaSec.rpt");
            classRpts.DsDatos = Ds911_5;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0,0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF 911.7G
        private void pdf_911_7G(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_7G Ds911_7G = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_7G();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_7G.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7G.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Bach_G_Inicio
            sqlQuery.Append("SELECT ID_Control,ID_CicloEscolar,Fecha_Actualizacion,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17");
            sqlQuery.Append(",v18,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,v34,v35,v36,v37,v38,v39,v40,v41,v42,v43,v44,v45");
            sqlQuery.Append(",v46,v47,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,v65,v66,v67,v68,v69,v70,v71,v72,v73");
            sqlQuery.Append(",v74,v75,v76,v77,v79,v78,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98,v99,v100,v101");
            sqlQuery.Append(",v102,v103,v104,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122,v123,v124");
            sqlQuery.Append(",v125,v126,v127,v128,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146,v147");
            sqlQuery.Append(",v148,v149,v150,v151,v152,v153,v154,v155,v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170");
            sqlQuery.Append(",v171,v172,v173,v174,v175,v176,v177,v178,v179,v180,v181,v182,v183,v184,v185,v186,v187,v188,v189,v190,v191,v192,v193");
            sqlQuery.Append(",v194,v195,v196,v197,v198,v199,v200,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,v212,v213,v214,v215,v216");
            sqlQuery.Append(",v217,v218,v219,v220,v221,v222,v223,v224,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239");
            sqlQuery.Append(",v240,v241,v242,v243,v244,v245,v246,v247,v248,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262");
            sqlQuery.Append(",v263,v264,v265,v266,v268,v269,v267,v270,v271,v272,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285");
            sqlQuery.Append(",v286,v287,v288,v289,v290,v291,v292,v293,v294,v295,v296,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306,v307,v308");
            sqlQuery.Append(",v309,v310,v311,v312,v313,v314,v315,v316,v317,v318,v319,v320,v321,v322,v323,v324,v325,v326,v327,v328,v329,v330,v331");
            sqlQuery.Append(",v332,v333,v334,v335,v336,v337,v338,v339,v340,v341,v342,v343,v344,v345,v346,v347,v348,v349,v350,v351,v352,v353,v354");
            sqlQuery.Append(",v355,v356,v357,v358,v359,v360,v361,v362,v363,v364,v365,v366,v367,v368,v369,v370,v371,v372,v373,v374,v375,v376,v377");
            sqlQuery.Append(",v378,v379,v380,v381,v382,v383,v384,v385,v386,v387,v388,v389,v390,v391,v392,v393,v394,v395,v396,v397,v398,v399,v400");
            sqlQuery.Append(",v401,v402,v403,v404,v405,v406,v407,v408,v409,v410,v411,v412,v413,v414,v415,v416,v417,v418,v419,v420,v421,v422,v423");
            sqlQuery.Append(",v424,v425,v426,v427,v428,v429,v430,v431,v432,v433,v434,v435,v436,v437,v438,v439,v440,v441,v442,v443,v444,v445,v446");
            sqlQuery.Append(",v447,v448,v449,v450,v451,v452,v453,v454,v455,v456,v457,v458,v459,v460,v461,v462,v463,v464,v465,v466,v467,v468,v469");
            sqlQuery.Append(",v470,v471,v472,v473,v474,v475,v476,v477,v478,v479,v480,v481,v482,v483,v484,v485,v486,v487,v488,v489,v490,v491,v492");
            sqlQuery.Append(",v493,v494,v495,v496,v497,v498,v499,v500,v501,v502,v503,v504,v505,v506,v507,v508,v509,v510,v511,v512,v513,v514,v515");
            sqlQuery.Append(",v516,v517,v518,v519,v520,v521,v522,v523,v524,v525,v526,v527,v528,v529,v530,v531,v532,v533,v534,v535,v536,v537,v538");
            sqlQuery.Append(",v539,v540,v541,v542,v543,v544,v545,v546,v547,v548,v549,v550,v551,v552,v553,v554,v555,v556,v557,v558,v559,v560,v561");
            sqlQuery.Append(",v562,v563,v564,v565,v566,v567,v568,v569,v570,v571,v572,v573,v574,v575,v576,v577,v578,v579,v580,v581,v582,v583,v584");
            sqlQuery.Append(",v585,v586,v587,v588,v589,v590,v591,v592,v593,v594,v595,v596,v597,v598,v599,v600,v601,v602,v603,v604,v605,v606,v607");
            sqlQuery.Append(",v608,v609,v610,v611,v612,v613,v614,v615,v616,v617,v618,v619,v620,v621,v622,v623,v624,v625,v626,v627,v628,v629,v630");
            sqlQuery.Append(",v631,v632,v633,v634,v635,v636,v637,v638,v639,v640,v641,v642,v643,v644,v645,v646,v647,v648,v649,v650,v651,v652,v653");
            sqlQuery.Append(",v654,v655,v656,v657,v658,v659,v660,v661,v662,v663,v664,v665,v666,v667,v668,v669,v670,v671,v672,v673,v674,v675,v676");
            sqlQuery.Append(",v677,v678,v679,v680,v681,v682,v683,v684,v685,v686,v687,v688,v689,v690,v691,v692,v693,v694,v695,v696,v697,v698,v699");
            sqlQuery.Append(",v700,v701,v702,v703,v704,v705,v706,v707,v708,v709,v710,v711,v712,v713,v714,v715,v716,v717,v718,v719,v720,v721,v722");
            sqlQuery.Append(",v723,v724,v725,v726,v727,v728,v729,v730,v731,v732,v733,v734,v735,v736,v737,v738,v739,v740,v741,v742,v743,v744,v745");
            sqlQuery.Append(",v746,v747,v748,v749,v750,v751,v752,v753,v754,v755,v756,v757,v758,v759,v760,v761,v762,v763,v764,v765,v766,v767,v768");
            sqlQuery.Append(",v769,v770,v771,v772,v773,v774,v775,v776,v777,v778,v779,v780,v781,v782,v783,v784,v785,v786,v787,v788,v789,v790,v791");
            sqlQuery.Append(",v792,v793,v794,v795,v796,v797,v798,v799,v800,v801,v802,v803,v804,v805,v806,v807,v808,v809,v810,v811,v812,v813,v814");
            sqlQuery.Append(",v815,v816,v817,v818,v819,v820,v821,v822,v823,v824,v825,v826,v827,v828,v829,v830,v831,v832,v833,v834,v835,v836,v837");
            sqlQuery.Append(",v838,v839,v840,v841,v842,v843,v844,v845,v846,v847,v848,v849,v850,v851,v852,v853,v854,v855,v856,v857,v858,v859,v860");
            sqlQuery.Append(",v861,v862,v863,v864,v865,v866,v867,v868,v869,v870,v871,Descripcion,Estatus  ");
            sqlQuery.Append("FROM sep_911.dbo.VW_7G_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7G.VW_7G_Inicio);
            #endregion

            classRpts.LoadReporte("911-7G.rpt");
            classRpts.DsDatos = Ds911_7G;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(1,0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_7G_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_7G Ds911_7G = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_7G();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_7G.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Bach_G_Inicio
            sqlQuery.Append("SELECT ID_Control,ID_CicloEscolar,Fecha_Actualizacion,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17");
            sqlQuery.Append(",v18,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,v34,v35,v36,v37,v38,v39,v40,v41,v42,v43,v44,v45");
            sqlQuery.Append(",v46,v47,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,v65,v66,v67,v68,v69,v70,v71,v72,v73");
            sqlQuery.Append(",v74,v75,v76,v77,v79,v78,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98,v99,v100,v101");
            sqlQuery.Append(",v102,v103,v104,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122,v123,v124");
            sqlQuery.Append(",v125,v126,v127,v128,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146,v147");
            sqlQuery.Append(",v148,v149,v150,v151,v152,v153,v154,v155,v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170");
            sqlQuery.Append(",v171,v172,v173,v174,v175,v176,v177,v178,v179,v180,v181,v182,v183,v184,v185,v186,v187,v188,v189,v190,v191,v192,v193");
            sqlQuery.Append(",v194,v195,v196,v197,v198,v199,v200,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,v212,v213,v214,v215,v216");
            sqlQuery.Append(",v217,v218,v219,v220,v221,v222,v223,v224,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239");
            sqlQuery.Append(",v240,v241,v242,v243,v244,v245,v246,v247,v248,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262");
            sqlQuery.Append(",v263,v264,v265,v266,v268,v269,v267,v270,v271,v272,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285");
            sqlQuery.Append(",v286,v287,v288,v289,v290,v291,v292,v293,v294,v295,v296,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306,v307,v308");
            sqlQuery.Append(",v309,v310,v311,v312,v313,v314,v315,v316,v317,v318,v319,v320,v321,v322,v323,v324,v325,v326,v327,v328,v329,v330,v331");
            sqlQuery.Append(",v332,v333,v334,v335,v336,v337,v338,v339,v340,v341,v342,v343,v344,v345,v346,v347,v348,v349,v350,v351,v352,v353,v354");
            sqlQuery.Append(",v355,v356,v357,v358,v359,v360,v361,v362,v363,v364,v365,v366,v367,v368,v369,v370,v371,v372,v373,v374,v375,v376,v377");
            sqlQuery.Append(",v378,v379,v380,v381,v382,v383,v384,v385,v386,v387,v388,v389,v390,v391,v392,v393,v394,v395,v396,v397,v398,v399,v400");
            sqlQuery.Append(",v401,v402,v403,v404,v405,v406,v407,v408,v409,v410,v411,v412,v413,v414,v415,v416,v417,v418,v419,v420,v421,v422,v423");
            sqlQuery.Append(",v424,v425,v426,v427,v428,v429,v430,v431,v432,v433,v434,v435,v436,v437,v438,v439,v440,v441,v442,v443,v444,v445,v446");
            sqlQuery.Append(",v447,v448,v449,v450,v451,v452,v453,v454,v455,v456,v457,v458,v459,v460,v461,v462,v463,v464,v465,v466,v467,v468,v469");
            sqlQuery.Append(",v470,v471,v472,v473,v474,v475,v476,v477,v478,v479,v480,v481,v482,v483,v484,v485,v486,v487,v488,v489,v490,v491,v492");
            sqlQuery.Append(",v493,v494,v495,v496,v497,v498,v499,v500,v501,v502,v503,v504,v505,v506,v507,v508,v509,v510,v511,v512,v513,v514,v515");
            sqlQuery.Append(",v516,v517,v518,v519,v520,v521,v522,v523,v524,v525,v526,v527,v528,v529,v530,v531,v532,v533,v534,v535,v536,v537,v538");
            sqlQuery.Append(",v539,v540,v541,v542,v543,v544,v545,v546,v547,v548,v549,v550,v551,v552,v553,v554,v555,v556,v557,v558,v559,v560,v561");
            sqlQuery.Append(",v562,v563,v564,v565,v566,v567,v568,v569,v570,v571,v572,v573,v574,v575,v576,v577,v578,v579,v580,v581,v582,v583,v584");
            sqlQuery.Append(",v585,v586,v587,v588,v589,v590,v591,v592,v593,v594,v595,v596,v597,v598,v599,v600,v601,v602,v603,v604,v605,v606,v607");
            sqlQuery.Append(",v608,v609,v610,v611,v612,v613,v614,v615,v616,v617,v618,v619,v620,v621,v622,v623,v624,v625,v626,v627,v628,v629,v630");
            sqlQuery.Append(",v631,v632,v633,v634,v635,v636,v637,v638,v639,v640,v641,v642,v643,v644,v645,v646,v647,v648,v649,v650,v651,v652,v653");
            sqlQuery.Append(",v654,v655,v656,v657,v658,v659,v660,v661,v662,v663,v664,v665,v666,v667,v668,v669,v670,v671,v672,v673,v674,v675,v676");
            sqlQuery.Append(",v677,v678,v679,v680,v681,v682,v683,v684,v685,v686,v687,v688,v689,v690,v691,v692,v693,v694,v695,v696,v697,v698,v699");
            sqlQuery.Append(",v700,v701,v702,v703,v704,v705,v706,v707,v708,v709,v710,v711,v712,v713,v714,v715,v716,v717,v718,v719,v720,v721,v722");
            sqlQuery.Append(",v723,v724,v725,v726,v727,v728,v729,v730,v731,v732,v733,v734,v735,v736,v737,v738,v739,v740,v741,v742,v743,v744,v745");
            sqlQuery.Append(",v746,v747,v748,v749,v750,v751,v752,v753,v754,v755,v756,v757,v758,v759,v760,v761,v762,v763,v764,v765,v766,v767,v768");
            sqlQuery.Append(",v769,v770,v771,v772,v773,v774,v775,v776,v777,v778,v779,v780,v781,v782,v783,v784,v785,v786,v787,v788,v789,v790,v791");
            sqlQuery.Append(",v792,v793,v794,v795,v796,v797,v798,v799,v800,v801,v802,v803,v804,v805,v806,v807,v808,v809,v810,v811,v812,v813,v814");
            sqlQuery.Append(",v815,v816,v817,v818,v819,v820,v821,v822,v823,v824,v825,v826,v827,v828,v829,v830,v831,v832,v833,v834,v835,v836,v837");
            sqlQuery.Append(",v838,v839,v840,v841,v842,v843,v844,v845,v846,v847,v848,v849,v850,v851,v852,v853,v854,v855,v856,v857,v858,v859,v860");
            sqlQuery.Append(",v861,v862,v863,v864,v865,v866,v867,v868,v869,v870,v871,Descripcion,Estatus  ");
            sqlQuery.Append("FROM sep_911.dbo.VW_7G_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7G.VW_7G_Inicio);
            #endregion

            classRpts.LoadReporte("estadistica7G.rpt");
            classRpts.DsDatos = Ds911_7G;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0,0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF 911.7T
        private void pdf_911_7T(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_7T Ds911_7T = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_7T();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_7T.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7T.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Bach_T_Inicio
            sqlQuery.Append(" SELECT ID_Control,ID_CicloEscolar,Fecha_Actualizacion,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18");
            sqlQuery.Append(" ,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,v34,v35,v36,v37,v38,v39,v40,v41,v42,v43,v44,v45,v46,v47");
            sqlQuery.Append(" ,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,v65,v66,v67,v68,v69,v70,v71,v72,v73,v74,v75,v76");
            sqlQuery.Append(" ,v77,v78,v79,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98,v99,v100,v101,v102,v103,v104");
            sqlQuery.Append(" ,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122,v123,v124,v125,v126,v127,v128");
            sqlQuery.Append(" ,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146,v147,v148,v149,v150,v151,v152");
            sqlQuery.Append(" ,v153,v154,v155,v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170,v171,v172,v173,v174,v175,v176");
            sqlQuery.Append(" ,v177,v178,v179,v180,v181,v182,v183,v184,v185,v186,v187,v188,v189,v190,v191,v192,v193,v194,v195,v196,v197,v198,v199,v200");
            sqlQuery.Append(" ,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,v212,v213,v214,v215,v216,v217,v218,v219,v220,v221,v222,v223,v224");
            sqlQuery.Append(" ,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239,v240,v241,v242,v243,v244,v245,v246,v247,v248");
            sqlQuery.Append(" ,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262,v263,v264,v265,v266,v267,v268,v269,v270,v271,v272");
            sqlQuery.Append(" ,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285,v286,v287,v288,v289,v290,v291,v292,v293,v294,v295,v296");
            sqlQuery.Append(" ,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306,v307,v308,v309,v310,v311,v312,v313,v314,v315,v316,v317,v318,v319,v320");
            sqlQuery.Append(" ,v321,v322,v323,v324,v325,v326,v327,v328,v329,v330,v331,v332,v333,v334,v335,v336,v337,v338,v339,v340,v341,v342,v343,v344");
            sqlQuery.Append(" ,v345,v346,v347,v348,v349,v350,v351,v352,v353,v354,v355,v356,v357,v358,v359,v360,v361,v362,v363,v364,v365,v366,v367,v368");
            sqlQuery.Append(" ,v369,v370,v371,v372,v373,v374,v375,v376,v377,v378,v379,v380,v381,v382,v383,v384,v385,v386,v387,v388,v389,v390,v391,v392");
            sqlQuery.Append(" ,v393,v394,v395,v396,v397,v398,v399,v400,v401,v402,v403,v404,v405,v406,v407,v408,v409,v410,v411,v412,v413,v414,v415,v416");
            sqlQuery.Append(" ,v417,v418,v419,v420,v421,v422,v423,v424,v425,v426,v427,v428,v429,v430,v431,v432,v433,v434,v435,v436,v437,v438,v439,v440");
            sqlQuery.Append(" ,v441,v442,v443,v444,v445,v446,v447,v448,v449,v450,v451,v452,v453,v454,v455,v456,v457,v458,v459,v460,v461,v462,v463,v464");
            sqlQuery.Append(" ,v465,v466,v467,v468,v469,v470,v471,v472,v473,v474,v475,v476,v477,v478,v479,v480,v481,v482,v483,v484,v485,v486,v487,v488");
            sqlQuery.Append(" ,v489,v490,v491,v492,v493,v494,v495,v496,v497,v498,v499,v500,v501,v502,v503,v504,v505,v506,v507,v508,v509,v510,v511,v512");
            sqlQuery.Append(" ,v513,v514,v515,v516,v517,v518,v519,v520,v521,v522,v523,v524,v525,v526,v527,v528,v529,v530,v531,v532,v533,v534,v535,v536");
            sqlQuery.Append(" ,v537,v538,v539,v540,v541,v542,v543,v544,v545,v546,v547,v548,v549,v550,v551,v552,v553,v554,v555,v556,v557,v558,v559,v560");
            sqlQuery.Append(" ,v561,v562,v563,v564,v565,v566,v567,v568,v569,v570,v571,v572,v573,v574,v575,v576,v577,v578,v579,v580,v581,v582,v583,v584");
            sqlQuery.Append(" ,v585,v586,v587,v588,v589,v590,v591,v592,v593,v594,v595,v596,v597,v598,v599,v600,v601,v602,v603,v604,v605,v606,v607,v608");
            sqlQuery.Append(" ,v609,v610,v611,v612,v613,v614,v615,v616,v617,v618,v619,v620,v621,v622,v623,v624,v625,v626,v627,v628,v629,v630,v631,v632");
            sqlQuery.Append(" ,v633,v634,v635,v636,v637,v638,v639,v640,v641,v642,v643,v644,v645,v646,v647,v648,v649,v650,v651,v652,v653,v654,v655,v656");
            sqlQuery.Append(" ,v657,v658,v659,v660,v661,v662,v663,v664,v665,v666,v667,v668,v669,v670,v671,v672,v673,v674,v675,v676,v677,v678,v679,v680");
            sqlQuery.Append(" ,v681,v682,v683,v684,v685,v686,v687,v688,v689,v690,v691,v692,v693,v694,v695,v696,v697,v698,v699,v700,v701,v702,v703,v704");
            sqlQuery.Append(" ,v705,v706,v707,v708,v709,v710,v711,v712,v713,v714,v715,v716,v717,v718,v719,v720,v721,v722,v723,v724,v725,v726,v727,v728");
            sqlQuery.Append(" ,v729,v730,v731,v732,v733,v734,v735,v736,v737,v738,v739,v740,v741,v742,v743,v744,v745,v746,v747,v748,v749,v750,v751,v752");
            sqlQuery.Append(" ,v753,v754,v755,v756,v757,v758,v759,v760,v761,v762,v763,v764,v765,v766,v767,v768,v769,v770,v771,v772,v773,v774,v775,v776");
            sqlQuery.Append(" ,v777,v778,v779,v780,v781,v782,v783,v784,v785,v786,v787,v788,v789,v790,v791,v792,v793,v794,v795,v796,v797,v798,v799,v800");
            sqlQuery.Append(" ,v801,v802,v803,v804,v805,v806,v807,v808,v809,v810,v811,v812,v813,v814,v815,v816,v817,v818,v819,v820,v821,v822,v823,v824");
            sqlQuery.Append(" ,v825,v826,v827,v828,v829,v830,v831,v832,v833,v834,v835,v836,v837,v838,v839,v840,v841,v842,v843,v844,Descripcion");
            sqlQuery.Append(" FROM sep_911.dbo.VW_7T_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7T.VW_7T_Inicio);
            #endregion

            #region Datos Carreras Bach Tec
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Carreras_7T
            sqlQuery.Append(" SELECT ID_Control,ID_CicloEscolar,Fecha_Actualizacion,id_Carrera,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20");
            sqlQuery.Append(" ,Descripcion  FROM sep_911.dbo.VW_Carreras_7T_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7T.VW_Carreras_7T_Inicio);
            #endregion


            classRpts.LoadReporte("911_7T.rpt");
            classRpts.DsDatos = Ds911_7T;


            byte[] PDF_byte = classRpts.GenerarEnMemoria(1,1);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_7T_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_7T Ds911_7T = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_7T();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_7T.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Bach_Tec_Inicio
            sqlQuery.Append(" SELECT ID_Control,ID_CicloEscolar,Fecha_Actualizacion,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18");
            sqlQuery.Append(" ,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,v34,v35,v36,v37,v38,v39,v40,v41,v42,v43,v44,v45,v46,v47");
            sqlQuery.Append(" ,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,v65,v66,v67,v68,v69,v70,v71,v72,v73,v74,v75,v76");
            sqlQuery.Append(" ,v77,v78,v79,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98,v99,v100,v101,v102,v103,v104");
            sqlQuery.Append(" ,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122,v123,v124,v125,v126,v127,v128");
            sqlQuery.Append(" ,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146,v147,v148,v149,v150,v151,v152");
            sqlQuery.Append(" ,v153,v154,v155,v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170,v171,v172,v173,v174,v175,v176");
            sqlQuery.Append(" ,v177,v178,v179,v180,v181,v182,v183,v184,v185,v186,v187,v188,v189,v190,v191,v192,v193,v194,v195,v196,v197,v198,v199,v200");
            sqlQuery.Append(" ,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,v212,v213,v214,v215,v216,v217,v218,v219,v220,v221,v222,v223,v224");
            sqlQuery.Append(" ,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239,v240,v241,v242,v243,v244,v245,v246,v247,v248");
            sqlQuery.Append(" ,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262,v263,v264,v265,v266,v267,v268,v269,v270,v271,v272");
            sqlQuery.Append(" ,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285,v286,v287,v288,v289,v290,v291,v292,v293,v294,v295,v296");
            sqlQuery.Append(" ,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306,v307,v308,v309,v310,v311,v312,v313,v314,v315,v316,v317,v318,v319,v320");
            sqlQuery.Append(" ,v321,v322,v323,v324,v325,v326,v327,v328,v329,v330,v331,v332,v333,v334,v335,v336,v337,v338,v339,v340,v341,v342,v343,v344");
            sqlQuery.Append(" ,v345,v346,v347,v348,v349,v350,v351,v352,v353,v354,v355,v356,v357,v358,v359,v360,v361,v362,v363,v364,v365,v366,v367,v368");
            sqlQuery.Append(" ,v369,v370,v371,v372,v373,v374,v375,v376,v377,v378,v379,v380,v381,v382,v383,v384,v385,v386,v387,v388,v389,v390,v391,v392");
            sqlQuery.Append(" ,v393,v394,v395,v396,v397,v398,v399,v400,v401,v402,v403,v404,v405,v406,v407,v408,v409,v410,v411,v412,v413,v414,v415,v416");
            sqlQuery.Append(" ,v417,v418,v419,v420,v421,v422,v423,v424,v425,v426,v427,v428,v429,v430,v431,v432,v433,v434,v435,v436,v437,v438,v439,v440");
            sqlQuery.Append(" ,v441,v442,v443,v444,v445,v446,v447,v448,v449,v450,v451,v452,v453,v454,v455,v456,v457,v458,v459,v460,v461,v462,v463,v464");
            sqlQuery.Append(" ,v465,v466,v467,v468,v469,v470,v471,v472,v473,v474,v475,v476,v477,v478,v479,v480,v481,v482,v483,v484,v485,v486,v487,v488");
            sqlQuery.Append(" ,v489,v490,v491,v492,v493,v494,v495,v496,v497,v498,v499,v500,v501,v502,v503,v504,v505,v506,v507,v508,v509,v510,v511,v512");
            sqlQuery.Append(" ,v513,v514,v515,v516,v517,v518,v519,v520,v521,v522,v523,v524,v525,v526,v527,v528,v529,v530,v531,v532,v533,v534,v535,v536");
            sqlQuery.Append(" ,v537,v538,v539,v540,v541,v542,v543,v544,v545,v546,v547,v548,v549,v550,v551,v552,v553,v554,v555,v556,v557,v558,v559,v560");
            sqlQuery.Append(" ,v561,v562,v563,v564,v565,v566,v567,v568,v569,v570,v571,v572,v573,v574,v575,v576,v577,v578,v579,v580,v581,v582,v583,v584");
            sqlQuery.Append(" ,v585,v586,v587,v588,v589,v590,v591,v592,v593,v594,v595,v596,v597,v598,v599,v600,v601,v602,v603,v604,v605,v606,v607,v608");
            sqlQuery.Append(" ,v609,v610,v611,v612,v613,v614,v615,v616,v617,v618,v619,v620,v621,v622,v623,v624,v625,v626,v627,v628,v629,v630,v631,v632");
            sqlQuery.Append(" ,v633,v634,v635,v636,v637,v638,v639,v640,v641,v642,v643,v644,v645,v646,v647,v648,v649,v650,v651,v652,v653,v654,v655,v656");
            sqlQuery.Append(" ,v657,v658,v659,v660,v661,v662,v663,v664,v665,v666,v667,v668,v669,v670,v671,v672,v673,v674,v675,v676,v677,v678,v679,v680");
            sqlQuery.Append(" ,v681,v682,v683,v684,v685,v686,v687,v688,v689,v690,v691,v692,v693,v694,v695,v696,v697,v698,v699,v700,v701,v702,v703,v704");
            sqlQuery.Append(" ,v705,v706,v707,v708,v709,v710,v711,v712,v713,v714,v715,v716,v717,v718,v719,v720,v721,v722,v723,v724,v725,v726,v727,v728");
            sqlQuery.Append(" ,v729,v730,v731,v732,v733,v734,v735,v736,v737,v738,v739,v740,v741,v742,v743,v744,v745,v746,v747,v748,v749,v750,v751,v752");
            sqlQuery.Append(" ,v753,v754,v755,v756,v757,v758,v759,v760,v761,v762,v763,v764,v765,v766,v767,v768,v769,v770,v771,v772,v773,v774,v775,v776");
            sqlQuery.Append(" ,v777,v778,v779,v780,v781,v782,v783,v784,v785,v786,v787,v788,v789,v790,v791,v792,v793,v794,v795,v796,v797,v798,v799,v800");
            sqlQuery.Append(" ,v801,v802,v803,v804,v805,v806,v807,v808,v809,v810,v811,v812,v813,v814,v815,v816,v817,v818,v819,v820,v821,v822,v823,v824");
            sqlQuery.Append(" ,v825,v826,v827,v828,v829,v830,v831,v832,v833,v834,v835,v836,v837,v838,v839,v840,v841,v842,v843,v844,Descripcion");
            sqlQuery.Append(" FROM sep_911.dbo.VW_7T_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7T.VW_7T_Inicio);
            #endregion

            classRpts.LoadReporte("estadistica7T.rpt");
            classRpts.DsDatos = Ds911_7T;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0,0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF 911.7P
        private void pdf_911_7P(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_7P Ds911_7P = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_7P();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            //sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_7P.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7P.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_7P_Inicio_1
            sqlQuery.Append(" SELECT ID_Control ,ID_CicloEscolar ,Fecha_Actualizacion ,v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14");
            sqlQuery.Append(" ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36 ,v37");
            sqlQuery.Append(" ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60");
            sqlQuery.Append(" ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83");
            sqlQuery.Append(" ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101 ,v102 ,v103 ,v104 ,v105");
            sqlQuery.Append(" ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124");
            sqlQuery.Append(" ,v125 ,v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141 ,v142 ,v143");
            sqlQuery.Append(" ,v144 ,v145 ,v146 ,v147 ,v148 ,v149 ,v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162");
            sqlQuery.Append(" ,v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173 ,v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181");
            sqlQuery.Append(" ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197 ,v198 ,v199 ,v200");
            sqlQuery.Append(" ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219");
            sqlQuery.Append(" ,v220 ,v221 ,v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237 ,v238");
            sqlQuery.Append(" ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245 ,v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257");
            sqlQuery.Append(" ,v258 ,v259 ,v260 ,v261 ,v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269 ,v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276");
            sqlQuery.Append(" ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293 ,v294 ,v295");
            sqlQuery.Append(" ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312 ,v313 ,v314");
            sqlQuery.Append(" ,v315 ,v316 ,v317 ,v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333");
            sqlQuery.Append(" ,v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341 ,v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352");
            sqlQuery.Append(" ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365 ,v366 ,v367 ,v368 ,v369 ,v370 ,v371");
            sqlQuery.Append(" ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389 ,v390");
            sqlQuery.Append(" ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409");
            sqlQuery.Append(" ,v410 ,v411 ,v412 ,v413 ,v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428");
            sqlQuery.Append(" ,v429 ,v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436 ,v437 ,v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447");
            sqlQuery.Append(" ,v448 ,v449 ,v450 ,v451 ,v452 ,v453 ,v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460 ,v461 ,v462 ,v463 ,v464 ,v465 ,v466");
            sqlQuery.Append(" ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477 ,v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484 ,v485");
            sqlQuery.Append(" ,v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501 ,v502 ,v503 ,v504");
            sqlQuery.Append(" ,v505 ,v506 ,v507 ,v508 ,v509 ,v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523");
            sqlQuery.Append(" ,v524 ,v525 ,v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532 ,v533 ,v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542");
            sqlQuery.Append(" ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549 ,v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556 ,v557 ,v558 ,v559 ,v560 ,v561");
            sqlQuery.Append(" ,v562 ,v563 ,v564 ,v565 ,v566 ,v567 ,v568 ,v569 ,v570 ,v571 ,v572 ,v573 ,v574 ,v575 ,v576 ,v577 ,v578 ,v579 ,v580");
            sqlQuery.Append(" ,v581 ,v582 ,v583 ,v584 ,v585 ,v586 ,v587 ,v588 ,v589 ,v590 ,v591 ,v592 ,v593 ,v594 ,v595 ,v596 ,v597 ,v598 ,v599");
            sqlQuery.Append(" ,v600 ,v601 ,v602 ,v603 ,v604 ,v605 ,v606 ,v607 ,v608 ,v609 ,v610 ,v611 ,v612 ,v613 ,v614 ,v615 ,v616 ,v617 ,v618");
            sqlQuery.Append(" ,v619 ,v620 ,v621 ,v622 ,v623 ,v624 ,v625 ,v626 ,v627 ,v628 ,v629 ,v630 ,v631 ,v632 ,v633 ,v634 ,v635 ,v636 ,v637");
            sqlQuery.Append(" ,v638 ,v639 ,v640 ,v641 ,v642 ,v643 ,v644 ,v645 ,v646 ,v647 ,v648 ,v649 ,v650 ,v651 ,v652 ,v653 ,v654 ,v655 ,v656");
            sqlQuery.Append(" ,v657 ,v658 ,v659 ,v660 ,v661 ,v662 ,v663 ,v664 ,v665 ,v666 ,v667 ,v668 ,v669 ,v670 ,v671 ,v672 ,v673 ,v674 ,v675");
            sqlQuery.Append(" ,v676 ,v677 ,v678 ,v679 ,v680 ,v681 ,v682 ,v683 ,v684 ,v685 ,v686 ,v687 ,v688 ,v689 ,v690 ,v691 ,v692 ,v693 ,v694");
            sqlQuery.Append(" ,v695 ,v696 ,v697 ,v698 ,v699 ,v700 ,Estatus");
            sqlQuery.Append(" FROM sep_911.dbo.VW_7P_Inicio_1 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7P.VW_7P_Inicio_1);

            sqlQuery = new System.Text.StringBuilder();
            #region select VW_7P_Inicio_2
            sqlQuery.Append(" SELECT ID_Control, ID_CicloEscolar, Fecha_Actualizacion, v701, v702, v703, v704, v705, v706, v707, v708, v709, v710");
            sqlQuery.Append(" , v711, v712, v713, v714, v715, v716, v717, v718, v719, v720, v721, v722, v723, v724, v725, v726, v727, v728, v729");
            sqlQuery.Append(" , v730, v731, v732, v733, v734, v735, v736, v737, v738, v739, v740, v741, v742, v743, v744, v745, v746, v747, v748");
            sqlQuery.Append(" , v749, v750, v751, v752, v753, v754, v755, v756, v757, v758, v759, v760, v761, v762, v763, v764, v765, v766, v767");
            sqlQuery.Append(" , v768, v769, v770, v771, v772, v773, v774, v775, v776, v777, v778, v779, v780, v781, v782, v783, v784, v785, v786");
            sqlQuery.Append(" , v787, v788, v789, v790, v791, v792, v793, v794, v795, v796, v797, v798, v799, v800, v801, v802, v803, v804, v805");
            sqlQuery.Append(" , v806, v807, v808, v809, v810, v811, v812, v813, v814, v815, v816, v817, v818, v819, v820, v821, v822, v823, v824");
            sqlQuery.Append(" , v825, v826, v827, v828, v829, v830, v831, v832, v833, v834, v835, v836, v837, v838, v839, v840, v841, v842, v843");
            sqlQuery.Append(" , v844, v845, v846, v847, v848, v849, v850, v851, v852, v853, v854, v855, v856, v857, v858, v859, v860, v861, v862");
            sqlQuery.Append(" , v863, v864, v865, v866, v867, v868, v869, v870, v871, v872, v873, v874, v875, v876, v877, v878, v879, v880, v881");
            sqlQuery.Append(" , v882, v883, v884, v885, v886, v887, v888, v889, v890, v891, v892, v893, v894, v895, v896, v897, v898, v899, v900");
            sqlQuery.Append(" , v901, v902, v903, v904, v905, v906, v907, v908, v909, v910, v911, v912, v913, v914, v915, v916, v917, v918, v919");
            sqlQuery.Append(" , v920, v921, v922, v923, v924, v925, v926, v927, v928, v929, v930, v931, v932, v933, v934, v935, v936, v937, v938");
            sqlQuery.Append(" , v939, v940, v941, v942, v943, v944, v945, v946, v947, v948, v949, v950, v951, v952, v953, v954, v955, v956, v957");
            sqlQuery.Append(" , v958, v959, v960, v961, v962, v963, v964, v965, v966, v967, v968, v969, v970, v971, v972, v973, v974, v975, v976");
            sqlQuery.Append(" , v977, v978, v979, v980, v981, v982, v983, v984, v985, v986, v987, v988, v989, v990, v991, v992, v993, v994, v995");
            sqlQuery.Append(" , v996, v997, v998, v999, v1000, v1001, v1002, v1003, v1004, v1005, v1006, v1007, v1008, v1009, v1010, v1011, v1012");
            sqlQuery.Append(" , v1013, v1014, v1015, v1016, v1017, v1018, v1019, v1020, v1021, v1022, v1023, v1024, v1025, v1026, v1027, v1028, v1029");
            sqlQuery.Append(" , v1030, v1031, v1032, v1033, v1034, v1035, v1036, v1037, v1038, v1039, v1040, v1041, v1042, v1043, v1044, v1045, v1046");
            sqlQuery.Append(" , v1047, v1048, v1049, v1050, v1051, v1052, v1053, v1054, v1055, v1056, v1057, v1058, v1059, v1060, v1061, v1062, v1063");
            sqlQuery.Append(" , v1064, v1065, v1066, v1067, v1068, v1069, v1070, v1071, v1072, v1073, v1074, v1075, v1076, v1077, v1078, v1079, v1080");
            sqlQuery.Append(" , v1081, v1082, v1083, v1084, v1085, v1086, v1087, v1088, v1089, v1090, v1091, v1092, v1093, v1094, v1095, v1096, v1097");
            sqlQuery.Append(" , v1098, v1099, Estatus, Descripcion");
            sqlQuery.Append(" FROM sep_911.dbo.VW_7P_Inicio_2 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7P.VW_7P_Inicio_2);


            #endregion

            #region Datos Carreras Bach Tec
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Carreras_7P
            sqlQuery.Append("   SELECT ID_Control, ID_CicloEscolar, Fecha_Actualizacion, id_Carrera, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11");
            sqlQuery.Append(" , C12, C13, C14, C15, C16, C17, C18, C19, C20, C21, C22, C23, C24, C25, C26, C27, C28, C29, Descripcion");
            sqlQuery.Append("   FROM sep_911.dbo.VW_Carreras_7P_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7P.VW_Carreras_7P_Inicio);
            #endregion


            classRpts.LoadReporte("911_7P.rpt");
            classRpts.DsDatos = Ds911_7P;


            byte[] PDF_byte = classRpts.GenerarEnMemoria(1,1);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_7P_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_7P Ds911_7P = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_7P();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            //sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_7P.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_7P_Inicio_1
            sqlQuery.Append(" SELECT ID_Control ,ID_CicloEscolar ,Fecha_Actualizacion ,v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14");
            sqlQuery.Append(" ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36 ,v37");
            sqlQuery.Append(" ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60");
            sqlQuery.Append(" ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83");
            sqlQuery.Append(" ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101 ,v102 ,v103 ,v104 ,v105");
            sqlQuery.Append(" ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124");
            sqlQuery.Append(" ,v125 ,v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141 ,v142 ,v143");
            sqlQuery.Append(" ,v144 ,v145 ,v146 ,v147 ,v148 ,v149 ,v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162");
            sqlQuery.Append(" ,v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173 ,v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181");
            sqlQuery.Append(" ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197 ,v198 ,v199 ,v200");
            sqlQuery.Append(" ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219");
            sqlQuery.Append(" ,v220 ,v221 ,v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237 ,v238");
            sqlQuery.Append(" ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245 ,v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257");
            sqlQuery.Append(" ,v258 ,v259 ,v260 ,v261 ,v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269 ,v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276");
            sqlQuery.Append(" ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293 ,v294 ,v295");
            sqlQuery.Append(" ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312 ,v313 ,v314");
            sqlQuery.Append(" ,v315 ,v316 ,v317 ,v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333");
            sqlQuery.Append(" ,v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341 ,v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352");
            sqlQuery.Append(" ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365 ,v366 ,v367 ,v368 ,v369 ,v370 ,v371");
            sqlQuery.Append(" ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389 ,v390");
            sqlQuery.Append(" ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409");
            sqlQuery.Append(" ,v410 ,v411 ,v412 ,v413 ,v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428");
            sqlQuery.Append(" ,v429 ,v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436 ,v437 ,v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447");
            sqlQuery.Append(" ,v448 ,v449 ,v450 ,v451 ,v452 ,v453 ,v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460 ,v461 ,v462 ,v463 ,v464 ,v465 ,v466");
            sqlQuery.Append(" ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477 ,v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484 ,v485");
            sqlQuery.Append(" ,v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501 ,v502 ,v503 ,v504");
            sqlQuery.Append(" ,v505 ,v506 ,v507 ,v508 ,v509 ,v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523");
            sqlQuery.Append(" ,v524 ,v525 ,v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532 ,v533 ,v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542");
            sqlQuery.Append(" ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549 ,v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556 ,v557 ,v558 ,v559 ,v560 ,v561");
            sqlQuery.Append(" ,v562 ,v563 ,v564 ,v565 ,v566 ,v567 ,v568 ,v569 ,v570 ,v571 ,v572 ,v573 ,v574 ,v575 ,v576 ,v577 ,v578 ,v579 ,v580");
            sqlQuery.Append(" ,v581 ,v582 ,v583 ,v584 ,v585 ,v586 ,v587 ,v588 ,v589 ,v590 ,v591 ,v592 ,v593 ,v594 ,v595 ,v596 ,v597 ,v598 ,v599");
            sqlQuery.Append(" ,v600 ,v601 ,v602 ,v603 ,v604 ,v605 ,v606 ,v607 ,v608 ,v609 ,v610 ,v611 ,v612 ,v613 ,v614 ,v615 ,v616 ,v617 ,v618");
            sqlQuery.Append(" ,v619 ,v620 ,v621 ,v622 ,v623 ,v624 ,v625 ,v626 ,v627 ,v628 ,v629 ,v630 ,v631 ,v632 ,v633 ,v634 ,v635 ,v636 ,v637");
            sqlQuery.Append(" ,v638 ,v639 ,v640 ,v641 ,v642 ,v643 ,v644 ,v645 ,v646 ,v647 ,v648 ,v649 ,v650 ,v651 ,v652 ,v653 ,v654 ,v655 ,v656");
            sqlQuery.Append(" ,v657 ,v658 ,v659 ,v660 ,v661 ,v662 ,v663 ,v664 ,v665 ,v666 ,v667 ,v668 ,v669 ,v670 ,v671 ,v672 ,v673 ,v674 ,v675");
            sqlQuery.Append(" ,v676 ,v677 ,v678 ,v679 ,v680 ,v681 ,v682 ,v683 ,v684 ,v685 ,v686 ,v687 ,v688 ,v689 ,v690 ,v691 ,v692 ,v693 ,v694");
            sqlQuery.Append(" ,v695 ,v696 ,v697 ,v698 ,v699 ,v700 ,Estatus");
            sqlQuery.Append(" FROM sep_911.dbo.VW_7P_Inicio_1 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7P.VW_7P_Inicio_1);

            sqlQuery = new System.Text.StringBuilder();
            #region select VW_7P_Inicio_2
            sqlQuery.Append(" SELECT ID_Control, ID_CicloEscolar, Fecha_Actualizacion, v701, v702, v703, v704, v705, v706, v707, v708, v709, v710");
            sqlQuery.Append(" , v711, v712, v713, v714, v715, v716, v717, v718, v719, v720, v721, v722, v723, v724, v725, v726, v727, v728, v729");
            sqlQuery.Append(" , v730, v731, v732, v733, v734, v735, v736, v737, v738, v739, v740, v741, v742, v743, v744, v745, v746, v747, v748");
            sqlQuery.Append(" , v749, v750, v751, v752, v753, v754, v755, v756, v757, v758, v759, v760, v761, v762, v763, v764, v765, v766, v767");
            sqlQuery.Append(" , v768, v769, v770, v771, v772, v773, v774, v775, v776, v777, v778, v779, v780, v781, v782, v783, v784, v785, v786");
            sqlQuery.Append(" , v787, v788, v789, v790, v791, v792, v793, v794, v795, v796, v797, v798, v799, v800, v801, v802, v803, v804, v805");
            sqlQuery.Append(" , v806, v807, v808, v809, v810, v811, v812, v813, v814, v815, v816, v817, v818, v819, v820, v821, v822, v823, v824");
            sqlQuery.Append(" , v825, v826, v827, v828, v829, v830, v831, v832, v833, v834, v835, v836, v837, v838, v839, v840, v841, v842, v843");
            sqlQuery.Append(" , v844, v845, v846, v847, v848, v849, v850, v851, v852, v853, v854, v855, v856, v857, v858, v859, v860, v861, v862");
            sqlQuery.Append(" , v863, v864, v865, v866, v867, v868, v869, v870, v871, v872, v873, v874, v875, v876, v877, v878, v879, v880, v881");
            sqlQuery.Append(" , v882, v883, v884, v885, v886, v887, v888, v889, v890, v891, v892, v893, v894, v895, v896, v897, v898, v899, v900");
            sqlQuery.Append(" , v901, v902, v903, v904, v905, v906, v907, v908, v909, v910, v911, v912, v913, v914, v915, v916, v917, v918, v919");
            sqlQuery.Append(" , v920, v921, v922, v923, v924, v925, v926, v927, v928, v929, v930, v931, v932, v933, v934, v935, v936, v937, v938");
            sqlQuery.Append(" , v939, v940, v941, v942, v943, v944, v945, v946, v947, v948, v949, v950, v951, v952, v953, v954, v955, v956, v957");
            sqlQuery.Append(" , v958, v959, v960, v961, v962, v963, v964, v965, v966, v967, v968, v969, v970, v971, v972, v973, v974, v975, v976");
            sqlQuery.Append(" , v977, v978, v979, v980, v981, v982, v983, v984, v985, v986, v987, v988, v989, v990, v991, v992, v993, v994, v995");
            sqlQuery.Append(" , v996, v997, v998, v999, v1000, v1001, v1002, v1003, v1004, v1005, v1006, v1007, v1008, v1009, v1010, v1011, v1012");
            sqlQuery.Append(" , v1013, v1014, v1015, v1016, v1017, v1018, v1019, v1020, v1021, v1022, v1023, v1024, v1025, v1026, v1027, v1028, v1029");
            sqlQuery.Append(" , v1030, v1031, v1032, v1033, v1034, v1035, v1036, v1037, v1038, v1039, v1040, v1041, v1042, v1043, v1044, v1045, v1046");
            sqlQuery.Append(" , v1047, v1048, v1049, v1050, v1051, v1052, v1053, v1054, v1055, v1056, v1057, v1058, v1059, v1060, v1061, v1062, v1063");
            sqlQuery.Append(" , v1064, v1065, v1066, v1067, v1068, v1069, v1070, v1071, v1072, v1073, v1074, v1075, v1076, v1077, v1078, v1079, v1080");
            sqlQuery.Append(" , v1081, v1082, v1083, v1084, v1085, v1086, v1087, v1088, v1089, v1090, v1091, v1092, v1093, v1094, v1095, v1096, v1097");
            sqlQuery.Append(" , v1098, v1099, Estatus, Descripcion");
            sqlQuery.Append(" FROM sep_911.dbo.VW_7P_Inicio_2 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_7P.VW_7P_Inicio_2);


            #endregion

            classRpts.LoadReporte("estadistica7P.rpt");
            classRpts.DsDatos = Ds911_7P;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0,0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF USAER-1
        private void pdf_911_USAER_1(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets._911_USAER_1 Ds911_USAER1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets._911_USAER_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_USAER1.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_USAER1.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_USAER_INICIO_1
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_CicloEscolar,ID_Cuestionario,Fecha_Actualizacion,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11");
            sqlQuery.Append(" ,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,v34,v35,v36,v37,v38,v39,v40");
            sqlQuery.Append(" ,v41,v42,v43,v44,v45,v46,v47,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,v65,v66,v67,v68,v69");
            sqlQuery.Append(" ,v70,v71,v72,v73,v74,v75,v76,v77,v78,v79,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98");
            sqlQuery.Append(" ,v99,v100,v101,v102,v103,v104,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122");
            sqlQuery.Append(" ,v123,v124,v125,v126,v127,v128,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146");
            sqlQuery.Append(" ,v147,v148,v149,v150,v151,v152,v153,v154,v155,v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170");
            sqlQuery.Append(" ,v171,v172,v173,v174,v175,v176,v177,v178,v179,v180,v181,v182,v183,v184,v185,v186,v187,v188,v189,v190,v191,v192,v193,v194");
            sqlQuery.Append(" ,v195,v196,v197,v198,v199,v200,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,v212,v213,v214,v215,v216,v217,v218");
            sqlQuery.Append(" ,v219,v220,v221,v222,v223,v224,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239,v240,v241,v242");
            sqlQuery.Append(" ,v243,v244,v245,v246,v247,v248,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262,v263,v264,v265,v266");
            sqlQuery.Append(" ,v267,v268,v269,v270,v271,v272,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285,v286,v287,v288,v289,v290");
            sqlQuery.Append(" ,v291,v292,v293,v294,v295,v296,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306,v307,v308,v309,v310,v311,v312,v313,v314");
            sqlQuery.Append(" ,v315,v316,v317,v318,v319,v320,v321,v322,v323,v324,v325,v326,v327,v328,v329,v330,v331,v332,v333,v334,v335,v336,v337,v338");
            sqlQuery.Append(" ,v339,v340,v341,v342,v343,v344,v345,v346,v347,v348,v349,v350,v351,v352,v353,v354,v355,v356,v357,v358,v359,v360,v361,v362");
            sqlQuery.Append(" ,v363,v364,v365,v366,v367,v368,v369,v370,v371,v372,v373,v374,v375,v376,v377,v378,v379,v380,v381,v382,v383,v384,v385,v386");
            sqlQuery.Append(" ,v387,v388,v389,v390,v391,v392,v393,v394,v395,v396,v397,v398,v399,v400,v401,v402,v403,v404,v405,v406,v407,v408,v409,v410");
            sqlQuery.Append(" ,v411,v412,v413,v414,v415,v416,v417,v418,v419,v420,v421,v422,v423,v424,v425,v426,v427,v428,v429,v430,v431,v432,v433,v434");
            sqlQuery.Append(" ,v435,v436,v437,v438,v439,v440,v441,v442,v443,v444,v445,v446,v447,v448,v449,v450,v451,v452,v453,v454,v455,v456,v457,v458");
            sqlQuery.Append(" ,v459,v460,v461,v462,v463,v464,v465,v466,v467,v468,v469,v470,v471,v472,v473,v474,v475,v476,v477,v478,v479,v480,v481,v482");
            sqlQuery.Append(" ,v483,v484,v485,v486,v487,v488,v489,v490,v491,v492,v493,v494,v495,v496,v497,v498,v499,v500,v501,v502,v503,v504,v505,v506");
            sqlQuery.Append(" ,v507,v508,v509,v510,v511,v512,v513,v514,v515,v516,v517,v518,v519,v520,v521,v522,v523,v524,v525,v526,v527,v528,v529,v530");
            sqlQuery.Append(" ,v531,v532,v533,v534,v535,v536,v537,v538,v539,v540,v541,v542,v543,v544,v545,v546,v547,v548,v549,v550,v551,v552,v553,v554");
            sqlQuery.Append(" ,v555,v556,v557,v558,v559,v560,v561,v562,v563,v564,v565,v566,v567,v568,v569,v570,v571,v572,v573,v574,v575,v576,v577,v578");
            sqlQuery.Append(" ,v579,v580,v581,v582,v583,v584,v585,v586,v587,v588,v589,v590,v591,v592,v593,v594,v595,v596,v597,v598,v599,v600,v601,v602");
            sqlQuery.Append(" ,v603,v604,v605,v606,v607,v608,v609,v610,v611,v612,v613,v614,v615,v616,v617,v618,v619,v620,v621,v622,v623,v624,v625,v626");
            sqlQuery.Append(" ,v627,v628,v629,v630,v631,v632,v633,v634,v635,v636,v637,v638,v639,v640,v641,v642,v643,v644,v645,v646,v647,v648,v649,v650");
            sqlQuery.Append(" ,v651,v652,v653,v654,v655,v656,v657,v658,v659,v660,v661,v662,v663,v664,v665,v666,v667,v668,v669,v670,v671,v672,v673,v674");
            sqlQuery.Append(" ,v675,v676,v677,v678,v679,v680,v681,v682,v683,v684,v685,v686,v687,v688,v689,v690,v691,v692,v693,v694,v695,v696,v697,v698");
            sqlQuery.Append(" ,v699,v700,v701,v702,v703,v704,v705,v706,v707,v708,v709,v710,v711,v712,v713,v714,v715,v716,v717,v718,v719,v720,v721,v722");
            sqlQuery.Append(" ,v723,v724,v725,v726,v727,v728,v729,v730,v731,v732,v733,v734,v735,v736,v737,v738,v739,v740,v741,v742,v743,v744,v745,v746");
            sqlQuery.Append(" ,v747,v748,v749,v750,v751,v752,v753,v754,v755,v756,v757,v758,v759,v760,v761,v762,v763,v764,v765,v766,v767,v768,v769,v770");
            sqlQuery.Append(" ,v771,v772,v773,v774,v775,v776,v777,v778,v779,v780,v781,v782,v783,v784,v785,v786,v787,v788,v789,v790,v791,v792,v793,v794");
            sqlQuery.Append(" ,v795,v796,v797,v798,v799,v800  FROM sep_911.dbo.VW_Usaer_Inicio_1 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_USAER1.VW_Usaer_Inicio_1);

            sqlQuery = new System.Text.StringBuilder();
            #region select VW_USAER_INICIO_2
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_CicloEscolar,ID_Cuestionario,Fecha_Actualizacion,v801,v802,v803,v804,v805,v806");
            sqlQuery.Append(" ,v807,v808,v809,v810,v811,v812,v813,v814,v815,v816,v817,v818,v819,v820,v821,v822,v823,v824,v825,v826,v827,v828,v829");
            sqlQuery.Append(" ,v830,v831,v832,v833,v834,v835,v836,v837,v838,v839,v840,v841,v842,v843,v844,v845,v846,v847,v848,v849,v850,v851,v852");
            sqlQuery.Append(" ,v853,v854,v855,v856,v857,v858,v859,v860,v861,v862,v863,v864,v865,v866,v867,v868,v869,v870,v871,v872,v873,v874,v875");
            sqlQuery.Append(" ,v876,v877,v878,v879,v880,v881,v882,v883,v884,v885,v886,v887,v888,v889,v890,v891,v892,v893,v894,v895,v896,v897,v898");
            sqlQuery.Append(" ,v899,v900,v901,v902,v903,v904,v905,v906,v907,v908,v909,v910,v911,v912,v913,v914,v915,v916,v917,v918,v919,v920,v921");
            sqlQuery.Append(" ,v922,v923,v924,v925,v926,v927,v928,v929,v930,v931,v932,v933,v934,v935,v936,v937,v938,v939,v940,v941,v942,v943,v944");
            sqlQuery.Append(" ,v945,v946,v947,v948,v949,v950,v951,v952,v953,v954,v955,v956,v957,v958,v959,v960,v961,v962,v963,v964,v965,v966,v967");
            sqlQuery.Append(" ,v968,v969,v970,v971,v972,v973,v974,v975,v976,v977,v978,v979,v980,v981,v982,v983,v984,v985,v986,v987,v988,v989,v990");
            sqlQuery.Append(" ,v991,v992,v993,v994,v995,v996,v997,v998,v999,v1000,v1001,v1002,v1003,v1004,v1005,v1006,v1007,v1008,v1009,v1010,v1011");
            sqlQuery.Append(" ,v1012,v1013,v1014,v1015,v1016,v1017,v1018,v1019,v1020,v1021,v1022,v1023,v1024,v1025,v1026,v1027,v1028,v1029,v1030,v1031");
            sqlQuery.Append(" ,v1032,v1033,v1034,v1035,v1036,v1037,v1038,v1039,v1040,v1041,v1042,v1043,v1044,v1045,v1046,v1047,v1048,v1049,v1050,v1051");
            sqlQuery.Append(" ,v1052,v1053,v1054,v1055,v1056,v1057,v1058,v1059,v1060,v1061,v1062,v1063,v1064,v1065,v1066,v1067,v1068,v1069,v1070,v1071");
            sqlQuery.Append(" ,v1072,v1073,v1074,v1075,v1076,v1077,v1078,v1079,v1080,v1081,v1082,v1083,v1084,v1085,v1086,v1087,v1088,v1089,v1090,v1091");
            sqlQuery.Append(" ,v1092,v1093,v1094,v1095,v1096,v1097,v1098,v1099,v1100,v1101,v1102,v1103,v1104,v1105,v1106,v1107,v1108,v1109,v1110,v1111");
            sqlQuery.Append(" ,v1112,v1113,v1114,v1115,v1116,v1117,v1118,v1119,v1120,v1121,v1122,v1123,v1124,v1125,v1126,v1127,v1128,v1129,v1130,v1131");
            sqlQuery.Append(" ,v1132,v1133,v1134,v1135,v1136,v1137,v1138,v1139,v1140,v1141,v1142,v1143,v1144,v1145,v1146,v1147,v1148,v1149,v1150,v1151");
            sqlQuery.Append(" ,v1152,v1153,v1154,v1155,v1156,v1157,v1158,v1159,v1160,v1161,v1162,v1163,v1164,v1165,v1166,v1167,v1168,v1169,v1170,v1171");
            sqlQuery.Append(" ,v1172,v1173,v1174,v1175,v1176,v1177,v1178,v1179,v1180,v1181,v1182,v1183,v1184,v1185,v1186,v1187,v1188,v1189,v1190,v1191");
            sqlQuery.Append(" ,v1192,v1193,v1194,v1195,v1196,v1197,v1198,v1199,v1200,v1201,v1202,v1203,v1204,v1205,v1206,v1207,v1208,v1209,v1210,v1211");
            sqlQuery.Append(" ,v1212,v1213,v1214,v1215,v1216,v1217,v1218,v1219,v1220,v1221,v1222,v1223,v1224,v1225,v1226,v1227,v1228,v1229,v1230,v1231");
            sqlQuery.Append(" ,v1232,v1233,v1234,v1235,v1236,v1237,v1238,v1239,v1240,v1241,v1242,v1243,v1244,v1245,v1246,v1247,v1248,v1249,v1250,v1251");
            sqlQuery.Append(" ,v1252,v1253,v1254,v1255,v1256,v1257,v1258,v1259,v1260,v1261,v1262,v1263,v1264,v1265,v1266,v1267,v1268,v1269,v1270,v1271");
            sqlQuery.Append(" ,v1272,v1273,v1274,v1275,v1276,v1277,v1278,v1279,v1280,v1281,v1282,v1283,v1284,v1285,v1286,v1287,v1288,v1289,v1290,v1291");
            sqlQuery.Append(" ,v1292,v1293,v1294,v1295,v1296,v1297,v1298,v1299,v1300,v1301,v1302,v1303,v1304,v1305,v1306,v1307,v1308,v1309,v1310,v1311");
            sqlQuery.Append(" ,v1312,v1313,v1314,v1315,v1316,v1317,v1318,v1319,v1320,v1321,v1322,v1323,v1324,v1325,v1326,v1327,v1328,v1329,v1330,v1331");
            sqlQuery.Append(" ,v1332,v1333,v1334,v1335,v1336,v1337,v1338,v1339,v1340,v1341,v1342,v1343,v1344,v1345,v1346,v1347,v1348,v1349,v1350,v1351");
            sqlQuery.Append(" ,v1352,v1353,v1354,v1355,v1356,v1357,v1358,v1359,v1360,v1361,v1362,v1363,v1364,v1365,v1366,v1367,v1368,v1369,v1370,v1371");
            sqlQuery.Append(" ,v1372,v1373,v1374,v1375,v1376,v1377,v1378,v1379,v1380,v1381,v1382,v1383,v1384,v1385,v1386,v1387,v1388,v1389,v1390,v1391");
            sqlQuery.Append(" ,v1392,v1393,v1394,v1395,v1396,v1397,v1398,v1399,v1400,v1401,v1402,v1403,v1404,v1405,v1406,v1407,v1408,v1409,v1410,v1411");
            sqlQuery.Append(" ,v1412,v1413,v1414,v1415,v1416,v1417,v1418,v1419,v1420,v1421,v1422,v1423,v1424,v1425,v1426,v1427,v1428,v1429,v1430,v1431");
            sqlQuery.Append(" ,v1432,v1433,v1434,v1435,v1436,v1437,v1438,v1439,v1440,v1441,v1442,v1443,v1444,v1445,v1446,Descripcion");
            sqlQuery.Append(" FROM sep_911.dbo.VW_Usaer_Inicio_2 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_USAER1.VW_Usaer_Inicio_2);


            #endregion

            classRpts.LoadReporte("USAER_1.rpt");
            classRpts.DsDatos = Ds911_USAER1;


            byte[] PDF_byte = classRpts.GenerarEnMemoria(1,0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_USAER_1_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets._911_USAER_1 Ds911_USAER1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets._911_USAER_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_USAER1.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_USAER_INICIO_1
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_CicloEscolar,ID_Cuestionario,Fecha_Actualizacion,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11");
            sqlQuery.Append(" ,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,v34,v35,v36,v37,v38,v39,v40");
            sqlQuery.Append(" ,v41,v42,v43,v44,v45,v46,v47,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,v65,v66,v67,v68,v69");
            sqlQuery.Append(" ,v70,v71,v72,v73,v74,v75,v76,v77,v78,v79,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98");
            sqlQuery.Append(" ,v99,v100,v101,v102,v103,v104,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122");
            sqlQuery.Append(" ,v123,v124,v125,v126,v127,v128,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146");
            sqlQuery.Append(" ,v147,v148,v149,v150,v151,v152,v153,v154,v155,v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170");
            sqlQuery.Append(" ,v171,v172,v173,v174,v175,v176,v177,v178,v179,v180,v181,v182,v183,v184,v185,v186,v187,v188,v189,v190,v191,v192,v193,v194");
            sqlQuery.Append(" ,v195,v196,v197,v198,v199,v200,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,v212,v213,v214,v215,v216,v217,v218");
            sqlQuery.Append(" ,v219,v220,v221,v222,v223,v224,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239,v240,v241,v242");
            sqlQuery.Append(" ,v243,v244,v245,v246,v247,v248,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262,v263,v264,v265,v266");
            sqlQuery.Append(" ,v267,v268,v269,v270,v271,v272,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285,v286,v287,v288,v289,v290");
            sqlQuery.Append(" ,v291,v292,v293,v294,v295,v296,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306,v307,v308,v309,v310,v311,v312,v313,v314");
            sqlQuery.Append(" ,v315,v316,v317,v318,v319,v320,v321,v322,v323,v324,v325,v326,v327,v328,v329,v330,v331,v332,v333,v334,v335,v336,v337,v338");
            sqlQuery.Append(" ,v339,v340,v341,v342,v343,v344,v345,v346,v347,v348,v349,v350,v351,v352,v353,v354,v355,v356,v357,v358,v359,v360,v361,v362");
            sqlQuery.Append(" ,v363,v364,v365,v366,v367,v368,v369,v370,v371,v372,v373,v374,v375,v376,v377,v378,v379,v380,v381,v382,v383,v384,v385,v386");
            sqlQuery.Append(" ,v387,v388,v389,v390,v391,v392,v393,v394,v395,v396,v397,v398,v399,v400,v401,v402,v403,v404,v405,v406,v407,v408,v409,v410");
            sqlQuery.Append(" ,v411,v412,v413,v414,v415,v416,v417,v418,v419,v420,v421,v422,v423,v424,v425,v426,v427,v428,v429,v430,v431,v432,v433,v434");
            sqlQuery.Append(" ,v435,v436,v437,v438,v439,v440,v441,v442,v443,v444,v445,v446,v447,v448,v449,v450,v451,v452,v453,v454,v455,v456,v457,v458");
            sqlQuery.Append(" ,v459,v460,v461,v462,v463,v464,v465,v466,v467,v468,v469,v470,v471,v472,v473,v474,v475,v476,v477,v478,v479,v480,v481,v482");
            sqlQuery.Append(" ,v483,v484,v485,v486,v487,v488,v489,v490,v491,v492,v493,v494,v495,v496,v497,v498,v499,v500,v501,v502,v503,v504,v505,v506");
            sqlQuery.Append(" ,v507,v508,v509,v510,v511,v512,v513,v514,v515,v516,v517,v518,v519,v520,v521,v522,v523,v524,v525,v526,v527,v528,v529,v530");
            sqlQuery.Append(" ,v531,v532,v533,v534,v535,v536,v537,v538,v539,v540,v541,v542,v543,v544,v545,v546,v547,v548,v549,v550,v551,v552,v553,v554");
            sqlQuery.Append(" ,v555,v556,v557,v558,v559,v560,v561,v562,v563,v564,v565,v566,v567,v568,v569,v570,v571,v572,v573,v574,v575,v576,v577,v578");
            sqlQuery.Append(" ,v579,v580,v581,v582,v583,v584,v585,v586,v587,v588,v589,v590,v591,v592,v593,v594,v595,v596,v597,v598,v599,v600,v601,v602");
            sqlQuery.Append(" ,v603,v604,v605,v606,v607,v608,v609,v610,v611,v612,v613,v614,v615,v616,v617,v618,v619,v620,v621,v622,v623,v624,v625,v626");
            sqlQuery.Append(" ,v627,v628,v629,v630,v631,v632,v633,v634,v635,v636,v637,v638,v639,v640,v641,v642,v643,v644,v645,v646,v647,v648,v649,v650");
            sqlQuery.Append(" ,v651,v652,v653,v654,v655,v656,v657,v658,v659,v660,v661,v662,v663,v664,v665,v666,v667,v668,v669,v670,v671,v672,v673,v674");
            sqlQuery.Append(" ,v675,v676,v677,v678,v679,v680,v681,v682,v683,v684,v685,v686,v687,v688,v689,v690,v691,v692,v693,v694,v695,v696,v697,v698");
            sqlQuery.Append(" ,v699,v700,v701,v702,v703,v704,v705,v706,v707,v708,v709,v710,v711,v712,v713,v714,v715,v716,v717,v718,v719,v720,v721,v722");
            sqlQuery.Append(" ,v723,v724,v725,v726,v727,v728,v729,v730,v731,v732,v733,v734,v735,v736,v737,v738,v739,v740,v741,v742,v743,v744,v745,v746");
            sqlQuery.Append(" ,v747,v748,v749,v750,v751,v752,v753,v754,v755,v756,v757,v758,v759,v760,v761,v762,v763,v764,v765,v766,v767,v768,v769,v770");
            sqlQuery.Append(" ,v771,v772,v773,v774,v775,v776,v777,v778,v779,v780,v781,v782,v783,v784,v785,v786,v787,v788,v789,v790,v791,v792,v793,v794");
            sqlQuery.Append(" ,v795,v796,v797,v798,v799,v800  FROM sep_911.dbo.VW_Usaer_Inicio_1 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_USAER1.VW_Usaer_Inicio_1);

            sqlQuery = new System.Text.StringBuilder();
            #region select VW_USAER_INICIO_2
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_CicloEscolar,ID_Cuestionario,Fecha_Actualizacion,v801,v802,v803,v804,v805,v806");
            sqlQuery.Append(" ,v807,v808,v809,v810,v811,v812,v813,v814,v815,v816,v817,v818,v819,v820,v821,v822,v823,v824,v825,v826,v827,v828,v829");
            sqlQuery.Append(" ,v830,v831,v832,v833,v834,v835,v836,v837,v838,v839,v840,v841,v842,v843,v844,v845,v846,v847,v848,v849,v850,v851,v852");
            sqlQuery.Append(" ,v853,v854,v855,v856,v857,v858,v859,v860,v861,v862,v863,v864,v865,v866,v867,v868,v869,v870,v871,v872,v873,v874,v875");
            sqlQuery.Append(" ,v876,v877,v878,v879,v880,v881,v882,v883,v884,v885,v886,v887,v888,v889,v890,v891,v892,v893,v894,v895,v896,v897,v898");
            sqlQuery.Append(" ,v899,v900,v901,v902,v903,v904,v905,v906,v907,v908,v909,v910,v911,v912,v913,v914,v915,v916,v917,v918,v919,v920,v921");
            sqlQuery.Append(" ,v922,v923,v924,v925,v926,v927,v928,v929,v930,v931,v932,v933,v934,v935,v936,v937,v938,v939,v940,v941,v942,v943,v944");
            sqlQuery.Append(" ,v945,v946,v947,v948,v949,v950,v951,v952,v953,v954,v955,v956,v957,v958,v959,v960,v961,v962,v963,v964,v965,v966,v967");
            sqlQuery.Append(" ,v968,v969,v970,v971,v972,v973,v974,v975,v976,v977,v978,v979,v980,v981,v982,v983,v984,v985,v986,v987,v988,v989,v990");
            sqlQuery.Append(" ,v991,v992,v993,v994,v995,v996,v997,v998,v999,v1000,v1001,v1002,v1003,v1004,v1005,v1006,v1007,v1008,v1009,v1010,v1011");
            sqlQuery.Append(" ,v1012,v1013,v1014,v1015,v1016,v1017,v1018,v1019,v1020,v1021,v1022,v1023,v1024,v1025,v1026,v1027,v1028,v1029,v1030,v1031");
            sqlQuery.Append(" ,v1032,v1033,v1034,v1035,v1036,v1037,v1038,v1039,v1040,v1041,v1042,v1043,v1044,v1045,v1046,v1047,v1048,v1049,v1050,v1051");
            sqlQuery.Append(" ,v1052,v1053,v1054,v1055,v1056,v1057,v1058,v1059,v1060,v1061,v1062,v1063,v1064,v1065,v1066,v1067,v1068,v1069,v1070,v1071");
            sqlQuery.Append(" ,v1072,v1073,v1074,v1075,v1076,v1077,v1078,v1079,v1080,v1081,v1082,v1083,v1084,v1085,v1086,v1087,v1088,v1089,v1090,v1091");
            sqlQuery.Append(" ,v1092,v1093,v1094,v1095,v1096,v1097,v1098,v1099,v1100,v1101,v1102,v1103,v1104,v1105,v1106,v1107,v1108,v1109,v1110,v1111");
            sqlQuery.Append(" ,v1112,v1113,v1114,v1115,v1116,v1117,v1118,v1119,v1120,v1121,v1122,v1123,v1124,v1125,v1126,v1127,v1128,v1129,v1130,v1131");
            sqlQuery.Append(" ,v1132,v1133,v1134,v1135,v1136,v1137,v1138,v1139,v1140,v1141,v1142,v1143,v1144,v1145,v1146,v1147,v1148,v1149,v1150,v1151");
            sqlQuery.Append(" ,v1152,v1153,v1154,v1155,v1156,v1157,v1158,v1159,v1160,v1161,v1162,v1163,v1164,v1165,v1166,v1167,v1168,v1169,v1170,v1171");
            sqlQuery.Append(" ,v1172,v1173,v1174,v1175,v1176,v1177,v1178,v1179,v1180,v1181,v1182,v1183,v1184,v1185,v1186,v1187,v1188,v1189,v1190,v1191");
            sqlQuery.Append(" ,v1192,v1193,v1194,v1195,v1196,v1197,v1198,v1199,v1200,v1201,v1202,v1203,v1204,v1205,v1206,v1207,v1208,v1209,v1210,v1211");
            sqlQuery.Append(" ,v1212,v1213,v1214,v1215,v1216,v1217,v1218,v1219,v1220,v1221,v1222,v1223,v1224,v1225,v1226,v1227,v1228,v1229,v1230,v1231");
            sqlQuery.Append(" ,v1232,v1233,v1234,v1235,v1236,v1237,v1238,v1239,v1240,v1241,v1242,v1243,v1244,v1245,v1246,v1247,v1248,v1249,v1250,v1251");
            sqlQuery.Append(" ,v1252,v1253,v1254,v1255,v1256,v1257,v1258,v1259,v1260,v1261,v1262,v1263,v1264,v1265,v1266,v1267,v1268,v1269,v1270,v1271");
            sqlQuery.Append(" ,v1272,v1273,v1274,v1275,v1276,v1277,v1278,v1279,v1280,v1281,v1282,v1283,v1284,v1285,v1286,v1287,v1288,v1289,v1290,v1291");
            sqlQuery.Append(" ,v1292,v1293,v1294,v1295,v1296,v1297,v1298,v1299,v1300,v1301,v1302,v1303,v1304,v1305,v1306,v1307,v1308,v1309,v1310,v1311");
            sqlQuery.Append(" ,v1312,v1313,v1314,v1315,v1316,v1317,v1318,v1319,v1320,v1321,v1322,v1323,v1324,v1325,v1326,v1327,v1328,v1329,v1330,v1331");
            sqlQuery.Append(" ,v1332,v1333,v1334,v1335,v1336,v1337,v1338,v1339,v1340,v1341,v1342,v1343,v1344,v1345,v1346,v1347,v1348,v1349,v1350,v1351");
            sqlQuery.Append(" ,v1352,v1353,v1354,v1355,v1356,v1357,v1358,v1359,v1360,v1361,v1362,v1363,v1364,v1365,v1366,v1367,v1368,v1369,v1370,v1371");
            sqlQuery.Append(" ,v1372,v1373,v1374,v1375,v1376,v1377,v1378,v1379,v1380,v1381,v1382,v1383,v1384,v1385,v1386,v1387,v1388,v1389,v1390,v1391");
            sqlQuery.Append(" ,v1392,v1393,v1394,v1395,v1396,v1397,v1398,v1399,v1400,v1401,v1402,v1403,v1404,v1405,v1406,v1407,v1408,v1409,v1410,v1411");
            sqlQuery.Append(" ,v1412,v1413,v1414,v1415,v1416,v1417,v1418,v1419,v1420,v1421,v1422,v1423,v1424,v1425,v1426,v1427,v1428,v1429,v1430,v1431");
            sqlQuery.Append(" ,v1432,v1433,v1434,v1435,v1436,v1437,v1438,v1439,v1440,v1441,v1442,v1443,v1444,v1445,v1446,Descripcion");
            sqlQuery.Append(" FROM sep_911.dbo.VW_Usaer_Inicio_2 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_USAER1.VW_Usaer_Inicio_2);


            #endregion


            classRpts.LoadReporte("estadisticaUSAER.rpt");
            classRpts.DsDatos = Ds911_USAER1;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0,0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF CAM-1
        private void pdf_911_CAM_1(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_CAM_1 Ds911_CAM1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_CAM_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_CAM1.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_CAM1.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_CAM_INICIO_1
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_CicloEscolar,ID_Cuestionario,Descripcion,Fecha_Actualizacion,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11");
            sqlQuery.Append(" ,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,v34,v35,v36,v37,v38,v39,v40");
            sqlQuery.Append(" ,v41,v42,v43,v44,v45,v46,v47,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,v65,v66,v67,v68,v69");
            sqlQuery.Append(" ,v70,v71,v72,v73,v74,v75,v76,v77,v78,v79,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98");
            sqlQuery.Append(" ,v99,v100,v101,v102,v103,v104,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122");
            sqlQuery.Append(" ,v123,v124,v125,v126,v127,v128,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146");
            sqlQuery.Append(" ,v147,v148,v149,v150,v151,v152,v153,v154,v155,v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170");
            sqlQuery.Append(" ,v171,v172,v173,v174,v175,v176,v177,v178,v179,v180,v181,v182,v183,v184,v185,v186,v187,v188,v189,v190,v191,v192,v193,v194");
            sqlQuery.Append(" ,v195,v196,v197,v198,v199,v200,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,v212,v213,v214,v215,v216,v217,v218");
            sqlQuery.Append(" ,v219,v220,v221,v222,v223,v224,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239,v240,v241,v242");
            sqlQuery.Append(" ,v243,v244,v245,v246,v247,v248,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262,v263,v264,v265,v266");
            sqlQuery.Append(" ,v267,v268,v269,v270,v271,v272,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285,v286,v287,v288,v289,v290");
            sqlQuery.Append(" ,v291,v292,v293,v294,v295,v296,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306,v307,v308,v309,v310,v311,v312,v313,v314");
            sqlQuery.Append(" ,v315,v316,v317,v318,v319,v320,v321,v322,v323,v324,v325,v326,v327,v328,v329,v330,v331,v332,v333,v334,v335,v336,v337,v338");
            sqlQuery.Append(" ,v339,v340,v341,v342,v343,v344,v345,v346,v347,v348,v349,v350,v351,v352,v353,v354,v355,v356,v357,v358,v359,v360,v361,v362");
            sqlQuery.Append(" ,v363,v364,v365,v366,v367,v368,v369,v370,v371,v372,v373,v374,v375,v376,v377,v378,v379,v380,v381,v382,v383,v384,v385,v386");
            sqlQuery.Append(" ,v387,v388,v389,v390,v391,v392,v393,v394,v395,v396,v397,v398,v399,v400,v401,v402,v403,v404,v405,v406,v407,v408,v409,v410");
            sqlQuery.Append(" ,v411,v412,v413,v414,v415,v416,v417,v418,v419,v420,v421,v422,v423,v424,v425,v426,v427,v428,v429,v430,v431,v432,v433,v434");
            sqlQuery.Append(" ,v435,v436,v437,v438,v439,v440,v441,v442,v443,v444,v445,v446,v447,v448,v449,v450,v451,v452,v453,v454,v455,v456,v457,v458");
            sqlQuery.Append(" ,v459,v460,v461,v462,v463,v464,v465,v466,v467,v468,v469,v470,v471,v472,v473,v474,v475,v476,v477,v478,v479,v480,v481,v482");
            sqlQuery.Append(" ,v483,v484,v485,v486,v487,v488,v489,v490,v491,v492,v493,v494,v495,v496,v497,v498,v499,v500,v501,v502,v503,v504,v505,v506");
            sqlQuery.Append(" ,v507,v508,v509,v510,v511,v512,v513,v514,v515,v516,v517,v518,v519,v520,v521,v522,v523,v524,v525,v526,v527,v528,v529,v530");
            sqlQuery.Append(" ,v531,v532,v533,v534,v535,v536,v537,v538,v539,v540,v541,v542,v543,v544,v545,v546,v547,v548,v549,v550,v551,v552,v553,v554");
            sqlQuery.Append(" ,v555,v556,v557,v558,v559,v560,v561,v562,v563,v564,v565,v566,v567,v568,v569,v570,v571,v572,v573,v574,v575,v576,v577,v578");
            sqlQuery.Append(" ,v579,v580,v581,v582,v583,v584,v585,v586,v587,v588,v589,v590,v591,v592,v593,v594,v595,v596,v597,v598,v599,v600");
            sqlQuery.Append(" FROM sep_911.dbo.VW_CAM_Inicio1 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_CAM1.VW_CAM_Inicio1);

            sqlQuery = new System.Text.StringBuilder();
            #region select VW_USAER_INICIO_2
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_CicloEscolar,ID_Cuestionario,Descripcion,Fecha_Actualizacion");
            sqlQuery.Append(" ,v601 ,v602 ,v603,v604,v605,v606,v607,v608,v609,v610,v611,v612,v613,v614,v615,v616,v617,v618,v619,v620,v621,v622,v623,v624,v625,v626");
            sqlQuery.Append(" ,v627,v628,v629,v630,v631,v632,v633,v634,v635,v636,v637,v638,v639,v640,v641,v642,v643,v644,v645,v646,v647,v648,v649,v650");
            sqlQuery.Append(" ,v651,v652,v653,v654,v655,v656,v657,v658,v659,v660,v661,v662,v663,v664,v665,v666,v667,v668,v669,v670,v671,v672,v673,v674");
            sqlQuery.Append(" ,v675,v676,v677,v678,v679,v680,v681,v682,v683,v684,v685,v686,v687,v688,v689,v690,v691,v692,v693,v694,v695,v696,v697,v698");
            sqlQuery.Append(" ,v699,v700,v701,v702,v703,v704,v705,v706,v707,v708,v709,v710,v711,v712,v713,v714,v715,v716,v717,v718,v719,v720,v721,v722");
            sqlQuery.Append(" ,v723,v724,v725,v726,v727,v728,v729,v730,v731,v732,v733,v734,v735,v736,v737,v738,v739,v740,v741,v742,v743,v744,v745,v746");
            sqlQuery.Append(" ,v747,v748,v749,v750,v751,v752,v753,v754,v755,v756,v757,v758,v759,v760,v761,v762,v763,v764,v765,v766,v767,v768,v769,v770");
            sqlQuery.Append(" ,v771,v772,v773,v774,v775,v776,v777,v778,v779,v780,v781,v782,v783,v784,v785,v786,v787,v788,v789,v790,v791,v792,v793,v794,v795,v796,v797,v798,v799,v800,v801,v802,v803,v804,v805,v806");
            sqlQuery.Append(" ,v807,v808,v809,v810,v811,v812,v813,v814,v815,v816,v817,v818,v819,v820,v821,v822,v823,v824,v825,v826,v827,v828,v829");
            sqlQuery.Append(" ,v830,v831,v832,v833,v834,v835,v836,v837,v838,v839,v840,v841,v842,v843,v844,v845,v846,v847,v848,v849,v850,v851,v852");
            sqlQuery.Append(" ,v853,v854,v855,v856,v857,v858,v859,v860,v861,v862,v863,v864,v865,v866,v867,v868,v869,v870,v871,v872,v873,v874,v875");
            sqlQuery.Append(" ,v876,v877,v878,v879,v880,v881,v882,v883,v884,v885,v886,v887,v888,v889,v890,v891,v892,v893,v894,v895,v896,v897,v898");
            sqlQuery.Append(" ,v899,v900,v901,v902,v903,v904,v905,v906,v907,v908,v909,v910,v911,v912,v913,v914,v915,v916,v917,v918,v919,v920,v921");
            sqlQuery.Append(" ,v922,v923,v924,v925,v926,v927,v928,v929,v930,v931,v932,v933,v934,v935,v936,v937,v938,v939,v940,v941,v942,v943,v944");
            sqlQuery.Append(" ,v945,v946,v947,v948,v949,v950,v951,v952,v953,v954,v955,v956,v957,v958,v959,v960,v961,v962,v963,v964,v965,v966,v967");
            sqlQuery.Append(" ,v968,v969,v970,v971,v972,v973,v974,v975,v976,v977,v978,v979,v980,v981,v982,v983,v984,v985,v986,v987,v988,v989,v990");
            sqlQuery.Append(" ,v991,v992,v993,v994,v995,v996,v997,v998,v999,v1000,v1001,v1002,v1003,v1004,v1005,v1006,v1007,v1008,v1009,v1010,v1011");
            sqlQuery.Append(" ,v1012,v1013,v1014,v1015,v1016,v1017,v1018,v1019,v1020,v1021,v1022,v1023,v1024,v1025,v1026,v1027,v1028,v1029,v1030,v1031");
            sqlQuery.Append(" ,v1032,v1033,v1034,v1035,v1036,v1037,v1038,v1039,v1040,v1041,v1042,v1043,v1044,v1045,v1046,v1047,v1048,v1049,v1050,v1051");
            sqlQuery.Append(" ,v1052,v1053,v1054,v1055,v1056,v1057,v1058,v1059,v1060,v1061,v1062,v1063,v1064,v1065,v1066,v1067,v1068,v1069,v1070,v1071");
            sqlQuery.Append(" ,v1072,v1073,v1074,v1075,v1076,v1077,v1078,v1079,v1080,v1081,v1082,v1083,v1084,v1085,v1086,v1087,v1088,v1089,v1090,v1091");
            sqlQuery.Append(" ,v1092,v1093,v1094,v1095,v1096,v1097,v1098,v1099,v1100,v1101,v1102,v1103,v1104,v1105,v1106,v1107,v1108,v1109,v1110,v1111");
            sqlQuery.Append(" ,v1112,v1113,v1114,v1115,v1116,v1117,v1118,v1119,v1120,v1121,v1122,v1123,v1124,v1125,v1126,v1127,v1128,v1129,v1130,v1131");
            sqlQuery.Append(" ,v1132,v1133,v1134,v1135,v1136,v1137,v1138,v1139,v1140,v1141,v1142,v1143,v1144,v1145,v1146,v1147,v1148,v1149,v1150,v1151");
            sqlQuery.Append(" ,v1152,v1153,v1154,v1155,v1156,v1157,v1158,v1159,v1160,v1161,v1162,v1163,v1164,v1165,v1166,v1167,v1168,v1169,v1170,v1171");
            sqlQuery.Append(" ,v1172,v1173,v1174,v1175,v1176,v1177,v1178,v1179,v1180,v1181,v1182,v1183,v1184,v1185,v1186,v1187,v1188,v1189,v1190,v1191,v1192");
            sqlQuery.Append(" FROM sep_911.dbo.VW_CAM_Inicio2 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_CAM1.VW_CAM_Inicio2);


            #endregion

            classRpts.LoadReporte("CAM_1.rpt");
            classRpts.DsDatos = Ds911_CAM1;


            byte[] PDF_byte = classRpts.GenerarEnMemoria(1, 0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_CAM_1_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_CAM_1 Ds911_CAM1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_CAM_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_CAM1.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_CAM_Inicio1
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_CicloEscolar,ID_Cuestionario,Descripcion,Fecha_Actualizacion,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11");
            sqlQuery.Append(" ,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22,v23,v24,v25,v26,v27,v28,v29,v30,v31,v32,v33,v34,v35,v36,v37,v38,v39,v40");
            sqlQuery.Append(" ,v41,v42,v43,v44,v45,v46,v47,v48,v49,v50,v51,v52,v53,v54,v55,v56,v57,v58,v59,v60,v61,v62,v63,v64,v65,v66,v67,v68,v69");
            sqlQuery.Append(" ,v70,v71,v72,v73,v74,v75,v76,v77,v78,v79,v80,v81,v82,v83,v84,v85,v86,v87,v88,v89,v90,v91,v92,v93,v94,v95,v96,v97,v98");
            sqlQuery.Append(" ,v99,v100,v101,v102,v103,v104,v105,v106,v107,v108,v109,v110,v111,v112,v113,v114,v115,v116,v117,v118,v119,v120,v121,v122");
            sqlQuery.Append(" ,v123,v124,v125,v126,v127,v128,v129,v130,v131,v132,v133,v134,v135,v136,v137,v138,v139,v140,v141,v142,v143,v144,v145,v146");
            sqlQuery.Append(" ,v147,v148,v149,v150,v151,v152,v153,v154,v155,v156,v157,v158,v159,v160,v161,v162,v163,v164,v165,v166,v167,v168,v169,v170");
            sqlQuery.Append(" ,v171,v172,v173,v174,v175,v176,v177,v178,v179,v180,v181,v182,v183,v184,v185,v186,v187,v188,v189,v190,v191,v192,v193,v194");
            sqlQuery.Append(" ,v195,v196,v197,v198,v199,v200,v201,v202,v203,v204,v205,v206,v207,v208,v209,v210,v211,v212,v213,v214,v215,v216,v217,v218");
            sqlQuery.Append(" ,v219,v220,v221,v222,v223,v224,v225,v226,v227,v228,v229,v230,v231,v232,v233,v234,v235,v236,v237,v238,v239,v240,v241,v242");
            sqlQuery.Append(" ,v243,v244,v245,v246,v247,v248,v249,v250,v251,v252,v253,v254,v255,v256,v257,v258,v259,v260,v261,v262,v263,v264,v265,v266");
            sqlQuery.Append(" ,v267,v268,v269,v270,v271,v272,v273,v274,v275,v276,v277,v278,v279,v280,v281,v282,v283,v284,v285,v286,v287,v288,v289,v290");
            sqlQuery.Append(" ,v291,v292,v293,v294,v295,v296,v297,v298,v299,v300,v301,v302,v303,v304,v305,v306,v307,v308,v309,v310,v311,v312,v313,v314");
            sqlQuery.Append(" ,v315,v316,v317,v318,v319,v320,v321,v322,v323,v324,v325,v326,v327,v328,v329,v330,v331,v332,v333,v334,v335,v336,v337,v338");
            sqlQuery.Append(" ,v339,v340,v341,v342,v343,v344,v345,v346,v347,v348,v349,v350,v351,v352,v353,v354,v355,v356,v357,v358,v359,v360,v361,v362");
            sqlQuery.Append(" ,v363,v364,v365,v366,v367,v368,v369,v370,v371,v372,v373,v374,v375,v376,v377,v378,v379,v380,v381,v382,v383,v384,v385,v386");
            sqlQuery.Append(" ,v387,v388,v389,v390,v391,v392,v393,v394,v395,v396,v397,v398,v399,v400,v401,v402,v403,v404,v405,v406,v407,v408,v409,v410");
            sqlQuery.Append(" ,v411,v412,v413,v414,v415,v416,v417,v418,v419,v420,v421,v422,v423,v424,v425,v426,v427,v428,v429,v430,v431,v432,v433,v434");
            sqlQuery.Append(" ,v435,v436,v437,v438,v439,v440,v441,v442,v443,v444,v445,v446,v447,v448,v449,v450,v451,v452,v453,v454,v455,v456,v457,v458");
            sqlQuery.Append(" ,v459,v460,v461,v462,v463,v464,v465,v466,v467,v468,v469,v470,v471,v472,v473,v474,v475,v476,v477,v478,v479,v480,v481,v482");
            sqlQuery.Append(" ,v483,v484,v485,v486,v487,v488,v489,v490,v491,v492,v493,v494,v495,v496,v497,v498,v499,v500,v501,v502,v503,v504,v505,v506");
            sqlQuery.Append(" ,v507,v508,v509,v510,v511,v512,v513,v514,v515,v516,v517,v518,v519,v520,v521,v522,v523,v524,v525,v526,v527,v528,v529,v530");
            sqlQuery.Append(" ,v531,v532,v533,v534,v535,v536,v537,v538,v539,v540,v541,v542,v543,v544,v545,v546,v547,v548,v549,v550,v551,v552,v553,v554");
            sqlQuery.Append(" ,v555,v556,v557,v558,v559,v560,v561,v562,v563,v564,v565,v566,v567,v568,v569,v570,v571,v572,v573,v574,v575,v576,v577,v578");
            sqlQuery.Append(" ,v579,v580,v581,v582,v583,v584,v585,v586,v587,v588,v589,v590,v591,v592,v593,v594,v595,v596,v597,v598,v599,v600");
            sqlQuery.Append(" FROM sep_911.dbo.VW_CAM_Inicio1 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_CAM1.VW_CAM_Inicio1);

            sqlQuery = new System.Text.StringBuilder();
            #region select VW_CAM_Inicio2
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_CicloEscolar,ID_Cuestionario,Descripcion,Fecha_Actualizacion");
            sqlQuery.Append(" ,v601 ,v602 ,v603,v604,v605,v606,v607,v608,v609,v610,v611,v612,v613,v614,v615,v616,v617,v618,v619,v620,v621,v622,v623,v624,v625,v626");
            sqlQuery.Append(" ,v627,v628,v629,v630,v631,v632,v633,v634,v635,v636,v637,v638,v639,v640,v641,v642,v643,v644,v645,v646,v647,v648,v649,v650");
            sqlQuery.Append(" ,v651,v652,v653,v654,v655,v656,v657,v658,v659,v660,v661,v662,v663,v664,v665,v666,v667,v668,v669,v670,v671,v672,v673,v674");
            sqlQuery.Append(" ,v675,v676,v677,v678,v679,v680,v681,v682,v683,v684,v685,v686,v687,v688,v689,v690,v691,v692,v693,v694,v695,v696,v697,v698");
            sqlQuery.Append(" ,v699,v700,v701,v702,v703,v704,v705,v706,v707,v708,v709,v710,v711,v712,v713,v714,v715,v716,v717,v718,v719,v720,v721,v722");
            sqlQuery.Append(" ,v723,v724,v725,v726,v727,v728,v729,v730,v731,v732,v733,v734,v735,v736,v737,v738,v739,v740,v741,v742,v743,v744,v745,v746");
            sqlQuery.Append(" ,v747,v748,v749,v750,v751,v752,v753,v754,v755,v756,v757,v758,v759,v760,v761,v762,v763,v764,v765,v766,v767,v768,v769,v770");
            sqlQuery.Append(" ,v771,v772,v773,v774,v775,v776,v777,v778,v779,v780,v781,v782,v783,v784,v785,v786,v787,v788,v789,v790,v791,v792,v793,v794,v795,v796,v797,v798,v799,v800,v801,v802,v803,v804,v805,v806");
            sqlQuery.Append(" ,v807,v808,v809,v810,v811,v812,v813,v814,v815,v816,v817,v818,v819,v820,v821,v822,v823,v824,v825,v826,v827,v828,v829");
            sqlQuery.Append(" ,v830,v831,v832,v833,v834,v835,v836,v837,v838,v839,v840,v841,v842,v843,v844,v845,v846,v847,v848,v849,v850,v851,v852");
            sqlQuery.Append(" ,v853,v854,v855,v856,v857,v858,v859,v860,v861,v862,v863,v864,v865,v866,v867,v868,v869,v870,v871,v872,v873,v874,v875");
            sqlQuery.Append(" ,v876,v877,v878,v879,v880,v881,v882,v883,v884,v885,v886,v887,v888,v889,v890,v891,v892,v893,v894,v895,v896,v897,v898");
            sqlQuery.Append(" ,v899,v900,v901,v902,v903,v904,v905,v906,v907,v908,v909,v910,v911,v912,v913,v914,v915,v916,v917,v918,v919,v920,v921");
            sqlQuery.Append(" ,v922,v923,v924,v925,v926,v927,v928,v929,v930,v931,v932,v933,v934,v935,v936,v937,v938,v939,v940,v941,v942,v943,v944");
            sqlQuery.Append(" ,v945,v946,v947,v948,v949,v950,v951,v952,v953,v954,v955,v956,v957,v958,v959,v960,v961,v962,v963,v964,v965,v966,v967");
            sqlQuery.Append(" ,v968,v969,v970,v971,v972,v973,v974,v975,v976,v977,v978,v979,v980,v981,v982,v983,v984,v985,v986,v987,v988,v989,v990");
            sqlQuery.Append(" ,v991,v992,v993,v994,v995,v996,v997,v998,v999,v1000,v1001,v1002,v1003,v1004,v1005,v1006,v1007,v1008,v1009,v1010,v1011");
            sqlQuery.Append(" ,v1012,v1013,v1014,v1015,v1016,v1017,v1018,v1019,v1020,v1021,v1022,v1023,v1024,v1025,v1026,v1027,v1028,v1029,v1030,v1031");
            sqlQuery.Append(" ,v1032,v1033,v1034,v1035,v1036,v1037,v1038,v1039,v1040,v1041,v1042,v1043,v1044,v1045,v1046,v1047,v1048,v1049,v1050,v1051");
            sqlQuery.Append(" ,v1052,v1053,v1054,v1055,v1056,v1057,v1058,v1059,v1060,v1061,v1062,v1063,v1064,v1065,v1066,v1067,v1068,v1069,v1070,v1071");
            sqlQuery.Append(" ,v1072,v1073,v1074,v1075,v1076,v1077,v1078,v1079,v1080,v1081,v1082,v1083,v1084,v1085,v1086,v1087,v1088,v1089,v1090,v1091");
            sqlQuery.Append(" ,v1092,v1093,v1094,v1095,v1096,v1097,v1098,v1099,v1100,v1101,v1102,v1103,v1104,v1105,v1106,v1107,v1108,v1109,v1110,v1111");
            sqlQuery.Append(" ,v1112,v1113,v1114,v1115,v1116,v1117,v1118,v1119,v1120,v1121,v1122,v1123,v1124,v1125,v1126,v1127,v1128,v1129,v1130,v1131");
            sqlQuery.Append(" ,v1132,v1133,v1134,v1135,v1136,v1137,v1138,v1139,v1140,v1141,v1142,v1143,v1144,v1145,v1146,v1147,v1148,v1149,v1150,v1151");
            sqlQuery.Append(" ,v1152,v1153,v1154,v1155,v1156,v1157,v1158,v1159,v1160,v1161,v1162,v1163,v1164,v1165,v1166,v1167,v1168,v1169,v1170,v1171");
            sqlQuery.Append(" ,v1172,v1173,v1174,v1175,v1176,v1177,v1178,v1179,v1180,v1181,v1182,v1183,v1184,v1185,v1186,v1187,v1188,v1189,v1190,v1191,v1192");
            sqlQuery.Append(" FROM sep_911.dbo.VW_CAM_Inicio2 ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_CAM1.VW_CAM_Inicio2);


            #endregion


            classRpts.LoadReporte("estadisticaCAM.rpt");
            classRpts.DsDatos = Ds911_CAM1;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0, 0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF 911.ECC11
        private void pdf_911_ECC11(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_ECC11 Ds911_ECC11 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_ECC11();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_ECC11.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO

            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_ECC11.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_PREESCOLAR_C_Inicio
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_Turno,ID_CicloEscolar,ID_Cuestionario,Fecha_Actualizacion,");
            sqlQuery.Append(" v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31,  ");
            sqlQuery.Append(" v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59,  ");
            sqlQuery.Append(" v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89,  ");
            sqlQuery.Append(" v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100, ");
            sqlQuery.Append(" Descripcion FROM sep_911.dbo.VW_Preescolar_Conafe_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_ECC11.VW_Preescolar_Conafe_Inicio);
            #endregion

            classRpts.LoadReporte("ECC-11.rpt");
            classRpts.DsDatos = Ds911_ECC11;


            byte[] PDF_byte = classRpts.GenerarEnMemoria(1, 0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_ECC11_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_ECC11 Ds911_ECC11 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_ECC11();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_ECC11.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_PREESCOLAR_CONAFE_INICIO
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_Turno,ID_CicloEscolar,ID_Cuestionario,Fecha_Actualizacion,");
            sqlQuery.Append(" v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31,  ");
            sqlQuery.Append(" v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59,  ");
            sqlQuery.Append(" v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89,  ");
            sqlQuery.Append(" v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100, ");
            sqlQuery.Append(" Descripcion  FROM sep_911.dbo.VW_Preescolar_Conafe_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_ECC11.VW_Preescolar_Conafe_Inicio);
            #endregion

            classRpts.LoadReporte("estadisticaECC11.rpt");
            classRpts.DsDatos = Ds911_ECC11;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0, 0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF 911.ECC12
        private void pdf_911_ECC12(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_ECC12 Ds911_ECC12 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_ECC12();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_ECC12.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO

            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_ECC12.VW_Anexo_Inicio);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_PRIMARIA_C_Inicio
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_Turno,ID_CicloEscolar,ID_Cuestionario,Fecha_Actualizacion, ");
            sqlQuery.Append(" v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31,  ");
            sqlQuery.Append(" v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59,  ");
            sqlQuery.Append(" v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89,  ");
            sqlQuery.Append(" v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101 ,v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114,  ");
            sqlQuery.Append(" v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125 ,v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138,  ");
            sqlQuery.Append(" v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149 ,v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162,  ");
            sqlQuery.Append(" v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173 ,v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187,  ");
            sqlQuery.Append(" v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197 ,v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212,  ");
            sqlQuery.Append(" v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221 ,v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237,  ");
            sqlQuery.Append(" v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245 ,v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262,  ");
            sqlQuery.Append(" v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269 ,v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287,  ");
            sqlQuery.Append(" v288 ,v289 ,v290 ,v291 ,v292 ,v293 ,v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312,  ");
            sqlQuery.Append(" v313 ,v314 ,v315 ,v316 ,v317 ,v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337,  ");
            sqlQuery.Append(" v338 ,v339 ,v340 ,v341 ,v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362,  ");
            sqlQuery.Append(" v363 ,v364 ,v365 ,v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388,  ");
            sqlQuery.Append(" v389 ,v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411,  ");
            sqlQuery.Append(" Descripcion  FROM sep_911.dbo.VW_Primaria_Conafe_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_ECC12.VW_Primaria_Conafe_Inicio);
            #endregion

            classRpts.LoadReporte("ECC-12.rpt");
            classRpts.DsDatos = Ds911_ECC12;


            byte[] PDF_byte = classRpts.GenerarEnMemoria(1, 0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_ECC12_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_ECC12 Ds911_ECC12 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_ECC12();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_ECC12.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_PRIMARIA_C_I
            sqlQuery.Append(" SELECT ID_Control,ID_CentroTrabajo,ID_Turno,ID_CicloEscolar,ID_Cuestionario,Fecha_Actualizacion, ");
            sqlQuery.Append(" v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15 ,v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31,  ");
            sqlQuery.Append(" v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44 ,v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59,  ");
            sqlQuery.Append(" v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73 ,v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89,  ");
            sqlQuery.Append(" v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101 ,v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114,  ");
            sqlQuery.Append(" v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125 ,v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138,  ");
            sqlQuery.Append(" v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149 ,v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162,  ");
            sqlQuery.Append(" v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173 ,v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187,  ");
            sqlQuery.Append(" v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197 ,v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212,  ");
            sqlQuery.Append(" v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221 ,v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237,  ");
            sqlQuery.Append(" v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245 ,v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262,  ");
            sqlQuery.Append(" v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269 ,v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287,  ");
            sqlQuery.Append(" v288 ,v289 ,v290 ,v291 ,v292 ,v293 ,v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312,  ");
            sqlQuery.Append(" v313 ,v314 ,v315 ,v316 ,v317 ,v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337,  ");
            sqlQuery.Append(" v338 ,v339 ,v340 ,v341 ,v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362,  ");
            sqlQuery.Append(" v363 ,v364 ,v365 ,v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388,  ");
            sqlQuery.Append(" v389 ,v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411,  ");
            sqlQuery.Append(" Descripcion FROM sep_911.dbo.VW_Primaria_Conafe_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_ECC12.VW_Primaria_Conafe_Inicio);
            #endregion

            classRpts.LoadReporte("estadisticaECC12.rpt");
            classRpts.DsDatos = Ds911_ECC12;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0, 0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF EI-1
        private void pdf_911_EI_1(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_EI_1 Ds911_EI_1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_EI_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_EI_1.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_EI_1.VW_Anexo_Inicio);
            #endregion


            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Primaria_G_Inicio
            sqlQuery.Append("   SELECT ID_Control ,ID_CentroTrabajo ,ID_CicloEscolar ,Descripcion ,Fecha_Actualizacion ,v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15,  ");
            sqlQuery.Append(" v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44,  ");
            sqlQuery.Append(" v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73,  ");
            sqlQuery.Append(" v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101,  ");
            sqlQuery.Append(" v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125,  ");
            sqlQuery.Append(" v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149,  ");
            sqlQuery.Append(" v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162 ,v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173,  ");
            sqlQuery.Append(" v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197,  ");
            sqlQuery.Append(" v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221,  ");
            sqlQuery.Append(" v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237 ,v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245,  ");
            sqlQuery.Append(" v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269,  ");
            sqlQuery.Append(" v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293,  ");
            sqlQuery.Append(" v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312 ,v313 ,v314 ,v315 ,v316 ,v317,  ");
            sqlQuery.Append(" v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341,  ");
            sqlQuery.Append(" v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365,  ");
            sqlQuery.Append(" v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389,  ");
            sqlQuery.Append(" v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412 ,v413,  ");
            sqlQuery.Append(" v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429 ");
            sqlQuery.Append(" FROM sep_911.dbo.VW_EI_1  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_EI_1.VW_EI_1);
            #endregion

            classRpts.LoadReporte("EI-1.rpt");

            classRpts.DsDatos = Ds911_EI_1;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(1, 0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_EI_1_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_EI_1 Ds911_EI_1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_EI_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_EI_1.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_EI_1
            sqlQuery.Append("   SELECT ID_Control ,ID_CentroTrabajo ,ID_CicloEscolar ,Descripcion ,Fecha_Actualizacion ,v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15,  ");
            sqlQuery.Append(" v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44,  ");
            sqlQuery.Append(" v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73,  ");
            sqlQuery.Append(" v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101,  ");
            sqlQuery.Append(" v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125,  ");
            sqlQuery.Append(" v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149,  ");
            sqlQuery.Append(" v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162 ,v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173,  ");
            sqlQuery.Append(" v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197,  ");
            sqlQuery.Append(" v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221,  ");
            sqlQuery.Append(" v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237 ,v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245,  ");
            sqlQuery.Append(" v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269,  ");
            sqlQuery.Append(" v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293,  ");
            sqlQuery.Append(" v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312 ,v313 ,v314 ,v315 ,v316 ,v317,  ");
            sqlQuery.Append(" v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341,  ");
            sqlQuery.Append(" v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365,  ");
            sqlQuery.Append(" v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389,  ");
            sqlQuery.Append(" v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412 ,v413,  ");
            sqlQuery.Append(" v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429 ");
            sqlQuery.Append(" FROM sep_911.dbo.VW_EI_1  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_EI_1.VW_EI_1);
            #endregion

            classRpts.LoadReporte("estadisticaEI1.rpt");
            classRpts.DsDatos = Ds911_EI_1;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0, 0);

            ImprimeReporte(PDF_byte);
        }

        #endregion


        #region PDF EI_NE1
        private void pdf_911_EI_NE_1(ControlDP controlDP, bool Lleno)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_EI_NE_1 Ds911_EI_NE_1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_EI_NE_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_EI_NE_1.DatosIdentificacionPDF);
            #endregion

            #region datos ANEXO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_ANEXO
            sqlQuery.Append("SELECT ID_Control, ID_CentroTrabajo, Clave, Nombre, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ID_Zona, ");
            sqlQuery.Append("ID_Sector, ID_TipoCuestionario, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones, Anx1, Anx2, Anx3, Anx4, Anx6, ");
            sqlQuery.Append("Anx7, Anx8, Anx9, Anx10, Anx11, Anx12, Anx13, Anx14, Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx107, ");
            sqlQuery.Append("Anx108, Anx109, Anx110, Anx111, Anx112, Anx26, Anx91, Anx92, Anx93, Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, ");
            sqlQuery.Append("Anx104, Anx113, Anx114, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx43, Anx44, Anx45, Anx46, Anx47, Anx48, Anx49, Anx50, ");
            sqlQuery.Append("Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx62, Anx63, Anx64, Anx65, Anx66, Anx67, Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, ");
            sqlQuery.Append("Anx76, Anx77, Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx86, Anx87, Anx88, Anx89 ");
            sqlQuery.Append("FROM VW_Anexo_Inicio ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_EI_NE_1.VW_Anexo_Inicio);
            #endregion


            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Primaria_G_Inicio
            sqlQuery.Append("   SELECT Estatus,ID_Control,ID_CentroTrabajo,ID_CicloEscolar,Fecha_Actualizacion,Descripcion ,v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15,  ");
            sqlQuery.Append(" v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44,  ");
            sqlQuery.Append(" v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73,  ");
            sqlQuery.Append(" v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101,  ");
            sqlQuery.Append(" v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125,  ");
            sqlQuery.Append(" v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149,  ");
            sqlQuery.Append(" v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162 ,v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173,  ");
            sqlQuery.Append(" v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197,  ");
            sqlQuery.Append(" v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221,  ");
            sqlQuery.Append(" v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237 ,v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245,  ");
            sqlQuery.Append(" v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269,  ");
            sqlQuery.Append(" v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293,  ");
            sqlQuery.Append(" v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312 ,v313 ,v314 ,v315 ,v316 ,v317,  ");
            sqlQuery.Append(" v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341,  ");
            sqlQuery.Append(" v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365,  ");
            sqlQuery.Append(" v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389,  ");
            sqlQuery.Append(" v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412 ,v413,  ");
            sqlQuery.Append(" v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429 ,v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436 ,v437,  ");
            sqlQuery.Append(" v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447 ,v448 ,v449 ,v450 ,v451 ,v452 ,v453 ,v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460 ,v461,  ");
            sqlQuery.Append(" v462 ,v463 ,v464 ,v465 ,v466 ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477 ,v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484 ,v485,  ");
            sqlQuery.Append(" v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501 ,v502 ,v503 ,v504 ,v505 ,v506 ,v507 ,v508 ,v509,  ");
            sqlQuery.Append(" v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523 ,v524 ,v525 ,v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532 ,v533,  ");
            sqlQuery.Append(" v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542 ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549 ,v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556 ,v557,  ");
            sqlQuery.Append(" v558 ,v559 ,v560 ,v561 ,v562 ,v563 ,v564 ,v565 ,v566 ,v567 ,v568 ,v569 ,v570 ,v571 ,v572 ,v573 ,v574 ,v575 ,v576 ,v577 ,v578 ,v579 ,v580 ,v581,  ");
            sqlQuery.Append(" v582 ,v583 ,v584 ,v585 ,v586 ,v587 ,v588 ,v589 ,v590 ,v591 ,v592 ,v593 ,v594 ,v595 ,v596 ,v597 ,v598 ,v599 ,v600 ,v601 ,v602 ,v603 ,v604 ,v605,  ");
            sqlQuery.Append(" v606 ,v607 ,v608 ,v609 ,v610 ,v611 ,v612 ,v613 ,v614 ,v615 ,v616 ,v617 ,v618 ,v619 ,v620 ,v621 ,v622 ,v623 ,v624 ,v625 ,v626 ,v627 ,v628 ,v629,  ");
            sqlQuery.Append(" v630 ,v631 ,v632 ,v633 ,v634 ,v635 ,v636 ,v637 ,v638 ,v639 ,v640 ,v641 ,v642 ,v643 ,v644 ,v645 ,v646 ,v647 ,v648 ,v649 ,v650 ,v651 ,v652 ,v653,  ");
            sqlQuery.Append(" v654 ,v655 ,v656 ,v657 ,v658 ,v659 ,v660 ,v661 ,v662 ,v663 ,v664 ,v665 ,v666 ,v667 ,v668 ,v669 ,v670 ,v671 ,v672 ,v673 ,v674 ,v675 ,v676 ,v677,  ");
            sqlQuery.Append(" v678 ,v679 ,v680 ,v681 ,v682 ,v683 ,v684 ,v685 ,v686 ,v687 ,v688 ,v689 ,v690 ,v691 ,v692 ,v693 ,v694 ,v695 ,v696 ,v697 ,v698 ,v699 ,v700 ,v701,  ");
            sqlQuery.Append(" v702 ,v703 ,v704 ,v705 ,v706 ,v707  ");
            sqlQuery.Append(" FROM sep_911.dbo.VW_EI_NE1  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            if (!Lleno) sqlQuery.Append(" AND 1=0");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_EI_NE_1.VW_EI_NE1);
            #endregion

            classRpts.LoadReporte("EI_NE1.rpt");

            classRpts.DsDatos = Ds911_EI_NE_1;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(1, 0);

            ImprimeReporte(PDF_byte);
        }

        private void pdf_911_EI_NE_1_Oficializar(ControlDP controlDP)
        {
            ClassRpts classRpts = new ClassRpts();

            SqlConnection cnn = cnn_DB911();
            SqlConnection cnn2 = cnn_SENLMaster();
            System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
            DataSets.ds911_EI_NE_1 Ds911_EI_NE_1 = new EstadisticasEducativas._911.inicio.Reportes.DataSets.ds911_EI_NE_1();
            SqlDataAdapter da = null;

            #region datos IDENTIFICACION
            SqlCommand com = null;
            #region SELECT DatosIdentificacionPDF
            sqlQuery.Append("SELECT ");
            sqlQuery.Append("ctrl.ID_Control, ");
            sqlQuery.Append("tnt.id_CCTNT, ");
            sqlQuery.Append("ct.id_CentroTrabajo, ");
            sqlQuery.Append("tnt.id_Turno, ");
            sqlQuery.Append("ct.id_nivel, ");
            sqlQuery.Append("ct.ID_SubNivel, ");
            sqlQuery.Append("ct.Clave, ");
            sqlQuery.Append("ct.Nombre, ");
            sqlQuery.Append("LTRIM (RTRIM( dom.Calle)) + ' NUM. ' + LTRIM( dom.Numero_Exterior) AS Domicilio, ");
            sqlQuery.Append("col.Nombre AS Colonia, ");
            sqlQuery.Append("mun.Nombre AS Municipio, ");
            sqlQuery.Append("ent.Nombre AS Entidad, ");
            sqlQuery.Append("dn.nombre AS DependenciaNormativa, ");
            sqlQuery.Append("ser.nombre AS Servicio, ");
            sqlQuery.Append("sos.Nombre AS Sostenimiento, ");
            sqlQuery.Append("ISNULL ( dbo.FN_GETNOMBREPERSONAEx( dbo.FN_GETNOMBREDIRECTORCCTID( ct.Id_CentroTrabajo),2), '') AS Director, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 1)), '') + ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 2)), '') +  ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND ");
            sqlQuery.Append("(Id_TelefonoCentroTrabajo = 3)), '') + ISNULL ");
            sqlQuery.Append("((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 FROM ");
            sqlQuery.Append("TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 11) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Telefono, ");
            sqlQuery.Append("ISNULL ((SELECT DISTINCT ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 1)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 2)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 3)), '') + ");
            sqlQuery.Append("ISNULL ((SELECT ' ' + LTRIM(RTRIM(Numero)) AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS a WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo) AND (Bit_Activo = 1) AND (Id_TipoTelefono = 12) AND (Id_TelefonoCentroTrabajo = 4)), '') AS Expr1 ");
            sqlQuery.Append("FROM TelefonoCentroTrabajo AS d WHERE (Id_CentroTrabajo = ct.Id_CentroTrabajo)), '') AS Fax, ");
            sqlQuery.Append("dom.Codigo_Postal, ");
            sqlQuery.Append("sec.numerosector as Sector, ");
            sqlQuery.Append("zon.numerozona as Zona, ");
            sqlQuery.Append("ct.id_region as Region, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(sec.clave) < 10 THEN '' ELSE sec.clave END AS CveSector, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(zon.clave) < 10 THEN '' ELSE zon.clave END AS CveZona, ");
            sqlQuery.Append("CASE ");
            sqlQuery.Append("WHEN len(reg.Clave) < 10 THEN '' ELSE reg.Clave END AS CveRegion, ");
            sqlQuery.Append("dbo.FN_GETNOMBRECICLO( dbo.FN_GETCICLOACTUAL(1)) AS CicloActual ");
            sqlQuery.Append("FROM ");
            sqlQuery.Append("sep_911.dbo.Tb_CONTROL AS ctrl, ");
            sqlQuery.Append("CentroTrabajo_Nivel_Turno tnt, ");
            sqlQuery.Append("CentroTrabajo ct, ");
            sqlQuery.Append("sep_911.dbo.Cat_ListaNiveles911 AS Cat, ");
            sqlQuery.Append("Inmueble inm, ");
            sqlQuery.Append("Domicilio dom, ");
            sqlQuery.Append("Municipio mun, ");
            sqlQuery.Append("Colonia col, ");
            sqlQuery.Append("Entidad ent, ");
            sqlQuery.Append("DependenciaNormativa dn, ");
            sqlQuery.Append("Servicio ser, ");
            sqlQuery.Append("Sostenimiento sos, ");
            sqlQuery.Append("Region reg, ");
            sqlQuery.Append("Zona zon, ");
            sqlQuery.Append("Sector sec ");
            sqlQuery.Append("Where ");
            sqlQuery.Append("ctrl.id_CCTNT=tnt.id_CCTNT and ");
            sqlQuery.Append("tnt.id_CentroTrabajo=ct.id_CentroTrabajo and ");
            sqlQuery.Append("ct.id_nivel=Cat.id_nivel and ");
            sqlQuery.Append("ct.id_subnivel=Cat.id_subnivel and ");
            sqlQuery.Append("ct.Id_Inmueble = inm.Id_Inmueble and ");
            sqlQuery.Append("inm.Id_Domicilio = dom.Id_Domicilio and ");
            sqlQuery.Append("dom.Id_Pais = mun.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = mun.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = mun.Id_Municipio and ");
            sqlQuery.Append("dom.Id_Pais = col.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = col.Id_Entidad AND ");
            sqlQuery.Append("dom.Id_Municipio = col.Id_Municipio AND ");
            sqlQuery.Append("dom.Id_Colonia = col.Id_Colonia and ");
            sqlQuery.Append("dom.Id_Pais = ent.Id_Pais AND ");
            sqlQuery.Append("dom.Id_Entidad = ent.Id_Entidad and ");
            sqlQuery.Append("dom.Id_localidad = col.Id_localidad and ");
            sqlQuery.Append("ct.id_dependencianormativa = dn.id_dependencianormativa and ");
            sqlQuery.Append("ct.id_servicio = ser.id_servicio and ");
            sqlQuery.Append("ct.Id_Sostenimiento = sos.Id_Sostenimiento and ");
            sqlQuery.Append("ct.Id_Region = reg.Id_Region AND ");
            sqlQuery.Append("ct.Id_Entidad = reg.Id_Entidad AND ");
            sqlQuery.Append("ct.Id_Pais = reg.Id_Pais and ");
            sqlQuery.Append("ct.Id_Zona = zon.id_zona and ");
            sqlQuery.Append("ct.id_sector = sec.id_sector and ");
            sqlQuery.Append("ctrl.bit_activo=1 and ");
            sqlQuery.Append("tnt.bit_activo=1 and ");
            sqlQuery.Append("ct.bit_activo=1 and ");
            sqlQuery.Append("ct.bitProvisional=0 and ");
            sqlQuery.Append("ctrl.ID_Control = @ID_Control and ");
            sqlQuery.Append("Cat.id_TipoCuestionario=1 ");

            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn2);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da = new SqlDataAdapter(com);
            da.Fill(Ds911_EI_NE_1.DatosIdentificacionPDF);
            #endregion

            #region Datos CUESTIONARIO
            sqlQuery = new System.Text.StringBuilder();
            #region select VW_Primaria_G_Inicio
            sqlQuery.Append("   SELECT Estatus,ID_Control,ID_CentroTrabajo,ID_CicloEscolar,Fecha_Actualizacion,Descripcion ,v1 ,v2 ,v3 ,v4 ,v5 ,v6 ,v7 ,v8 ,v9 ,v10 ,v11 ,v12 ,v13 ,v14 ,v15,  ");
            sqlQuery.Append(" v16 ,v17 ,v18 ,v19 ,v20 ,v21 ,v22 ,v23 ,v24 ,v25 ,v26 ,v27 ,v28 ,v29 ,v30 ,v31 ,v32 ,v33 ,v34 ,v35 ,v36 ,v37 ,v38 ,v39 ,v40 ,v41 ,v42 ,v43 ,v44,  ");
            sqlQuery.Append(" v45 ,v46 ,v47 ,v48 ,v49 ,v50 ,v51 ,v52 ,v53 ,v54 ,v55 ,v56 ,v57 ,v58 ,v59 ,v60 ,v61 ,v62 ,v63 ,v64 ,v65 ,v66 ,v67 ,v68 ,v69 ,v70 ,v71 ,v72 ,v73,  ");
            sqlQuery.Append(" v74 ,v75 ,v76 ,v77 ,v78 ,v79 ,v80 ,v81 ,v82 ,v83 ,v84 ,v85 ,v86 ,v87 ,v88 ,v89 ,v90 ,v91 ,v92 ,v93 ,v94 ,v95 ,v96 ,v97 ,v98 ,v99 ,v100 ,v101,  ");
            sqlQuery.Append(" v102 ,v103 ,v104 ,v105 ,v106 ,v107 ,v108 ,v109 ,v110 ,v111 ,v112 ,v113 ,v114 ,v115 ,v116 ,v117 ,v118 ,v119 ,v120 ,v121 ,v122 ,v123 ,v124 ,v125,  ");
            sqlQuery.Append(" v126 ,v127 ,v128 ,v129 ,v130 ,v131 ,v132 ,v133 ,v134 ,v135 ,v136 ,v137 ,v138 ,v139 ,v140 ,v141 ,v142 ,v143 ,v144 ,v145 ,v146 ,v147 ,v148 ,v149,  ");
            sqlQuery.Append(" v150 ,v151 ,v152 ,v153 ,v154 ,v155 ,v156 ,v157 ,v158 ,v159 ,v160 ,v161 ,v162 ,v163 ,v164 ,v165 ,v166 ,v167 ,v168 ,v169 ,v170 ,v171 ,v172 ,v173,  ");
            sqlQuery.Append(" v174 ,v175 ,v176 ,v177 ,v178 ,v179 ,v180 ,v181 ,v182 ,v183 ,v184 ,v185 ,v186 ,v187 ,v188 ,v189 ,v190 ,v191 ,v192 ,v193 ,v194 ,v195 ,v196 ,v197,  ");
            sqlQuery.Append(" v198 ,v199 ,v200 ,v201 ,v202 ,v203 ,v204 ,v205 ,v206 ,v207 ,v208 ,v209 ,v210 ,v211 ,v212 ,v213 ,v214 ,v215 ,v216 ,v217 ,v218 ,v219 ,v220 ,v221,  ");
            sqlQuery.Append(" v222 ,v223 ,v224 ,v225 ,v226 ,v227 ,v228 ,v229 ,v230 ,v231 ,v232 ,v233 ,v234 ,v235 ,v236 ,v237 ,v238 ,v239 ,v240 ,v241 ,v242 ,v243 ,v244 ,v245,  ");
            sqlQuery.Append(" v246 ,v247 ,v248 ,v249 ,v250 ,v251 ,v252 ,v253 ,v254 ,v255 ,v256 ,v257 ,v258 ,v259 ,v260 ,v261 ,v262 ,v263 ,v264 ,v265 ,v266 ,v267 ,v268 ,v269,  ");
            sqlQuery.Append(" v270 ,v271 ,v272 ,v273 ,v274 ,v275 ,v276 ,v277 ,v278 ,v279 ,v280 ,v281 ,v282 ,v283 ,v284 ,v285 ,v286 ,v287 ,v288 ,v289 ,v290 ,v291 ,v292 ,v293,  ");
            sqlQuery.Append(" v294 ,v295 ,v296 ,v297 ,v298 ,v299 ,v300 ,v301 ,v302 ,v303 ,v304 ,v305 ,v306 ,v307 ,v308 ,v309 ,v310 ,v311 ,v312 ,v313 ,v314 ,v315 ,v316 ,v317,  ");
            sqlQuery.Append(" v318 ,v319 ,v320 ,v321 ,v322 ,v323 ,v324 ,v325 ,v326 ,v327 ,v328 ,v329 ,v330 ,v331 ,v332 ,v333 ,v334 ,v335 ,v336 ,v337 ,v338 ,v339 ,v340 ,v341,  ");
            sqlQuery.Append(" v342 ,v343 ,v344 ,v345 ,v346 ,v347 ,v348 ,v349 ,v350 ,v351 ,v352 ,v353 ,v354 ,v355 ,v356 ,v357 ,v358 ,v359 ,v360 ,v361 ,v362 ,v363 ,v364 ,v365,  ");
            sqlQuery.Append(" v366 ,v367 ,v368 ,v369 ,v370 ,v371 ,v372 ,v373 ,v374 ,v375 ,v376 ,v377 ,v378 ,v379 ,v380 ,v381 ,v382 ,v383 ,v384 ,v385 ,v386 ,v387 ,v388 ,v389,  ");
            sqlQuery.Append(" v390 ,v391 ,v392 ,v393 ,v394 ,v395 ,v396 ,v397 ,v398 ,v399 ,v400 ,v401 ,v402 ,v403 ,v404 ,v405 ,v406 ,v407 ,v408 ,v409 ,v410 ,v411 ,v412 ,v413,  ");
            sqlQuery.Append(" v414 ,v415 ,v416 ,v417 ,v418 ,v419 ,v420 ,v421 ,v422 ,v423 ,v424 ,v425 ,v426 ,v427 ,v428 ,v429 ,v430 ,v431 ,v432 ,v433 ,v434 ,v435 ,v436 ,v437,  ");
            sqlQuery.Append(" v438 ,v439 ,v440 ,v441 ,v442 ,v443 ,v444 ,v445 ,v446 ,v447 ,v448 ,v449 ,v450 ,v451 ,v452 ,v453 ,v454 ,v455 ,v456 ,v457 ,v458 ,v459 ,v460 ,v461,  ");
            sqlQuery.Append(" v462 ,v463 ,v464 ,v465 ,v466 ,v467 ,v468 ,v469 ,v470 ,v471 ,v472 ,v473 ,v474 ,v475 ,v476 ,v477 ,v478 ,v479 ,v480 ,v481 ,v482 ,v483 ,v484 ,v485,  ");
            sqlQuery.Append(" v486 ,v487 ,v488 ,v489 ,v490 ,v491 ,v492 ,v493 ,v494 ,v495 ,v496 ,v497 ,v498 ,v499 ,v500 ,v501 ,v502 ,v503 ,v504 ,v505 ,v506 ,v507 ,v508 ,v509,  ");
            sqlQuery.Append(" v510 ,v511 ,v512 ,v513 ,v514 ,v515 ,v516 ,v517 ,v518 ,v519 ,v520 ,v521 ,v522 ,v523 ,v524 ,v525 ,v526 ,v527 ,v528 ,v529 ,v530 ,v531 ,v532 ,v533,  ");
            sqlQuery.Append(" v534 ,v535 ,v536 ,v537 ,v538 ,v539 ,v540 ,v541 ,v542 ,v543 ,v544 ,v545 ,v546 ,v547 ,v548 ,v549 ,v550 ,v551 ,v552 ,v553 ,v554 ,v555 ,v556 ,v557,  ");
            sqlQuery.Append(" v558 ,v559 ,v560 ,v561 ,v562 ,v563 ,v564 ,v565 ,v566 ,v567 ,v568 ,v569 ,v570 ,v571 ,v572 ,v573 ,v574 ,v575 ,v576 ,v577 ,v578 ,v579 ,v580 ,v581,  ");
            sqlQuery.Append(" v582 ,v583 ,v584 ,v585 ,v586 ,v587 ,v588 ,v589 ,v590 ,v591 ,v592 ,v593 ,v594 ,v595 ,v596 ,v597 ,v598 ,v599 ,v600 ,v601 ,v602 ,v603 ,v604 ,v605,  ");
            sqlQuery.Append(" v606 ,v607 ,v608 ,v609 ,v610 ,v611 ,v612 ,v613 ,v614 ,v615 ,v616 ,v617 ,v618 ,v619 ,v620 ,v621 ,v622 ,v623 ,v624 ,v625 ,v626 ,v627 ,v628 ,v629,  ");
            sqlQuery.Append(" v630 ,v631 ,v632 ,v633 ,v634 ,v635 ,v636 ,v637 ,v638 ,v639 ,v640 ,v641 ,v642 ,v643 ,v644 ,v645 ,v646 ,v647 ,v648 ,v649 ,v650 ,v651 ,v652 ,v653,  ");
            sqlQuery.Append(" v654 ,v655 ,v656 ,v657 ,v658 ,v659 ,v660 ,v661 ,v662 ,v663 ,v664 ,v665 ,v666 ,v667 ,v668 ,v669 ,v670 ,v671 ,v672 ,v673 ,v674 ,v675 ,v676 ,v677,  ");
            sqlQuery.Append(" v678 ,v679 ,v680 ,v681 ,v682 ,v683 ,v684 ,v685 ,v686 ,v687 ,v688 ,v689 ,v690 ,v691 ,v692 ,v693 ,v694 ,v695 ,v696 ,v697 ,v698 ,v699 ,v700 ,v701,  ");
            sqlQuery.Append(" v702 ,v703 ,v704 ,v705 ,v706 ,v707  ");
            sqlQuery.Append(" FROM sep_911.dbo.VW_EI_NE1  ");
            sqlQuery.Append("WHERE ID_Control = @ID_Control ");
            #endregion
            com = new SqlCommand(sqlQuery.ToString(), cnn);
            da = new SqlDataAdapter(com);
            com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
            da.Fill(Ds911_EI_NE_1.VW_EI_NE1);
            #endregion

            classRpts.LoadReporte("estadisticaEINE1.rpt");
            classRpts.DsDatos = Ds911_EI_NE_1;

            byte[] PDF_byte = classRpts.GenerarEnMemoria(0, 0);

            ImprimeReporte(PDF_byte);
        }

        #endregion




        #region PDF 911.2
        //private void pdf_911_2(ControlDP controlDP, bool Lleno)
        //{
        //    ClassRpts classRpts = new ClassRpts();

        //    SqlConnection cnn = cnn_DB911();
        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_2 Ds911_2 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_2();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append("SELECT ID_Control, ");
        //    sqlQuery.Append("id_CCTNT, ");
        //    sqlQuery.Append("Id_CentroTrabajo, ");
        //    sqlQuery.Append("id_turno, ");
        //    sqlQuery.Append("id_nivel, ");
        //    sqlQuery.Append("id_subnivel, ");
        //    sqlQuery.Append("Clave, ");
        //    sqlQuery.Append("CNT_id_turno, ");
        //    sqlQuery.Append("Nombre, ");
        //    sqlQuery.Append("Domicilio, ");
        //    sqlQuery.Append("Colonia, ");
        //    sqlQuery.Append("Municipio, ");
        //    sqlQuery.Append("Entidad, ");
        //    sqlQuery.Append("DependenciaNormativa, ");
        //    sqlQuery.Append("Servicio, ");
        //    sqlQuery.Append("Sostenimiento, ");
        //    sqlQuery.Append("Director, ");
        //    sqlQuery.Append("Telefono, ");
        //    sqlQuery.Append("Fax, ");
        //    sqlQuery.Append("Codigo_Postal, ");
        //    sqlQuery.Append("Sector, ");
        //    sqlQuery.Append("Zona, ");
        //    sqlQuery.Append("Region, ");
        //    sqlQuery.Append("CicloActual ");
        //    sqlQuery.Append("FROM dbo.DatosIdentificacionPDF ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(Ds911_2.DatosIdentificacionPDF);
        //    #endregion

        //    #region datos ANEXO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_ANEXO
        //    sqlQuery.Append(" SELECT ID_Control, ID_CentroTrabajo, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ");
        //    sqlQuery.Append(" ID_Zona, ID_Sector, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones,");
        //    sqlQuery.Append(" Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx26, Anx27,   Anx28, Anx29, ");
        //    sqlQuery.Append(" Anx30, Anx31, Anx32, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx42, Anx43, Anx44, Anx45, ");
        //    sqlQuery.Append(" Anx46, Anx47, Anx48, Anx49, Anx50, Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx57, Anx58, Anx59, Anx60, Anx61, ");
        //    sqlQuery.Append(" Anx62, Anx63, Anx64, Anx65, Anx66, Anx67,  Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, Anx76, Anx77, ");
        //    sqlQuery.Append(" Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx84, Anx85, Anx86, Anx87, Anx88, Anx89, Anx90, Anx91, Anx92, Anx93, ");
        //    sqlQuery.Append(" Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, Anx104, Anx105, Anx106, Anx107, Anx108, Anx109, ");
        //    sqlQuery.Append(" Anx110, Anx111,   Anx112, Anx113, Anx114, Anx115,   Anx116, Anx117, Anx118, Anx119, Anx120, Anx121, Anx122, Anx123, Anx124, Anx125, ");
        //    sqlQuery.Append(" Anx126, Anx127, Anx128, Anx129, Anx130, Anx131, Anx132, Anx133, Anx134, Anx135, Anx136, Anx137, Anx138, Anx139, Anx140, Anx141, ");
        //    sqlQuery.Append(" Anx142, Anx143, Anx144, Anx145, Anx146, Anx147, Anx148, Anx149, Anx150, Anx151,   Anx152, Anx153, Anx154, Anx155, Anx156, ");
        //    sqlQuery.Append(" Anx157, Anx158, Anx159, Anx160, Anx161, Anx162, Anx163, Anx164, Anx165, Anx166, Anx167, Anx168, Anx169, Anx170, Anx171, Anx172, ");
        //    sqlQuery.Append(" Anx173, Anx174, Anx175, Anx176, Anx177, Anx178, Anx179, Anx180, Anx181, Anx182, Anx183, Anx184, Anx185, Anx186,   Anx187, ");
        //    sqlQuery.Append(" Anx188,   Anx189, Anx190, Anx191, Anx192, Anx193, Anx194, Anx195, Anx196, Anx197, Anx198, Anx199, Anx200, Anx201, Anx202, Anx203, Anx204, Anx205, ");
        //    sqlQuery.Append(" Anx206, Anx207, Anx208, Anx209, Anx210, Anx211, Anx212, Anx213, Anx214, Anx215, Anx216, Anx217, Anx218, Anx219, Anx220, Anx221, ");
        //    sqlQuery.Append(" Anx222,   Anx223, Anx224, Anx225, Anx226, Anx227, Anx228, Anx229, Anx230, Anx231, Anx232, Anx233, Anx234, Anx235, Anx236, Anx237, ");
        //    sqlQuery.Append(" Anx238, Anx239, Anx240, Anx241, Anx242, Anx243, Anx244, Anx245, Anx246, Anx247, Anx248, Anx249, Anx250, Anx251, Anx252, Anx253, ");
        //    sqlQuery.Append(" Anx254, Anx255, Anx256, Anx257, Anx258, Anx259, Anx260, Anx261, Anx262, Anx263, Anx264, Anx265, Anx266, Anx267, Anx268, Anx269, ");
        //    sqlQuery.Append(" Anx270, Anx271, Anx272, Anx273, Anx274, Anx275, Anx276, Anx277, Anx278, Anx279, Anx280, Anx281, Anx282, Anx283, Anx284, Anx285, ");
        //    sqlQuery.Append(" Anx286, Anx287, Anx288, Anx289, Anx290, Anx291, Anx292, Anx293, Anx294, Anx295, Anx296, Anx297, Anx298, Anx299, ");
        //    sqlQuery.Append(" Anx300, Anx301, Anx302, Anx303, Anx304, Anx305, Anx306, Anx307, Anx308, Anx309, Anx310, Anx311, Anx312, Anx313, Anx314, Anx315, ");
        //    sqlQuery.Append(" Anx316, Anx317, Anx318, Anx319, Anx320, Anx321, Anx322, Anx323, Anx324, Anx325, Anx326, Anx327, Anx328, Anx329, Anx330,   Anx331, ");
        //    sqlQuery.Append(" Anx332, Anx645, Anx646, Anx647, Anx648, Anx649, Anx650, Anx651, Anx652, Anx653, Anx654, Anx655, Anx656, Anx657, GETDATE() as Anx658, Anx659, ");
        //    sqlQuery.Append(" Anx660, Anx661, Anx662, Anx663    FROM dbo.VW_Anexo ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_2.VW_Anexo);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_PREESCOLAR_G_F
        //    sqlQuery.Append(" SELECT ID_Control, v1,v2, v3,v4, v5,v6, v7,v8, v9,v10, v11,v12, v13,v14, v15,v16, v17,v18, v19,v20, ");
        //    sqlQuery.Append(" v21,v22, v23,v24, v25,v26, v27,v28, v29,v30, v31,v32, v33, v34, v35, v36, v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v50,  ");
        //    sqlQuery.Append(" v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79,  ");
        //    sqlQuery.Append(" v80, v81, v82, v83, v84, v85, v86, v87, v88, v89, v90, v91, v92, v93, v94, v95, v96, v97, v98, v99, v100, v101, v102, v103, v104, v105, v106, v107,  ");
        //    sqlQuery.Append(" v108, v109, v110, v111, v112, v113, v114, v115, v116, v117, v118, v119, v120, v121, v122, v123, v124, v125, v126, v127, v128, v129, v130, v131, v132,  ");
        //    sqlQuery.Append(" v133, v134, v135, v136, v137, v138, v139, v140, v141, v142, v143, v144, v145, v146, v147, v148, v149, v150, v151, v152, v153, v154, v155, v156, v157,  ");
        //    sqlQuery.Append(" v158, v159, v160, v161, v162, v163, v164, v165, v166, v167, v168, v169, v170, v171, v172, v173, v174, v175, v176, v177, v178, v179, v180, v181, v182,  ");
        //    sqlQuery.Append(" v183, v184, v185, v186, v187, v188, v189, v190, v191, v192, v193, v194, v195, v196, v197, v198, v199, v200, v201, v202, v203, v204, v205, v206, v207,  ");
        //    sqlQuery.Append(" v208, v209, v210, v211, v212, v213, v214, v215, v216, v217, v218, v219, v220, v221, v222, v223, v224, v225, v226, v227, v228, v229, v230, v231, v232,  ");
        //    sqlQuery.Append(" v233, v234, v235, v236, v237, v238, V239,V240  ");
        //    sqlQuery.Append(" FROM dbo.VW_Preescolar_G_F ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_2.VW_Preescolar_G_F);
        //    #endregion

        //    classRpts.LoadReporte("911_2.rpt");
        //    classRpts.DsDatos = Ds911_2;

        //    #region Datos INMUEBLE
        //    ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
        //    SEroot.WsEstadisticasEducativas.InmuebleDP inmuebleDP = wsEstadisticas.CargaCuestionarioInmueble(controlDP.ID_Control);
        //    if (inmuebleDP != null && Lleno)
        //    {
        //        classRpts.Add_Parametro("A_si", X(inmuebleDP.a));
        //        classRpts.Add_Parametro("A_no", X(!inmuebleDP.a));
        //        classRpts.Add_Parametro("B_si", X(inmuebleDP.b));
        //        classRpts.Add_Parametro("B_no", X(!inmuebleDP.b));
        //        classRpts.Add_Parametro("C_si", X(inmuebleDP.c));
        //        classRpts.Add_Parametro("C_no", X(!inmuebleDP.c));
        //        classRpts.Add_Parametro("D_si", X(inmuebleDP.d));
        //        classRpts.Add_Parametro("D_no", X(!inmuebleDP.d));
        //        classRpts.Add_Parametro("E_si", X(inmuebleDP.e));
        //        classRpts.Add_Parametro("E_no", X(!inmuebleDP.e));
        //        classRpts.Add_Parametro("Especifique", inmuebleDP.especifique);

        //    }
        //    else
        //    {
        //        classRpts.Add_Parametro("A_si", "");
        //        classRpts.Add_Parametro("A_no", "");
        //        classRpts.Add_Parametro("B_si", "");
        //        classRpts.Add_Parametro("B_no", "");
        //        classRpts.Add_Parametro("C_si", "");
        //        classRpts.Add_Parametro("C_no", "");
        //        classRpts.Add_Parametro("D_si", "");
        //        classRpts.Add_Parametro("D_no", "");
        //        classRpts.Add_Parametro("E_si", "");
        //        classRpts.Add_Parametro("E_no", "");
        //        classRpts.Add_Parametro("Especifique", "");
        //    }
        //    #endregion


        //    byte[] PDF_byte = classRpts.GenerarEnMemoria();

        //    ImprimeReporte(PDF_byte);
        //}
        //private void pdf_911_2_Oficializar(ControlDP controlDP)
        //{
        //    ClassRpts classRpts = new ClassRpts();

        //    SqlConnection cnn = cnn_DB911();
        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_2 Ds911_2 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_2();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append("SELECT ID_Control, ");
        //    sqlQuery.Append("id_CCTNT, ");
        //    sqlQuery.Append("Id_CentroTrabajo, ");
        //    sqlQuery.Append("id_turno, ");
        //    sqlQuery.Append("id_nivel, ");
        //    sqlQuery.Append("id_subnivel, ");
        //    sqlQuery.Append("Clave, ");
        //    sqlQuery.Append("CNT_id_turno, ");
        //    sqlQuery.Append("Nombre, ");
        //    sqlQuery.Append("Domicilio, ");
        //    sqlQuery.Append("Colonia, ");
        //    sqlQuery.Append("Municipio, ");
        //    sqlQuery.Append("Entidad, ");
        //    sqlQuery.Append("DependenciaNormativa, ");
        //    sqlQuery.Append("Servicio, ");
        //    sqlQuery.Append("Sostenimiento, ");
        //    sqlQuery.Append("Director, ");
        //    sqlQuery.Append("Telefono, ");
        //    sqlQuery.Append("Fax, ");
        //    sqlQuery.Append("Codigo_Postal, ");
        //    sqlQuery.Append("Sector, ");
        //    sqlQuery.Append("Zona, ");
        //    sqlQuery.Append("Region, ");
        //    sqlQuery.Append("CicloActual ");
        //    sqlQuery.Append("FROM dbo.DatosIdentificacionPDF ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(Ds911_2.DatosIdentificacionPDF);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_PREESCOLAR_G_F
        //    sqlQuery.Append(" SELECT ID_Control, v1,v2, v3,v4, v5,v6, v7,v8, v9,v10, v11,v12, v13,v14, v15,v16, v17,v18, v19,v20, ");
        //    sqlQuery.Append(" v21,v22, v23,v24, v25,v26, v27,v28, v29,v30, v31,v32, v33, v34, v35, v36, v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v50,  ");
        //    sqlQuery.Append(" v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79,  ");
        //    sqlQuery.Append(" v80, v81, v82, v83, v84, v85, v86, v87, v88, v89, v90, v91, v92, v93, v94, v95, v96, v97, v98, v99, v100, v101, v102, v103, v104, v105, v106, v107,  ");
        //    sqlQuery.Append(" v108, v109, v110, v111, v112, v113, v114, v115, v116, v117, v118, v119, v120, v121, v122, v123, v124, v125, v126, v127, v128, v129, v130, v131, v132,  ");
        //    sqlQuery.Append(" v133, v134, v135, v136, v137, v138, v139, v140, v141, v142, v143, v144, v145, v146, v147, v148, v149, v150, v151, v152, v153, v154, v155, v156, v157,  ");
        //    sqlQuery.Append(" v158, v159, v160, v161, v162, v163, v164, v165, v166, v167, v168, v169, v170, v171, v172, v173, v174, v175, v176, v177, v178, v179, v180, v181, v182,  ");
        //    sqlQuery.Append(" v183, v184, v185, v186, v187, v188, v189, v190, v191, v192, v193, v194, v195, v196, v197, v198, v199, v200, v201, v202, v203, v204, v205, v206, v207,  ");
        //    sqlQuery.Append(" v208, v209, v210, v211, v212, v213, v214, v215, v216, v217, v218, v219, v220, v221, v222, v223, v224, v225, v226, v227, v228, v229, v230, v231, v232,  ");
        //    sqlQuery.Append(" v233, v234, v235, v236, v237, v238, V239, V240, TipoOficializacion ");
        //    sqlQuery.Append(" FROM dbo.VW_Preescolar_G_F ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_2.VW_Preescolar_G_F);
        //    #endregion

        //    classRpts.LoadReporte("estadisticaPre.rpt");
        //    classRpts.DsDatos = Ds911_2;

        //    byte[] PDF_byte = classRpts.GenerarEnMemoria();

        //    ImprimeReporte(PDF_byte);
        //}

        #endregion

        #region PDF 911.4
        //private void pdf_911_4(ControlDP controlDP, bool Lleno)
        //{
        //    ClassRpts classRpts = new ClassRpts();
        //    SqlConnection cnn = cnn_DB911();
        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_4 Ds911_4 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_4();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append("SELECT ID_Control, ");
        //    sqlQuery.Append("id_CCTNT, ");
        //    sqlQuery.Append("Id_CentroTrabajo, ");
        //    sqlQuery.Append("id_turno, ");
        //    sqlQuery.Append("id_nivel, ");
        //    sqlQuery.Append("id_subnivel, ");
        //    sqlQuery.Append("Clave, ");
        //    sqlQuery.Append("CNT_id_turno, ");
        //    sqlQuery.Append("Nombre, ");
        //    sqlQuery.Append("Domicilio, ");
        //    sqlQuery.Append("Colonia, ");
        //    sqlQuery.Append("Municipio, ");
        //    sqlQuery.Append("Entidad, ");
        //    sqlQuery.Append("DependenciaNormativa, ");
        //    sqlQuery.Append("Servicio, ");
        //    sqlQuery.Append("Sostenimiento, ");
        //    sqlQuery.Append("Director, ");
        //    sqlQuery.Append("Telefono, ");
        //    sqlQuery.Append("Fax, ");
        //    sqlQuery.Append("Codigo_Postal, ");
        //    sqlQuery.Append("Sector, ");
        //    sqlQuery.Append("Zona, ");
        //    sqlQuery.Append("Region, ");
        //    sqlQuery.Append("CicloActual ");
        //    sqlQuery.Append("FROM dbo.DatosIdentificacionPDF ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(Ds911_4.DatosIdentificacionPDF);
        //    #endregion

        //    #region datos ANEXO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_ANEXO
        //    sqlQuery.Append(" SELECT ID_Control, ID_CentroTrabajo, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ");
        //    sqlQuery.Append(" ID_Zona, ID_Sector, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones,");
        //    sqlQuery.Append(" Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx26, Anx27,   Anx28, Anx29, ");
        //    sqlQuery.Append(" Anx30, Anx31, Anx32, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx42, Anx43, Anx44, Anx45, ");
        //    sqlQuery.Append(" Anx46, Anx47, Anx48, Anx49, Anx50, Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx57, Anx58, Anx59, Anx60, Anx61, ");
        //    sqlQuery.Append(" Anx62, Anx63, Anx64, Anx65, Anx66, Anx67,  Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, Anx76, Anx77, ");
        //    sqlQuery.Append(" Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx84, Anx85, Anx86, Anx87, Anx88, Anx89, Anx90, Anx91, Anx92, Anx93, ");
        //    sqlQuery.Append(" Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, Anx104, Anx105, Anx106, Anx107, Anx108, Anx109, ");
        //    sqlQuery.Append(" Anx110, Anx111,   Anx112, Anx113, Anx114, Anx115,   Anx116, Anx117, Anx118, Anx119, Anx120, Anx121, Anx122, Anx123, Anx124, Anx125, ");
        //    sqlQuery.Append(" Anx126, Anx127, Anx128, Anx129, Anx130, Anx131, Anx132, Anx133, Anx134, Anx135, Anx136, Anx137, Anx138, Anx139, Anx140, Anx141, ");
        //    sqlQuery.Append(" Anx142, Anx143, Anx144, Anx145, Anx146, Anx147, Anx148, Anx149, Anx150, Anx151,   Anx152, Anx153, Anx154, Anx155, Anx156, ");
        //    sqlQuery.Append(" Anx157, Anx158, Anx159, Anx160, Anx161, Anx162, Anx163, Anx164, Anx165, Anx166, Anx167, Anx168, Anx169, Anx170, Anx171, Anx172, ");
        //    sqlQuery.Append(" Anx173, Anx174, Anx175, Anx176, Anx177, Anx178, Anx179, Anx180, Anx181, Anx182, Anx183, Anx184, Anx185, Anx186,   Anx187, ");
        //    sqlQuery.Append(" Anx188,   Anx189, Anx190, Anx191, Anx192, Anx193, Anx194, Anx195, Anx196, Anx197, Anx198, Anx199, Anx200, Anx201, Anx202, Anx203, Anx204, Anx205, ");
        //    sqlQuery.Append(" Anx206, Anx207, Anx208, Anx209, Anx210, Anx211, Anx212, Anx213, Anx214, Anx215, Anx216, Anx217, Anx218, Anx219, Anx220, Anx221, ");
        //    sqlQuery.Append(" Anx222,   Anx223, Anx224, Anx225, Anx226, Anx227, Anx228, Anx229, Anx230, Anx231, Anx232, Anx233, Anx234, Anx235, Anx236, Anx237, ");
        //    sqlQuery.Append(" Anx238, Anx239, Anx240, Anx241, Anx242, Anx243, Anx244, Anx245, Anx246, Anx247, Anx248, Anx249, Anx250, Anx251, Anx252, Anx253, ");
        //    sqlQuery.Append(" Anx254, Anx255, Anx256, Anx257, Anx258, Anx259, Anx260, Anx261, Anx262, Anx263, Anx264, Anx265, Anx266, Anx267, Anx268, Anx269, ");
        //    sqlQuery.Append(" Anx270, Anx271, Anx272, Anx273, Anx274, Anx275, Anx276, Anx277, Anx278, Anx279, Anx280, Anx281, Anx282, Anx283, Anx284, Anx285, ");
        //    sqlQuery.Append(" Anx286, Anx287, Anx288, Anx289, Anx290, Anx291, Anx292, Anx293, Anx294, Anx295, Anx296, Anx297, Anx298, Anx299, ");
        //    sqlQuery.Append(" Anx300, Anx301, Anx302, Anx303, Anx304, Anx305, Anx306, Anx307, Anx308, Anx309, Anx310, Anx311, Anx312, Anx313, Anx314, Anx315, ");
        //    sqlQuery.Append(" Anx316, Anx317, Anx318, Anx319, Anx320, Anx321, Anx322, Anx323, Anx324, Anx325, Anx326, Anx327, Anx328, Anx329, Anx330,   Anx331, ");
        //    sqlQuery.Append(" Anx332, Anx645, Anx646, Anx647, Anx648, Anx649, Anx650, Anx651, Anx652, Anx653, Anx654, Anx655, Anx656, Anx657, GETDATE() as Anx658, Anx659, ");
        //    sqlQuery.Append(" Anx660, Anx661, Anx662, Anx663    FROM dbo.VW_Anexo ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_4.VW_Anexo);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_Primaria_G_F
        //    sqlQuery.Append("SELECT ID_Control, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, ");
        //    sqlQuery.Append("v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40, v41, v42, v43, v44, ");
        //    sqlQuery.Append("v45, v46, v47, v48, v49, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, v61, v62, v63, v64, v65, v66, v67, ");
        //    sqlQuery.Append("v68, v69, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79, v80, v81, v82, v83, v84, v85, v86, v87, v88, v89, v90, ");
        //    sqlQuery.Append("v91, v92, v93, v94, v95, v96, v97, v98, v99, v100, v101, v102, v103, v104, v105, v106, v107, v108, v109, v110, v111, ");
        //    sqlQuery.Append("v112, v113, v114, v115, v116, v117, v118, v119, v120, v121, v122, v123, v124, v125, v126, v127, v128, v129, v130, v131, ");
        //    sqlQuery.Append("v132, v133, v134, v135, v136, v137, v138, v139, v140, v141, v142, v143, v144, v145, v146, v147, v148, v149, v150, v151, ");
        //    sqlQuery.Append("v152, v153, v154, v155, v156, v157, v158, v159, v160, v161, v162, v163, v164, v165, v166, v167, v168, v169, v170, v171, ");
        //    sqlQuery.Append("v172, v173, v174, v175, v176, v177, v178, v179, v180, v181, v182, v183, v184, v185, v186, v187, v188, v189, v190, v191, ");
        //    sqlQuery.Append("v192, v193, v194, v195, v196, v197, v198, v199, v200, v201, v202, v203, v204, v205, v206, v207, v208, v209, v210, v211, ");
        //    sqlQuery.Append("v212, v213, v214, v215, v216, v217, v218, v219, v220, v221, v222, v223, v224, v225, v226, v227, v228, v229, v230, v231, ");
        //    sqlQuery.Append("v232, v233, v234, v235, v236, v237, v238, v239, v240, v241, v242, v243, v244, v245, v246, v247, v248, v249, v250, v251, ");
        //    sqlQuery.Append("v252, v253, v254, v255, v256, v257, v258, v259, v260, v261, v262, v263, v264, v265, v266, v267, v268, v269, v270, v271, ");
        //    sqlQuery.Append("v272, v273, v274, v275, v276, v277, v278, v279, v280, v281, v282, v283, v284, v285, v286, v287, v288, v289, v290, v291, ");
        //    sqlQuery.Append("v292, v293, v294, v295, v296, v297, v298, v299, v300, v301, v302, v303, v304, v305, v306, v307, v308, v309, v310, v311, ");
        //    sqlQuery.Append("v312, v313, v314, v315, v316, v317, v318, v319, v320, v321, v322, v323, v324, v325, v326, v327, v328, v329, v330, v331, ");
        //    sqlQuery.Append("v332, v333, v334, v335, v336, v337, v338, v339, v340, v341, v342, v343, v344, v345, v346, v347, v348, v349, v350, v351, ");
        //    sqlQuery.Append("v352, v353, v354, v355, v356, v357, v358, v359, v360, v361, v362, v363, v364, v365, v366, v367, v368, v369, v370, v371, ");
        //    sqlQuery.Append("v372, v373, v374, v375, v376, v377, v378, v379, v380, v381, v382, v383, v384, v385, v386, v387, v388, v389, v390, v391, ");
        //    sqlQuery.Append("v392, v393, v394, v395, v396, v397, v398, v399, v400, v401, v402, v403, v404, v405, v406, v407, v408, v409, v410, v411, ");
        //    sqlQuery.Append("v412, v413, v414, v415, v416, v417, v418, v419, v420, v421, v422, v423, v424, v425, v426, v427, v428, v429, v430, v431, ");
        //    sqlQuery.Append("v432, v433, v434, v435, v436, v437, v438, v439, v440, v441, v442, v443, v444, v445, v446, v447, v448, v449, v450, v451, ");
        //    sqlQuery.Append("v452, v453, v454, v455, v456, v457, v458, v459, v460, v461, v462, v463, v464, v465, v466, v467, v468, v469, v470, v471, ");
        //    sqlQuery.Append("v472, v473, v474, v475, v476, v477, v478, v479, v480, v481, v482, v483, v484, v485, v486, v487, v488, v489, v490, v491, ");
        //    sqlQuery.Append("v492, v493, v494, v495, v496, v497, v498, v499, v500, v501, v502, v503, v504, v505, v506, v507, v508, v509, v510, v511, ");
        //    sqlQuery.Append("v512, v513, v514, v515, v516, v517, v518, v519, v520, v521, v522, v523, v524, v525, v526, v527, v528, v529, v530, v531, ");
        //    sqlQuery.Append("v532, v533, v534, v535, v536, v537, v538, v539, v540, v541, v542, v543, v544, v545, v546, v547, v548, v549, v550, v551, ");
        //    sqlQuery.Append("v552, v553, v554, v555, v556, v557, v558, v559, v560, v561, v562, v563, v564, v565, v566, v567, v568, v569, v570, v571, ");
        //    sqlQuery.Append("v572, v573, v574, v575, v576, v577, v578, v579, v580, v581, v582, v583, v584, v585, v586, v587, v588, v589, v590, v591, ");
        //    sqlQuery.Append("v592, v593, v594, v595, v596, v597, v598, v599, v600, v601, v602, v603, v604, v605, v606, v607, v608, v609, v610, v611, ");
        //    sqlQuery.Append("v612, v613, v614, v615, v616, v617, v618, v619, v620, v621, v622, v623, v624, v625, v626, v627, v628, v629, v630, v631, ");
        //    sqlQuery.Append("v632, v633, v634, v635, v636, v637, v638, v639, v640, v641, v642, v643, v644, v645, v646, v647, v648, v649, v650, v651, ");
        //    sqlQuery.Append("v652, v653, v654, v655, v656, v657, v658, v659, v660, v661, v662, v663, v664, v665, v666, v667, v668, v669, v670, v671, ");
        //    sqlQuery.Append("v672, v673, v674, v675, v676, v677, v678, v679, v680, v681, v682, v683, v684 FROM dbo.VW_Primaria_G_F  ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_4.VW_Primaria_G_F);
        //    #endregion
        //    //classRpts.LoadReporte("911_4.rpt");
        //    //classRpts.DsDatos = Ds911_4;

        //    //classRpts.rpt.Subreports[0].SetDataSource(Ds911_4);

        //    Reportes.Crystals.Crystal_Report1911_4 rpt9_11 = new EstadisticasEducativas._911.fin.Reportes.Crystals.Crystal_Report1911_4();
        //    rpt9_11.ReportOptions.EnableSaveDataWithReport = false;
        //    rpt9_11.SetDataSource(Ds911_4);
        //    rpt9_11.Subreports[0].SetDataSource(Ds911_4);

        //    #region Datos INMUEBLE
        //    ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
        //    SEroot.WsEstadisticasEducativas.InmuebleDP inmuebleDP = wsEstadisticas.CargaCuestionarioInmueble(controlDP.ID_Control);


        //    //rpt9_11.Subreports[0].SetParameterValue

        //    if (inmuebleDP != null && Lleno)
        //    {
        //        rpt9_11.SetParameterValue("A_si", X(inmuebleDP.a), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("A_no", X(!inmuebleDP.a), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("B_si", X(inmuebleDP.b), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("B_no", X(!inmuebleDP.b), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("C_si", X(inmuebleDP.c), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("C_no", X(!inmuebleDP.c), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("D_si", X(inmuebleDP.d), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("D_no", X(!inmuebleDP.d), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("E_si", X(inmuebleDP.e), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("E_no", X(!inmuebleDP.e), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("Especifique", inmuebleDP.especifique, "Anexo.rpt");

        //    }
        //    else
        //    {
        //        rpt9_11.SetParameterValue("A_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("A_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("B_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("B_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("C_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("C_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("D_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("D_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("E_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("E_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("Especifique", "", "Anexo.rpt");
        //    }
        //    #endregion
        //    rpt9_11.SetParameterValue("CicloActual", Ds911_4.DatosIdentificacionPDF[0].CicloActual, "Anexo.rpt");
        //    rpt9_11.SetParameterValue("Cuestionario", "911_4", "Anexo.rpt");
        //    rpt9_11.SetParameterValue("Clave", controlDP.Clave, "Anexo.rpt");
        //    rpt9_11.SetParameterValue("Turno", controlDP.ID_Turno.ToString(), "Anexo.rpt");
        //    rpt9_11.SetParameterValue("Nombre", controlDP.Nombre, "Anexo.rpt");

        //    byte[] PDF_byte = ConvertStreamToByteBuffer(rpt9_11.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
        //    rpt9_11.Close();

        //    ImprimeReporte(PDF_byte);

        //}
        //private void pdf_911_4_Oficializar(ControlDP controlDP)
        //{
        //    ClassRpts classRpts = new ClassRpts();
        //    SqlConnection cnn = cnn_DB911();

        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_4 Ds911_4 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_4();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append("SELECT ID_Control, ");
        //    sqlQuery.Append("id_CCTNT, ");
        //    sqlQuery.Append("Id_CentroTrabajo, ");
        //    sqlQuery.Append("id_turno, ");
        //    sqlQuery.Append("id_nivel, ");
        //    sqlQuery.Append("id_subnivel, ");
        //    sqlQuery.Append("Clave, ");
        //    sqlQuery.Append("CNT_id_turno, ");
        //    sqlQuery.Append("Nombre, ");
        //    sqlQuery.Append("Domicilio, ");
        //    sqlQuery.Append("Colonia, ");
        //    sqlQuery.Append("Municipio, ");
        //    sqlQuery.Append("Entidad, ");
        //    sqlQuery.Append("DependenciaNormativa, ");
        //    sqlQuery.Append("Servicio, ");
        //    sqlQuery.Append("Sostenimiento, ");
        //    sqlQuery.Append("Director, ");
        //    sqlQuery.Append("Telefono, ");
        //    sqlQuery.Append("Fax, ");
        //    sqlQuery.Append("Codigo_Postal, ");
        //    sqlQuery.Append("Sector, ");
        //    sqlQuery.Append("Zona, ");
        //    sqlQuery.Append("Region, ");
        //    sqlQuery.Append("CicloActual ");
        //    sqlQuery.Append("FROM dbo.DatosIdentificacionPDF ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(Ds911_4.DatosIdentificacionPDF);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_Primaria_G_F
        //    sqlQuery.Append("SELECT ID_Control, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, ");
        //    sqlQuery.Append("v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v40, v41, v42, v43, v44, ");
        //    sqlQuery.Append("v45, v46, v47, v48, v49, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, v61, v62, v63, v64, v65, v66, v67, ");
        //    sqlQuery.Append("v68, v69, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79, v80, v81, v82, v83, v84, v85, v86, v87, v88, v89, v90, ");
        //    sqlQuery.Append("v91, v92, v93, v94, v95, v96, v97, v98, v99, v100, v101, v102, v103, v104, v105, v106, v107, v108, v109, v110, v111, ");
        //    sqlQuery.Append("v112, v113, v114, v115, v116, v117, v118, v119, v120, v121, v122, v123, v124, v125, v126, v127, v128, v129, v130, v131, ");
        //    sqlQuery.Append("v132, v133, v134, v135, v136, v137, v138, v139, v140, v141, v142, v143, v144, v145, v146, v147, v148, v149, v150, v151, ");
        //    sqlQuery.Append("v152, v153, v154, v155, v156, v157, v158, v159, v160, v161, v162, v163, v164, v165, v166, v167, v168, v169, v170, v171, ");
        //    sqlQuery.Append("v172, v173, v174, v175, v176, v177, v178, v179, v180, v181, v182, v183, v184, v185, v186, v187, v188, v189, v190, v191, ");
        //    sqlQuery.Append("v192, v193, v194, v195, v196, v197, v198, v199, v200, v201, v202, v203, v204, v205, v206, v207, v208, v209, v210, v211, ");
        //    sqlQuery.Append("v212, v213, v214, v215, v216, v217, v218, v219, v220, v221, v222, v223, v224, v225, v226, v227, v228, v229, v230, v231, ");
        //    sqlQuery.Append("v232, v233, v234, v235, v236, v237, v238, v239, v240, v241, v242, v243, v244, v245, v246, v247, v248, v249, v250, v251, ");
        //    sqlQuery.Append("v252, v253, v254, v255, v256, v257, v258, v259, v260, v261, v262, v263, v264, v265, v266, v267, v268, v269, v270, v271, ");
        //    sqlQuery.Append("v272, v273, v274, v275, v276, v277, v278, v279, v280, v281, v282, v283, v284, v285, v286, v287, v288, v289, v290, v291, ");
        //    sqlQuery.Append("v292, v293, v294, v295, v296, v297, v298, v299, v300, v301, v302, v303, v304, v305, v306, v307, v308, v309, v310, v311, ");
        //    sqlQuery.Append("v312, v313, v314, v315, v316, v317, v318, v319, v320, v321, v322, v323, v324, v325, v326, v327, v328, v329, v330, v331, ");
        //    sqlQuery.Append("v332, v333, v334, v335, v336, v337, v338, v339, v340, v341, v342, v343, v344, v345, v346, v347, v348, v349, v350, v351, ");
        //    sqlQuery.Append("v352, v353, v354, v355, v356, v357, v358, v359, v360, v361, v362, v363, v364, v365, v366, v367, v368, v369, v370, v371, ");
        //    sqlQuery.Append("v372, v373, v374, v375, v376, v377, v378, v379, v380, v381, v382, v383, v384, v385, v386, v387, v388, v389, v390, v391, ");
        //    sqlQuery.Append("v392, v393, v394, v395, v396, v397, v398, v399, v400, v401, v402, v403, v404, v405, v406, v407, v408, v409, v410, v411, ");
        //    sqlQuery.Append("v412, v413, v414, v415, v416, v417, v418, v419, v420, v421, v422, v423, v424, v425, v426, v427, v428, v429, v430, v431, ");
        //    sqlQuery.Append("v432, v433, v434, v435, v436, v437, v438, v439, v440, v441, v442, v443, v444, v445, v446, v447, v448, v449, v450, v451, ");
        //    sqlQuery.Append("v452, v453, v454, v455, v456, v457, v458, v459, v460, v461, v462, v463, v464, v465, v466, v467, v468, v469, v470, v471, ");
        //    sqlQuery.Append("v472, v473, v474, v475, v476, v477, v478, v479, v480, v481, v482, v483, v484, v485, v486, v487, v488, v489, v490, v491, ");
        //    sqlQuery.Append("v492, v493, v494, v495, v496, v497, v498, v499, v500, v501, v502, v503, v504, v505, v506, v507, v508, v509, v510, v511, ");
        //    sqlQuery.Append("v512, v513, v514, v515, v516, v517, v518, v519, v520, v521, v522, v523, v524, v525, v526, v527, v528, v529, v530, v531, ");
        //    sqlQuery.Append("v532, v533, v534, v535, v536, v537, v538, v539, v540, v541, v542, v543, v544, v545, v546, v547, v548, v549, v550, v551, ");
        //    sqlQuery.Append("v552, v553, v554, v555, v556, v557, v558, v559, v560, v561, v562, v563, v564, v565, v566, v567, v568, v569, v570, v571, ");
        //    sqlQuery.Append("v572, v573, v574, v575, v576, v577, v578, v579, v580, v581, v582, v583, v584, v585, v586, v587, v588, v589, v590, v591, ");
        //    sqlQuery.Append("v592, v593, v594, v595, v596, v597, v598, v599, v600, v601, v602, v603, v604, v605, v606, v607, v608, v609, v610, v611, ");
        //    sqlQuery.Append("v612, v613, v614, v615, v616, v617, v618, v619, v620, v621, v622, v623, v624, v625, v626, v627, v628, v629, v630, v631, ");
        //    sqlQuery.Append("v632, v633, v634, v635, v636, v637, v638, v639, v640, v641, v642, v643, v644, v645, v646, v647, v648, v649, v650, v651, ");
        //    sqlQuery.Append("v652, v653, v654, v655, v656, v657, v658, v659, v660, v661, v662, v663, v664, v665, v666, v667, v668, v669, v670, v671, ");
        //    sqlQuery.Append("v672, v673, v674, v675, v676, v677, v678, v679, v680, v681, v682, v683, v684, TipoOficializacion  FROM dbo.VW_Primaria_G_F  ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_4.VW_Primaria_G_F);
        //    #endregion

        //    if (controlDP.ID_SubNivel == 10)
        //    {
        //        classRpts.LoadReporte("estadisticaPrim_DTV.rpt");
        //    }
        //    else
        //    {
        //        classRpts.LoadReporte("estadisticaPrim.rpt");
        //    }

        //    classRpts.DsDatos = Ds911_4;

        //    byte[] PDF_byte = classRpts.GenerarEnMemoria();

        //    ImprimeReporte(PDF_byte);
        //}
        #endregion

        #region PDF 911.6
        //private void pdf_911_6(ControlDP controlDP, bool Lleno)
        //{
        //    ClassRpts classRpts = new ClassRpts();
        //    SqlConnection cnn = cnn_DB911();
        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_6 Ds911_6 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_6();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append("SELECT ID_Control, ");
        //    sqlQuery.Append("id_CCTNT, ");
        //    sqlQuery.Append("Id_CentroTrabajo, ");
        //    sqlQuery.Append("id_turno, ");
        //    sqlQuery.Append("id_nivel, ");
        //    sqlQuery.Append("id_subnivel, ");
        //    sqlQuery.Append("Clave, ");
        //    sqlQuery.Append("CNT_id_turno, ");
        //    sqlQuery.Append("Nombre, ");
        //    sqlQuery.Append("Domicilio, ");
        //    sqlQuery.Append("Colonia, ");
        //    sqlQuery.Append("Municipio, ");
        //    sqlQuery.Append("Entidad, ");
        //    sqlQuery.Append("DependenciaNormativa, ");
        //    sqlQuery.Append("Servicio, ");
        //    sqlQuery.Append("Sostenimiento, ");
        //    sqlQuery.Append("Director, ");
        //    sqlQuery.Append("Telefono, ");
        //    sqlQuery.Append("Fax, ");
        //    sqlQuery.Append("Codigo_Postal, ");
        //    sqlQuery.Append("Sector, ");
        //    sqlQuery.Append("Zona, ");
        //    sqlQuery.Append("Region, ");
        //    sqlQuery.Append("CicloActual ");
        //    sqlQuery.Append("FROM dbo.DatosIdentificacionPDF ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(Ds911_6.DatosIdentificacionPDF);
        //    #endregion

        //    #region datos ANEXO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_ANEXO
        //    sqlQuery.Append(" SELECT ID_Control, ID_CentroTrabajo, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ");
        //    sqlQuery.Append(" ID_Zona, ID_Sector, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones,");
        //    sqlQuery.Append(" Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx26, Anx27,   Anx28, Anx29, ");
        //    sqlQuery.Append(" Anx30, Anx31, Anx32, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx42, Anx43, Anx44, Anx45, ");
        //    sqlQuery.Append(" Anx46, Anx47, Anx48, Anx49, Anx50, Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx57, Anx58, Anx59, Anx60, Anx61, ");
        //    sqlQuery.Append(" Anx62, Anx63, Anx64, Anx65, Anx66, Anx67,  Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, Anx76, Anx77, ");
        //    sqlQuery.Append(" Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx84, Anx85, Anx86, Anx87, Anx88, Anx89, Anx90, Anx91, Anx92, Anx93, ");
        //    sqlQuery.Append(" Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, Anx104, Anx105, Anx106, Anx107, Anx108, Anx109, ");
        //    sqlQuery.Append(" Anx110, Anx111,   Anx112, Anx113, Anx114, Anx115,   Anx116, Anx117, Anx118, Anx119, Anx120, Anx121, Anx122, Anx123, Anx124, Anx125, ");
        //    sqlQuery.Append(" Anx126, Anx127, Anx128, Anx129, Anx130, Anx131, Anx132, Anx133, Anx134, Anx135, Anx136, Anx137, Anx138, Anx139, Anx140, Anx141, ");
        //    sqlQuery.Append(" Anx142, Anx143, Anx144, Anx145, Anx146, Anx147, Anx148, Anx149, Anx150, Anx151,   Anx152, Anx153, Anx154, Anx155, Anx156, ");
        //    sqlQuery.Append(" Anx157, Anx158, Anx159, Anx160, Anx161, Anx162, Anx163, Anx164, Anx165, Anx166, Anx167, Anx168, Anx169, Anx170, Anx171, Anx172, ");
        //    sqlQuery.Append(" Anx173, Anx174, Anx175, Anx176, Anx177, Anx178, Anx179, Anx180, Anx181, Anx182, Anx183, Anx184, Anx185, Anx186,   Anx187, ");
        //    sqlQuery.Append(" Anx188,   Anx189, Anx190, Anx191, Anx192, Anx193, Anx194, Anx195, Anx196, Anx197, Anx198, Anx199, Anx200, Anx201, Anx202, Anx203, Anx204, Anx205, ");
        //    sqlQuery.Append(" Anx206, Anx207, Anx208, Anx209, Anx210, Anx211, Anx212, Anx213, Anx214, Anx215, Anx216, Anx217, Anx218, Anx219, Anx220, Anx221, ");
        //    sqlQuery.Append(" Anx222,   Anx223, Anx224, Anx225, Anx226, Anx227, Anx228, Anx229, Anx230, Anx231, Anx232, Anx233, Anx234, Anx235, Anx236, Anx237, ");
        //    sqlQuery.Append(" Anx238, Anx239, Anx240, Anx241, Anx242, Anx243, Anx244, Anx245, Anx246, Anx247, Anx248, Anx249, Anx250, Anx251, Anx252, Anx253, ");
        //    sqlQuery.Append(" Anx254, Anx255, Anx256, Anx257, Anx258, Anx259, Anx260, Anx261, Anx262, Anx263, Anx264, Anx265, Anx266, Anx267, Anx268, Anx269, ");
        //    sqlQuery.Append(" Anx270, Anx271, Anx272, Anx273, Anx274, Anx275, Anx276, Anx277, Anx278, Anx279, Anx280, Anx281, Anx282, Anx283, Anx284, Anx285, ");
        //    sqlQuery.Append(" Anx286, Anx287, Anx288, Anx289, Anx290, Anx291, Anx292, Anx293, Anx294, Anx295, Anx296, Anx297, Anx298, Anx299, ");
        //    sqlQuery.Append(" Anx300, Anx301, Anx302, Anx303, Anx304, Anx305, Anx306, Anx307, Anx308, Anx309, Anx310, Anx311, Anx312, Anx313, Anx314, Anx315, ");
        //    sqlQuery.Append(" Anx316, Anx317, Anx318, Anx319, Anx320, Anx321, Anx322, Anx323, Anx324, Anx325, Anx326, Anx327, Anx328, Anx329, Anx330,   Anx331, ");
        //    sqlQuery.Append(" Anx332, Anx645, Anx646, Anx647, Anx648, Anx649, Anx650, Anx651, Anx652, Anx653, Anx654, Anx655, Anx656, Anx657, GETDATE() as Anx658, Anx659, ");
        //    sqlQuery.Append(" Anx660, Anx661, Anx662, Anx663    FROM dbo.VW_Anexo ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_6.VW_Anexo);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_Secundaria_F
        //    sqlQuery.Append("SELECT ID_Control,v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17, ");
        //    sqlQuery.Append("v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, ");
        //    sqlQuery.Append("v36, v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53, ");
        //    sqlQuery.Append("v54, v55, v56, v57, v58, v59, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v70, v71, ");
        //    sqlQuery.Append("v72, v73, v74, v75, v76, v77, v78, v79, v80, v81, v82, v83, v84, v85, v86, v87, v88, v89, ");
        //    sqlQuery.Append("v90, v91, v92, v93, v94, v95, v96, v97, v98, v99, v100, v101, v102, v103, v104, v105, v106, ");
        //    sqlQuery.Append("v107, v108, v109, v110, v111, v112, v113, v114, v115, v116, v117, v118, v119, v120, v121, ");
        //    sqlQuery.Append("v122, v123, v124, v125, v126, v127, v128, v129, v130, v131, v132, v133, v134, v135, v136, ");
        //    sqlQuery.Append("v137, v138, v139, v140, v141, v142, v143, v144, v145, v146, v147, v148, v149, v150, v151, ");
        //    sqlQuery.Append("v152, v153, v154, v155, v156, v157, v158, v159, v160, v161, v162, v163, v164, v165, v166, ");
        //    sqlQuery.Append("v167, v168, v169, v170, v171, v172, v173, v174, v175, v176, v177, v178, v179, v180, v181, ");
        //    sqlQuery.Append("v182, v183, v184, v185, v186, v187, v188, v189, v190, v191, v192, v193, v194, v195, v196, ");
        //    sqlQuery.Append("v197, v198, v199, v200, v201, v202, v203, v204, v205, v206, v207, v208, v209, v210, v211, ");
        //    sqlQuery.Append("v212, v213, v214, v215, v216, v217, v218, v219, v220, v221, v222, v223, v224, v225, v226, ");
        //    sqlQuery.Append("v227, v228, v229, v230, v231, v232, v233, v234, v235, v236, v237, v238, v239, v240, v241, ");
        //    sqlQuery.Append("v242, v243, v244, v245, v246, v247, v248, v249, v250, v251, v252, v253, v254, v255, v256, ");
        //    sqlQuery.Append("v257, v258, v259, v260, v261, v262, v263, v264, v265, v266, v267, v268, v269, v270, v271, ");
        //    sqlQuery.Append("v272, v273, v274, v275, v276, v277, v278, v279, v280, v281, v282, v283, v284, v285, v286, ");
        //    sqlQuery.Append("v287, v288, v289, v290, v291, v292, v293, v294, v295, v296, v297, v298, v299, v300, v301, ");
        //    sqlQuery.Append("v302, v303, v304, v305, v306, v307, v308, v309, v310, v311, v312, v313, v314, v315, v316, ");
        //    sqlQuery.Append("v317, v318, v319, v320, v321, v322, v323, v324, v325, v326, v327, v328, v329, v330, v331, ");
        //    sqlQuery.Append("v332, v333, v334, v335, v336, v337, v338, v339, v340, v341, v342, v343, v344, v345, v346, ");
        //    sqlQuery.Append("v347, v348, v349, v350, v351, v352, v353, v354, v355, v356, v357, v358, v359, v360, v361, ");
        //    sqlQuery.Append("v362, v363, v364, v365, v366, V367, V368, v369, v370, v371, v372, v373, v374, v375, v376, ");
        //    sqlQuery.Append("v377, v378, v379, v380, v381, v382, v383, v384, v385, v386, v387, v388, v389, v390, v391, ");
        //    sqlQuery.Append("v392, v393, v394, v395, v396, v397, v398, v399, v400, v401, v402, v403, v404, v405, v406, ");
        //    sqlQuery.Append("v407, v408, v409, v410, v411, v412, v413, v414, v415, v416, v417, v418, v419, v420, v421, ");
        //    sqlQuery.Append("v422, v423, v424, v426, v427, v428, v429, v430, v425, TipoOficializacion   ");
        //    sqlQuery.Append("from VW_Secundaria_F ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_6.VW_Secundaria_F);
        //    #endregion
        //    //classRpts.LoadReporte("911_4.rpt");
        //    //classRpts.DsDatos = Ds911_4;

        //    //classRpts.rpt.Subreports[0].SetDataSource(Ds911_4);

        //    Reportes.Crystals.Crystal_Report1911_6 rpt9_11 = new EstadisticasEducativas._911.fin.Reportes.Crystals.Crystal_Report1911_6();
        //    rpt9_11.ReportOptions.EnableSaveDataWithReport = false;
        //    rpt9_11.SetDataSource(Ds911_6);
        //    rpt9_11.Subreports[0].SetDataSource(Ds911_6);

        //    #region Datos INMUEBLE
        //    ServiceEstadisticas wsEstadisticas = new ServiceEstadisticas();
        //    SEroot.WsEstadisticasEducativas.InmuebleDP inmuebleDP = wsEstadisticas.CargaCuestionarioInmueble(controlDP.ID_Control);


        //    //rpt9_11.Subreports[0].SetParameterValue

        //    if (inmuebleDP != null && Lleno)
        //    {
        //        rpt9_11.SetParameterValue("A_si", X(inmuebleDP.a), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("A_no", X(!inmuebleDP.a), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("B_si", X(inmuebleDP.b), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("B_no", X(!inmuebleDP.b), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("C_si", X(inmuebleDP.c), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("C_no", X(!inmuebleDP.c), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("D_si", X(inmuebleDP.d), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("D_no", X(!inmuebleDP.d), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("E_si", X(inmuebleDP.e), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("E_no", X(!inmuebleDP.e), "Anexo.rpt");
        //        rpt9_11.SetParameterValue("Especifique", inmuebleDP.especifique, "Anexo.rpt");

        //    }
        //    else
        //    {
        //        rpt9_11.SetParameterValue("A_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("A_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("B_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("B_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("C_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("C_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("D_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("D_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("E_si", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("E_no", "", "Anexo.rpt");
        //        rpt9_11.SetParameterValue("Especifique", "", "Anexo.rpt");
        //    }
        //    #endregion
        //    rpt9_11.SetParameterValue("CicloActual", Ds911_6.DatosIdentificacionPDF[0].CicloActual, "Anexo.rpt");
        //    rpt9_11.SetParameterValue("Cuestionario", "911_6", "Anexo.rpt");
        //    rpt9_11.SetParameterValue("Clave", controlDP.Clave, "Anexo.rpt");
        //    rpt9_11.SetParameterValue("Turno", controlDP.ID_Turno.ToString(), "Anexo.rpt");
        //    rpt9_11.SetParameterValue("Nombre", controlDP.Nombre, "Anexo.rpt");

        //    byte[] PDF_byte = ConvertStreamToByteBuffer(rpt9_11.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
        //    rpt9_11.Close();
        //    ImprimeReporte(PDF_byte);

        //}
        //private void pdf_911_6_Oficializar(ControlDP controlDP)
        //{
        //    ClassRpts classRpts = new ClassRpts();
        //    SqlConnection cnn = cnn_DB911();

        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_6 Ds911_6 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_6();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append("SELECT ID_Control, ");
        //    sqlQuery.Append("id_CCTNT, ");
        //    sqlQuery.Append("Id_CentroTrabajo, ");
        //    sqlQuery.Append("id_turno, ");
        //    sqlQuery.Append("id_nivel, ");
        //    sqlQuery.Append("id_subnivel, ");
        //    sqlQuery.Append("Clave, ");
        //    sqlQuery.Append("CNT_id_turno, ");
        //    sqlQuery.Append("Nombre, ");
        //    sqlQuery.Append("Domicilio, ");
        //    sqlQuery.Append("Colonia, ");
        //    sqlQuery.Append("Municipio, ");
        //    sqlQuery.Append("Entidad, ");
        //    sqlQuery.Append("DependenciaNormativa, ");
        //    sqlQuery.Append("Servicio, ");
        //    sqlQuery.Append("Sostenimiento, ");
        //    sqlQuery.Append("Director, ");
        //    sqlQuery.Append("Telefono, ");
        //    sqlQuery.Append("Fax, ");
        //    sqlQuery.Append("Codigo_Postal, ");
        //    sqlQuery.Append("Sector, ");
        //    sqlQuery.Append("Zona, ");
        //    sqlQuery.Append("Region, ");
        //    sqlQuery.Append("CicloActual ");
        //    sqlQuery.Append("FROM dbo.DatosIdentificacionPDF ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(Ds911_6.DatosIdentificacionPDF);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_Secundaria_F
        //    sqlQuery.Append("SELECT ID_Control,v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17, ");
        //    sqlQuery.Append("v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, ");
        //    sqlQuery.Append("v36, v37, v38, v39, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53, ");
        //    sqlQuery.Append("v54, v55, v56, v57, v58, v59, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v70, v71, ");
        //    sqlQuery.Append("v72, v73, v74, v75, v76, v77, v78, v79, v80, v81, v82, v83, v84, v85, v86, v87, v88, v89, ");
        //    sqlQuery.Append("v90, v91, v92, v93, v94, v95, v96, v97, v98, v99, v100, v101, v102, v103, v104, v105, v106, ");
        //    sqlQuery.Append("v107, v108, v109, v110, v111, v112, v113, v114, v115, v116, v117, v118, v119, v120, v121, ");
        //    sqlQuery.Append("v122, v123, v124, v125, v126, v127, v128, v129, v130, v131, v132, v133, v134, v135, v136, ");
        //    sqlQuery.Append("v137, v138, v139, v140, v141, v142, v143, v144, v145, v146, v147, v148, v149, v150, v151, ");
        //    sqlQuery.Append("v152, v153, v154, v155, v156, v157, v158, v159, v160, v161, v162, v163, v164, v165, v166, ");
        //    sqlQuery.Append("v167, v168, v169, v170, v171, v172, v173, v174, v175, v176, v177, v178, v179, v180, v181, ");
        //    sqlQuery.Append("v182, v183, v184, v185, v186, v187, v188, v189, v190, v191, v192, v193, v194, v195, v196, ");
        //    sqlQuery.Append("v197, v198, v199, v200, v201, v202, v203, v204, v205, v206, v207, v208, v209, v210, v211, ");
        //    sqlQuery.Append("v212, v213, v214, v215, v216, v217, v218, v219, v220, v221, v222, v223, v224, v225, v226, ");
        //    sqlQuery.Append("v227, v228, v229, v230, v231, v232, v233, v234, v235, v236, v237, v238, v239, v240, v241, ");
        //    sqlQuery.Append("v242, v243, v244, v245, v246, v247, v248, v249, v250, v251, v252, v253, v254, v255, v256, ");
        //    sqlQuery.Append("v257, v258, v259, v260, v261, v262, v263, v264, v265, v266, v267, v268, v269, v270, v271, ");
        //    sqlQuery.Append("v272, v273, v274, v275, v276, v277, v278, v279, v280, v281, v282, v283, v284, v285, v286, ");
        //    sqlQuery.Append("v287, v288, v289, v290, v291, v292, v293, v294, v295, v296, v297, v298, v299, v300, v301, ");
        //    sqlQuery.Append("v302, v303, v304, v305, v306, v307, v308, v309, v310, v311, v312, v313, v314, v315, v316, ");
        //    sqlQuery.Append("v317, v318, v319, v320, v321, v322, v323, v324, v325, v326, v327, v328, v329, v330, v331, ");
        //    sqlQuery.Append("v332, v333, v334, v335, v336, v337, v338, v339, v340, v341, v342, v343, v344, v345, v346, ");
        //    sqlQuery.Append("v347, v348, v349, v350, v351, v352, v353, v354, v355, v356, v357, v358, v359, v360, v361, ");
        //    sqlQuery.Append("v362, v363, v364, v365, v366, V367, V368, v369, v370, v371, v372, v373, v374, v375, v376, ");
        //    sqlQuery.Append("v377, v378, v379, v380, v381, v382, v383, v384, v385, v386, v387, v388, v389, v390, v391, ");
        //    sqlQuery.Append("v392, v393, v394, v395, v396, v397, v398, v399, v400, v401, v402, v403, v404, v405, v406, ");
        //    sqlQuery.Append("v407, v408, v409, v410, v411, v412, v413, v414, v415, v416, v417, v418, v419, v420, v421, ");
        //    sqlQuery.Append("v422, v423, v424, v426, v427, v428, v429, v430, v425, TipoOficializacion   ");
        //    sqlQuery.Append("from VW_Secundaria_F ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_6.VW_Secundaria_F);
        //    #endregion

        //    classRpts.LoadReporte("estadisticaSec.rpt");
        //    classRpts.DsDatos = Ds911_6;
        //    //classRpts.rpt.  .ReportObjects["Box91"].Border.BorderColor = System.Drawing.Color.White;
        //    byte[] PDF_byte = classRpts.GenerarEnMemoria();

        //    ImprimeReporte(PDF_byte);
        //}
        #endregion

        #region PDF ECC_21
        //private void pdf_ECC_21(ControlDP controlDP, bool Lleno)
        //{
        //    ClassRpts classRpts = new ClassRpts();
        //    SqlConnection cnn = cnn_DB911();
        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_ECC_21 DsECC_21 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_ECC_21();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append("SELECT ID_Control, ");
        //    sqlQuery.Append("id_CCTNT, ");
        //    sqlQuery.Append("Id_CentroTrabajo, ");
        //    sqlQuery.Append("id_turno, ");
        //    sqlQuery.Append("id_nivel, ");
        //    sqlQuery.Append("id_subnivel, ");
        //    sqlQuery.Append("Clave, ");
        //    sqlQuery.Append("CNT_id_turno, ");
        //    sqlQuery.Append("Nombre, ");
        //    sqlQuery.Append("Domicilio, ");
        //    sqlQuery.Append("Colonia, ");
        //    sqlQuery.Append("Municipio, ");
        //    sqlQuery.Append("Entidad, ");
        //    sqlQuery.Append("DependenciaNormativa, ");
        //    sqlQuery.Append("Servicio, ");
        //    sqlQuery.Append("Sostenimiento, ");
        //    sqlQuery.Append("Director, ");
        //    sqlQuery.Append("Telefono, ");
        //    sqlQuery.Append("Fax, ");
        //    sqlQuery.Append("Codigo_Postal, ");
        //    sqlQuery.Append("Sector, ");
        //    sqlQuery.Append("Zona, ");
        //    sqlQuery.Append("Region, ");
        //    sqlQuery.Append("CicloActual ");
        //    sqlQuery.Append("FROM dbo.DatosIdentificacionPDF ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(DsECC_21.DatosIdentificacionPDF);
        //    #endregion

        //    #region datos ANEXO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_ANEXO
        //    sqlQuery.Append(" SELECT ID_Control, ID_CentroTrabajo, ID_Turno, ID_CicloEscolar, ID_Cuestionario, ID_Nivel, ID_SubNivel, ID_CCTNT, ID_Region, ");
        //    sqlQuery.Append(" ID_Zona, ID_Sector, Estatus, Fecha_Cierre, Bit_Activo, Id_Usuario, Fecha_Actualizacion, Observaciones,");
        //    sqlQuery.Append(" Anx15, Anx16, Anx17, Anx18, Anx19, Anx20, Anx21, Anx22, Anx23, Anx24, Anx25, Anx26, Anx27,   Anx28, Anx29, ");
        //    sqlQuery.Append(" Anx30, Anx31, Anx32, Anx33, Anx34, Anx35, Anx36, Anx37, Anx38, Anx39, Anx40, Anx41, Anx42, Anx43, Anx44, Anx45, ");
        //    sqlQuery.Append(" Anx46, Anx47, Anx48, Anx49, Anx50, Anx51, Anx52, Anx53, Anx54, Anx55, Anx56, Anx57, Anx58, Anx59, Anx60, Anx61, ");
        //    sqlQuery.Append(" Anx62, Anx63, Anx64, Anx65, Anx66, Anx67,  Anx68, Anx69, Anx70, Anx71, Anx72, Anx73, Anx74, Anx75, Anx76, Anx77, ");
        //    sqlQuery.Append(" Anx78, Anx79, Anx80, Anx81, Anx82, Anx83, Anx84, Anx85, Anx86, Anx87, Anx88, Anx89, Anx90, Anx91, Anx92, Anx93, ");
        //    sqlQuery.Append(" Anx94, Anx95, Anx96, Anx97, Anx98, Anx99, Anx100, Anx101, Anx102, Anx103, Anx104, Anx105, Anx106, Anx107, Anx108, Anx109, ");
        //    sqlQuery.Append(" Anx110, Anx111,   Anx112, Anx113, Anx114, Anx115,   Anx116, Anx117, Anx118, Anx119, Anx120, Anx121, Anx122, Anx123, Anx124, Anx125, ");
        //    sqlQuery.Append(" Anx126, Anx127, Anx128, Anx129, Anx130, Anx131, Anx132, Anx133, Anx134, Anx135, Anx136, Anx137, Anx138, Anx139, Anx140, Anx141, ");
        //    sqlQuery.Append(" Anx142, Anx143, Anx144, Anx145, Anx146, Anx147, Anx148, Anx149, Anx150, Anx151,   Anx152, Anx153, Anx154, Anx155, Anx156, ");
        //    sqlQuery.Append(" Anx157, Anx158, Anx159, Anx160, Anx161, Anx162, Anx163, Anx164, Anx165, Anx166, Anx167, Anx168, Anx169, Anx170, Anx171, Anx172, ");
        //    sqlQuery.Append(" Anx173, Anx174, Anx175, Anx176, Anx177, Anx178, Anx179, Anx180, Anx181, Anx182, Anx183, Anx184, Anx185, Anx186,   Anx187, ");
        //    sqlQuery.Append(" Anx188,   Anx189, Anx190, Anx191, Anx192, Anx193, Anx194, Anx195, Anx196, Anx197, Anx198, Anx199, Anx200, Anx201, Anx202, Anx203, Anx204, Anx205, ");
        //    sqlQuery.Append(" Anx206, Anx207, Anx208, Anx209, Anx210, Anx211, Anx212, Anx213, Anx214, Anx215, Anx216, Anx217, Anx218, Anx219, Anx220, Anx221, ");
        //    sqlQuery.Append(" Anx222,   Anx223, Anx224, Anx225, Anx226, Anx227, Anx228, Anx229, Anx230, Anx231, Anx232, Anx233, Anx234, Anx235, Anx236, Anx237, ");
        //    sqlQuery.Append(" Anx238, Anx239, Anx240, Anx241, Anx242, Anx243, Anx244, Anx245, Anx246, Anx247, Anx248, Anx249, Anx250, Anx251, Anx252, Anx253, ");
        //    sqlQuery.Append(" Anx254, Anx255, Anx256, Anx257, Anx258, Anx259, Anx260, Anx261, Anx262, Anx263, Anx264, Anx265, Anx266, Anx267, Anx268, Anx269, ");
        //    sqlQuery.Append(" Anx270, Anx271, Anx272, Anx273, Anx274, Anx275, Anx276, Anx277, Anx278, Anx279, Anx280, Anx281, Anx282, Anx283, Anx284, Anx285, ");
        //    sqlQuery.Append(" Anx286, Anx287, Anx288, Anx289, Anx290, Anx291, Anx292, Anx293, Anx294, Anx295, Anx296, Anx297, Anx298, Anx299, ");
        //    sqlQuery.Append(" Anx300, Anx301, Anx302, Anx303, Anx304, Anx305, Anx306, Anx307, Anx308, Anx309, Anx310, Anx311, Anx312, Anx313, Anx314, Anx315, ");
        //    sqlQuery.Append(" Anx316, Anx317, Anx318, Anx319, Anx320, Anx321, Anx322, Anx323, Anx324, Anx325, Anx326, Anx327, Anx328, Anx329, Anx330,   Anx331, ");
        //    sqlQuery.Append(" Anx332, Anx645, Anx646, Anx647, Anx648, Anx649, Anx650, Anx651, Anx652, Anx653, Anx654, Anx655, Anx656, Anx657, GETDATE() as Anx658, Anx659, ");
        //    sqlQuery.Append(" Anx660, Anx661, Anx662, Anx663    FROM dbo.VW_Anexo ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(DsECC_21.VW_Anexo);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_Preescolar_Conafe_Fin
        //    sqlQuery.Append("SELECT ID_Control,v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, ");
        //    sqlQuery.Append("v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, ");
        //    sqlQuery.Append("v33, v34, v35, v36, v37, v38, V39, v40, v41, ");
        //    sqlQuery.Append("TipoOficializacion   ");
        //    sqlQuery.Append("from VW_Preescolar_Conafe_Fin ");
        //    sqlQuery.Append("WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(DsECC_21.VW_Preescolar_Conafe_Fin);
        //    #endregion


        //    Reportes.Crystals.ECC_21 rpt_ECC_21 = new EstadisticasEducativas._911.fin.Reportes.Crystals.ECC_21();
        //    rpt_ECC_21.ReportOptions.EnableSaveDataWithReport = false;
        //    rpt_ECC_21.SetDataSource(DsECC_21);
        //    rpt_ECC_21.Subreports[0].SetDataSource(DsECC_21);


        //    rpt_ECC_21.SetParameterValue("CicloActual", DsECC_21.DatosIdentificacionPDF[0].CicloActual, "Anexo.rpt");
        //    rpt_ECC_21.SetParameterValue("Clave", controlDP.Clave, "Anexo.rpt");
        //    rpt_ECC_21.SetParameterValue("Turno", controlDP.ID_Turno.ToString(), "Anexo.rpt");
        //    rpt_ECC_21.SetParameterValue("Nombre", controlDP.Nombre, "Anexo.rpt");

        //    byte[] PDF_byte = ConvertStreamToByteBuffer(rpt_ECC_21.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
        //    rpt_ECC_21.Close();
        //    ImprimeReporte(PDF_byte);

        //}
        //private void pdf_ECC_21_Oficializar(ControlDP controlDP)
        //{
        //    ClassRpts classRpts = new ClassRpts();
        //    SqlConnection cnn = cnn_DB911();

        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_ECC_21 ds911_ECC = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_ECC_21();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append(DataSets.SQL.SQL_DatosIdentificacionPDF);
        //    sqlQuery.Append(" WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(ds911_ECC.DatosIdentificacionPDF);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_Secundaria_F
        //    sqlQuery.Append(DataSets.SQL.SQL_VW_Preescolar_Conafe_Fin);
        //    sqlQuery.Append(" WHERE ID_Control = @ID_Control ");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(ds911_ECC.VW_Preescolar_Conafe_Fin);
        //    #endregion

        //    classRpts.LoadReporte("estadisticaPreConafe.rpt");
        //    classRpts.DsDatos = ds911_ECC;
        //    //classRpts.rpt.  .ReportObjects["Box91"].Border.BorderColor = System.Drawing.Color.White;
        //    byte[] PDF_byte = classRpts.GenerarEnMemoria();

        //    ImprimeReporte(PDF_byte);
        //}
        #endregion

        #region PDF ECC_22
        //private void pdf_ECC_22(ControlDP controlDP, bool Lleno)
        //{
        //    ClassRpts classRpts = new ClassRpts();
        //    SqlConnection cnn = cnn_DB911();
        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_ECC_22 Ds911_ECC_22 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_ECC_22();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append(DataSets.SQL.SQL_DatosIdentificacionPDF);
        //    sqlQuery.Append(" WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(Ds911_ECC_22.DatosIdentificacionPDF);
        //    #endregion

        //    #region datos ANEXO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_ANEXO
        //    sqlQuery.Append(DataSets.SQL.SQL_VW_Anexo);
        //    sqlQuery.Append(" WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_ECC_22.VW_Anexo);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_Secundaria_F
        //    sqlQuery.Append(DataSets.SQL.SQL_VW_Primaria_Conafe_Fin);
        //    sqlQuery.Append(" WHERE ID_Control = @ID_Control ");
        //    if (!Lleno) sqlQuery.Append(" AND 1=0");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_ECC_22.VW_Primaria_Conafe_Fin);
        //    #endregion

        //    Reportes.Crystals.ECC_22 rptECC_22 = new EstadisticasEducativas._911.fin.Reportes.Crystals.ECC_22();
        //    rptECC_22.ReportOptions.EnableSaveDataWithReport = false;
        //    rptECC_22.SetDataSource(Ds911_ECC_22);
        //    rptECC_22.Subreports[0].SetDataSource(Ds911_ECC_22);


        //    rptECC_22.SetParameterValue("CicloActual", Ds911_ECC_22.DatosIdentificacionPDF[0].CicloActual, "Anexo.rpt");
        //    rptECC_22.SetParameterValue("Cuestionario", "911_6", "Anexo.rpt");
        //    rptECC_22.SetParameterValue("Clave", controlDP.Clave, "Anexo.rpt");
        //    rptECC_22.SetParameterValue("Turno", controlDP.ID_Turno.ToString(), "Anexo.rpt");
        //    rptECC_22.SetParameterValue("Nombre", controlDP.Nombre, "Anexo.rpt");

        //    byte[] PDF_byte = ConvertStreamToByteBuffer(rptECC_22.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat));
        //    rptECC_22.Close();
        //    ImprimeReporte(PDF_byte);

        //}
        //private void pdf_ECC_22_Oficializar(ControlDP controlDP)
        //{
        //    ClassRpts classRpts = new ClassRpts();
        //    SqlConnection cnn = cnn_DB911();

        //    System.Text.StringBuilder sqlQuery = new System.Text.StringBuilder();
        //    DataSets.ds911_ECC_22 Ds911_ECC_22 = new EstadisticasEducativas._911.fin.Reportes.DataSets.ds911_ECC_22();
        //    SqlDataAdapter da = null;

        //    #region datos IDENTIFICACION
        //    SqlCommand com = null;
        //    #region SELECT DatosIdentificacionPDF
        //    sqlQuery.Append(DataSets.SQL.SQL_DatosIdentificacionPDF);
        //    sqlQuery.Append(" WHERE ID_Control = @ID_Control ");

        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da = new SqlDataAdapter(com);
        //    da.Fill(Ds911_ECC_22.DatosIdentificacionPDF);
        //    #endregion

        //    #region Datos CUESTIONARIO
        //    sqlQuery = new System.Text.StringBuilder();
        //    #region select VW_Primaria_Conafe_Fin
        //    sqlQuery.Append(DataSets.SQL.SQL_VW_Primaria_Conafe_Fin);
        //    sqlQuery.Append(" WHERE ID_Control = @ID_Control ");
        //    #endregion
        //    com = new SqlCommand(sqlQuery.ToString(), cnn);
        //    da = new SqlDataAdapter(com);
        //    com.Parameters.Add(new SqlParameter("@ID_Control", controlDP.ID_Control));
        //    da.Fill(Ds911_ECC_22.VW_Primaria_Conafe_Fin);
        //    #endregion

        //    classRpts.LoadReporte("estadisticaPrimConafe.rpt");
        //    classRpts.DsDatos = Ds911_ECC_22;
        //    byte[] PDF_byte = classRpts.GenerarEnMemoria();

        //    ImprimeReporte(PDF_byte);
        //}
        #endregion

        #region Herramientas Comunes a todas las Encuestas
        private string X(bool a)
        {
            if (a)
                return "X";
            else
                return "_";
        }

        private byte[] ConvertStreamToByteBuffer(System.IO.Stream theStream)
        {
            int b1;
            System.IO.MemoryStream tempStream = new System.IO.MemoryStream();
            while ((b1 = theStream.ReadByte()) != -1)
            {
                tempStream.WriteByte(((byte)b1));
            }
            return tempStream.ToArray();
        }

        protected void DoDeleteFile()
        {
            if (Session["lGUID"] != null)
            {
                string lGUID = Session["lGUID"].ToString();
                if (System.IO.File.Exists(Server.MapPath("~/PDFReports/EE" + lGUID + ".pdf")))
                    System.IO.File.Delete(Server.MapPath("~/PDFReports/EE" + lGUID + ".pdf"));
            }
        }

        private void ImprimeReporte(byte[] aArchivo)
        {
            DoDeleteFile();
            System.Guid lGUID = Guid.NewGuid();
            System.IO.FileStream lFileStream = new System.IO.FileStream(Server.MapPath("~/PDFReports/EE" + lGUID.ToString() + ".pdf"), System.IO.FileMode.CreateNew);
            try
            {
                lFileStream.Write(aArchivo, 0, aArchivo.Length - 1);
            }
            finally
            {
                lFileStream.Flush();
                lFileStream.Close();
            }
            Session["lGUID"] = lGUID.ToString();
            Response.Redirect("~/PDFReports/EE" + lGUID.ToString() + ".pdf");
        }

        private SqlConnection cnn_DB911()
        {
            AppSettingsReader reader = new AppSettingsReader();
            SqlConnection cnn = new System.Data.SqlClient.SqlConnection();
            cnn.ConnectionString = reader.GetValue("DB911ConnectionString", typeof(string)).ToString();
            cnn.FireInfoMessageEventOnUserErrors = false;
            return cnn;
        }

        private SqlConnection cnn_SENLMaster()
        {
            AppSettingsReader reader = new AppSettingsReader();
            SqlConnection cnn = new System.Data.SqlClient.SqlConnection();
            cnn.ConnectionString = reader.GetValue("SENLMasterConnectionString", typeof(string)).ToString();
            cnn.FireInfoMessageEventOnUserErrors = false;
            return cnn;
        }

        #endregion
        /// ImprimeReporte y crea la session que lo maneja
        

        */
        #endregion



    }
}
