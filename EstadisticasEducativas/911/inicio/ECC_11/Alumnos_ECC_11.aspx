<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" Title="ECC-11(Alumnos)" AutoEventWireup="true" CodeBehind="Alumnos_ECC_11.aspx.cs" Inherits="EstadisticasEducativasInicio._911.inicio.ECC_11.Alumnos_ECC_11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
<div id="logo"></div>
     <div style="min-width:800px; height:65px;">
     <div id="header">
     <table style="width:100%">
        <tr>
            <td>
                <asp:Label ID="lblNivel" runat="server" Text="EDUCACI�N PREESCOLAR CONAFE" Font-Bold="True" Font-Size="14px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="lblCiclo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td >
                <span  >
                    <asp:Label ID="lblCentroTrabajo" runat="server"></asp:Label>
                </span>
            </td>
        </tr>

    </table>
    </div>
    </div>
    <div id="menu" style="min-width:800px;">
      <ul class="left">
        <li onclick="openPage('Identificacion_ECC_11',true)"><a href="#" title=""><span>IDENTIFICACI�N</span></a></li>
        <li onclick="openPage('Alumnos_ECC_11',true)"><a href="#" title="" class="activo"><span>ALUMNOS</span></a></li>
        <li onclick="openPage('Anexo_ECC_11',false)"><a href="#" title=""><span>ANEXO</span></a></li>
        <li onclick="denyPage()"><a href="#" title="" class="inactivo"><span>OFICIALIZACI�N</span></a></li>
      </ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li>
      </ul>
    </div>
     <div  id="tooltipayuda" class="balloonstyle">
    <p>Una vez que haya ingresado y revisado los datos dar clic en la opci�n SIGUIENTE 
    para continuar con la generaci�n de las Estad�sticas Educativas.</p>
    </div> 
    <br /><br /><br />
        <asp:Panel ID="pnlOficializado"  runat="server" CssClass="banOfi">
               <img src="../../../tema/images/OFICIALIZADO.png" alt="Oficializado" height="60px"/>
           </asp:Panel>
    <center>
            
  <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>


    <table style="width: 100%" width="800">
        <tr>
            <td colspan="2" style="height: 17px; text-align: center">
                <asp:Label ID="lblImportante" runat="server" CssClass="lblRojo" Text="IMPORTANTE: AL CONTESTAR ESTE CUESTIONARIO NO CONSIDERE LA INFORMACI�N DE PRIMARIA"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 400px; vertical-align: top; height: 665px; horizontal-align: center;">
                <table style="width: 100%">
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lblAlumnos" runat="server" CssClass="lblGrisTit" Text="I. ALUMNOS" Width="100%" Font-Size="16px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="lbltipoServicio" runat="server" CssClass="lblRojo" Text="1. Seleccione el tipo de servicio."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 559px; text-align: left;">
                            <asp:Label ID="lblComunitario" runat="server" CssClass="lblGrisTit" Height="17px" Text="Preescolar comunitario" Width="100%"></asp:Label></td>
                        <td>
                            &nbsp;<asp:RadioButton ID="optV1" runat="server" GroupName="servicio" TabIndex="10101" onkeydown="return Arrows(event,this.tabIndex)"/>
                            <asp:TextBox ID="txtV1" runat="server" Columns="1" MaxLength="1" style="visibility:hidden;" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 559px; text-align: left">
                            <asp:Label ID="lblPAEPI" runat="server" CssClass="lblGrisTit" Height="17px" Text="Proyecto de Atenci�n Educativa a la Poblaci�n Ind�gena (PAEPI)" Width="100%"></asp:Label></td>
                        <td>
                            <asp:RadioButton ID="optV2" runat="server" GroupName="servicio" TabIndex="10201" onkeydown="return Arrows(event,this.tabIndex)"/>
                            <asp:TextBox ID="txtV2" runat="server" Columns="1" MaxLength="1" style="visibility:hidden;"  ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 559px; text-align: left">
                            <asp:Label ID="lblCIC" runat="server" CssClass="lblGrisTit" Height="17px" Text="Centro Infantil comunitario (CIC) " Width="100%"></asp:Label></td>
                        <td>
                            <asp:RadioButton ID="optV3" runat="server" GroupName="servicio" TabIndex="10301"  onkeydown="return Arrows(event,this.tabIndex)"/>
                            <asp:TextBox ID="txtV3" runat="server" Columns="1" MaxLength="1" style="visibility:hidden;" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 559px; text-align: left">
                            <asp:Label ID="lblPAEPIAM" runat="server" CssClass="lblGrisTit" Height="17px" Text="Proyecto de Atenci�n Educativa a la Poblaci�n Infantil Agr�cola Migrante (PAEPIAM)" Width="100%"></asp:Label></td>
                        <td>
                            <asp:RadioButton ID="optV4" runat="server" GroupName="servicio" TabIndex="10401" onkeydown="return Arrows(event,this.tabIndex)"/>
                            <asp:TextBox ID="txtV4" runat="server" Columns="1" MaxLength="1" style="visibility:hidden;" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="width: 559px">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 19px; text-align: left">
                            <asp:Label ID="lblCompartidas" runat="server" CssClass="lblRojo" Text="2. �El proyecto es de aulas compartidas? (Seleccione una opci�n)."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center" colspan="2">
                            <br />
                            <table style="width: 418px">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="optV54" runat="server" GroupName="compartidas" Text="S�" TabIndex="10501" onkeydown="return Arrows(event,this.tabIndex)"/>
                                        <asp:TextBox ID="txtV54" runat="server" Columns="1" MaxLength="1" style="visibility:hidden;" ></asp:TextBox></td>
                                    <td>
                                        <asp:RadioButton ID="optV55" runat="server" GroupName="compartidas" Text="NO" TabIndex="10502" onkeydown="return Arrows(event,this.tabIndex)"/>
                                        <asp:TextBox ID="txtV55" runat="server" Columns="1" MaxLength="1" style="visibility:hidden;" ></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right; width: 559px;">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: justify">
                            <asp:Label ID="Label2" runat="server" CssClass="lblRojo" Text="3. Escriba la cantidad de alumnos inscritos a partir de la fecha de inicio de cursos, sumando las altas y restando las bajas hasta el 31 de octubre, desglos�ndola por sexo y edad."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <br />
                            <table style="width: 493px">
                                <tr>
                                    <td style="width: 67px; height: 19px">
                                    </td>
                                    <td style="height: 19px">
                                        <asp:Label ID="Label6" runat="server" CssClass="lblGrisTit" Height="17px" Text="3 a�os"></asp:Label></td>
                                    <td style="height: 19px">
                                        <asp:Label ID="Label7" runat="server" CssClass="lblGrisTit" Height="17px" Text="4 a�os"></asp:Label></td>
                                    <td style="height: 19px">
                                        <asp:Label ID="Label8" runat="server" CssClass="lblGrisTit" Height="17px" Text="5 a�os"></asp:Label></td>
                                    <td style="height: 19px">
                                        <asp:Label ID="Label10" runat="server" CssClass="lblGrisTit" Height="17px" Text="6 a�os"></asp:Label></td>
                                    <td style="height: 19px">
                                        <asp:Label ID="Label9" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 67px">
                                        <asp:Label ID="Label3" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV5" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10601" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV6" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10602" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV7" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10603" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV8" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10604" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV9" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10605" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 67px">
                                        <asp:Label ID="Label4" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV10" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10701" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV11" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10702" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV12" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10703" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV13" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10704" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV14" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10705" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 67px">
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 67px">
                                        <asp:Label ID="Label5" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV15" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10801" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV16" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10802" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV17" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10803" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV18" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10804" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV19" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="10805" ></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 559px;">
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="text-align: center; width: 400px; vertical-align: top; height: 665px;" valign="middle">
                <table style="width: 299px">
                    <tr>
                        <td style="text-align:justify">
                            <asp:Label ID="lblNota2" runat="server" CssClass="lblRojo" 
                                Text="4. Escriba la cantidad de alumnos con discapacidad, aptitudes sobresalientes u otras condiciones, 
                                desglos�ndolos por sexo."
                                    ></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <br />
                            <table style="width: 365px; height: 1px">
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblSituaci�n" runat="server" CssClass="lblNegro" Height="17px" Text="SITUACI�N DEL ALUMNO"
                                            Width="100%" Font-Bold="True"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblHombres4" runat="server" CssClass="lblGrisTit" Height="17px" Text="HOMBRES"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblMujeres4" runat="server" CssClass="lblGrisTit" Height="17px" Text="MUJERES"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:Label ID="lblTotal4" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="height: 19px; text-align: left">
                                        <asp:Label ID="lblCeguera" runat="server" CssClass="lblGrisTit" Height="17px" Text="CEGUERA"
                                            Width="100%"></asp:Label></td>
                                    <td style="height: 19px">
                                        <asp:TextBox ID="txtV20" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10901" ></asp:TextBox></td>
                                    <td style="height: 19px">
                                        <asp:TextBox ID="txtV21" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10902" ></asp:TextBox></td>
                                    <td style="height: 19px">
                                        <asp:TextBox ID="txtV22" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="10903" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblVisual" runat="server" CssClass="lblGrisTit" Height="17px" Text="BAJA VISI�N"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV23" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11001" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV24" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11002" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV25" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11003" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblSordera" runat="server" CssClass="lblGrisTit" Height="17px" Text="SORDERA"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV26" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11101" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV27" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11102" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV28" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11103" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblAuditiva" runat="server" CssClass="lblGrisTit" Height="17px" Text="HIPOACUSIA"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV29" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11201" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV30" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11202" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV31" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11203" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 19px; text-align: left">
                                        <asp:Label ID="lblMotriz" runat="server" CssClass="lblGrisTit" Height="17px" Text="DISCAPACIDAD MOTRIZ"
                                            Width="100%"></asp:Label></td>
                                    <td style="height: 19px">
                                        <asp:TextBox ID="txtV32" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11301" ></asp:TextBox></td>
                                    <td style="height: 19px">
                                        <asp:TextBox ID="txtV33" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11302" ></asp:TextBox></td>
                                    <td style="height: 19px">
                                        <asp:TextBox ID="txtV34" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11303" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblIntelectual" runat="server" CssClass="lblGrisTit" Height="17px" Text="DISCAPACIDAD INTELECTUAL"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV35" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11401" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV36" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11402" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV37" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11403" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: left">
                                        <asp:Label ID="lblCAS" runat="server" CssClass="lblGrisTit" Height="100%" Text="APTITUDES SOBRESALIENTES"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV38" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11501" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV39" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11502" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV40" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11503" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="height: 26px; text-align: left">
                                        <asp:Label ID="lblOtros" runat="server" CssClass="lblGrisTit" Height="17px" Text="OTRAS CONDICIONES"
                                            Width="100%"></asp:Label></td>
                                    <td style="height: 26px">
                                        <asp:TextBox ID="txtV41" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11601" ></asp:TextBox></td>
                                    <td style="height: 26px">
                                        <asp:TextBox ID="txtV42" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11602" ></asp:TextBox></td>
                                    <td style="height: 26px">
                                        <asp:TextBox ID="txtV43" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11603" ></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right">
                                        <asp:Label ID="lblTotalGeneral" runat="server" CssClass="lblGrisTit" Height="17px" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="txtV44" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11701" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV45" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11702" ></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtV46" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11703" ></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: justify; height: 53px;">
                            <asp:Label ID="lblNEE" runat="server" CssClass="lblRojo" Text="5. Escriba la cantidad de alumnos con Necesidades Educativas Especiales (NEE), independientemente de que presenten o no alguna discapacidad, desglos�ndola por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="height: 70px">
                            <table style="width: 393px">
                                <tr>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblHombres5" runat="server" CssClass="lblGrisTit" Height="100%" Text="HOMBRES"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblMujeres5" runat="server" CssClass="lblGrisTit" Height="100%" Text="MUJERES"
                                            Width="100%"></asp:Label>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblTotal5" runat="server" CssClass="lblGrisTit" Height="100%" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV47" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11801" ></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV48" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11802" ></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV49" runat="server" Columns="3" MaxLength="3" CssClass="lblNegro" TabIndex="11803" ></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <asp:Label ID="lblPersonal" runat="server" CssClass="lblGrisTit" Text="II. PERSONAL"
                                Width="100%" Font-Size="16px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="text-align: justify">
                            <asp:Label ID="lblinstructores" runat="server" CssClass="lblRojo" Text="1. Escriba la cantidad de instructores comunitarios o agentes educativos, desglos�ndola por sexo."
                                Width="100%"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 393px">
                                <tr>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblHombresP" runat="server" CssClass="lblGrisTit" Height="100%" Text="HOMBRES"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblMujeresP" runat="server" CssClass="lblGrisTit" Height="100%" Text="MUJERES"
                                            Width="100%"></asp:Label></td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblTotalP" runat="server" CssClass="lblGrisTit" Height="100%" Text="TOTAL"
                                            Width="100%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV50" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11901" ></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV51" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11902" ></asp:TextBox></td>
                                    <td style="text-align: center">
                                        <asp:TextBox ID="txtV52" runat="server" Columns="2" MaxLength="2" CssClass="lblNegro" TabIndex="11903" ></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
        <hr />
        <table align="center">
            <tr>
                <td ><span  onclick="openPage('Identificacion_ECC_11',true)"><a href="#" title=""><img src="../../../tema/images/prev.gif" alt="Ir p�gina previa" /></a></span></td> 
                <td ><span  onclick="openPage('Identificacion_ECC_11',true)"><a href="#" title=""><span class="SigAnt">ANTERIOR</span></a></span></td>
                <td style="width: 330px;">
                &nbsp;
                </td>
                <td ><span  onclick="openPage('Anexo_ECC_11',false)"><a href="#" title=""><span class="SigAnt">SIGUIENTE</span></a></span></td> 
                <td ><span  onclick="openPage('Anexo_ECC_11',false)"><a href="#" title=""><img src="../../../tema/images/next.gif" alt="Ir p�gina siguiente" /></a></span></td>
            </tr>
        </table>
        
        <div id="divResultado" class="divResultado" ></div>

           <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>


       
        <br />
        <input type="Hidden" id="hdnIdCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnInm" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnCct" runat="server" value="" style="font-weight: bold" />
        <input type="Hidden" id="hdnTur" runat="server" value="" style="font-weight: bold" />
        <input id="hidIdCtrl" type="hidden" runat= "server" />
        <input id="hidListaTxtBoxs" type="hidden" runat = "server" />
        <input id="hidDisparador" type="hidden" runat = "server" value="90" /><br />
</center> 
         <div id="divWait">
                <div class="fondoDegradado">
                 <br /><br /><br /><br />
                <span style="color:White; font-size:22px; font-weight:bold; line-height:25px;">Almacenando y Validando informaci�n por favor espere,<br /><br />
                 Este proceso puede tardar....</span>
                 <br />
                </div>
         </div>
           <asp:Panel ID="pnlFallas" runat="server">
           </asp:Panel>
        <script type="text/javascript" language="javascript">
                 MaxCol = 20;
                MaxRow = 20;
                TabIndexesArray = MultiDimensionalArray(3,MaxRow,MaxCol);
                GetTabIndexes();
                
                var CambiarPagina = "";
                var hidListaTxtBoxs = '<%=hidListaTxtBoxs.ClientID %>';
                
 		   
 		        
 		        function OPTs2Txt(){
                     marcarTXT('1');
                     marcarTXT('2');
                     marcarTXT('3');
                     marcarTXT('4');
                     marcarTXT('54');
                     marcarTXT('55');
                } 
                function PintaOPTs(){
                     marcar('1');
                     marcar('2');
                     marcar('3');
                     marcar('4');
                     marcar('54');
                     marcar('55');
                } 
                function marcarTXT(variable){
                     var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                     if (chk != null) {
                         var txt = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (chk.checked)
                             txt.value = 'X';
                         else
                             txt.value = '_';
                     }
                }  
                function marcar(variable){
                     try{
                         var txtv = document.getElementById('ctl00_cphMainMaster_txtV'+variable);
                         if (txtv != null) {
                             txtv.value = txtv.value;
                             var chk = document.getElementById('ctl00_cphMainMaster_optV'+variable);
                             
                             if (txtv.value == 'X'){
                                 chk.checked = true;
                             } else {
                                 chk.checked = false;
                             }                                            
                         }
                     }
                     catch(err){
                         alert(err);
                     }
                }   
                
              PintaOPTs();
              Disparador(<%=hidDisparador.Value %>);
        </script> 
</asp:Content>
