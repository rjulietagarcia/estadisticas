﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasicMenu.Master" AutoEventWireup="true"
    Codebehind="SolicitudDBF.aspx.cs" Inherits="EstadisticasEducativas._911.SolicitudDBF"
    Title="Solicitud DBF" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    <table class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center"
        border="0">
        <tr>
            <td class="EsqSupIzq">
            </td>
            <td class="RepSup">
            </td>
            <td class="EsqSupDer">
            </td>
        </tr>
        <tr>
            <td class="RepLatIzq">
            </td>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td class="Titulo" style="font-weight: bold" align="center">
                            <asp:Label ID="lblTitulo" runat="server" Text="SOLICITUD DE BD DBF"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td align="right" style="width: 60px">
                                        <asp:Label CssClass="lblEtiqueta" ID="Label2" runat="server" Text="Ciclo : "></asp:Label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlCiclo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCiclo_SelectedIndexChanged">
                                            <asp:ListItem Text="Seleccione."></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td rowspan="5">
                                        <table>
                                            <tr align="center">
                                                <td colspan="2">
                                                    <asp:CheckBox onclick="habilitaFecha('ctl00_cphMainMaster_chkPorFecha')" Checked="false"
                                                        Text="Por Fecha" ID="chkPorFecha" runat="server" /></td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 100px">
                                                    <asp:Label CssClass="lblEtiqueta" ID="Label4" runat="server" Text="Fecha Inicio : "></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ReadOnly="true" Width="70px" ID="txtFecInicio" runat="server"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="ceInicio" Format="dd/MM/yyyy" runat="server" TargetControlID="txtFecInicio">
                                                    </cc1:CalendarExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" style="width: 100px">
                                                    <asp:Label CssClass="lblEtiqueta" ID="Label5" runat="server" Text="Fecha Fin : "></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ReadOnly="true" Width="70px" ID="txtFecFin" runat="server"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="ceFin" Format="dd/MM/yyyy" runat="server" TargetControlID="txtFecFin">
                                                    </cc1:CalendarExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 60px">
                                        <asp:RequiredFieldValidator ValidationGroup="valAdd" ControlToValidate="rblTipo"
                                            Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ErrorMessage="*">
                                        </asp:RequiredFieldValidator>
                                        <asp:Label CssClass="lblEtiqueta" ID="Label3" runat="server" Text="Tipo : "></asp:Label></td>
                                    <td>
                                        <asp:RadioButtonList ID="rblTipo" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Inicio" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Fin" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 60px">
                                        <asp:Label CssClass="lblEtiqueta" ID="Label6" runat="server" Text="Entidad : "></asp:Label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlEntidad" runat="server">
                                            <asp:ListItem Text="Seleccione" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 60px">
                                        <asp:Label CssClass="lblEtiqueta" ID="Label7" runat="server" Text="Cuestionario : "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSeleccione" Enabled="false" runat="server">
                                            <asp:ListItem Text="Seleccione un tipo de cuestionario"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlCuestionarioIni" runat="server">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlCuestionarioFin" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <asp:Label CssClass="lblEtiqueta" ID="lblError" ForeColor="red" runat="server"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddSolicitud" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAddSolicitud" ValidationGroup="valAdd" runat="server" Text="Agregar solicitud"
                                OnClick="btnAddSolicitud_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 21px">
                            <asp:Label ID="Label8" CssClass="lblSubtitulo1" runat="server" Text="Solicitudes Pendientes"></asp:Label>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="panelSol" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvSolPend" runat="server" Width="700px" AllowPaging="True" AutoGenerateColumns="False"
                                        OnRowCommand="gvSolPend_RowCommand" OnPageIndexChanging="gvSolPend_PageIndexChanging"
                                        PageSize="11">
                                        <Columns>
                                            <asp:BoundField DataField="Id" HeaderText="Id"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Fecha Solicitud">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# DateTime.Parse((Eval("FechaSolicitud")).ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Usuario" HeaderText="Usuario"></asp:BoundField>
                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus"></asp:BoundField>
                                            <asp:BoundField DataField="Tipo" HeaderText="Tipo"></asp:BoundField>
                                            <asp:BoundField DataField="Cuestionario" HeaderText="Cuestionario"></asp:BoundField>
                                            <asp:BoundField DataField="Id_Entidad" HeaderText="Entidad"></asp:BoundField>
                                            <asp:ButtonField CommandName="Cancelar" Text="Cancelar"></asp:ButtonField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label CssClass="lblEtiqueta" ID="Label18" runat="server" Text=" No existen solicitudes pendientes"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:Label ID="lblbNoSolicitudes" runat="server"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlCiclo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddSolicitud" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <br />
                            <asp:Label ID="Label9" CssClass="lblSubtitulo1" runat="server" Text="Descargas Disponibles"></asp:Label>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvDescDisp" runat="server" Width="700px" AllowPaging="True" AutoGenerateColumns="False"
                                        OnPageIndexChanging="gvDescDisp_PageIndexChanging">
                                        <Columns>
                                            <asp:BoundField DataField="Id" HeaderText="Id" />
                                            <asp:TemplateField HeaderText="Archivo">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label100" runat="server" Text='<%#"<a href=\"../DescargasDBF/"+(Eval("Archivo")).ToString()+"\"  >"+(Eval("Archivo")).ToString()+"</a>"%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label101" CssClass="lblEtiqueta" runat="server" Text=" No existen descargas disponibles"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:Label ID="lblNoDescargas" runat="server"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlCiclo" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddSolicitud" EventName="Click"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <div align="center">
                                <span class="divAviso">Las solicitudes serán atendidas a la brevedad.</span>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td class="RepLatDer">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="EsqInfIzq">
            </td>
            <td class="RepInf">
            </td>
            <td class="EsqInfDer">
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    habilitaFecha('ctl00_cphMainMaster_chkPorFecha');
    muestraCuestionarios('ctl00_cphMainMaster_rblTipo_0','ctl00_cphMainMaster_rblTipo_1')
    
    
    function habilitaFecha(IDchkF)
    {
        chkF=document.getElementById(IDchkF);
        
        txtIni=document.getElementById('ctl00_cphMainMaster_txtFecInicio');
        txtFin=document.getElementById('ctl00_cphMainMaster_txtFecFin');
        if(chkF.checked)
        {
            txtIni.disabled=false;
            txtFin.disabled=false;
        }
        else
        {
            txtIni.disabled=true;
            txtFin.disabled=true;
        }
    }
    function muestraCuestionarios(IDrb1,IDrb2)
    {
        rb1=document.getElementById(IDrb1);
        rb2=document.getElementById(IDrb2);
      
        ddlCFin=document.getElementById('ctl00_cphMainMaster_ddlCuestionarioFin');
        ddlCIni=document.getElementById('ctl00_cphMainMaster_ddlCuestionarioIni');
        //ddlSeleccione
        ddlSelect=document.getElementById('ctl00_cphMainMaster_ddlSeleccione');
        if(rb1.checked)
        {
            ddlSelect.style.display="none";
            ddlCFin.style.display="none";
            ddlCIni.style.display="";
        }
        if(rb2.checked)
        {
            ddlSelect.style.display="none";
            ddlCFin.style.display="";
            ddlCIni.style.display="none";
        }
        if(!rb1.checked && !rb2.checked)
        {
            ddlSelect.style.display="";
            ddlCFin.style.display="none";
            ddlCIni.style.display="none";
        }
    }
    </script>

</asp:Content>
